### Things we learned 

 It was a big process with a lot of learning, and yes, the result is a functional boat - but the process was quite rough with a lot of space for improvement. 

On the way we also made more tests and analysed the properties of the materials.
You can find a report here: 
https://drive.google.com/file/d/1Qf6IKGYNRBJ3DJkxxsOehNt4faN2SPxk/view?usp=sharing

And the document we shared in Step 1 has everything with more details as well :)