### Get ready  

 Check out the attached material list and get your materials ready for the mould and making the lights.

Equipment:
- Metalworking tools/workspace
- Extrusion machine
- shredded PS

A quick tip: make sure you make your mold longer than your lamp length! The extreme ends are always on a low quality so it’s easier to cut them off. I would say at least 5 cm longer on each side.


Related links:
Extrude beams 👉 tiny.cc/extrude-beams
Make glass-like beams 👉 tiny.cc/make-glasslike-beams

