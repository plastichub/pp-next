### Prepare your materials 

 For the mould we need:
- two cylindrical wooden elements (size as you prefer)
- threads and bolts (amount depends on the size of your lamp)
- paper sheets: one corrugated and one softer type

For the lampshade:
- shredded plastic, HDPE recommended

To finish the lamp:
- another wooden circle as support
- bulb socket and cable