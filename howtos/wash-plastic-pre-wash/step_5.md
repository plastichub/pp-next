### Build the mixer 

 The mixer should move the dirty plastic through the water. After some experiments and tests, this seemed to be the most efficient design for us. Short “arms” ensure that the plastic gets mixed without getting stuck.

Weld the metal sheets to to the steel pipe as shown.Drill two ø8mm holes on different heights of the steel pipe, so it can be fixed in two different positions: One to mix and one to move the barrel out underneath it.

You will also need a smaller hole to attach the gear later, but the location depends on how you will connect it. When done, paint it properly (or even better is to use stainless steel, because the friction will remove the paint fast).
Finally, drill a 20 mm hole in the centre of the lid, so the mixer can enter the barrel. 