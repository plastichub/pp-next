### Log splitter injector upgrade 


Need a little more power, or just feeling lazy? We've upgraded our PP injection mould machine using a 5T log splitter - this has made a world of difference to our production capabilities. In these videos, we discuss how we did it, as well as our process for creating thick parts.

Note: THIS UPGRADE IS DANGEROUS! Please take care with hydraulic machinery.