### Start the Aluminium Scales 

 We used 8mm aluminium plate for the scales. We traced the template onto the metal and cut out 2 pieces with an angle grinder. We also marked out the 3 hole positions for the pins and centre-punched these.