### Prepare your moulds 

 Like the machines you use in public your moulds have to be reliable as well. We recommend moulds with a small cycle time, so as many people as possible can produce a product...

That means:
- Quick release-System to open and close your mould fast
- Small injection volume
- The product is easy to demould
