### Import perspective view 

 -Select the item you wish to do a blueprint of in the view by left clicking the item
-Then holding CTRL & clicking on the page in the side menu
-Click on the "Insert Projection View" button
-Open the Page and you will see the drawing in the page by 
-You can adjust the rotation, scaling & which views are visible from the side menu
-Click "OK" in the task menu to confirm your choice
-If it's not updating click the refresh button in the top menu
-The good thing about doing it this was is that the blueprint and any dimensions you add should update if you update the base model