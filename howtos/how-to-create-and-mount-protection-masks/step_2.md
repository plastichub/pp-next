### Make the holes to the screen and attach it to the mask 

 Follow the guide to make the holes in your A4 sized sheet. 
They should be made at 3.1 cm, 10.2 cm, 19.3 cm and 26.4 cm; and between 1,5 to 2cm wide. We recommend using a vertical drill to make the holes to all your sheets much faster. 