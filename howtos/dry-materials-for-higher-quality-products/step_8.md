### Bonus tip 

 Rapid changes in the Temperature lead to surface moisture.
If a cold drink placed outside in the hot summer, you will notice that water starts condensing on the surface.
This also is the case for polymers. We had many problems because we took plastic from the cold, damp garage to the warm production area. This resulted in a lot of surface moisture that produced bubbles and inconsistencies in our 3D-Printing Filament.
