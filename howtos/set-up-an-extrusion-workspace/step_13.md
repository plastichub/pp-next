### Find the space 

 To help you find the perfect place for your workspace you can use the floor plan in the Download Kit, with all the minimum requirements and a little cardboard tool to place your machines and tools in the workspace. 