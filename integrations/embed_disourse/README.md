# embed-topics

Examples of using Discourse's embeddable topic lists.

You can view the topics at [embed.eviltrout.com](https://embed.eviltrout.com).

You might find the [feature topic on meta](https://meta.discourse.org/t/embedding-a-list-of-discourse-topics-in-another-site/125911)
informative, too.

### Building

1. Install the gems using `bundle install`

2. Build using `bundle exec jekyll build`


## interface

How to Embed a list of Topics
First, you must enable the embed topics list site setting.

Then, in your HTML add a <script> tag that includes the javascript necessary to embed the Discourse topics. You can add this wherever you normally add scripts. For example:

<script src="http://URL/javascripts/embed-topics.js"></script>

Replace URL with the forum address, including the subfolder if it exists.

After that, in the <body> of your HTML document, add a a tag to indicate the list of topics you�d like to embed. You�ll need to replace URL with your base URL here too:

<d-topics-list discourse-url="URL" category="1234" per-page="5"></d-topics-list>

Any attributes you provide (besides discourse-url which is required) will be converted into the query parameters for the topic search. So if you wanted to search topics by tag you could do this:

<d-topics-list discourse-url="URL" tags="cool"></d-topics-list>

If a query parameter has underscores, convert it to dashes. In the above example, you probably noticed thatper_page became per-page.

Finally it�s worth mentioning that Discourse is smart enough to know if you�re logged into the forum, and will display the results accordingly. Don�t be surprised if you see secure categories and such when logged in - anonymous users will not be able to!