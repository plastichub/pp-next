### Intro 

 Okay, so first off, this boat is a first of its kind, a prototype. The processes we used were done for the first time and are not perfect (far from that actually!). So don’t take this as a guide to copy identically but more as something to learn and get inspired from :)

If you want to dive more into details, see more photos, and test results, have a look at the document here:
👉 https://drive.google.com/file/d/1NZgn58G6XC_cCiG8W5toE4hkAw53BlPY/view?usp=sharing