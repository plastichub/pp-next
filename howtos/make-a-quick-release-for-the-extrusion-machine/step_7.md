### Drilling extrusion hole 

 With all the parts tightly together now, drill a hole in the centre of the welded metal strip. ø10mm is recommended but depends on your planned product input diameter. 
