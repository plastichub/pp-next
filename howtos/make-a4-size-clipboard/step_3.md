### Postprocessing 

 Cut out edges with knife,
You can sand surface.
Drill 2 holes M4 (size and location of holes depends on your clip design).
I prefer screw type rivets - you do not need special tools and you can make easy and quick change board design with clip.

Rivet size:

A: 4mm
B: 6mm
C: 10mm
S: 4mm
