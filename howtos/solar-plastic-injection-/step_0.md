### Prepare the setup 

 You need :

- volks.eco solar concentrator 
- Hydraulic Jack press
- Cartridge made with a plumbing pipe
- Shredded HDPE
- Injection mold
- Cup of water
- Oven gloves
- Clamp

Find a sunny place and preheat the solar tube concentrator