### Collect the valuable offcuts 

 Processes like drilling create a lot of small plastic pieces. Try to collect all the offcuts you generate since they can be used perfectly as pellets for your further projects - and that also saves them from ending up as microplastic in the environment!