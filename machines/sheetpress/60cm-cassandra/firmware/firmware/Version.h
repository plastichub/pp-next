#ifndef VERSION_H 

    #define VERSION_H 

    #define VERSION "1.0.0|PlasticHub|OSR|Cassandra|4e54b1577297690d9cf6c1b7635c773c26fa26fa"

    #define SUPPORT "support@plastic-hub.com | www.plastic-hub.com"
    #define BUILD "Cassandra-RevA"
    #define FW_SRC "https://gitlab.com/plastichub/pp-next/tree/master/machines/sheetpress/60cm-cassandra/firmware/firmware"
    #define CID "-1"

#endif