### Make it live long! 

 Congratulations, you made it! Enjoy and take care of your new chair.

This chair is designed to be disassembled easily. So in case a part breaks or you want to change something, just unscrew the seat from the frame, unbolt the backrest and take it off. And lastly, hit the metal plug with a hammer to break the backrest apart (here we recommend wearing safety glasses to protect your eyes).

Once you have all materials separate again, they can be reused or recycled for new products. Make sure to bring them to your local Precious Plastic workspace or recycle in another responsible way :)
