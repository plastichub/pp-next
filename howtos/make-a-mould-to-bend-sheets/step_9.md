### Making the negative mould: screws 

 The process is pretty much the same as for the positive mould, you start as well from the middle. Here, you just have to clamp the sheet metal down first and attach it in the middle. Then again, go outwards, the only difference is that no wedges are necessary.