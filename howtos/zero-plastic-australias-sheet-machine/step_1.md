### Setting up your Mold 

 We will be using HDPE to make these sheets (however have made sheets using PP and LDPE)

1. Add 1 Aluminium sheet to your heat press.
2. Add Mold. 
3. Fill Mold with 800 grams of HDPE shredded plastic.
4. Add the other Aluminium sheet. 

If you have a mold release i would recommend added it to the sheets and mold. 