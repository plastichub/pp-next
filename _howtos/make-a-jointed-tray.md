---
image: "/howtos/make-a-jointed-tray/Tray-3.jpg"
title: "Make a Jointed Tray"
tagline: ""
description: "Working with sheets to make valuable objects with wood joint techniques."
keywords: "product,compression,sheetpress"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "compression"
- "sheetpress"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/leoroth">leoroth</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - 3D Modelling the product</h4>
<div class="step-text">
  In this case we used Solidworks to model and render the product so we could export the necessary patterns and to render the product so the client could see it and approve it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/modelado 1.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/modelado 1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/modelado 2.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/modelado 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/Process-0.1.png">
        <img class="step-image" src="/howtos/make-a-jointed-tray/Process-0.1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Print the patterns</h4>
<div class="step-text">
  They are the key to make it accurate cuts and thereby good joints.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/Base-01.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/Base-01.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make sheet and make rough cuts</h4>
<div class="step-text">
  Once you make the sheet, we made it in PLA, cut it so you get the five pieces necessary to make de product. Depending on the cutting tool you could make it more or less accurate. We make them with a not so precise endless saw, so the were rough cuts and then we improved the edges with a router.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/1 y 2.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/1 y 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/2.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Transfer the patterns and cut</h4>
<div class="step-text">
  Fists transfer all the markings to the pieces and start cutting, the key here to cut a little less than necessary and then try the joint, cut more and repeat this process until it&#39;s a perfecto fit. Besides de cutting, we used a lime to get more soft edges and little dimensional changes.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/5 y 6.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/5 y 6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/7.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/7.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/10.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/10.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Final assembly and welding</h4>
<div class="step-text">
  Once you have all nice and fitted, with a heat gun and a spatula we weld locally some joint so the entire tray stays always nice and sturdy.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/IMG_20200630_131031.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/IMG_20200630_131031.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/IMG_20200630_104845.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/IMG_20200630_104845.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/IMG_20200630_131110.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/IMG_20200630_131110.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Finally, nice set up & photos</h4>
<div class="step-text">
  Contextualise and details :)<br />
  <br />
  More photos here:<br />
  <a href="https://www.instagram.com/p/CCjX6kUAuLU/">https://www.instagram.com/p/CCjX6kUAuLU/</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/Final-1.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/Final-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/Final-4.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/Final-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-jointed-tray/Final-3.jpg">
        <img class="step-image" src="/howtos/make-a-jointed-tray/Final-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>