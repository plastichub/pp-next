| Specification    |     |
|----------|-------------|
| Type   |     Single Screw   |
| Weight (inc frame) |   110 kg   |
| Dimension   | 1500 x 600 x 1550 mm|
| Power (W) | 5 kW|
| Voltage | 380V or 220V |
| Amperage | 16A |
| Input Flake Size  | Small  |
| Screw diameter | 30mm |
| Length of screw (mm) | 790 mm |
| Effective screw length | 600 mm |
| Rated Motor Power | 3 kW |
| Motor Type   |    |
| - Rated Motor output Torque |  109 Nm |
| - Rated Motor output speed |   263 RPM   |
| - Max. Motor and Inverter power   | 3 kW|
| - Recommended motor shaft   | 30 mm|
| - Heating zones   | 3 |
| - Heating power: max.   | 2 kW|
