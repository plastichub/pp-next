### Pouring the Silicone & Clean Up 

 Pouring:
Concentrate on the highest point and the centre of the object—pour directly onto the most detailed section first, then moving around the object making sure the silicone reaches every part of the surface and the object is evenly coated.
Leave the silicone to set (approx 30 mins)

Cleanup:
Leave the excess silicone on your jug and stirrer. Let it to dry fro 30 mins. Once dry the silicone can be pealed off leaving the jug and stirrer perfectly clean.