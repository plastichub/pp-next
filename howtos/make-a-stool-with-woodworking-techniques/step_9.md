### Finish top 

 First use a flexible japanese saw in order to remove the part from the leg that sticks out from the top. Bend it and try to avoid deep scratches on the top. Once the majority of the material is removed you can use a chisel to take out the rest and making it as even and flat as possible. 

Finally you can sand the top going from 400 to 1200.
