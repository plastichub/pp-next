### Make the Pedal Mold: Weld It 

 Before welding you need to add an angle (around 2 degrees) on each vertical face of the wooden parts (triangle, trapezes and pedal). This will ease the extracting process. I accomplished this with the sanding machine. 

Weld the pedal perimeter: Clamp the wooden pedal on your welding surface and clamp the edges you want to weld (cf. picture). Continue with the other parts always ensuring that your parts are well against the ground. It was my first arc welding job and you can notice the poor result but it doesn't matter, it just extend the grinding time. 