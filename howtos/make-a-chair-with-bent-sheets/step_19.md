### Drill holes for bolts 

 Take the position of the tapped holes from the connector plugs and transfer them to the tube. After that, drill holes (Ø6mm) on the inner median of the tube’s bending radius. They will later hold together the frame and the connector plugs. It’s nice to use countersunk screws, they will make the joint invisible. 
