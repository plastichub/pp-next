### Making the positive mould: wedges 

 As soon as you’re close to the end, you can use the previously cut pieces to wedge the sheet metal in place. Fix those wedges with care as they are under a constant tension and might flip out.