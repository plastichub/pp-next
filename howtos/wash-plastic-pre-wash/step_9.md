### How to pre-wash 

 Now it is time to connect all the pipes to the barrels and the sand filter. Fill up with water and you are good to go. Before using the pre-washing machine, we recommend to reduce the size of bigger plastic pieces and to remove the very rough dirt like sauces etc. 

Mix the water in the pre-washing machine with laundry powder or soap, turn on the sand filter so there is a constant loop of water splashing at the plastic and let the motor run.

After a few minutes, your plastic should be clean enough to shred. You can experiment with temperature, the amount of soap and the motor speed.
