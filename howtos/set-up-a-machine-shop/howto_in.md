### Set up a Machine Shop 


In this How-to we’re going to guide you through all the steps to set up a  Machine Shop. Learn about machines, expertise, how to find a space, get your equipment, find customers and connect to the Precious Plastic Universe. 

Download files: 
👉 https://cutt.ly/starterkit-machine 👈

Step 1-3: Intro
Step 4-9: Learn
Step 10-16: Set up
Step 17-22: Run
Step 23-25: Share