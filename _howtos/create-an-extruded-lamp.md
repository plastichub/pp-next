---
image: "/howtos/create-an-extruded-lamp/how-to-extruded-lamp-31.png"
title: "Create an extruded lamp"
tagline: ""
description: "We&#39;ll show you how to make a lamp using the extrusion machine and a very simple mould.<br />Simple, beautiful, functional.<br /><br />Workspace: KOUN, Morocco"
keywords: "extrusion,HDPE,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "HDPE"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prepare your materials</h4>
<div class="step-text">
  For the mould we need:<br />
  - two cylindrical wooden elements (size as you prefer)<br />
  - threads and bolts (amount depends on the size of your lamp)<br />
  - paper sheets: one corrugated and one softer type<br />
  <br />
  For the lampshade:<br />
  - shredded plastic, HDPE recommended<br />
  <br />
  To finish the lamp:<br />
  - another wooden circle as support<br />
  - bulb socket and cable
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-30.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-30.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-19.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-19.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Assemble the mould</h4>
<div class="step-text">
  Once you’ve got all the components, it’s time to assemble the mould. Pick the length you prefer for your lamp. Connect the 2 wooden circles with each other using the 6 rods, and tighten all together.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-15.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-15.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-09.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-09.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Wrap the mould in paper</h4>
<div class="step-text">
  We need to create a smoother skin on the mould for the plastic to lay on. We use one corrugated sheet of paper to give the structural strength for the plastic not to collapse. On top of this first layer, we lay a second paper layer which is smoother to give a clean finishing to the product. Again, size depends on your product but generally it should cover the area between the two round elements. We use paper because plastic does not stick to it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-10.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-10.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-11.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-11.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-12.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-12.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-27.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-27.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-23.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-23.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Mark the mould </h4>
<div class="step-text">
  To help you being consistent with size over time, mark the highest point where you want the extruded plastic to reach.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-07.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-07.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Extrusion time - Make the base</h4>
<div class="step-text">
  Get your recycled plastic ready and load it into the extrusion machine.<br />
  <br />
  Wait for the plastic filament to come out and start overlaying multiple plastic strings. First we build a solid base for the lamp: We overlap 9 lines of plastic. Make sure each line melts into the previous one so they stick well onto each other.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-26.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-26.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-22.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-22.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Extrusion time - Make the pattern</h4>
<div class="step-text">
  Once the base is nicely laid out we start creating the actual lamp’s pattern by moving the mould up and down like in the pictures below. You can get creative with the patterns created but generally you will want to create enough of a structure for the lamp to support itself.<br />
  <br />
  When you’re happy with the pattern you can cut the end of the extrusion line and glue it nicely to the rest of the product.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-03.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-03.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-04.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-04.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-38.jpg">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-38.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Wait and cool</h4>
<div class="step-text">
  Once we’re done with extruding, we let it cool down. Make sure it is well cold before removing it from the mould, otherwise the plastic will shrink unpredictably and ruin your final product. <br />
  We cool it by simply letting it rest for 20-30 mins.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-02.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-02.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Remove the mould</h4>
<div class="step-text">
  The lamp is designed to have one of the circles as the base of the lamp. So, when disassembling the mould, only remove the top circular wooden element and the metal rods and bolts.<br />
  <br />
  We can now remove the paper from inside of the lamp.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-37.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-37.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-34.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-34.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-35.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-35.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-36.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-36.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Ready, almost</h4>
<div class="step-text">
  Now we’re left with the core of the lamp made of the wooden base plus the beautiful plastic patterns.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-39.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-39.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Electronic support</h4>
<div class="step-text">
  Let&#39;s make the lamp functional!<br />
  First, we make another wooden circle to support the cables and electronics. Drill two guides in the wood to allow the cables to stay in the right place.<br />
  <br />
  Then get the cables through the holes and connect the socket to the wooden support.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-18.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-18.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-44.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-44.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-45.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-45.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-46.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-46.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Connect it all</h4>
<div class="step-text">
  Time to connect it all together. Get the cable connector with the lamp and close it together.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-41.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-41.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-42.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-42.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-43.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-43.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Finished!</h4>
<div class="step-text">
  And we&#39;re done :) <br />
  Lamp is ready to be sold or make your living room shine from recycled plastic!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-01.png">
        <img class="step-image" src="/howtos/create-an-extruded-lamp/how-to-extruded-lamp-01.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>