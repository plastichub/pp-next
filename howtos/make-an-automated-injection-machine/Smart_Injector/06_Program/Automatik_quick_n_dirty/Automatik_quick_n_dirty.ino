/*
 Name:    automatik_mode_quick_and_dirty.ino
 Created: 8/17/2020 11:41:14 PM
 Author:  Manuel Maeder
*/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 4);

//set parameters in programm
int rotationsfor = 70;	//rotations forward for injecting
int delaybetween = 1;	//delay between forward rotation and backward rotation
int rotationsback = 18;	//rotations back after injecting
int waitbeforeopen = 1;	//delay before mould is opened

//stepper left side
int EN = 40;				
int PUL = 41;				
int DIR = 42;				

//stepper right side
int EN2 = 37;				
int PUL2 = 38;				
int DIR2 = 39;				

//stepper extruder
int EN3 = 34;				
int PUL3 = 35;				
int DIR3 = 36;				

//variables for buttons
int b1 = 19;
int b2 = 16;
int b3 = 18;
int b4 = 14;

int valb1;
int valb2;
int valb3;
int valb4;

int entprell = 300;

//variables for extruder
int Xdtref = 600;
int Xdt = Xdtref;
int Xdtend = 150;
int Xdtendback = 250;
int Xstepsinfor = 6*400;	//400 steps per rev has to be multiplied by 6 for one rev of Extruder (because of the gear ratio)
int Xstepsslowingdown = 250;
int Xcurrentrotations = 0;

//variables for clamp_mechanism
int Kdtref = 200;
int Kdt = Kdtref;
int Kdtend = 100;
int Kstepsinfor = 200;

int Kendstop1 = 22;
int Kvalendstop1;
int Klimitedsteps1 = 9000;	//more steps after triger endstop1

int Kendstop2 = 43;
int Kvalendstop2;
int Klimitedsteps2 = 32000;	//more steps after triger endstop2

//relais for heating mould
int q1 = 30;
int q2 = 31;
int q3 = 32;


//the setup function runs once when you press reset or power the board
void setup()
{
	pinMode(EN, OUTPUT);
	pinMode(PUL, OUTPUT);
	pinMode(DIR, OUTPUT);

	pinMode(EN2, OUTPUT);
	pinMode(PUL2, OUTPUT);
	pinMode(DIR2, OUTPUT);

	pinMode(EN3, OUTPUT);
	pinMode(PUL3, OUTPUT);
	pinMode(DIR3, OUTPUT);

	pinMode(b1, INPUT_PULLUP);
	pinMode(b2, INPUT_PULLUP);
	pinMode(b3, INPUT_PULLUP);
	pinMode(b4, INPUT_PULLUP);

	pinMode(Kendstop1, INPUT_PULLUP);
	pinMode(Kendstop2, INPUT_PULLUP);

	pinMode(q1, OUTPUT);
	pinMode(q2, OUTPUT);
	pinMode(q3, OUTPUT);

	Serial.begin(9600);

	lcd.init();
	lcd.backlight();

}

// the loop function runs over and over again until power down or reset
void loop()
{
	digitalWrite(q1, LOW);
	digitalWrite(q2, LOW);
	digitalWrite(q3, LOW);
	
	while (1)
	{
		nopower();
		Serial.println("wait for signal");
		valb1 = digitalRead(b1);
		valb2 = digitalRead(b2);

		if (valb1 == 0)
		{
			valb1 = 1;
			Serial.println("starts soon");
			delay(3000);

			close_mould();
			forward();
			Serial.println("open");
			delay(delaybetween * 1000);
			open_mould();
			backward();
			delay(waitbeforeopen * 1000);
		}
		if (valb2 == 0)
		{
			backwardsmanuell();
		}
	}
}

// no current on steppers
void nopower()
{
	digitalWrite(EN, LOW);
	digitalWrite(EN2, LOW);
	digitalWrite(EN3, LOW);
}
