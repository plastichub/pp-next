### Make a box to cast your mould 

 Make a box around your model and ensure everything is sealed and secure (you don't want your model floating up when you pour the plaster). For the box, melamine works really well but you can also use plywood. 

You might also want to use a mould release (vaseline works!) to make sure the plaster releases more easily. 

Some reference pins are also handy to make sure the moulds line up with each other later on.