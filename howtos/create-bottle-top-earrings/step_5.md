### Finish the plastic 

 Once the tiles have cooled (around 10 minutes) you can separate them and reveal your freshly pressed plastic discs. With practice, they should come out nearly perfectly round, but if some don't have the desired shape, now is the time to finish them. This can be done by sanding or cutting to achieve the desired outcome. Alternatively, you can select the best and save the rest for re-shredding.
