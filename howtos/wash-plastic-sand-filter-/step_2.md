### Drill holes in the barrel 

 We start by preparing the barrel. All you have to do is remove the handles and drill three holes.

Mark the centres of the holes according to the measurements on the picture. Try to use a 45 mm Forstner drill bit for the big holes and a 20 mm one for the smaller hole (you can easily sand off the extra milimeter).