<?php
namespace SR;

require __DIR__ . './../vendor/autoload.php';

use SR\ArrayUtils;
use SR\Session;
use SR\VEvent;

class Events
{
    public static $TYPE = 'type';
    public static $VID = 'viewId';
    public static $TIME = 'time';

    public static function removeParts($partsString, &$events)
    {
        if (!$partsString) {
            return $events;
        }
        $parts = explode('|', $partsString) ?: [$partsString];

        foreach ($events as &$ev) {
            foreach ($parts as $part) {
                if (isset($ev, $prop)) {
                    //error_log('unset2 ' . $part);
                    unset($prop, $ev);
                }

            }
        }
        return $events;
    }

    public static function updateTimes($events, $start, $end)
    {
        for ($i = 0; $i < count($events); $i++) {
            $next = $events[i + 1];
            if ($next) {
                $events[i]->end = $next->start;
            }
        }

        if ($events && $events[0]) {
            $events[0]->start = $start;
            $events[count($events) - 1]->end = $end;
        } else {

        }
        return $events;
    }
    public static function view($events)
    {

        return $events;
    }
    public static function sortByTime(&$events)
    {
        usort($events, function ($a, $b) {
            return $a->time > $b->time;
        });
        return $events;
    }

    public static function filterByTime($events, $start, $end)
    {
        $ts = [
            'start' => $start,
            'end' => $end,
        ];
        return array_values(array_filter($events, function ($e) use ($ts) {
            return $e->time >= $ts['start'] && $e->time <= $ts['end'];
        }));
    }
    public static function filterByType($types, $events)
    {
        $types = $types ?: '';
        if (strlen($types) == 0) {
            return $events;
        }
        $parts = explode('|', $types) ?: [$types];
        return array_values(array_filter($events, function ($e) use ($parts) {
            return in_array($e->type, $parts);
        }));

    }

    public static function eventPayload($obj)
    {
        $ret = clone (object) $obj;
        unset($ret->{self::$TYPE});
        unset($ret->{self::$TIME});
        unset($ret->{self::$VID});
        return $ret;
    }
    public static function toRawEvents(Session $session, $startTS)
    {
        $events = $session['events'];
        if (is_string($events)) {
            $events = json_decode($events, true);
        }

        $events = $events ?: [];
        $ret = [];
        foreach ($events as &$e) {
            if ($e['type'] == 'e') {
                $payload = $e['payload'] ?: [];
                foreach ($payload as $evt) {
                    // d($evt, 'events');
                    if (!$evt) {
                        break;
                    }
                    $ve = new VEvent(
                        $startTS,
                        $evt['type'],
                        $e['startTime'] + $evt['time'],
                        0,
                        -1,
                        (array) self::eventPayload($evt),
                        ArrayUtils::get($evt, 'tabId', ''),
                        [
                            's' => $e['startTime'],
                            'e' => $e['endTime'],
                            'time' => ArrayUtils::get($evt, 'time'),
                        ]
                    );
                    array_push($ret, $ve);
                };
            }
            if ($e['type'] == 'b') {
                
                $ve = new VEvent(
                    $startTS,
                    'frame',
                    $e['startTime'],
                    0,
                    ArrayUtils::get($e, 'sn'),
                    [
                        'value' => '' . base64_decode($e['payload']) . '',
                    ],
                    0,
                    [
                        's' => $e['startTime'],
                        'e' => $e['endTime'],
                        'time' => ArrayUtils::get($e, 'time'),
                    ]
                );
                array_push($ret, $ve);
            }
        }
        usort($ret, function ($a, $b) {
            return $a->time > $b->time;
        });
        return array_values($ret);
    }
}
