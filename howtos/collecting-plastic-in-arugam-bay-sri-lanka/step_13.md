### Products 

 With the PE bottle caps collected we make a variety of products including key rings, surf wax combs, buttons and buckles. These products are used within our company, sold to privates, or to fair trade wholesalers.
