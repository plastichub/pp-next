### Set the right temperature 

 A bending machine is a pretty basic machine that relies on two heating elements to warm up the plate just enough to perform a bending without breaking the piece. 

The first step is to set the machine at the right temperature.
We can recommend:
- for PS: 90-100ºC above the melting point, to reduce the exposure timing.
- for HDPE and PP: 50-60ºC above the melting point. These materials take longer to fully heat up and increasing the temperature could burn the surface before melting the inside. Therefore, longer exposure periods tend to give better results. 