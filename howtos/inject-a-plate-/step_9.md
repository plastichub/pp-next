### oh man! Injection time!   

 Fully fill the injector with material, but be careful not to overfill as this can lead to plastic glueing to the hopper or outside of the injector. Set your temperature based on the correct melting temperatures for your plastic type. It’s good to make the lower heating element slightly hotter than the upper, to avoid solidification of the plastic. 
Move the injector lever up and down to push the material deeper into the barrel. Afterwards add more material, and leave the handle bar down to keep pressure on the plastic. After around 15 min heating lift the handle bar up and open the injector. Cut away the first drip of plastic from the barrel (might still contain some unmelted plastic), and directly screw your mould to the injector. 
Wear a protective mask and eye cover, as well as heat resistant gloves during this part of the process. Safety first.
Inject your plastic by pulling down with all the pressure that you can manage, to create one continuous injection. You will need to maintain pressure for 30 seconds. Do not press in parts! If you are not sure that your strength and weight are enough, you can ask someone to help you in applying force to the injection. 

