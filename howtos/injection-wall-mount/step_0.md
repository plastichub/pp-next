### Get ready 

 We have created a simple wall mount so you don’t need to make the entire bottom half of the frame. Moreover, this mount makes the machine way more stable and solid. Nothing moves. However, keep in mind that it doesn’t fit every place and you can’t move it around anymore. You can find the technical drawing in our download-kit.