### Transfer the sheets 

 Two minutes before the mould is due to be transferred from the sheetpress to the cooling press, apply pressure using the jack one last time, fully. During this two minutes scrape off any overflow from the mould to keep the transfer process clean. 

Once the melting time has completed, open the sheetpress. Using the sliding tool slide the sheet from the sheetpress into the cooling press. 
Make sure to wear gloves. If you need to pull it from the other side you can use pliers to grip the edge of the bottom mould sheet allowing it to be moved. Do not pull the top mould sheet as it can open the mould. 

Close the cooling. Once the coldpress is fully closed, close the sheetpress to keep the heat.