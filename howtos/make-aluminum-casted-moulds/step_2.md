### Done & Share 

 After watching this video you should understand the basics of aluminium casting. You can reproduce the wall grips or try to make other products. We’re looking forward to seeing what you can come up with. Make sure to share back your new creations so the community can learn from you. Tag #preciousplastic on social media, create a new how-to or send us an email. 
