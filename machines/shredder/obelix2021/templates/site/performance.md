| Material | Description | Times shredded | Output (Kg/h) | Sieve | Size |
|---|---|---|---|---|---|
| HDPE | Shampoo bottles | 1st time  | 36,7 | No  | Too big  |
|   |   |  2nd time | 375  | No  |  Large |
|   |   |  **Total** |  **30.9** |   | **Large**  |
| PS | Transparent CD cases | 1st time  | 9.8 | No  | Medium  |
|   |   |  2nd time | 86.5  | 7 mm sieve  |  Small |
|   |   |  **Total** |  **8.8** |   | **Small**  |
| PP | Bottle caps | 1st time  | 76.7 | No  | Large  |
|   |   |  2nd time | 92  | 10 mm sieve  |  Medium |
|   |   |  **Total** |  **41.8** |   | **Medium**  |