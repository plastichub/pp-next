### Make the cross-braces 

 Next up it’s time to bend the seat cross-braces (parts E+F). Cut them in length approximately in a 35° angle and mark the position of the indicated holes with a center punch. 

Then bend them to the shape of the jig by clamping or hammering. This step requires a bit of experience, but it doesn’t need to be perfectly precise, as you will have the opportunity to make corrections once the seat is screwed together.

Now drill and countersink the previously marked holes, approximately perpendicular to the curvature. Take your tube/round bar for the cross-brace (part G) and notch it as shown in step 11. 

Put everything aside for welding during the assembly.
