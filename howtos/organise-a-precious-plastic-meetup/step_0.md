### Get your contacts ready 

 Alright, let’s start thinking about the people who should join the meetup. Especially for the first meetup you probably want to bring together people who already know or work with Precious Plastic as well as potentially interested ones.

Best is if you already made a search in your area and set up a communication channel for the people who are involved in the local (Precious Plastic) recycling or interested in it.

If not done yet, follow the steps 4-9 from the Community Point Starter Kit to get some tips to start the conversation in your local community.

👉 tiny.cc/starterkit-community



