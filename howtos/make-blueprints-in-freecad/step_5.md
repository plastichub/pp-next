### Add Linear Dimensions 

 -Select two points by holding CTRL and clicking on the two points you need
-Click the horizontal or vertical dimension buttons & adjust location of the dimension to your needs 
-Adjust the location so the dimension is legible
-Repeat till all relevant information is present