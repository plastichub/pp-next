### Adding the bottom and top frame 

 Once all 4 sides have been welded. Place part number (BG5) on the bottom and part (BI5) on the top. This may need a gentle hammering to fit, but must not be forced otherwise it may bend. 

Once in place clamp together. Ensuring the inside surface is smooth and weld any external surface.

