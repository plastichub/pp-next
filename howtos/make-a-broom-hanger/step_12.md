### Mould done! 

 And you’re done! Here is your broom hanger mold, it's time to inject.
Flexible plastics like HDPE and PP work better for the broom hangers as they won't crack during use. 

To open the mold, us a flat screwdriver to pull apart the parts gently. To take out the plastic product, use the flat screwdriver or a putty knife to open and release it from the male mold. It's easier if you do this process when the plastic part is still hot, but remember to close it back to its original shape after releasing.


