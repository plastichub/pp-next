### Extrude (2) - Cloudy colours 

 Now we use the re-shredded excess material from the previous process which makes the colour more cloudy. In this example we used the following mixture:
Mixture: 70% clear PS, 30% shredded PS-pigment-mix

Start by putting your clear PS into the extruder followed by the PS-pigment-mix. You can experiment with adding more colour by putting pigment back into this mixture to create dynamic colour tones and patterns.

P.S. Keep collecting your left over material to melt it again, this is the stuff that will make your material glass-like.