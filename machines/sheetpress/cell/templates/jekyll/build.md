## General

- [BOM - Bill of materials](${product_bom})

### Build Video

[Build Video](https://www.youtube.com/watch?v=j3OctDe3xVk&feature=emb_title)

  - [Lower frame](https://youtu.be/j3OctDe3xVk?t=213)
  - [Side frames](https://youtu.be/j3OctDe3xVk?t=279)
  - [Press - Plate - Mount : **Dont!**](https://youtu.be/j3OctDe3xVk?t=443)
  - [Final test](https://youtu.be/j3OctDe3xVk?t=990)

<hr/>

### Requirements

${requirements}

[Table Source](${requirements-source})


<div class="container">
  <div class="row">
    <div class="col">
      {% include product_image.html download=false size="medium" src="resources/mini_drillpress.JPG"  title="Mini Drill Press" %}
    </div>
    <div class="col">
      {% include product_image.html download=false size="medium" src="resources/forklift.JPG"  title="Forklift" %}
    </div>
    <div class="col">
      {% include product_image.html download=false size="medium" src="resources/press.JPG"  title="Shop Press" %}
    </div>
    <div class="col">
      {% include product_image.html download=false size="medium" src="resources/cheater-press.JPG"  title="Pressing cartridge heaters" %}
    </div>
  </div>
</div>

<hr/>
