### Get ready for melting 

 Set your oven to preheat at 170°C. 
Whilst it’s warming up, set down one of your tiles and lay out the bottle tops with even and sufficient spacing to allow for the caps to melt without fusing to one another. With your coloured shreds, put a little sprinkling of plastic into each bottle top. This will create a nice aesthetic to the finish! You will not need to add much, as the pressed material should be relatively thin.