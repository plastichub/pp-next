---
image: /_howto/extrusion/shaft_clamp/IMG_1969.JPG
category: "extrusion"
title: "Shaft Clamp"
tagline: ""
description: ""
usedin:
  - zoe
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_1965.JPG" _target="_blank">
        <img id="IMG_1965.JPG" src="./IMG_1965.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1968.JPG" _target="_blank">
        <img id="IMG_1968.JPG" src="./IMG_1968.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1969.JPG" _target="_blank">
        <img id="IMG_1969.JPG" src="./IMG_1969.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

