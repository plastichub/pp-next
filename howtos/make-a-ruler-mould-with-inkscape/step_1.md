### Create a blank document 

 -Open Inkscape and it should open a new document for you
-You can adjust the document size by clicking File > Document Properties
-We chose A4 for this project but you can adjust this to your needs
-Save it down in a place you can find later