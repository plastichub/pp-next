### Run a workshop on an event 


A crucial part of working towards reducing plastic pollution is to show people the process offline! The following introduction gives you a quick overview about all the stuff we at Kunststoffschmiede pay attention before starting a workshop in public. 