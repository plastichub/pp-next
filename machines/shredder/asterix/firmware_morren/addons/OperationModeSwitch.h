#ifndef OPERATION_MODE_SWITCH_H
#define OPERATION_MODE_SWITCH_H

#ifdef HAS_STATES
#include <ArduinoJson.h>
#endif

#ifndef OP_MODE_ANALOG
#include <Bounce2.h>
#endif

#include "../config.h"
#include "../Addon.h"
#include <Streaming.h>
#include "../common/macros.h"
#include "../common/ppmath.h"

class OperationModeSwitch : public Addon
{

public:
  short pin1;
  short _value;
  OperationModeSwitch(short _pin1) : pin1(_pin1),
                                     Addon(OPERATION_MODE_SWITCH_STR, OPERATION_MODE_SWITCH),
                                     _value(OP_NORMAL)
  {
    //setFlag(DEBUG);
  }
#ifdef HAS_STATES
  String state()
  {
    const int capacity = JSON_OBJECT_SIZE(2);
    StaticJsonDocument<capacity> doc;
    doc['0'] = id;
    doc['1'] = value();
    return doc.as<String>();
  }
#endif

  void debug(Stream *stream)
  {
    *stream << this->name << SPACE(value());
  }
  void info(Stream *stream)
  {
    //*stream << this->name << "\n\t ";
  }

  short value()
  {
    return _value;
  }

  short loop()
  {
    _value = digitalRead(pin1) ? OP_NORMAL : OP_MANUAL;
    return _value;
  }
};

#endif
