### Time to rescue 

 Now it’s time to save these single-use beauties from the waste into the Precious Plastic recycle flow. By collecting a certain product, that does come in regularly, you will create a more valuable recycled plastic kind for the shredder workspace to buy. This because you know that it will always have the same colors, quality, weight and that it is clean. By separating this plastic, from the sticky labels plastic you keep the quality as high as possible.

