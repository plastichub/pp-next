### Injection Faceshield 


With the current, global situation of COVID-19 we face many challenges.


One challenge is the short pass and access to safety equipment.

With our machines in place the batch production of products like a Face Shield can start immediately.

With this Mould you are able to produce around 120 Shields per day, with one person and one machine.