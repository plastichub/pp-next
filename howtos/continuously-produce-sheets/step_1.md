### Write a timetable 

 The heating and cooling time are typically the same. 

You can offset each of the three transitions (1. Load in to sheetpress, 2. transfer into cooling press 3. Unload, demould and store) by half the cooling time (In this example 40 mins / 2 = 20 mins) to give you two nice big windows of time. You can use one of the windows to demould the sheet and one of the windows to prepare the next sheet. 

To make sure you don't miss any of the steps you can use a simple kitchen timer as a reminder.