### Measure the space that is needed 

 By letting someone in the local area collect their plastic waste for a week or 2, you will know how much space you will need to lay this out in the layout. Layout their collected plastic in the categories that you choose. Start to measure the outlines per category and the outline of the whole layout. 

