### Make a ruler mould with Inkscape 


Educational resources are not equally distributed across the world so you might as well recycle plastic and make products that are useful tools for learning and creating at the same time. In this how-to you will learn how to use Inkscape to make a laser cut ruler mould for injection moulding!