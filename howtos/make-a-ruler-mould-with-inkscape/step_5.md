### Adjust for laser cutting 

 If you are laser cutting a mould you need to ensure that the text is flipped so it reads correctly when you inject the part

-Select the part you wish to flip
-Then click on the "Flip selected objects horizontally" button on the top of the page
-You can also use the "Rotate selection 90°" buttons to adjust as necessary