### Download your illustrations  

 Above on that beautiful button, you are gonna find the image of the stencil. Send this image to your nearest laser cut machine. Ask for a laser cutting on a plywood 3 mm. 

The dimension of the stencil is 150x150 mm. The red lines on the picture mean cutting and the black one means engraving.

Or if you want to try to make it by yourself on cardboard print the image on an A4 paper and stick it to the cardboard.