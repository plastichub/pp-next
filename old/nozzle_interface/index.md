---
image: /_howto/nozzle_interface/IMG_1946.JPG
category: "extrusion"
title: "Nozzle Interface"
tagline: ""
description: ""
usedin:
  - lydia-v
  - lydia
  - zoe
  - injection
  - elena
---





<div class="thumbs"><div class="thumb">
    <a href="./1.JPG" _target="_blank">
        <img id="1.JPG" src="./1.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./2.JPG" _target="_blank">
        <img id="2.JPG" src="./2.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./3.JPG" _target="_blank">
        <img id="3.JPG" src="./3.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./4.JPG" _target="_blank">
        <img id="4.JPG" src="./4.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./5.JPG" _target="_blank">
        <img id="5.JPG" src="./5.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./6.JPG" _target="_blank">
        <img id="6.JPG" src="./6.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./7.JPG" _target="_blank">
        <img id="7.JPG" src="./7.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./8.JPG" _target="_blank">
        <img id="8.JPG" src="./8.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./9.JPG" _target="_blank">
        <img id="9.JPG" src="./9.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1944.JPG" _target="_blank">
        <img id="IMG_1944.JPG" src="./IMG_1944.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1945.JPG" _target="_blank">
        <img id="IMG_1945.JPG" src="./IMG_1945.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1946.JPG" _target="_blank">
        <img id="IMG_1946.JPG" src="./IMG_1946.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

