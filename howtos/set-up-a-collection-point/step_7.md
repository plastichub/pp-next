### Learn the basics of plastic 

 Alright, so you are going to start a collection point! First, it's important to get some knowledge about the plastic topic. Head over to our Academy and dive into the plastic chapters to learn about the different types and properties etc.

As a Collection Point, you want to introduce to people the “refuse, reduce, reuse and then recycle” way of living. By knowing a lot about the plastic topic yourself, you are able to answer their questions and spread the knowledge. 

👉 http://tiny.cc/basics-about-plastic