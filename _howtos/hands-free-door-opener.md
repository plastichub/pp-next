---
image: "/howtos/hands-free-door-opener/door-cover image.jpg"
title: "Hands-Free Door Opener"
tagline: ""
description: "With this How-to you can make your mould to inject a &quot;hands free&quot; door handle - to help stop the spread of COVID-19!"
keywords: "mould,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/alumoulds">alumoulds</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Download and mill the moulds</h4>
<div class="step-text">
  Attached you can find the CNC-files I created based on the 3D files provided by Materialise.<br />
  (https://www.materialise.com/en/hands-free-door-opener/technical-information)<br />
  <br />
  Download the files, CNC cut your aluminium mould and add required screws to close the mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/hands-free-door-opener/mould.jpg">
        <img class="step-image" src="/howtos/hands-free-door-opener/mould.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Inject and assemble</h4>
<div class="step-text">
  Once you injected your parts with your injection machine, you can assemble the product.<br />
  <br />
  You&#39;ll need:<br />
  - The two injected parts<br />
  - 2x M4 screws and Nuts<br />
  - 1x bike tube for in between<br />
  <br />
  You can find more instructions for assembly here:<br />
  <a href="https://www.youtube.com/watch?time_continue=163&amp;v=95aPYlXShTY&amp;feature=emb_logo">https://www.youtube.com/watch?time_continue=163&amp;v=95aPYlXShTY&amp;feature=emb_logo</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/hands-free-door-opener/door-step 2-1.jpg">
        <img class="step-image" src="/howtos/hands-free-door-opener/door-step 2-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/hands-free-door-opener/door-step 2-2.jpg">
        <img class="step-image" src="/howtos/hands-free-door-opener/door-step 2-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/hands-free-door-opener/door-step 2-3.jpg">
        <img class="step-image" src="/howtos/hands-free-door-opener/door-step 2-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Use it!</h4>
<div class="step-text">
  Do less harm, use your arm :)<br />
  <br />
  Oh, and If you prefer to buy the mould, you can order it from me on the bazar:<br />
  <a href="https://bazar.preciousplastic.com/index.php?dispatch=products.view&amp;product_id=281">https://bazar.preciousplastic.com/index.php?dispatch=products.view&amp;product_id=281</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/hands-free-door-opener/door-step 3.jpg">
        <img class="step-image" src="/howtos/hands-free-door-opener/door-step 3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>