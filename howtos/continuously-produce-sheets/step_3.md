### Prepare the mould 

 Place one mould sheet on to the prep table. You can leave the other mould sheet flat on the ground with the side that will contact the plastic facing up. 

Apply a light layer of silicone to the mould sheets. We can highly recommend using a paint roller for this. We keep the paint roller in an airtight box to keep it free of dust. Try and find a sponge roller like in the picture, not one that has hair as they will break off and stick to the mould. 

Place the mould frame in the middle of the mould sheet on the prep table. 