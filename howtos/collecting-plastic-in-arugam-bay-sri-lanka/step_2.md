### Our strategy 

 Our strategy is to mainly collect clean PET bottles from the local population and businesses. The collected PET bottles are sold back to the industry while we keep the PE caps for our own recycling production. Sporadically we also run and assist beach cleanups. 
