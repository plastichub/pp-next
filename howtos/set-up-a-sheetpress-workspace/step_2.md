### Is this for you? 

 For the Sheetpress Workspace you will have to be quite technical as you have to understand how the Sheetpress machine works, ideally, know how to maintain it and, as a plus, know how to fix it when it needs a bit of extra love. 

Attention to details and some creativity is also a nice plus to come up with unique sheets of stunning beauty.
