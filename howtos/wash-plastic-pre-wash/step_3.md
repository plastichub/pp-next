### Build the mobile base 

 To be able to move the barrel (filled with water), we build a solid board with some casters.

Cut wood plates to size, make sure they are strong enough to carry at least 100 kg and attach fitting casters. In the middle we need a hole, so the water outlet has a way to go.