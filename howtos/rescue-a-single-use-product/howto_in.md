### Rescue a single-use product 


Single-use products do still get used a lot. In this how-to, we show you in a few steps on how to collect these products, so you are able to import them into the Precious Plastic Universe. 

It's also a good way to create awareness and promote your Collection Point if you have one. And by being focused on the collection of one certain product you will increase the value of your recycled plastic. 
