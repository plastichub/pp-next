### Front Part + Zipper 

 After choosing the face side of the front part, marking the middle and aligning the center of the zipper with the mark, sew the parts together. It would be helpful if the zipper can be taken apart and easily put back together as it will ease the sewing process.