### Make some products! 

 Now that you have all the things in place it’s time to start making some products with the different machines.

Browse the How-to and Create chapter in the Academy to learn how to make different products and adopt the best practices.