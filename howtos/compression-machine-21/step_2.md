### Add the oven 

 Take your desired oven and fasten it to the machine. If it already works then you could leave it as is, or you can install a PID controller which might give you slightly more control. 
Tip: It’s never a bad idea to add more insulation which will make your machine more energy efficient. 
