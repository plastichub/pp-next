## General

[BOM - Bill of materials](${product_bom})

**Parts to make**

<a href="${product_exploded}">
![](${product_exploded})
</a>

<a href="${production_section_1}">
    ![](${production_section_1})
</a>


### Requirements

- Bigger lathe with adjustable 4 jaw chuck, able to deal with a stock size of 40mm OD and 1 meter length
- Decent MIG Welder
- Drill press and power tools
- Optional & recommended : Mill, for keyway, height gauge, precision caliper

### Notes

- This is a simplified version, using 2 taper and thrust bearings. Please watch the [video](https://www.youtube.com/embed/zWpKdaYInZ0) about the screw components

## Videos

### Screw Components

<iframe width="560" height="315" src="https://www.youtube.com/embed/zWpKdaYInZ0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
