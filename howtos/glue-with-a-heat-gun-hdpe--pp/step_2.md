### Prepare the surfaces 

 It’s known that hot melts don’t offer a very strong bonding, so to maximize the contact area and improve the grip we recommend to scratch the surfaces where the joinery will be made.

Before applying the hot melt make sure to clean the surfaces you are going to glue together with a non-aggressive product like IPA (Isopropyl Alcohol) cleaner and a Scotch Brite or similar to remove any dirt or release agent left from the mould. 
