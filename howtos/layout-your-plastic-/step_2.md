### Choose a good spot 

 Now the size of the layout is clear we can go to find the right spot to place the layout. It’s nice to have enough space around the layout to be able to move and take pictures. It will make sense to keep the layout close to the collection bags, so the visitor can after seeing his plastic, throw it into the bags to be recycled.

Tip: You can also make it to the wall and play with shelves to make it easier to take photos of. It can be done in lots of different ways to be creative and share your results with the community. 
