### Gather lamp components 

 To make this lamp your will need: 

- LED stripe 1m
- metal wires to hang your lamp (In this case 2 per lamp) with their connectors. 
- power controller 
- cable 
- laser cut metal bar to cover LED
- 2 mm bolts 8mm long 
- BEAM
