# Opensource Plastic Recyling Knowledgebase

## Todos

### Common

- [x] Remove all the goofy, cultworshipping and emoji crap
- [x] fix URLS: new forum & library.*, prefix URLS (eg: forum, site, library, media)
- [ ] move all machine docs into machine appendixes
- [ ] fix this goofy frecking specs
- [ ] Version it
- [ ] Download buttons
- [ ] Print layout/css
- [ ] Related mixin

### Backend

- [ ] Add edit button (Jekyll admin)
- [ ] Admin bar: NPM tasks, import/export(forum, PDF)

### Chapters

- [ ] Small scale plastic recycling
- [ ] Plastic / material Development
- [ ] Business opportunities:  research, development & manufacturing, aka ´build´
- [ ] Alternative materials for PP´ers
- [ ] The network


### Related Module

Context document should provide a number of general tags to determine related content types : 

- machines / library
- solutions (external, Wikipedia, ...)
- howtos
- forum posts
- custom web search
- contact/team

NLP/Elasticsearch indexes may unveil a number of new tags for full text which can assist to refine the results


### Tech

- [ ] NodeJS/TS Pre-Processor
- [ ] Jekyll
- [ ] Jekyll - Admin or Github as fallback, basing on ACL
- [ ] Media processors
- [ ] Puppeteer for PDF generation
- [ ] Elasticsearch with NLP/Fulltext extensions


