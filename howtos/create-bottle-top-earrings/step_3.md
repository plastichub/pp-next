### Check on the melting progress 

 Now your tops and tiles are heating in the oven, there is a little time to wait. With our oven we leave the tops for 30 minutes. During this time, you could already prepare another batch of earrings (see step 2). After 10 minutes, check on the melting progress of your bottle tops. 

Some oven strengths vary, so you might need have to adjust the melting temperature or time for the best result. Below, you can see how the bottle tops looked after 10 minutes, and then how the looked when ready for pressing, after 30 minutes in the oven.
