---
image: "/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-1.png"
title: "Glue HDPE & PP with 2-component adhesives"
tagline: ""
description: "2-component adhesives can offer a strong bonding and they are easy to use. On the other hand they are extremely wasteful since they usually come in single-use packaging. <br /><br />We generally don&#39;t recommend glueing parts together as it will make the disassembly harder at the end of the product’s life. So check out other joining techniques first!<br />If this is still needed, here some guidelines."
keywords: "melting,PP,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
- "PP"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Stay safe</h4>
<div class="step-text">
  While handling with 2-component adhesives it’s safe to use an ABEK mask and safety glasses in order to prevent nose, eyes and throat from irritations. Also gloves are needed to avoid any direct contact with the adhesives. Preferably it’s better to work in an open and well ventilated space to avoid gases to accumulate.<br />
  <br />
  Recommended safety equipment:<br />
  - ABEK mask<br />
  - safety glasses<br />
  - gloves
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-2.png">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-2.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Which is the best Adhesive?</h4>
<div class="step-text">
  First off: Using 2-component adhesives is very wasteful and its use should be avoided as much as possible. Please first consider to use screws, snapping unions, or even change the design or the material.<br />
  <br />
  But we want to show which adhesive offers the best performance in case you see no other option.<br />
  <br />
  Among the world of 2-component adhesives there are several types: epoxy, methyl methacrylate, polyurethane... We found that methacrylate adhesives work best for bonding plastic. These can especially be useful when trying to bond PP and HDPE which are generally difficult to bond with other solutions. <br />
  <br />
  Of course the composition of the adhesive will change depending on the brand. Based on our experiences we recommend to use the Permabond Structural Adhesive TA4605.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-3.png">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-3.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Clean surface</h4>
<div class="step-text">
  Before applying the adhesive make sure to clean the surfaces with a non-aggressive product like IPA (Isopropyl Alcohol) cleaner and a Scotch Brite or similar to remove any dirt or release agent left from the mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-4.png">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-4.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-5.png">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-5.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Apply the adhesive</h4>
<div class="step-text">
  First we have to make the mixture. Usually this adhesive comes with a single-use mixer nozzle, but it can also be made by hand, with a flat stick.<br />
  <br />
  Once the mixture is homogeneous we can apply that to the surface we previously cleaned and place carefully the other piece on top. The advantage of this adhesive is that you have time to move your pieces to their precise position, since it takes 5-10 min to start working.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-11.jpg">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-11.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-5.png">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-5.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Apply pressure </h4>
<div class="step-text">
  Once the piece is in the right position, we have to apply pressure (preferably with a clamp) and leave it 24h till it’s fully curated (at 23ºC). If the temperature is lower it will take longer to curate, with each -8ºC the curating time doubles.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-8.png">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-8.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-9.png">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-9.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Recommended applications</h4>
<div class="step-text">
  This method is quite specific and expensive to get. We suggest to only use this adhesive when a really strong bonding is needed between same materials like PP or HDPE. <br />
  <br />
  Oh, and better try to avoid mixing materials with this or other permanent solutions to keep your materials as recyclable as possible.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-7.png">
        <img class="step-image" src="/howtos/glue-hdpe--pp-with-2-component-adhesives/how-to-glue-2-7.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>