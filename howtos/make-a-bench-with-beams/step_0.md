### Get ready 

 Okay, before jumping into the building process make sure you're ready to make this How-to.
As the bench is made with extruded beams, you’ll need to make or find an extrusion machine to produce the beams, or otherwise commission a workspace to make them for you :) For the rest you will need access to a wood and metal workshop.

To make the beams you will need a 80x80x3mm square tube for the mould and around 10kg plastic..

Other  materials needed for the bench:
- 6mm threaded rod
- 10mm Barrel nuts
- Wood
- 4x70mm wood screws
- Tamper-proof nuts (optional)
