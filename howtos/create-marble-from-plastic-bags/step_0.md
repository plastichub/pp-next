### Gather everything you need 

 Before you start get all your gear ready:
- Respirator mask
- Gloves
- Scalpello
- Lots of plastic PE bags from shopping or supermarkets
- Respirator mask
- Shredder machine or scissors
- Basic mould
- Scrap metal bowl
- Compression machine
- Sanding machine
- Planner (if available)
- Plastic Type Stamp


