### The washing bag 2 

 Our bag is designed so you can just hang it straight beneath your shredder and put it in the machine afterward. The zipper is zipped up into a little pocket so it doesn’t open up while washing.
