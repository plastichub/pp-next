### Sew the side seams 

 Fold your current construction so that the cotton side is exposed, and sew along the side seams of the raincoat; remembering to use the 2cm that you added as a sewing excess. 