### Add plastic identifier 

 Noting what plastic the rulers you are making will be made from is important because then the person in the future who is recycling it will know what temperatures to use. These can be found in the download kit for Precious Plastic or you can download them from the files attached to this How-to.

-Import the symbol you need in the same way you done with the ruler files
-It will then give you some import settings
-We selected "rough" precision to keep the file size down and because we know that the detail will be sufficient for laser cutting
-Now align it and place it where you want it to be

