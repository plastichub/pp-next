### Troubleshooting 

 Injection moulding is a complicated process where many different process parameters have to be dialed in to the right settings. We keep a Troubleshooting guide updated ( REFERENCE ) to help you if something goes wrong. 