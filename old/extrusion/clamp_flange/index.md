---
image: /_howto/extrusion/clamp_flange/IMG_1962.JPG
category: "extrusion"
title: "Clamp Flange"
tagline: ""
description: ""
usedin:
  - zoe
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_1954.JPG" _target="_blank">
        <img id="IMG_1954.JPG" src="./IMG_1954.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1955.JPG" _target="_blank">
        <img id="IMG_1955.JPG" src="./IMG_1955.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1956.JPG" _target="_blank">
        <img id="IMG_1956.JPG" src="./IMG_1956.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1957.JPG" _target="_blank">
        <img id="IMG_1957.JPG" src="./IMG_1957.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1958.JPG" _target="_blank">
        <img id="IMG_1958.JPG" src="./IMG_1958.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1960.JPG" _target="_blank">
        <img id="IMG_1960.JPG" src="./IMG_1960.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1962.JPG" _target="_blank">
        <img id="IMG_1962.JPG" src="./IMG_1962.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

