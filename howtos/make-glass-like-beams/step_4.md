### Weigh your quantities 

 Controlling your quantities is important to get your desired colour and effect. You can weigh a piece of a previously made beam to get an idea of the required total weight.
Weighing out the quantities of your materials into separate containers will enable you to have more control of your result.

In the next steps we will show the mixtures and results of two processes:
Mixing clear PS with pure pigment (Step 7)
Mixing clear PS with a PS-pigment-mix (Step 8)