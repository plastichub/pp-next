### Colors don't mix up 

 In case you look for specific target colors you have to consider some parameters on colormixing plastics.

Solutions (specifc for each case)
- To add a second color to a white material start with around 2-5% of color to 95-98% white material
- To add a second color to a black/colored material start with around 30% of color to 70% black/colored material
- To achieve solid colors you may need to "masterbatch" your material. This means you mix the different colors with an extrusion machine prior to the processing with the injection machine.
- If you use special color to colorize your material masterbatching is crucial. Try to Divide the color pellets to a smaller size.