### Welding the humps to the base 

 Now place the humps on top of the base frame and clamp them tight together.

First weld around the outside, then turn the mould over and weld the point where the two humps meet to avoid plastic leaking in the future.

Finish by grinding and sanding the welds again for a smooth surface and round edges. 
