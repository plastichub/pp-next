### Make a lamp with beams 


Ambient light made with the technique of extruded beam. Embracing the qualities of translucent polystyrene, giving what was once old CD cases a new life. 

To make this lamp you will need a groove in your beam to place the LED stripe inside.

This How-to will explain: 
Step 1-10: How to make two parts extrusion mould
Step 11-17: How to assemble the lamp  
