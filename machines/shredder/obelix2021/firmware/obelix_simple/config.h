#ifndef CONFIG_H
#define CONFIG_H

#include "enums.h"
#include "common/macros.h"
#include <Controllino.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Core settings
//

#define LOOP_DELAY 100  // Our frame time, exluding delays in some places
#define BOOT_DELAY 400 // Wait at least this amount in ms after boot before doing anything

// Please consider to set this to false for production - especially with the full feature set since this is requiring extra
// time for the serial communication and will affect the overall framerate/performance
// #define DEBUG true

#define DEBUG_INTERVAL 500
#define DEBUG_BAUD_RATE 19200 // Serial port speed

#define RELAY_ON 0    // The relay bank's on value (eg: normally closed)
#define RELAY_OFF 255 // The relay bank's off value (eg: normally closed)

#define DIR_SWITCH_UP_PIN CONTROLLINO_A1   // The 3 position's up output
#define DIR_SWITCH_DOWN_PIN CONTROLLINO_A0 // The 3 position's down output

// #define IGNORE_FIRST_DIRECTION              // Uncomment to ignore the 3pos switch (forward/reverse) after booting. This prevents surprises but possibly also accidents.
//#define DIR_SWITCH_DELAY        500      // If defined, add this as blocking delay between direction changes. Needed for some types of relays.

// Interval to read analog inputs
#define ANALOG_READ_INTERVAL 200

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Machine settings
//

// #define USE_MEGA                               // On Arduino Uno we have only limited ports which are not enough to enable all features.
#define USE_CONTROLLINO
#define HAS_AUTO_REVERSE

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    VFD related
//

// #define HAS_MODBUS_BRIDGE

// VFD FWD Pin. This goes into the relay and then into the VFD.
#define FWD_PIN CONTROLLINO_D5

// VFD REV Pin. This goes into the relay and then into the VFD.
#define REV_PIN CONTROLLINO_D4

// Delay between direction switching. Important in case the motor is controlled via delays. This prevents a short circuit.
#define DIR_SWITCH_DELAY 300

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Motor related
//

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Auto-Reverse
//

// When shredding, stop all after this time. Uncomment to activate.
// This will be only used when AUTO_REVERSE_MODE is not AR_MODE::EXTRUSION.
// That means of course that shredding will always stop after one hour.
// I considered this a safety feature and will be of importance when the operator is absent unexpectly.
// @TODO: in case there is a VFD speed sensor, or a hall sensor : augment mean deltas to determine 'activity' which
// will then extend the time. It may make sense to apply this feature also for extruding then.
#define MAX_SHRED_TIME MIN_MS
#define MAX_IDLE_TIME SECS * 20

#define MAX_REVERSE_TRIALS 3 // Max. trials to reverse before aborting (goes in FATAL state, unlock via 3pos switch = 0 or reset button)

// The pin for the current sensor
#define CURRENT_SENSOR_PIN CONTROLLINO_IN0

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    General switches

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Feedback

// Status LEDS (ok, error) - uncomment to activate
#define HAS_STATUS
#define STATUS_OK_PIN CONTROLLINO_D2
#define STATUS_ERROR_PIN CONTROLLINO_D3

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    externals
//

// pull in internal constants
#include "constants.h"

// pull in internal configs
#include "config_adv.h"

// The user_config.h is initially added to the github repository but changes will be ignored via .gitignore. Please keep this file safe and possibly
// on a per tenant base stored. You can override parameters in this file by using #undef SOME_PARAMETER and then re-define again if needed, otherwise disable
// default features by using #undef FEATURE_OR_PARAMETER.
// This presents the possibilty to play with the code whilst staying in the loop with latest updates.
#include "user_config.h"

// At last we check all configs and spit compiler errors
#include "config_validator.h"

#endif
