---
image: "/howtos/make-a-lamp-with-beams/_DSF9396.JPG"
title: "Make a lamp with beams"
tagline: ""
description: "Ambient light made with the technique of extruded beam. Embracing the qualities of translucent polystyrene, giving what was once old CD cases a new life. <br /><br />To make this lamp you will need a groove in your beam to place the LED stripe inside.<br /><br />This How-to will explain: <br />Step 1-10: How to make two parts extrusion mould<br />Step 11-17: How to assemble the lamp"
keywords: "extrusion,product,PS"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "product"
- "PS"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready </h4>
<div class="step-text">
  Check out the attached material list and get your materials ready for the mould and making the lights.<br />
  <br />
  Equipment:<br />
  - Metalworking tools/workspace<br />
  - Extrusion machine<br />
  - shredded PS<br />
  <br />
  A quick tip: make sure you make your mold longer than your lamp length! The extreme ends are always on a low quality so it’s easier to cut them off. I would say at least 5 cm longer on each side.<br />
  <br />
  <br />
  Related links:<br />
  Extrude beams 👉 tiny.cc/extrude-beams<br />
  Make glass-like beams 👉 tiny.cc/make-glasslike-beams
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/1.1-materials.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/1.1-materials.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/1-materials.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/1-materials.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Choose your design </h4>
<div class="step-text">
  In our case, we wanted to obtain an ambient light.<br />
  <br />
  After making a few tests we decided to cut the oval beam we found in the scrapyard to obtain a smaller profile and so allow the light to spread more equally. <br />
  <br />
  Mark the height you want with a metal marking tool, then you can cut the beam with an angle grinder. Make sure to be accurate.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/2- mark the beam.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/2- mark the beam.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/2.3 cut.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/2.3 cut.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/2.2 cut the beam.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/2.2 cut the beam.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare central bar</h4>
<div class="step-text">
  Cut the central metal bar to the right length.<br />
  <br />
  Mark the centerline on the top of the bar and colour the sides, to help you send the right angle.<br />
  <br />
  Make the 2-degree draft with a belt sander after setting the table with the right angle. Once you can’t see the color anymore means you sanded the entire surface. <br />
  <br />
  This draft is really important! It will help you extracting your beam from the mould and preventing from cracking your piece as in this case we are working with polystyrene, which has the minimum shrinkage (0.4/0.7%) and it becomes quite difficult to extract from the mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/3.1 cut the bar.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/3.1 cut the bar.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/3.3 draft__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/3.3 draft__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/3.2 mark center line.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/3.2 mark center line.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Cut the rest of the material</h4>
<div class="step-text">
  Grab the 2 metal bars and cut them as long as the first piece. <br />
  <br />
  Cut the sheet to the right length and width so it fits the u-profile and the bars on top of it.<br />
  <br />
  Now cut the mounting brackets that will connect the mould to the nozzle.<br />
  <br />
  Related link:<br />
  Extrude beams 👉 tiny.cc/extrude-beams
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/4.1 cut the sheet.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/4.1 cut the sheet.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/4.3 cut mounting brackets.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/4.3 cut mounting brackets.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Drill the metal bar</h4>
<div class="step-text">
  Now that we have all the materials cut it’s time to start building our first part mould!<br />
  <br />
  Mark where you will drill the holes along the central bar, place it at the center of your sheet and tap weld the edges so it won’t move. <br />
  <br />
  Clamp the pieces in the device so they are ready to drill. Placing a wooden block under the sheet will help to block the pieces while drilling.<br />
  <br />
  Drill 3,3 mm holes along the bar (one each 100mm should be enough). Now you can remove the spot weld from the bar and enlarge the 3,3mm holes to 4mm holes on the sheet only. Finally, thread the holes on the bar to M4.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/5.1 mark central bar.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/5.1 mark central bar.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/5.3 tap weld.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/5.3 tap weld.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/5.2 drill and tap__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/5.2 drill and tap__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Fix the bar to the sheet</h4>
<div class="step-text">
  Clean and sand the bottom of the bar so it’s ready to insert the 4mm bolts to fix the metal bar on the sheet. Once it’s fixed you don’t have to remove it anymore. You will need the bolts to stick out of the bar so you can grind them off and make the top surface really flat and polish as this will show on your beam.<br />
  <br />
  This metal bar will define the groove where you can insert the LED stripe plus it will prevent a big amount of manual work! You could also mill it by hand but it takes a long time and creates lots of microplastic!<br />
  <br />
  To prevent the plastic to flow inside the bar you could place silicon on the side.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/6 grind off the bolts.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/6 grind off the bolts.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/6 sand and polish__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/6 sand and polish__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Weld the metal bars to the u profile</h4>
<div class="step-text">
  The order in which you make this mold is quite important as having the sheet ready will help you welding the bars and the U profile in the right place. <br />
  <br />
  Set the components on the welding table, clamp the 2 metal bars on the top of the sheet, (the bars will lock the beam in the middle) make sure to clamp everything properly.<br />
  <br />
  Firstly, tap weld the bars to the sheet (this will help you block the pieces), then you can start welding the bars along with the U profile stay, 50mm alternating each side, doesn’t need to be welded all length.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/7.1  tap weld__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/7.1  tap weld__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/7.2 weld along__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/7.2 weld along__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Drilling holes</h4>
<div class="step-text">
  Mark where you want the holes with a center pin along with the sidebars. <br />
  <br />
  Before removing the tap welds, we are gonna drill 8mm holes along with the sidebars and the sheet to fit 8mm bolts. <br />
  It’s important to keep everything clamped together so we are sure the holes will match after disassembling and reassemble the 2 parts molds!<br />
  <br />
  Now you can grind off the taps weld and take the 2 parts mould apart.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/8.1 mark center of bars to drill.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/8.1 mark center of bars to drill.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/8.3 grind off tap welds.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/8.3 grind off tap welds.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/8.2 drill 8mm holes.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/8.2 drill 8mm holes.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Welding mountain brackets</h4>
<div class="step-text">
  As the next step, we are going to weld the mounting brackets to both sides of the mold. Before doing that make sure the brackets match with your plate and the center of your beam matches the center of the plate. <br />
  <br />
  You can find all the information you need to make a plate here:<br />
  <br />
  Related link:<br />
  Extruded beams 👉 tiny.cc/extrude-beams<br />
  <br />
  Let’s proceed: Drill the holes on the brackets before welding. <br />
  We recommend to weld along the entire sheet&#39;s surface. The first welds were too small and the plastic flew out of the mold, so as pictures show, we had to weld another piece on top of the previous ones.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/9.1 brackets KIT__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/9.1 brackets KIT__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/9.2 plate welded.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/9.2 plate welded.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/9.3 weld brackets.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/9.3 weld brackets.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Finishing up</h4>
<div class="step-text">
  You made it! Before extruding make sure you clean both parts of the mold and put silicon oil to facilitate the extraction of the beam. <br />
  Assemble the 2 parts mould with nuts and bolts and connect your mold to the nozzle.<br />
  <br />
  For better finishing, we recommend preheating your mould with heating elements or in the oven, or both! <br />
  <br />
  Now it’s time to extrude and test your mould! <br />
  <br />
  Relevant links:<br />
  Make glass-like beams 👉 tiny.cc/make-glasslike-beams
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/10.3 extrude.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/10.3 extrude.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/10.1 assemble.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/10.1 assemble.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/10. sanding.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/10. sanding.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Gather lamp components</h4>
<div class="step-text">
  To make this lamp your will need: <br />
  <br />
  - LED stripe 1m<br />
  - metal wires to hang your lamp (In this case 2 per lamp) with their connectors. <br />
  - power controller <br />
  - cable <br />
  - laser cut metal bar to cover LED<br />
  - 2 mm bolts 8mm long <br />
  - BEAM
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/11. components__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/11. components__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Metal bar</h4>
<div class="step-text">
  To cover the LED stripe and hang the beam from the ceiling, we designed a metal bar that holds the pieces together. <br />
  We made drawings for lasercutting, for a better finish.<br />
  <br />
  You will need to engrave the central lines to stick the LED stripe exactly in the middle, the sidelines to facilitate the bending, cut 5mm holes on each side where you will let the light cable pass through and cut 2.5mm grove which will allow to slot the metal cable through.<br />
  <br />
  If you lasercut we recommend to send more than one piece as the unit price will get cheaper. <br />
  You can find the CAD file attached above.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/12 laser cut part beam light Drawing.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/12 laser cut part beam light Drawing.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/12.1  laser cut bars with engrave lines.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/12.1  laser cut bars with engrave lines.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Cut your beam</h4>
<div class="step-text">
  After extracting your beam from the mold cut the right length that will match your metal plate.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/13.1 extract beam.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/13.1 extract beam.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/13.2 sign the lengh.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/13.2 sign the lengh.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/13.3 cut beam to the right length.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/13.3 cut beam to the right length.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Bend the plate</h4>
<div class="step-text">
  Place the plate in the metal device along the line previously engraved and bend it with a hammer.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/14.1 allign bending line with the device.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/14.1 allign bending line with the device.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/14.3  detail bending.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/14.3  detail bending.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/14.2 bend the plate.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/14.2 bend the plate.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Prepare the beam and the bar</h4>
<div class="step-text">
  Before assembling everything together with 3mm bolts you will need to drill 3mm holes on both sides of the beam matching with the bar’s holes previously laser cut. You do this by clamping the beam together with the metal plate already bent.<br />
  <br />
  After that take them apart and thread 3mm holes on the bar to fit the bolts. Finish off the beam, sand the edges and the sides so now you can insert the 4mm bolts on the sides to check if they match.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/15.1 finishing off the beam.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/15.1 finishing off the beam.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/15.3 thread bar.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/15.3 thread bar.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/15.2 prepare the beam.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/15.2 prepare the beam.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Assemble</h4>
<div class="step-text">
  Once the 2 components are ready you can stick the LED to the metal bar, letting the metal cable first, and then the light cable passing through the holes. <br />
  <br />
  Assemble the plate and the beam with the 3mm bolts and put a 5mm screw on the side with no light cable. This will prevent the metal cable from escaping.<br />
  <br />
  Now solder the LED cable to the power controller!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/16 led cable through__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/16 led cable through__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/16.2 soldering.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/16.2 soldering.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/16.3 assemble.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/16.3 assemble.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Play with the colours :)</h4>
<div class="step-text">
  You&#39;re done!<br />
  <br />
  Depending on your LED strip you can now program your light, intensity, color, etc. with Arduino. <br />
  <br />
  A guide explaining how to program the board can be found here: <a href="http://bit.ly/2QQHZV9<br/>">http://bit.ly/2QQHZV9<br /></a>
  You can find the relevant Arduino code in the download files.<br />
  <br />
  Once your board is programmed then you can change the LED settings from your phone connecting to the board’s wifi. <br />
  <br />
  Well done you made it!<br />
  Plugin your light and hang it to your ceiling!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/17.2 light controll.JPG">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/17.2 light controll.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lamp-with-beams/17.1 light settings__.jpg">
        <img class="step-image" src="/howtos/make-a-lamp-with-beams/17.1 light settings__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>