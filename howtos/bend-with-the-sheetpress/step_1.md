### Temperature and timing 

 The next step is to decide on temperature and timing. 

The right temperature depends on the plastic type and thickness. You can check in between to see if it bends far enough or experiment with smaller pieces before starting with bigger sheets.

Below are some of the settings we have worked with.

