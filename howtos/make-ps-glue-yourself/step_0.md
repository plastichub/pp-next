###  Get your materials 

 For this process we will need the following materials and equipment.

Materials needed to make the glue:			
- D-limone					
- EPS (Styrofoam)				
- glass jar with lid	

Materials/tools needed for application:
- IPA (Isopropyl Alcohol) cleaner
- Scotch Brite (or similar)
- stick or brush to spread
- clamp to press together
- stick to stir

Recommended safety equipment:
- ABEK mask
- gloves
- safety glasses