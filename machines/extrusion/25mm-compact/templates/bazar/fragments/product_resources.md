**Product Resources**

- [3D Preview CAD Model ](${product_3d})
- [Wiki](${product_wiki})
- [Forum](${product_forum})
- [Discord Chat](${product_chat})
- [Electrical wiring](${product_wiring})
