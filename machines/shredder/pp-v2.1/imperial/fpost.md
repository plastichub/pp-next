Hello everyone 

After several days learning CAD and some 3D modeling tools, I have finally finished my conversion of the Precious Plastic Shredder to imperial units.

This is a request that tons of people have been making for months and there are a few machine builders who have managed to convert the drawings to inches, but for some reason no one else has shared the converted files with the community (something I consider very selfish).

These files have been checked from every single angle to make sure they will fit correctly. While these have not been used to build a real shredder yet for their final test, I will send them myself to a laser cutting company soon and will post the results here as soon as possible.

Without further ado, here are the technical details:

**Steel equivalents**  
3mm = 1/8″ = 3.175mm  
5mm = 3/16″ = 4.7625mm  
6mm = 1/4″ = 6.35mm

**Original metric design (inside lenght)**

14 x 5mm rotating knives = 70mm (BOM shows 15 knives but only 14 are used in the shredder)  
13 x 6mm spacers = 78mm  
70mm + 78mm = 148mm total hex length (plus 85mm on each side of the hex bar)

**New imperial design (inside lenght)**

14 x 3/16″ rotating knives = 66.675mm  
13 x 1/4″ spacers = 82.55mm  
66.675mm + 82.55mm = 149.225mm total hex width (plus 85mm on each side of the hex bar)

**Parts that had to be resized:**

***Hex bar**  
*The original Hex bar had a total length of 318mm, with a central hex length of 148mm and 20mm diameter round ends with a length of 85mm each end.  
*The imperial Hex bar has a total length of 319.225mm, with a central hex length of 149.225 and 20mm or 3/4″ diameter ends (you chose) with a length of 85mm each end. You can round this number to 320mm, or just do what I did and buy 1ft of hex bar, since you only need one end of the bar to hook up the motor, there’s no need to have 85mm of round bar on each side. Buying 1 ft of bar [is normally cheaper](https://www.onlinemetals.com/merchant.cfm?pid=19776&step=4&showunits=inches&id=1341&top_cat=197) than getting a custom cut. With a 1ft bar (304.8mm), you will end up with 85mm on one end of the bar, 149.225 in the center and the remaining end of the bar will be 70,575mm.  
***Side panels** (front, back)  
*The front and back panels remains the same except for the slots where the middle panels are connected.  
*The 1/4″ “side” (front, back) panels have 2 slots on each side, and those slots were originally 3.2mm wide to allow the 3mm middle panels to fit in and have some clearance. In inches, those middle panels are 1/8″ thick (3.175mm) and I made the slots 3.3mm (0.130″) wide to allow for 0.125mm clearance  
***Middle panels**  
*The middle panel needed to be extended to fit the increase of length. The original panel was 149mm wide, the new panel is 150,8125mm wide (5.9375″).  
*Also, the holes at both sides of the panel had to be enlarged, from 3mm to 1/8″  
*The side inserts that connect the middle panels with the side (front/back) panels had to be enlarged a little bit, from 6mm to 3/16″  
*Finally, all the slots (3 from the top and 5 on the bottom) where resized, repositioned and aligned to adjust to the new panel dimensions  
***Side 1**  
*Adapted top and lateral inserts for 1/8″ thickness  
***Side 2**  
*Adapted top and lateral inserts for 1/8″ thickness  
***Bottom 1**  
*Resized from 149mm to 150,8125mm wide (5.9375″)  
*All slots resized and repositioned proportionally to the new part length  
***Bottom 2**  
*Resized from 149mm to 150,8125mm wide (5.9375″)  
*All slots resized and repositioned proportionally to the new part length  
***Top 1**  
*Length increased to 5.9375″ and all slot dimensions resized to fit into the middle panel slots  
***Top 2**  
*Length increased to 5.9375″ and all slot dimensions resized to fit into the middle panel slots

*****************************************************

***Additional notes**

***IMPORTANT:** For the imperial version of the shredder I decided to use the 1″ hex bar files modified by [@keesdeligt](https://davehakkens.nl/community/members/keesdeligt/), since [it’s much easier/cheaper](https://www.onlinemetals.com/merchant.cfm?pid=19776&step=4&showunits=inches&id=1341&top_cat=197) to get 1″ hex bar than 27mm. Also, make sure to read the changelog at the bottom of this post, it shows important information about the hex bar and bearing spacers.  
*Original BOM shows that for the part “Fixed Knife Small” 15 pieces are needed, but actually only 14 pieces are needed  
*Original BOM shows that for the part “Knife3″ 5 pieces are needed, but actually only 4 pieces are needed. The total amount of knives used in the shredder is 14 not 15.  
*This redesign of the shredder was made for black steel which is available in 1/8″, 3/16″ and 1/4”. It is my understanding that Inox Steel is available in different gauges. If you are interested on an imperial design of the shredder using inox steel gauges, just contact me and I might design a new set of files for inox steel gauges. I have zero knowledge on whats available for inox steel so you will have to tell me.  
*This shredder design is based on the 2.0 version of the shredder, however, all the changes from 2.1/3.0 are fully compatible (new hopper and new sieve design)

Soon I will be sharing as well a new set of files in Imperial units for a Shredder twice as big, something like [THIS](https://davehakkens.nl/community/forums/topic/big-shredder/)

[DOWNLOAD](https://1drv.ms/u/s!Asv5yaQakrNShspMOPurA2H4nO6ZFg)

<span class="embed-youtube" style="text-align:center; display: block;"><iframe class="youtube-player" type="text/html" width="640" height="360" src="https://www.youtube.com/embed/hpG7XU5R7FA?version=3&amp;rel=1&amp;fs=1&amp;autohide=2&amp;showsearch=0&amp;showinfo=1&amp;iv_load_policy=1&amp;wmode=transparent" allowfullscreen="true" style="border:0;"></iframe></span>

Changelog:

4/Feb/2018  
*Fixed minor sizing issues with frame parts  
*Appart from the .iam assembly file, I also included the assembly in .stp and .igs format  
*Added pdf files with hex bar measurements, one for a 1″ hex bar with 3/4″ bores for 3/4″ bearings, and another for a 1″ hex bar with 20mm bores (original).  
*While it is true that this design is made for Imperial units, there might be some people who already purchased UCFL204 bearings with 20mm bores, and that’s the reason why both hex bar designs were included.  
*Added 2 additional files for the 1/8″ and 1/4″ bearing spacers for 3/4″ bearings in the DXF folder.  
*If you are planning to use 3/4″ UCFL204 bearings then you will need to lasercut the files “1-8inch 2x BearingSpacer for 3-4inch Bearing.dxf” and “1-4inch 2x Bearing Spacer for 3-4inch Bearing.dxf”, and delete the files “1-8inch 2x BearingSpacer for 20mm Bearing.dxf” and “1-4inch 2x Bearing Spacer for 20mm Bearing.dxf”  
*If you are planning to use 20mm UCFL204 bearings then you will need to lasercut the files “1-8inch 2x BearingSpacer for 20mm Bearing.dxf” and “1-4inch 2x Bearing Spacer for 20mm Bearing.dxf”, and delete the files “1-8inch 2x BearingSpacer for 3-4inch Bearing.dxf” and “1-4inch 2x Bearing Spacer for 3-4inch Bearing.dxf”  
*Added 2 additional files for the 1/4″ bearing spacers for 3/4″ bores in the DXF folder.  
*In case anyone is interested, found this deal on UFCL204 3/4″ bearings [https://goo.gl/7Bg2WP](https://goo.gl/7Bg2WP)

7/Feb/2018  
*Fixed 0.007″ offset on “Bottom2” and “Top2” parts, updated download package with new dfx, ipt, igs and stp files

9/Feb/2018  
*I was told by my waterjet cutting company that they had problems opening the DXF files because they were in 2018 format and they only supported AutoCad 2013, so I added an separate folder into the download package with AutoCad 2013 format files and kept the 2018 files as well because why not…

23/Feb/2018  
*Finally received the water cut pieces and after a visual inspection and a few measurements with my caliper they look perfect. I’ll upload some photos soon and will report back once I have assembled the shredder

02/Mar/2018  
*I finished sanding and grinding all the parts and also milled my jimmy-hex-bar (more info [HERE](https://davehakkens.nl/community/forums/topic/cheap-source-for-hex-bars/)). So far everything looks perfect and all the pieces fit as expected, you can see some pictures at the botom of this thread [HERE](https://davehakkens.nl/community/forums/topic/shredder-converted-to-imperial-system-inches/#post-118409). I’ll upload more content in the next 1 or 2 days once I finish the shredder and it’s fully assembled.

16/Mar/2018  
*Habemus shredder 