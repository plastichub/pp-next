### Trouble shooting injection moulding 


Here I try to to cover all issues that may appear while you are trying to make parts with the process of injection moulding.

Some issues might appear but not relevant for you to fix - so see it more as an optional guide where you pick the problems you want to solve. Some "problems" can also widen the artistic freedom to play :)

The solutions are ordered in a recommended order!