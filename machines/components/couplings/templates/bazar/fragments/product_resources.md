**Product Resources**

- [3D Preview](${product_3d})
- [Drawings](${product_drawings})
- [CAD model](${product_cad})
- [Wiki](${product_wiki})
- [Source files](${product_github})
- [Discord Chat](${product_chat})
- [Firmware - New](${product_firmware})
- [Firmware - Old](${product_firmware_old})
- [Howtos - Machine builder](${product_howtos})

<hr/>

- [Product Assembly](${product_assembly})
