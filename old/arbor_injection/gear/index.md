---
image: /_howto/arbor_injection/gear/IMG_2087.JPG
category: "arborinjection"
title: "Gear"
tagline: ""
description: ""
usedin:
  - elena
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2076.JPG" _target="_blank">
        <img id="IMG_2076.JPG" src="./IMG_2076.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2077.JPG" _target="_blank">
        <img id="IMG_2077.JPG" src="./IMG_2077.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2078.JPG" _target="_blank">
        <img id="IMG_2078.JPG" src="./IMG_2078.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2079.JPG" _target="_blank">
        <img id="IMG_2079.JPG" src="./IMG_2079.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2080.JPG" _target="_blank">
        <img id="IMG_2080.JPG" src="./IMG_2080.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2081.JPG" _target="_blank">
        <img id="IMG_2081.JPG" src="./IMG_2081.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2082.JPG" _target="_blank">
        <img id="IMG_2082.JPG" src="./IMG_2082.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2083.JPG" _target="_blank">
        <img id="IMG_2083.JPG" src="./IMG_2083.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2084.JPG" _target="_blank">
        <img id="IMG_2084.JPG" src="./IMG_2084.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2085.JPG" _target="_blank">
        <img id="IMG_2085.JPG" src="./IMG_2085.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2087.JPG" _target="_blank">
        <img id="IMG_2087.JPG" src="./IMG_2087.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

