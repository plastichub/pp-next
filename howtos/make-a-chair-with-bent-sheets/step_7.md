### Cut your plastic  

 Finally we can start making our chair! 
Begin with cutting the 14mm PS sheets for your seat and backrest to the dimensions in the part list. For a first rough shape, transfer the shape of the wooden jig to the plastic sheet with a thick marker and cut along that line with the jigsaw (more info about this process in the How to cut plastic: jigsaw 👉 tiny.cc/cut-with-jigsaw.

❗️In this process, you’ll probably create a lot of plastic dust. Please be conscious and try to save it from going into the environment.