### Finishing up 

 After the fitting and welding its time to finish up. The last step in the process is to make sure yout plastic flows smoothly out your machine into the mould, and most importantly, be easily removed! For this we need to remove all 'undercuts' by drilling two holes into the plate. The first hole will be the same size as the inside of the nozzle piece. 

This should yeild one straight hole all the way through.. but if thats not enough, or you still find there are some undercuts, you can use a larger bit to open the plate more on the beam side.