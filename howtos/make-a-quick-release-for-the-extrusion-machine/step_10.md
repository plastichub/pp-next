### Smoothing the sliding mechanism 

 Almost ready! Now the only thing remaining is to ensure that the mechanism slots together with ease. You will no longer be able to adjust the nozzle portion, however, the sliding part can be sanded, ground, and polished to ensure a smooth surface is achieved. 

The result should be a mechanism that slides into and out of the nozzle with ease but should not be loose. Both plates need to pressed firmly together for the system to work. Test out where the plate gets a bit stuck and then adjust the sides by grinding, sanding and polishing them.
