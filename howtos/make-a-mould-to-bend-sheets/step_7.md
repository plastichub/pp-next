### Making the positive mould: screws 

 Mark the centerline on the sheet metal, align it to the frame and attach it with screws. Then bend it downwards around the curvature and fix it every once in a while. 