<p>This is our best seller. It comes with auto-reverse and lots of safety functions.</p>

<div style="clear:both">
    ${mini_specs}
</div>


<div style="clear:both">
    ${product_overview_drawings}
</div>


### Features

- Safe hopper – with optional door.
- Precise auto-reverse when jamming.
- Status lights : Ok & Error
- Emergency and reset button
- Various motor protections (thermal & magnetic).
- Jam detection for extrusion mode
- Record and replay extrusion times
- Rigid, flexible and modular framework – enabling hacking and extensions. The base plate is 15 mm thick.
- The legs have M10 threads on each end so you can connect levelers or wheels. The legs have also extra holes to install easily extra sheets all around.
- Extrusion hopper can be easily detached.
- Thermocouples (sensors) have clips, easy to detach.
- Stops automatically after one hour of inactivity.
- Quality cables and wiring according to industrial standards.
- Precision mounts for shredder & extrusion, making sure everything is aligned perfectly.
- Strong 32 mm hexbar and 35 mm driveshaft
- Compression screw
- Industrial controller

<hr/>

<div>    
    <div style="padding: 16px; display: inline-block">
        ${product_resources}
    </div>
</div>
