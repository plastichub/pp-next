### Explore the possibilities! 

 Finally, the mould is ready, well done! Have fun extruding custom shaped and long beams. For best results extrude with a hot mould using heating elements or an oven. This technique of mould building opens up a door for many more custom shapes and sizes with regular fabrication.

Related links:
How to extrude different textures 👉 tiny.cc/extrude-textures
How to build a shelving system with T beams 👉 tiny.cc/make-shelving-system

Have fun exploring!