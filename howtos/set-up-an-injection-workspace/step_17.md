### Get your precious plastic 

 Now you need to get some plastic waste to be recycled. 

You can make contact with a Collection Point or find clever ways to get people to bring you plastic. The volumes you will need are not huge so a small collection setup in your workspace can work.

If you want to dive more into ways to collect plastic,