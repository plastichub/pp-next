### The gear 

 The connection between motor and shaft is very simple: Two same sized gears from multiplex - one fixed to the motor, the other one on the shaft which can be lifted up.

The connection to the shaft is made in a very simple way, as you can see on the pictures, but feel free to use a more conventional method. 