### Reduce micro-plastic & dust when shredding 


Minimise micro-plastic and dust with this simple shredder upgrade! This simple hack also helps when it comes time to clean your shredder for colour or material changes. 

You'll need the following components for this project:
• Vacuum
• 20L Bucket
• Cyclone Dust Collector
• Funnel
