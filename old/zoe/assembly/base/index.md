---
image: /_howto/zoe/assembly/base/IMG_2202.JPG
category: "zoe"
title: "Base"
tagline: ""
description: ""
tags :
 - v3
 - zoe
categories:
  - Shredder
usedin:
  - zoe
  - shredder_v31
---





<div class="thumbs"><div class="thumb">
    <a href="./1.JPG" _target="_blank">
        <img id="1.JPG" src="./1.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2186.JPG" _target="_blank">
        <img id="IMG_2186.JPG" src="./IMG_2186.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2187.JPG" _target="_blank">
        <img id="IMG_2187.JPG" src="./IMG_2187.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2188.JPG" _target="_blank">
        <img id="IMG_2188.JPG" src="./IMG_2188.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2189.JPG" _target="_blank">
        <img id="IMG_2189.JPG" src="./IMG_2189.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2190.JPG" _target="_blank">
        <img id="IMG_2190.JPG" src="./IMG_2190.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2192.JPG" _target="_blank">
        <img id="IMG_2192.JPG" src="./IMG_2192.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2193.JPG" _target="_blank">
        <img id="IMG_2193.JPG" src="./IMG_2193.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2194.JPG" _target="_blank">
        <img id="IMG_2194.JPG" src="./IMG_2194.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2195.JPG" _target="_blank">
        <img id="IMG_2195.JPG" src="./IMG_2195.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2196.JPG" _target="_blank">
        <img id="IMG_2196.JPG" src="./IMG_2196.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2197.JPG" _target="_blank">
        <img id="IMG_2197.JPG" src="./IMG_2197.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2198.JPG" _target="_blank">
        <img id="IMG_2198.JPG" src="./IMG_2198.JPG" width="100%" />
    </a>
    <span class="thumb-label">13. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2202.JPG" _target="_blank">
        <img id="IMG_2202.JPG" src="./IMG_2202.JPG" width="100%" />
    </a>
    <span class="thumb-label">14. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

