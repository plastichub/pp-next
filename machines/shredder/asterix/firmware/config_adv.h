#ifndef CONFIG_ADV_H
#define CONFIG_ADV_H

// enable bridge debugging
//#define BRIDGE_DEBUG

////////////////////////////////////////////////////////////////////////////////
//
//  Auto reverse time settings

// the time used to reverse after jamming
#define AR_REVERSE_TIME 2000

// after reversing came to stop, this is the delay
// before it's forwarding
#define AR_FORWARD_WAIT_TIME 1000

// after the delay AR_FORWARD_WAIT_TIME, we set the VFD to forward
// by this time, motor / VFD specifc
#define AR_FORWARDING_TIME 1500

////////////////////////////////////////////////////////////////////////////////
//
// Power settings

// optional current sensor to validate primary power is there
// #define POWER_CSENSOR_PRIMARY                           CONTROLLINO_A15

// optional current sensor to validate primary power is there
#define POWER_CSENSOR_SECONDARY CONTROLLINO_A14

////////////////////////////////////////////////////////////////////////////////
//
// Shredding cycle timings
//  this times are mainly delays to switch between
//  different cycle states:
//
//    enum SHRED_STATE
//    {
//        WAITING = 0,
//        INIT = 1,           --> cycle start
//        POWERED = 2,
//        STARTED = 3,
//        HOMED = 4,
//        PLUNGED = 5,
//        SHREDDED = 6,
//        UNPOWERED = 7,
//        DONE = 8,           --> cycle end
//        CANCELLING = 10,
//        JAMMED = 11,          --> auto reverse begin
//        REVERSING = 12,
//        REVERSED = 13,
//        STOPPING = 14,
//        FORWARDING = 15,
//        CANCELLED = 16
//    };

// the time to wait before checking the power status
// if you experiencing weird behavior, please increase this
// time, there can be spikes when powering and unpowering the VFD
#define SWT_INIT 1000

// after the VFD powered up, wait this time to proceed with shredding
// or start the homing procedure if there is a plunger present
#define SWT_POWERED 2500

// the time interval to use for checking whether the plunger is homed
#define SWT_HOMING 2500

// the time interval to use for checking whether the plunger is homed
#define SWT_HOMING 2500

// after everything is ready, eg:
//  1. powered
//  2. homed (optinal, plunger present)
//  3. vfd is set to forward
// this is the time to wait before we activate the plunger
#define SWT_STARTED 500

// the interval used to check the 'PLUNGED' state (fixed or limit switches)
#define SWT_PLUNGED 2500

// as soon it's plunged - if plunger present - this is the time to wait
// before it's stops the VFD. this time depends on the plunger distance to
// the shredder blades. in some cases it's good to leave it spinning by exactly
// this time
#define SWT_UNPOWERED 2000

// the interval to use to checking the HOMED state of the plunger after
// it stopped the shredder; when it reaches the 'homed' state, the shredding cycle
// is complete
#define SWT_SHREDDED 3000

#define SWT_SHREDDED_POWER_OFF 10000

////////////////////////////////////////////////////////////////////////////////
//
//  Plunger settings and timings
//
//  Times are base on observations rather than calculations. Calculating it
//  will affect plunger speed drasitcally even more lower spec boards.

// interval before reading the limit switch
// this setting will affect your plunging speed since analogRead will
// consume some time and we do have only one thread.
#define LIMIT_SWITCH_INTERVAL 30

// base speed and use as factor to scale settings to your hardware
#define PLUNGER_BASE_SPEED 1000

// manual move speed
#define PLUNGER_MOVE_SPEED PLUNGER_BASE_SPEED * 3000

// homing speed
#define PLUNGER_HOMING_SPEED PLUNGER_BASE_SPEED * 200

// retract speed
#define PLUNGER_RETRACT_SPEED PLUNGER_BASE_SPEED * 200

// plunge speed
#define PLUNGER_PLUNGE_SPEED PLUNGER_BASE_SPEED * 1600

// plunge/homing travel
#define PLUNGE_PLUNG_DISTANCE 145000

// inverted travel
#define PLUNGE_HOME_DISTANCE -PLUNGE_PLUNG_DISTANCE

// retract travel
#define PLUNGER_RETRACE_DISTANCE 1500

// in case limit switch broke
#define PLUNGE_MAX_MOVE_TIME SECS * 12

///     Optional : current monitor for the plunger stepper motor

// the current when the drives have no power
#define PLUNGER_STEPPER_OFFLINE_1 163
// the current when the drives have power
#define PLUNGER_STEPPER_ONLINE_1 166

/////////////////////////////////////////////////////////////
//
// Motor load settings, this requires a current sensor or can be
// taken from the VFD's output.

// the interval to read the current
#define MOTOR_LOAD_READ_INTERVAL 100

// the current measured when the motor runs idle, min - max range
#define MOTOR_IDLE_LOAD_RANGE_MIN 30
#define MOTOR_IDLE_LOAD_RANGE_MAX 50

// the current measured when the motor is under load, min - max range
#define MOTOR_SHREDDING_LOAD_RANGE_MIN 60
#define MOTOR_SHREDDING_LOAD_RANGE_MAX 300

// the current measured when the motor is overloaded, min - max range
#define MOTOR_OVERLOAD_RANGE_MIN 160
#define MOTOR_OVERLOAD_RANGE_MAX 400

#define MOTOR_MIN_DT 2500

/////////////////////////////////////////////////////////////
//
//  Bridge related
#define STATE_RESPONSE_CODE 1000
// #define BRIDGE_HAS_RESPONSE
/////////////////////////////////////////////////////////////
//
//  Error codes
//
#define E_MSG_OK "Ok"
#define E_MSG_STUCK "Shredder is stuck"

// common operating failures
#define E_OK 0      //all good
#define E_STUCK 100 //Shredder stuck

// power failures

#define E_POWER_PRIM_ON 145  // Power is on whilst it shouldn't be
#define E_POWER_PRIM_OFF 146 // Power is off whilst it should be

#define E_POWER_SEC_ON 147  // Power is on whilst it shouldn't be
#define E_POWER_SEC_OFF 148 // Power is off whilst it should be
#define E_POWER 150         // Nothing is online

#define E_VFD_OFFLINE E_POWER_PRIM_OFF    // VFD should be online
#define E_PLUNGER_OFFLINE E_POWER_SEC_OFF // Plunger should be online

// sensor failures
#define E_VFD_CURRENT 200      // VFD current abnormal: below or above average
#define E_PLUNGER_CURRENT 210  // Plunger current abnormal: below or above average
#define E_OPERATING_SWITCH 220 // Operating switch invalid value

#define E_PLUNGER_LIMIT 230    // Something is wrong with the limit switches, should
#define E_PLUNGER_OVERLOAD 235 // Stepper above average

#define E_CARTRIDGE_OPEN 240 // Cartridge open sensor doesn't work
#define E_SERVICE_OPEN 241   // Sensor open sensor doesn't work

////////////////////////////
//
// sub system failures
//
#define E_USER_START 1000 // base offset for sub system errors

// vfd
#define E_VFD_RUN 300                    // Motor should spin but doesnt
#define E_VFD_LOSS 301                   // Motor should not spin but does
#define E_VFD_CUSTOM(A) E_USER_START + A // Custom VFD error

// plunger
#define E_PLUNGER_RUN 400 310 // Plungers runs with weird limit/homing switch activity

// motor
#define E_MOTOR_DT_IDLE 320     // Motor runs idle longer as defined
#define E_MOTOR_DT_OVERLOAD 321 // Motor runs overloaded longer as defined

#define E_BRIDGE_LOSS 400 // bridge poll timeout

// bridge
#define E_BRIDGE_START 2000                 // base offset for custom bridge errors
#define E_BRIDGE_CUSTOM(A) E_USER_START + A // Custom bridge error
#define E_BRIDGE_PARITY E_BRIDGE_CUSTOM(1)  // @todo, parity check failure
#define E_BRIDGE_CRC E_BRIDGE_CUSTOM(2)     // @todo, crc  failure
#define E_BRIDGE_FLOOD E_BRIDGE_CUSTOM(3)   // @todo, msg queue

#endif