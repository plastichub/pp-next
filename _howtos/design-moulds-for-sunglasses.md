---
image: "/howtos/design-moulds-for-sunglasses/_IGP9762.jpg"
title: "Design moulds for sunglasses"
tagline: ""
description: "At Esfèrica we’ve been working to understand how to uphold the value of waste plastics for the past 5 years. Today, our project FOS Barcelona offers eyewear made with local waste and moulds for others to fabricate them in their area, in an effort to enhance distributed design. <br /><br />Here we’ll guide you through aspects we learned on our way.<br /><br />1: What you need<br />2-5: Design sunglasses<br />6-10: Design mould"
keywords: "mould,injection,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "injection"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/esfrica">esfrica</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - What you need</h4>
<div class="step-text">
  To make all the process yourself, you will need: <br />
  1. A clear mind about the model you want to create. <br />
  2. A computer and some experience working with CAD software.<br />
  3. A good CNC milling machine or a manufacturing partner.<br />
  4. An injection machine.<br />
  <br />
  Ok. Let’s dive into the steps then.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/00.jpg">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/00.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - DESIGN THE SUNGLASSES</h4>
<div class="step-text">
  When designing eyewear you can go wild, and reach few, or start with the classics, and reach many. It only depends on your intentions to choose one way or another. <br />
  <br />
  Once you have a target chosen, you will need to make decisions regarding a couple of components, which we’ll go through in the next steps.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/1.png">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Sunglasses model - Hinges</h4>
<div class="step-text">
  Metal hinges: There are several metal hinges options in the market, being the most common riveted - fixed with rivets -, in mould - placed in the mould before every injection -, screwed in - inserted after the injection and fixed using screws -, and fused - inserted after injection using heat -. <br />
  Of all of them, the easiest to detach from the product once its lifespan is over, is the screw-in models and that is the reason we choose to use this one. <br />
  <br />
  Plastic hinges: See next step
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/22.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/22.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Sunglasses model - Hinges</h4>
<div class="step-text">
  Plastic hinges: Made in the mould. This is the most sustainable choice. No materials mixtures and durability that might go beyond eyewear with metal hinges, if designed properly. The downside is the quality perception, a key point when developing products that need to sell, and a parameter that many times has no connection to actual quality, but rather with the different perceptions around materials and products present in each corner of the world.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/3.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/3.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Sunglasses model - Lenses </h4>
<div class="step-text">
  There is a wide variety of lenses in the market. Again, depending on your target and the type of eyewear you want to make you will choose one type or another. The main ones are the following, although there are many others for specific applications: <br />
  <br />
  Polycarbonate: Lenses for sports and action eyewear due to its resistance. <br />
  CR-39 (polymer): The most used type of lenses. <br />
  Mineral lenses: The glass lenses that are used less and less due to their weight and the fact that they break when falling. The main advantage is its scratch resistance.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/4.png">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/4.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Sunglasses Model - Base</h4>
<div class="step-text">
  Depending on the type of eyewear you will want the frame to have a bigger curvature to cover as much face as possible, or a completely flat frame, to follow the lastest of the fashion trends ;)<br />
  <br />
  The Base value is what is used to define this curvature. The higher the base number, the smaller the diameter of the curvature of the frame will be. Base nine is used for sport eyewear, which offers a pretty tight fit to protect the eyes as much as possible, whereas a base 2 is used for fashion eyewear where the frame is almost flat. <br />
  <br />
  Our Classic model used to be base 6. Our new models are base 4.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/5.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/5.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - DESIGN THE MOULD</h4>
<div class="step-text">
  When designing moulds to make eyewear with Precious Plastic machinery you need to make a fine exercise of balancing costs and mould quality.<br />
  For a really good finished product with little post processing needed, high quality inscriptions and outstanding surface finishes, you will have to spend €€€.<br />
  For testing purposes you can mill the mould way faster and with simpler machines, saving quite a chunk. <br />
  It is vital to understand that it doesn’t make sense to make moulds that are as good as industrial moulds for the obvious reason that they will then be almost as expensive as industrial moulds!<br />
  We went for a high quality mould that will require a small amount of postprocessing.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/6.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/6.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Parting lines</h4>
<div class="step-text">
  If you go for a cheaper mould, then go for parting lines in the edge of the parts, making it easy to post process and to achieve good final results.<br />
  <br />
  If you go for a good mould, place them either way, since they will be good enough for you not to post process them or simply polish them slightly. In this case, design choices might be the parameters that will help you decide. The images you see show how to slightly hide the parting line by adding a round up, which will make the mould slightly more expensive (especially if you do this on small features that could want a few tool changes and a lot more time) or how to keep it simple.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/7.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/7.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/8.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/8.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Closing system: How complex? </h4>
<div class="step-text">
  Closing system: How complex? <br />
  A closing system that allows for faster productions can be very costly. The clamping pressure in an injection mould is vital for good results and mould life. It is, however, not easy to find a system that allows for constant mould change and high enough pressures, evenly distributed, at a low price. <br />
  Using simple screws is a good solution for a smaller budget: It’s cheap and offers a perfect clamping pressure if they are placed right. However, it will be slower to open and close the mould.<br />
  Our recommendation is to start with screws, understand your real needs, and work from there.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/9.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/9.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/10.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/10.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/11.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/11.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Surface finishings</h4>
<div class="step-text">
  Depending on budget and intentions you can leave the tool paths, or bead blast or polish them.<br />
  <br />
  1: Leaving the tool paths is the cheapest option. You can always play with the cutting direction to make patterns - the result can be pretty interesting. <br />
  <br />
  2: Bead blasting is the cheapest way to have an even surface in a mould. With this, however, it is not possible to get a shiny finish. There are several standards for bead blasting, varying for different parts of the world. Your manufacturing partner will be able to guide you through them.<br />
  <br />
  3: Polishing offers the evenest result. As it is made by hand, it is also the most expensive one. Look for the grade you need.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/12.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/12.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Logos and inscriptions</h4>
<div class="step-text">
  To end your product, it is interesting to get awesome inscriptions on it and make sure whomever receives your product knows who made it. Two main options here: <br />
  <br />
  1: Mill a logo or inscription of your brand, limiting the production to one brand name, but being able to have high quality inscriptions without extra efforts or time. The image shows the moulds we made for Experiencia Emprendedora from Argentina. <br />
  <br />
  2: Using the awesome Samsara’s method of pressure-marking your logos and inscriptions on the surfaces. The quality won’t be as high, but it will allow you to use the mould for a variety of projects and brands, while you establish yours (if that is your plan).<br />
  <br />
  Photo credits: samsaratrc
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/13.png">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/13.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/14.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/14.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - GET READY TO INJECT THOUSANDS!</h4>
<div class="step-text">
  Once you understand the process fully, it’s time to tailor to your needs, your machines, moulds and products so you can reach the targets you set. Automating part of your process might be a good idea once you start selling regularly.<br />
  And don’t forget to pay a lot of attention to the colour formulas for your products, it’s fun!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/15.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/15.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - EMBRACE DISTRIBUTED DESIGN</h4>
<div class="step-text">
  At FOS we aim to help projects and people to be able to make the eyewear we’ve designed in their local ecosystems, hence making possible for us to stop shipping products individually and by airmail, which is a HUGE contradiction. For this, we’ve created packages that include knowledge and moulds for anyone to be able to start selling eyewear made out of plastic waste ANYWHERE. <br />
  <br />
  Distributed design is a powerful concept that needs to become a standard. We’re still far from that, but this is our contribution, so if you are interested in those packages, check our profile out. :)<br />
  <br />
  WEB: <a href="https://www.fosbarcelona.com/<br/>">https://www.fosbarcelona.com/<br /></a>
  IG: <a href="https://www.instagram.com/<br/>">https://www.instagram.com/<br /></a>
  BAZAR: <a href="https://bazar.preciousplastic.com/esferica">https://bazar.preciousplastic.com/esferica</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/FOS_sotllo_front15.jpg">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/FOS_sotllo_front15.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/isometriques_sotllo3.jpg">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/isometriques_sotllo3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/design-moulds-for-sunglasses/16.PNG">
        <img class="step-image" src="/howtos/design-moulds-for-sunglasses/16.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>