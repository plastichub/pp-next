---
image: "/howtos/dry-materials-for-higher-quality-products/Precious Plastic Thumbnail.jpg"
title: "Dry materials for higher quality products"
tagline: ""
description: "When you want to make products using more advanced Materials (e.g. industrial waste), drying your granulate is a necessary step. <br />We will show you how to dry your granulate before extrusion cheaply and effectively, which is especially important if you want to make high quality products like 3d printing filament.<br /><br />You can check out our How-To video here:<br />&lt;a href=&quot;https://youtu.be/dkm_gXxX2pk&quot;&gt;https://youtu.be/dkm_gXxX2pk&lt;/a&gt;"
keywords: "hack"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/qitech-industries">qitech-industries</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - When and why to dry</h4>
<div class="step-text">
  There are two main types of polymers, polar and nonpolar. Nonpolar Materials usually don&#39;t need to be dried. Exposing nonpolar materials (HDPE/PP etc.) to water is like putting oil in water. Almost every other commercial polymer has some level of polarity and therefore can absorb a certain amount of moisture from the air. Many materials are dried only to optimize surface appearance, too much moisture can lead to cosmetic defects known as splay or silver streaking. Other polymers are damaged if they are processed with too much moisture. These materials chemically react with the moisture (hydrolysis), resulting in a reduction in material strength. Check out the material overview below.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Erste Tabelle_Zeichenfläche 1.png">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Erste Tabelle_Zeichenfläche 1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Different drying methods</h4>
<div class="step-text">
  There are several different DIY methods to dry polymeres; you can use an oven, a dehydrator or a vacuum dryer.<br />
  But In this How-To we will focus on a self-built drybox with silica gel, because this method delivers the best results while being cheap, easy and energy efficient. One of the problems we encountered with the oven is overheating the plastic, resulting in a big mess. We tried replacing the silica gel with a household dehydrator, but they are not designed to achieve this kind of low humidity levels needed to effectively dry plastic. We also included a temperature table for different polymers.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Zweite Tabelle_Zeichenfläche 1.png">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Zweite Tabelle_Zeichenfläche 1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make and airtight plastic box</h4>
<div class="step-text">
  To make our drybox, we simply use a plastic box that we upgrade with some window seals to make it airtight. Simply, glue them to the rim of the box.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 3 (2).jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 3 (2).jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 3.jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Get Silica Gel</h4>
<div class="step-text">
  You might already know silica gel from a shoebox for instance. It comes in small bags in order to keep the inside dry by absorbing any moisture. Silica gel works perfectly with polymers as well so just put around 2 kilograms in the plastic box. There is no particular amount you have to use, but the more you have in the box the less often you need to take them out and dry them.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 4 .jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 4 .jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 4 (1).jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 4 (1).jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Get a hygrometer</h4>
<div class="step-text">
  A hygrometer comes in handy to check the humidity inside the drybox. It is convenient to use a transparent box so you can keep track of the drying process at all time.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 5.jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 5 (2).jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 5 (2).jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Place granulate inside the box</h4>
<div class="step-text">
  Using a microplastic bag (we use one from Guppyfriend) to place the material inside of the drybox will save you time and space. It keeps everything in place while the humidity can get out.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 6.jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 6 (2).jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 6 (2).jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Check the results</h4>
<div class="step-text">
  The simplest method to measure how much moisture has been absorbed is weighing the granulate beforehand and afterwards. <br />
  Unfortunately, if you are using hot air to dry granulat, volatiles are released from the sample, also reducing the weight. Therefore the weight loss does not exactly equal the loss of moisture so the measurement is imprecise. The professional approach involves making accurate moisture measurements in the drying hopper. This is possible using measurements of dielectric properties in real time. sensors are very expensive though.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step 7.jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step 7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Renew the silica gel</h4>
<div class="step-text">
  After a while, you will notice that the silica gel reaches its maximum capacity of water that it can absorb. Simply put it in the oven for one or two hours at about 100°C/212°F and it will be as good as new.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Def7hdBUwAA2YTq.jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Def7hdBUwAA2YTq.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Bonus tip</h4>
<div class="step-text">
  Rapid changes in the Temperature lead to surface moisture.<br />
  If a cold drink placed outside in the hot summer, you will notice that water starts condensing on the surface.<br />
  This also is the case for polymers. We had many problems because we took plastic from the cold, damp garage to the warm production area. This resulted in a lot of surface moisture that produced bubbles and inconsistencies in our 3D-Printing Filament.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step last.jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step last.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Step last 2.jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Step last 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/dry-materials-for-higher-quality-products/Ste last 3.jpg">
        <img class="step-image" src="/howtos/dry-materials-for-higher-quality-products/Ste last 3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>