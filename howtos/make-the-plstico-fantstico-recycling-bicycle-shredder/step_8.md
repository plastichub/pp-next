### Securing the shredder in place 

 Once the shredder is assembled with the axle and gear securely connected and the shredder bolted to the support frame the chain and gearing can next be considered.   