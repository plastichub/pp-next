### Why a washing machine? 

 A washing machine offers a commonly available machine with functions we can make use of. It provides heated water, which helps to remove oils and dissolves many contaminants. It creates friction through spinning, which scrubs plastic against each other and takes of dirt. It has multiple flushing cycles to get out of the old water and centrifugation helps to dry the plastic. Though we need to shred the plastic before because unshredded plastic has a huge volume and a washing machine has only a little space.
