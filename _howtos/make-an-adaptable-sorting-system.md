---
image: "/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-12-20 at 3.03.16 PM.jpeg"
title: "Make an adaptable sorting system"
tagline: ""
description: "Make a sorting system that can adapt! if you have more plastic from one type just move the tabs to suit your needs!"
keywords: "collection,sorting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "collection"
- "sorting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/guas">guas</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Cuting the panels- CNC machine</h4>
<div class="step-text">
  In this step, you can send the files we provide to be cut on a CNC machine or laser cutting machine. Be aware, these files were made in proportion to our material thickness of 25mm so all the joints match, if you change this measurement make sure you change it in the file too.<br />
  In our case, we only cut the outline on the machine but you can use this machine to do all the bevels and number engraving too.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/Captura de ecrã 2020-12-08 125522.png">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/Captura de ecrã 2020-12-08 125522.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/20201002_114814_1_Moment.jpg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/20201002_114814_1_Moment.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/20201002_114814_1_Momeffrnt.jpg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/20201002_114814_1_Momeffrnt.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Plastic type- LaserCut</h4>
<div class="step-text">
  After cutting all the panels use the files with the numbers and laser cut them, make sure you align it well because each divider panel as a number on both sides, the best corner to align it from is the top highest corner.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/kjhgfds.jpg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/kjhgfds.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.14 PM (1).jpeg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.14 PM (1).jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/jgydfhdgfg.jpg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/jgydfhdgfg.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Cuting and Finishing</h4>
<div class="step-text">
  In this step, we cut the front, bottom, and back panels.<br />
  Also in this step, you can start finishing all of the parts and sanding to give a smooth touch.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.15 PM.jpeg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.15 PM.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.17 PM.jpeg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.17 PM.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Mounting all the pieces</h4>
<div class="step-text">
  After sanding everything you can assemble it with screws. First, you attach the sides and the bottom then you just fit the divisors as it suits your needs.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.16 PM.jpeg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.16 PM.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.14 PM.jpeg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/WhatsApp Image 2020-11-11 at 2.32.14 PM.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - ALL DONE!!</h4>
<div class="step-text">
  Now you can organize your plastic much more easily!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/AAD_3358.jpg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/AAD_3358.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/DSC_0011.jpg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/DSC_0011.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-adaptable-sorting-system/AAD_3221.jpg">
        <img class="step-image" src="/howtos/make-an-adaptable-sorting-system/AAD_3221.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>