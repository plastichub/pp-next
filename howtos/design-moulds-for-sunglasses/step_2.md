### Sunglasses model - Hinges 

 Metal hinges: There are several metal hinges options in the market, being the most common riveted - fixed with rivets -, in mould - placed in the mould before every injection -, screwed in - inserted after the injection and fixed using screws -, and fused - inserted after injection using heat -. 
Of all of them, the easiest to detach from the product once its lifespan is over, is the screw-in models and that is the reason we choose to use this one. 

Plastic hinges: See next step
