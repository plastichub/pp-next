---
title: Filament
permalink: /resources/filament
breadcrumbs: true
layout : single
author: false
sidebar:
    nav: "resources"
type: resource
---

## Making filament, the industrial way

{% include video id="OEkksADFjP8" provider="youtube" %}

In [even larger environments](https://www.robotdigg.com/product/1657/PEEK-3D-printing-filament-production-line), there are machines called the 'filament miles', exceeding easily 5 meters. Such machine can cost around 35000 Euro. Here some [thoughts building one from scratch](https://precious-plastic.org/home/library/machines/filament-extruder/).

![](http://precious-plastic.org/wp-content/uploads/2019/05/2955962bc01328bbc1898976e223ba32-600-600.jpeg)

### Filament for DIY

There is success creating filament from printer waste. Please check some of the links below :

![](http://precious-plastic.org/wp-content/uploads/2019/06/diy-300x161.png)

[](https://www.youtube.com/watch?time_continue=1&v=TkZ1h22KqbU&ab_channel=Werbewunder)

We recommend to watch also all the other videoThere is success creating filament from printer waste. Please check some of the links below :

We recommend to watch also all the other videos of Werbewunders of [Werbewunder](https://www.youtube.com/channel/UCxkd2FJltPA2vwofdKxn-cw/videos)

Not filament but this here prints recycled pellets.

<a href="https://davehakkens.nl/community/forums/topic/large-format-flake-extruding-3d-printer-prototype/">
![](http://precious-plastic.org/wp-content/uploads/2019/05/Capture2-300x161.png)]
</a>


Or use forum search '[filament](https://davehakkens.nl/community/forums/search/filament/)'.

Other remarks :

* The wood auger drill is simply unfitted as [reported multiple times in the forum](https://www.google.com/search?q=%27wood+auger+NEAR+filament%27+site%3Adavehakkens.nl&oq=%27wood+auger+NEAR+filament%27+site%3Adavehakkens.nl&aqs=chrome..69i57.21183j0j7&sourceid=chrome&ie=UTF-8)
* Please check Dave Hakken's [filament maker mile](https://precious-plastic.org/library/machines/filament-maker/)
* Bazar [link](https://bazar.preciousplastic.com/?q=filament) to LDPE filament but we couldn't get any feedback
* [3dseed](https://www.facebook.com/3dSeed/) is using grinded plastic then print it right away. The only real suitable plastic type is PET which requires you to have a [good shredder (with grinder)](https://3devo.com/) in place
* [vanplestik.nl](http://vanplestik.nl) managed to print larger diameter of PET
* There is some development to print granulate by [Sam](https://www.facebook.com/sam.smith.311?__tn__=%2CdlC-R-R&eid=ARCp4zgkfvXPhideSIKIEHOc686pqyvj40UbK17cA-npis1PgJRf9Kxxalu6acR9XSOdlkuplVh1Z-1C&hc_ref=ARR_9L_otfdc8_FjH-bZvOucg_gnhhaZZvTaMRXmQuWMCEtSSaBgmTG4r7vUwyHsHik) ([forum link](https://davehakkens.nl/community/forums/topic/large-format-flake-extruding-3d-printer-prototype/)) see here the [video](https://vimeo.com/341913802).
