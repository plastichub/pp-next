### Make the female mold  

 Now we’ll make the female mold. Take the aluminium block (no. 1) and drill a 1” deep hole in the center of face A of the block. Start with smaller bits until you reach the inch. Then, mill face B to open a channel of 1” wide. Use a round point bit to get a better quality finish. 

(Drawings page 6)