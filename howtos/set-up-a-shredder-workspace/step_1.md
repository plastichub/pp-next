### Outcomes 

 The outcome of a Shredder Workspace is shredded plastic.

Shredded plastic can be of various sizes (small, medium, large) and colours. Single colour shredded plastic is higher in value and people can pay more for that.

The shredded plastic can then be bought from other Precious Plastic workspaces.
