## Hydraulic Power Pack

# Little Champ

![Little Champ](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/reference/little-champ/Little-Champ-web.jpg)


![Little Champ Kit](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/reference/little-champ/champ-kit-clean-web.jpg)

This development will draw heavily on the design of the Little Champ Hydraulic Power unit (10G)

[Continental Hydraulics](http://www.continentalhydraulics.co.uk)

[Catalog with Schematics](http://www.continentalhydraulics.co.uk/pdf/2013/power_units/little_champ_hydraulic_power_unit_kits.pdf)


