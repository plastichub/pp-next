---
image: "/howtos/create-marble-from-plastic-bags/marble_01.jpg"
title: "Create marble from plastic bags"
tagline: ""
description: "Sonja will show you a simple technique to make blocks that look like marble made from recycled bags."
keywords: "LDPE,compression,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "LDPE"
- "compression"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather everything you need</h4>
<div class="step-text">
  Before you start get all your gear ready:<br />
  - Respirator mask<br />
  - Gloves<br />
  - Scalpello<br />
  - Lots of plastic PE bags from shopping or supermarkets<br />
  - Respirator mask<br />
  - Shredder machine or scissors<br />
  - Basic mould<br />
  - Scrap metal bowl<br />
  - Compression machine<br />
  - Sanding machine<br />
  - Planner (if available)<br />
  - Plastic Type Stamp
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-marble-from-plastic-bags/marble_02.jpg">
        <img class="step-image" src="/howtos/create-marble-from-plastic-bags/marble_02.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Watch this video tutorial</h4>
<div class="step-text">
  Watch this video tutorial to learn step-by-step how to make the plastic dough, transform it into a block and eventually polish it nicely.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Done & Share</h4>
<div class="step-text">
  After watching this video you should understand the basics of this low-tech technique You can reproduce the blocks or try to make other products. We’re looking forward to seeing what you can come up with. Make sure to share back your new creations so the community can learn from you. Tag #preciousplastic on social media, create a new how-to or send us an email.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-marble-from-plastic-bags/marble_03.png">
        <img class="step-image" src="/howtos/create-marble-from-plastic-bags/marble_03.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>