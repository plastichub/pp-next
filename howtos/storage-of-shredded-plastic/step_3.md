### Bottle lids 

 To solve this problem, I suggest using special covers with ventilation holes. They protect the material from dust, but the plastic inside can be ventilated. On the top lid there is a plastic type designation. Below the inner surface, the outer diameter is indicated.
Yes, this is another problem, even if we take bottles from the same manufacturer, from batch to batch the diameter of the neck will differ (in my practice, this value varies from 45.75 mm to 47.75 mm).

In the attachment you will find link for files for different types of plastic and different sizes (in formats: f3d and step).