### How we built it 

 Here, we're covering the upgrade to the Injection Machine.

You'll need:
• A Precious Plastic injection machine
• A 5T log splitter
• Ability to cut steel, weld and drill holes
• Hopper (see .zip file attached)

Below is the direct link to the particular log splitter we used:
www.bunnings.com.au/homelite-2200w-5t-electric-log-splitter_p0044725