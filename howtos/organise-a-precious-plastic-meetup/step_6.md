### Extra: Prepare samples 

 It’s always nice to bring some Precious Plastic material samples and products to help people understand the process and get them excited about the results.

If you can’t find anything locally, you can have a look at the Bazar and get some community creations.

👉 bazar.preciousplastic.com