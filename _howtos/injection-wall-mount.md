---
image: "/howtos/injection-wall-mount/wallmount-2.jpg"
title: "Injection wall mount"
tagline: ""
description: "The version 2 of our injection machines comes with a basic frame that keeps it standing. Easy peasy. However, how cool would that be to mount your injection machine on the wall?"
keywords: "injection,hack"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "hack"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  We have created a simple wall mount so you don’t need to make the entire bottom half of the frame. Moreover, this mount makes the machine way more stable and solid. Nothing moves. However, keep in mind that it doesn’t fit every place and you can’t move it around anymore. You can find the technical drawing in our download-kit.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-wall-mount/wall-technical-p-1080.jpeg">
        <img class="step-image" src="/howtos/injection-wall-mount/wall-technical-p-1080.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cut, drill, weld and paint</h4>
<div class="step-text">
  Follow the dimensions shown in the download files and cut your materials to size. Once cut you can drill the parts as needed.<br />
  <br />
  With your parts ready, position them together and weld. <br />
  <br />
  After welding you can add some paint for a better look and extended life.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-wall-mount/wallmount-1.jpg">
        <img class="step-image" src="/howtos/injection-wall-mount/wallmount-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Mount your machine</h4>
<div class="step-text">
  Now you can connect your bracket to the wall and hang your injection machine. Be sure to put it in the right place.<br />
  <br />
  Done :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-wall-mount/wallmount-3.jpg">
        <img class="step-image" src="/howtos/injection-wall-mount/wallmount-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>