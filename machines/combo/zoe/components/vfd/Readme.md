# Huanyang

Parameters

| Code  | Value | Remarks|
|---	|---	|---	|
| PD001	|  1  	|   	|
| PD002 |  1  	|   	|
| PD003 |  50  	|  Main frequency 	|
| PD004 |  50  	|  Base frequency 	|
| PD005 |  85  	|  Max frequency  	|
| PD011 |  20  	|  Frequency lower limit  	|
