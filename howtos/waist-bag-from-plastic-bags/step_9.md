### Finish the Straps 

 Using the sewing pins roll the strap to make the ends. Make sure to put the snap-fit buckle on the strap first!

To the best of your abilities sew the end. At this point the bag is ready, yay!