### Post processing 

 If you've done everything properly, there will only be minimal post processing required. This involves cutting off the injection point and the relief channels. 

You can also clean up the part line. We recommend doing this with a knife so you can recycle the shavings again!