### Collect and select your foils 

 This part will be a combination of your own design choice, as well as what plastic material is available around you. Collect your waste foils, which can include plastic bags and any small plastic foils, wrappers, and packaging. Try to make sure that you are using the same type of material (LDPE, HDPE, PP etc.). 

Now, use some artistic flair! From what you have, select a colour palette that you will work with. Your raincoat will have a much nicer finish if you ensure that any colours/patterns work well together. If you’re happy to use any combination of colours, then the coat will still be functional. 