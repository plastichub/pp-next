# Spain / Europe

## Aluminium castings

- [fundicionaluminio](http://www.fundicionaluminio.com/tecnologia)

## Galvanizing / Zinc

- [unizinc](http://www.unizinc.com/home.php)


## Pulleys

- [RS-Online](https://uk.rs-online.com/web/c/pneumatics-hydraulics-power-transmission/power-transmission-pulleys/wedge-vee-belt-pulleys/)


## Belt tensioner

- [maedler.de](https://www.maedler.de/product/1643/1617/keilriemenspanner-mit-montierten-keilriemenscheiben)


# China

- [Custom Knifes - Nanjing Wanguo Machinery Technology Co., Ltd.](https://wgmachine.en.alibaba.com/?spm=a2700.12243863.0.0.6a903e5fCSiHCe)
