### Preparing Your Object 

 First clean your object of dirt or dust.
 
Using clay place a small strip to the back of your object making sure the object sits flat when placed down—this will become the entry for your plastic once the mould is complete.

Be sure to cover any wholes and neaten the edges with a knife cutting away any excess.
Place the object facing upwards onto the take-away container lid.

Now you are ready to build the walls of your mould box.