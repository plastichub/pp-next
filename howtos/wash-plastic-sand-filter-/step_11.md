### How to clean the filter: Backwashing 

 Once the sand clogged up too much and the manometer shows a high pressure, it is time to clean it. For this, the sand filter uses a process called backwashing. Clean water is pumped through the output of the filter and finds its way out through the input. While doing this, it’s forced to pass the sand from the opposite way, the sand and dirt float up, but only the dirt is light enough to go out with the water.

Roughly one 120 litre barrel of water passes this way. The water is collected and can get cleaned with a single-use filter to have it further in the system.