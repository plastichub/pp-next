### Your map pin 

 This is the part where you add your pin to the map.
We will use your image from the step above. You only need to add a short text to give other people a quick understanding of what you’re doing.

Info: Currently there is still a bug in the map that doesn't show your exact location but 2 blocks away (working on it!)