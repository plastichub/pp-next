### Locate a concrete slab (Fixed structure) 

 To ensure a strong foundation for our structure, we will need to locate or make a concrete foundation. We will not explain how to make one here but you should be able to find comprehensive guides online.

In this How-to we used the concrete slab of our workspace, which is at least 30cm thick. Depending on your structure 15cm could be utilised.
