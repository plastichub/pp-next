### Silver Streaks 

 Silver Streaks describe the appearance of silver lines on your part. This occurs mostly with the use of PS or ABS material.

Solutions:
- Pre drying of your material will solve the problem. A standard oven with convection heating can be used as a quick fix. For long term use special pre drying applications should be used for higher energy efficiency.