### Role 

 First of all, make sure you had a look at the showcase page of this Starterkit!
👉 preciousplastic.com/starterkits/showcase/injection

Now about your Role:

Injection Workspaces shred plastic waste and transform it into valuable products. The volumes of plastic are smaller, so they can either set up a small plastic collection in-house or get in contact with the local Collection point. The products are sold directly to customers or organizations. Workshops are also a valuable asset for Injection Workspaces. 

Injection workspaces should also reach out to Community Point to connect with the local Precious Plastic community and maybe get help selling their products or find plastic waste. 