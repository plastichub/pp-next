---
image: "/howtos/make-a-carabiner-cnc-vs-lasercut/Carabiner Single-howto.jpg"
title: "Make a carabiner (CNC vs. lasercut)"
tagline: ""
description: "A carabiner is a useful token that can be given away after an injection moulding workshop at Precious Plastic. Its small size allows you to make up to 6 pieces in one mould, and no assembly needed after injection.<br /><br />Each of them will come out with a different marbling pattern, making them unique and memorable!"
keywords: "mould,PP,product,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "PP"
- "product"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - CNC or laser cut?</h4>
<div class="step-text">
  CNC moulds are precise and allow you to have a better surface finish rounded corners, also they are lightweight, durable and fast to unmould, since you can make products with draft angles, so it&#39;s ideal for bigger productions. However they can be very expensive and not super accessible.<br />
  <br />
  On the other hand, the laser cut version of this product will make you save some money and very probably is going to be easier for you to find someone to make this job for you! However steel density is 3 times higher than aluminum which makes it harder to work with, also it&#39;ll have less details, as you will only get the outline of the carabiner therefore the edges will be sharper.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/cnc version.jpg">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/cnc version.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/laser version.jpg">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/laser version.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - CNC - Prepare the file </h4>
<div class="step-text">
  First of all, decide if you want to customise your carabiners or not. If you are already investing in the CNC cutting, it can make sense to take advantage and add some cool engravings.<br />
  <br />
  To make it even more functional, you can add your logo, web page and even the plastic type you will use (PP in this case), so then you’ll not need to stamp it afterwards, one step less! To know more about tips and tricks for CNC moulds go to the academy section.<br />
  <br />
  Here some tips on how to make injection moulds:<br />
  👉 tiny.cc/injection-moulds
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/DSC_0135.jpg">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/DSC_0135.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/DSC_0129.jpg">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/DSC_0129.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - CNC - Mould</h4>
<div class="step-text">
  After you customise you mould, you will be ready to send it to cnc. After you receive it, you’ll need to finish some details be able to use it with the injection machine. To know more about how to prepare your mould to use it with the injection machine, go to te link below.<br />
  <br />
  Here some tips for using the Injection machine:<br />
  👉 tiny.cc/work-injection-machine
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/1. drill the 19mm hole.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/1. drill the 19mm hole.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - CNC - Prepare your material </h4>
<div class="step-text">
  The material we are going to use for this product is Polypropylene, because of its flexibility. This is very important to make these carabiners functional.<br />
  <br />
  Try to use one source of PP as a base to make it as pure a possible (at least 70%) and then add a couple of colorful flakes from other source (also PP) to make it fun.<br />
  <br />
  To fill up this mould you will need around 100 grams, I would recommend you to always fill up the barrel to be able to inject with more pressure!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7007.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7007.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - CNC - Inject and finish</h4>
<div class="step-text">
  After injecting you should be able to take the carabiners from the mould just by twisting them.<br />
  <br />
  Then you just need to remove the leftovers from the twisting process with a cutter.<br />
  <br />
  Now your carabiners are ready to use!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_6914.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_6914.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_6921.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_6921.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_6941.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_6941.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Laser cut - Prepare the file</h4>
<div class="step-text">
  Download the file and send it to laser cut in steel, it’s important to ask for the middle plate (n°2) in 6 mm thick steel. This will determine the thickness of the final product.<br />
  <br />
  The top and bottom plate (1 and 3) should be cut with 8-10 mm thick steel (the thicker the better, but also heavier!) to make the mould stronger and avoid bending while injecting.<br />
  <br />
  (If you have other ways to reinforce moulds keeping it lighter, go for it! And don&#39;t forget to share (: ).<br />
  <br />
  If you are good with the grinder you can also cut these two by yourself or just send everything to lasercut!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/laser cut.png">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/laser cut.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Laser cut - Finish the mould</h4>
<div class="step-text">
  After receiving your laser cut order you will probably need to sand the middle plate.<br />
  <br />
  Depending on how dirty the cut is, either use the grinder or just do it by hand with sanding paper.<br />
  <br />
  After this, weld the nozzle. It goes exactly to the center of the top plate with a 5 mm hole, so you can drill a bigger hole with the diameter of the nozzle.<br />
  <br />
  Now close the mould, remember to tighten the bolts very well and your are ready to go!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/_DSF4831 (1).JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/_DSF4831 (1).JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/7.1 Laser cut - Finish the mould.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/7.1 Laser cut - Finish the mould.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7017.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7017.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Laser cut - Inject and finish</h4>
<div class="step-text">
  Time to inject!<br />
  <br />
  To separate the middle plate from the top plate, hammer a bolt into the nozzle, you will be able to support the top and middle part with two pieces of wood to do this step, as you can see in the first picture (that&#39;s why the middle one is sligthly smaller). <br />
  <br />
  Afterwards tap the channels gently to take everything out from the mould. You can use something like a bolt for this.<br />
  <br />
  Whenever it&#39;s out, twist the carabiner from the channel and finish it with a cutter.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/laser 2.jpg">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/laser 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/8.1 Inject and finish.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/8.1 Inject and finish.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/8.2 Inject and finish.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/8.2 Inject and finish.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Mark your products!</h4>
<div class="step-text">
  Last but not least, stamp your carabiners!<br />
  <br />
  This is one of the most important steps and at the same time, the most forgotten. It’s very important to stamp your plastic in order to be able to recycle it in the future if its needed.<br />
  <br />
  You can either engrave it by hand with a dremel directly into your mould or make a stamp which you can also use for other products.<br />
  <br />
  Here you can find a tutorial on how to make a simple wire stamp:<br />
  👉 tiny.cc/wire-stamp
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7101.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7101.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7106.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7106.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7090.JPG">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/IMG_7090.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Done!</h4>
<div class="step-text">
  And here you go, you got your first bunch of carabiners!<br />
  Have fun with them and use them to inspire people about the hidden value of plastic waste :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/Carabiner Linked-howto.jpg">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/Carabiner Linked-howto.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-carabiner-cnc-vs-lasercut/Carabiner Keys-howto.jpg">
        <img class="step-image" src="/howtos/make-a-carabiner-cnc-vs-lasercut/Carabiner Keys-howto.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>