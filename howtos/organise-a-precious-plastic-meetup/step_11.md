### Group your group 

 Now let’s analyse the potential and interests in your group.
Let the group answer the following questions:
Which area would you be interested to start/contribute/offer? (collection, machine building, working with machines, funding, following/sharing)
What resources do we have available? (space, time, money, people)
Who would be volunteering, charging or investing money?
How much time could you invest (per day, per week, per month)?
In bigger groups it can be fun to do this as a grouping exercise where they have to position themselves to a certain category.

Make sure to document this on a paper/digital document, so you can share it and refer to it when needed.
