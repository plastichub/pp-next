### Build the workspace 

 As you will probably have different operations running at the same time, we recommend your workspace to be organised and split your space into at least two areas:

A “dusty” area where you will cut, drill grind, weld etc. dedicated to heavy messy jobs. Ventilation is a must here. 

And a "clean" area where you carry out assembling and electricity cabling. Here you can also dedicate space to neatly store parts and components.

Ideally, you also have a "ventilation" area for paint jobs (sand blasting and painting).