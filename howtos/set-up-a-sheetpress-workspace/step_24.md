### Good things take time 

 Starting off will take some time in the beginning. It’s normal. Be patient, work smart and reach out to your Precious Plastic community if you need help. The efforts will pay off eventually :)

You’re changing the world. Thank you!
