### DESIGN THE SUNGLASSES 

 When designing eyewear you can go wild, and reach few, or start with the classics, and reach many. It only depends on your intentions to choose one way or another. 

Once you have a target chosen, you will need to make decisions regarding a couple of components, which we’ll go through in the next steps.
