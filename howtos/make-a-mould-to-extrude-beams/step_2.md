### Make mounting brackets 

 For connecting the beam to the plate we need to make mounting brackets.. angle iron works well for this. All you need to do is cut angle iron into sections of 30-40mm and drill a hole in one side to fit your bolts. For larger beams use bigger, thicker angle iron. 

Pro tip: This is a relatively simple process.. so you can save some time here by making lots of brackets in different sizes at once. When you need to make more moulds in future you'll thank yourself for that little extra you put in.
