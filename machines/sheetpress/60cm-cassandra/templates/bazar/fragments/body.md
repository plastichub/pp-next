Advanced sheetpress, ideal for workshops, fablabs and small enterprise

The press mechanism is electronical (stepper motor with toggle clamp)

### Specifications

${mini_specs}

### Options

- Precision Temperature controllers (+/- 1 DegC tolerance)
- Loader table
- Touchscreen for storing different temperature profiles


<div style="clear:both">
    ${product_overview_drawings}
    <div style="padding: 16px; display: inline-block">
        ${product_resources}
    </div>
</div>
