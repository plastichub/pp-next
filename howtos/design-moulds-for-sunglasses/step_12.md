### EMBRACE DISTRIBUTED DESIGN 

 At FOS we aim to help projects and people to be able to make the eyewear we’ve designed in their local ecosystems, hence making possible for us to stop shipping products individually and by airmail, which is a HUGE contradiction. For this, we’ve created packages that include knowledge and moulds for anyone to be able to start selling eyewear made out of plastic waste ANYWHERE. 

Distributed design is a powerful concept that needs to become a standard. We’re still far from that, but this is our contribution, so if you are interested in those packages, check our profile out. :)

WEB: https://www.fosbarcelona.com/
IG: https://www.instagram.com/
BAZAR: https://bazar.preciousplastic.com/esferica
