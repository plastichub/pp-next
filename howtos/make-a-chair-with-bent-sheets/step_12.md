### Trim the legs 

 Put the legs back to the jig and cut them roughly where the jig ends. The final alignment will be done after assembly. Mark the position where to cut at the top end and trim with precision, as this is final.
