---
image: "/howtos/make-a-bench-with-beams/TO Front On Big Angle-howto.jpg"
title: "Make a bench with beams"
tagline: ""
description: "The technique of extruding angled beams provides many applications in furniture design. In this How-To, we will take you through the steps of making a bench from angled PP beams and wood."
keywords: "extrusion,product,PP,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "product"
- "PP"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  Okay, before jumping into the building process make sure you&#39;re ready to make this How-to.<br />
  As the bench is made with extruded beams, you’ll need to make or find an extrusion machine to produce the beams, or otherwise commission a workspace to make them for you :) For the rest you will need access to a wood and metal workshop.<br />
  <br />
  To make the beams you will need a 80x80x3mm square tube for the mould and around 10kg plastic..<br />
  <br />
  Other materials needed for the bench:<br />
  - 6mm threaded rod<br />
  - 10mm Barrel nuts<br />
  - Wood<br />
  - 4x70mm wood screws<br />
  - Tamper-proof nuts (optional)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-1.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Make your moulds</h4>
<div class="step-text">
  Let&#39;s start with making the moulds for our angles beams! Make sure you&#39;re familiar with the process of the How-to &quot;Make angled beams&quot; as this step is based on that process. For this bench, two moulds will be needed to make the four components. <br />
  <br />
  Take your 80x80x3mm metal tube, cut it to the measurements shown in the image and weld the mounting brackets.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-3.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare your material</h4>
<div class="step-text">
  Let&#39;s start making the angled beams. Get your cleaned and sorted plastic shredded into a size that works well with your extrusion machine. PP and HDPE should work quite well for this product (PS is probably to brittle for a furniture like that).<br />
  <br />
  (In this case we used PP and made a mixture of yellow and orange (1:4) to achieve a zesty orange.)<br />
  <br />
  Pro tip: Let your plastic dry fully before melting it, as moisture can cause a large air pockets in your part.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-2.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Extrude your parts</h4>
<div class="step-text">
  For many reasons your machine may differ to the one we’re using at the workspace here in Eindhoven, so take care with your settings and use the experience you’ve had with PP. In this case, we set our machine to temperatures (from hopper to nozzle) 235°C, 245°C, 250°C. While running the machine at 102rpm it took 15 minutes for each part, letting one cool while extruding the next. <br />
  <br />
  In this case we went for the textured beams - have a look at the How-to below to decide what texture you want.<br />
  <br />
  Related links:<br />
  Extrude different textures 👉 tiny.cc/extrude-textures
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-4.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Finish the beams</h4>
<div class="step-text">
  After having produced your beams you can now begin preparing them for the bench. Use the<br />
  measurements and angles as pictured. Note that the parts may be textured and probably have shrunk in the process of cooling. Each part will be slightly different so take care to match the parts while measuring so they are the same. It is also helpful to de-burr the parts after they have been cut to define the edges.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-5-1.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-5-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-5-3.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-5-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-5-2.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-5-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Cut grooves</h4>
<div class="step-text">
  In this step we will create the grooves into which part A will be held within Part B. Find the exact position to cut by laying your parts as they should fit and use a straight line to find the spot. Rough dimensions have been shown. To begin, set the saw blade to a depth of 10mm. Take care on the first two outer cuts as you’d like to hold a tight tolerance. Follow these by making more cuts in between to remove as much material as possible. Finish by clamping the part down and working at it with a chisel.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-6-2.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-6-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-6-3-__.png">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-6-3-__.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-6-4.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-6-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Test fit and refine</h4>
<div class="step-text">
  Due to the nature of this recycling process, you cannot count on every part being exactly the same. So now is the opportunity to test fit and refine the grooves for that perfect, solid fit. Ideally partA should fit snug within part B, tight enough to hold itself.<br />
  <br />
  Tip: Use a heat gun to smooth and finish your plastic once your grooves fit well.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-7-1.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-7-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-7-2.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-7-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Drill thread rod holes</h4>
<div class="step-text">
  In this step we are going to be drilling the 6.5mm holes that will house the 6mm threaded rod. Begin by marking the midpoint and ‘true’ angle of Part A through part B. Follow this line around the corner to find the centre point and mark it. The lines on the side can be used to position the piece under the drill bit with a vice clamp. <br />
  <br />
  Take care to align drill and marker lines as closely as possible and drill through. We can then use this hole to mark the point on Part A for drilling. As before, position the piece under the drill press, align the bit with the edge of the part and drill.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-8-2.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-8-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-8-1.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-8-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-8-3.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-8-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Drill barrel nut holes</h4>
<div class="step-text">
  Insert the threaded rod into the hole you have made and take note of how deep the rod goes. Use this measurement to define where you want the barrel nut to anchor the rod. Place your part under the drill and make a 10mm hole for your 10mm barrel nut. Do not drill this hole all the way through. A smaller hole can be made through the beam that you can use to remove the barrel nut in future.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-9-2.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-9-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-9-1.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-9-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-9-3.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-9-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Cut thread and assemble</h4>
<div class="step-text">
  With all the holes drilled, insert the barrel nuts, align and screw the thread. From this we can cut the rod to the exact length for that clean finish. Then it’s just a matter of sliding the parts together, adding a washer and tightening the nuts! <br />
  <br />
  We’ve used ‘tamper-proof’ nuts in this case to prevent any fiddling with the fit in the future.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-10-1.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-10-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-10-2.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-10-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Prepare the wood</h4>
<div class="step-text">
  With your parts done, you can now continue with your wooden planks (or other material) for the seating. In this case we’ve gone with pine cut into 8x strips of 28x60x1500. This has then been drilled, sanded and oiled for the finish. These planks will be mounted to the plastic bench via wood screws.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-11-2.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-11-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-11-1.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-11-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Time to assemble</h4>
<div class="step-text">
  With the wood prepared, you can begin assembling the bench together. Use some wood to create an equal spacing between all the planks and fasten them to the plastic with screws. We have used 4x70mm wood screws.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-12-2-.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-12-2-.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-12-3.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-12-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Add the reinforcement</h4>
<div class="step-text">
  Depending on the material you have used, you may need to reinforce the seat. In this case a short section of a 25x25mm plastic beam has been fastened to the middle of the seat. However, wood will work just fine or perhaps another material.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-13.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-13.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Take a seat!</h4>
<div class="step-text">
  Congratulations! Your bench is complete. You can now sit back and enjoy or possibly disassemble and pack it for a lucky customer :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/howto-bench-14.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/howto-bench-14.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Go all in with plastic!</h4>
<div class="step-text">
  You can also make this bench entirely with plastic beams and play around with colours and textures. Note that the plastic is softer than wood and may need extra support.<br />
  <br />
  And don&#39;t forget to stamp your plastic parts, so it&#39;s clear which plastic type is used here :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/TP Concrete Side Angle-howto.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/TP Concrete Side Angle-howto.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-bench-with-beams/TP Concrete Close Up Stamp-howto.jpg">
        <img class="step-image" src="/howtos/make-a-bench-with-beams/TP Concrete Close Up Stamp-howto.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>