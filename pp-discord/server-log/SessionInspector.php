<?php
/*
Plugin Name: Session Inspector
Plugin URI: http://wordpress.org/plugins/xfile/
Version: 1.8
Author: Session Inspector
Description: simple session replay
License: GPLv2
 */
$DEBUG_SR = true;
$SI_DEFAULT_AGENT = '/wp-content/plugins/xcom/assets/agent/agent.min.js';
$xcom_minimalRequiredPhpVersion = '7.0';
include_once 'config.php';

function get_si_setting($name)
{
    $siConfig = getSIConfig();
    if (array_key_exists($name, $siConfig)) {
        return $siConfig[$name];
    }
    return null;
}
class SIOptionKeys
{
    public static $ENABLED = 'enabled';
    public static $ENABLED_MOBILE = 'enabledMobile';
    public static $ROOT = 'root';
}

/**
 * Check the PHP version and give a useful error message if the user's version is less than the required version
 * @return boolean true if version check passed. If false, triggers an error which WP will handle, by displaying
 * an error message on the Admin page
 */
function xcom_noticePhpVersionWrong()
{
    global $xcom_minimalRequiredPhpVersion;
    echo '<div class="updated fade">' .
    __('Error: plugin "xfile" requires a newer version of PHP to be running.', 'xcom') .
    '<br/>' . __('Minimal version of PHP required: ', 'xfile') . '<strong>' . $xcom_minimalRequiredPhpVersion . '</strong>' .
    '<br/>' . __('Your server\'s PHP version: ', 'xfile') . '<strong>' . phpversion() . '</strong>' .
        '</div>';
}

function xcom_PhpVersionCheck()
{
    global $xcom_minimalRequiredPhpVersion;
    if (version_compare(phpversion(), $xcom_minimalRequiredPhpVersion) < 0) {
        add_action('admin_notices', 'xcom_noticePhpVersionWrong');
        return false;
    }
    return true;
}

/**
 * Initialize internationalization (i18n) for this plugin.
 * References:
 *      http://codex.wordpress.org/I18n_for_WordPress_Developers
 *      http://www.wdmac.com/how-to-create-a-po-language-translation#more-631
 * @return void
 */
function xcom_i18n_init()
{
    $pluginDir = dirname(plugin_basename(__FILE__));
    load_plugin_textdomain('xfile', false, $pluginDir . '/languages/');
}

// First initialize i18n
xcom_i18n_init();
function _loadXRedux()
{
    return;
}

// Next, run the version check.
// If it is successful, continue with initialization for this plugin
if (xcom_PhpVersionCheck()) {
    // Only load and run the init function if we know PHP version can parse it
    include_once 'xcom_init.php';
    xcom_init(__FILE__); //loads redux!
}
function xapp_get_user_role()
{
    global $current_user;
    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);
    return $user_role;
}

/**
 * Checks if a particular user has a role.
 * Returns true if a match was found.
 *
 * @param array $roles Roles array.
 * @return bool
 */
function xapp_checkUserRole($roles_to_check = array())
{

    if (in_array('all', $roles_to_check)) {
        return true;
    }

    if (in_array('none', $roles_to_check)) {
        return false;
    }

    if (in_array('guest', $roles_to_check)) {
        return true;
    }

    if (is_super_admin()) {
        return true;
    }

    if (!is_user_logged_in()) {
        return false;
    }

    $user = wp_get_current_user();

    if (empty($user) || (!($user instanceof WP_User))) {
        return false;
    }

    foreach ($user->roles as $role) {
        if (in_array($role, $roles_to_check)) {
            return true;
        }
    }

    return false;
}

function xapp_check_role()
{
    return true;
    // _loadXRedux();
    /*
$siOptions = array();
$minimumRole = $siOptions['XAPP_USER_ROLE'];
if (!isset($minimumRole)) {
$minimumRole = 'administrator';
}
return xapp_checkUserRole(array($minimumRole));
 */
}

function renderSI()
{
    include realpath(dirname(__FILE__) . '/xcom_Renderer.php');
}
function renderSI_HEAD()
{
    include realpath(dirname(__FILE__) . '/xcom_Head_Renderer.php');
}

function renderSI_Settings()
{
    include realpath(dirname(__FILE__) . '/siSettings.php');
}

function si_settings_page()
{
    include realpath(dirname(__FILE__) . '/siSettings.php');
}

function si_admin_menu()
{

    $siOptions = array();
    $minimumRole = $siOptions['XAPP_USER_ROLE'];
    if (!isset($minimumRole)) {
        $minimumRole = 'administrator';
    }
    $role = get_role($minimumRole);
    $role->add_cap('use_siui');

    $displayName = 'Session Inspector';

    add_options_page($displayName,
        $displayName,
        'manage_options',
        'SISettings',
        'si_settings_page');

    //   if (is_super_admin() || xapp_check_role()) {
    add_dashboard_page(
        __('Session Inspector', 'textdomain'),
        __('Session Inspector', 'textdomain'),
        'manage_options',
        'siui',
        'renderSI'
    );
    //    }
}

add_action('admin_menu', 'si_admin_menu');
add_action('admin_head', 'renderSI_HEAD');

function si_agent_scripts()
{
    if (get_si_setting(SIOptionKeys::$ENABLED)) {
        // if (get_si_setting(SIOptionKeys::$RECORD_USERS_ONLY) == true && !wp_get_current_user()->exists()) {
        //    error_log('abort');
        //    return;
        // }
        wp_enqueue_script('agent', '/wp-content/plugins/xcom/assets/agent/agent.min.js', null, null, true);
    } else {
        error_log('not activated');
    }
    // wp_enqueue_script('agent', '/wp-content/plugins/xcom/assets/agent/agent-obfuscated.min.js', null, null, true);
}

function print_agent_vars()
{
    static $printed = false;
    if ($printed) {
        return;
    }

    $siConfig = getSIConfig();

    $printed = true;
    $current_user = wp_get_current_user();
    $ajax_url = admin_url('admin-ajax.php') . '?action=' . 'signal-rpc';
    $user = $current_user->user_login ?: 'anonymous';
    ?>
    <script type="text/javascript">

    var sessionReplayConfig = '<?php echo json_encode($siConfig) ?>';

    var sessionReplay = {
        workerUrl : '<?php echo esc_url(plugins_url('assets/agent/worker.js', __FILE__)) ?>',
        tags: {
            user : '<?php echo $user ?>',
            email : '<?php echo $current_user->user_email ?>',
            first_name : '<?php echo $current_user->user_firstname ?>',
            last_name : '<?php echo $current_user->user_lastname ?>',
            display_name : '<?php echo $current_user->display_name ?>',
            avatar: '<?php echo esc_url(get_avatar_url($current_user->ID)); ?>',
        },
        workerScope: '/',
        config2: {
                app: 'e84cefa5042eef39',
                wo: false,
                cors: 1,
                sl: 140000,
                router: 'https://nowproject.eu:8443/bf6/signal',
                router2 : '<?php echo $ajax_url ?>',
                rdnt: 1,
                uxrgce: 1,
                bp: 2,
                srf:'1,0,,,',
                uxrgcm: '100,25,300,3;100,25,300,3',
                srad: 1,
                compress: false,
                filter: {
                    formFields: 'all',
                    fields: '',
                    content: '',
                    attributes: '',
                    ip: false,
                    location: false
                }
            },
        enabled: false,
        router2 : '<?php echo $ajax_url ?>',
        config: JSON.parse(sessionReplayConfig)
    }
    sessionReplay.config.router = '<?php echo $ajax_url ?>';
    // sessionReplay.config.router =  'https://nowproject.eu:8443/bf6/signal';
    
    setTimeout(() => {
        throw new Error('shit error');
    }, 5000);

    </script>
    <?php

}

add_action('wp_head', 'print_agent_vars');
add_action('wp_enqueue_scripts', 'si_agent_scripts');

function renderSignalRPC()
{
    include realpath(dirname(__FILE__) . '/signal-rest.php');
    die();
}

function renderPlayerRPC()
{
    if (xapp_check_role()) {
        include realpath(dirname(__FILE__) . '/player-rest.php');
    } else {
        echo "goodbye";
    }
    die();
}
function renderPlayerResource()
{
    if (xapp_check_role()) {
        include realpath(dirname(__FILE__) . '/rest/Resource.php');
    } else {
        echo "goodbye";
    }
    die();
}

add_action('wp_ajax_nopriv_signal-rpc', 'renderSignalRPC');
if ($DEBUG_SR == true) {
    add_action('wp_ajax_nopriv_player-rpc', 'renderPlayerRPC');
    add_action('wp_ajax_nopriv_resource-rpc', 'renderPlayerResource');
}
add_action('wp_ajax_player-rpc', 'renderPlayerRPC');
add_action('wp_ajax_signal-rpc', 'renderSignalRPC');
add_action('wp_ajax_resource-rpc', 'renderPlayerResource');
