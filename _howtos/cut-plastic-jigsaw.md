---
image: "/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-2.jpg"
title: "Cut plastic: Jigsaw"
tagline: ""
description: "The jigsaw is one of the most accessible professional tools to cut. It’s also one of the more versatile ones offering straight cuts to curves and even the possibility to make inner holes. Here we’re going to explain some tips for applying this technique on plastic."
keywords: "PS,PP,melting,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PS"
- "PP"
- "melting"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Stay safe</h4>
<div class="step-text">
  When talking about safety we are only referring to precautions about working specifically with plastic, as it’s our thing. We assume that working with the jigsaw needs a certain level of expertise so please take all the precautions related with how the tools work.<br />
  <br />
  When cutting with the jigsaw there is danger of overheating and melting the plastic. If this happens some bad fumes could be released. <br />
  <br />
  Special attention on plastics like PS and PVC! So in order to work safer make sure to use a gas mask with ABEK filters to prevent inhaling possible toxic fumes. An easy way to identify them is to check if the filter has four colour lines (brown, grey, yellow &amp; green).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-4.jpg">
        <img class="step-image" src="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Collect the chips</h4>
<div class="step-text">
  While cutting plastic with this tool, lots of chips will end up flying around the machine.<br />
  <br />
  Try to keep your working area clean and collect the chips as they can be used for further recycling. Like this you help reducing the amount of waste generated in your workspace and you save the chips from ending up as micro plastic in our environment! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-3.jpg">
        <img class="step-image" src="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Choose the right blade</h4>
<div class="step-text">
  When selecting the blade several factors have to be taken in account.<br />
  <br />
  Type: We prefer using High-Carbon Steel blades (HCS) and High-Speed Steel blades (HSS) for these jobs.<br />
  <br />
  Teeth per inch (TPI): For brittle materials like PS will need a relatively big amount of TPI, around 10-13, in order to allow a safe and clean cut. A smaller amount will make the work faster but rougher.<br />
  For tough plastics like HDPE and PP we don’t need that many TPI, around 6 is good. An excessive amount of teeth will make it unnecessarily harder.<br />
  <br />
  Shape/Blade direction: The blade can point upward or downwards. It produces a smoother cut on the side of the piece that it’s cutting towards. And sometimes in brittle materials can produce chipping on the other side.<br />
  We recommend to use blades like the ones in the image, where the cutting angle of the teeth is negative or close to zero.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-1.jpg">
        <img class="step-image" src="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Cutting settings</h4>
<div class="step-text">
  With our jigsaw we can choose between 5 different levels of cutting speed. For cutting plastic we want to set the speed high enough to cut easily but not low enough to prevent the plastic from melting. That’s why we set it on 4.<br />
  <br />
  For the orbital action cutting we set it at 1 or 2 for pieces up to 10mm for PS, and for pieces up to 20mm for HDPE and PP. Higher orbital actions will make the job faster and easier but won’t always guarantee a clean cut.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-5.jpg">
        <img class="step-image" src="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-6.jpg">
        <img class="step-image" src="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Secure the piece firmly</h4>
<div class="step-text">
  Vibrations will make the job unsafe and the cut inaccurate.<br />
  <br />
  Therefore make sure to clamp the piece firmly to the workbench. Use clamps with a rubberised jaw to prevent any scratches or marks on the sheet.<br />
  <br />
  Cut close to the workbench, to avoid as much vibration as possible and enable a stable cut.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-8.jpg">
        <img class="step-image" src="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-8.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Add coolant if needed</h4>
<div class="step-text">
  With thicker pieces (&gt; 10mm) the material sometimes keeps melting while cutting, no matter what blade and setting you use. <br />
  <br />
  In this case, adding cooling while cutting the piece helps. We like using an air compressor, which blows fresh air to the blade so it doesn’t overheat too easily.<br />
  <br />
  Using both machines at the same time may be a bit difficult though, so make sure to ask for help if needed.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-7.jpg">
        <img class="step-image" src="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - What makes a clean cut</h4>
<div class="step-text">
  Generally, when cutting plastic, the biggest issue comes with overheating the material. That generates undesired melting.<br />
  <br />
  With the right blade and cutting settings you can avoid overheating. The explained settings proved useful for us and should be a good starting point, but it might slightly change with different plastic types and tools - so feel free to test it out yourself! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-9.jpg">
        <img class="step-image" src="/howtos/cut-plastic-jigsaw/how-to-cut-jigsaw-9.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>