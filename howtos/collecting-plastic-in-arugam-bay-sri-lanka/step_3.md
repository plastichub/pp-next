### Our space 

 We work in a big space in the centre of Arugam Bay. The space consists of two 20ft shipping containers and has easy access to the main road where lots of tourists wander around between a mojito and a surf session. This offers great visibility for us to educate everyone about the problem and the alternatives. 
