### Assemble a team 

 We’ve heard it over and over. People are what makes a project succeed or fail. Choose carefully and create a team of passionate people that want to change the world and are not afraid of sleepless nights. 