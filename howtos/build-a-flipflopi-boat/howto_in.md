### Build a 'Flipflopi' boat 


The Flipflopi is a sailing boat made from 100% recycled plastic and flip-flops collected from the streets and beaches in Kenya. 

Here we want to share how it was made and what we learned on the way.