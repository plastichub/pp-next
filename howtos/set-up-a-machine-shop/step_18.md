### Other specialists 

 On top of the Machine Shop’s expertise, there are a number of other technical expertise that you can find in your local industrial network, which you might need at some point.

Try to map out machining specialists, lasercut and waterjet services, mould makers, CNC gurus and shops selling specific components. These are very specialised sectors that can offer you high precision, better prices and less hassle. 

Also make sure to note and keep together all the contacts you find. Makes it easier for you to get back to them and also easier to share with your team and your local recycling network.