---
image: "/howtos/set-up-a-community-point/cover-community-3.jpg"
title: "Set up a Community Point"
tagline: ""
description: "In this How-to we’re going to guide you through the steps to build set up you local Community Point. This is just as important as setting up the workspaces to transform plastic, as it is absolutely crucial to involve as many people in your area as possible.<br /><br />Download files: <br /> &lt;a href=&quot;https://cutt.ly/starterkit-community&quot;&gt;https://cutt.ly/starterkit-community&lt;/a&gt; <br /><br />1-3: Intro<br />4-6: Learn<br />7-11: Setup<br />12-19: Run<br />20-22: Share"
keywords: "starterkit"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "starterkit"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Role in the universe</h4>
<div class="step-text">
  Community Points are the glue of the Precious Plastic Universe. They help strengthening the collaboration between the existing recycling spaces as well as involving more and more people from the general public.<br />
  <br />
  Moreover they’re closely connected to Collection Points as they can serve as great help in finding new people to collect their plastic. <br />
  <br />
  Community Points should be run as a collective but if you’re alone in your area you can also start solo.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/community points - roles-01.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/community points - roles-01.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Outcomes</h4>
<div class="step-text">
  The outcome of a Community Point is a active and strong local network of people and places collecting plastic, building machines, shredding and melting and inspire others to get involved.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/community-badges.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/community-badges.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Is this for you?</h4>
<div class="step-text">
  For this role you will have to be very communicative and social. Ideally you like talking to people about the Precious Plastic online and in real life. So access to a phone and computer to use all the platforms properly is crucial.<br />
  <br />
  As a community builder it’s also important to be organised and good at keeping an overview of the area and your local network. Your goal is to strengthen the existing community and expand to new audiences.<br />
  <br />
  How you can go about sparking your community will vary depending on your context, but there are a number of tools you can use in order to get started.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step2-2.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step2-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Learn about Precious Plastic</h4>
<div class="step-text">
  As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions. Especially as a Community Point you will have to talk and answer questions about the project a lot!<br />
  <br />
  If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.<br />
  👉 preciousplastic.com<br />
  👉 community.preciousplastic.com<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.<br />
  👉 tiny.cc/preciousplasticuniverse
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/learn-precious-plastic-universe.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/learn-precious-plastic-universe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Check out your area</h4>
<div class="step-text">
  Get an overview of who and what is already existing in your area. <br />
  <br />
  Have a look on the Precious Plastic Map to see the activity around you and if there are pins you can get in contact with. <br />
  <br />
  👉 community.preciousplastic.com/map<br />
  <br />
  Make sure not to jam the local network - if there is a Community Point in your area already, get in touch and have a look how you can work together, to keep ONE clear contact point for the surrounding people of your network :)<br />
  <br />
  Additionally, you can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step4-map-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Check out the Discord</h4>
<div class="step-text">
  Precious Plastic also uses Discord to connect people around the world. We have channels for different countries, roles and topics. <br />
  <br />
  Make sure to see what’s already going on in your country and let people know you want to get involved.<br />
  <br />
  👉 discordapp.com/invite/rnx7m4t
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step6.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Create a Community Point pin!</h4>
<div class="step-text">
  If there is already a Community Point near you on the Precious Plastic Map, get in touch with the group and and see how you can join and help. Better to collaborate than to do double work. <br />
  <br />
  If there is no Community Point in your area yet, it’s time to create one. Create an account on the Community Platform and put a pin on the map. This way people can find and contact you.<br />
  <br />
  👉 community.preciousplastic.com/sign-up<br />
  <br />
  Have a look at these profile guidelines to get help with setting up your profile.<br />
  👉 tiny.cc/profile-guidelines
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step7-2.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step7-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step7-1.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step7-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Visit people and places</h4>
<div class="step-text">
  It&#39;s nice and creates much stronger relationships to get to know people in real life. Makes it more human and fun.<br />
  <br />
  Do a little search of your area and get the chance to get in touch and visit people working on Precious Plastic around you (Collection Points, Machine Shops, Shredder Workspaces etc..).<br />
  <br />
  This can give you a good understanding of the needs, problems and circumstances of the different recycling spaces in your area.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/cb-visit-machine shop-2.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/cb-visit-machine shop-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/cb-visit-injection-2.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/cb-visit-injection-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/CB-visit-waste1500.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/CB-visit-waste1500.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - IMPORTANT: Your contact list!</h4>
<div class="step-text">
  You can use our contact list tool as a starting point to gather all your contacts and keep an overview. Keeping this document organised and updated will be super helpful to make the connections between people in your community, so make sure to take care of it!<br />
  <br />
  If you have another good way of gathering and organising the contacts and details, we&#39;re happy to learn about it :)<br />
  <br />
  👉 tiny.cc/community-contactlist
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step9-contactlist.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step9-contactlist.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Start chatting</h4>
<div class="step-text">
  It’s good practice to setup other local communication channels to fit your needs (Telegram, Whatsapp, etc..) Use this tool to keep ongoing communication that is relevant to your area.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step10.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step10.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Start meeting each other</h4>
<div class="step-text">
  Alright. Now it’s time to do something with those contacts. <br />
  <br />
  A good way to bring people together is to organise a meetup. This can be with existing recycling spaces to see how to grow together or with new people to start the local recycling network.<br />
  <br />
  Check out our tips for organising a meetup to get some ideas about how to do this.<br />
  <br />
  👉 tiny.cc/organise-meetup
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/SK Community Present 2.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/SK Community Present 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/SK Community Group Chat 2.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/SK Community Group Chat 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/SK Community Contact List.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/SK Community Contact List.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Start collaborating</h4>
<div class="step-text">
  Once you’ve made the connections and made a plan how to work together and who will do what, the group can start collaborating.<br />
  <br />
  The goal here is that people know where they can bring their plastic, where to shred plastic, where to melt plastic and where to finally sell and buy the final products.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step12-1.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step12-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Be the local Bazar</h4>
<div class="step-text">
  As the central point of your recycling community, you could order badges, flags, stamps, posters in bulk from the Bazar (as local as possible) and distribute them locally as needed. <br />
  <br />
  Saving lots of CO2, while making things more accessible for the local community.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step13-1.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step13-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Keep connecting</h4>
<div class="step-text">
  Eventually, your community will collect and transform plastic locally. Nice!<br />
  <br />
  Now it’s all about keeping people involved and motivated. Be available for questions, welcome newcomers and connect people to the relevant contacts and places.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/SK Community-greet.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/SK Community-greet.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Keep meeting</h4>
<div class="step-text">
  It can also be good to meet up more often to update each other and give the opportunity for newcomers to join.<br />
  <br />
  Try you can find a place and time where you can meet more regularly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/cb-meetuptimes-2.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/cb-meetuptimes-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Keep growing</h4>
<div class="step-text">
  The more people get to know about Precious Plastic and participate, the better it can run and succeed.<br />
  <br />
  So constantly keep your eyes open for new people and partners (and make sure they are in your contact list and on the map), so everyone in your community can find them.<br />
  <br />
  To help you in the process we’ve prepared some graphic materials you can use as a starting point.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step17.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step17.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Create How-to's!</h4>
<div class="step-text">
  Share to the world how you run your Community Point so other people can learn from you and start using your solutions to grow their network. Make sure to document and share your best processes and techniques.<br />
  <br />
  Also stay up to date what&#39;s going on in your network and try to motivate and help your group to create How-to&#39;s of their successful techniques, hacks and products.<br />
  <br />
  👉 community.preciousplastic.com/howto
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step21-1.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step21-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step21-2.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step21-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Keep inspiring</h4>
<div class="step-text">
  Public speaking is a great and effective way to inspire people to participate. This can keep your group motivated and open the doors for new collaborations.<br />
  <br />
  In the Download package you can find a sample presentation which you can use and modify for your contexts to talk about Precious Plastic and your local community.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/SK Community-presenting.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/SK Community-presenting.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/cb-presentation.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/cb-presentation.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Inspire with samples</h4>
<div class="step-text">
  You might also want to put together a little sample kit of product and material samples from the community, that can help you to explain and get people excited.<br />
  <br />
  In best case you can gather some good products and samples from your local workspaces - if they are not ready yet, you can use the Bazar to find products from the global community.<br />
  <br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-samples.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-samples.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Be part of the global community</h4>
<div class="step-text">
  Participate and interact in this community to learn and grow together.<br />
  <br />
  Make sure people in your local network have their pins on the map so they can be found more easily.<br />
  <br />
  Maybe help them set up, sell on the Precious Plastic Bazar and make it more visible and accessible to buy their machines, products and services.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step20.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step20.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Learn with other communities</h4>
<div class="step-text">
  It can also help if you connect with other Community Points to see how they are working and make sure to document and share what worked well in your community. <br />
  <br />
  This way you can create and participate in a powerful local and international network!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step22.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step22.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Good things take time :)</h4>
<div class="step-text">
  Building a community isn&#39;t something which happens over night. It’s a big thing, but that shouldn’t discourage you.<br />
  You can start with your friend and friend’s friend. Step by step.<br />
  <br />
  Good things take time - and you have to start somewhere and plant the seed.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/communitypoint-step23.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/communitypoint-step23.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Download and start!</h4>
<div class="step-text">
  Excited? Ready to start? Let’s go!<br />
  <br />
  Download your Community Point package with your precious files and start your recycling journey!<br />
  <br />
  👉 <a href="https://cutt.ly/starterkit-community">https://cutt.ly/starterkit-community</a> 👈
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-community-point/Community Starter Kit.jpg">
        <img class="step-image" src="/howtos/set-up-a-community-point/Community Starter Kit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>