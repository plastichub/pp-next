### Create How-tos! 

 Share to the world how you run your Extrusion Workspace so other people can learn from you and start using your solution to tackle the plastic problem. 

Make sure to only create How-tos for your best processes and techniques, not tryouts or one-offs. This can also help you create a name for yourself in the Precious Plastic community.