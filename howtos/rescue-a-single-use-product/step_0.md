### Research 

 Have a walk through your local area to find places where they use single-use products. Choose one that you are going to save from the waste. Observe how the product gets used? What is the reason for using this product? Are you able to clean it easily? 

To get a clear overview you can write them all down, so you can start to create ideas based on this information. 

This can be, for example, ice-cream spoons from local ice-cream shops as they are very common and already clean (licked :). Be creative and find some single-use products that you are able to save. 
