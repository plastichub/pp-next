### Welding mountain brackets 

 As the next step, we are going to weld the mounting brackets to both sides of the mold. Before doing that make sure the brackets match with your plate and the center of your beam matches the center of the plate. 

You can find all the information you need to make a plate here:

Related link:
Extruded beams 👉 tiny.cc/extrude-beams

Let’s proceed: Drill the holes on the brackets before welding. 
We recommend to weld along the entire sheet's surface. The first welds were too small and the plastic flew out of the mold, so as pictures show, we had to weld another piece on top of the previous ones.
