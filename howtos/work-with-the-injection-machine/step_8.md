### Opening the mould 

 The mould usually can be opened immediately after the injection process. Unscrew the mould clamping screws. Two flat headed screwdrivers can be used to equally separate the two mould halfs apart from each other (see figure 1).
	
After opening the mould, the part can be removed from the mould. Depending on the mould design it can be easier to remove the injection sprue first (see figure 2). So all the connected parts come out at the same time. 

Be very careful in removing the part from the mould and try to avoid the use of any hard (metal) tools within the cavity! They can scratch and damage the mould permanently. A damaged mould requires time intensive re-work by sanding and polishing or even closing the holes by welding and further machining.