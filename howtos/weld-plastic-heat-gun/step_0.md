### Stay safe 

 When talking about safety, we are only referring to precautions about working specifically with plastic, as it’s our thing. Working with machines like the table saw requires a certain level of expertise, so please take all the precautions related with how the machines work.

As we are melting plastic, bad fumes can be released. In order to work safer, make sure to use a gas mask with ABEK filters to prevent inhaling possibly toxic fumes. Special attention on plastics like PS and PVC. Also when handling with heated elements we recommend to wear working gloves.

Recommended safety equipment:
- ABEK mask
- gloves
