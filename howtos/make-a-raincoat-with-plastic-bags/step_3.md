### Make your base 

 Find the biggest plastic sheet you have, and make a base out of it. Foils from construction waste are often large and good for this purpose. If you don’t have something such as this to hand, then you can cut down the sides of a bag and spread it out flat on your working surface. 