---
image: /_howto/extrusion/frame/IMG_1007.JPG
category: "extrusion"
title: "Frame"
tagline: ""
description: ""

---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_0990.JPG" _target="_blank">
        <img id="IMG_0990.JPG" src="./IMG_0990.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0991.JPG" _target="_blank">
        <img id="IMG_0991.JPG" src="./IMG_0991.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0995.JPG" _target="_blank">
        <img id="IMG_0995.JPG" src="./IMG_0995.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0996.JPG" _target="_blank">
        <img id="IMG_0996.JPG" src="./IMG_0996.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0997.JPG" _target="_blank">
        <img id="IMG_0997.JPG" src="./IMG_0997.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0998.JPG" _target="_blank">
        <img id="IMG_0998.JPG" src="./IMG_0998.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0999.JPG" _target="_blank">
        <img id="IMG_0999.JPG" src="./IMG_0999.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1000.JPG" _target="_blank">
        <img id="IMG_1000.JPG" src="./IMG_1000.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1001.JPG" _target="_blank">
        <img id="IMG_1001.JPG" src="./IMG_1001.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1002.JPG" _target="_blank">
        <img id="IMG_1002.JPG" src="./IMG_1002.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1003.JPG" _target="_blank">
        <img id="IMG_1003.JPG" src="./IMG_1003.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1004.JPG" _target="_blank">
        <img id="IMG_1004.JPG" src="./IMG_1004.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1005.JPG" _target="_blank">
        <img id="IMG_1005.JPG" src="./IMG_1005.JPG" width="100%" />
    </a>
    <span class="thumb-label">13. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1006.JPG" _target="_blank">
        <img id="IMG_1006.JPG" src="./IMG_1006.JPG" width="100%" />
    </a>
    <span class="thumb-label">14. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1007.JPG" _target="_blank">
        <img id="IMG_1007.JPG" src="./IMG_1007.JPG" width="100%" />
    </a>
    <span class="thumb-label">15. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

