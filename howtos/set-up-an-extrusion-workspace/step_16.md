### Safety 

 Always stay safe!

Of course, Extrusion machines get hot. And can cause a hazard in different ways.

So before starting to melt, please check out our safety video to learn about dangers and how to stay safe when working with plastic.

👉 http://tiny.cc/plastic-safety