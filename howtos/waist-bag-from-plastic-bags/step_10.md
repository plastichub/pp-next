### Final Notes 

 The bag is now ready to use. Adjust the straps and wear this fashionable bag with pride cause you made it yourself! The bag should be ok with a few raindrops but I would not advise carrying it in the rain or similar situations. Be ready also for all the questions about how you made it.

Also to note, the cool thing about the plastic bag leftovers is that these could be used again in the next plastic bag project.

Thank you for following this tutorial, hope to see your versions of the bags.