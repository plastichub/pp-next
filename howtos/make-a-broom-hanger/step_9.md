### Connect the mould 

 Fix the nozzle, the female and the male parts with a small press or locking pliers, and drill four 9/32” holes through the corners of both parts.
Close the mold and turn the ends to get an even surface between the male and the female parts. 

(Drawings page 12)