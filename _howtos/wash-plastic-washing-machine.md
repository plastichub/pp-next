---
image: "/howtos/wash-plastic-washing-machine/COVER.jpg.png"
title: "Wash Plastic: Washing Machine"
tagline: ""
description: "Different steps are necessary to make plastic clean. First is the prewashing after the plastic can be shredded, but it will still contain some oils, glue remains or even paper. To get rid of this we need water, soap, heat, and friction. Building a machine, which can do all of this takes time and costs a lot of money. So instead we are going to use a washing machine. It is accessible, cheap and integrates with a few modifications perfectly in an existing sand filter system."
keywords: "research,other machine,collection,washing"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "research"
- "other machine"
- "collection"
- "washing"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Why a washing machine?</h4>
<div class="step-text">
  A washing machine offers a commonly available machine with functions we can make use of. It provides heated water, which helps to remove oils and dissolves many contaminants. It creates friction through spinning, which scrubs plastic against each other and takes of dirt. It has multiple flushing cycles to get out of the old water and centrifugation helps to dry the plastic. Though we need to shred the plastic before because unshredded plastic has a huge volume and a washing machine has only a little space.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP0.5.jpg.png">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP0.5.jpg.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Get your materials</h4>
<div class="step-text">
  For the washing machine, we are going to need:<br />
  - a 120 liter HDPE barrel<br />
  - a washing machine with a rather big drum capacity<br />
  - a sand filter 👉 tiny.cc/wash-plastic-sandfilter<br />
  - 1” 24V solenoid valve<br />
  - flexible pipes (also according to the sand filter)<br />
  - relay 240V<br />
  - 240V to 24VAC Trafo<br />
  - 3 Switches<br />
  - cables etc.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP1.png">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Removing everything we don’t need</h4>
<div class="step-text">
  We pump water straight into the drum with our own pump, which means we will not need any of the mixers and can remove everything until the connection to the drum. The process depends on your machine but should be doable with basic tools.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP2.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Modifications on the machine</h4>
<div class="step-text">
  After clearing up the machine, we can start with our modifications.<br />
  First, attach the solenoid valve to the drum inlet and tighten it well. Here we will later connect the water pump.<br />
  Second, we need to connect to a signal from the machine to control when the pump will turn on and off, so it doesn’t have to run nonstop. In this case, we found the signal in a cable going to the machine’s small solenoid valve. Split the cable and connect your own.<br />
  ❗Careful here, this signal can be 220Volt! 💀<br />
  Now we guide out the cables from the valve and signal so we can connect it later to our electronics box. Once that’s done we cut a hole in the lid of the machine and put it back on.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP3.1.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP3.1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Electronics</h4>
<div class="step-text">
  This step is still under development and has to be taken fully on your own risk. In our case, it worked but the box would need a better layout design to make it safer. <br />
  What we are doing is:<br />
  The main switch which turns on/off everything, this means we also lead the power cable from the washing machine into this box. We have a switch that changes the state of the pump between always on and on on-demand. When the pump turns on, also the solenoid valve needs to open, so we reach that with a 220V to 24VAC transformer, which also interacts with the relay. It doesn’t open when the pump is in the function always on. I also moved the electronics from the pump into my box, to keep everything together.<br />
  If you also have a Pre-Washing Station with a heated water barrel going, then you should also move the PID controller and a switch for it into this box. <br />
  ❗Again to emphasize, this is not a professional setup and you should act with care and knowledge.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP4.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP4.1.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP4.1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - The washing bag 1</h4>
<div class="step-text">
  We use shreds in the washing machine because this way we can process way more material at the same time. But just putting the shredded plastic in the drum would result in blocked pumps and create chaos. So we experimented with bags. Our design is made of a strong polyester textile, with strongly reinforced stitches and a good quality zipper. We recommend making it rather long so it is easier to stuff it into the machine when filled with shreds. Also, we added a strong mash in the bag so dirt and small particles can leave, to reach a better result.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP5.2.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP5.2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - The washing bag 2</h4>
<div class="step-text">
  Our bag is designed so you can just hang it straight beneath your shredder and put it in the machine afterward. The zipper is zipped up into a little pocket so it doesn’t open up while washing.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP5.4.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP5.4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Put everything together</h4>
<div class="step-text">
  Now you connect the machine with the sand filter and your barrel system. The machine has an inlet, where you connect the sand filter and an outlet which leads to the reservoir barrel, that’s it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/DSCF4779.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/DSCF4779.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Run your washing system</h4>
<div class="step-text">
  Now fill the bag with pre-washed shreds and put it in the machine. We recommend adding a little bit of laundry detergent, which helps to remove the last bits of oil. The settings of the machine depend on the grade of contamination. Usually, we use the 40-degree quick wash program. Afterward, hang it up so the last bit of moisture can go away.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP6.jpg.png">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP6.jpg.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/STEP5.1.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/STEP5.1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Turn it into something new!</h4>
<div class="step-text">
  Now you have your clean plastic and you can create some beautiful Precious Plastic products with it.<br />
  Though as mentioned this is not finished development, but already works and can give you a good starting point. Feel free to test and modify and share back if you find other good solutions!<br />
  ❗Be extra careful with the electronics as everything runs on 220 Volts and you have to deal with a lot of water.❗<br />
  But if everything works you have a very efficient system to process large amounts of dirty plastic for your workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-washing-machine/DSC05508edit.jpg">
        <img class="step-image" src="/howtos/wash-plastic-washing-machine/DSC05508edit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>