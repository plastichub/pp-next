### Prepare your machines 

 Ok, so before you start giving workshops, think about what kind of machine you would like to use in this context. What kind of workshop is it? How many people are coming and how much time do they have?
All the machines you use in public have to be safe against gross negligence. For a better performance, make sure that everything is clean, working properly and maybe you have some spare parts with you.
