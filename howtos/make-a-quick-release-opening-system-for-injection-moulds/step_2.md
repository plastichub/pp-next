### Cut the clamps 

 Cut your clamp to your needed length (with a hand saw or angle grinder) and then soften the sharp edges.

This is very important so to screw in the nut easily without damaging its thread.
