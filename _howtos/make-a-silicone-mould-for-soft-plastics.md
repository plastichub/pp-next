---
image: "/howtos/make-a-silicone-mould-for-soft-plastics/cover image.jpg"
title: "Make a Silicone Mould for Soft Plastics"
tagline: ""
description: "This tutorial will show you how to cast an object in silicone to use with soft plastics. Casting with silicone will allow you to remake intricate, delicate or unusual objects in plastic. <br /><br />You can get as experimental as you wish casting simples items such as a vase or something as extreme as a tree! Don’t worry you will be able to use this mould over and over again."
keywords: "compression,mould,LDPE,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "compression"
- "mould"
- "LDPE"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/chrystal">chrystal</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gathering the Tools </h4>
<div class="step-text">
  Before we begin make sure you have the right tools.<br />
  <br />
  You will need:<br />
  Casting silicone (equal part 1:1)<br />
  Measuring jug<br />
  Plastic stirrer<br />
  Take-away container (recycled of course!)<br />
  Clay<br />
  Knife<br />
  Your chosen object<br />
  <br />
  For this tutorial I have used 500ml of silicone for an object which measures (h)10x(w)2.5x(l)3cm.<br />
  Make sure you have a clean work space and all your tools are ready. Silicone sets fast and can be costly to waste
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/1.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Preparing Your Object</h4>
<div class="step-text">
  First clean your object of dirt or dust.<br />
  <br />
  Using clay place a small strip to the back of your object making sure the object sits flat when placed down—this will become the entry for your plastic once the mould is complete.<br />
  <br />
  Be sure to cover any wholes and neaten the edges with a knife cutting away any excess.<br />
  Place the object facing upwards onto the take-away container lid.<br />
  <br />
  Now you are ready to build the walls of your mould box.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/2a.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/2a.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/2b.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/2b.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/2c.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/2c.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Making the Mould Box </h4>
<div class="step-text">
  As take-away containers usually have uneven bases cut out the bottom of the container using a knife. You should be left with the container edges, the bottom and the lid. The container edges will be used as the walls of the box, the bottom will be used as an additional wall and the lid will become the bottom of your box.<br />
  <br />
  Place the walls of the container over the lid and object. Leave a 2-3cm gap between the object and the walls. If the walls are too far from the object use the additional wall (the cut out bottom) as an insert to get closer to the object. <br />
  The closer you are to your object the less silicone you waste!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/3.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/3b.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/3b.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/3c.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/3c.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Sealing the Mould Box</h4>
<div class="step-text">
  Once you have the right area around the object we need to seal the base of the walls with clay so the silicone does not escape. <br />
  <br />
  Roll the clay into a ball and then into thin noodles. Press the noodles along the edges off your walls inside and out. The mould box should now be silicone tight!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/82167547_2705923779483259_4001545713136697344_n.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/82167547_2705923779483259_4001545713136697344_n.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/82796602_1074520712892040_3173552120993415168_n.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/82796602_1074520712892040_3173552120993415168_n.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/83145938_155783545857514_2988962402365603840_n.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/83145938_155783545857514_2988962402365603840_n.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Preparing and Mixing the Silicone</h4>
<div class="step-text">
  The hardest part is over! Now it&#39;s time to prepare the silicone for pouring.<br />
  <br />
  Preparation:<br />
  Line up the measuring jug, the silicone bottles (A) and (B), the stirrer and the mould box. Remember you must work fast before the silicone sets so you must have everything ready to go.<br />
  Mixing:<br />
  Add 250ml of part A into the measuring jug, then add 250ml of B. This should equal 500ml of silicone in the jug.<br />
  Now quickly mix the silicone with your stirrer. Be sure that the mixture is thoroughly mixed otherwise your silicone will not set properly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/5a.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/5a.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/4a.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/4a.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Pouring the Silicone & Clean Up</h4>
<div class="step-text">
  Pouring:<br />
  Concentrate on the highest point and the centre of the object—pour directly onto the most detailed section first, then moving around the object making sure the silicone reaches every part of the surface and the object is evenly coated.<br />
  Leave the silicone to set (approx 30 mins)<br />
  <br />
  Cleanup:<br />
  Leave the excess silicone on your jug and stirrer. Let it to dry fro 30 mins. Once dry the silicone can be pealed off leaving the jug and stirrer perfectly clean.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/4b.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/4b.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/5c.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/5c.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/82850774_205837740450973_5169312273985961984_n.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/82850774_205837740450973_5169312273985961984_n.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Releasing the Mould</h4>
<div class="step-text">
  After 30mins or after the silicone stops being sticky you may release your mould. To do this you must remove the clay from the edges of your container and silicone mould. Once the clay is removed from the container and the silicone, stretch the sides of the mould to remove the object.<br />
  <br />
  Keep the clay and the plastic container as you may use them again.<br />
  <br />
  Once the Object is removed you are ready to use the mould!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/6a.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/6a.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/82266669_117159946211447_3602648508101820416_n.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/82266669_117159946211447_3602648508101820416_n.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/6c.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/6c.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Using the Mould</h4>
<div class="step-text">
  Once the mould is released you are ready to begin using it.<br />
  <br />
  Please note; this mould is suitable for soft plastics. This mould is best when plastic is hand-pressed into the mould. Be sure that you are using heat and chemical resistant gloves when handling the plastic. <br />
  <br />
  For this mould I have reused plastic from another project to melt and press into my mould. <br />
  You may need to sand or cut off some messy edges to complete the look you want.<br />
  <br />
  Silicone will capture any surface as will your plastic when being moulded so go big or go home!<br />
  <br />
  <br />
  Note: I have only used soft plastics (LDPE &amp; HDPE) with this technique but please feel free to experiment.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/82511252_956938401367282_4234002606055227392_n.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/82511252_956938401367282_4234002606055227392_n.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/82527092_114154206576965_6037856426192273408_n.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/82527092_114154206576965_6037856426192273408_n.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-silicone-mould-for-soft-plastics/82905818_164061221524516_5612330106094616576_n.jpg">
        <img class="step-image" src="/howtos/make-a-silicone-mould-for-soft-plastics/82905818_164061221524516_5612330106094616576_n.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>