### Build it, hack it, share it! 

 If you have any other ideas, improvements or hacks for this machine - make sure to share them with the community.
You can do this through a how-to, discord or instagram!

Have fun building :)