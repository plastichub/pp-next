### Finishing and Using 

 Once the shredder is installed and the chain attached its time to use the pedal power to shred some plastic! 
It can be used to shred all sorts of plastic the best proved to be HDPE as this was softer and easier to shred when pedalling. The shredded plastic can then be used to create new plastic inventions such as simple flat cladding panels. 