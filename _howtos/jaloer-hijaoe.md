---
image: "/howtos/jaloer-hijaoe/IMG_1882.JPG"
title: "Jaloer Hijaoe"
tagline: ""
description: "I just started the Jaloer Hijaoe community with a garbage bank where I exchanged plastic with 1 tree seedling"
keywords: "product,PP,HDPE,LDPE"
tags:
- "product"
- "PP"
- "HDPE"
- "LDPE"
---
<h4 id="summary">{{page.title}}</h4>
{{ page.description }}
<h4 class="step-title" id="step-1">Step 1 - Plastic waste = 1 tree seed</h4>
<div class="step-text">
  the plastic trash that you exchange will be processed into a handicraft through the Jaloer Hijaoe waste bank
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/jaloer-hijaoe/15933446276221827623295180951970.jpg">
        <img class="step-image" src="/howtos/jaloer-hijaoe/15933446276221827623295180951970.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/jaloer-hijaoe/20200620_125735.jpg">
        <img class="step-image" src="/howtos/jaloer-hijaoe/20200620_125735.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/jaloer-hijaoe/IMG-20200621-WA0052.jpg">
        <img class="step-image" src="/howtos/jaloer-hijaoe/IMG-20200621-WA0052.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
{% if page.related %}
<h3 id="related"> Related </h3>
{% endif %}
---
{% if page.tags %}
{% endif %}
{% if page.resources %}
{% endif %}