### Business Plan Template 

 To help you pitch your idea to potential partners, financial institutions or investors we made a Business Plan Template (and a specific example for the Extrusion Workspace) for you to use.

This template helps you to talk the business language and should help you access the necessary money to begin.  

For more explanation check out the video in the Academy:
👉 http://tiny.cc/business-plan-calculator
