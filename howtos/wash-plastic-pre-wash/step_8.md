### Extra barrel with a heating element 

 Some contaminants, especially oil and fat, are difficult to remove without warm water. Furthermore it’s very useful to have a second barrel to pump in the water from the first barrel if you need to clean it or reach plastic on the bottom. 

We prepare this barrel in exactly the same way as in step 3, but add a heating element and a PID controller. Heating elements can come in many forms, for example from deep fryers. You need to find solid, waterproof way to connect it to the barrel.

Afterwards, place the thermometer from the PID in the tank and connect the pipes to the system.