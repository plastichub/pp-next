**Product Resources**

- [3D Preview](${abs_url}/${product_rel}resources/edrawings.html)
- [3D Preview - 4kW | Auto-Plunger](${abs_url}/${product_rel}resources/edrawings_4kW.html)
- [Download](${download})
- [Product Page](${product_page})
