
/*
Automatic injecting extruder (forward rotation)
*/

void forward()
{
	while (Xcurrentrotations < rotationsfor)
	{
		for (int i = 0; i < Xstepsinfor; i++)
		{
			if ((i%40) == 1 && Xdt > Xdtend)
			{
				Xdt = Xdt -10;
			}
			digitalWrite(DIR3, HIGH);
			digitalWrite(EN3, HIGH);
			digitalWrite(PUL3, LOW);
			delayMicroseconds(Xdt);
			digitalWrite(PUL3, HIGH);
			delayMicroseconds(Xdt);
		}
		Xcurrentrotations++;
		Serial.println(Xcurrentrotations);
	}
	Xcurrentrotations = 0;
	while (Xcurrentrotations < 6)
	{
		for (int i = 0; i < Xstepsslowingdown; i++)
		{
			if ((i % 50) == 1 && Xdt < Xdtref)
			{
				Xdt = Xdt + 50;
			}
			digitalWrite(DIR3, HIGH);
			digitalWrite(EN3, HIGH);
			digitalWrite(PUL3, LOW);
			delayMicroseconds(Xdt);
			digitalWrite(PUL3, HIGH);
			delayMicroseconds(Xdt);
		}
		Xcurrentrotations++;
		Serial.println(Xcurrentrotations);
	}
	Xcurrentrotations = 0;
}

