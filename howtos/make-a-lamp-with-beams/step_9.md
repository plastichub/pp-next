### Finishing up 

 You made it! Before extruding make sure you clean both parts of the mold and put silicon oil to facilitate the extraction of the beam. 
Assemble the 2 parts mould with nuts and bolts and connect your mold to the nozzle.

For better finishing, we recommend preheating your mould with heating elements or in the oven, or both!  

Now it’s time to extrude and test your mould! 

Relevant links:
Make glass-like beams 👉 tiny.cc/make-glasslike-beams