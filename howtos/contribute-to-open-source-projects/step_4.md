### Fork the repository 

 This is done by clicking the fork button while signed in on the repository you’re interested in contributing to. You’ll know it’s yours as you’ll have your account name in the URL and at the top of the page.