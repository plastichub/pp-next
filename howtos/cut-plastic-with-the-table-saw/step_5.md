### Reduce the vibrations 

 Give the piece enough stability to embrace the cut without vibrations. This will not only guarantee a safer job but will also give you the best results.

You can do so by clamping the piece to the table and placing your hands safely where the clamp cannot reach. 
