### CNC - Prepare the file  

 First of all, decide if you want to customise your carabiners or not. If you are already investing in the CNC cutting, it can make sense to take advantage and add some cool engravings.

To make it even more functional, you can add your logo, web page and even the plastic type you will use (PP in this case), so then you’ll not need to stamp it afterwards, one step less! To know more about tips and tricks for CNC moulds go to the academy section.

Here some tips on how to make injection moulds:
👉 tiny.cc/injection-moulds

