### Extrude the stripe 

 This is a delicate part and needs improvement as you need to pull the plastic out so that the thickness and flow is even. Make your motor rotate at the slowest speed possible.
Because of changes in the thickness and rate of cooling your stripe might twist
We heat it up back with the hot air gun and pass it inside our metal mangle. This will straighten the band. Don#t expect miracles, you will keep the concave shape. Although in the end it makes no difference with our design
Based on the work on the Badger shield
https://www.delve.com/assets/documents/OPEN-SOURCE-FACE-SHIELD-DRAWING-v1.PDF
We can see that ideally the contact with the head should be 13"/ 33 cm. Cut to length
Here we have used HDPE from ocean plastic we regularly recover on the local beaches. We recommend HDPE as it is easier to source and also low temperature to melt.