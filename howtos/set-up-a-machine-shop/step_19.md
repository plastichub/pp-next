### Sell 

 You can now make great machines in series. At this point, it is crucial to find people and organisations that will buy your recycling machines. 

Put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. You could alternatively also try to sell the machines locally but this might be a little harder.

*try to avoid shipping machines across the planet as this increases CO2 pollution

👉 bazar.preciousplastic.com
