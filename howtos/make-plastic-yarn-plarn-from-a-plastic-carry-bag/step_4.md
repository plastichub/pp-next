### Making the Diagonal Cuts 

 Image 7:

Unfold the strips of the bag.


Image 8:

Open out & unfold the margin area.


Image 9:

Instead if cutting parallelly, we will be cutting diagonally so that we get a continuous spiral.
Start by cutting the the first end off, the cut that is towards you.
Now cut from the 2nd cut towards you to the 1st cut on the other side, away from you. 
Keep doing this until you reach the end. Cut the last end off.


And the yarn is ready, hooray! 
Ball it up or wind it around a bobbin.
