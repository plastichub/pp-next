### Outcomes 

 The outcome of an Injection Workspace is recycled plastic products. These products can vary greatly depending on needs but they’re generally of high precision and can be made in series creating small productions for customers.  