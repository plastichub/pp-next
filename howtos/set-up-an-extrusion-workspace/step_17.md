### Buy shredded plastic  

 Buy shredded plastic from your local Shredder Workspace. Make sure to specify your preferred shreds size (small, medium or large) and to have a variety of colours and types.