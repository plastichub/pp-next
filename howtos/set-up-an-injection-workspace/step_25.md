### Create your profile 

 If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform to connect with people. Follow this link and sign up with your email, pick your role, put your pin on the map and upload nice pics to show the world what you’re doing.
