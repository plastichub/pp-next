### Gather materials and tools 

 To make a beam mould you will need:
- Metal tube (wall thickness preferably 3mm or more)
- Angle iron
- Metal sheet (3mm or more)
- Threaded pipe or fitting (BSPT size of your nozzle)
- Angle grinder or metal saw
- Welding machine
- File
- Drill
- M8 or M10 nuts and bolts
