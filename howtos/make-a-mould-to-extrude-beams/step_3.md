### Cut and plan plate 

 We can now begin with the plate. Use the size of your beam section to plan the rough size of your mounting plate. Cut a shape from your sheet that will fit your beam mould as well as the brackets cut in step 3. 

Pro Tip: Use the thin section of your beam from step 2 as a template for marking the position of your brackets and their holes. 

Cut the sheet, drill the holes.
