### See electric & BOM diagram 

 We made some changes tot the electric circuit diagram and updated the Bom list with suppliers we have chosen.

Also we added a kWh meter which can set to zero when you start your day melting plates! This way you can track the electricity needed, and thereafter calculate this to a Co2 load for a plate of plastic. 

We added signal lights on each group and old ceramic fuses. The signal lights gives visual feedback when a heatign group is active. The ceramic fuses protect the heating goups in case of a short circuit. We discovered ceramic is needed since they can get a bit hot.