### Turn it into reality 

 Now that you have the design for your mold it's time to actually build it and make your product.
You can find the building process of this broom hanger in this How-to "Make a broom hanger" (https://community.preciousplastic.com/how-to/make-a-broom-hanger) or use these tips to come up with your own idea :)

