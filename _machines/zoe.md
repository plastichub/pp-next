---
image: /machines/combo/zoe//renderings/perspective.JPG
category: "shredder_extrusion_combo"
title: "Extrusion & Shredder Combo 'Zoe'"
product_rel: "/machines/combo/zoe/"
tagline: ""
description: "Precious Plastic - Machine : Shredder_extrusion_combo :: Extrusion & Shredder Combo 'Zoe'"
alternatives:
 - lydia
 - lydia-v4
 - shredder-pro
 - extrusion-pro
header2:
  image: /products/zoe/media/header.jpg
  overlay_image: /products/zoe/media/header.jpg
  overlay_filter: 0.2 # same as adding an opacity of 0.5 to a black background
  caption: ""
components:
  - hal
  - shredder_v31
  - couplings
product_id: zoe
preview: /products/zoe/media/teaser.png
buy: "mailto:sales@plastic-hub.com?subject=Inquiry%20-%20zoe"
teaser: "Our best seller ! Enables easy and safe shredding and comes with an extruder"
products: true
overview_drawing: true

gallery:
 - url: "/machines/combo/zoe/media/header.jpg"
   image_path: "/machines/combo/zoe/media/header.jpg"
   alt: ""
   title: ""
 - url: "/machines/combo/zoe/media/preview.jpg"
   image_path: "/machines/combo/zoe/media/preview.jpg"
   alt: ""
   title: ""
 - url: "/machines/combo/zoe/media/v3.6.jpg"
   image_path: "/machines/combo/zoe/media/v3.6.jpg"
   alt: ""
   title: ""
 - url: "/machines/combo/zoe/media/v3.65.jpg"
   image_path: "/machines/combo/zoe/media/v3.65.jpg"
   alt: ""
   title: ""
gallery_drawings:
 - url: "/machines/combo/zoe/drawings/Baseplate_Asterix.PDF"
   image_path: "/machines/combo/zoe/drawings/Baseplate_Asterix.jpg"
   alt: ""
   title: ""
 - url: "/machines/combo/zoe/drawings/dimensions.PDF"
   image_path: "/machines/combo/zoe/drawings/dimensions.JPG"
   alt: ""
   title: ""
 - url: "/machines/combo/zoe/drawings/exploded.PDF"
   image_path: "/machines/combo/zoe/drawings/exploded.jpg"
   alt: ""
   title: ""
 - url: "/machines/combo/zoe/drawings/Global Assembly.PDF"
   image_path: "/machines/combo/zoe/drawings/Global Assembly.JPG"
   alt: ""
   title: ""
 - url: "/machines/combo/zoe/drawings/overview.PDF"
   image_path: "/machines/combo/zoe/drawings/overview.JPG"
   alt: ""
   title: ""
 - url: "/machines/combo/zoe/drawings/parts.PDF"
   image_path: "/machines/combo/zoe/drawings/parts.JPG"
   alt: ""
   title: ""
---
<div prefix="/templates/jekyll/" file="machine.html" context="machine global" class="fragment">
    
    <div prefix="/templates/jekyll/" file="machine_layout.html" context="machine global" class="fragment">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
        </li>
        {% if page.tabs.build != false %}
        <li class="nav-item">
            <a class="nav-link" id="build-tab" data-toggle="tab" href="#build" role="tab" aria-controls="build" aria-selected="false">Build</a>
        </li>
        {% endif %}
        <li class="nav-item">
            <a class="nav-link" id="resources-tab" data-toggle="tab" href="#resources" role="tab" aria-controls="resources" aria-selected="false">Resources</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="howtos-tab" data-toggle="tab" href="#howtos" role="tab" aria-controls="howtos" aria-selected="false">Howtos</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#media" role="tab" aria-controls="media" aria-selected="false">Media</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#discussion" role="tab" aria-controls="media" aria-selected="false">Discussion</a>
        </li>

        {% if page.wiring %}
        <li class="nav-item">
            <a class="nav-link" id="wiring-tab" data-toggle="tab" href="#wiring" role="tab" aria-controls="wiring" aria-selected="false">Electronics</a>
        </li>

        {% endif %}


    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="overview" role="tabpanel" aria-labelledby="overview-tab">

            {% include download_spec.html %}

            <div prefix="machines/combo/zoe/" file="overview.html" context="machine" class="fragment">
    <div>

        <a href="">
            <img src="" style="margin:8px; float: left;max-width:50%;max-height: 400px;" />
        </a>

        <span style="font-size: smaller; margin-top: 16px;">
            Shredder and extrusion combo. Easy to transport and ideal for a educational context and small enterprises.
        </span>

        <div style="display: table-cell;">
            <div prefix="machines/combo/zoe/" file="mini_specs.md" context="machine" class="fragment">
    <p><span id="specs" style="padding: 16px">
            <table>
                <tbody>
                    <tr>
                        <td>Type:
                        </td>
                        <td> Combination
                        </td>
                    </tr>
                    <tr>
                        <td>Version:
                        </td>
                        <td>3.2
                        </td>
                    </tr>
                    <tr>
                        <td> Status:
                        </td>
                        <td> Mature
                        </td>
                    </tr>
                    <tr>
                        <td> License
                        </td>
                        <td><a href="https://ohwr.org/cernohl">CERN Open Source Hardware License</a>
                        </td>
                    </tr>
                    <tr>
                        <td> Authors </td>
                        <td>
                            <a href="https://precious-plastic.org/pp/">PlasticHub S.L.</a> |
                            <a href="https://preciousplastic.com/">PrecisousPlastic - Eindhoven</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </span></p>
</div>

            {% include machine_toolbar.html download="https://precious-plastic.org//archives/zoe.zip" preview="https://a360.co/3ed8tKH" %}


        </div>

    </div>


    <div style="margin: 8px;clear:both">

    </div>

    <hr />

    <div prefix="machines/combo/zoe/" file="detail.md" context="machine" class="fragment">
    <div style="clear:both">
        <div>
            <a href="/products/zoe/drawings/overview.JPG">
                <p style="text-align: center">
                    <img width="100%" src="/products/zoe/drawings/overview.JPG">
                </p>
            </a>
        </div>
    </div>
    <hr />
    <h3 id="features">Features</h3>
    <ul>
        <li>Safe hopper – with optional door.</li>
        <li>Very precise auto-reverse when jamming. It stops after 3 trials and gives visual</li>
        <li>Status lights : Ok &amp; Error</li>
        <li>Emergency and reset button</li>
        <li>Various motor protections (thermal &amp; magnetic).</li>
        <li>Jam detection for extrusion mode.</li>
        <li>Rigid, flexible and modular framework – enabling hacking and extensions. - The base plate is 15 mm thick.</li>
        <li>The legs have M10 threads on each end so you can connect levelers or wheels. The legs have also extra holes to install easily extra sheets all around.</li>
        <li>Extrusion hopper can be easily detached.</li>
        <li>Thermocouples (sensors) have clips, easy to detach.</li>
        <li>Complies also with basic regulations in schools.</li>
        <li>Stops automatically after one hour of inactivity.</li>
        <li>High quality cables and wiring according to industrial standards.</li>
        <li>High precision mounts for shredder &amp; extrusion, making sure everything is aligned perfectly.</li>
        <li>Won’t start when power is back.</li>
        <li>Strong 32 mm hexbar and 35 mm driveshaft</li>
        <li>Compression screw</li>
    </ul>
    <hr />
</div>

    <div prefix="machines/combo/zoe/" file="products.html" context="machine" class="fragment">
    <div style="padding:16px;text-align: center;font-size: smaller;">
        <h3>Products</h3>
        <div class="ty-vendor-plans">
        </div>
    </div>
</div>
</div>
            <hr />
            
            <hr />

            <div prefix="/templates/jekyll/" file="used_in.md" context="machine global" class="fragment">
    <p>{% if page.usedin %}
    <h4>This is used in</h4>
    <div class="ty-vendor-plans small">
        {% for component in page.usedin %}
        {% for doc in site.machines %}
        {% if doc.product_id == component %}
        <div class="ty-grid-list__item_small">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover_small" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    {% endif %}</p>
</div>

        </div>

        {% if page.tabs.build != false %}
        <div class="tab-pane" id="build" role="tabpanel" aria-labelledby="build-tab">

            

            <div prefix="machines/combo/zoe/" file="build.md" context="machine" class="fragment">
    <h3 id="buildthemachine">Build the machine</h3>
    <p><div prefix="/templates/jekyll/" file="build_components.md" context="machine global" class="fragment">
    <p>{% if page.components %}</p>
    <h4>Required Components</h4>
    <div class="ty-vendor-plans small">
        {% for component in page.components %}
        {% for doc in site.machines %}
        {% if doc.category == "component" %}
        {% if doc.product_id == component %}
        <div class="ty-grid-list__item_small">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover_small" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    <p>{% endif %}</p>
</div></p>
</div>

            {% if page.gallery_drawings %}
            <h4>Drawings</h4>
            {% include gallery_drawings caption="" %}
            {% endif %}

        </div>
        {% endif %}

        <div class="tab-pane" id="resources" role="tabpanel" aria-labelledby="resources-tab">
            <span style="font-size:smaller">

                <div prefix="machines/combo/zoe/" file="product_resources.md" context="machine" class="fragment">
    <ul>
        <li><a href="https://a360.co/3ed8tKH">3D Preview</a></li>
        <li><a href="https://precious-plastic.org/products/products/zoe/electrics/wiring.png">Electrical wiring</a></li>
    </ul>
</div>

                <div prefix="machines/combo/zoe/" file="resources.md" context="machine" class="fragment">
    <h3 id="resources">Resources</h3>
</div>

                <h4>Changelog</h4>

                <div prefix="machines/combo/zoe/" file="changelog.html" context="machine" class="fragment"></div>

            </span>

        </div>
        <div class="tab-pane" id="howtos" role="tabpanel" aria-labelledby="howtos-tab">
            <div prefix="machines/combo/zoe/" file="howtos.md" context="machine" class="fragment"></div>
            <h4>Build Howtos</h4>
            <div prefix="/templates/jekyll/" file="howto_used_in_machine.md" context="machine global" class="fragment">
    <div class="ty-vendor-plans">

        {% for doc in site.howto %}
        {% if doc.usedin and doc.enabled!=false %}
        {% for used in doc.usedin %}
        {% if used == page.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endif %}
        {% endfor %}
    </div>
</div>
        </div>
        <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="media-tab">
            <div prefix="machines/combo/zoe/" file="media.md" context="machine" class="fragment">
    <h3 id="media">Media</h3>
</div>
            {% if page.gallery %}
            {% include gallery caption="" %}
            {% endif %}

            {% if page.gallery_social %}
            <h4>Media on the network</h4>
            {% include gallery_social caption="" %}
            {% endif %}

        </div>

        <div class="tab-pane" id="discussion" role="tabpanel" aria-labelledby="discussion-tab">
            <div id='discourse-comments' style="min-height: 600px;"></div>
            <script type="text/javascript">
                DiscourseEmbed = {
                    discourseUrl: 'https://forum.precious-plastic.org/',
                    discourseEmbedUrl: '{{site.url}}{{page.url}}.html'
                };

                (function() {
                    var d = document.createElement('script');
                    d.type = 'text/javascript';
                    d.async = true;
                    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
                })();
            </script>
        </div>

        {% if page.wiring %}
        <div class="tab-pane" id="wiring" role="tabpanel" aria-labelledby="wiring-tab">
            
        </div>
        {% endif %}

    </div>

    <div prefix="/templates/jekyll/" file="alternative_machines.md" context="machine global" class="fragment">
    <p>{% if page.alternatives %}</p>
    <h3 id="alternatives">Alternatives</h3>
    <div class="ty-vendor-plans">

        {% for doc in site.machines %}
        {% for alternative in page.alternatives %}
        {% if alternative == doc.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    <p>{% endif %}</p>
</div>
</div>


</div>