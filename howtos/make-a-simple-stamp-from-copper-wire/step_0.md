### Get ready 

 Let's start with getting our materials and tools ready.
To make the stamps, you will need:
- Wire/cables
- Cutter (Workshop knife)
- Pliers to bend and cut
- File to soften corners
- Soldering Iron

