#ifndef EXTERNAL_SERVO_H
#define EXTERNAL_SERVO_H

#include "../enums.h"
#include "../common/macros.h"
#include "../types.h"
#include "../config.h"

/***
 * Managed component to control a server via external pulse generator
 */
class ExternalServo
{
public:
   enum S_FLAGS
   {
      INITIATED = 1,
      MOVING = 2,
      RETRACTING = 3,
      RETRACTED = 4,
      FREEING = 5,
      DONE = 6,
      PAUSED = 7
   };

   class _LimitSwitch
   {
   public:
      _LimitSwitch(short _pin) : pin(_pin),
                                 ts(0){};
      bool value;
      byte pin;
      millis_t ts;
      void loop()
      {
         if (millis() - ts > LIMIT_SWITCH_INTERVAL)
         {
            value = digitalRead(pin);
            ts = millis();
         }
      }
      void setup()
      {
         pinMode(pin, INPUT);
      }
   };

   ExternalServo(short _stepPin, short _dirPin) : stepPin(_stepPin),
                                                  dirPin(_dirPin),
                                                  pFlags(S_FLAGS::INITIATED)
   {
   }

   short setup()
   {
      digitalWrite(stepPin, LOW);
      digitalWrite(dirPin, LOW);
      return E_OK;
   }

   short loop()
   {
   }

   short stop()
   {
      digitalWrite(stepPin, LOW);
      CBI(pFlags, S_FLAGS::MOVING);
      CBI(pFlags, S_FLAGS::FREEING);
   }

   short move(short dir)
   {
      digitalWrite(stepPin, HIGH);
      digitalWrite(dirPin, dir);
      SBI(pFlags, S_FLAGS::MOVING);
   }

   int getFlags()
   {
      return pFlags;
   }

protected:
   short stepPin;
   short dirPin;
   int pFlags;

private:
   int read()
   {
   }
};

#endif
