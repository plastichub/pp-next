### The planks 

 For the planks we collaborated with other manufacturers in Nairobi, which also produce fencing posts as well as other construction material.

It's a quite common practice there to mix in sawdust to make the material stiffer and cheaper to produce (but that material mix also breaks much easier than pure plastic). It took a couple of attempts, but in the end we managed to get the 100% recycled plastic planks.

These planks were produced with professional, industrial machines, but could definitely be made with the Precious Plastic Extrusion Pro as well!