---
image: "/howtos/rescue-a-single-use-product/DSC_0607 smaller.jpg"
title: "Rescue a single-use product"
tagline: ""
description: "Single-use products do still get used a lot. In this how-to, we show you in a few steps on how to collect these products, so you are able to import them into the Precious Plastic Universe. <br /><br />It&#39;s also a good way to create awareness and promote your Collection Point if you have one. And by being focused on the collection of one certain product you will increase the value of your recycled plastic."
keywords: "collection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "collection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Research</h4>
<div class="step-text">
  Have a walk through your local area to find places where they use single-use products. Choose one that you are going to save from the waste. Observe how the product gets used? What is the reason for using this product? Are you able to clean it easily? <br />
  <br />
  To get a clear overview you can write them all down, so you can start to create ideas based on this information. <br />
  <br />
  This can be, for example, ice-cream spoons from local ice-cream shops as they are very common and already clean (licked :). Be creative and find some single-use products that you are able to save.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/research.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/research.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Brainstorm</h4>
<div class="step-text">
  Get your creative mind working (maybe with some chocolate or coffee ;) )and start to write all the words that are related to this product. These words will make it easier to come up with some sentences that you can use in your graphic content. <br />
  The sentences need to tell the user of the single-use product to drop the (clean) used product in the bucket after using it. Keep it short and clear so people do read it before they throw their spoon. <br />
  <br />
  Extra awareness: If you can, add a note to make the person think twice before he decides to use the single-use product.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/brainstorm.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/brainstorm.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make a rough sketch of the set-up</h4>
<div class="step-text">
  Make a sketch of how the set up will look like on the location. How are you going to hang/place the poster? Where are you going to collect the single-use products in? How do you create a connection between these two? <br />
  <br />
  When you find answers to all these questions you can start gathering all the materials that you need and make it ready to use. Make sure that you ask the owner of the place if he/she agrees with the set-up.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/sketching time.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/sketching time.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Create your own poster</h4>
<div class="step-text">
  Make an illustration of the product that you want to collect. This can be done digitally, drawn on paper, painted, etc. When the illustration is done you can add the sentences to make the whole poster speaking for itself. <br />
  <br />
  Give it an extra check before copying it, make sure that the poster is understandable for anyone by, for example asking feedback from people in your surroundings. <br />
  <br />
  Tip: Keep it clear and simple
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/sketch set up.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/sketch set up.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Find the right spot</h4>
<div class="step-text">
  Now you will need to find places where you can place this set-up and start to collect. Visit some locations that use a certain single-use product that you want to collect. Do this with the bucket and the poster, that will make it easier for the owner of the location to understand what you mean. You can also use the presentation + the “how to interact with businesses”.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/make the set up.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/make the set up.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/vragen met het bakje.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/vragen met het bakje.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Set up </h4>
<div class="step-text">
  Once you agreed with the business owner to take care of this part of their plastic waste by having a single-use collection on their location, it is time to set up. Make sure it is done nicely and in a way that smoothly fits with their daily workflow (you don&#39;t want to disrupt their work).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/DSC_0607 smaller.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/DSC_0607 smaller.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/DSC_0609.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/DSC_0609.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Time to rescue</h4>
<div class="step-text">
  Now it’s time to save these single-use beauties from the waste into the Precious Plastic recycle flow. By collecting a certain product, that does come in regularly, you will create a more valuable recycled plastic kind for the shredder workspace to buy. This because you know that it will always have the same colors, quality, weight and that it is clean. By separating this plastic, from the sticky labels plastic you keep the quality as high as possible.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/drop it in the bucket .jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/drop it in the bucket .jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/rescue-a-single-use-product/spoons.jpg">
        <img class="step-image" src="/howtos/rescue-a-single-use-product/spoons.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>