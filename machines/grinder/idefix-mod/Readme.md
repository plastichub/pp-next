---
id: Idefix-Mod
layout: home
---


# Grinder Addon for Obelix and Asterix

![](./renderings/rev-b.jpg)


**brief** : General overview for a PET grinder

**document scope** : client, vendor, manufacturer

**version** : Rev-A

**status** : design & prototyping

**license** : open-source

---


## Todos

## Files / Components

[SolidWorks](./cad/)

## References

- [Short video rev.3](https://www.youtube.com/watch?v=ElOnSA7pS18&feature=youtu.be)

## Related

- [Noah umbrella project](https://gitlab.com/plastichub/noah/blob/master/machines/Noah.md)
- [PP and filament & 3D-print projects](https://precious-plastic.org/home/library/articles/filament/)
- [PP Wiki - components](https://precious-plastic.org/home/library/components/)
- [PP Forum - PET print](https://davehakkens.nl/community/forums/topic/3d-printer-for-pet-particles-no-filament-ever/)
- [Video - 3D-seed PET Printer](https://www.youtube.com/watch?v=EbxNa3WLpjA)
- [Youtube channel : large scale PET grinder & shredder](https://www.youtube.com/watch?v=3xWvHz9JjjU)

