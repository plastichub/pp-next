---
image: "/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/2F80C5D6-A07B-4B07-84E3-DA703DACE960.png"
title: "Rapid-Fire-Method for Injection / 300 Face Shields a Day"
tagline: ""
description: "With our Rapid-Fire-Method you can reduce your time-per-part by 50%.<br /><br />We started 3D-Printing face shields a while ago, but we couldn´t keep up with the demand. So we built a small injection machine to speed up production. Though we had to wait 7-8 minutes bevor we could make another face shield frame, because the plastic flakes had to heat up. <br /><br />To reduce the time-per-part we invented the Rapid-Fire-Method. Using our extruder we produce hot munition for the injection and instantly process it.<br /><br />We made a video how to do this. Check it out:<br /><br />&lt;a href=&quot;https://youtu.be/69aYBJfxHw0&quot;&gt;https://youtu.be/69aYBJfxHw0&lt;/a&gt;"
keywords: "extrusion,HDPE,PP,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "HDPE"
- "PP"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/qi-tech">qi-tech</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Extrude hot munition</h4>
<div class="step-text">
  Instead of filling up the injection machine with flakes, use your extruder to produce hot munition. Remove the nozzle, in order to extrude thick stripes. Instantly take them and fill your injection barrel with them. <br />
  <br />
  In the extruder, the plastic is mixed well and heated properly, so it is instantly processable with the injection machine.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 1 Extrude hot munition.PNG">
        <img class="step-image" src="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 1 Extrude hot munition.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 1 Extrude hot munition cutting.PNG">
        <img class="step-image" src="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 1 Extrude hot munition cutting.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 1 Munition.PNG">
        <img class="step-image" src="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 1 Munition.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Load Injection Machine</h4>
<div class="step-text">
  Take the hot stripes and insert them into the barrel of the injection machine. Try to extrude them a bit thinner than the diameter of your barrel. If they are too thick it will be a pain to load the machine. If they are to thin the barrel won&#39;t be loaded to its full extent.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 2 load injection.PNG">
        <img class="step-image" src="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 2 load injection.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Fire the injection Machine</h4>
<div class="step-text">
  Now place the mold under your injection machine and inject the melted plastic. In this case, we are using the professional mold for face shields from Plastic Preneur (a cool company from Austria). In order to achieve maximum efficiency, we have one person reload the injection machine, while the other person opens &amp; closes the mold. This way, we achieve a cycle of about 2 minutes per part.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 3 inject.PNG">
        <img class="step-image" src="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Step 3 inject.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Bild Milan mit Spritzguss Maschiene und Visieren in der Produktion.JPG">
        <img class="step-image" src="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/Bild Milan mit Spritzguss Maschiene und Visieren in der Produktion.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/D9301E2A-A803-4392-8B6A-1D381C189A77.jpg">
        <img class="step-image" src="/howtos/rapid-fire-method-for-injection--300-face-shields-a-day/D9301E2A-A803-4392-8B6A-1D381C189A77.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>