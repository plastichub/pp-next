### Prepare your blends carefully 

 In order to create patterns with nice layers, you need at least 2 colors. It doesn't really matter if you work with PP or PE, I haven't seen differences while working with these two. Keep the color mixes seperate. I mixed the dark blue that I used here with transparant flakes because it's a nice 'filler'. You will need less dark blue to create the same output because the color pigments are often very strong. So if you add 1kg transparant to 1kg dark blue,  you get 2kg dark blue without loss of color quality. 
The white used here is pure white.