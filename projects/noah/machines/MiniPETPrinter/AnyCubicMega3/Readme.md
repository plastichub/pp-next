# Support files
[Upgrades](https://all3dp.com/1/anycubic-i3-mega-upgrades-mods/)
[AnyCubic](https://www.anycubic.com/collections/anycubic-mega-3d-printers/products/anycubic-i3-mega)
[AliExpress](https://www.aliexpress.com/item/32842956599.html?spm=a2g0s.9042311.0.0.5cc04c4dZEVWoa)
[Firmware - Marlin](https://github.com/ANYCUBIC-3D/I3-MEGA)
