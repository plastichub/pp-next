### Make or buy the mould 

 Nylon was used for the mould as it's melting temperature is much higher than that of LDPE which we use to make the No Touch Tools. We are conscious of consuming virgin plastic but using 3D printed moulds means we're only using the exact amount of plastic required (opposed to CNC Machining sheets of polycarbonate for example). Although metal moulds are ideal for longevity and quality, they do come with a higher carbon footprint and cost. The nylon moulds have so far had over 100 injections with little wear and are holding up very well for a fraction of the cost of a metal mould. 

We printed the moulds using an Ultimaker 3 3D printer.