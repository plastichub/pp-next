### Making the negative mould: trim 

 In case the sheet metal exceeds the curvature’s length, you can easily grind it off. It’s crucial that the flat wedges of both halves come nicely together, otherwise the offset is not right and might deform your plastic sheet.
