# Beam deflection calculations

Standard formula to find deflection y at any given point x from the supported end of a beam:
      wx(L-x)
y=    _______ [L^2+x(L-x)]
       24EIL

Where
E= Young's Modulus of Elasticity of material (Nm^2)
I= Area Moment of Inertia (m^4)
L= total Length of beam between supports ('span') (m)
x= distance from one end support at which deflection is measured (m)
w= load (N or kN)

Concentrated Point Load:

        wx
  y=  ______ [3L^2-4x^2]
       48EI

for convenience convert forces to kN and distances to mm
