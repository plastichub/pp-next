### Get your CNC turned parts 

 To get that curved, smooth and shiny surface for the cavity of the mold, get the parts no. 1-2 and the 3D files, and take them to the best CNC lathe workshop in town. They will handle the different file extensions, but for any doubts, the drawings will make everything clear. (See drawings pages 7-8-9)
