### Set up a Community Point 


In this How-to we’re going to guide you through the steps to build set up you local Community Point. This is just as important as setting up the workspaces to transform plastic, as it is absolutely crucial to involve as many people in your area as possible.

Download files: 
👉 https://cutt.ly/starterkit-community 👈

1-3: Intro
4-6: Learn
7-11: Setup
12-19: Run
20-22: Share