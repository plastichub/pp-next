### Learn about Precious Plastic 

 As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions. Especially as a Community Point you will have to talk and answer questions about the project a lot!

If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.
👉 preciousplastic.com
👉 community.preciousplastic.com
👉 bazar.preciousplastic.com

Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.
👉 tiny.cc/preciousplasticuniverse