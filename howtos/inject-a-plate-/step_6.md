### Sand the mould edges  

 The mould will be more user friendly if you sand the hard edges away, as CNC cut aluminum can be very sharp. Optionally, I would advise that you add an inclined edge to the bottom part of the mould, this will make it much easier to open and close the mould.
It is important that the inside of the mould is sanded and polished for a clean finish of the product. This will make the difference between a high quality product and a rough one. Use a wooden block with sanding paper to keep your sanding straight. Start with 120 grain size and double the amount with each step (120, 240, 440 etc. all the way up to 2000). 

Use a cloth to clean your mould from aluminium dust. Don’t blow it out of the mould this is very bad for your lungs! After, use metal grained steel wool and then fine steel wool. The last step is to polish your mould with an additional clean cloth and polishing paste.
