# Frame for Noah

## "Tube and clamp"

The framework of Noah is built using widely available standard scaffolding tubes, beams and fittings.

Standard scaffold tubes are 48.3mm (~ 2") diameter and are available in galvanized steel or aluminium alloy.
The universal system used is known as "tube and coupler" or "tube and clamp".

![scaffolding-generic-stock-image.jpg](./scaffolding-generic-stock-image.jpg)


## Couplers

Scaffold tube connection fittings are known as "couplers", clips, clamps etc. They are available either pressed or drop-forged steel, and use set bolts with and 18mm head. Various types are available, but here we concentrate on only the basic "Doubles" (fixed 90 degree coupler) and "Swivels" (rotating coupler for any angle). These accommodate most requirements for the frame. 

![pressed-steel-double-coupler.jpg](./pressed-steel-double-coupler.jpg)

![pressed-steel-swivel-coupler.jpg](./pressed-steel-swivel-coupler.jpg)



## "Lattice" beams

Scaffold beams are known as "lattice" beams, Lahyer beams, "needle" beams, and other names.
Many types are available, but here we will focus on the simple two-rail beam with 45 degree lattice bracing.

![lattice-beam.jpg](./lattice-beam.jpg)

Beams known as "ladder beams" have vertical struts between the horizontal rails. These tend to be made from steel only, and while cheaper and less flexible, they are much heavier. They may be suitable for the Y-axis rails only, since they remain stationary.

Scaffold beams are usually available in lengths of 4,6 and 8 meters but can be joined end-to-end using a "spigot" coupling. This will allow Noah to be extended in different directions, since the spigot does not interfere with the motion of the roller passing over it.

![spigot-connection.jpg](./spigot-connection.jpg)




## Design considerations

The main limiting factor will be the weight of the gantry assembly and print head, since a heavy gantry will require greater force to accelerate and to change direction. Therefore the design of Noah's gantry assembly should be as lightweight as possible. Ideally, only aluminium beams and tubes should be used, and any extra length removed. The Z axis and print head should also be as lightweight as possible. Aluminium can flex more than steel, but this may be compensated for in the software using load/deflection calculations.

The main concern in design should always be user safety, and exponentially so when dealing with larger machinery. Therefore it is essential that Noah is set up with an appropriate system of safety stops, both user-activated and automatic. In addition to these, closed-loop positional feedback will allow the controller to maintain Noah's position precisely and sense errors immediately, suspending the program when appropriate.

It is proposed that development assemblies of Noah should be built with short Z-axis, simlar to the 'Lowrider' CNC machine. This will allow work to be done on the Cartesian arrangement at ground level. 

![xy-structure-concept-with-carriage.jpg](./xy-structure-concept-with-carriage.jpg)

Later versions will raise the Y rails on taller posts to 8 meters forming a cube-shaped frame structure. This will allow full travel in the Z axis as is required for 3d printing. This main framework should be built using ordinary steel tubes, and must be fully braced in each direction.

Of course, the corner posts of the frame must be placed on steady ground with post shoes, the rails must be carefully levelled using a laser, and the frame must be perfectly square and parallel. It may also be neccesasary to shelter the whole assembly under a roof, as is commonly done in commercial scaffolding projects.

![covered-gantry.jpg](./covered-gantry.jpg)


## ToDos

1) Complete 3d model assembly, bill of components, weight and deflection calcs
2) Assemble test rig (x axis and carriage only)
3) Design z-axis assembly (trapped between rollers within carriage) - dimensions dictated by print head
