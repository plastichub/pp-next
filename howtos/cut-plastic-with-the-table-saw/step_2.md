### Choose the right blade 

 
When choosing a blade one important feature is the tooth configuration (drawing). That’s the shape of each teeth and that’s going to affect how the material is removed. After trying different blades we have conclude that a tooth configuration like this is more versatile as it works the best on HDPE, PP and PS. But If this specific blade is not provided by any of your local dealers try a Triple chip grind (TCG) as it would work pretty good for HDPE and PP. 

Also the cutting angle (∝) is going to define the way the material is going to be removed. This is the angle at which the blade is going to enter our piece. When working with plastic the cutting angle must be negative or at least 0º to reduce the stress while cutting and prevent chips from melting. Moreover, it’s  important to keep your blades sharpened in order to guarantee the best result.

Another important point is the number of teeth (Z) and that depends on how thick is the piece we want to cut. For a Ø300mm blade:
- A high number of teeth (84-96) works good for thicknesses up to 25mm. 
- For thicker solid pieces we’ll need a blade with less teeth (72). The reason why is that we’ll need more space between tooth in order to remove the increased amount of material.
