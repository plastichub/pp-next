### Create How-to's! 

 Share to the world how you run your Collection Point so other people can learn from you and start using your solution to tackle the plastic problem. 

Make sure to document and create How-to's for your best processes and techniques. This can also help you create a name for yourself in the Precious Plastic community. 

👉 community.preciousplastic.com/how-to
