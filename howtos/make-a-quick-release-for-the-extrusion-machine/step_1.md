### Rounding the corners 

 Angle bars tend to come with rounded internal corners but we need a snug fit for 1 of our metal plates (part A). To do this we use a grinder to round the edges of the sides (longer edges) so that the plate and the angle bar sit together smoothly.
