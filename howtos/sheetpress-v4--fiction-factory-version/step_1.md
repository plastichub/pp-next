### See CAD files 

 All sheets and tubes are optimised for lasercutting & bended sheet metal. So you can order it as a IKEA-kit ready to weld, no need to cut any steel tubes by hand anymore. This greatly reduces the labour involved in making it since the parts easily fit and are numbered.