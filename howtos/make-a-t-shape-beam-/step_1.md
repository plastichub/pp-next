### Creating a template  

 Before we start work on the mould it will be good to create a template of the beam size we need to help make the mould making process easier. For the template we can use 12mm and 6mm thick MDF or plywood . Find the parts for the template in the image below (all pieces are 190cm longs)
