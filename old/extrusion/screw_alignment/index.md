---
image: /_howto/extrusion/screw_alignment/IMG_0994.JPG
category: "extrusion"
title: "Screw Alignment"
tagline: ""
description: ""
usedin:
  - lydia
  - zoe
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_0974.JPG" _target="_blank">
        <img id="IMG_0974.JPG" src="./IMG_0974.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0975.JPG" _target="_blank">
        <img id="IMG_0975.JPG" src="./IMG_0975.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0976.JPG" _target="_blank">
        <img id="IMG_0976.JPG" src="./IMG_0976.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0977.JPG" _target="_blank">
        <img id="IMG_0977.JPG" src="./IMG_0977.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0978.JPG" _target="_blank">
        <img id="IMG_0978.JPG" src="./IMG_0978.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0979.JPG" _target="_blank">
        <img id="IMG_0979.JPG" src="./IMG_0979.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0980.JPG" _target="_blank">
        <img id="IMG_0980.JPG" src="./IMG_0980.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0981.JPG" _target="_blank">
        <img id="IMG_0981.JPG" src="./IMG_0981.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0982.JPG" _target="_blank">
        <img id="IMG_0982.JPG" src="./IMG_0982.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0983.JPG" _target="_blank">
        <img id="IMG_0983.JPG" src="./IMG_0983.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0984.JPG" _target="_blank">
        <img id="IMG_0984.JPG" src="./IMG_0984.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0985.JPG" _target="_blank">
        <img id="IMG_0985.JPG" src="./IMG_0985.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0986.JPG" _target="_blank">
        <img id="IMG_0986.JPG" src="./IMG_0986.JPG" width="100%" />
    </a>
    <span class="thumb-label">13. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0987.JPG" _target="_blank">
        <img id="IMG_0987.JPG" src="./IMG_0987.JPG" width="100%" />
    </a>
    <span class="thumb-label">14. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0988.JPG" _target="_blank">
        <img id="IMG_0988.JPG" src="./IMG_0988.JPG" width="100%" />
    </a>
    <span class="thumb-label">15. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0989.JPG" _target="_blank">
        <img id="IMG_0989.JPG" src="./IMG_0989.JPG" width="100%" />
    </a>
    <span class="thumb-label">16. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0993.JPG" _target="_blank">
        <img id="IMG_0993.JPG" src="./IMG_0993.JPG" width="100%" />
    </a>
    <span class="thumb-label">17. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_0994.JPG" _target="_blank">
        <img id="IMG_0994.JPG" src="./IMG_0994.JPG" width="100%" />
    </a>
    <span class="thumb-label">18. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

