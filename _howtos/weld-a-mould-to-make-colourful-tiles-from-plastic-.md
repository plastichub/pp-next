---
image: "/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/tile_01.jpg"
title: "Weld a mould to make colourful tiles from plastic"
tagline: ""
description: "Taco will show you a simple technique to weld a mould to make colourful recycled tiles."
keywords: "mould,product,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "product"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather everything you need</h4>
<div class="step-text">
  Before you start get all your gear ready:<br />
  - Respirator mask<br />
  - Metal<br />
  - Bolts and nuts<br />
  - Plastic (PE, PP, PS)<br />
  - Angle grinder<br />
  - Drill<br />
  - Welding machine<br />
  - Sanding paper<br />
  - Injection machine<br />
  - Plastic Type Stamp
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/tile_02.png">
        <img class="step-image" src="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/tile_02.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/tile_03.png">
        <img class="step-image" src="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/tile_03.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Watch this video tutorial</h4>
<div class="step-text">
  Watch this video tutorial to learn step-by-step how to make the octagonal mould, polish it and inject a classic recycled tile.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Done & Share</h4>
<div class="step-text">
  After watching this video you should understand the basics of wedling moulds. You can reproduce the tile or try to make other products. We’re looking forward to seeing what you can come up with. Make sure to share back your new creations so the community can learn from you. Tag #preciousplastic on social media, create a new how-to or send us an email.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/tile_04.jpg">
        <img class="step-image" src="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/tile_04.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/recycled-plastic-coaster.jpg">
        <img class="step-image" src="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/recycled-plastic-coaster.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - (optional) Lasercut the mould</h4>
<div class="step-text">
  This technique was originally released in V3. By now many community members around the world used and upgraded it. Lasercutting is a common technique for it so you can easily make many different shapes and sizes. There are also many variations for sale in our bazar.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/hexo_02.JPG">
        <img class="step-image" src="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/hexo_02.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/lasercut.jpg">
        <img class="step-image" src="/howtos/weld-a-mould-to-make-colourful-tiles-from-plastic-/lasercut.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>