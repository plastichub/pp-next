<span id="specs" style="padding: 16px">
    <table class="center specs">
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td> Sheetpress
                </td>
            </tr>        
            <tr>
                <td>Version:
                </td>
                <td> 4.3
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Tested
                </td>
            </tr>
            <tr>
                <td> Sheet - size:
                </td>
                <td> 1.20 x 1.20 meter
                </td>
            </tr>
            <tr>
                <td> Based on:
                </td>
                <td> [PP Eindhoven Sheetpress](https://community.preciousplastic.com/academy/build/sheetpress)
                </td>
            </tr>
            <tr>
            <td> Authors </td>
                <td>
                    <li>[PlasticHub S.L.](${author_link})</li>
                    <li>[PrecisousPlastic - Eindhoven](${author_link_pp}) </li>
                    <li>[David Basetti - 3D Seed](https://www.facebook.com/david.bassetti.79)</li>
                </td>
            </tr>
        </tbody>
    </table>
</span>
