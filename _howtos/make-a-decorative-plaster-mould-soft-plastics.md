---
image: "/howtos/make-a-decorative-plaster-mould-soft-plastics/Title Page for Demo.jpg"
title: "Make a decorative plaster mould (soft plastics)"
tagline: ""
description: "Learn how to make a decorative plaster mould to use for soft plastics. There are two parts to this process. Stay tuned to learn how to work with plaster and then plastic in a decorative way."
keywords: "HDPE,LDPE,compression,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "HDPE"
- "LDPE"
- "compression"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/chrystal">chrystal</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Create your plaster mould</h4>
<div class="step-text">
  The first step is to collect your materials;<br />
  - scissors<br />
  - mixing stick<br />
  - liquid craft glue<br />
  - spray glue <br />
  - leafy material <br />
  - laminated cardboard or wood<br />
  - 4 laminated ply pieces <br />
  - plymer clay<br />
  - dust mask<br />
  - plaster<br />
  - water <br />
  - mixing containers<br />
  - soapy water <br />
  - brush
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Melt your plastic</h4>
<div class="step-text">
  What you will need;<br />
  <br />
  - soft plastic <br />
  - oven<br />
  - baking paper<br />
  - oven tray <br />
  -chemical &amp; heatproof gloves<br />
  -resperator mask &amp; cartridges <br />
  - plank of wood <br />
  - drill<br />
  - screws<br />
  - hanging wire
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Happy Melting!</h4>
<div class="step-text">
  Plaster is a very cheap and versatile material, use it to make as many moulds as you like! Seriously......go crazy. Here are some examples of how different colours and shapes can give you a fantastic piece to and on your wall.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-decorative-plaster-mould-soft-plastics/DSC_8665.jpg">
        <img class="step-image" src="/howtos/make-a-decorative-plaster-mould-soft-plastics/DSC_8665.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-decorative-plaster-mould-soft-plastics/DSC_8676.jpg">
        <img class="step-image" src="/howtos/make-a-decorative-plaster-mould-soft-plastics/DSC_8676.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-decorative-plaster-mould-soft-plastics/DSC_8662.jpg">
        <img class="step-image" src="/howtos/make-a-decorative-plaster-mould-soft-plastics/DSC_8662.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>