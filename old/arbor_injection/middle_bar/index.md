---
image: /_howto/arbor_injection/middle_bar/IMG_2244.JPG
category: "arborinjection"
title: "Middle Bar"
tagline: ""
description: ""
usedin:
  - elena
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2242.JPG" _target="_blank">
        <img id="IMG_2242.JPG" src="./IMG_2242.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2243.JPG" _target="_blank">
        <img id="IMG_2243.JPG" src="./IMG_2243.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2244.JPG" _target="_blank">
        <img id="IMG_2244.JPG" src="./IMG_2244.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

