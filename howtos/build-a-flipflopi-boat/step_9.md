### Sail and inspire! 

 Alright, now we only had to add (second hand) parts from other sailing boats like the mast, boom, sail and ropes. And were finally ready to do what the boat was meant to be for: Sailing around Kenya to create awareness around the problem and inspire local communities to be part of a positive change!

On our first expedition the boat sailed smoothly for more than 500km from the north of Kenya to Zanzibar, carried its passengers safely while creating excitement and fascination everywhere we went!

❗️IMPORTANT: Definitely test and check your boat if it's seaworthy! You'll be responsible for your passengers and yourself, and don't want to risk your lives 🙏 