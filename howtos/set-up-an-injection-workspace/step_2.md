### Is this for you? 

 For the Injection Workspace you will have to be quite technical as you have to understand how the Injection machine and shredder work, ideally know how to maintain it and, as a plus, know how to fix it when it needs a bit of extra love. 

You will also need some degree of design understanding and creativity to be able to come up with good products and design a mould. A plus would also be some knowledge on 3D software.

And paying attention to details will result in stunning and high quality products.