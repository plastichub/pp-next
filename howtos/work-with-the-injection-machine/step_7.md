### Injecting 

 To achieve the highest leverage, it is recommended to actuate the lever from the very end.
You can increase the force by operating the machines with two persons or increase the length of the lever (see figure 1). 

If you reached the final temperature and the plastic tends to drip out from the nozzle, you are ready to start with injecting. To be sure to have a successful injection, actuate the injection lever gently and remove the very front of the material for the first injection.

To start you remove the closing cap from the injection barrel. Make sure to attach the mould as fast as possible, after removing the closing cap. Otherwise the front of the plastic might cool down and impede the injection process.
	
Keep the pressure for ~5 seconds. This reduces the shrinkage and prevents from a "vacuum" effect, where the molten material is moved back into the injection barrel (see figure 2)