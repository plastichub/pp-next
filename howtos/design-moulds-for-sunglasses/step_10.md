### Logos and inscriptions 

 To end your product, it is interesting to get awesome inscriptions on it and make sure whomever receives your product knows who made it. Two main options here: 

1: Mill a logo or inscription of your brand, limiting the production to one brand name, but being able to have high quality inscriptions without extra efforts or time. The image shows the moulds we made for Experiencia Emprendedora from Argentina. 

2: Using the awesome Samsara’s method of pressure-marking your logos and inscriptions on the surfaces. The quality won’t be as high, but it will allow you to use the mould for a variety of projects and brands, while you establish yours (if that is your plan).

Photo credits: samsaratrc