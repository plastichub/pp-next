### Soften the edges 

 During the cuts and bending your parts, they can get a bit rough. It's important to remove any excess material and round all your edges with a sander or a file. This will help your parts fit together well and make handling your nozzle more comfortable. A crucial spot to focus on is the section that has just been bent because it has most likely deformed. This is where the two pieces of bar will be welded, so getting it flat is a good idea! 
