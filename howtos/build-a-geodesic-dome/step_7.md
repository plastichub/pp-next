### Assemble (Concept) 

 Finally it's time to assemble! Basically, the dome is made out of 6 pentagons which are connected with their edges. The raised plate in the 6-way-joint is connecting to the 5-way-joint, the rest is connecting to another 6-way piece.
