### Visit people and places 

 It's nice and creates much stronger relationships to get to know people in real life. Makes it more human and fun.

Do a little search of your area and get the chance to get in touch and visit people working on Precious Plastic around you (Collection Points, Machine Shops, Shredder Workspaces etc..).

This can give you a good understanding of the needs, problems and circumstances of the different recycling spaces in your area.
