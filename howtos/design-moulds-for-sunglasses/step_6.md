### DESIGN THE MOULD 

 When designing moulds to make eyewear with Precious Plastic machinery you need to make a fine exercise of balancing costs and mould quality.
For a really good finished product with little post processing needed, high quality inscriptions and outstanding surface finishes, you will have to spend €€€.
For testing purposes you can mill the mould way faster and with simpler machines, saving quite a chunk. 
It is vital to understand that it doesn’t make sense to make moulds that are as good as industrial moulds for the obvious reason that they will then be almost as expensive as industrial moulds!
We went for a high quality mould that will require a small amount of postprocessing. 

