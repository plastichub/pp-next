### Gather your materials  

 In order to make a jacket from plastic foils, you will first need to gather your tools and materials. These are:

Materials
- plastic foils (see step 3), enough to make four large sheets (70cm x 170cm)
- natural fabric such as cotton for the lining
- a heat resistant sheet material such as teflon fabric as a base for ironing/pressing
- baking paper which will also be used for ironing/pressing
- optionally: fastening of some sort for the raincoat (buttons, zip etc)

Tools
- iron or thermo press
- sewing machine, thread and scissors

Safety recommendations
- respirator mask to prevent the inhalation of plastic fumes!