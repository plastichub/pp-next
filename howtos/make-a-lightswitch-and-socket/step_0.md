### Make the moulds: CNC 

 Those pieces will be injected, so you need the moulds and access to an Injection machine.

For the mould-making, download the files above and CNC-mill it yourself or send it to a mould maker. 

In the latter case, make sure to communicate clearly if your part is designed as the resulted part or already oversized to compensate for shrinkage. The file attached above is the actual size of the end result, so you need to scale it depending on your material. For the PP we used we scaled the model up by 2,6%.