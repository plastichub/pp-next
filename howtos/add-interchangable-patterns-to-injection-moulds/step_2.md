### Create a compatible file  

 When lasercutting a 2D file is sufficient. Some software allows to use PNG, AI or other files. 

When CNC milling a 3D file is needed. Using software like Fusion 360 you can change your vector file into a 3D shape.

In our example we've set the cutting depth to 1.5mm.

