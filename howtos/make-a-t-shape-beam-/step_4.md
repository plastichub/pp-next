### Mould part 2 

 Now that we have the bottom part we can move on to the second part of the mould. Place part 3 of the template back into the mould then place the 2 L angles on top and the square rod in between them and fasten down with clamps. 
 
Drill 5mm holes every 30cm and fasten with nuts and bolts. Weld the portion between the square rod and the L angle to seal the mould for the whole length. (Again, weld in shorter stretches and alternate with gaps to prevent the mould from deforming)
