### Create the Shredder and Housing 

 The basic design and instructions for the shredder can be found on the Precious Plastic Instruction pages (https://community.preciousplastic.com/academy/build/shredder). 

Initially the number of shredder blades will need to be reduced down to around 9 from the original design so that the shredder is thin and light enough to be mounted on the back of a bicycle, however more can be incorporated if desired. The depth of the shredder housing will then need to be scaled down in co-ordination with the reduced number of blades. 
For the housing, aluminium or steel can be used so long as the blades and spacers are still made of steel. Using aluminium will mean that the housing has to be bolted together instead of welded.
