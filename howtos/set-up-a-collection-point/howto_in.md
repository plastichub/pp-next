### Set up a Collection Point 


In this How-to we’re going to guide you through all the steps to set up a Collection Point. Learn about plastic, how to create the space, and how to grow a collection network and connect to the Precious Plastic Universe.

Download files: 
👉 https://cutt.ly/starterkit-collection 👈

Step 1-3: Intro
Step 4-11: Learn
Step 12-19: Set up
Step 20-25: Run
Step 26-29: Share