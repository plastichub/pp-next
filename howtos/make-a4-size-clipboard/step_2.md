### Baking + pressing 

 I use IKEA Görlig electric oven.
Set up temperature according to your material type.
For HDPE and PP I use temperature 225 deg Celsius.
I put mould in the oven for 35 min. 
Set up timer. 


Attention - use heat resistent gloves when you operate with mould from oven :) 

After baking process, press mould. 
Cooling process take 20 - 30 min. 
If you open mould earlier, there is risk of deformation.

