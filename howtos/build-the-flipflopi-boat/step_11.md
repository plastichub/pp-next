### Join us for the big boat! :) 

 We already reached hundreds of thousands locally and globally with our expedition and the story around the boat. It's a great tool to reach people in a positive way to push good changes.

So, we'll be building a much bigger boat which can sail longer distances and reach millions!
There is still a lot to improve and figure out for a boat of that size! But as Ali Skanda says:
"Kila kitu inaweze kana." - Everything is possible :)

Hope this was insightful or at least a bit inspiring 🙃

Want to get involved? ✉️ theflipflopi@gmail.com
Or become a supporter (yay!) 👐 https://www.patreon.com/theflipflopi

And if you end up building a similar boat, make sure to share it! (@theflipflopi) 