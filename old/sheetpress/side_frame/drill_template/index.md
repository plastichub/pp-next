---
image: /_howto/sheetpress/side_frame/drill_template/IMG_2347.JPG
category: "sheetpress"
title: "Drill Template"
tagline: ""
description: ""
title: "Side frame - Drill Template"
usedin:
  - sheetpress-cell
  - sheetpress
---



<div prefix="_howto\sheetpress\side_frame\drill_template" file="header.md" context="md:thumbs" class="fragment">
    <p><strong>This template enables you:</strong></p>
    <ul>
        <li>always spot on holes for the sliding mechanism - needed!</li>
        <li>compensate bad saw cuts</li>
        <li>compensate too short lengths</li>
    </ul>
    <p><strong>Other remarks</strong></p>
    <ul>
        <li>Always mark holes this on both sides (up &amp; down) !</li>
        <li>Always use it from the same end of the tube! If the bar was too shoort, you can take this into account when welding the side frame together.</li>
        <li>You can alter this design to use it also as saw gauge to cut it on length.</li>
    </ul>
</div>

<div class="thumbs">test


<div class="thumb">
    <a href="./IMG_2327.JPG" _target="_blank">
        <img id="IMG_2327.JPG" src="./IMG_2327.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2328.JPG" _target="_blank">
        <img id="IMG_2328.JPG" src="./IMG_2328.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2329.JPG" _target="_blank">
        <img id="IMG_2329.JPG" src="./IMG_2329.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2330.JPG" _target="_blank">
        <img id="IMG_2330.JPG" src="./IMG_2330.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2331.JPG" _target="_blank">
        <img id="IMG_2331.JPG" src="./IMG_2331.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2332.JPG" _target="_blank">
        <img id="IMG_2332.JPG" src="./IMG_2332.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2334.JPG" _target="_blank">
        <img id="IMG_2334.JPG" src="./IMG_2334.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2336.JPG" _target="_blank">
        <img id="IMG_2336.JPG" src="./IMG_2336.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2337.JPG" _target="_blank">
        <img id="IMG_2337.JPG" src="./IMG_2337.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2338.JPG" _target="_blank">
        <img id="IMG_2338.JPG" src="./IMG_2338.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2339.JPG" _target="_blank">
        <img id="IMG_2339.JPG" src="./IMG_2339.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2340.JPG" _target="_blank">
        <img id="IMG_2340.JPG" src="./IMG_2340.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2341.JPG" _target="_blank">
        <img id="IMG_2341.JPG" src="./IMG_2341.JPG" width="100%" />
    </a>
    <span class="thumb-label">13. </span>
</div>
<hr /> next, add drill guides for the top holes of the sidebar frames. Those will hold the upper press plate. You want those even more precise since the assembly needs to be done with a crane or forklift, or just lots of people!




<div class="thumb">
    <a href="./IMG_2342.JPG" _target="_blank">
        <img id="IMG_2342.JPG" src="./IMG_2342.JPG" width="100%" />
    </a>
    <span class="thumb-label">14. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2343.JPG" _target="_blank">
        <img id="IMG_2343.JPG" src="./IMG_2343.JPG" width="100%" />
    </a>
    <span class="thumb-label">15. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2344.JPG" _target="_blank">
        <img id="IMG_2344.JPG" src="./IMG_2344.JPG" width="100%" />
    </a>
    <span class="thumb-label">16. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2345.JPG" _target="_blank">
        <img id="IMG_2345.JPG" src="./IMG_2345.JPG" width="100%" />
    </a>
    <span class="thumb-label">17. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2346.JPG" _target="_blank">
        <img id="IMG_2346.JPG" src="./IMG_2346.JPG" width="100%" />
    </a>
    <span class="thumb-label">18. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2347.JPG" _target="_blank">
        <img id="IMG_2347.JPG" src="./IMG_2347.JPG" width="100%" />
    </a>
    <span class="thumb-label">19. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

