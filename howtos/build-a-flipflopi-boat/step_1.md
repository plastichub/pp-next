### Get a boatbuilder 

 Okay, so it all started with finding a local boat builder, who was confident and visionary enough to believe that we could build a boat from a totally different material than what they were used to.

Ali Skanda, from Lamu, was our man and gathered his boatbuilder team to apply their knowledge to a new material.  

We definitely recommend finding someone who knows how to build boats, so you can focus on learning how to use plastic for already existing processes, instead of trying to learn another complex skill on top of that!