### Role in the Precious Plastic Universe 

 First of all, make sure you had a look at the showcase page of this Starterkit!
👉 preciousplastic.com/starterkits/showcase/shredder 

Now about your Role:

Shredder Workspaces are the backbone of the Precious Plastic Universe. 

They shred all the plastic gathered from the Collection Points and provide the raw material for all the other workspaces who transform it into new materials and objects.

Shredder Workspaces are also responsible for maintaining high quality and pure material for the entire local network. 
