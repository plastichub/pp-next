### Placing limiters 

 To ensure the bricks do not slip off the wooden substructure, we create small spacers which are exactly the same dimension of the base of the brick. These spacers should be located near/on bolts and near corners.

These can be screwed into place. 
