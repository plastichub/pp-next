### Fix mould part 1 

 Make sure to clamp the metal pieces and template, then drill 6mm holes every 15 cm for the first half and every 25 cm in the second half, on both the square rods. (Note: we add more screws in the first half as more pressure is built up here during extrusion and to prevent leakage) 

Now we can remove part 1 and 2 of the template and fasten on the nuts and bolts. Weld the outer joint of the square rods and the flat. (note: weld in shorter stretches and alternate with gaps to prevent the mould from deforming)
