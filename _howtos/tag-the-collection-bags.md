---
image: "/howtos/tag-the-collection-bags/DSC08419.jpg"
title: "Tag the collection bags"
tagline: ""
description: "In this how-to, you will learn how to make tags for your collection bags. The tags are attached to the bags during transport. This way the Shredder Workspace will not confuse the plastic-type waste from the Collection Point."
keywords: "shredder,collection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "shredder"
- "collection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Choose your stencil technique</h4>
<div class="step-text">
  You will need a stencil to create your own tags. The stencil will help you speed up the process. You can make it in many ways, such as using a cardboard box and a knife, a laser-cut machine on wood, a CNC machine on steel or plastic, or even a 3D printer using PLA. Each technique requires different materials, and therefore different costs. <br />
  <br />
  The best technique is the one you have most easily accessed, is economical and will produce a long-lasting stencil. Cardboard boxes with a cutter are the cheapest option, but it will take a long time to make and the stencil will not last long. We recommend the laser cut on plywood. Ask the nearest FabLab in your area, they can help you :).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/1.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/20.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/20.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/19.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/19.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Download your illustrations </h4>
<div class="step-text">
  Above on that beautiful button, you are gonna find the image of the stencil. Send this image to your nearest laser cut machine. Ask for a laser cutting on a plywood 3 mm. <br />
  <br />
  The dimension of the stencil is 150x150 mm. The red lines on the picture mean cutting and the black one means engraving.<br />
  <br />
  Or if you want to try to make it by yourself on cardboard print the image on an A4 paper and stick it to the cardboard.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/stencil_preciousplastic-01.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/stencil_preciousplastic-01.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Paint over your outline</h4>
<div class="step-text">
  If you order the stencil on a laser cut machine then the result will be perfect. Make sure to sand the plywood and remove the black stain from the laser.<br />
  <br />
  Your stencil is ready. Now it is time to use it. I recommend a thick marker. Cut through the fabric and attach the stencil. We used a piece of canvas fabric. Use the marker to draw the triangle.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/14.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/14.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/18.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/18.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/15.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/15.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Draw the number and write your info</h4>
<div class="step-text">
  You drew the triangle, now its time for the number. In the stencil, you will find all the recycling numbers made in Precious Plastic font, but you can always make it more personal. Don’t forget to write your Collection Point name! Use the stencil as a ruler if you want to make straight lines. <br />
  <br />
  Last tip: you can always use different colours if you want to sort your plastic label also by colour.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/12.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/12.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/10.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/10.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/9.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/9.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Open a hole and place the string</h4>
<div class="step-text">
  Time to open the hole, you can do it easily with a hammer and a thick nail. Now put the string and you are done. Choose a strong string, we used leather.<br />
  <br />
  You can give your label some love. Sew the ends, iron them and keep them on your hanging board to reuse them again.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/8.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/8.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/3.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/tag-the-collection-bags/2.jpg">
        <img class="step-image" src="/howtos/tag-the-collection-bags/2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>