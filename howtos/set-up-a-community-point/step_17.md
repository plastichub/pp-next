### Keep inspiring 

 Public speaking is a great and effective way to inspire people to participate. This can keep your group motivated and open the doors for new collaborations.

In the Download package you can find a sample presentation which you can use and modify for your contexts to talk about Precious Plastic and your local community.