#ifndef MOTOR_AUTO_REVERSE_H
#define MOTOR_AUTO_REVERSE_H

#include <Arduino.h>
#include "./Addon.h"
#include "./config.h"
#include <Streaming.h>
#include "./common/macros.h"
#include "./enums.h"

class App;
class VFD;
class Status;

class AutoReverse : Addon
{
public:
  AutoReverse(App *app);
  AutoReverse() : Addon(AUTO_REVERSE_STR, AUTO_REVERSE) {}
  enum SHRED_STATE
  {
    WAITING = 0,
    INIT = 1,
    POWERED = 2,
    STARTED = 3,
    SHREDDING = 6,
    UNPOWERED = 7,
    DONE = 8,
    CANCELLING = 10,
    JAMMED = 11,
    REVERSING = 12,
    REVERSED = 13,
    STOPPING = 14,
    FORWARDING = 15,
    CANCELLED = 16,
    STUCK = 17,
    HALTING = 18,
    REVERSING_RUN = 19,
  };

  virtual short setup()
  {
  }
  virtual short ok()
  {
    return true;
  }
  void debug(Stream *stream)
  {
    // *stream << this->name << ":" << this->ok();
  }
  void info(Stream *stream)
  {
    // *stream << this->name << "\n\t : " SPACE("Pin:" << MOTOR_IDLE_PIN);
  }

private:
  short shredState;
};
#endif