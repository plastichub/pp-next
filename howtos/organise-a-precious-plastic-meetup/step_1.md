### Find a good place 

 Now think of a place where you can meet. Consider that it should be somewhere rather central and accessible and have enough space for the number of people who will probably join the meeting. Ideally find a place where you can show a presentation and videos (on a laptop or even better with a projector) and can comfortably sit and exchange ideas and thoughts.

Ask in your group, maybe they have ideas or even a good space for this meeting in their own collection point, workspace, design office, fablab etc. - this could also give the group the opportunity to see and understand how other members of the community operate.
