### Stickers 

 In the Download Kit you can find a sticker for businesses collaborating with you and giving you their plastic waste. You can put it on their window so their customers they can see they’re part of the movement and doing something about the plastic problem. 