---
image: /_howto/zoe/base_completion/IMG_2185.JPG
category: "zoe"
title: "Base Completion"
tagline: ""
description: ""
tags :
 - v3
 - zoe
categories:
  - Shredder
usedin:
  - zoe
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2157.JPG" _target="_blank">
        <img id="IMG_2157.JPG" src="./IMG_2157.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2158.JPG" _target="_blank">
        <img id="IMG_2158.JPG" src="./IMG_2158.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2159.JPG" _target="_blank">
        <img id="IMG_2159.JPG" src="./IMG_2159.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2160.JPG" _target="_blank">
        <img id="IMG_2160.JPG" src="./IMG_2160.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2161.JPG" _target="_blank">
        <img id="IMG_2161.JPG" src="./IMG_2161.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2162.JPG" _target="_blank">
        <img id="IMG_2162.JPG" src="./IMG_2162.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2165.JPG" _target="_blank">
        <img id="IMG_2165.JPG" src="./IMG_2165.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2168.JPG" _target="_blank">
        <img id="IMG_2168.JPG" src="./IMG_2168.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2170.JPG" _target="_blank">
        <img id="IMG_2170.JPG" src="./IMG_2170.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2171.JPG" _target="_blank">
        <img id="IMG_2171.JPG" src="./IMG_2171.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2172.JPG" _target="_blank">
        <img id="IMG_2172.JPG" src="./IMG_2172.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2173.JPG" _target="_blank">
        <img id="IMG_2173.JPG" src="./IMG_2173.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2174.JPG" _target="_blank">
        <img id="IMG_2174.JPG" src="./IMG_2174.JPG" width="100%" />
    </a>
    <span class="thumb-label">13. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2175.JPG" _target="_blank">
        <img id="IMG_2175.JPG" src="./IMG_2175.JPG" width="100%" />
    </a>
    <span class="thumb-label">14. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2176.JPG" _target="_blank">
        <img id="IMG_2176.JPG" src="./IMG_2176.JPG" width="100%" />
    </a>
    <span class="thumb-label">15. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2177.JPG" _target="_blank">
        <img id="IMG_2177.JPG" src="./IMG_2177.JPG" width="100%" />
    </a>
    <span class="thumb-label">16. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2178.JPG" _target="_blank">
        <img id="IMG_2178.JPG" src="./IMG_2178.JPG" width="100%" />
    </a>
    <span class="thumb-label">17. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2179.JPG" _target="_blank">
        <img id="IMG_2179.JPG" src="./IMG_2179.JPG" width="100%" />
    </a>
    <span class="thumb-label">18. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2180.JPG" _target="_blank">
        <img id="IMG_2180.JPG" src="./IMG_2180.JPG" width="100%" />
    </a>
    <span class="thumb-label">19. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2181.JPG" _target="_blank">
        <img id="IMG_2181.JPG" src="./IMG_2181.JPG" width="100%" />
    </a>
    <span class="thumb-label">20. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2183.JPG" _target="_blank">
        <img id="IMG_2183.JPG" src="./IMG_2183.JPG" width="100%" />
    </a>
    <span class="thumb-label">21. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2184.JPG" _target="_blank">
        <img id="IMG_2184.JPG" src="./IMG_2184.JPG" width="100%" />
    </a>
    <span class="thumb-label">22. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2185.JPG" _target="_blank">
        <img id="IMG_2185.JPG" src="./IMG_2185.JPG" width="100%" />
    </a>
    <span class="thumb-label">23. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

