### Construction on site 

 Label all the stations and create enough space around the machines so many people can see what’s happening. Compare your setup with the course of the recycling process and check if it’s similar.  

Build up your mobile workshop with enough time and take a break before everything starts.
