### Make a rough sketch of the set-up 

 Make a sketch of how the set up will look like on the location. How are you going to hang/place the poster? Where are you going to collect the single-use products in? How do you create a connection between these two? 

When you find answers to all these questions you can start gathering all the materials that you need and make it ready to use. Make sure that you ask the owner of the place if he/she agrees with the set-up.
