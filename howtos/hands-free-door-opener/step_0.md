### Download and mill the moulds 

 Attached you can find the CNC-files I created based on the 3D files provided by Materialise.
(https://www.materialise.com/en/hands-free-door-opener/technical-information)

Download the files, CNC cut your aluminium mould and add required screws to close the mould.
