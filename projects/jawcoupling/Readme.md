3 Jaw Coupling

![JawCoupling](https://gitlab.com/plastichub/products/blob/master/projects/jawcoupling/JawCoupling.JPG)

A heavy duty 3 jaw coupling with SLA printed spider bushing. 

Outer Diameter 90mm

Overall Length 70mm

Jaw Depth 20mm

Inner Bore 24, 30, 35mm options (35mm shown)

Keyway 10mm

Set bolts 2x M8, one over key, one at 120deg

Spider Insert wall thickness 2.5mm throughout (plastic)

Steel or aluminium?

The jaws can be machined in 3 straight though passes on the mill with a 16mm endmill leaving a small triangle to clear with a stopping pass.

First cut

![first cut](https://gitlab.com/plastichub/products/blob/master/projects/jawcoupling/first_cut.JPG)

After 3 through cuts, first clearing pass

![clearing pass](https://gitlab.com/plastichub/products/blob/master/projects/jawcoupling/mid_machining.JPG)

These can then be bored and keyed to suit the axle as needed.


3D printed Spider Bushing - preferably SLA but FDM would probably be fine..

![Spider Bushing](https://gitlab.com/plastichub/products/blob/master/projects/jawcoupling/Spider.JPG)


Closed Coupling

![Closed coupling](https://gitlab.com/plastichub/products/blob/master/projects/jawcoupling/JawCoupling_closed.JPG)
