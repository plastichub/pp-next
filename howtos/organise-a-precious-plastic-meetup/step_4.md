### Make a plan 

 Now it’s time to make a more detailed plan for your meetup. Write down what you want to find out and achieve and think about what you will do with your group.

This will depend a lot on the number and kind of people coming. Newcomers will need more explanations about Precious Plastic, more experienced people will probably focus more on figuring out how to work together to build a stronger network.
