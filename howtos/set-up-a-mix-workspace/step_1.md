### Outcomes 

 The Mix Workspace can have multiple outcomes. The knowledge and education generated for its participants and the broader society are huge. Amongst other 5 things Mix Workspaces can output small productions, workshops or presentations.