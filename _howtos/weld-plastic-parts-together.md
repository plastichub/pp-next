---
image: "/howtos/weld-plastic-parts-together/IMG_1555 1280x960px.JPG"
title: "Weld plastic parts together"
tagline: ""
description: "If you want to join several plastic parts together, you can join them together at the joint surfaces with a simple soldering iron. <br />It&#39;s quick and you don&#39;t need any additional material."
keywords: "hack,melting,HDPE,product,injection,PP"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
- "melting"
- "HDPE"
- "product"
- "injection"
- "PP"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/johannplasto">johannplasto</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Choose your soldering tip</h4>
<div class="step-text">
  A very stable connection is created when the connection surfaces are first welded deeper and then at the edge. <br />
  For the deeper welding, a thin soldering tip is suitable and for the edge seam a larger one.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-parts-together/IMG_1477.JPG">
        <img class="step-image" src="/howtos/weld-plastic-parts-together/IMG_1477.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-plastic-parts-together/IMG_1479.JPG">
        <img class="step-image" src="/howtos/weld-plastic-parts-together/IMG_1479.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-plastic-parts-together/IMG_1480.JPG">
        <img class="step-image" src="/howtos/weld-plastic-parts-together/IMG_1480.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Choose the welding temperature</h4>
<div class="step-text">
  Do some temperature tests.<br />
  I use around 250 ° C for HDPE and 270 ° C for PP.<br />
  Please use a gas mask for this work! <br />
  See also: <a href="https://community.preciousplastic.com/academy/plastic/safety">https://community.preciousplastic.com/academy/plastic/safety</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-parts-together/IMG_1483.JPG">
        <img class="step-image" src="/howtos/weld-plastic-parts-together/IMG_1483.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-plastic-parts-together/IMG_1497.JPG">
        <img class="step-image" src="/howtos/weld-plastic-parts-together/IMG_1497.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Weld it</h4>
<div class="step-text">
  Position the parts and fix / clamp them.<br />
  Weld the parts first with the thin soldering tip and then with the thick soldering tip. Not too fast!<br />
  Check the connection and change the parameters: temperature, welding depth and speed if necessary.<br />
  Happy melting!
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - How to make the lamps</h4>
<div class="step-text">
  The recycling guy in this video shows how he combines 12 plates to one hanging lamp ;) <br />
  In addition to HDPE, you can also use this welding technique with PP. LDPE or PS.<br />
  After welding, you can also improve the edges. I rub with additional hard plastic over the edges, but you can also polish.<br />
  <br />
  For more information about the lamps visit <a href="https://johannplasto.de/.de">https://johannplasto.de/.de</a> or <br />
  <a href="https://www.instagram.com/johannplasto/<br/>">https://www.instagram.com/johannplasto/<br /></a>
  <br />
  Greets! 🙂<br />
  Thomas
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>