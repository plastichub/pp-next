### Assembly of the top section (Part A) 

 Each corner requires 1 bolt and 3 nuts. The bolt should be 45-50 mm long, the nuts are 10mm. Place 6 bolts through the top plate (AD5) and add an additional two nuts to each. This should be followed by the interlocking parts (AB5 & AC5) followed by the bottom plate (AA5). The ideal distance of the two plates (AA5 & AD5) is 20 mm.
