### Prepare your message 

 You also have to prepare yourself and ask yourself these questions:

- Why are you giving this workshop?
- What kind of knowledge and experience do you want to teach people?
- What are your good at?
