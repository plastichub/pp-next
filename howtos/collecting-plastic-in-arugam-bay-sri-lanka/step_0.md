### Arugam Bay 

 We’re based in Arugam Bay, a small town of only 150 people on the east coast of Sri Lanka. The little town’s economy is mainly based on tourism with seasonal fishing but lacks proper waste management. During tourist season tourism related businesses demand all waste management capacity of the municipality so even more locals than usual resort to burning their plastic waste. Also businesses were burning large piles of plastic bottles as tourists consume such a vast amount of it. 
