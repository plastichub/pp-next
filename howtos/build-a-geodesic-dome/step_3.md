### Inject the joints 

 Ready to inject! Set the temperature of the tube to 240°C and the nozzle to 230°C. The following workflow will take 12-15 mins per piece:
1 - Insert plastic to the hopper
2 - Heat up the plastic (8-9 min)
3 - Attach the mould 
4 - Inject plastic
5 - Take the mould out of the machine

Repeat this process for each joint. Ongoing, while you're waiting for one mould to cool down, you can prepare the other mould for the next injection process.

If needed, here are some tips for injecting:
👉 tiny.cc/work-injection-machine
