### Cooling the mould 

 Depending on your mould and product we recommend placing the mould in a bath of water to cool the plastic allowing quicker removal. Depending on your mould you may want to leave it in water for a while. However, in this case, it’s crucial to remove it while the plastic is still warm to prevent the plastic from shrinking and sticking to the male part of the mould. So for hollow objects, be aware of this shrinkage!

The water will get very warm over time so its best to change it regularly or use a large body of water. 
