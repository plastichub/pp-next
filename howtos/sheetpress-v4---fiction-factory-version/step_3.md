### Making plastic precious again! 

 The first commercial client where we used our machine for is The Student Hotel in Delft. From post-production HDPE plastic we made the black&white cladding of their bar. The client wanted us to incoporate their waste streams which where mainly (HDPE) bottle-caps from soda bottles. The caps are melted in to the plate's.

Also we used a lot of PET plastic plate's from Smile plastics in England. With the help of the Sheetpress we could easily bend them for the rounded bars.

The blue-white plates from PS where made by the Good Plastic Company.