### Preparation: Gather Materials and Tools 

 Materials:
Plastic shopping bags
Strap - 1.5m
Zipper - 0.4m
Snap-fit buckle
Baking paper
Tape

Tools:
Iron
Ironing board
Sewing machine
Sewing pins
Scissors
Marker
Lighter