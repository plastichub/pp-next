### Bend the plastic sheets 

 Time to bend! Learn the basics in the How-to “Bend sheets” and make sure to Stay safe when melting plastic.
Heat up your oven to 190°C and heat your sheets (parts A+B) for about 7min. Turn it half way through to ensure a more even heat distribution. We recommend using Teflon fabric or baking paper to prevent the plastic from sticking to the oven.

When soft enough for bending, put the sheet into its mould and align it in the center and along the fence. After pre-bending by hand, put the upper part of the mould on top and make it snap into place. After a couple of minutes it should be cooled down and ready to be taken out of the mould.