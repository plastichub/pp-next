### Bend with the Sheetpress 


We have used the sheetpress as a tool for bending plastic sheets, solid surface materials or even extruded beams. The process is quite simple but might require some preparation to get the best results. Below are the steps taken to do this, although there is still room for further improvements!  

Tools needed:
-	Sheet press
-	Some kind of mould
-	Clamps
