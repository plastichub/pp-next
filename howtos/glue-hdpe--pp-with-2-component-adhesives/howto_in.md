### Glue HDPE & PP with 2-component adhesives 


2-component adhesives can offer a strong bonding and they are easy to use. On the other hand they are extremely wasteful since they usually come in single-use packaging. 

We generally don't recommend glueing parts together as it will make the disassembly harder at the end of the product’s life. So check out other joining techniques first!
If this is still needed, here some guidelines.