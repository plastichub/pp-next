### Get your Sheetpress 

 Cool, now you have a space it’s time to get hold of your Sheetpress system. There are three ways to do that:

1 Build it yourself following our tutorials
👉 tiny.cc/build-sheetpress

2 Buy it on the Bazar.
👉 bazar.preciousplastic.com

3 Find a Machine Shop near you on the map that can build it for you.
👉 community.preciousplastic.com/map