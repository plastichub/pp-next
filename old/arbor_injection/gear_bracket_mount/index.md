---
image: /_howto/arbor_injection/gear_bracket_mount/IMG_2295.JPG
category: "arborinjection"
title: "Gear Bracket_mount"
tagline: ""
description: ""
usedin:
  - elena
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2278.JPG" _target="_blank">
        <img id="IMG_2278.JPG" src="./IMG_2278.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2280.JPG" _target="_blank">
        <img id="IMG_2280.JPG" src="./IMG_2280.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2281.JPG" _target="_blank">
        <img id="IMG_2281.JPG" src="./IMG_2281.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2282.JPG" _target="_blank">
        <img id="IMG_2282.JPG" src="./IMG_2282.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2283.JPG" _target="_blank">
        <img id="IMG_2283.JPG" src="./IMG_2283.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2287.JPG" _target="_blank">
        <img id="IMG_2287.JPG" src="./IMG_2287.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2288.JPG" _target="_blank">
        <img id="IMG_2288.JPG" src="./IMG_2288.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2290.JPG" _target="_blank">
        <img id="IMG_2290.JPG" src="./IMG_2290.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2291.JPG" _target="_blank">
        <img id="IMG_2291.JPG" src="./IMG_2291.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2292.JPG" _target="_blank">
        <img id="IMG_2292.JPG" src="./IMG_2292.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2293.JPG" _target="_blank">
        <img id="IMG_2293.JPG" src="./IMG_2293.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2294.JPG" _target="_blank">
        <img id="IMG_2294.JPG" src="./IMG_2294.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2295.JPG" _target="_blank">
        <img id="IMG_2295.JPG" src="./IMG_2295.JPG" width="100%" />
    </a>
    <span class="thumb-label">13. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

