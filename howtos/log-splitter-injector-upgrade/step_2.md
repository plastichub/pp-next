### Build your own! 

 This machine may not suit all workshops, however it has certainly been worth it's weight in gold for us!

If you plan on upgrading your machine, here are a few things to try:
• Monitor pressure. We used a load cell (see second image) in our first version, however it shortened our stroke length and we are now weighing each shot
• Change the orientation to suit your workshop.
• Create a quick release that will work on a moving barrel (and share the How-To!)

Finally, be careful. We do not accept any responsibly for loss or damages - this 'How-To' is intended to be educational not instructional. 

Follow us @preciousplasticmelbourne and tag us in your upgrade!