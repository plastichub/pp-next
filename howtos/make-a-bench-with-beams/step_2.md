### Prepare your material 

 Let's start making the angled beams. Get your cleaned and sorted plastic shredded into a size that works well with your extrusion machine. PP and HDPE should work quite well for this product (PS is probably to brittle for a furniture like that).

(In this case we used PP and made a mixture of yellow and orange (1:4) to achieve a zesty orange.)

Pro tip: Let your plastic dry fully before melting it, as moisture can cause a large air pockets in your part.
