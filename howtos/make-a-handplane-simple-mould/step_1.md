### LIMITATIONS & TESTING 

 As complex shapes will be hard to achieve without complex machinery, you will have to finish your product by hand. With our handplanes we simplify everything as much as possible, to reduce this post-injection finishing time and material waste. Simplification is key.
For the purpose of testing our design, we carried out trials with compressed plastic plates, cut to different shapes and then bent to different diameter concaves.