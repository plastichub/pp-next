### Extrude into a closed mould 


The extrusion machine provides continuous flow which means with the right mould we can create much larger objects than possible with the injection, while also allowing us to create products faster.