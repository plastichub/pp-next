### Extrude your parts 

 For many reasons your machine may differ to the one we’re using at the workspace here in Eindhoven, so take care with your settings and use the experience you’ve had with PP. In this case, we set our machine to temperatures (from hopper to nozzle) 235°C, 245°C, 250°C. While running the machine at 102rpm it took 15 minutes for each part, letting one cool while extruding the next. 

In this case we went for the textured beams - have a look at the How-to below to decide what texture you want.

Related links:
Extrude different textures 👉 tiny.cc/extrude-textures