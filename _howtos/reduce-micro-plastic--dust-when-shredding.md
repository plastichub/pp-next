---
image: "/howtos/reduce-micro-plastic--dust-when-shredding/HowTo_Slide.jpg"
title: "Reduce micro-plastic & dust when shredding"
tagline: ""
description: "Minimise micro-plastic and dust with this simple shredder upgrade! This simple hack also helps when it comes time to clean your shredder for colour or material changes. <br /><br />You&#39;ll need the following components for this project:<br />• Vacuum<br />• 20L Bucket<br />• Cyclone Dust Collector<br />• Funnel"
keywords: "hack,collection,shredder"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
- "collection"
- "shredder"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/infoplasticorgau">infoplasticorgau</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - The Vacuum</h4>
<div class="step-text">
  The vacuum, is of course the most crucial component of this hack. You can use something you already have, or purchase a vacuum fit for purpose.<br />
  <br />
  We went with a backpack vacuum - our small workshop is a shared space, so we need something that is fairly compact and quiet, yet powerful. As our vacuum needs to run for long periods of time, we also felt it was best to opt for one that&#39;s built for commercial use.<br />
  <br />
  Direct link to our vacuum of choice: <a href="https://sydneytools.com.au/product/bayer-bp45l-1200w-hi-powered-4-5l-tank-dry-backpack-vacuum">https://sydneytools.com.au/product/bayer-bp45l-1200w-hi-powered-4-5l-tank-dry-backpack-vacuum</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/reduce-micro-plastic--dust-when-shredding/vacuum.jpg">
        <img class="step-image" src="/howtos/reduce-micro-plastic--dust-when-shredding/vacuum.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - The Bucket</h4>
<div class="step-text">
  This type of bucket is fairly common, and generally pretty easy to source secondhand. Look for something in good condition, with a removable lid.<br />
  <br />
  Additionally, a round bucket tends to work better than a square container for this purpose.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/reduce-micro-plastic--dust-when-shredding/bucket.jpg">
        <img class="step-image" src="/howtos/reduce-micro-plastic--dust-when-shredding/bucket.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Dust Cyclone</h4>
<div class="step-text">
  Dust cyclones remove over 99% of dust and debris from the airstream, containing it before it ever reaches the vacuum. We&#39;ve been running our Dust Extractor for quiet a while, and are yet to spot even a speck of plastic in our vacuum.<br />
  <br />
  The cyclone will include a cutting template and instructions, along with nuts/bolts and connection pieces. We picked one up from eBay for around $40AUD: <a href="https://bit.ly/3iKR4e6<br/>">https://bit.ly/3iKR4e6<br /></a>
  <br />
  If you&#39;re keen to save some cash or prefer a DIY option, check out this tutorial: <a href="https://bit.ly/33FxUSP">https://bit.ly/33FxUSP</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/reduce-micro-plastic--dust-when-shredding/cyclone.jpg">
        <img class="step-image" src="/howtos/reduce-micro-plastic--dust-when-shredding/cyclone.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/reduce-micro-plastic--dust-when-shredding/IMG_5416.jpg">
        <img class="step-image" src="/howtos/reduce-micro-plastic--dust-when-shredding/IMG_5416.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/reduce-micro-plastic--dust-when-shredding/IMG_5417.jpg">
        <img class="step-image" src="/howtos/reduce-micro-plastic--dust-when-shredding/IMG_5417.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - The Funnel</h4>
<div class="step-text">
  You&#39;ll need an airtight funnel to capture the shredded plastic as it falls from the sieve into the vacuum hose.<br />
  You can use something purpose built, 3D printed, or a D Square grate and plumbing pipe.<br />
  <br />
  Something like this certainly does the job: <a href="http://www.timbecon.com.au/dust-extractor-hood-large-big-gulp">www.timbecon.com.au/dust-extractor-hood-large-big-gulp</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/reduce-micro-plastic--dust-when-shredding/funnel.jpg">
        <img class="step-image" src="/howtos/reduce-micro-plastic--dust-when-shredding/funnel.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/reduce-micro-plastic--dust-when-shredding/IMG_5413.png">
        <img class="step-image" src="/howtos/reduce-micro-plastic--dust-when-shredding/IMG_5413.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Connect all the bits!</h4>
<div class="step-text">
  Cyclone:<br />
  • Place the template (included with cyclone) in the middle of the lid<br />
  • Mark and drill the bolt holes<br />
  • Mark and cut centre hole<br />
  • Connect the cyclone using the nuts and bolts provided (see images in Step 3)<br />
  <br />
  Funnel:<br />
  • Connect the funnel directly below the shredder sieve. We used a connected a D Square grate, some left over plumbing pipe and cut a whole in the cap (see image in Step 4)<br />
  <br />
  Vacuum connection:<br />
  • Cut the vacuum pipe in half<br />
  • Using one half of the cut vacuum pipe, connect the top outlet of the cyclone to the vacuum <br />
  • Connect the side inlet of the cyclone to the funnel beneath your shredder<br />
  <br />
  Happy shredding!
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>