### Cooling 

 Because we removed the product while it was hot, we need to ensure that the product doesn't warp after being released from the mould. To do this we place it on a jig that restricts the shrinkage. For several bricks, we made several jigs, because it takes time for the bricks to cool down. We keep distance between the jigs, so air can flow between the hot bricks, which helps cooling.
