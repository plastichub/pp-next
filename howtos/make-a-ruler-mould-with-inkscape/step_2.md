### Generate the ruler 

 Vector Ruler Generator is a free and open source tool to generate rulers for etching/laser cutting into various materials

-Go to https://robbbb.github.io/VectorRuler/
-On our ruler we wanted both centimetres and inches and to be roughly 15 cm long
-Select the parameters you want and save them down somewhere you can find them with understandable file names so you know which one's which
