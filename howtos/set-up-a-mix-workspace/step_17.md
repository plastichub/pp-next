### Experiment and have fun 

 Make sure to play around and have some fun trying out new things, moulds, patterns and designs. Learning is about playing and experimenting.

If you discover something cool, share it back to the community, so everyone can learn together :)