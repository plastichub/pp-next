---
image: "/howtos/injection-faceshield/flo2.jpg"
title: "Injection Faceshield"
tagline: ""
description: "With the current, global situation of COVID-19 we face many challenges.<br /><br /><br />One challenge is the short pass and access to safety equipment.<br /><br />With our machines in place the batch production of products like a Face Shield can start immediately.<br /><br />With this Mould you are able to produce around 120 Shields per day, with one person and one machine."
keywords: "injection,product,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "product"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/plasticpreneur">plasticpreneur</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Order the laser cut parts</h4>
<div class="step-text">
  In the Downloadkit you can find all the DXF Files for lasercutting the parts.<br />
  <br />
  I used 6mm steel for the top, bottom and one of the insert plate.<br />
  The other insert plate is made out of 5mm steel so that you can make the holes in the foil with an office punch.<br />
  <br />
  If you are planing to make a bigger production, there is also a CNC Mould attached.<br />
  U can skip to step 4, if u are using the CNC Mould.<br />
  For the CNC Mould you can use 20 - 25mm aluminium.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200330_160826.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200330_160826.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cut the thread</h4>
<div class="step-text">
  Cut M6 threads in the 5mm holes on the insert plates.<br />
  <br />
  You also can drill up the two 5,8mm holes for the metal pin with a H6 reamer in the injection Plate.<br />
  <br />
  <br />
  Then you can drill up the holes in the other insert plate so 6.1 mm
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200330_161842.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200330_161842.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Assemble</h4>
<div class="step-text">
  Mount the plates with M6x12mm screws.<br />
  On the Injection insert plate you have to sand away the screws, since the plate is a little bit thinner.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200330_174445.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200330_174445.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200330_175437.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200330_175437.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Mount the Metal Pins</h4>
<div class="step-text">
  Depending on the pins you get you have to cut them to be shorter than 11mm.<br />
  We use H6x20mm pins and cut them to length.<br />
  <br />
  Punch in the metal pins with a hammer, ore glue them in place if you don´t have a reamer.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200401_173446.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200401_173446.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200401_173443.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200401_173443.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/IMG_0021.JPG">
        <img class="step-image" src="/howtos/injection-faceshield/IMG_0021.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Drill the bevel</h4>
<div class="step-text">
  Since we have a spring clamp system on our plasticpreneur machines, we just have to make a bevel to the mould.<br />
  <br />
  If you are workingg with precious plastic noozle system u have to weld on an adapter.<br />
  Feel free to change my design if needed.<br />
  <br />
  U can use a flat screwdriver to seperate the mould again.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200330_175935.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200330_175935.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200330_183659.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200330_183659.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200330_184401.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200330_184401.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Finish it</h4>
<div class="step-text">
  We allways like to sandblust our moulds to have them nice and clean.<br />
  <br />
  If you are producting the shields for a medical use, make sure, your mould is perfectly clean!<br />
  <br />
  All you need now are some M6x30 screws an some nuts to screw the mould together an you are yeady to go.<br />
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  CNC Mould from Industriedesigner Silke Grimmelmann und Adrian Heymann.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200401_173353.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200401_173353.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/20200401_180231.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/20200401_180231.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/IMG_0020.JPG">
        <img class="step-image" src="/howtos/injection-faceshield/IMG_0020.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Cut the Faceshield</h4>
<div class="step-text">
  There is also a cutting plan for the shiled in the download kit.<br />
  <br />
  We used 0,2 to 0,5mm PET Foil.<br />
  2 liter PET bottles also work great.<br />
  <br />
  You can use a laser or a scissors to cut it.<br />
  <br />
  The holes are designt to can be made with an office puncher.<br />
  <br />
  <br />
  To mount the shield on your face you can use a rubber or an buttonhole tape.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/Foil.JPG">
        <img class="step-image" src="/howtos/injection-faceshield/Foil.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Spread the Vision</h4>
<div class="step-text">
  As Part of the Precious Plastic Community we love to share our ideas.<br />
  <br />
  Feel free to share, to explain people the process.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Fight the Virus</h4>
<div class="step-text">
  If you don´t have acces to a lasercutter or a cnc machine, you also can buy the mould in the bazar.<br />
  <a href="https://bazar.preciousplastic.com/moulds/injection-moulds/faceshield-mould-plasticpreneur-clone/<br/>">https://bazar.preciousplastic.com/moulds/injection-moulds/faceshield-mould-plasticpreneur-clone/<br /></a>
  <a href="https://bazar.preciousplastic.com/index.php?dispatch=products.view&amp;product_id=312<br/>">https://bazar.preciousplastic.com/index.php?dispatch=products.view&amp;product_id=312<br /></a>
  <br />
  If you have any problem with the design, feel free to contact me.<br />
  Email: florian@doingcircular.com<br />
  <br />
  Send us pictures from your finished products on Instagram ;)<br />
  <a href="https://www.instagram.com/plasticpreneur/<br/>">https://www.instagram.com/plasticpreneur/<br /></a>
  <br />
  <br />
  STAY HOME &amp; STAY SAFE!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/flo2.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/flo2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/03.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/03.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-faceshield/overall.jpg">
        <img class="step-image" src="/howtos/injection-faceshield/overall.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>