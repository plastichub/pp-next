### Take a step back and look 

 Finally, the moment is there to use this plastic layout tool. You can use it for example, when a collector comes in your Collection Point and he/she has time to listen to some more information about plastic. Explain how to separate the collected plastic by the categories. 

Tip: Give a hand where needed, it’s fun to do this together.

After sorting you can both take two steps backward and look at the overview of the laid out plastic. Start a little conversation about the result and the differences in the products that you see. It's your moment to shine with all your knowledge about plastic, go for it and share :) If there is even more time you can give some plastic-free examples to reduce plastic use.  

Tip: Let the collector take a photo of the overview to share on, for example, social media.
