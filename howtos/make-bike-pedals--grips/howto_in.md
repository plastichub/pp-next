### Make Bike Pedals & Grips 


In this How-To I describe how I made the mold. 
Injecting the pedal is fast but making the mold takes time. I suggest you to mill the mold if you have the possibility because I welded the mold and this is time consuming. You can download the 3D model and make your own version.