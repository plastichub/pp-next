### Finish the beams 

 After having produced your beams you can now begin preparing them for the bench. Use the
measurements and angles as pictured. Note that the parts may be textured and probably have shrunk in the process of cooling. Each part will be slightly different so take care to match the parts while measuring so they are the same. It is also helpful to de-burr the parts after they have been cut to define the edges.