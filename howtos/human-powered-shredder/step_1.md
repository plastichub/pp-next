### User Requirement Specifications 

 Or URS for short is just a fancy way of saying 'identify and define the characteristics of what you want to make'. In other words, before you build the machine, these decisions will have an influence in the design:

- How to power it. You can use a motor (single phase or three phase?), human power (hand-cranked or pedal powered?), animal power (a little hamster in a wheel?), other natural source of energy (wind or hydro power? nuclear? dark matter?)

- How much use it's going to get. If you plan on using it once a month, perhaps it makes sense to keep it cheap and simple (hand cranked power). If you plan to use it for a few hours a day, you might want something other than your arms to power it.

- What materials do you have available? If you have a salvaged motor maybe it can be adapted. Or if you have an exercise bike that you don't use anymore, you have half the work done for you.

- What skills and processes do you have available? If you can weld, the standard design of the machines would work for you. If like me, you can't, you'll have to outsource the task to someone else (which can cost you more than you'd like) or redesign it.

- Is it going to be used off-grid? If access to electricity is difficult then that's going to drive the design to a non-electric one.