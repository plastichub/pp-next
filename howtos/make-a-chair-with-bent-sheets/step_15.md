### Backrest connector: Drill the locking pin holes 

 Clamp the backrest in a cross table on the drill press to drill the holes for the locking pins (part J). Make sure that the face of the backrest is perpendicular to the drill press table. 

Avoid clamping the piece too tightly as it might crack, hence support the backrest on both sides with two pieces of identical thickness to prevent it from tipping to one side. Precision is crucial in this step! 

Chose a drill around 0.5-1mm smaller than the diameter of the locking pins. Now align the x-axis to the center of the blind holes (step 13) and the y-axis to the middle of the material thickness. Drill until passing the blind holes and further in as much as possible. This is crucial to make the connecting plugs shake proof in both directions.
