### Drilling holes 

 Mark where you want the holes with a center pin along with the sidebars. 

Before removing the tap welds, we are gonna drill 8mm holes along with the sidebars and the sheet to fit 8mm bolts. 
It’s important to keep everything clamped together so we are sure the holes will match after disassembling and reassemble the 2 parts molds!

Now you can grind off the taps weld and take the 2 parts mould apart.
