### Gathering the tools  

 Before we begin make sure you have the right tools.

You will need:
Casting silicone (equal part 1:1)
Measuring jug
Plastic stirrer
Take-away container (recycled of course!)
Clay
Knife
Your chosen object

For this tutorial I have used 500ml of silicone for an object which measures (h)10x(w)2.5x(l)3cm.
Make sure you have a clean work space and all your tools are ready. Silicone sets fast and can be costly to waste