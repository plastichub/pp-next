### Draw the number and write your info 

 You drew the triangle, now its time for the number. In the stencil, you will find all the recycling numbers made in Precious Plastic font, but you can always make it more personal.  Don’t forget to write your Collection Point name! Use the stencil as a ruler if you want to make straight lines. 

Last tip: you can always use different colours if you want to sort your plastic label also by colour.
