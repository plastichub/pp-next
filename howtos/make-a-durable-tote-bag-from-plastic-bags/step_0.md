### Gather everything you need 

 Before you start get all your gear ready:
- Respirator mask
- Lots of plastic PE bags from shopping or supermarkets
- Baking paper
- Cotton sheet
- Iron
- Sewing machine 