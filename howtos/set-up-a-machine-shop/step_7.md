### Different expertises 

 To create a Machine Shop you will need a combination of multiple expertises and, above all, a strong project manager in charge of timely sourcing components, solid planning, coordinate the team, sell machines and close contracts. 

Depending on your project you will manage a team of assemblers, welders, electricians, mould makers and machining experts. 