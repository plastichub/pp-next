### Choose your soldering tip 

 A very stable connection is created when the connection surfaces are first welded deeper and then at the edge. 
For the deeper welding, a thin soldering tip is suitable and for the edge seam a larger one.