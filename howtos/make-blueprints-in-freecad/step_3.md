### Generate a blank template 

 -FreeCAD already comes prepared with various Blueprint sizes and standards
-You can select it by clicking the folder icon in the TechDraw workspace and select the type
-Or you can select the default one by clicking the "Insert new default Page" button in the TechDraw workspace
-You can also make your own as it takes an SVG format so you can adapt it to your needs