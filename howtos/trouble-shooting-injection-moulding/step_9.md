### Air bubbles inside the part 

 In case you process clear material, make parts with integrated function (like flexing) or your parts get post processed by machining you want to avoid that air is trapped within your parts. To verify there are no air bubbles within your parts, cut them into sections carefully!

Solution:
- Decrease the material temperature
- Pre-compress the material with a closed nozzle and make sure the material is fully molten
- Increase the pressure