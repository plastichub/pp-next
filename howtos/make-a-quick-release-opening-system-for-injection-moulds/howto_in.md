### Make a quick release opening system for injection moulds 


Injecting is a slower process compare to the other machines, generally it takes from 10 to 15 min to inject a product. That's why we've been looking for different alternatives to make this process as fast as possible, even for very simple steps like opening and closing. When you inject 3-5 times is not that bad to screw in and out 4-6 bolts, but when you need to do the same step 50 times, to save a couple of seconds each time, it's a lot! 