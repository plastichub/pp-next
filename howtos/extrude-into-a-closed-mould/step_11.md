### Repeat! 

 Want to make more? Good, now all you need to do is repeat steps 5 to 9! Obviously the process can vary with different moulds and we would love to hear and learn from your experiences of this process.

What next?
Check this link and learn how to "Build brick structures":
👉 tiny.cc/build-brick-structures