---
image: /_howto/nozzle/IMG_2263.JPG
category: "injection"
title: "Nozzle"
tagline: ""
description: ""
usedin:
  - injection
  - elena
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2255.JPG" _target="_blank">
        <img id="IMG_2255.JPG" src="./IMG_2255.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2256.JPG" _target="_blank">
        <img id="IMG_2256.JPG" src="./IMG_2256.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2258.JPG" _target="_blank">
        <img id="IMG_2258.JPG" src="./IMG_2258.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2259.JPG" _target="_blank">
        <img id="IMG_2259.JPG" src="./IMG_2259.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2260.JPG" _target="_blank">
        <img id="IMG_2260.JPG" src="./IMG_2260.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2262.JPG" _target="_blank">
        <img id="IMG_2262.JPG" src="./IMG_2262.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2263.JPG" _target="_blank">
        <img id="IMG_2263.JPG" src="./IMG_2263.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

