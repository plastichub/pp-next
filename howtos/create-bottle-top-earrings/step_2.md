### Transfer tiles to the oven 

 Once the oven is preheated, it’s time to put your tiles in and watch the plastic melt! Both tiles will need to be heated, one with the bottle tops prepared (as in the previous step) and the other should be heated for pressing.
