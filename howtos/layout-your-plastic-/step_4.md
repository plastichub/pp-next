### Make a sign for every category  

 In this step, it’s time to visualize each category by an illustration or written sign. This can be done on a piece of paper (something temporary) or being painted on the floor (something permanent). Make sure that you choose a color that has good contrast with the background, this will make the layout clear even when people take photos from far.
