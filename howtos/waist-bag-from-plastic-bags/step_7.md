### Attaching the Strap 

 Next we need to prepare the straps.

The strap is cut into two equal pieces. Before we sew them on, it is necessary to make sure they do not come loose. To do that use the lighter, accurately heat up the ends of the strap. Do not put fire directly on the strap but rather have it close by.

When the straps preparation is done, sew them to the inside of the Back part of the bag. The more threads you put at this point the stronger the bag will be.