### Drill the injection hole 

 Using a 13mm bit, drill a hole in the center of the bottom mould part. This is where the plastic will flow into the mould, out of the injection machine. The center drill indicator is also included in the STEP file. 