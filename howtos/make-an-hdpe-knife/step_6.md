### Cut out the Blade 

 We like to use the scroll saw for this as it creates much less waste. Wherever possible we keep as much of the waste produced for future projects. The cut edges gives a glimpse of the marble effect on the blank!