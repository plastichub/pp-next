<span id="specs" style="padding: 16px">
    <table>
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td>Manual Injection
                </td>
            </tr>        
            <tr>
                <td>Version:
                </td>
                <td>1.3
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Mature
                </td>
            </tr>
            <tr>
            <td> License
                </td>
                <td><a href="https://ohwr.org/cernohl">CERN Open Source Hardware License</a>
            </td>
            </tr>
            <tr>
            <td> Authors </td>
                <td>
                 [PlasticHub S.L.](${author_link}) |
                 [PrecisousPlastic - Eindhoven](${author_link_pp})
                </td>
            </tr>
        </tbody>
    </table>
</span>
