### Inject the parts 

 Time to inject. The plastic should be evenly molten to easily spread within the mould, so it might run out of the nozzle just because of gravity. Use a valve into the nozzle and open it right before you start injecting.Act fast and keep the pressure for a couple of seconds before lifting the lever. This will prevent sink marks as the plastic is cooling down under pressure.