---
image: "/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-1.jpg"
title: "Make your sheet shiny"
tagline: ""
description: "Getting the best look in your sheets is as important as the process of making them. Having a few details in mind will help you to make the most out of your product."
keywords: "PS"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PS"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Different material, different finishing</h4>
<div class="step-text">
  First of all, the finishing potentials and techniques depend a lot on the material.<br />
  Hard materials like PS are easy to polish and it’s just a matter of repeating the process with higher grain sanding paper. For softer materials like PP or HDPE this process makes it more matt instead.<br />
  <br />
  If you really require a shiny result then the best choice is to work with PS.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-2-1.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-2-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-2-2.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-2-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Treat your mould</h4>
<div class="step-text">
  For a high quality product, a high quality mould is the key!<br />
  In order to create a clean and shiny sheet, you need a very smooth and shiny mould. It might take time to get there but it will pay off in the end: The shinier the mould is,the shinier will be the result of your sheets (and less finishing work to do).<br />
  <br />
  Mould material:<br />
  For a shiny plastic sheet we recommend to use a stainless steel mould, as it’s a good material to polish. But take care because it’s also easy to scratch and fairly more expensive than other metals.<br />
  <br />
  Mould treatment:<br />
  We suggest to use silicon oil as mould release in order to protect the mould during the process and help demoulding the material. Also pay attention to clean the mould after every cycle in order to extend the mould life, and to keep a good output quality.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-3-1.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-3-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-3-2.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-3-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - From sanding to polishing</h4>
<div class="step-text">
  For a smooth and even surface we first have to sand it with low density grain sanding paper (from 200-800). This will remove the surface layer and clean the sheet from scratches and tiny holes.<br />
  <br />
  For deeper scratches you’ll have to spend more time or even use lower density sanding paper. Once the scratches are no longer visible you can use higher density grain papers in order to polish the surface.<br />
  <br />
  In the case of hard plastics like PS you can get shiny results while on HDPE or PP they will remain matt.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-4-1.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-4-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-4-2.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-4-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Polish paste to make it shine</h4>
<div class="step-text">
  For the final step we apply polishing paste and carefully finish the surface with a soft sanding disk. Make sure the disk is clean, otherwise the material may change its colour. <br />
  <br />
  Polish until the paste disappears and the result is shiny. Finish by using a clean humid rag to remove any dust left.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-5-1.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-5-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-5-2.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-5-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-5-3.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-5-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-5-4.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-5-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Learn from your results</h4>
<div class="step-text">
  Each material behaves a bit differently, so it&#39;s always a testing and learning which mould material and technique works best for which plastic type.<br />
  <br />
  Here we have two samples:<br />
  A sheet out of a mild steel mould. Polishing it afterwards made it smooth and cleaned it from dirt of the mould.<br />
  A sheet out of a very smooth stainless steel mould. No polishing was needed as it came out very shiny!<br />
  <br />
  Feel free to try out different mould materials yourself and always compare your results to learn which technique works best for different products and plastic types. :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-6.jpg">
        <img class="step-image" src="/howtos/make-your-sheet-shiny/how-to-cut-finish-sheets-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>