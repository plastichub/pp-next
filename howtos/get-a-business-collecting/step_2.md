### Start contacting 

 It’s time to start contacting businesses, go through the list of local businesses and see how you are able to contact them, this can be for example by mail, social media, phone or by a visit on location and asking for the manager. 

Visiting the businesses and asking for the manager does take more time but it makes a big difference if they have seen you already once. Sometimes you are lucky and the manager is available straight away. Make sure that you are prepared for this and take your chance. 

Tip: Start with small businesses to get used to the way of communicating before you go to the big ones. 
