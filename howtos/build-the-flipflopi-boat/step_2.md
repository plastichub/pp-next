### We made a plan 

 With Ali Skanda we made a plan and designed the boat.
The goal originally was (and still is), to build a boat which would be big enough to travel the message to fight single-use plastic and plastic pollution around the world.

Here some of the sketches to get an overview of the boat components.