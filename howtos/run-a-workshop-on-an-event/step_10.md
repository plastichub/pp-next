### Site-specific questions 

 As mentioned before you are leaving your lovely workshop. So sad :(
To be sure that everything runs great with your mobile workshop, you should clarify the following questions with the organizer:

- Power source: Is it strong enough, and how far is the plug?
- You need fresh water? Ask for the next water tap.
- Weather conditions: Is it protected from rain, ventilated with fresh air?
- How much space is available?
- What kind of people are expected?
- When are construction and dismantling times?
- Who is liable if something goes wrong?
