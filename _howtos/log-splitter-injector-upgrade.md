---
image: "/howtos/log-splitter-injector-upgrade/HowToCOVER_InjectionMouldLogSplitter.png"
title: "Log splitter injector upgrade"
tagline: ""
description: "Need a little more power, or just feeling lazy? We&#39;ve upgraded our PP injection mould machine using a 5T log splitter - this has made a world of difference to our production capabilities. In these videos, we discuss how we did it, as well as our process for creating thick parts.<br /><br />Note: THIS UPGRADE IS DANGEROUS! Please take care with hydraulic machinery."
keywords: "injection,hack"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "hack"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/infoplasticorgau">infoplasticorgau</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - How we built it</h4>
<div class="step-text">
  Here, we&#39;re covering the upgrade to the Injection Machine.<br />
  <br />
  You&#39;ll need:<br />
  • A Precious Plastic injection machine<br />
  • A 5T log splitter<br />
  • Ability to cut steel, weld and drill holes<br />
  • Hopper (see .zip file attached)<br />
  <br />
  Below is the direct link to the particular log splitter we used:<br />
  <a href="http://www.bunnings.com.au/homelite-2200w-5t-electric-log-splitter_p0044725">www.bunnings.com.au/homelite-2200w-5t-electric-log-splitter_p0044725</a>
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - How we use it</h4>
<div class="step-text">
  In this video, we walk you through how we use our hydraulic injection moulding machine. We designed this machine to assist us in creating larger, thicker and &#39;trickier&#39; products.<br />
  <br />
  As safety is a concern, it may be helpful to include something to monitor pressure within the barrel. Initially, we were using a load cell, however as it reduced our stroke length, we are now either adding vents to our moulds or dosing the barrel with the correct amount of plastic for each shot.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Build your own!</h4>
<div class="step-text">
  This machine may not suit all workshops, however it has certainly been worth it&#39;s weight in gold for us!<br />
  <br />
  If you plan on upgrading your machine, here are a few things to try:<br />
  • Monitor pressure. We used a load cell (see second image) in our first version, however it shortened our stroke length and we are now weighing each shot<br />
  • Change the orientation to suit your workshop.<br />
  • Create a quick release that will work on a moving barrel (and share the How-To!)<br />
  <br />
  Finally, be careful. We do not accept any responsibly for loss or damages - this &#39;How-To&#39; is intended to be educational not instructional. <br />
  <br />
  Follow us @preciousplasticmelbourne and tag us in your upgrade!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/log-splitter-injector-upgrade/Vert.PNG">
        <img class="step-image" src="/howtos/log-splitter-injector-upgrade/Vert.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/log-splitter-injector-upgrade/V1 splitter.jpg">
        <img class="step-image" src="/howtos/log-splitter-injector-upgrade/V1 splitter.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>