### Outcomes 

 The outcome of an Extrusion Workspace can be recycled plastic beams, bricks or other big objects. 

The beams can be of various sizes and shapes, and as long as needed. Make sure you don't miss playing around with different gradients and colours to make create a playful variety of outcomes.