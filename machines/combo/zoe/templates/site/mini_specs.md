<span id="specs" style="padding: 16px">
    <table class="center specs">
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td> Combination (Shredder & Extrusion)
                </td>
            </tr>
            <tr>
                <td>Version:
                </td>
                <td>3.3
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Mature
                </td>
            </tr>
            <tr>
                <td> License
                    </td>
                    <td><a href="https://ohwr.org/cernohl">CERN Open Source Hardware License</a>
                </td>
            </tr>
            <tr>
            <td> Authors </td>
                <td>
                 <li>[PlasticHub S.L.](${author_link})</li>
                 <li>[PreciousPlastic - Eindhoven](${author_link_pp})</li>
                 <li>[Dan Shirley - Timberstar](https://www.plastic-hub.com)</li>
                </td>
            </tr>
            <tr><td>Weight</td><td>110 kg</td></tr>
            <tr><td>Blade width</td><td>6 mm</td></tr>
            <tr><td>Voltage</td><td>220V | 380V</td></tr>
            <tr><td>AMP</td><td>7.8A</td></tr>
            <tr><td>Nominal Torque</td><td>240 Nm</td></tr>
            <tr><td>Output Speed</td><td>±70 r/min</td></tr>
        </tbody>
    </table>
</span>
