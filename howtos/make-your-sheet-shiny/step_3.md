### Polish paste to make it shine 

 For the final step we apply polishing paste and carefully finish the surface with a soft sanding disk. Make sure the disk is clean, otherwise the material may change its colour. 

Polish until the paste disappears and the result is shiny. Finish by using a clean humid rag to remove any dust left.
