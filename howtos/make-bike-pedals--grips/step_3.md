### Grip mold: cut the parts 

 Cut the tube at the length you want for your grips (mine are 130 mm).
Cut four hexagones with side length 33 mm.
In each of them, drill three holes in the corners.
In the first one, drill another hexagone with 10 mm side length in the center.
In the second one, drill a 5 mm hole in the center.
In the third one, drill a 22.5 mm hole in the center.
In the fourth one, drill a 8 mm hole in the center.
Cut three M5 and one M8 threaded rods at 150 mm.
Cut the cylinder at the length you want (111 mm for me).
Make a M8 x 20 threaded hole at the center of one cylinder side.