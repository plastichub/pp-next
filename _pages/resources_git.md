---
title: About GIT & Github
permalink: /resources/git
sidebar:
    nav: "resources"
---

[About this system]({{ "/resources/library.html" | relative }})

[Software Requirements](https://gitlab.com/plastichub/pp-webinars/tree/master/basics#hardware)

[Basic Commands - Youtube - Video](https://youtu.be/z0qNbGAxVHE)

