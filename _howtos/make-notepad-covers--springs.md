---
image: "/howtos/make-notepad-covers--springs/notebook.jpg"
title: "Make notepad covers & springs"
tagline: ""
description: "How to make small sheets and springs to assemble a notepad"
keywords: "PP,compression,extrusion"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PP"
- "compression"
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/estudio-618">estudio-618</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prepare your mix</h4>
<div class="step-text">
  Select, weight, and mix the colors you want, we used PP shredded bottle caps.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-notepad-covers--springs/IMG_8871.jpg">
        <img class="step-image" src="/howtos/make-notepad-covers--springs/IMG_8871.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Make the springs</h4>
<div class="step-text">
  Fill the injection machine and wait for the plastic to start flowing, with a small nozzle and a constant weight applied on the machine, use a wooden rod to &quot;screw&quot; the filament around it, make it as long as you want, we recommend using a wooden rod because the plastics sort of sticks and makes it easier to form the spring.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-notepad-covers--springs/IMG_8877.jpg">
        <img class="step-image" src="/howtos/make-notepad-covers--springs/IMG_8877.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make the covers</h4>
<div class="step-text">
  Remove the small nozzle from the injection machine and extrude some plastic on a flat sheet, press it against another sheet in a press, wait for it to cool and remove the plastic sheet, cut it to shape, make the holes and assemble the notebook.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-notepad-covers--springs/WhatsApp Image 2021-01-21 at 11.41.16 AM.jpeg">
        <img class="step-image" src="/howtos/make-notepad-covers--springs/WhatsApp Image 2021-01-21 at 11.41.16 AM.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-notepad-covers--springs/WhatsApp Image 2021-01-21 at 11.42.45 AM.jpeg">
        <img class="step-image" src="/howtos/make-notepad-covers--springs/WhatsApp Image 2021-01-21 at 11.42.45 AM.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-notepad-covers--springs/WhatsApp Image 2021-01-21 at 11.43.41 AM.jpeg">
        <img class="step-image" src="/howtos/make-notepad-covers--springs/WhatsApp Image 2021-01-21 at 11.43.41 AM.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Enjoy your recycled booklet</h4>
<div class="step-text">
  Try different combinations, you can make the springs out of the same mixture of however you want :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-notepad-covers--springs/IMG_8879.jpg">
        <img class="step-image" src="/howtos/make-notepad-covers--springs/IMG_8879.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-notepad-covers--springs/IMG_8880.jpg">
        <img class="step-image" src="/howtos/make-notepad-covers--springs/IMG_8880.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-notepad-covers--springs/IMG_8888.jpg">
        <img class="step-image" src="/howtos/make-notepad-covers--springs/IMG_8888.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>