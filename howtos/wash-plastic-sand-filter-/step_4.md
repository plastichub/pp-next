### Build the inside with PVC pipes 

 Drilling holes in the barrel weakens the structure. As there will be pressure of more than 1 bar on it later on, reinforcing that part is needed to avoid damage. 

Cut a thick piece of HDPE roughly 200 x 120 mm and again drill two holes with the 45 mm Forstner drill bit, with the same distance as in the barrel.

As the HDPE sheet is flat and the barrel is round it needs to be brought in to the same shape. Heat up the sheet with a hot air gun (wear a mask!) or in the oven until it is soft enough to be reshaped easily. 

Put on work gloves and use the bulkhead fittings to tighten the soft sheet inside the barrel. This way it will copy the exact form.

Once cooled down remove the bulkhead fittings, apply silicone or HDPE glue to the sheet and tighten again with the fittings. Now let the glue cure and the reinforcement is done.