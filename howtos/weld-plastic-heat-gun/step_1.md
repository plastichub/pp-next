### Gather your tools for welding 

 Welding is a process in which we join two parts together by applying heat and adding a filler of the same material in between the parts.
In order to do this we are using a hot gun with a specific nozzle that helps us to spread the melted material on the surfaces which we want to weld. This nozzle will concentrate the heat on the welding line and will guide the stick in the process. 