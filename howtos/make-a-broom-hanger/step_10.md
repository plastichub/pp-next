### Make end caps 

 Fix each cap (no. 4) in place and drill four 5mm diameter holes. All through the cap and 25mm deep into the female and male mold parts. Tap each hole of the female and male parts with a ¼” thread.
On the caps, re-drill the holes up to ¼” and fix them with the bolts (no. 12).

(Drawings page 13)