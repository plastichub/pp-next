---
image: "/howtos/make-a4-size-clipboard/clipboard.jpg"
title: "Make A4 size clipboard"
tagline: ""
description: "Make original clipboard from recycled plastic material. Board size is 315x230 mm thickness 5mm, 4x radius R8 in corners."
keywords: "HDPE,compression,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "HDPE"
- "compression"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/plastmakers">plastmakers</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prepare your mould</h4>
<div class="step-text">
  I share with you 3D data and blueprints for personal use. <br />
  Material steel, thickness 5mm.<br />
  You can also buy mould or mini press on Bazar.<br />
  <br />
  Recommended mould fasterners: <br />
  M6x45 (at least 4 pieces) <br />
  M6 washers<br />
  M6 nuts
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Prepare material + mould</h4>
<div class="step-text">
  I have already tested HDPE, PP or PLA with good results. <br />
  You can play with different colours and bottle cap design. :) <br />
  You need between 320 - 350 g / clipboard. <br />
  <br />
  Before testing new material type, make a note/ picture how many material did you use. It will help you with future production. <br />
  <br />
  Clean mould from impurities, dust or remaining plastic particles from previous production. (I use smooth sand paper)<br />
  To avoid sticking use mould release or oil. <br />
  Make sure your oil will not get to its smoke point: <br />
  <a href="https://en.wikipedia.org/wiki/Template:Smoke_point_of_cooking_oils<br/>">https://en.wikipedia.org/wiki/Template:Smoke_point_of_cooking_oils<br /></a>
  I recommend oil with smoke point above 230 deg. Celsius
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/material.jpg">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/material.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/coca.jpg">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/coca.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Baking + pressing</h4>
<div class="step-text">
  I use IKEA Görlig electric oven.<br />
  Set up temperature according to your material type.<br />
  For HDPE and PP I use temperature 225 deg Celsius.<br />
  I put mould in the oven for 35 min. <br />
  Set up timer. <br />
  <br />
  <br />
  Attention - use heat resistent gloves when you operate with mould from oven :) <br />
  <br />
  After baking process, press mould. <br />
  Cooling process take 20 - 30 min. <br />
  If you open mould earlier, there is risk of deformation.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/Snímek5_40y4-uz.png">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/Snímek5_40y4-uz.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/Snímek6_vimf-i4.png">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/Snímek6_vimf-i4.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Postprocessing</h4>
<div class="step-text">
  Cut out edges with knife,<br />
  You can sand surface.<br />
  Drill 2 holes M4 (size and location of holes depends on your clip design).<br />
  I prefer screw type rivets - you do not need special tools and you can make easy and quick change board design with clip.<br />
  <br />
  Rivet size:<br />
  <br />
  A: 4mm<br />
  B: 6mm<br />
  C: 10mm<br />
  S: 4mm
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/2.jpg">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/3.jpg">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/sroubovaci-nyt-na-popruhy-duhovy.jpg">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/sroubovaci-nyt-na-popruhy-duhovy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Follow Plastmakers</h4>
<div class="step-text">
  To provide more open source content, I will be happy for share, subscribe, like, follow or comment. &lt;3<br />
  <br />
  More information: <a href="https://linktr.ee/plastmakers<br/>">https://linktr.ee/plastmakers<br /></a>
  <br />
  Thank you.<br />
  <br />
  Tom
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/coca cola.jpg">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/coca cola.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a4-size-clipboard/clipboard.jpg">
        <img class="step-image" src="/howtos/make-a4-size-clipboard/clipboard.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>