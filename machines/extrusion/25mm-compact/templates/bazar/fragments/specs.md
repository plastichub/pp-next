Type | Extrusion Machine
--- | ---
Version | 3.1
Weight | 50 kg
Screw size | 26 x 600 mm compression screw
Max running time | 4H/Day
Voltage | 220V
AMP | 5.8A
Input Flake Size | Medium, Small  
Nominal Power | 800W
Output Speed | 40-140 r/min

<hr/>
**Input & Output**

 **Type:** HDPE, LDPE, PP, PS

 **Flake size:** ~5mm

 **Output:** Depends on nozzle, ±5 kg/h
