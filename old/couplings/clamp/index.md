---
image: /_howto/couplings/clamp/IMG_1943.JPG
category: "zoe"
title: "Clamp"
tagline: ""
description: ""
usedin:
  - lydia
  - zoe
  - couplings
---





<div class="thumbs"><div class="thumb">
    <a href="./0.JPG" _target="_blank">
        <img id="0.JPG" src="./0.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./1.JPG" _target="_blank">
        <img id="1.JPG" src="./1.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./2.JPG" _target="_blank">
        <img id="2.JPG" src="./2.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1939.JPG" _target="_blank">
        <img id="IMG_1939.JPG" src="./IMG_1939.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1940.JPG" _target="_blank">
        <img id="IMG_1940.JPG" src="./IMG_1940.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1941.JPG" _target="_blank">
        <img id="IMG_1941.JPG" src="./IMG_1941.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1942.JPG" _target="_blank">
        <img id="IMG_1942.JPG" src="./IMG_1942.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1943.JPG" _target="_blank">
        <img id="IMG_1943.JPG" src="./IMG_1943.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

