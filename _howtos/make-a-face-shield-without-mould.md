---
image: "/howtos/make-a-face-shield-without-mould/mascara terminada.jpeg"
title: "Make a face shield (without mould)"
tagline: ""
description: "We use the extruder to make the structure of a protective mask, without molds. For this, three strips of 55 Cm, ​​40 Cm and 28 Cm are made. For the protective screen we have used an old folder.<br /><br />&lt;a href=&quot;https://www.youtube.com/watch?v=BTiQqPFE9vs&quot;&gt;https://www.youtube.com/watch?v=BTiQqPFE9vs&lt;/a&gt;"
keywords: "extrusion,PP,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "PP"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/menor-plastic">menor-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Beginning</h4>
<div class="step-text">
  We use the extruder to make the strips that will hold the mask. In case of you don´tt have an extruder you can use the strap of a bag or an old belt. And we will use an old folder for the screen. We will cut the transparent part and then join it to the resulting structure.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/WhatsApp Image 2020-03-24 at 17.10.03.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/WhatsApp Image 2020-03-24 at 17.10.03.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/WhatsApp-Image-2020-03-24-at-17jk.png">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/WhatsApp-Image-2020-03-24-at-17jk.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/WhatsApp-Image-2020-03-24-at-17.27.jpg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/WhatsApp-Image-2020-03-24-at-17.27.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - We make a strips</h4>
<div class="step-text">
  We make a 55 cm long and 1 cm wide strip with the extruder, once the strip has cooled, we surround the head with it to mark where we will close it.and we will use an old folder for the screen. We will cut the transparent part and then join it to the resulting structure.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/midiendo priemera tira.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/midiendo priemera tira.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/tira cerrada.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/tira cerrada.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - We make the holes </h4>
<div class="step-text">
  Then we make two holes through which we pass a string to close it. In our case we have used a fishing line that we collected on the beach.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/WhatsApp Image 2020-03-24 at 17.09.59.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/WhatsApp Image 2020-03-24 at 17.09.59.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/tira cerrada.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/tira cerrada.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - We make the rest of strips</h4>
<div class="step-text">
  We make two other strips, one 40 cm and the other 28 cm. The 40cm screen will display the mask. The 28 Cm will serve to better hold the mask to the head. The holes in the two strips will be made to join them later
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/materiales.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/materiales.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/marczar plastico transparente.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/marczar plastico transparente.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Assembly</h4>
<div class="step-text">
  The structure will be mounted with the three strips and then the screen will be put on with a little tie or a bridle.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/posicion de tiras.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/posicion de tiras.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/Como-atar.jpg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/Como-atar.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/atando la pantalla.jpeg">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/atando la pantalla.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Well done!</h4>
<div class="step-text">
  You made your own face shield
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-without-mould/mascara-terminada.png">
        <img class="step-image" src="/howtos/make-a-face-shield-without-mould/mascara-terminada.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>