### Print your plans 

 Let’s start with preparing the plans to make the jigs and moulds for the chair. Print all the provided plans and templates. Pay attention to set the scale to 100% in the print dialogue!
