### Set up your space 

 Super, you’ve got your Shredder Pro! But a shredder alone is not enough.

Follow our tutorials on how to fully set up your shredder workspace with all the additional tools, furniture and posters needed to make your space ready. 

👉 community.preciousplastic.com/academy/spaces/shredder