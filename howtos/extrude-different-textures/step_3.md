### Cool mould  

 Now you have your material ready you are nearly ready to extrude. Next turn on the extruder to ___ degrees. Before you start extruding obviously make sure your mould is on correctly. Once heated and mould on, your ready to extrude! Start by putting your material in one bit at a time, try to avoid putting too much in at once. Keep an eye out to make sure the hopper always has material in it. Your mould is better if it’s cooler to achieve the texture because it’s the rapid cooling that creates this effect. So depending on the room temperature, unless your mould is somewhere in the burning sun or sauna, it should be cool enough! You can cool it down by placing it into a water container. 

Pro Tip: make sure if your washing the material before putting it into the extruder, always dry it out first because the steam created when extruding can cause the material to sink and create hollow, weaker areas. 



