---
title : PP Library Development and Authoring Setup
permalink: /resources/library
sidebar: 
   nav: "resources"
---

# Brief

The system contains out of a number of command line tools and generates content for various target platforms as the 'library' (what you're looking at now), the bazar (market place), newsletter and a few other systems. Primarly it uses HTML & Markdown files which be inserted into templates(also HTML and Markdown files).


# System Overview

The system consists out of 3 components

1. ['PlasticHub - Language & Tools'](https://gitlab.com/plastichub/lang/tree/master/cli). This are command line tools and primarly generate content for various output targets as Jekyll. It uses templates to generate the content. It also does some conversation as pdf to jpg or resize images.
2. ['PH Products & Library'](https://gitlab.com/plastichub/products). This is the main repository 
containing product descriptions but also the templates for various output targets.
3. [Jekyll](www.jekyll.com) This is a static site generator, using Markdown files to generate a static HTML website. It comes with it's own template language ['Liquid'](https://shopify.github.io/liquid/)

{% include asset_image.html src="/library/LibOverview.png" %}

# Installation

## Requirements

- [Ruby](https://rubyinstaller.org/downloads/) (Install MinGW on windows !)
- [NodeJS](https://nodejs.org/en/download/) (Any 10.x version)
- [VSCode](https://code.visualstudio.com/download)
- [GhostScript](https://www.ghostscript.com/download/gsdnld.html) (needed for converting PDF to JPG)
- [Imagick](https://imagemagick.org/script/download.php) (needed for resizing images)
- [Plastic-Hub Language and Tools](https://gitlab.com/plastichub/lang/tree/master/cli)

## Post Installation

Open a terminal inside this directory and run:

```sh
    sh scripts/install.sh
```

# Working on the files

1. Open a terminal inside this directory and run:

```sh
    npm run serve
```

2. Now you can edit the files in

    - `_machines`
    - `_pages`
    - `_projects`
    - `_howto`

Changes in those will trigger rebuilding the site. You can preview the site in the browser at [http://127.0.0.1:8008/pp](http://127.0.0.1:8008/pp). You can also use the Jekyll admin at [http://127.0.0.1:8008/admin](http://127.0.0.1:8008/admin)


# Site structure and templates

tbc

