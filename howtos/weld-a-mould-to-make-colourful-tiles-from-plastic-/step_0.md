### Gather everything you need 

 Before you start get all your gear ready:
- Respirator mask
- Metal
- Bolts and nuts
- Plastic (PE, PP, PS)
- Angle grinder
- Drill
- Welding machine
- Sanding paper
- Injection machine
- Plastic Type Stamp