### Plan to get clean plastic 

 It’s the Collection Point's responsibility to collect clean and label-free plastic, this is crucial for further recycling. Cleaning all the plastic yourself will require more time, space and setup, so we highly recommend to set up your collection in a way that you don't have to take care of the cleaning. 

The best way to do this is by educating the people that bring you the plastic (we made some posters for you in the download kit). So they’re involved in the process and everyone is doing their part. The shredder workspace will pay less or might not even accept dirty plastic.

