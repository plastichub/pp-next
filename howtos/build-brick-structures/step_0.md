### Before you start 

 Before you start, this how-to pulls on a slightly different set of skills than some of the other guides and assumes you have a basic understanding of construction/building/architecture. If any of the elements of the guide are vague, we recommend you query the wider internet as most of these techniques are based on standard building methods. 
In this how-to, you are going to an assortment of materials, but the basics are plastic bricks, timber planks (of various dimensions), screws, expansion bolts and threaded bar. The main tools you will need are a drill, rubber mallet and saw. 

If you have the capacity, you could also change the timber for plastic beams.
