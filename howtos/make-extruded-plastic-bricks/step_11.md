### Weld each hump individually 

 The main section comes in many parts that we are going to tackle one at a time. The first is the humps. There are two and they are identical. Clamp them together and weld them along the seams. Preserving the corners.  

Like in step 8, use an angle grinder and sander to achieve round and smooth corners.
