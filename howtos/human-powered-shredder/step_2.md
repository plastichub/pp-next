### To weld or not to weld 

 If you can't weld steel, or don't have access to it, there are other techniques that you can use for the frame:

- Steel sections can be fastened with nuts and bolts. You save the welding time, but need to drill holes instead.

- You can use aluminium extrusion T-slot profiles instead, which are easy to bolt and work with.

- Be careful if you use wood as an structural element. Some parts of the shredder may be subject to strong forces that wood may not resist.

As for the shredder itself, it can be modified so that it doesn't need welding, but would require a different technique: precision engineering (holes and dimensions need to be very accurate and perfectly square)