### Building the lid reinforcement 

 The barrel is going to sit inside a metal frame to enable it to hold pressure. The lid is not constructed to keep pressure from the inside, so it needs reinforcement from the top.

A flat surface will ensure the pressure to be distributed equally. To achieve this, cut out two circles of wood or plastic.
Small circle: 	ø 240mm, 9mm high 
Big circle:	ø 260 mm, 18mm high

When they are cut in size lay them in the lid and proceed with building the frame.