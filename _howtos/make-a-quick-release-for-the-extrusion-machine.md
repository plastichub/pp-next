---
image: "/howtos/make-a-quick-release-for-the-extrusion-machine/_DSF0337.JPG"
title: "Make a quick release for the extrusion machine"
tagline: ""
description: "The extrusion machine has the capacity to extrude a lot of plastic very quickly, but what if you wanted to make smaller or bigger items that can’t be screwed on to the barrel. This is why we developed the low tech sliding quick release for all your extrusion needs."
keywords: "injection,extrusion,hack"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "extrusion"
- "hack"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prepare your pieces</h4>
<div class="step-text">
  First off, in this how-to precision is key. We recommend you to take extra care and time making this nozzle adapter. Once ready, this adapter should fit with all of your future products. Prepare the materials you need. Bear in mind that these items are a rough guide and you can use similar items that you may have available to the same effect. <br />
  <br />
  In this How-to we will use:<br />
  A: 2x 5mm plates (65mm x 70mm)<br />
  B: 2x 20mmx 3mm angle bar (70mm)<br />
  C: 2x 5mm strips (70mm x 25mm)<br />
  D: 20mm Galvanised Union<br />
  E: Scrap pieces of metal (approx 20mm x 20mm)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100161EDIT.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100161EDIT.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Rounding the corners</h4>
<div class="step-text">
  Angle bars tend to come with rounded internal corners but we need a snug fit for 1 of our metal plates (part A). To do this we use a grinder to round the edges of the sides (longer edges) so that the plate and the angle bar sit together smoothly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100168.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100168.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100162.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100162.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100167.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100167.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Welding the angle bars</h4>
<div class="step-text">
  Once both sides are grounded take the second plate (part A) and 2 angle bars (parts B) and clamp them together as seen in the picture. A snug fit is required and the angle bars must be pressed firmly and evenly against the plates.<br />
  <br />
  Tack weld the angle bars to the recently added flat plate (the one without the rounded edges). Take this opportunity to make sure that the rounded corner plate can still be removed. It will be tight, that is ok - too loose would be a problem.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100170.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100170.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100174.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100174.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100173.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100173.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Strip weld</h4>
<div class="step-text">
  With both plates in place, and you are confident everything is still tight and fully aligned fully weld the previous tacks welds.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100175.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100175.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Adding stoppers</h4>
<div class="step-text">
  Now take the two small pieces of metal strips (part E) and place them onto one of the ends of your quick release (either side works as long as they are both on the same sides). These pieces will act as a limiter ensuring the future extrusion hole lines up every time. Once aligned weld the stoppers to the angle bar and non-moveable plate. <br />
  <br />
  You may need to clean up the edges with an angle grinder after but this is optional.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110197.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110197.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110188.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110188.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110196.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110196.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Welding strips</h4>
<div class="step-text">
  If you are able to source 10mm steel, then you can skip this step. If not you will need to weld together the two 25mm x 5mm strips (part C). Ensuring they are tightly pressed together.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100177.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100177.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100178.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA100178.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Connecting the strip to the metal body</h4>
<div class="step-text">
  Place the strip metal piece on to the plate not welded to the angle bar. No part of the strip should touch the angle bar. Then weld on the top and bottom ensuring that you weld the strip to only one plate, so you can still slide the mains parts independently.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110197.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110197.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110199.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110199.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110196.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110196.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Drilling extrusion hole</h4>
<div class="step-text">
  With all the parts tightly together now, drill a hole in the centre of the welded metal strip. ø10mm is recommended but depends on your planned product input diameter.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110201.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110201.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110209.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110209.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110208.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110208.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Welding the nozzle</h4>
<div class="step-text">
  Now on the main plate is welded to the angle bar, add your nozzle adapter (part D) to attach the quick release to our extrusion. We used a ½ inch galvanised union, but find the one which fits your extrusion machine. (Note that brass will not work here since you cannot weld brass to steel.)<br />
  <br />
  The union is cut in half and then placed on the centre of the hole. The nozzle is then welded ensuring that no welds touch/damage the threads.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110210.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110210.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110213.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110213.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Expanding the nozzle hole</h4>
<div class="step-text">
  Now that the nozzle is welded, we need to ensure the nozzle channel is smooth and uniform To do this we remove the sliding portion of the quick release leaving only the nozzle portion of the quick release. We then choose a drill bit that will take a fraction of a millimetre off the inside of the nozzle and make our original hole larger using a drill press.<br />
  <br />
  This will ensure a clean passage for our plastic when we extrude and allow us to clean any blockages.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110218.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110218.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110216.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/PA110216.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Smoothing the sliding mechanism</h4>
<div class="step-text">
  Almost ready! Now the only thing remaining is to ensure that the mechanism slots together with ease. You will no longer be able to adjust the nozzle portion, however, the sliding part can be sanded, ground, and polished to ensure a smooth surface is achieved. <br />
  <br />
  The result should be a mechanism that slides into and out of the nozzle with ease but should not be loose. Both plates need to pressed firmly together for the system to work. Test out where the plate gets a bit stuck and then adjust the sides by grinding, sanding and polishing them.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/_DSF6447.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/_DSF6447.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Welding it to your mould</h4>
<div class="step-text">
  Once a smooth motion is achieved, you can weld this piece to your mould permanently. Take care to not block the movement with fresh welds.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/_DSF5541.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/_DSF5541.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - DONE!</h4>
<div class="step-text">
  Great you are ready to extrude!<br />
  <br />
  To use the quick release you have to screw the female section onto your extruder, ensuring you are able to slide in from the top.<br />
  <br />
  When in place you are ready to extrude.<br />
  <br />
  <br />
  You can find a full guide which includes the usage of the quick release in the How to “Extrude into a closed mould”.<br />
  👉 tiny.cc/extrude-into-closed-mould
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-for-the-extrusion-machine/_DSF9612.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-for-the-extrusion-machine/_DSF9612.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>