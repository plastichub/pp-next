### Steel Frame 

 Building the steel frame requires proper knowledge and experience in welding and working with metal. If you’re new in the game, look for someone with experience in metalwork.To simplify the welding process we prepared gauges -> plywood sheets to keep the right distances between the steel tubes and some big 45° angles.

Before welding everything make sure everything is drilled and milled into the steel tubes. Afterwards it's a bit tricky to get them to the right places!

Another advice: welding the m16 nuts; make sure the two nuts are in one alignment. That's important to clamp the rear-wheel right in place. We take a long steel bar (14mm in diameter).
