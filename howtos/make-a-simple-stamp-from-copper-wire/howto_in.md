### Make a simple stamp from copper wire 


It is very important to mark your products with their specific plastic type, so they can be recycled better. There are several ways to do this. Here we show a very simple technique to make your own stamps with a copper wire.

(Alternatively you can also buy plastic type stamps on the Precious Plastic bazar)