### Drilling holes into the base 

 We now need to drill holes for our threaded bar, and counter sink the nuts into the base of the wooden frame at about 1.2m intervals, ensuring extras are placed at the corners and ends of the wall . To do this we use a spade head bit with the same diameter as our 10mm washers  and then drill the 11mm holes for the threaded bar to pass through.
