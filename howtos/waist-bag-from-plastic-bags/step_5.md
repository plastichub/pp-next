### Attach the Back Part 

 With a simple straight stitch, connect the Top and the Back parts together. This step could be avoided if both parts came combined at the preparation stage. Sewing the parts together does help set the shape of the bag and will keep it more sturdy.