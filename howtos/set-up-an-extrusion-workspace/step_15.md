### Build your space 

 Super, you’ve got your Extrusion! But an Extrusion alone is not enough.

You can watch our video on how to fully set up your Extrusion workspace with all the additional tools, furniture and posters needed to make your space ready. 

👉 http://tiny.cc/space-shredder