### Get a hygrometer 

 A hygrometer comes in handy to check the humidity inside the drybox. It is convenient to use a transparent box so you can keep track of the drying process at all time.
