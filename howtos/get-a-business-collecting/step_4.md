### Agree on the conditions  

 After the meeting, you will give the business time to think about the collaboration. After a couple of days, you can arrange a new meeting or visit them again to sign an agreement/little paper that says, what you are going to pick up and when. This is nice from both sides to know what to expect. You can download and example template above. But feel free to change and edit to your local needs.

Tip: Find the right timing to get back in touch. Don’t wait too long but also don’t be too impatient. 

Idea: You can give the business a sticker/sign to show everyone that they are part of the precious plastic universe. 
