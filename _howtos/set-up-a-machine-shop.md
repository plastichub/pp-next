---
image: "/howtos/set-up-a-machine-shop/cover-machine-3.jpg"
title: "Set up a Machine Shop"
tagline: ""
description: "In this How-to we’re going to guide you through all the steps to set up a Machine Shop. Learn about machines, expertise, how to find a space, get your equipment, find customers and connect to the Precious Plastic Universe. <br /><br />Download files: <br /> &lt;a href=&quot;https://cutt.ly/starterkit-machine&quot;&gt;https://cutt.ly/starterkit-machine&lt;/a&gt; <br /><br />Step 1-3: Intro<br />Step 4-9: Learn<br />Step 10-16: Set up<br />Step 17-22: Run<br />Step 23-25: Share"
keywords: "starterkit"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "starterkit"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Role in the Precious Plastic Universe</h4>
<div class="step-text">
  First of all, make sure you had a look at the showcase page of this Starterkit!<br />
  preciousplastic.com/starterkits/showcase/machine-shop<br />
  <br />
  Now about your Role:<br />
  <br />
  Machine Shops provide machines, parts and moulds to individuals and recycling workspaces within the Precious Plastic Universe.<br />
  <br />
  Machine Shops should also reach out to Community Points to connect with the local Precious Plastic community and maybe get help finding people who need machines and parts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/machineshop-role.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/machineshop-role.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Outcomes</h4>
<div class="step-text">
  As Machine Shop your outcomes are machines or machine parts, components and, as a plus, you can offer maintenance as well.<br />
  <br />
  Your hardware and services can be purchased by different workspaces and individuals from the recycling community on the Bazar.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/5e0c30111520073e137116cd_ms-outcomes-p-1600.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/5e0c30111520073e137116cd_ms-outcomes-p-1600.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Is this for you?</h4>
<div class="step-text">
  Setting up a Machine Shop is a very technical role. You need to know how to build, understand your expertise and limits, and surround yourself with trusted partners and suppliers.<br />
  <br />
  On top of providing machines and parts to workspaces and maintain them, Machine Shops also have to establish relationships with the industrial network of their area to procure material and supplies of specific components. <br />
  <br />
  And last but not least, a certain degree of organisation is required as you will probably have to work as a team in a very connected environment.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/cb-visit-machine shop-2.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/cb-visit-machine shop-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Learn about Precious Plastic</h4>
<div class="step-text">
  As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions.<br />
  <br />
  If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.<br />
  👉 preciousplastic.com<br />
  👉 community.preciousplastic.com<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.<br />
  👉 community.preciousplastic.com/academy/universe
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/learn-precious-plastic-universe.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/learn-precious-plastic-universe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Check out your area</h4>
<div class="step-text">
  Get an overview of who and what is already existing in your area. Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. <br />
  <br />
  Make sure not to jam the local network - if there are already many Machine Shops nearby, you may want to collaborate and specialise in one area (assembling, welding, maintenance etc..).<br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/communitypoint-step4-map-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Find a Community Point near you</h4>
<div class="step-text">
  Community Points are the glue of the Precious Plastic Universe. <br />
  <br />
  They know the ins and outs of your local network. They can help you in multiple ways but they generally have a very in-depth overview of local Precious Plastic spaces, people, useful shops, resources and can help you with planning out your project. Have a look on the Map if there is one near you.<br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image15.png">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image15.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Get on Discord</h4>
<div class="step-text">
  Day to day discussions in the Precious Plastic Universe happen on Discord.<br />
  <br />
  Introduce yourself, say hi in your country channel and start to discover the different channels where people go deep into specific topics (building, collection, design etc..)<br />
  <br />
  Join the Discord:<br />
  👉 discordapp.com/invite/rnx7m4t
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image9.png">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image9.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Different expertises</h4>
<div class="step-text">
  To create a Machine Shop you will need a combination of multiple expertises and, above all, a strong project manager in charge of timely sourcing components, solid planning, coordinate the team, sell machines and close contracts. <br />
  <br />
  Depending on your project you will manage a team of assemblers, welders, electricians, mould makers and machining experts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image12.png">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image12.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Machine Shop services</h4>
<div class="step-text">
  As part of a Machine Shop you can offer different services to your customers:<br />
  <br />
  Assembling: Putting together machines using high precision measurement tools, a small lathe or milling machine for adjustments.<br />
  <br />
  Welding: Making the machines’ frames. A semi-professional MIG/MAG and a dedicated area will do the job.<br />
  <br />
  Electrician: Cabling and installing the electric box. To adapt the machine to specific configurations, troubleshooting and to ensure full safety a professional level is required.<br />
  <br />
  Mould Making: Creating precise aluminium moulds with a CNC machine.<br />
  <br />
  Machining: Offering professional-level lathe and milling for manufacturing shredder axis and the main extrusion parts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image20.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image20.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Action Plan</h4>
<div class="step-text">
  Before jumping into making machines or finding a space it is smart to sit down and properly plan your project and shape your vision. <br />
  <br />
  To help you plan we’ve made a tool called the Action Plan that helps you to craft your mission, understand your customers, revenue streams, costs and much more. With this tool, you should get a step closer to create a successful project. <br />
  <br />
  Find the Action Plan in the Download Kit or learn more in the Academy<br />
  👉 <a href="http://tiny.cc/business-actionplan">http://tiny.cc/business-actionplan</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image10.png">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image10.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Workspace Calculator</h4>
<div class="step-text">
  Now you have your project nicely planned out and it’s starting to take shape. <br />
  <br />
  It is important at this stage to make a serious estimation of how much it will cost you to set up and run your workspace. Otherwise, you might run out of money halfway. The Workspace Calculator is a spreadsheet that helps you do just that. <br />
  <br />
  Find the Workspace Calculator in the Download Kit or learn more in the Academy:<br />
  👉 <a href="http://tiny.cc/workspace-calculator">http://tiny.cc/workspace-calculator</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image2.png">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image2.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Business Plan Template</h4>
<div class="step-text">
  To help you pitch your idea to potential partners, financial institutions or investors we made a Business Plan Template (and a specific example for the Machine Shop) for you to use. <br />
  <br />
  This template helps you to talk the business language and should help you access the necessary money to begin.<br />
  <br />
  For more explanation check out the video in the Academy:<br />
  👉 <a href="http://tiny.cc/business-plan-calculator">http://tiny.cc/business-plan-calculator</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image11.png">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image11.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Assemble a team</h4>
<div class="step-text">
  We’ve heard it over and over. People are what makes a project succeed or fail. Choose carefully and create a team of passionate people that want to change the world and are not afraid of sleepless nights.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image13.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image13.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Get the machines</h4>
<div class="step-text">
  Cool, now you have a space and team. <br />
  <br />
  It’s time to procure your equipment, in the tools list from the Download Kit, we provide the list of basics tools and machines you will need for starting a Machine Shop.<br />
  <br />
  We recommend investing in good quality machines. If you want to reduce your investment, keep an eye on second-hand markets, you might find proper machinery for decent prices. They will generally keep their value, last longer, and will be easier to repair due to parts availability, so the investment will eventually pay off.<br />
  <br />
  If you want to run a specialised Machine Shop or expand its capabilities, you might need to upgrade it depending on your needs.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/machines.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/machines.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Build the workspace</h4>
<div class="step-text">
  As you will probably have different operations running at the same time, we recommend your workspace to be organised and split your space into at least two areas:<br />
  <br />
  A “dusty” area where you will cut, drill grind, weld etc. dedicated to heavy messy jobs. Ventilation is a must here. <br />
  <br />
  And a &quot;clean&quot; area where you carry out assembling and electricity cabling. Here you can also dedicate space to neatly store parts and components.<br />
  <br />
  Ideally, you also have a &quot;ventilation&quot; area for paint jobs (sand blasting and painting).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/machineshop-space.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/machineshop-space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Build partnerships</h4>
<div class="step-text">
  The Machine Shop needs to procure many machined parts, laser-cut pieces and components. <br />
  <br />
  For this, the project manager needs to set up connections with the local industrial network to find partners that can provide reliable orders at acceptable prices. Ordering regularly or in bulk will help to maintain the prices low. <br />
  <br />
  If parts are not available locally at reasonable prices, make sure to check the Precious Plastic Bazar: Some other Machine Shop might specialise into specific components and sell them, saving you precious time and money.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/CB-visit-waste1500.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/CB-visit-waste1500.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Buy in bulk</h4>
<div class="step-text">
  It’s often good to buy materials and parts in bulk as it can help you save money, packaging and shipping. However, this can be a little difficult in the beginning as it requires some initial capital to invest. <br />
  <br />
  We don’t recommend to accept orders/payments and then buy in bulk, this could result in delayed shipping and angry customers. If you do want to take this approach make sure to mention it before you sell, to prepare everyone for longer waiting times and keep your customers happy :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/bulk.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/bulk.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Build in series</h4>
<div class="step-text">
  Once you feel confident in bulding the machines it is good practice to build them in series. This will increase efficiency and speed up your building time.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/machineshop-series.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/machineshop-series.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Other specialists</h4>
<div class="step-text">
  On top of the Machine Shop’s expertise, there are a number of other technical expertise that you can find in your local industrial network, which you might need at some point.<br />
  <br />
  Try to map out machining specialists, lasercut and waterjet services, mould makers, CNC gurus and shops selling specific components. These are very specialised sectors that can offer you high precision, better prices and less hassle. <br />
  <br />
  Also make sure to note and keep together all the contacts you find. Makes it easier for you to get back to them and also easier to share with your team and your local recycling network.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/sk-ms-landscape-01.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/sk-ms-landscape-01.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Sell</h4>
<div class="step-text">
  You can now make great machines in series. At this point, it is crucial to find people and organisations that will buy your recycling machines. <br />
  <br />
  Put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. You could alternatively also try to sell the machines locally but this might be a little harder.<br />
  <br />
  *try to avoid shipping machines across the planet as this increases CO2 pollution<br />
  <br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/machineshop-sell.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/machineshop-sell.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Create your profile</h4>
<div class="step-text">
  If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform to connect with people. Follow the link below and sign up with your email, pick your role, put your pin on the map and upload nice pics to show the world what you’re doing.<br />
  <br />
  👉 community.preciousplastic.com/sign-up
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image17.png">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image17.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Create How-tos!</h4>
<div class="step-text">
  Share to the world how you run your Machine Shop so other people can learn from you and start using your solution to build machines to tackle the plastic problem. <br />
  <br />
  Make sure to only create How-tos for your best processes and techniques, no tryouts or one-offs. This can also help you create a name for yourself in the Precious Plastic community. <br />
  <br />
  👉 community.preciousplastic.com/how-to
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/image18.png">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/image18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Learn and improve together</h4>
<div class="step-text">
  Precious Plastic is nothing without the people. Working together and helping each other. Participate in the community, share back and make use of it, it can really pay off!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/howto-collection-discord.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/howto-collection-discord.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-24">Step 24 - Good things take time</h4>
<div class="step-text">
  Starting off will take some time in the beginning. It’s normal. Be patient, work smart and reach out to your Precious Plastic community if you need help. Everything will take off. <br />
  <br />
  You’re changing the world.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/Machine-Shop--takes-time.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/Machine-Shop--takes-time.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-25">Step 25 - Download and start!</h4>
<div class="step-text">
  Ready and excited to start?<br />
  You&#39;re a hero!<br />
  <br />
  In this download package you will find all the files you need to set up your Machine Shop.<br />
  Download and start your recycling journey!<br />
  <br />
  👉 <a href="https://cutt.ly/starterkit-machine">https://cutt.ly/starterkit-machine</a> 👈
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-machine-shop/Machine Shop Starter Kit.jpg">
        <img class="step-image" src="/howtos/set-up-a-machine-shop/Machine Shop Starter Kit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>