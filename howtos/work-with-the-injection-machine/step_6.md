### Attach the nozzle connector 

 As a nozzle connector (see figure 1 and 2) on the machine side normally a female G 1/2" pipe thread (red) is used. A mould needs a counter-fitting thread or a separate nozzle adapter which fits. We use a male G1/2" to male G1/2" adapter (yellow). 
Note that the standard of G and R thread type are compatible.

If there is no separate nozzle adapter used on the mould, take care that you do not over tighten the nozzle connector on the mould side. If you ruin a thread in a aluminium mould, the mould needs expensive and difficult repair. For this reason, try to leave the adapter attached to the mould at all times, so the thread in the mould does not get worn out.

To prevent damage to the thread over a large number of injections (for events or small series production) change your nozzle adapter to a slider connector ( https://community.preciousplastic.com/how-to/make-a-quick-release-for-the-extrusion-machine ) or use additional thread adapters which can be swapped easily (see figure 3).