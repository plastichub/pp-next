### Modifications on the machine 

 After clearing up the machine, we can start with our modifications.
First, attach the solenoid valve to the drum inlet and tighten it well. Here we will later connect the water pump.
Second, we need to connect to a signal from the machine to control when the pump will turn on and off, so it doesn’t have to run nonstop. In this case, we found the signal in a cable going to the machine’s small solenoid valve. Split the cable and connect your own.
❗Careful here, this signal can be 220Volt! 💀
Now we guide out the cables from the valve and signal so we can connect it later to our electronics box. Once that’s done we cut a hole in the lid of the machine and put it back on.
