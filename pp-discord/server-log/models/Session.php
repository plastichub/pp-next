<?php
namespace SR;

require __DIR__ . './../vendor/autoload.php';

class Session extends \WeDevs\ORM\Eloquent\Model
{

    /**
     * Name for table without prefix
     *
     * @var string
     */
    protected $table = 'sessionreplay';

    public $referer;
    public $payload;
    public $visit;
    public $tags;
    public $events;
    public $status;
    public $session;

    protected $casts = [
        'events2' => 'array',
    ];

    /**
     * Disable created_at and update_at columns, unless you have those.
     */
    public $timestamps = true;

    /**
     * Set primary key as ID, because WordPress
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Columns that can be edited - IE not primary key or timestamps if being used
     */
    protected $fillable = [
        'events',
        'status',
        'flags',
        'session',
        'referer',
        'visit',
        'tags'
    ];

    /**
     * Overide parent method to make sure prefixing is correct.
     *
     * @return string
     */
    public function getTable()
    {
        //In this example, it's set, but this is better in an abstract class
        if (isset($this->table)) {
            $prefix = $this->getConnection()->db->prefix;
            return $prefix . $this->table;

        }

        return parent::getTable();
    }

}
