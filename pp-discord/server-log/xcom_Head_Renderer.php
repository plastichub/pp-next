<?php
/**
 * @version 0.1.0
 * @link https://github.com/mc007
 * @author XApp-Studio.com support@xapp-studio.com
 * @license : GPL v2. http://www.gnu.org/licenses/gpl-2.0.html
 */

//////////////////////////////////////////////////////////////////////////////////
//
//  Killswitchs
//
//////////////////////////////////////////////////////////////////////////////////
$screen=(array)get_current_screen();
if(!$screen['base']==='toplevel_page_xfile')
    return;
