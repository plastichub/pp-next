#ifndef ENUMS_H
#define ENUMS_H

enum BOARD
{
  UNO,
  MEGA
};

enum POS3_DIRECTION
{
  UP = 1,
  MIDDLE = 0,
  DOWN = 2,
  INVALID = -1
};

enum ADDON_FLAGS
{
  DEBUG = 1,
  INFO = 2,
  LOOP = 3,
  DISABLED = 4,
  SETUP = 5,
  MAIN = 6,
  STATE = 7
};

enum ADDONS
{
  MOTOR_IDLE = 1,
  MOTOR_LOAD = 2,
  VFD_CONTROL = 4,
  DIRECTION_SWITCH = 5,
  MOTOR_SPEED = 7,
  POWER = 11,
  OPERATION_MODE_SWITCH = 20,
  HOPPER_LOADED = 21,
  PLUNGER = 24,
  APP = 25,
  LAST = 64
};

enum OPERATION_MODE
{
  OP_NONE = 0,
  OP_NORMAL = 1,
  OP_MANUAL = 2
};

enum ERROR
{
  ERROR_OK = 0,
  ERROR_WARNING = 1,
  ERROR_FATAL = 2
};

#endif
