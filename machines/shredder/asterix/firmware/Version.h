#ifndef VERSION_H 

    #define VERSION_H 

    #define VERSION "1.0.0|PlasticHub|OSR|Asterix|7a0bc49fb310fab856f6457facbe81330c13107c"

    #define SUPPORT "support@plastic-hub.com | www.plastic-hub.com"
    #define BUILD "Asterix-RevA"
    #define FW_SRC "https://gitlab.com/plastichub/pp-next/tree/master/machines/shredder/asterix/firmware/firmware"
    #define CID "-1"

#endif