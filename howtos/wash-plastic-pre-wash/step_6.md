### Attach the motor 

 Attach the motor parallel to the mixer shaft. The position and the way how you do it depends a lot on your motor, but in general it is pretty straight forward.

Once the motor is fixed, connect it with the frequency inverter, so you are able to change the speed later on.