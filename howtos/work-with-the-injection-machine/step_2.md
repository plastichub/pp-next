### Fix the machine 

 It is useful if you can bolt the machine to the floor, a wall or add a pallet under it to prevent it from falling over during the injection process.