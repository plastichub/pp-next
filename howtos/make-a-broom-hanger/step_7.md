### Drill the conical guides holes 

 Align in place the female and the male parts of the mold and fix them with a small press or locking pliers. Apply the the hole positions from the drawings to the face of part no. 2 and drill two 9,5mm diameter holes. Drill through no. 2 and 1cm deep into no.1.

(Drawings page 10)