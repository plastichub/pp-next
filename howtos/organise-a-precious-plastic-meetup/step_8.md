### Meet up time! 

 Time to meet! The following steps are based on the agenda we added in the download files. You can do exactly the same, but feel free to change it or add your own ideas so it fits to your context.

Try to be there earlier than the meetup time so you can already prepare a bit. Greet the people when they arrive, introduce yourself and make sure they feel comfortable.

When everyone is there, welcome everyone and give an overview of the agenda, the purpose of the meeting and how long it will probably take, so they know what to expect.
