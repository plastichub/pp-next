---
image: "/howtos/make-a-mould-to-bend-sheets/Leon Chair Mould-2.jpg"
title: "Make a mould to bend sheets"
tagline: ""
description: "Bending plastic sheets is a relatively easy technique, at least compared to metal and wood. Once you have a mould you can use it over and over again to get the exact same shape. This specific how-to aims to build a simple mould from plywood and sheet metal for bending sheets in one direction. Steps 1-4 guide you through the design and preparation process, while steps 5-14 explain how to build the actual mould."
keywords: "PS,sheetpress"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PS"
- "sheetpress"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prototyping</h4>
<div class="step-text">
  It’s kind of obvious, but first things first: before making a proper mould, you should figure out what exactly you want to achieve. It’s easy to build a rough prototype from plywood or cardboard to test proportions and ergonomics. So right after sketching out your design, go and test it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/01_1.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/01_1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/01_2.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/01_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/01_3.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/01_3.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - 3D modelling</h4>
<div class="step-text">
  Take all the measurements you derived from your prototype and feed it to CAD. For curvatures, take a quick photo, place it in CAD, scale it to the right size and rebuild the curve from there.<br />
  The mould consists of two parts, the positive and the negative half that encapsulate your desired part from both sides. The bending surface is a 1mm sheet metal screwed onto cross-sections cut from plywood. The surface offset of both halves should be set to the material thickness of your plastic sheets. <br />
  See the attached .step file for an example mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/02_1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/02_1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Making the drawings: 3D to vector</h4>
<div class="step-text">
  The aim of the drawing is to transfer the precise measurements and curvatures from your CAD model to a physical mould. A good way to do that is to make 2D drawings, print them, laminate them onto plywood and cut them with a bandsaw or jigsaw. While you’re making the drawings for each part, try to anticipate how you are going to use those drawings. A smart way of avoiding a large format print is to figure out how to put together several standard sized prints. Anyways, you cannot rely on the printer to align your print perfectly on the page, so add an alignment line on every drawing.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/03_1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/03_1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Making the drawings: layout</h4>
<div class="step-text">
  Besides that, every sheet should contain some sort of part number or description, not to mix them up later. When moving from one application to another or while printing, pay extra attention to always maintain the same scale!<br />
  Use dashed or colored lines for different purposes (e.g. cutting, aligning, centerline …) and hatched areas for offcuts. The lines you print should be as thin as possible and use pure CMYK tones (e.g. 0 0 0 100 for black). This will avoid imprecise lines due a potential registration misalignment in the printer.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/04_1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/04_1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Preparing the material</h4>
<div class="step-text">
  Get all the dimensions from your CAD model and cut all parts at the same time to speed up the process. Drill holes parallel to the long edge of the sheet metal with half the cross-sections material thickness as an offset.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/05_1.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/05_1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/05_2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/05_2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Cutting the cross-sections</h4>
<div class="step-text">
  Cut the drawings as you planned before. Use a ruler and a sharp knife and work precisely. Then take the previously cut plywood parts and laminate them with the drawings. If there are several drawings for one part, starting from the center. Apply spray mount on the back of one sheet, align it according to the instructions and sweep from the middle outwards.<br />
  Drill holes for the jigsaw to dive in and cut along the lines. Keep in mind what you want to keep and what’s going to be off-cut. Smoothen the edges with sandpaper.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/06_1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/06_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/06_2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/06_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/06_3.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/06_3.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Assembling the frames</h4>
<div class="step-text">
  Screw together as you planned before. In most of the materials you should pre-drill to avoid cracking! Hence it’s a lot faster to use two cordless drills at the same time. Don’t put the wedges to the positive mould, you will need them in step 9.<br />
  As well attach some guides like aluminium L-bar to the corners in order to align the two halves more easily later. While doing so, place both halves on top of each other to make sure the whole frame is square.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/07_1.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/07_1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/07_2.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/07_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/07_3.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/07_3.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Making the positive mould: screws</h4>
<div class="step-text">
  Mark the centerline on the sheet metal, align it to the frame and attach it with screws. Then bend it downwards around the curvature and fix it every once in a while.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/08_1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/08_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/08_2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/08_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/08_3.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/08_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Making the positive mould: wedges</h4>
<div class="step-text">
  As soon as you’re close to the end, you can use the previously cut pieces to wedge the sheet metal in place. Fix those wedges with care as they are under a constant tension and might flip out.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/09_1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/09_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/09_2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/09_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/09_3.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/09_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Making the negative mould: screws</h4>
<div class="step-text">
  The process is pretty much the same as for the positive mould, you start as well from the middle. Here, you just have to clamp the sheet metal down first and attach it in the middle. Then again, go outwards, the only difference is that no wedges are necessary.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/10_01.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/10_01.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/10_02.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/10_02.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/10_03.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/10_03.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Making the negative mould: trim</h4>
<div class="step-text">
  In case the sheet metal exceeds the curvature’s length, you can easily grind it off. It’s crucial that the flat wedges of both halves come nicely together, otherwise the offset is not right and might deform your plastic sheet.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/11_01.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/11_01.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/11_2.JPG">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/11_2.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Cut guides</h4>
<div class="step-text">
  When it comes to the bending, you’re handling a 200°C hot piece of malleable plastic, so you really want to make it as quick as possible. A guide can help you to align the piece within the mold. In this case we simply use the off-cuts from step 5. Cut them to the right length and remove the screws from the mould on both halves where you want to put the guide. The sheet metal will still stay in shape thanks to the tension.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/12_1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/12_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/12_2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/12_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/12_3.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/12_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Attach guides</h4>
<div class="step-text">
  Then mark the position of the screws, pre-drill and countersink the holes. Screw through the holes and the sheet metal into the cross-section using countersunk screws. Again, the screws should fully immerse in the material as the opposite half must not interfere with them.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/13_1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/13_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/13_2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/13_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/13_3.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/13_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Ready to bend some sheets!</h4>
<div class="step-text">
  Good job, you’re done! For the usage of your brand new mould, check out 👉 tiny.cc/bend-plastic-sheets.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-bend-sheets/final.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-bend-sheets/final.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>