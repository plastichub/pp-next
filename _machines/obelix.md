---
image: /machines/shredder/obelix2021//renderings/perspective.JPG
category: "shredder"
title: "Dual Axis Shredder - Obelix"
product_rel: "/machines/shredder/obelix2021/"
tagline: ""
description: "Precious Plastic - Machine : Shredder :: Dual Axis Shredder - Obelix"
product_id: obelix
tags:
 - shredder
alternatives: 
  - zoe
wiring: true
teaser: "Dual axis shredder"
product_perspective: "/products/obelix/media/perspective.JPG"
gallery:
 - url: "/machines/shredder/obelix2021/media/IMG_3140.JPG"
   image_path: "/machines/shredder/obelix2021/media/IMG_3140.JPG"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/media/latest.jpg"
   image_path: "/machines/shredder/obelix2021/media/latest.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/media/shipping.jpg"
   image_path: "/machines/shredder/obelix2021/media/shipping.jpg"
   alt: ""
   title: ""
gallery_drawings:
 - url: "/machines/shredder/obelix2021/drawings/100_ShredderBoxAssembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/100_ShredderBoxAssembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/160_MobileKnifes.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/160_MobileKnifes.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/160_Stationary.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/160_Stationary.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/190_Sieve.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/190_Sieve.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/350_UpperHopperAssembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/350_UpperHopperAssembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/600_TableAssembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/600_TableAssembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/700-BinAssembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/700-BinAssembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/dimensions.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/dimensions.JPG"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/dimensions2.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/dimensions2.JPG"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/DriveShaft.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/DriveShaft.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/Main endpanel Assembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/Main endpanel Assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/Motor bay Assembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/Motor bay Assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/Motor mount Assembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/Motor mount Assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/parts.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/parts.JPG"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/Side Assembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/Side Assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/Table base assembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/Table base assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/shredder/obelix2021/drawings/Table top assembly.PDF"
   image_path: "/machines/shredder/obelix2021/drawings/Table top assembly.jpg"
   alt: ""
   title: ""
---
<div prefix="/templates/jekyll/" file="machine.html" context="machine global" class="fragment">
    
    <div prefix="/templates/jekyll/" file="machine_layout.html" context="machine global" class="fragment">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
        </li>
        {% if page.tabs.build != false %}
        <li class="nav-item">
            <a class="nav-link" id="build-tab" data-toggle="tab" href="#build" role="tab" aria-controls="build" aria-selected="false">Build</a>
        </li>
        {% endif %}
        <li class="nav-item">
            <a class="nav-link" id="resources-tab" data-toggle="tab" href="#resources" role="tab" aria-controls="resources" aria-selected="false">Resources</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="howtos-tab" data-toggle="tab" href="#howtos" role="tab" aria-controls="howtos" aria-selected="false">Howtos</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#media" role="tab" aria-controls="media" aria-selected="false">Media</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#discussion" role="tab" aria-controls="media" aria-selected="false">Discussion</a>
        </li>

        {% if page.wiring %}
        <li class="nav-item">
            <a class="nav-link" id="wiring-tab" data-toggle="tab" href="#wiring" role="tab" aria-controls="wiring" aria-selected="false">Electronics</a>
        </li>

        {% endif %}


    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="overview" role="tabpanel" aria-labelledby="overview-tab">

            {% include download_spec.html %}

            <div prefix="machines/shredder/obelix2021/" file="overview.html" context="machine" class="fragment">
    <div>
        <a href="">
            <img src="" style="margin:8px; float: left;max-width:50%;max-height: 400px;" />
        </a>

        <span style="font-size: smaller; margin-top: 16px;">
            Dual Axis Shredder - Obelix
        </span>

        <div style="display: table-cell;">
            <div prefix="machines/shredder/obelix2021/" file="mini_specs.md" context="machine" class="fragment">
    <p><span id="specs" style="padding: 16px">
            <table>
                <tbody>
                    <tr>
                        <td>Type:
                        </td>
                        <td> Shredder
                        </td>
                    </tr>
                    <tr>
                        <td>Version:
                        </td>
                        <td> 4.1
                        </td>
                    </tr>
                    <tr>
                        <td> Status:
                        </td>
                        <td> Tested
                        </td>
                    </tr>
                    <tr>
                        <td> Authors </td>
                        <td>
                            <a href="https://preciousplastic.com/">Precious Plastic - Eindhoven</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </span></p>
</div>

            {% include machine_toolbar.html download="https://precious-plastic.org//archives/obelix.zip" preview="" %}

        </div>
    </div>

    <div style="margin: 8px;clear:both">

    </div>

    <hr />

    <div prefix="machines/shredder/obelix2021/" file="features.md" context="machine" class="fragment"></div>

    <hr />

    <div prefix="machines/shredder/obelix2021/" file="academy_overview.md" context="machine" class="fragment"></div>

    <hr />

    
</div>
            <hr />
            
            <hr />

            <div prefix="/templates/jekyll/" file="used_in.md" context="machine global" class="fragment">
    <p>{% if page.usedin %}
    <h4>This is used in</h4>
    <div class="ty-vendor-plans small">
        {% for component in page.usedin %}
        {% for doc in site.machines %}
        {% if doc.product_id == component %}
        <div class="ty-grid-list__item_small">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover_small" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    {% endif %}</p>
</div>

        </div>

        {% if page.tabs.build != false %}
        <div class="tab-pane" id="build" role="tabpanel" aria-labelledby="build-tab">

            

            <div prefix="machines/shredder/obelix2021/" file="build.md" context="machine" class="fragment">
    <h2 id="general">General</h2>
    <p><a href="https://docs.google.com/spreadsheets/d/1bsABuw7DK893pk66kJFMCaeYyWwoFdA1Yb9VoP8c17k/edit?usp=sharing">BOM - Bill of materials</a></p>
    <p><a href="">Build Instructions</a></p>
</div>

            {% if page.gallery_drawings %}
            <h4>Drawings</h4>
            {% include gallery_drawings caption="" %}
            {% endif %}

        </div>
        {% endif %}

        <div class="tab-pane" id="resources" role="tabpanel" aria-labelledby="resources-tab">
            <span style="font-size:smaller">

                <div prefix="machines/shredder/obelix2021/" file="product_resources.md" context="machine" class="fragment">
    <h3 id="productresources">Product Resources</h3>
    <h4 id="build">Build</h4>
    <ul>
        <li><a href="">3D Preview</a></li>
        <li><a href="">Drawings</a></li>
        <li><a href="">CAD model</a></li>
        <li><a href="">Source files</a></li>
        <li><a href="https://docs.google.com/spreadsheets/d/1bsABuw7DK893pk66kJFMCaeYyWwoFdA1Yb9VoP8c17k/edit?usp=sharing">BOM - Bill of materials</a></li>
        <li><a href="https://gitlab.com/plastichub/firmware/tree/master/shredder-extrusion/zoe">Better auto-reverse</a></li>
    </ul>
    <h4 id="support">Support</h4>
    <ul>
        <li><a href="">Discord Chat</a></li>
        <li><a href="https://preciousplastic.com/starterkits/showcase/shredder.html">Starterkit / Howto </a></li>
    </ul>
    <hr />
    <p></p>
</div>

                

                <h4>Changelog</h4>

                <div prefix="machines/shredder/obelix2021/" file="changelog.html" context="machine" class="fragment"></div>

            </span>

        </div>
        <div class="tab-pane" id="howtos" role="tabpanel" aria-labelledby="howtos-tab">
            <div prefix="machines/shredder/obelix2021/" file="howtos.md" context="machine" class="fragment"></div>
            <h4>Build Howtos</h4>
            <div prefix="/templates/jekyll/" file="howto_used_in_machine.md" context="machine global" class="fragment">
    <div class="ty-vendor-plans">

        {% for doc in site.howto %}
        {% if doc.usedin and doc.enabled!=false %}
        {% for used in doc.usedin %}
        {% if used == page.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endif %}
        {% endfor %}
    </div>
</div>
        </div>
        <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="media-tab">
            
            {% if page.gallery %}
            {% include gallery caption="" %}
            {% endif %}

            {% if page.gallery_social %}
            <h4>Media on the network</h4>
            {% include gallery_social caption="" %}
            {% endif %}

        </div>

        <div class="tab-pane" id="discussion" role="tabpanel" aria-labelledby="discussion-tab">
            <div id='discourse-comments' style="min-height: 600px;"></div>
            <script type="text/javascript">
                DiscourseEmbed = {
                    discourseUrl: 'https://forum.precious-plastic.org/',
                    discourseEmbedUrl: '{{site.url}}{{page.url}}.html'
                };

                (function() {
                    var d = document.createElement('script');
                    d.type = 'text/javascript';
                    d.async = true;
                    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
                })();
            </script>
        </div>

        {% if page.wiring %}
        <div class="tab-pane" id="wiring" role="tabpanel" aria-labelledby="wiring-tab">
            
        </div>
        {% endif %}

    </div>

    <div prefix="/templates/jekyll/" file="alternative_machines.md" context="machine global" class="fragment">
    <p>{% if page.alternatives %}</p>
    <h3 id="alternatives">Alternatives</h3>
    <div class="ty-vendor-plans">

        {% for doc in site.machines %}
        {% for alternative in page.alternatives %}
        {% if alternative == doc.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    <p>{% endif %}</p>
</div>
</div>


</div>