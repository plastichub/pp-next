---
image: "/howtos/make-glass-like-beams/howto-beams-glass-0-3.jpg"
title: "Make glass-like beams"
tagline: ""
description: "Creating with recycled plastic offers a big variety of colours and transparencies.<br />In this how-to we will show how you can transform discarded Polystyrene into a beam with a glass like colour and finish."
keywords: "PS,melting,extrusion"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PS"
- "melting"
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  Alright so before starting, here some basics to check out and prepare first.<br />
  <br />
  For this technique you will need:<br />
  Machines: Shredder (or already shredded plastic), Extrusion machine, Oven<br />
  Tools: Scissors / Knife, Pliers, Weighing scales and two containers, Silicone oil, Clamps, Spanner<br />
  Materials: Polystyrene and pigment<br />
  <br />
  If you haven&#39;t extruded beams before, learn more in the How-to &quot;Make a mould for beams&quot;.<br />
  (https://community.preciousplastic.com/how-to/make-a-mould-for-extruding-beams)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-1-1.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-1-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Stay safe</h4>
<div class="step-text">
  When melting plastic, bad fumes can be released. In order to work safer, make sure to use a breathing mask with ABEK filters to prevent inhaling possibly toxic fumes. Special attention on plastics like PS (polystyrene) and PVC. Also when handling with heated elements and plastic we recommend to wear working gloves.<br />
  <br />
  Recommended safety equipment: ABEK mask, gloves and glasses
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-2.JPG">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-2.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare your polystyrene</h4>
<div class="step-text">
  Selecting the right material is crucial when it comes to aesthetics. What properties does the material have, what colour is it? <br />
  As PS stays transparent in the recycling process, it has the perfect quality to make a light. Collect your clean PS, which is typically found in old CD and cassette cases. Make sure to shred enough, the required amount will vary depending on the size of your mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-3.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Prepare your colour</h4>
<div class="step-text">
  Now we use pigment to add colour to the transparent polystyrene. You can find it online (search for &#39;pigment for polystyrene&#39;), but do check out if you can find it in a local shop. Using pigment has the advantage that it keeps the translucency of your material. If you only have access to coloured PS plastic, you will probably not get the same effect (unless the plastic is also translucent), so this will need some testing.<br />
  Start by cutting one pellet of pigment into smaller pieces. The pigment we use (___) has a really strong affect so you only need a small amount to mix with.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-4-1.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-4-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-4-2.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-4-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Weigh your quantities</h4>
<div class="step-text">
  Controlling your quantities is important to get your desired colour and effect. You can weigh a piece of a previously made beam to get an idea of the required total weight.<br />
  Weighing out the quantities of your materials into separate containers will enable you to have more control of your result.<br />
  <br />
  In the next steps we will show the mixtures and results of two processes:<br />
  Mixing clear PS with pure pigment (Step 7)<br />
  Mixing clear PS with a PS-pigment-mix (Step 8)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-5.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Get ready for extruding</h4>
<div class="step-text">
  Alright, let’s get everything ready to melt! <br />
  First, turn on the extruder and set the temperature to 230°C. <br />
  And then there are a few tricks to prepare your mould for a smoother process and outcome. We recommend to coat the inside of the mould with silicone oil to help getting the beam out of the mould after it cooled down. And in order to achieve a smooth material surface it helps to heat up your mould with a heat gun or warm it up in an oven for around __ minutes before attaching it to the preheated extruder.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-6.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Extrude (1) - Concentrated colours</h4>
<div class="step-text">
  Time to melt!<br />
  Mixture: 95% clear PS, 5% blue cut-up pigment<br />
  <br />
  Put your clean PS in your extruder hopper and sprinkle the smashed pigment in evenly. Do not add all of your mix in at once, gradually sprinkle making sure there’s enough mixture in the hopper. Keep an eye on the other end of the mould and stop the extruder as soon as you see material approach the end.<br />
  <br />
  It will come out as a strong concentrated colour. To create a cloudy pastel colour, we will mix this material again in a further process (Step 8).<br />
  <br />
  Note: Collect your excess material and squish it flat before it hardens, so it will be easier to shred it again for further processes.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-6-1.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-6-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-6-2.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-6-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-6-3.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-6-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Extrude (2) - Cloudy colours</h4>
<div class="step-text">
  Now we use the re-shredded excess material from the previous process which makes the colour more cloudy. In this example we used the following mixture:<br />
  Mixture: 70% clear PS, 30% shredded PS-pigment-mix<br />
  <br />
  Start by putting your clear PS into the extruder followed by the PS-pigment-mix. You can experiment with adding more colour by putting pigment back into this mixture to create dynamic colour tones and patterns.<br />
  <br />
  P.S. Keep collecting your left over material to melt it again, this is the stuff that will make your material glass-like.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-8-1.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-8-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-8-2.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-8-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-8-3.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-8-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Explore the possibilities!</h4>
<div class="step-text">
  Et voilá , this is how you create beams and materials with magical colours, transparencies and patterns. As the mixing process is hard to control, achieving exactly the same outcome is unrealistic. But the beauty of melting plastic this way is that each piece will be unique! The transparency of PS offers itself perfectly for lighting, like shown in our how-to &quot;Make a lamp from PS&quot;.<br />
  <br />
  But the further possibilities of this material can be taken into many other directions.<br />
  Have fun exploring!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-9-1.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-9-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-glass-like-beams/howto-beams-glass-9-2.jpg">
        <img class="step-image" src="/howtos/make-glass-like-beams/howto-beams-glass-9-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>