### Play with the colours :) 

 You're done!

Depending on your LED strip you can now program your light, intensity, color, etc. with Arduino. 

A guide explaining how to program the board can be found here: http://bit.ly/2QQHZV9
You can find the relevant Arduino code in the download files.

Once your board is programmed then you can change the LED settings from your phone connecting to the board’s wifi. 

Well done you made it!
Plugin your light and hang it to your ceiling!