### Cut, drill, weld and paint 

 Follow the dimensions shown in the download files and cut your materials to size. Once cut you can drill the parts as needed.

With your parts ready, position them together and weld. 

After welding you can add some paint for a better look and extended life.