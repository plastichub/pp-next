### Sheetpress v4.1 


We have build the sheetpress system with some upgrades which we believe are worth sharing! In short:

- Shared our labour hours for realistic price estimation
- Making all sheets & parts suitable for lasercutting and sheet metal bending
- Updated the bom-list
- Made changes to the circuit diagram
- Etc,

See files below for more information
