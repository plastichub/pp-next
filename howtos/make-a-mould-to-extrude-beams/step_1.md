### Cut beam 

 After planning your mould and gathering your materials, it's time to cut them up. Cutting your metal tube is simple enough - make it as long as you want your beam. Take note that the longer your beam, the harder your machine will need to work to fill the entire mould.

Pro tip: It's important to remove any burrs and file the edges here, as this will ensure a better fit in the stages to come. Cut a thin slice extra to use as a template to use in the step 4. 