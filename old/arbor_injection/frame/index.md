---
image: /_howto/arbor_injection/frame/IMG_2277.JPG
category: "arborinjection"
title: "Frame"
tagline: ""
description: ""
usedin:
  - elena
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2266.JPG" _target="_blank">
        <img id="IMG_2266.JPG" src="./IMG_2266.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2269.JPG" _target="_blank">
        <img id="IMG_2269.JPG" src="./IMG_2269.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2270.JPG" _target="_blank">
        <img id="IMG_2270.JPG" src="./IMG_2270.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2271.JPG" _target="_blank">
        <img id="IMG_2271.JPG" src="./IMG_2271.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2272.JPG" _target="_blank">
        <img id="IMG_2272.JPG" src="./IMG_2272.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2274.JPG" _target="_blank">
        <img id="IMG_2274.JPG" src="./IMG_2274.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2275.JPG" _target="_blank">
        <img id="IMG_2275.JPG" src="./IMG_2275.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2276.JPG" _target="_blank">
        <img id="IMG_2276.JPG" src="./IMG_2276.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2277.JPG" _target="_blank">
        <img id="IMG_2277.JPG" src="./IMG_2277.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

