### Get your shredder 

 Cool, now you have a space it’s time to get hold of your Shredder Pro. There are three ways to do that:

1 Build it yourself following our tutorials
👉 tiny.cc/build-shredderpro

2 Buy it on the Bazar.
👉 bazar.preciousplastic.com

3 Find a Machine Shop near you on the map that can build it for you.
👉 community.preciousplastic.com/map