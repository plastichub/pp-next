### Make a clamp for the mould  

 We used plates of metal either side of the mould to apply an even pressure but also wick away some of the heat. You could design bolt slots in the mould halves to clamp them directly. 
Initially our moulds had through-bolts which was of course silly and we quickly moved to a significant 10mm steel plate either side of the mould clamped together with M10 bolts. This setup becomes really slick as you can slide the moulds out of the clamp easily, separate the part and slot it back together when complete. 