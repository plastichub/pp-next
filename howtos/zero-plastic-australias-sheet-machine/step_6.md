### FAQs 

 You might have a few other questions with this as it is a big process but once you work it out it is very easy to do with hardly any labor time needed when making the sheet. 

1.How to adjust the height of the machine? this will come in the manual with the Heat press but it is just the screw at the top of the machine. 
2. can you use different plastics? Yes but i have only been collecting HDPE. please share the run time and temp if you use another type of plastic 
3. What can you use these sheets for? have a look at the photos attached or visit our social pages for further updates Zero Plastics Australia

If you need any other questions please email us at zero.plastics.australia@gmail.com