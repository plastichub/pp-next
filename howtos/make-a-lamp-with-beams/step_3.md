### Cut the rest of the material 

 Grab the 2 metal bars and cut them as long as the first piece. 

Cut the sheet to the right length and width so it fits the u-profile and the bars on top of it.

Now cut the mounting brackets that will connect the mould to the nozzle.

Related link:
Extrude beams 👉 tiny.cc/extrude-beams