### Incomplete fill of the mould 1 

 If your mould is not filled completely.

Solutions:
- Check if there is enough material in the barrel at the next try
- Check if your injection machine volume is at least +20% larger than your mould volume
- Increase the speed between opening the injection chamber and injecting the plastic
- Check there is no "cold material" at the nozzle front
- Increase material temperature
- Increase mould temperature
- If your mould volume is just under the machine volume, you may need to pre-compress the material several times with a closed nozzle to densify the material
- Increase Pressure (Take care to not damage the machine by the use of lever extensions etc. !)