### Connecting the top plate 

 Next you will need to connect the top plate together using screws and depending on the screw length you may need to drill a hole into the wood to allow the screws to join the wood. This will ensure the top plate acts as a single unit and will be strong enough when we tighten down the structure in the following steps. 
