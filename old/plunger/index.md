---
image: /_howto/plunger/IMG_2254.JPG
category: "injection"
title: "Plunger"
tagline: ""
description: ""
usedin:
  - injection
  - elena
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2245.JPG" _target="_blank">
        <img id="IMG_2245.JPG" src="./IMG_2245.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2247.JPG" _target="_blank">
        <img id="IMG_2247.JPG" src="./IMG_2247.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2248.JPG" _target="_blank">
        <img id="IMG_2248.JPG" src="./IMG_2248.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2249.JPG" _target="_blank">
        <img id="IMG_2249.JPG" src="./IMG_2249.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2252.JPG" _target="_blank">
        <img id="IMG_2252.JPG" src="./IMG_2252.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2253.JPG" _target="_blank">
        <img id="IMG_2253.JPG" src="./IMG_2253.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2254.JPG" _target="_blank">
        <img id="IMG_2254.JPG" src="./IMG_2254.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

