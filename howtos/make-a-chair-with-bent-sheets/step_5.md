### Build the mould 

 The last thing is to build the mould to bend the plastic sheet. You can find the building process for this mould in the How to make a mould to bend sheets 👉 tiny.cc/mould-to-bend-sheets.