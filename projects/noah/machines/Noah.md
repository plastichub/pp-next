# Overview
Noah is a macro-scale 3D printing concept.

Noah is based around the idea of using universal scaffolding tubes, beams and fittings to create the structural framework of a large Cartesian machine. Motion is achieved using roller wheels, and motors controlled by computer. The print head will house a plastic extruder inspired by the Precious Plastics designs, allowing very large objects to be 3D printed with recycled materials.

The objective of the Noah concept is to deliver large scale, low cost and adaptable 3d printing, using mostly components which are commonly available. 

The development version of Noah described below has a print bed area of around 7mx7m but it will be possible to expand the size almost indefinitely due to the end-to-end spigot couplings available for the rail beams.

## Components

### [Frame](./noah/documents/frame/readme.md)

- Structural Framework and Y axis rails
- Gantry framework and X axis rails

### [Motion](./noah/documents/Motion/readme.md)

- Motors
- Rollers, pulleys and cables

### [Print head](./noah/documents/printhead/readme.md)

- Print head framework and Z axis mechanism
- Chassis and enclosure
- Extruder, Extruder Motor and Gearbox
- [Feed mechanism](./noah/documents/printhead/feed-system/readme.md)

### [Control](./noah/documents/control/readme.md)

- Controller Hardware
- Sensor/Encoder feedback 
- User Interface
- Safety devices
- Power Supply and Invertors, Power cables, Signal cables, Cable management
- Controller Software
- Connectivity and File Transfer
- Translation of G-code, Compensation, Calibration

### Applications

- Marine
- Civil engineering
- Automotive
- Architecture

## References

- [Universal Tube and Clamp Scaffolding System](https://en.wikipedia.org/wiki/Tube_and_clamp_scaffold)
- Alloy Beams (aka. "truss" beams, Lahyer beams, needle beams etc.)
- [Scaff Rollers and Sidewinders](www.scaff-rollers.com)
- [Precious Plastics Extruder](https://preciousplastic.com/en/videos/build/extrusion.html)
- Controller Hardware and [Software](http://linuxcnc.org/)

## Research

- Structural design and engineering calculations
- Motion hardware and calculations
- Extruder and Feed mechanism
- Controller hardware and sensors
- Software and connectivity devices

### Existing large scale 3d printers and other related projects

- [D-Shape](https://dshape.wordpress.com/company/company-vision-2020/) - probably the first really large 3d printer
- [Vanplestik](https://vanplestik.nl/en/about_us/) - large scale 3d printing wth reycled plastics
- [Remake Plastics](https://www.remakeplastics.com/) - 3d printing recycled plastics with an extruder mounted on a robotic arm
- [University of Maine](https://umaine.edu/news/blog/2019/10/10/umaine-composites-center-receives-three-guinness-world-records-related-to-largest-3d-printer/) - the largest ever 3d printed boat, using an ndustrial gantry robot
- [Trash2Cash](https://www.alchemi.tech/research) - large scale Delta-style 3d printer using recycled plastics
- [Wasp](https://www.3dwasp.com/en/giant-3d-printer-bigdelta-wasp-12mt/) - large scale Delta printer
- [Mahor extruder](https://mahorxyz.wordpress.com/2016/05/02/pellet-extruder/) - a print head for desktop 3d printers which prints with finely shredded flakes of recycled PET

## Todos

- Further develop 3d model of structure; estimate mass, deflection, costs etc
- Fabricate full sets of Scaff Rollers for testing; ship to developers
- Continue research into filament-fed extruders; Report
- Continue research into optical linear sensors and closed loop positional feedback; Report
- Continue research into Controller hardware and software; Report
