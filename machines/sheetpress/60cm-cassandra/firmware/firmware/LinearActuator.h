#ifndef PLUNGER_H
#define PLUNGER_H

#include <Streaming.h>
#include <Controllino.h>

#include "config.h"
#include "Addon.h"
#include "common/macros.h"
#include "config.h"
#include "types.h"

class LinearActuator : public Addon
{
public:
  class LimitSwitch
  {
  public:
    LimitSwitch(short _pin) : pin(_pin),
                              ts(0){};
    bool value;
    byte pin;
    millis_t ts;
    void loop()
    {
      if (millis() - ts > LIMIT_SWITCH_INTERVAL)
      {
        value = analogRead(pin) > 700 ? false : true;
        ts = millis();
      }
    }
    void setup()
    {
      pinMode(pin, INPUT);
    }
  };

  enum E_ACTUATOR_STATE
  {
    NONE = 0,
    PLUNGING = 100,
    STOPPED = 200,
    HOMING = 300,
    RETRACT = 350,
    MANUAL = 360,
    ERROR = 400
  };

  enum E_ACTUATOR_FLAGS
  {
    INITIATED = 1,
    MOVING = 2,
    RETRACTING = 3,
    RETRACTED = 4,
    FREEING = 5,
    DONE = 6,
    PAUSED = 7
  };

  LinearActuator(
      short _limitUp,
      short _limitDown,
      short _motorDirPin,
      short _motorStepPin,
      short _motorAlarmPin,
      short _motorPowerPin,
      Addon *owner,
      AddonFnPtr _onChange,
      millis_t _maxMoveTime) : Addon(LINEAR_ACTUATOR_STR, ACTUATOR),
                               _state(NONE),
                               _last_state(NONE),
                               owner(owner),
                               onChange(_onChange),
                               u1(LimitSwitch(_limitUp)),
                               l1(LimitSwitch(_limitDown)),
                               motorDirPin(_motorDirPin),
                               motorStepPin(_motorStepPin),
                               motorAlarmPin(_motorAlarmPin),
                               motorPowerPin(_motorPowerPin),
                               maxMoveTime(_maxMoveTime)
  {
    // setFlag(DEBUG);
    setFlag(INFO);
  }
#ifdef HAS_STATES
  String state();
#endif
  void debug(Stream *stream);
  void info(Stream *stream);

  short setup();
  short loop();

  short plunge(short val = 0);
  short home(short val = 0);
  short test(short val = 0);
  short retract();
  short reset(short val = 0);
  short stop(short val = 0);
  bool pause();
  bool resume();
  short move(short dir);

  short getSate()
  {
    return _state;
  }

private:
  short _last_state;
  bool change(short newState);

  LimitSwitch u1;
  LimitSwitch l1;

  short motorStepPin;
  short motorDirPin;
  short motorAlarmPin;
  short motorPowerPin;

  millis_t maxMoveTime;
  millis_t moveStartTS;

  AddonFnPtr onChange;
  Addon *owner;
  short pFlags;
  bool retracting;
  short _state;

protected:
};

#endif
