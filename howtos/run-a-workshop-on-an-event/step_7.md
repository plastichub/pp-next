### Typical questions asked at a workshop in public: 

 During the process most of the people have a lot of questions. We recommend that you always have an answer to the following question:
 
- What’s your story/backround?
- What’s your goal?
- What’s precious plastic?
- Can you be booked
- What type of plastic are you able to recycle?
- Are the fumes toxic?
- What is plastic? (ingredients)
- How can I change my lifestyle into an ecofriendly lifestyle?
