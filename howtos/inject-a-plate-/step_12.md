### Label your plastic! Stamp the plate! 

 Ideally, at this point you should add a plastic type symbol to your product to show what plastic is made from!
This is important so that later on people know which type it is, and it can be recycled again.
There are different techniques you can use to stamp your material. 
Here we used the Precious Plastic stamps (You can get them online on the Bazar).

