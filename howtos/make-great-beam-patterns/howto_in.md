### Make great beam patterns! 


People ask me occasionally how to get beautiful patterns in the beams and the funny thing is that zero technical tricks are used to make that happen! The used technique for making beams is exactly the way Precious Plastic does it, using a simple steel pipe as a mold and let the extruder do the work :-) 
Here is what you can try to get the same results!