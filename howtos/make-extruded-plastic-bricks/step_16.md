### Adding bolt release mechanism 

 To ensure we can always take the mould apart we need to install a set of bolts that prevent the mould from being stuck together. To do this, we disassemble the top section of the mould (Part A). Taking part AD5x1, thread a bolt through the bottom of the two 12mm holes and tighten the nut until it is placed firmly on the surface of the part. Then weld the nut that is on the top surface of the mould, ensuring no weld touches the nut.

Repeat with this process with part AA5.
