### Attaching the chain 

 The chain attached to the shredder is the connecting element that allows the bike to shred and so it is important to ensure the gearing is correct and the chain is tight once installed. I used a quick release chain as this allowed me to quickly adjust the chain to the correct length then thread it around the gears before tightening.

In my process I did not use a derailleur to keep the chain tight instead I used the adjust-ability of the support frame to tighten the chain by raising the Seatpost that was connected to the shredder support base and so pulled the shredder up tightening the chain. 
