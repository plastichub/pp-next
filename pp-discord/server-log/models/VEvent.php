<?php
namespace SR;

require __DIR__ . './../vendor/autoload.php';

class VEvent
{
    public $viewId;
    public $type;
    public $time;
    public $duration;
    public $sn;
    public $payload;
    public $tabId;
    public $times;

    public function __construct(
        $viewId,
        $type,
        $time,
        $duration,
        $sn,
        $payload,
        $tabId,
        $times) {

        $this->viewId = $viewId;
        $this->type = $type;
        $this->time = $time;
        $this->duration = $duration;
        $this->sequenceNo = $sn;
        $this->payload = $payload;
        $this->tabId = $tabId;
        $this->times = $times;
    }

    public function getEnd()
    {
        return $this->time + $this->duration;
    }

    /*
    public compareTo(o: VisualEvent) {
    const isEqualObj = this.type === o.type;
    if (this.equals(o)) {
    return 0;
    } else if (this.time === o.time && !isEqualObj) {
    return 1;
    } else {
    return compare(this.time, o.getTime());
    }
    }

    public equals(obj: any) {
    if (obj === null) {
    return false;
    }

    // if (getClass() != obj.getClass()) {
    //    return false;
    // }

    const event: VisualEvent = obj as VisualEvent;
    return this.viewId === event.viewId && this.time === event.time && this.type === event.type;
    }
     */

    /*
    public int hashCode() {
    const result = type.hashCode();
    result = 31 * result + viewId.hashCode();
    result = 31 * result + Objects.hashCode(time);
    return result;
    }
     */

    /*
@Override
public String toString() {
StringBuilder sb = new StringBuilder("replayElement {")
.append("viewId: ").append(viewId)
.append(", type: ").append(type)
.append(", time: ").append(time)
.append(", duration: ").append(duration)
.append(", sequenceNo: ").append(sequenceNo)
.append(", payload: ").append(payload)
.append(", tabId: ").append(tabId);
return sb.toString();
}
 */
}
