### Welding the mould 

 Next we're going to weld it all together. One of the easiest ways to go about this is to attach the brackets to the plate with bolts. You can then clamp the brackets to the beam and tack them together. It's important that you only weld the brackets to the beam. 

Then position where you want your plastic to enter the mould and weld the threaded nozzle to the to other side of the plate.