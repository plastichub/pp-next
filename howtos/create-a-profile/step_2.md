### Choose your focus 

 Choose your focus. Most likely you are a workspace or machine shop (since all the other ones are new for the platform). It might be hard to choose. Here you can find some more information on how to choose.

Info: Currently it’s not possible to select multiple focus activities - if you are active in more areas (collection + shredding), create one account for each.
