# DC Servo control for Noah

[Dcservo](https://github.com/misan/dcservo) is an Arduino-based servo driver which allows an ordinary brushless DC motor to be used as a servo. 

On each of 3 axes, Noah will use an individual Arduino Micro running dcservo to recieve positional information from linear encoder and send PMW to a high power H-bridge driving each motor.


The approach described below using dcservo aims to give closed loop control of the largest brushless DC motors possible at the lowest cost.



## Components - Candidates:

### Controller: 

- [Arduino Mega](https://store.arduino.cc/arduino-mega-2560-rev3) £35, clones around £13; running Marlin firmware

### 3x Servo Drivers:  

-[Arduino Micro](https://store.arduino.cc/arduino-micro)s £18 each = £56;
 (or Pro Micro clone as little as £2.50 each) - Running dcservo
 
### 6x Linear Encoders:  

[KY-032](https://www.amazon.co.uk/Electrely-Avoidance-Photoelectric-Reflection-Detecting-Green/dp/B07G38MXW8/ref=sr_1_fkmr0_2?keywords=IR+Infrared+Obstacle+Avoidance+Sensor+6+x&qid=1574100459&s=electronics&sr=1-2-fkmr0)  IR reflective sensors ~£10 /set

 ### 3x Motor Drivers:
 (H-bridge units also known as ESC)
 
 [XY-1620010-50V](https://www.aliexpress.com/item/4000183522391.html) 7500W 200A (so they claim); £70 each = £210

Possibly the open source [VESC](http://vedder.se/2015/01/vesc-open-source-esc/) would be capable of the task? [Article with video](https://hackaday.com/2017/09/11/open-source-high-power-ev-motor-controller/)

A commercially available high-power unit based on VESC is the [water cooled VESC6 from Flipsky](https://flipsky.net/collections/electronic-products/products/high-current-fsesc-200a-60v-base-on-vesc6), which can deliver 200A constant and 800A burst curent. (Also available as [non-water cooled](https://flipsky.net/collections/electronic-products/products/dual-fsesc6-6-plus-based-on-vesc6)

### Brushless DC Motors:

Industrial style: (Mid priced but heavy)

[BM1412ZXF](https://www.aliexpress.com/item/32764420340.html?spm=a2g0o.productlist.0.0.659da3c7lRzaap&algo_pvid=3698dd2d-6207-4169-bbe5-86c944bddf68&algo_expid=3698dd2d-6207-4169-bbe5-86c944bddf68-21&btsid=490d82d8-7d55-4007-a6f6-b964f2a91650&ws_ab_test=searchweb0_0,searchweb201602_2,searchweb201603_52) 1200W 72V £200 each = £600

Sport/leisure vehicle style: (light but expensive)

[70131](https://flipsky.net/collections/e-skateboard/products/flipsky-brushless-inrunner-motor-70131-150kv-12kw-for-e-foil-foils-lift-foils) from Flipsky - light and high power £254 each = £762

[Reconditioned electric vehcle motors](https://www.secondlife-evbatteries.com/accessories/meiden-ev-motor-60kw-14000rpm-137nm-electric-project-emrax-replacement.html) from SecondLifeEV 60kw 137Nm £600 each



## Schematic

![LAYOUT](./LAYOUT.jpg)



## Similar projects:

[RAMPSSB](http://scottziv.com/rampssb-bringing-closed-loop-dc-motor-control-to-3d-printers/?unapproved=3940&moderation-hash=622f57f1ee5d360fa9583dd5785de340#comment-3940) 
RAMPSSB uses cheap, low power L298N h-bridges is designed to run small motors as servos.
- [VIDEO](https://www.youtube.com/watch?time_continue=10&v=hFBo3W_ZEq8&feature=emb_logo) is a shield extension for RAMPS which gives 2-axis servo control

- [Github](https://github.com/SZiv/RAMPSSB)
