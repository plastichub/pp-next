---
image: /_howto/extrusion/barrel/flange/IMG_2510.JPG
category: "extrusion"
title: "Flange"
tagline: ""
description: ""
title: "Barrel Flange"
usedin:
  - lydia-v4
  - zoe
  - extrusion-pro
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2489.JPG" _target="_blank">
        <img id="IMG_2489.JPG" src="./IMG_2489.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2490.JPG" _target="_blank">
        <img id="IMG_2490.JPG" src="./IMG_2490.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2501.JPG" _target="_blank">
        <img id="IMG_2501.JPG" src="./IMG_2501.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2503.JPG" _target="_blank">
        <img id="IMG_2503.JPG" src="./IMG_2503.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2504.JPG" _target="_blank">
        <img id="IMG_2504.JPG" src="./IMG_2504.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2505.JPG" _target="_blank">
        <img id="IMG_2505.JPG" src="./IMG_2505.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2506.JPG" _target="_blank">
        <img id="IMG_2506.JPG" src="./IMG_2506.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2507.JPG" _target="_blank">
        <img id="IMG_2507.JPG" src="./IMG_2507.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2508.JPG" _target="_blank">
        <img id="IMG_2508.JPG" src="./IMG_2508.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2509.JPG" _target="_blank">
        <img id="IMG_2509.JPG" src="./IMG_2509.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2510.JPG" _target="_blank">
        <img id="IMG_2510.JPG" src="./IMG_2510.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

