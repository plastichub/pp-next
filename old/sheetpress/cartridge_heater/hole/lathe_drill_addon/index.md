---
image: /_howto/sheetpress/cartridge_heater/hole/lathe_drill_addon/IMG_1743.JPG
category: "sheetpress"
title: "Lathe Drill_addon"
tagline: ""
description: ""
usedin:
  - sheetpress-cell
  - sheetpress
---



<div prefix="_howto\sheetpress\cartridge_heater\hole\lathe_drill_addon" file="header.md" context="md:thumbs" class="fragment"></div>

<div class="thumbs"><div class="thumb">
    <a href="./IMG_1735.JPG" _target="_blank">
        <img id="IMG_1735.JPG" src="./IMG_1735.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1736.JPG" _target="_blank">
        <img id="IMG_1736.JPG" src="./IMG_1736.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1737.JPG" _target="_blank">
        <img id="IMG_1737.JPG" src="./IMG_1737.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1739.JPG" _target="_blank">
        <img id="IMG_1739.JPG" src="./IMG_1739.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1740.JPG" _target="_blank">
        <img id="IMG_1740.JPG" src="./IMG_1740.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1743.JPG" _target="_blank">
        <img id="IMG_1743.JPG" src="./IMG_1743.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

