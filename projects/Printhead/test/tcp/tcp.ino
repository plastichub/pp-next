#include <Controllino.h>
#include <SPI.h>
#include <Ethernet.h>

#include "Mudbus.h"

Mudbus Mb;

int D0 = CONTROLLINO_D0;
int D1 = CONTROLLINO_D1;
int R0 = CONTROLLINO_R0;
int A0_ = CONTROLLINO_A0;

//Function codes 1(read coils), 3(read registers), 5(write coil), 6(write register)
//signed int Mb.R[0 to 125] and bool Mb.C[0 to 128] MB_N_R MB_N_C
//Port 502 (defined in Mudbus.h) MB_PORT

void setup()
{
    uint8_t mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};
    uint8_t ip[] = {192, 168, 1, 111};
    uint8_t gateway[] = {192, 168, 1, 1};
    uint8_t subnet[] = {255, 255, 255, 0};
    Ethernet.begin(mac, ip, gateway, subnet);

    delay(5000);
    Serial.begin(19200);
    Serial.println("Started");

    pinMode(D0, OUTPUT);
    pinMode(D1, INPUT);
    pinMode(R0, OUTPUT);
    pinMode(A0_, INPUT);
}

int l = 0;

void loop()
{

    Mb.Run();

    analogWrite(D0, Mb.R[0]);
    digitalWrite(R0, Mb.R[2]);
    Mb.R[1] = l + 4;
    Mb.R[3] = l + 2;

    l++;
    if (l % 100)
    {
        Serial.println("l");
    }
    if(l > 5000){
        l = 0;
    }
    delay(1);
}