### Time to bend 

 Once the piece is warm enough it’s time to bend. If the piece is soft enough it shouldn’t take much effort to bend it.

Take in consideration that if the plate is thicker than 10mm some deformation may appear on the sides of plate due compression of the material in the inner part of the radius. This could be solved with a half cut along the bending edge to reduce the amount of material compressed.

In order to achieve a good bending, hold the piece in the desired position until the plate cools down. If the plate has been heated correctly, it will keep its shape.