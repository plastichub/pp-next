### Sewing the Top Part to Zipper 

 Mark the middle of the curve of the top part of the bag and arrange the middle of the zipper to align with the mark. To sew the parts, put the face of the zipper side with the side that you would want to keep as the outside of the bag together and sew them together with your favourite type of stitch.