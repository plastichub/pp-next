### Create How-to's! 

 Share to the world how you run your Mix Workspace so other people can learn from you and start using your solution to build machines to tackle the plastic problem. 

Make sure to only create How-tos for your best processes and techniques, not try outs or one offs. This can also help you create a name for yourself in the Precious Plastic community. 

👉 community.preciousplastic.com/how-to