### Run your washing system 

 Now fill the bag with pre-washed shreds and put it in the machine. We recommend adding a little bit of laundry detergent, which helps to remove the last bits of oil. The settings of the machine depend on the grade of contamination. Usually, we use the 40-degree quick wash program. Afterward, hang it up so the last bit of moisture can go away.