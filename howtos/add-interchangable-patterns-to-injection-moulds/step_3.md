### Lasercut 

 Depending on the type of cutter used settings will differ. 

Best is to do some test cuts first, changing the speed and intensity of the laser. Acrylic is safe to ut but does smell so dust extraction is advised. 

Make sure the pattern is not cut trough the acrylic plate as it will weaken the texture mould and make it less durable (although its possible for some shapes).

Make sure the edges are cut through to prevent having to drill and cut manually later on in the proces. 

You can use multiple depths in one mould by assigning different settings to the cutting lines. 