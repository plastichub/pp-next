### Have a mould 

 We are adding a part to an existing mould. In this example we use the hexagon tile mould. It's a simple 3 part flat mould held together with boults. Outer size : 15 x 15 cm. The top and bottom plate are 10mm, the center part with hexagon cutout is 4mm.

We will describe how to do lasercutting and CNC milling. For acces to a lasercutter/cnc milling device you can check with your local Fablab/makerspace, sometimes even high schools or universities.

For the lasercut add ons we use 4mm acrylic plates (15 x15cm).
For the CNC we used 3mm aluminium plates (15 x 15cm).
