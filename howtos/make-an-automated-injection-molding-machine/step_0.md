### Download the folder 

 Download and read carefully the Guide.pdf.

Link:

https://www.dropbox.com/sh/bvus8maneewhxhk/AACpXCwufQSTb2FyYe8HFChFa?dl=0

IMPORTANT! The machine is still work in progress and not perfect yet. We added a troubleshooting chapter in the download with details on what has to be improved.

The Guide.pdf contains the most important informations and gives you an idea how all the info is structured.

The package contains:

- CAD
- Bill of material (BOM)
- Blueprints
- Program
- Circuit diagram
- Additional pictures
- ...

We decided to rather put a little bit more information than to few. So dont be scared when we are going a bit into the details :)