### Layout your plastic  


Most of the people don’t even realize how much plastic waste they create. The throw-away culture makes it happen that you leave this waste behind. This makes us, in a lot of moments, just skipping the moment of actually taking a look at what we throw away. 

By laying all the collected plastic waste out and sorting it in categories it will give a clear overview. This can be done at a Collection Point or even after a cleanup. Lay it out and explain/talk/share with the people around you. 
