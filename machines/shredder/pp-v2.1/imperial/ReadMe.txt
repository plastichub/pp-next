3/Feb/2018

With love, from Olivier to Precious Plastic.

¡Pura vida!

Changelog:

4/Feb/2018
	*Fixed minor sizing issues with frame parts
	*Appart from the .iam assembly file, I also included the assembly in .stp and .igs format
	*Added pdf files with hex bar measurements, one for a 1" hex bar with 3/4" bores for 3/4" bearings, and another for a 1" hex bar with 20mm bores (original).
		*While it is true that this design is made for Imperial units, there might be some people who already purchased UCFL204 bearings with 20mm bores, and that's the reason why both hex bar designs were included.
	*Also added 2 additional files for the 1/8" bearing spacers for 3/4" bores in the DXF folder.
		*If you are planning to use 3/4" UCFL204 bearings then you will need to lasercut the file "1-8inch 2x BearingSpacer for 3-4inch Bearing.dxf" and delete the file "1-8inch 2x BearingSpacer for 20mm Bearing.dxf"
		*If you are planning to use 20mm UCFL204 bearings then you will need to lasercut the file "1-8inch 2x BearingSpacer for 20mm Bearing.dxf" and delete the file "1-8inch 2x BearingSpacer for 3-4inch Bearing.dxf"
	*In case anyone is interested, found this deal on UFCL204 3/4" bearings https://goo.gl/7Bg2WP
	
