### Get Started 

 We've been building Precious Plastic machines since V2 of the machines so we're very passionate about the great work that has been done so far and where this community will grow to!

If you're interested in purchasing a machine or interested in inquiring about our services for any research and development purposes do take a look at the products we are selling on the Bazar or message us directly!

https://bazar.preciousplastic.com/darigov-research-limited/