### Find a Community Point near you 

 Community Points are the glue of the Precious Plastic Universe. They know the ins and outs of your local network.

They can help you in multiple ways but they generally have a very in-depth overview of local Precious Plastic spaces, people, useful shops, resources and can help you with planning out your project. You can find them on the map here. 

👉 community.preciousplastic.com/map