#ifndef APP_H
#define APP_H

#include "config.h"
#include <Vector.h>
#include "types.h"
#include "Addon.h"
#include "common/timer.h"

#include "components/StatusLight.h"

class DirectionSwitch;
class Power;
class ExternalServo;
class LinearActuator;

class App : public Addon
{

public:
    App();
    DirectionSwitch *dirSwitch;
    Addon *byId(short id);
    ExternalServo *servo;

    LinearActuator *actuator;
    short onActuatorChange(short state);

    StatusLight statusLightOnline;
    StatusLight statusLightMove;

    short setup();
    short loop();
    short debug();
    short info();
    short ok();

    void onError(int error);

    void loop_service();
    void loop_normal();

    void _loop_manual();
    void loop_addons();

    void setup_addons();
    ushort numByFlag(ushort flag);

    // operation mode specific
    void App::debug_mode_loop();

    Vector<Addon *> addons;

    // bridge
    short setFlag(ushort addonId, ushort flag);

#ifdef HAS_STATES
    short appState(short nop = 0);
    String state();
#endif

    millis_t comTS;
    millis_t loopTS;
    millis_t wait;
    millis_t waitTS;
    millis_t shredStart;
    millis_t bootTime;
    Timer<10, millis> timer; // 10 concurrent tasks, using micros as resolution

    enum APP_STATE
    {
        RESET = 6,
        STANDBY = 2,
        ERROR = 5
    };

    short _state;
    short _error;
    short getLastError(short val = 0)
    {
        return _error;
    }
    short setLastError(short val = 0);
    short setAppState(short newState);
    short getAppState(short val);

    short loopPress();

private:
#ifdef MEARSURE_PERFORMANCE
    millis_t addonLoopTime;
    millis_t bridgeLoopTime;
    millis_t printPerfTS;
#endif

    millis_t debugTS;
};

#endif