### Prepare materials 

 Prepare everything you need to make your plan happen.
Organise a computer (and if available projector) for presentations and videos.

Get paper and pencils (maybe post-its) if you want to brainstorm together and write down ideas and questions, and print if you have questionnaires to fill out or exercises to print out.
And think of any games or activities which could be nice icebreakers to get to know the group. 

If possible, also bring or ask someone to bring a camera to document your meetup and maybe even take notes of what’s being discussed. It’s always nice to capture and share these moments!
