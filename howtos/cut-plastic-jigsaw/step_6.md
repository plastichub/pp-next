### What makes a clean cut 

 Generally, when cutting plastic, the biggest issue comes with overheating the material. That generates undesired melting.
 
With the right blade and cutting settings you can avoid overheating. The explained settings proved useful for us and should be a good starting point, but it might slightly change with different plastic types and tools - so feel free to test it out yourself! :)