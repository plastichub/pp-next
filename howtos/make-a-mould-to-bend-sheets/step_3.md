### Making the drawings: layout 

 Besides that, every sheet should contain some sort of part number or description, not to mix them up later. When moving from one application to another or while printing, pay extra attention to always maintain the same scale!
Use dashed or colored lines for different purposes (e.g. cutting, aligning, centerline …) and hatched areas for offcuts. The lines you print should be as thin as possible and use pure CMYK tones (e.g. 0 0 0 100 for black). This will avoid imprecise lines due a potential registration misalignment in the printer. 