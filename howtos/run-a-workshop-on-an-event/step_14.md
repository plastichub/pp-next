### Post processing 

 The last step is all about documentation...

How many people have you reached and how many products did you produce? Could you make interesting contacts or was the press there and wrote a report about you? 

It’s good to track your impact and learn from your experiences.
And of course collect all your pictures! :)
