### Align legs 

 Next step is to make the stool even.

Use a level to know whether the stool sits straight or not and put wooden shims until it does. Once you are happy with how it feels, use a height caliper to mark all the legs at the same height. 

fter doing that, cut them straight with a blade or sand them till you are happy with the result. 
