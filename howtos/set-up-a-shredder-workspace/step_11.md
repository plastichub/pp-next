### Safety 

 Always stay safe. Of course, don’t put your fingers or hands in the blades.

Find out more tips on how to stay safe in the Academy:
👉 tiny.cc/plastic-safety

It is also good to check out our safety videos on plastic here. 