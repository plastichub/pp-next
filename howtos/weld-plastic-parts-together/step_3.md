### How to make the lamps 

 The recycling guy in this video shows how he combines 12 plates to one hanging lamp ;) 
In addition to HDPE, you can also use this welding technique with PP. LDPE or PS.
After welding, you can also improve the edges. I rub with additional hard plastic over the edges, but you can also polish.

For more information about the lamps visit https://johannplasto.de/.de or 
https://www.instagram.com/johannplasto/

Greets! 🙂
Thomas