### Set up  

 Once you agreed with the business owner to take care of this part of their plastic waste by having a single-use collection on their location, it is time to set up. Make sure it is done nicely and in a way that smoothly fits with their daily workflow (you don't want to disrupt their work).