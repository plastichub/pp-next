### Prepare material + mould 

 I have already tested HDPE, PP or PLA with good results. 
You can play with different colours and bottle cap design. :) 
You need between 320 - 350 g / clipboard. 

Before testing new material type, make a note/ picture how many material did you use. It will help you with future production. 

Clean mould from impurities, dust or remaining plastic particles from previous production. (I use smooth sand paper)
To avoid sticking use mould release or oil. 
Make sure your oil will not get to its smoke point: 
https://en.wikipedia.org/wiki/Template:Smoke_point_of_cooking_oils
I recommend oil with smoke point above 230 deg. Celsius