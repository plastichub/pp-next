### 3D print the mold using hi-temp resin 

 Once you have the 3D model, print it using a resin that’s able to withstand the high heat of the injection molding machine. The most common resin available is FormLabs’ Hi Temp resin which can withstand temperatures up to 269 degrees celsius.