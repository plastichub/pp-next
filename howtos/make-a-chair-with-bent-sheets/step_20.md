### Assemble the frame 

 Start by sticking the frame into the assembly jig, then attach the backrest with the connector plugs onto the frame. Weld the cross-braces (both seat and rear) to the legs and use the jigs to align everything accurately. Now it will pay off if you put effort into precise jigs!

After welding, grind all the beats and make them look beautiful.
Remember: Grinder and paint make you the welder you ain't! :)
