### Is this for you? 

 If you want to start a Mix Workspace the most important aspect is to be super keen on learning this new craft, local plastic recycling.  

A technical background or interest could be a plus but is not mandatory as you could also get the machines built for you. As a plus, some product design knowledge could help you when you start making products. 

Working on a Mix Workspace you could be dealing with people on a daily basis for supplies, workshops, visits and more. It’s good to be a little sociable and enjoy explaining the project to others. 
