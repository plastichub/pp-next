---
image: /_howto/controlbox/dinrail_layout/IMG_1978.JPG
category: "zoe"
title: "Dinrail Layout"
tagline: ""
description: ""
usedin:
  - hal
  - zoe
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_1972.JPG" _target="_blank">
        <img id="IMG_1972.JPG" src="./IMG_1972.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1974.JPG" _target="_blank">
        <img id="IMG_1974.JPG" src="./IMG_1974.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1977.JPG" _target="_blank">
        <img id="IMG_1977.JPG" src="./IMG_1977.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1978.JPG" _target="_blank">
        <img id="IMG_1978.JPG" src="./IMG_1978.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

