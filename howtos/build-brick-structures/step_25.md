### Fixing roofing sheet 

 Next, you will need to use the relevant roofing screws to attach the roof to the timber. The number of screws and locations depend on your roofing material type and it is best to consult with your roofing supplier on these details. 
