---
image: "/howtos/make-an-interlocking-brick/bope.jpg"
title: "Make an interlocking brick"
tagline: ""
description: "Here we&#39;ll show you how to make a brick using the injection machine and the mould we designed in Chiangmai, Thailand. A beautiful, functional model. <br /><br />From: BOPE, Thailand"
keywords: "injection,product,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "product"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  What you need: Injection machine, mould making tools<br />
  This is our workspace in Chiangmai. :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-1.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Idea and first drawing</h4>
<div class="step-text">
  We wanted to develop a product that can have many functions. So we decided to figure out a shape that can be adapted or compliment one another to get a variety of uses. Finally we decided to draw a curved shape. The idea of this shape is to be attached to each other like a Lego. You can use this design as a plant pot or connect it as a partition and build a wall.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-2.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make the 3D drawing</h4>
<div class="step-text">
  First of all, we would like to tell you that our mould design is highly detailed and takes a lot of time to craft. The mould should be made from aluminum with CNC machine. To be easy for you all, please download our mould design above.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/Bope-brick-5.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/Bope-brick-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Machining the mould</h4>
<div class="step-text">
  Using the Sketchup file above to make the mould. We used the CNC Machine from a local manufacturer in Chiangmai.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-6.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Prepare for injecting</h4>
<div class="step-text">
  Tools you need for injecting:<br />
  Wrench<br />
  Bolt x 8 pieces<br />
  Nut x 8 pieces<br />
  Bolt &amp; Nut for locking x 1 set<br />
  Electric drill<br />
  Cutter
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-5.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Prepare your material</h4>
<div class="step-text">
  Prepare your shredded plastic. For this product we use PP. For one brick you will need around 300 gram.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-6-1.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-6-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-6-2.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-6-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Injection time!</h4>
<div class="step-text">
  Turn on the Injection machine (180°C) and wait for it to heat up.<br />
  When the temperature is ready, you can put the shredded PP into the Machine.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-7.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Prepare the mould</h4>
<div class="step-text">
  While waiting for the plastic to melt, you can assemble the mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-8-1.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-8-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-8-2.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-8-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Press</h4>
<div class="step-text">
  When both your machine and the mould are ready, connect the mould to the injection machine. Then press it!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-9.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-9.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Detach the mould</h4>
<div class="step-text">
  Wait for the moudl to cool down. Then detach and carefully open the mould to take out your freshly baked recycled plastic brick!<br />
  Remove the injection channel with a little scissor or knife
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-10-1.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-10-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-10-2.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-10-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Finish the product</h4>
<div class="step-text">
  Last finishes.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-11.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-11.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Explore the possibilities!</h4>
<div class="step-text">
  You can use this for Flower Pots. Or You can make more &amp; more for a partition or the wall.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-12-1.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-12-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-12-2.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-12-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-interlocking-brick/howto-bope brick-12.jpg">
        <img class="step-image" src="/howtos/make-an-interlocking-brick/howto-bope brick-12.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>