### Mask the outlines 

 It’s the moment to mask the tape on the floor. Follow the measurements that you wrote down earlier. Line out the rectangles by using a measuring tape/90-degree corner and paste the masking tape on the lines. Make sure that you press all the tape nicely, so it doesn’t come off too easily. 
