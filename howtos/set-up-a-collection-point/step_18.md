### Receive your plastic 

 Once you’ve learned the basics and got a space you’re ready to open the doors of your Collection Point. Now you have to get people and businesses to give you their plastic. Now is the time for the plastic to come to you (or you're going to pick it up). 

You probably won't be too pleased with the quality of the plastic you collect at first. Don't be disappointed, it takes time! It is important to invest time and energy to explain out to those collecting for you how plastic should be (clean and unlabeled) and why you are asking it like this. By involving them in the process they are willing to make these extra efforts. 