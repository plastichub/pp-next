### Get ready 

 Okay, so before jumping into the building process, get an overview of what you need to follow this How-to.

First, download the files attached at the top of this How-to and check the part list to know exactly which tools and materials are needed.

You can make this stool applying basic woodworking tools and techniques.

It's made entirely out of a plastic sheet, so you’ll need to get one first or make one if you have a sheetpress :)

Related links:
How to make sheets 👉 tiny.cc/run-the-sheetpress
Find sheets on the Bazar👉 bazar.preciousplastic.com