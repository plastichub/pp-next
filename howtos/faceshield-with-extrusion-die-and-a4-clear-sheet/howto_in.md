### Faceshield with extrusion die and A4 clear sheet 


We make a face shield from basic elements and the extrusion machine. Face shields are critically lacking in many countries affected by COVID-19. You need up to 22 per patient. We need a DIY way of making these and making these fast..
Remember face shields are only a complement to googles and masks and are meant to reduce the viral charge.