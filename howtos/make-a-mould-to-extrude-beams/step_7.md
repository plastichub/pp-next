### Explore the possibilities! 

 Now you can extrude! 
There is a lot to explore, you can get some inspiration in other How-to's!

For example:

Make glass like beams
👉 tiny.cc/make-glasslike-beams

Extrude different textures
👉 tiny.cc/extrude-textures

Make T-shaped beams
👉 tiny.cc/make-t-beam