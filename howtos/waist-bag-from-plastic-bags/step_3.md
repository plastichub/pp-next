### Parts Preparation 

 Now that all the plastic material is ready, we can make the parts. For this we use the parts outline PDF file above. The file needs to be printed (or hand drawn if you're into that). All dimensions in millimeters [mm].

Process flow: paper template part outline is cut out, traced onto the plastic. Part is then cut out.

NB! One of the following steps (Step 6) could be skipped if the Top and Back parts are not cut separately, but rather combined into one part with markings.

The photos attached (in order) Front Part, Top Part, Back.