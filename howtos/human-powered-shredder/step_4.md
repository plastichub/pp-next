### I believe I can FLYWHEEL 

 The magic of the flywheels is that it gives the machine inertia: When it's turning, it keeps the machine moving with little input. Remember to put it on the 'fast' side of your mechanism.

The factors that influence the efficiency of the flywheel is its weight, shape and speed. Usually a disc or wheel works best, a distribution of weight away from the axis helps. The faster it moves, the better it is at mantaining rotation with little effort.

Think of the exercise bike in the picture: the flywheel on the front will keep turning even if you're not pedalling.