### Tool list 

 Alongside your Extrusion machine, you will need a number of other tools and machines to help you with the operations of the Extrusion Workspace.

In the Download Kit, you will find a tool list with all the necessary tools to run your workspace.