### Leave to cool and separate 

 After injecting, leave the moulds clamped to cool for 3 minutes before releasing the No Touch Tool from the mould. If you take out the part too early, it may be soft and you risk deforming it. 
Equally, don't leave it in longer than 6 minutes as the shrinkage may cause it to hug the sides which makes it much harder to release! 

Unclamp the mould, peel the part from the mould and leave the moulds to cool (being a plastic mould, it keeps warm for longer which can cause issues over multiple injections). A fan can help this process. 

Using multiple moulds and clamps here enables you to get quick cycle times - you can be injecting the next tools whilst the previous ones cool for example. 