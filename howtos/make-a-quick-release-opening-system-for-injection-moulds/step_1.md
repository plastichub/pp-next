### Prepare your clamps 

 The principle of this system is that you press the mould parts together with self-made clamps.

Let’s start with adapting the quick release wheel bike clamps. 

Measure the length of the threaded part, then put the clamp through your mould and apply the measured length with a marker. This is how much you need to cut off to fit the mould thickness, so you can tighten it up properly. 
