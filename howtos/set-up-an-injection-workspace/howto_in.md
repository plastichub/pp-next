### Set up an Injection Workspace 


In this How-to we’re going to guide you through all the steps to setup an Injection Workspace. Learn about plastic, how to find a space, get the Injection and Shredder machine, find customers and connect to the Precious Plastic Universe. 

Download Files:
👉 https://cutt.ly/starterkit-injection 👈

Step 1-3: Intro
Step 4-9: Learn
Step 10-19: Set up
Step 20-25: Run
Step 26-29: Share