### Organise a Precious Plastic Meetup 


A meetup is a great opportunity to get to know your local recycling community, introduce Precious Plastic to people who don’t know it yet and find ways to help each other and work together to grow and succeed together.
Here we’ll give you some tips to organise and run this meetup.

Meetups are a very useful tool for Community Points - if you haven’t seen it yet, you might want to check out our Community Point Starter Kit! 

Step 1-6: Get ready
Step 8-10: Meet up!
