### Dry materials for higher quality products 


When you want to make products using more advanced Materials (e.g. industrial waste), drying your granulate is a necessary step. 
We will show you how to dry your granulate before extrusion cheaply and effectively, which is especially important if you want to make high quality products like 3d printing filament.

You can check out our How-To video here:
https://youtu.be/dkm_gXxX2pk