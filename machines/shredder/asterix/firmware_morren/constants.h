#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "enums.h"

/////////////////////////////////////////////////////////////////////////
//
//  Addons
//
#define VFD_STR "VDF"
#define DIRECTION_SWITCH_STR "DirectionSwitch"
#define MOTOR_LOAD_STR "MotorLoad"
#define PLUNGER_STR "Plunger"
#define AUTO_REVERSE_STR "AutoReverse"
#define OPERATION_MODE_SWITCH_STR "OperationModeSwitch"
#define APP_STR "App"

#endif