# Obelix Shredder

[Bill of Materials - Markdown](./bom.md) | [Bill of Materials - GoogleSheet](https://docs.google.com/spreadsheets/d/1IUm40Wl332mANwB9yhWo0A7lEvKBukIyNHc7jZba5BU/edit?usp=sharing)


## Todos - (Rev-A)

- [x] Tab and Slot wrong for back panel (34_HOPPER-BACK_x1_2mm_INOX)
- [x] 0.2mm more clearance for stationary knifes (06_SIDEPANEL-TOP-FLANGE_x2_12mm_FERRO)
- [x] non-CE flange for upper hopper (37_S-HOPPER-BASE-FLANGE_x1_2mm_INOX)
- [x] compensate tab offset for bearing housing gauge (03_ENDPANEL-FLANGE_x4_12mm_FERRO)

## Todos (Rev-B)

- [-] Sieve tooo expensive - use stock material
- [x] Sieve structure 0.5mm more clearance
- [ ] auto auto-reverse (adaptive/smart mode)
- [-] 15% less bolts
- [ ] 0.5mm more material for bearing housing
- [x] Rails for bin
- [ ] Suction pipe interface for bin
- [x] 5x LED Amp feedback for CP
- [x] Laser cut table
- [-] Grinder module table adapter fuckery for Idefix-Module-Rev-B
- [-] Adjust/add for nominal sheet errors
- [ ] Stainless/hardox config
- [x] Migrate electronic plunger module from Asterix
- [ ] Migrate oil housing for gears from 4kW

## Variants

- [x] 250x130 - 2kW
- [ ] 350x180 - 4kW ('xl')
- [-] 500x240 - 7kW ('xxl')
- [-] 700 - 11kW ('xxl')

### Variant - 4kW

- [ ] Integrate Motor from cad/4kW/50_Motor/CidepaCKM77.pdf
- [ ] Sealed gear housing for lubrication - on the other side of the chamber
    - this will be from stainless 2mm sheets, using a form to bend it
    - the caps will be TIG weld
    - should have a sight/window nut
    - seal ring fittings will be turned on the lathe
- [ ] Increase chamber with to aprox. 300mm - (adjust to spacers/knifes)


## Todos - XMax Version (2021 - Q3)

- [ ] recalc for 11kW, approx. 16cm rotary diameter

