### Inject (using the extruder) 

 Ok, you’re almost ready to inject. Start heating your plaster mould. While that’s heating, fire up your extruder and prepare your plastic. When your mould is hot, you can start injecting. This could take anywhere from a few minutes to a few hours depending on the size of your product. In this case, the injection process took about 2.5 hours to fill the mould.

When plastic has reached all of your reference points (those little holes you drilled earlier) that means your product is fully injected. At this point, turn off your heat guns and extruder. You also need to plug all of the holes to maintain pressure inside the mould.