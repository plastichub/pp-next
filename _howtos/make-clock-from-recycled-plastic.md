---
image: "/howtos/make-clock-from-recycled-plastic/Edice - Monet (žluté ručičky).jpg"
title: "Make clock from recycled plastic"
tagline: ""
description: "Making clock is not difficult and you are able to recycle about 300g per clock in less than 2 hours. With creative design can be clock nice present for your friends or family. These hand made products can be done with electric oven, mini press and a simple compression mould. Clock diameter is 30 cm with thickness of 5mm. Material cost is about 6 EUR/ clock."
keywords: "compression,product,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "compression"
- "product"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/plastmakers">plastmakers</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prepare your mould</h4>
<div class="step-text">
  You can get laser cutted mould on Precious Plastic bazar: <br />
  <a href="https://bazar.preciousplastic.com/moulds/compression-moulds/clock-compression-mould/<br/>">https://bazar.preciousplastic.com/moulds/compression-moulds/clock-compression-mould/<br /></a>
  <br />
  I understand that transporting mould can be costly for countries, especially outside Europe.<br />
  Therefore, I can provide you with 3D files in STP format, plans as PDF + plans in DXF format.<br />
  If you have access to a laser cutting machine and are only interested in files, send me a message and we can discuss the possibilities<br />
  Why don&#39;t I release all the models for free now? Because I need money for further development :)<br />
  And now let&#39;s start ..
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Prepare material + mould</h4>
<div class="step-text">
  I have already tested HDPE, PP or PLA with good results.<br />
  You can play with different colours and bottle cap design. :)<br />
  You need between 270 - 350 g / clock (depends on material density)<br />
  <br />
  Before testing new material type, make a note/ picture how many material did you use. It will help you with future production. I use technological sheet uploaded as PDF/ Excel.<br />
  <br />
  Clean mould from impurities, dust or remaining plastic particles from previous production. (I use smooth sand paper)<br />
  To avoid sticking use mould release or oil.<br />
  Make sure your oil will not get to its smoke point:<br />
  <a href="https://en.wikipedia.org/wiki/Template:Smoke_point_of_cooking_oils<br/>">https://en.wikipedia.org/wiki/Template:Smoke_point_of_cooking_oils<br /></a>
  I recommend oil with smoke point above 230 deg. Celsius
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-clock-from-recycled-plastic/materiál.jpg">
        <img class="step-image" src="/howtos/make-clock-from-recycled-plastic/materiál.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Baking + pressing</h4>
<div class="step-text">
  I use IKEA Görlig electric oven.<br />
  Set up temperature according to your material type.<br />
  For HDPE and PP I use temperature 225 deg Celsius.<br />
  I put mould in the oven for 50 min.<br />
  Set up timer.<br />
  <br />
  <br />
  Attention - use heat resistent gloves when you operate with mould from oven :)<br />
  <br />
  After baking process, press mould.<br />
  Cooling process take 20 - 30 min.<br />
  If you open mould earlier, there is risk of deformation.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-clock-from-recycled-plastic/flow.jpg">
        <img class="step-image" src="/howtos/make-clock-from-recycled-plastic/flow.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Postprocessing</h4>
<div class="step-text">
  Cut out edges with knife,<br />
  You can sand surface.<br />
  Drill hole in the middle according to size of your clock mechanism.<br />
  List of components and tools for clock assembly that I use - available in supporting material. <br />
  I buy them from Czech company. <br />
  On back side of clock there is small square frame which fits to the clock mechanism I use. <br />
  You can modify mould as you want. I prefer to fix position of clock mechanism to prevent its rotation.<br />
  I do not recommend sanding of surface if you use plate with numbers (area around numbers can be difficult to sand).<br />
  Nice advantage is different colours of clock hands, you can combine different colours. :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-clock-from-recycled-plastic/Minutové a hodinové ručičky.jpg">
        <img class="step-image" src="/howtos/make-clock-from-recycled-plastic/Minutové a hodinové ručičky.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-clock-from-recycled-plastic/clock.jpg">
        <img class="step-image" src="/howtos/make-clock-from-recycled-plastic/clock.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-clock-from-recycled-plastic/Edice - Monet (zadní strana).jpg">
        <img class="step-image" src="/howtos/make-clock-from-recycled-plastic/Edice - Monet (zadní strana).jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Follow Plastmakers</h4>
<div class="step-text">
  To provide more open source content, I will be happy for share, subscribe, like, follow or comment. &lt;3<br />
  <br />
  More information: <a href="https://linktr.ee/plastmakers<br/>">https://linktr.ee/plastmakers<br /></a>
  <br />
  Thank you.<br />
  <br />
  Tom
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-clock-from-recycled-plastic/černé s oranžovými ručičkami.jpg">
        <img class="step-image" src="/howtos/make-clock-from-recycled-plastic/černé s oranžovými ručičkami.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-clock-from-recycled-plastic/modré s bílým víčkem.jpg">
        <img class="step-image" src="/howtos/make-clock-from-recycled-plastic/modré s bílým víčkem.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-clock-from-recycled-plastic/víčkový mix.jpg">
        <img class="step-image" src="/howtos/make-clock-from-recycled-plastic/víčkový mix.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>