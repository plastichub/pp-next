### Prepare your team 

 Don’t be alone, it’s much more fun and relaxed to work in a team!+

Here at Kunststoffschmiede we recommend 3-4 supervisors when you’re working with two machines. There are always people who have a lot of questions.

What does your team need:
- perfect operation of the machines and moulds
- communicative
- Your mindset is on one level. So your workshops have a consistent quality.
- They know the answers to the most asked questions. 👉 Next step!
