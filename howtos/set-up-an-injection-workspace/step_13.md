### Find a space 

 To help you find the perfect place for your workspace in the Download Kit you will find a floor plan with all the minimum requirements and a little cardboard tool to place your machines and tools in the workspace. 