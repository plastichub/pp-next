### Get ready 

 Before you start, gather the materials and tools you need to make the quick release system:

Materials: 4 bike skewers (standard thread thickness: 5mm).
You can use more depending on the type of mould you have, i would recommend you to use at least 4. 

Tools: ruler, M5 die, safety glasses, marker.
And for cutting and sanding, you can either use. a hand saw, file adna sanding paper or a grinder with cutting and sanding disc
