### Place in the Sheetpress 

 We placed the plastic on top of a 3mm sheet of mdf to make sure it doesn’t stick to the press. Once the sheet is in place move the bottom bed to the top so it’s almost closed (see picture). A distance of 1 cm should be ok.

It is also possible to bend sheets larger than the sheetpress, as long as the bended part fits the press (second image is a 2x1 sheet).