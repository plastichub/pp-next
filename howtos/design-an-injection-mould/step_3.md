### Define the product shape 

 This is thinking on what shapes are going to create the cavity inside the mold. For this product, there are basically three pieces:
A cylinder (part 1) for the space of the broomstick,
concentric with a circular tube (part 2) which contains the outside of the hanger,
and a block (part 3) to cut the inner space and create the opening. 

With this, I’m starting to think in the standard bars and/or tubes I will use.