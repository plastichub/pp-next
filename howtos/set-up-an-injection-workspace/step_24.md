### Run workshops 

 A great way to educate (and earn some money) is to make workshops and demonstrations for people, organizations and schools. You can run workshops at events, conferences and conventions showing how Precious Plastic recycling works.  