### Add Diameter Dimensions 

 -Select the circle you wish to add a diameter to
-Click on the Diameter dimension button
-Adjust the location so the dimension is legible
-Repeat for all circles in your drawing