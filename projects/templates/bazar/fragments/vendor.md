### Plastic-Hub is the biggest contributor of the Precious-Plastic open source movement 


We're 100% open source and we mainly produce for communities, schools, universities, small enterprises and startups and provide after sales support.

### Our activities

- donations to various talents and teams in the community
- develop new machines, updates and fixes of the machines
- develop various open source software tools: Precious-Plastic Wiki Language, and machine firmware
- extensive community suppport in the forum and Discord since 2 years
- maintainence and support of community projects and activities

</hr>

### The team

- **Guenter** - Senior software and hardware engineer from Germany
- **Anne** - Communication and sales officer from France
- **Daniel** - Lead engineer from the UK
- **David - 3D Seed** : Product Engineer and Design from Australia
- **David** :  Production, from Catalonia
- **Kampru** : Electronics Engineer, from Catalonia
- **Nials** : Product Designer, from the UK

### Projects

- **[Asterix](https://gitlab.com/plastichub/products/tree/master/products/asterix)** : Full automatic bottle vending machiene for schools and bars.
- **[Precious Plastic](https://gitlab.com/plastichub/products)** : maintainence and updates for Precious Plastic machinery
- **[Precious Plastic Language](https://gitlab.com/plastichub/lang)** : development of a new Wiki language
- **[Plastic - Hub](https://gitlab.com/plastichub/products/tree/master/products/)** : development of new and open source plastic recycling machines, based on Precious Plastic and beyond
- **[Firmware](https://gitlab.com/plastichub/firmware)** : development of firmware for plastic recycling machines
- **[Forum](https://forum.precious-plastic.org/)** : development of a new forum for the Precious Plastic community
- **[Community Projects](https://gitlab.com/plastichub/products/tree/master/projects)** : maintainence and development of community projects





<p style="color:#f58d8e;">
    <a href="https://www.instagram.com/plastichubcat/"><span style="color:#f58d8e;">Instagram</span></a> | <a
        href="https://gitlab.com/plastichub/"><span style="color:#f58d8e;">Github (open source
            files, machines)</span></a> | <a href="mailto:mailto://cgoflyn@gmail.com"><span
            style="color:#f58d8e;">EMail</span></a> | <a href="https://www.facebook.com/plastichubcat"><span
            style="color:#f58d8e;">Facebook</span></a> | <a href="https://precious-plastic.org/library/machines/"><span
            style="color:#f58d8e;">Wiki</span></a> | <a href="https://community.preciousplastic.com/u/plastichub"><span
            style="color:#f58d8e;">Community map</span></a>
</p>
<p><a href="https://discord.gg/nVaNCs3"><span style="color:#f58d8e;">Discord - Support Chat (pre-sales,
            community)</span></a> | <a href="tel://0034666894789"><span style="color:#f58d8e;">Telefon (Whatsapp,
            English, French, German, Spanish, Catalan)</span></a>
</p>
<br>

<div style="padding:16px;text-align: center;">
    <h3> See our other products </h3>
    <div>
    <div class="ty-vendor-plans">
        <p style="flex-grow:1;position:relative;overflow:hidden;">
            ${elena}
        </p>
        <p style="flex-grow:1;position:relative;overflow:hidden;">
            ${zoe}
        </p>
        <p style="flex-grow:1;position:relative;overflow:hidden;">
            ${zoex}
        </p>
        <p style="flex-grow:1;position:relative;overflow:hidden;">
            ${lydia}
        </p>
        <p style="flex-grow:1;position:relative;overflow:hidden;">
            ${lydia-v4}
        </p>
        <p style="flex-grow:1;position:relative;overflow:hidden;">
            ${sheetpress}
        </p>
    </div>
    </div>
</div>
