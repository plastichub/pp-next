### Make the shelves 

 Let’s continue with the sheets.

Print the shape of the shelves which are provided in the download files. Create a template on a MDF sheet and use it to copy this shape five times on your plastic sheets.

Cut it roughly with the jigsaw and route the edges with the help of the template. Drill and countersink the holes. 

Remember to wear a mask and glasses here!