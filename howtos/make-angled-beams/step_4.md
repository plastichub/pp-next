### Weld mounting brackets 

 With your parts cut and ready, it’s time to weld.
As described in Step 5 of Make a mould for extruding beams, weld together the brackets to the beam mould and the nozzle to the mounting plate.

To the attach the brackets that will hold together the two parts of the angled beam, lay both parts on a flat surface and plan where you want the brackets to sit. It can be helpful to start with the brackets bolted together and overlay one side (as shown on the image), so that they can function as an alignment.
Once everything sits in the right position, tack the brackets to the beam and repeat the process on the other side. Once all your parts are tacked into position, weld it all up!
Tip: By aligning the brackets with the angle of your cut, and adding a bit of space between the brackets you can achieve a well sealed joint.
