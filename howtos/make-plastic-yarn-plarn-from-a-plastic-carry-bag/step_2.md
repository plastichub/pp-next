### Folding the Bag 

 Image 3:

You will notice that your bag has 2 flaps or folds. 
(Most plastic bags have it, but if yours doesn't, then Image 3 and Image 4 do not apply.)


Image 4:

Take the flap that is away from you and fold it in, towards you.


Image 5:

Now, fold the bag in half, not all the way to the end but leaving a margin of about 3 to 5 cm.
Fold it a couple of times more.
If your plastic bag is thick, you can fold it a fewer times so that it is easier to cut.