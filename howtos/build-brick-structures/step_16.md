### Clamping the bricks together 

 Next step is to clamp the bricks together ensuring they cannot move. To do this you will need to add an additional washer and nut to the threaded bar, tightening the wood down until either the washer sinks into the wood or a good level of resistance is felt to the nut. This step should also increase the strength of the wall and its stability. 