---
image: "/howtos/make-a-wall-peg/IMG_1846.JPG"
title: "Make a Wall Peg"
tagline: ""
description: "Here you will find the 3D model and blueprints to create the wall peg mold!"
keywords: "injection,product,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "product"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/el-tornillo-taller">el-tornillo-taller</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get your materials and prepare the work:</h4>
<div class="step-text">
  Make sure you have all your materials ready and go through the drawings and steps to understand the full picture of the process. This will help you to work more efficiently and accurate.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/1.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cut the nozzle nipple:</h4>
<div class="step-text">
  With all the parts in the bag, let’s start cutting the steel pipe nipple (no. 7) in half to make the mold nozzle. (Drawings page 3).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image005.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image005.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make the nozzle flange:</h4>
<div class="step-text">
  Get the steel disc (no. 3) and turn a hole in the center with diameter to fit in tightly one half of the steel pipe nipple (part no. 7). (See drawings page 4)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image007.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image007.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image009.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image009.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Turn the nozzle guide </h4>
<div class="step-text">
  Turn one face of the flange to create a 3” diameter guide to fit the mold body no. 1: (See drawings page 4)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image011.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image011.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image013.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image013.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Weld the nozzle</h4>
<div class="step-text">
  Weld no. 3 and no. 7 together. Then chamfer the welded edge on the lathe: (See drawings page 5)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image015.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image015.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Drill the base center hole</h4>
<div class="step-text">
  Get disc no. 4 and drill a 9/32” hole in the center. (See drawings page 6)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image017.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image017.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Drill and cut the screw holes</h4>
<div class="step-text">
  Drill four holes in the border of discs no. 3-4 and cut its sides. (See drawings pages 4-6)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image019.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image019.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image021.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image021.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image023.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image023.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Drill the base fixing holes</h4>
<div class="step-text">
  Drill four more 3/16” holes in disc no. 4. (See drawings page 6)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image025.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image025.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Get your CNC turned parts</h4>
<div class="step-text">
  To get that curved, smooth and shiny surface for the cavity of the mold, get the parts no. 1-2 and the 3D files, and take them to the best CNC lathe workshop in town. They will handle the different file extensions, but for any doubts, the drawings will make everything clear. (See drawings pages 7-8-9)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image027.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image027.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image029.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image029.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image031.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image031.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Make the mold base</h4>
<div class="step-text">
  Get part no. 5 and cut its corners tangent to a 3” circle (diameter of part no. 2). (See drawings page 10)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image033.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image033.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image035.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image035.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Cut the metal sheet</h4>
<div class="step-text">
  From a thin metal sheet, cut part no. 6 and cut its corners to prevent injuries. With four nails, hammer it in the center of no. 5. (See drawings pages 11-12)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image037.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image037.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image039.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image039.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image041.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image041.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Drill the screw holder hole</h4>
<div class="step-text">
  Drill a ⅛” hole in the center of parts no. 5 and 6. Insert a screw to create the thread in the wood. (See drawings pages 10-11-12)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image043.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image043.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image045.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image045.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Done!</h4>
<div class="step-text">
  And you’re done! Here is your Wall Peg mold.<br />
  Remember to put a new screw in the wooden mold base every time you are going to inject. If you forget, the hole will be filled with plastic and won’t work. But don’t worry! Drill it again and you are done.<br />
  To open the mold, take off the bolts sideways, then cut the plastic at the entrance and pull apart the mold parts. Then, unscrew the peg off the wooden part and you have your peg ready.. <br />
  Since the plug has some volume, it will take time to cool down and the outgoing screw will be soft. Avoid tilting it and make sure it is in the right position.<br />
  It will work with all the plastics and it is very easy and smooth to inject. Just explore and find your favorite plastics and mixtures.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image047.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image047.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Happy hanging :)</h4>
<div class="step-text">
  To install the peg on the wall, drill a hole and fix it by hand with a wall plug.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image049.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image049.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-wall-peg/image051.jpg">
        <img class="step-image" src="/howtos/make-a-wall-peg/image051.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>