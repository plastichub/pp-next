### Prototyping 

 It’s kind of obvious, but first things first: before making a proper mould, you should figure out what exactly you want to achieve. It’s easy to build a rough prototype from plywood or cardboard to test proportions and ergonomics. So right after sketching out your design, go and test it. 