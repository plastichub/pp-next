### Material Preparation 

 In order to get material ready, we need to fuse layers of plastic bags together using a home iron. This could easily be done at home with no risk of messing up the iron or anything around.

The setup requires baking paper to be placed on an ironing board, two layers of bags placed over it and another layer of baking paper to be placed on top. Plastic bags should not stick out, otherwise there is a risk of melting them onto the board or the iron.

The layers of bags are then ironed at the temperature somewhere between COTTON and LINEN settings. The iron has to be always moving and not stopping on any spot for too long as it might overheat the plastic. Plastic is to be ironed until it is completely fused. The more pressure you put into ironing, the more flat will the surface of the material be. This process to be repeated until you have 3-4 layers of plastic bags to allow for material durability.

Given that the instructions are followed properly, no smell or fumes will be released, but for safety measures it is advised to work in a well ventilated room.