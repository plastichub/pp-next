### Weld the mouth 

 This is the last step in the process and is crucial to do your best. You should choose how wide you want the nozzle to be and find an object to maintain that distance. In this example an old angle grinder disc has been used as a spacer. The two  pieces have been held together with a clamp and tacked to the pipe on the short edges. Once you have those in place, you can then seal up the long edges and finish the nozzle. 
