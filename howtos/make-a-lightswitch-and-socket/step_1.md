### Make the moulds: Sliders 

 The lightswitch cover needs holes for sliders to achieve a snap connector feature which shapes an undercut.

Drill the corresponding holes perpendicular to the unmoulding direction and use a tightly fitting pin (e.g. the back of the drill or a parallel pin) as slider.