---
image: "/howtos/-insulate-the-injectionextrusion-barrel/Insulation-8.jpg"
title: "Insulate the Injection/Extrusion barrel"
tagline: ""
description: "Insulation makes the machines more efficient and safe to use.<br />Easy peasy :)"
keywords: "extrusion,injection,hack"
tags:
- "extrusion"
- "injection"
- "hack"
---
<h4 id="summary">{{page.title}}</h4>
{{ page.description }}
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  This is a pretty simple upgrade. There are many ways to insulate barrels, but in this example we have gone with using some steel mesh and hand-bent brackets.<br />
  <br />
  Before you begin, you should have: <br />
  - Hammer<br />
  - Vice<br />
  - Drill<br />
  - Steel mesh<br />
  - Insulation wool<br />
  - Scrap metal (flat bar)<br />
  - Nuts &amp; Bolts
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-2.jpg">
        <img class="step-image" src="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cut the mesh</h4>
<div class="step-text">
  To begin, calculate the circumference you want your insulation cover to wrap around.<br />
  <br />
  Remember to add a bit extra for the tabs and then cut your mesh with a grinder or tin snips.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-3.jpg">
        <img class="step-image" src="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Form the mesh</h4>
<div class="step-text">
  Now find something cylindrical to use in order to shape your mesh into the desired shape. We have used a gas tank, but you could use another piece of steel. Get creative!<br />
  <br />
  Tip: You might need to hammer it a bit smaller after your first shaping because metal often has a ‘springback’ and may be too big.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-4.jpg">
        <img class="step-image" src="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-5.jpg">
        <img class="step-image" src="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Centering brackets</h4>
<div class="step-text">
  These brackets will connect and hold the cover around the barrel. Find a small pipe, around the same size as your barrel, and shape the strips around it. Make sure one bracket has longer tabs to be used for centering. <br />
  <br />
  Use your hammer to shape the tabs to the shape you want and drill some holes for bolting. You can finally weld or bolt the bigger bracket to the mesh.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-6.jpg">
        <img class="step-image" src="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-7.jpg">
        <img class="step-image" src="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Fill with fluff</h4>
<div class="step-text">
  Now with your cover finished, all you need to do is stuff the cover full of your insulation wool. <br />
  Done!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-1.jpg">
        <img class="step-image" src="/howtos/-insulate-the-injectionextrusion-barrel/Insulation-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
{% if page.related %}
<h3 id="related"> Related </h3>
{% endif %}
---
{% if page.tags %}
{% endif %}
{% if page.resources %}
{% endif %}