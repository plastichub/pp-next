### Centering brackets 

 These brackets will connect and hold the cover around the barrel. Find a small pipe, around the same size as your barrel, and shape the strips around it. Make sure one bracket has longer tabs to be used for centering. 

Use your hammer to shape the tabs to the shape you want and drill some holes for bolting. You can finally weld or bolt the bigger bracket to the mesh.
