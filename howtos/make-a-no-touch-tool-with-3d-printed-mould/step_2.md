### Inject into the mould 

 We use LDPE sourced from wheel nut indicators (Checkpoints) which is vibrantly coloured and very nice quality to work with. It injects nicely between 160-170*C. 
For each tool we put 40g of plastic into the injection moulder - accounting for some leakage at the beginning to ensure the plastic is flowing nicely and over-spill at the end to ensure the mould is 100% filled. 
We inject the plastic relatively slowly by hand and hold the pressure once the mould is filled (indicated by when it overflows at the top). Using nylon moulds means that the injected plastic is insulated so it doesn't cool quickly so wants to escape if you don't hold the pressure after injecting. 