### Set up a Sheetpress Workspace 


In this How-to we’re going to guide you through all the steps to set up a Sheetpress Workspace. Learn about plastic, how to find a space, get the Sheetpress, find customers and connect to the Precious Plastic Universe. 

Download files:
👉 https://cutt.ly/starterkit-sheetpress 👈

Step 1-3: Intro
Step 4-9: Learn
Step 10-18: Set up
Step 19-23: Run
Step 24-25: Share