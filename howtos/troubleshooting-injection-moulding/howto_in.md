### Troubleshooting injection moulding 


This is not an exact step by step tutorial. Each step describes a problem you might face during the process of injection moulding. 

The solutions are ordered in the recommended order of optimization top -> bottom.

If you have a problem which is not listed here, feel free to PM me ;)