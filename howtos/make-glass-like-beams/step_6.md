### Extrude (1) - Concentrated colours 

 Time to melt!
Mixture: 95% clear PS, 5% blue cut-up pigment

Put your clean PS in your extruder hopper and sprinkle the smashed pigment in evenly. Do not add all of your mix in at once, gradually sprinkle making sure there’s enough mixture in the hopper. Keep an eye on the other end of the mould and stop the extruder as soon as you see material approach the end.

It will come out as a strong concentrated colour. To create a cloudy pastel colour, we will mix this material again in a further process (Step 8).

Note: Collect your excess material and squish it flat before it hardens, so it will be easier to shred it again for further processes. 