---
image: "${image}"
title: "${title}"
tagline: "${tagline}"
description: "${description}"
keywords: "${keywords}"
enabled: "${enabled}"
sidebar: 
   nav: "howtos"
${config}
---

${header_global}

<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/${user}">${user}</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)

<br/>
<br/>

{{ page.description }}

${header}

---

${used_machines}

---

${steps}

---

${footer}

---
${footer_global}

---

<div class="resources">

   {% if page.related %}

      <h3 id="related"> Related </h3>

      ${page.related}

   {% endif %}
   ---

   {% if page.similar %}

      <h3 id="related"> Similar </h3>

      ${page.similar}

   {% endif %}
   ---

   {% if page.howto_tags %}

      <h3 id="related"> Tags </h3>
      
      ${page.howto_tags}

   {% endif %}

   {% if page.resources %}

      
      <h3 id="related"> Resources </h3>
      
      ${page.resources}
      

   {% endif %}

<div>
