# Test Data

## PET Flakes

### Test - 21.01.2021 - RevC ([GIT](https://gitlab.com/plastichub/pp-next/commit/50d86488758ad397688fce3b1358fb3c871c05e6))

---

1st Pass

**Input**: 3-5cm flakes from 40 1.5l bottles, at 30g per bottle = 1.2Kg which was one full hopper load

**Duration** : 6m:24s

**Sieve** : 3mm

**Noise** : 90db

**Motor Temperature - 2kW** : unchanged

**Knife temperature** : warm

**Average output flake size** (median) : 2.5mm

**Remarks**

- there was noticeable warm air with plastic smell in the hopper
- hopper needs redesign for continues feeding

---

2nd Pass

**Duration** : 4 minutes

**Average output flake size** (median) : 2.2mm
