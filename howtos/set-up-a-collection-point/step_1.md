### Outcomes 

 The outcome of a Collection Point is collected plastic, cleaned and sorted by type (and colour), ready to be shred and recycled. 

It can be collected from consumers or businesses. It's crucial that the plastic is label-free and clean for further processes, to keep a good quality of the recycled material. The cleaner and purer the plastic the higher its value.