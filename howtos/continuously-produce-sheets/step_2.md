### Turn on your system 

 One way to make sure you don't forget ventilation is to attach it to the same switch as the outlet for the sheetpress so it's impossible to turn on the sheetpress without the ventilation being on. 

Turn on the sheet press 45mins before your first sheet is due to go in, with the pressing plates closed, to allow it to heat up. Set the PID controller to match the type of plastic you are melting, if you are not sure you can check our “Melting temperatures” poster in the download kit or on the academy. 

Now that the press is on, you should wear a mask whilst in the space.