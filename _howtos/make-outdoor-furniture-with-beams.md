---
image: "/howtos/make-outdoor-furniture-with-beams/Extruded furniture-title.jpg"
title: "Make outdoor furniture with beams"
tagline: ""
description: "We extrude the mix of all the different kinds of plastic to sticks and screw them together to furniture. For our chairs you need stickd with hard parts for the legs of the chair and a soft part in the middle.<br />HARD ..mix LDPE HDOE PP PS<br />SOFT .mix LDPE HDPE EVA flip flops"
keywords: "PP,LDPE,HDPE,extrusion,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PP"
- "LDPE"
- "HDPE"
- "extrusion"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/pacific-beauty">pacific-beauty</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Extruding sticks</h4>
<div class="step-text">
  We use the mix of plastic found on the beach or in the environment, about 40 % LDPE, 20% HDPE, 20 % PP, 10 % shrunk EPS and a small amount of shredded PET, aluminium coa. ted chips bags, EVA , but no PVC .... we accept some downgrading which is possible for a bit lower quality in this way.<br />
  My friend Toan Nguyen has developed the first extruders built in Vietnam.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-5.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-3.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/20200527_100754.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/20200527_100754.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Prepare your pieces</h4>
<div class="step-text">
  Choose a furniture design (you can just copy an existing piece of furniture which is made out of wood) and prepare the pieces you need to build it.<br />
  <br />
  In most furniture it&#39;s enough to use straight beams, but you can also bend the sticks so you have round elements.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-10.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-10.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-2.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Screw the parts together</h4>
<div class="step-text">
  If you have all your parts ready you just need to screw them together like you would do with wood.<br />
  Then your furniture is done!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-4.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-7.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-7.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-8.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-8.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Make more!</h4>
<div class="step-text">
  In this way we can process lots of plastic and all kinds. <br />
  We build outdoor shelves, tables ...<br />
  and publish on our Youtube channel PACIFIC BEAUTY plastic. <br />
  <br />
  You can see more about this process in this video:<br />
  <a href="https://www.youtube.com/watch?v=_0Kbeaz63OY<br/>">https://www.youtube.com/watch?v=_0Kbeaz63OY<br /></a>
  <br />
  Have fun! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-9.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/Extruded furniture-9.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/20200214_163555.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/20200214_163555.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-outdoor-furniture-with-beams/2df6dc615cfa5c0d898b44dce125e440.0.jpg">
        <img class="step-image" src="/howtos/make-outdoor-furniture-with-beams/2df6dc615cfa5c0d898b44dce125e440.0.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>