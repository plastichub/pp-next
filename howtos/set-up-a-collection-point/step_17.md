### Put your Pin on the Map 

 If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform and put your pin on the map.

This makes it easier for workspaces like the shredder guy to contact you to get plastic or even for a community builder to connect you to the local recycling community. Your Collection Point can also be an inspiration for others in your area to join the Precious Plastic community.

👉 community.preciousplastic.com/sign-up
