<?php
require __DIR__ . './../vendor/autoload.php';
require __DIR__ . './../debug.php';
require __DIR__ . './../utils.php';

use GuzzleHttp\Client;
use Slim\Http\Request;
use Slim\Http\Response;
$app = new \Slim\App;
use SR\ArrayUtils;

$app->get('/', function (Request $request, Response $response, array $args) {
    $action = ArrayUtils::get($request->getQueryParams(), 'action', '');
    if ($action === 'resource-rpc') {
        $resource = urldecode(ArrayUtils::get($request->getQueryParams(), 'url', ''));
        if ($resource) {
            $client = new Client([
                'timeout' => 4.0,
                'verify' => false,
            ]);
            $fileName = basename($resource);
            $response_in = $client->request('GET', $resource);
            $headers = (array) $response_in->getHeaders();
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            foreach ($headers as $key => $value) {
                $key = strtolower($key);
                if ($key == 'content-type' || $key == 'content-length') {
                    header($key . ': ' . $value[0]);
                }
            }
            $body = $response_in->getBody();
            $ret = $body->getContents();
            $response->getBody()->write($ret);
        }
    }
    return $response;
});
$app->run();
