### Shred plastic 

 While the mold is printing, you can prepare your plastic by cleaning and shredding it. If you’re making Daily Sporks, be sure to use food-grade plastic which is marked with the food-grade symbol.