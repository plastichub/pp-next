### Bend the metal legs 

 For bending the legs (parts C+D), cut them to size and put them in the bending jig to mark the position of the first radius. Then bend the tube to the corresponding angle and repeat that step for every corner. The leg angle jig helps you to obtain more precision.
Once you are done make sure the leg is still planar and correct any deformation by clamping and bending the piece straight.
