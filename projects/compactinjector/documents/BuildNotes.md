Frame to be welded throughout; legs are friction fit to base frame

Base faceplates to be tack welded to base frame

MDF base plate to be loose (no fixings) for removal to access electrics

Cables to run inside legs, terminating in XT90 connectors so that the base can be separated for shipping

**Controls**:
- Left rocker switch: Power on/off
- Right rocker switch: Actuators up/down
