---
image: "/howtos/stamp-products-fast-without-heating/8.jpg"
title: "Stamp products fast. Without heating"
tagline: ""
description: "We will show you how you can stamp the products you made in a fast efficient way. The main benefit is that it doesn’t require heating so you have no heatup/cooldown times. This is good for bigger productions and just saves energy :)"
keywords: "HDPE,product,PP"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "HDPE"
- "product"
- "PP"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/samsara_trc">samsara_trc</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get a product</h4>
<div class="step-text">
  First you need to have a product. We work with sheets and cut them with CNC to make products. We only tried this technique with PP and HDPE. So let’s met show you how we stamp them.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/10.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/10.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Make your stamp</h4>
<div class="step-text">
  Make a design for your stamp and mill it in metal. We made ours from brass, but I guess other metals would work as well. Tip: make sure to mirror your design
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/5.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/3.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/4.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Press</h4>
<div class="step-text">
  For pressing we like to use arbor press. It works fast and are easy to use/find. Put the product underneath apply some fore and take it out. You can get really fast here
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/1.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Use mould for alignment</h4>
<div class="step-text">
  We like it when things are consistent. So we use a little mould to put in our product to make sure we always stamp in the same place. This actually also makes sure the product doesn’t move when applying force. Not a must, but would highly recommend
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/2.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Ready!</h4>
<div class="step-text">
  Here you can see a few products we stamped. We made one stamp for our logo and one for the type of plastic.<br />
  Have fun making yours! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/6.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/9.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/9.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/stamp-products-fast-without-heating/7.jpg">
        <img class="step-image" src="/howtos/stamp-products-fast-without-heating/7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>