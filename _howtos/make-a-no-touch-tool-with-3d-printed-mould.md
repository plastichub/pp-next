---
image: "/howtos/make-a-no-touch-tool-with-3d-printed-mould/no-touch-tool.jpg"
title: "Make a No Touch Tool with 3D printed mould"
tagline: ""
description: "Create a tool for interacting with high contact areas such as door handles, pin pads, and light switches so your hands don&#39;t have to... helping prevent Covid-19 contamination spread!<br />Made from 3D printed nylon for a quick turnaround of moulds to address the problem as soon as possible."
keywords: "product,mould,LDPE,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "mould"
- "LDPE"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/martin-ppl-uk">martin-ppl-uk</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Make or buy the mould</h4>
<div class="step-text">
  Nylon was used for the mould as it&#39;s melting temperature is much higher than that of LDPE which we use to make the No Touch Tools. We are conscious of consuming virgin plastic but using 3D printed moulds means we&#39;re only using the exact amount of plastic required (opposed to CNC Machining sheets of polycarbonate for example). Although metal moulds are ideal for longevity and quality, they do come with a higher carbon footprint and cost. The nylon moulds have so far had over 100 injections with little wear and are holding up very well for a fraction of the cost of a metal mould. <br />
  <br />
  We printed the moulds using an Ultimaker 3 3D printer.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/3D printing.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/3D printing.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/383bf69d-ce83-4839-b0a2-d3e06588a402.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/383bf69d-ce83-4839-b0a2-d3e06588a402.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Make a clamp for the mould </h4>
<div class="step-text">
  We used plates of metal either side of the mould to apply an even pressure but also wick away some of the heat. You could design bolt slots in the mould halves to clamp them directly. <br />
  Initially our moulds had through-bolts which was of course silly and we quickly moved to a significant 10mm steel plate either side of the mould clamped together with M10 bolts. This setup becomes really slick as you can slide the moulds out of the clamp easily, separate the part and slot it back together when complete.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/IMG_5240.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/IMG_5240.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Inject into the mould</h4>
<div class="step-text">
  We use LDPE sourced from wheel nut indicators (Checkpoints) which is vibrantly coloured and very nice quality to work with. It injects nicely between 160-170*C. <br />
  For each tool we put 40g of plastic into the injection moulder - accounting for some leakage at the beginning to ensure the plastic is flowing nicely and over-spill at the end to ensure the mould is 100% filled. <br />
  We inject the plastic relatively slowly by hand and hold the pressure once the mould is filled (indicated by when it overflows at the top). Using nylon moulds means that the injected plastic is insulated so it doesn&#39;t cool quickly so wants to escape if you don&#39;t hold the pressure after injecting.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/Filling injector.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/Filling injector.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200723_142104.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200723_142104.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Leave to cool and separate</h4>
<div class="step-text">
  After injecting, leave the moulds clamped to cool for 3 minutes before releasing the No Touch Tool from the mould. If you take out the part too early, it may be soft and you risk deforming it. <br />
  Equally, don&#39;t leave it in longer than 6 minutes as the shrinkage may cause it to hug the sides which makes it much harder to release! <br />
  <br />
  Unclamp the mould, peel the part from the mould and leave the moulds to cool (being a plastic mould, it keeps warm for longer which can cause issues over multiple injections). A fan can help this process. <br />
  <br />
  Using multiple moulds and clamps here enables you to get quick cycle times - you can be injecting the next tools whilst the previous ones cool for example.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200610_130653.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200610_130653.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/WhatsApp Image 2020-07-21 at 18.20.46 (2).jpeg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/WhatsApp Image 2020-07-21 at 18.20.46 (2).jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Finishing</h4>
<div class="step-text">
  Cut away the sprue using a sharp knife.<br />
  Cut away any flashing that may have occurred.<br />
  Drill a hole for a keyring in the bottom. <br />
  Attach a retractable lanyard which helps keep the tool keep out of pockets and reduce contamination risk but remains close to hand.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200723_141726.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200723_141726.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200723_141750.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200723_141750.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200723_141918.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/20200723_141918.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Finished! </h4>
<div class="step-text">
  Keep your hands away from high-contact surfaces and use the tool instead!<br />
  <br />
  Stay safe out there.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/ntt-white-bkgd.png">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/ntt-white-bkgd.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/No Touch Tool Door.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/No Touch Tool Door.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/No Touch Tool Pinpad.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/No Touch Tool Pinpad.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Bonus: Speed up the process</h4>
<div class="step-text">
  To meet demand or lower the price point of the tools, there are a number of ways you can speed up the process:<br />
  <br />
  1. Heat the plastic before putting into the injector like Qi-Tech <br />
  <a href="https://community.preciousplastic.com/how-to/rapid-fire-method-for-injection--300-face-shields-a-day<br/>">https://community.preciousplastic.com/how-to/rapid-fire-method-for-injection--300-face-shields-a-day<br /></a>
  You can also use an oven to pre-heat the shreds rather than an extruder. <br />
  2. Use multiple moulds.<br />
  This enables you to have some products cooling whilst you inject others. <br />
  3. Fill the injection tube for multiple shots. <br />
  If you can load your injector with multiple 40g shots of plastic, this will enable you to inject multiple shots at a time.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-no-touch-tool-with-3d-printed-mould/background.jpg">
        <img class="step-image" src="/howtos/make-a-no-touch-tool-with-3d-printed-mould/background.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>