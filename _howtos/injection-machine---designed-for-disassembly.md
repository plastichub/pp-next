---
image: "/howtos/injection-machine---designed-for-disassembly/cover pic INJMREAL.png"
title: "Injection Machine - Designed for disassembly"
tagline: ""
description: "Injection machine designed to be disassembled, easy to transport and ship. It now fits in a 1000x200x200 mm box when disassembled and weighs less than 30 kg."
keywords: "injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Download</h4>
<div class="step-text">
  Click here 👉 <a href="http://tiny.cc/injectionmachine">http://tiny.cc/injectionmachine</a> to download the following files:<br />
  - 3D .step files<br />
  - Laser cut files .dwg<br />
  - Bill of materials.<br />
  - Aseembly guide.<br />
  <br />
  To build this machine you&#39;ll need to have access to:<br />
  - Metal laser cut<br />
  - Grinder<br />
  - Welding machine<br />
  - Drill press<br />
  - Basic tools: spanners, allen keys, screwdivers, etc.<br />
  <br />
  If you&#39;re setting up a Injection workspace, you should still download our Injection Starterkit from our Academy. <br />
  Also, here you&#39;ll find some links to the videos and how tos for running an Injection Workspace:<br />
  <br />
  👉 <a href="https://www.youtube.com/watch?v=Iu6vh75Th2M<br/>">https://www.youtube.com/watch?v=Iu6vh75Th2M<br /></a>
  👉 <a href="https://community.preciousplastic.com/how-to/work-with-the-injection-machine">https://community.preciousplastic.com/how-to/work-with-the-injection-machine</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-machine---designed-for-disassembly/step1.png">
        <img class="step-image" src="/howtos/injection-machine---designed-for-disassembly/step1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Designed for disassembly </h4>
<div class="step-text">
  The machine has been designed to be disassembled for easy packing and transportation. It now fits in a 1000x200x200 mm box when disassembled.<br />
  <br />
  The electronics box it&#39;s the same, but we added some powercon connectors for the heating elements and kettle plug for the power supply, so that it can also be unplugged and dismounted for transportation.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-machine---designed-for-disassembly/step222-1.png">
        <img class="step-image" src="/howtos/injection-machine---designed-for-disassembly/step222-1.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-machine---designed-for-disassembly/IMG_9866.JPG">
        <img class="step-image" src="/howtos/injection-machine---designed-for-disassembly/IMG_9866.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-machine---designed-for-disassembly/step22-1.png">
        <img class="step-image" src="/howtos/injection-machine---designed-for-disassembly/step22-1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Build it, hack it, share it!</h4>
<div class="step-text">
  If you have any other ideas, improvements or hacks for this machine - make sure to share them with the community.<br />
  You can do this through a how-to, discord or instagram!<br />
  <br />
  Have fun building :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/injection-machine---designed-for-disassembly/INJ machine cropped.jpg">
        <img class="step-image" src="/howtos/injection-machine---designed-for-disassembly/INJ machine cropped.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/injection-machine---designed-for-disassembly/IMG_9872.JPG">
        <img class="step-image" src="/howtos/injection-machine---designed-for-disassembly/IMG_9872.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>