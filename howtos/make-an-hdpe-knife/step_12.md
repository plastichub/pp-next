### Sand the Pins Flush 

 We VERY carefully sanded the pins flush to the surface of the aluminium scales. The low melting temperature of HDPE means that it warps incredibly easily so we kept is cool by dunking it in a bucket of water every few seconds. The pins turned out awesome though!