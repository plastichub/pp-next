---
image: /_howto/extrusion/bearing_housing/IMG_2470.JPG
category: "extrusion"
title: "Bearing Housing"
tagline: ""
description: ""
usedin:
  - lydia-v4
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2458.JPG" _target="_blank">
        <img id="IMG_2458.JPG" src="./IMG_2458.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2459.JPG" _target="_blank">
        <img id="IMG_2459.JPG" src="./IMG_2459.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2461.JPG" _target="_blank">
        <img id="IMG_2461.JPG" src="./IMG_2461.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2463.JPG" _target="_blank">
        <img id="IMG_2463.JPG" src="./IMG_2463.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2464.JPG" _target="_blank">
        <img id="IMG_2464.JPG" src="./IMG_2464.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2465.JPG" _target="_blank">
        <img id="IMG_2465.JPG" src="./IMG_2465.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2466.JPG" _target="_blank">
        <img id="IMG_2466.JPG" src="./IMG_2466.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2467.JPG" _target="_blank">
        <img id="IMG_2467.JPG" src="./IMG_2467.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2468.JPG" _target="_blank">
        <img id="IMG_2468.JPG" src="./IMG_2468.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2469.JPG" _target="_blank">
        <img id="IMG_2469.JPG" src="./IMG_2469.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2470.JPG" _target="_blank">
        <img id="IMG_2470.JPG" src="./IMG_2470.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

