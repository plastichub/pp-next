### Finish the legs 

 There’s one dimension that is on purpose bigger than needed. The reason why is so we can adjust it to the perfect size so it fits just tight. In order to do that we’ll have to measure the bigger length in one of the holes we just made in the stool top.

Then we’ll grab the legs and cut or sand down with a file the front so the final piece measures 0.8mm longer than the hole. 
Example: if the hole measures 95.8mm we want the piece to measure 96.6mm

It is recommended to do it patiently since on this step we are defining partly how tight the piece is going to join.