### Build the frame 

 To build the steel frame is a simple welding job. Cut your steel tubes according to the measurements in the picture. 

Drill two 17mm holes above each other in the centre of the frame to  add profiles which will keep the mixer shaft in a vertical position. 
To ensure that the frame always stands stable and straight, weld four nuts into the legs and add bolts with which you can adjust the height individually. 

Once everything is done, give it a proper paint job, to prevent it from rusting (as it can get very wet).