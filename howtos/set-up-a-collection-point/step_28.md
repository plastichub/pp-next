### Download and start! 

 Ready and excited to start?
You're a hero!

Download your Collection Point package and start your recycling journey!

👉 https://cutt.ly/starterkit-collection 👈