### Bend the sheets! 

 After heating up the sheets sufficiently it is time to move it to the mould. This requires some fast handling, since it quickly cools down again. Be sure to have all the clamps within reach! It also helps to do this with two persons. 