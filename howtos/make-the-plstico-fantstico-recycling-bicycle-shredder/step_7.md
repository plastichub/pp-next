### Securing the ball bearing in place 

 A ball bearing housing will need to be made to securely hold the ball bearing and therefore the axle in place. The ball bearing housing will need to be made of layers of laser cut aluminium or steel where the total thickness of all the layers equates to the thickness of the ball bearing and has a circular hole cut which is the same diameter as the outside diameter of the ball bearing to hold it securely in place. An end cap with a diameter in-between the inside and outside diameter of the ball bearing will also be needed to stop the ball bearing sliding out. Any shape can be made as long as it is attached to the shredder housing and holds the ball bearing in tight. 

I found that I additionally needed 1.5mm spacers on the inside of the ball bearing to stop it moving and keep it locked securely in place.
