### Prepare your plastic 

 Make sure you have your material ready before starting to inject. For the joints, both HDPE and PP work for the dome due to their flexibility and strength. However, PP is recommended as it is easier to inject. PS and PET are not recommended as they are brittle. Consider that the weight of one joint is about 130g, so you will need a bit more than 3.5kg for all pieces. Use around 150g PP each time to prevent lack of pressure and loss from leak out.

Tip: Try to use finely shredded plastic to reach the injection machine’s full capacity. 
