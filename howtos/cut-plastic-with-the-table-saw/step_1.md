### What makes a clean cut 

 When cutting, the blade removes small layers of plastic, which we call chips. A circular blade is made out of multiple small blades, which are called teeth. It could happen that due to the speed and force needed in the process some chips got melted damaging both the piece and the blade.

So in order to avoid overheating, several factors have to be taken into account and finding the right blade and using the right cutting settings play an important role.
