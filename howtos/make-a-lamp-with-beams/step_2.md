### Prepare central bar 

 Cut the central metal bar to the right length.

Mark the centerline on the top of the bar and colour the sides, to help you send the right angle.

Make the 2-degree draft with a belt sander after setting the table with the right angle. Once you can’t see the color anymore means you sanded the entire surface. 

This draft is really important! It will help you extracting your beam from the mould and preventing from cracking your piece as in this case we are working with polystyrene, which has the minimum shrinkage (0.4/0.7%) and it becomes quite difficult to extract from the mould.
