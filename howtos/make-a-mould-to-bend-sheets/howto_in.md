### Make a mould to bend sheets 


Bending plastic sheets is a relatively easy technique, at least compared to metal and wood. Once you have a mould you can use it over and over again to get the exact same shape. This specific how-to aims to build a simple mould from plywood and sheet metal for bending sheets in one direction. Steps 1-4 guide you through the design and preparation process, while steps 5-14 explain how to build the actual mould.