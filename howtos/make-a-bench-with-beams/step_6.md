### Test fit and refine 

 Due to the nature of this recycling process, you cannot count on every part being exactly the same. So now is the opportunity to test fit and refine the grooves for that perfect, solid fit. Ideally partA should fit snug within part B, tight enough to hold itself.

Tip: Use a heat gun to smooth and finish your plastic once your grooves fit well.