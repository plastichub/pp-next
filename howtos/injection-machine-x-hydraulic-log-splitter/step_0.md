### What you'll need 

 Barrel:

• as per PP download kit
• Steel to weld barrel to machine
• Nuts, bolts and washers x 4

Injection Rod:

• as per PP download kit
• bored out to fit M12 thread
• Steel to weld and slot over splitting knife

Hopper:

• your design, 3D printed or laser cut

Electrics:
• as per PP download kit
• load cell or something to monitor the pressure if required

Log splitter:

• 2200W 5t Electric Log Splitter. 
Here is the link the the version we purchased 
https://www.bunnings.com.au/homelite-2200w-5t-electric-log-splitter_p0044725


