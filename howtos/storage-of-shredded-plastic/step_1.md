### 19L bottles 

 A 19 liter water bottle can be a good container for storing material.
Of the pluses, it can be noted: it is lightweight, it is transparent, available worldwide.
One of the disadvantages is that you may encounter a problem of static electricity.

These bottles can be made of PET or PC (Polycarbonate). PC bottles are preferable, as they usually have a handle  on the body.
There are also special racks for storing bottles, they are very simple in design (but this is the topic for the next how-to)


