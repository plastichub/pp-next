### Drill the metal bar 

 Now that we have all the materials cut it’s time to start building our first part mould!

Mark where you will drill the holes along the central bar, place it at the center of your sheet and tap weld the edges so it won’t move. 

Clamp the pieces in the device so they are ready to drill. Placing a wooden block under the sheet will help to block the pieces while drilling.

Drill 3,3 mm holes along the bar (one each 100mm should be enough). Now you can remove the spot weld from the bar and enlarge the 3,3mm holes to 4mm holes on the sheet only. Finally, thread the holes on the bar to M4.