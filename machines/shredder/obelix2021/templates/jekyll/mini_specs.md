<span id="specs" style="padding: 16px">
    <table>
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td> Shredder
                </td>
            </tr>        
            <tr>
                <td>Version:
                </td>
                <td> 4.1
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Tested
                </td>
            </tr>
            <tr>
            <td> Authors </td>
                <td>
                 [Precious Plastic - Eindhoven](${author_link_pp})
                </td>
            </tr>
        </tbody>
    </table>
</span>
