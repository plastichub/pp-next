---
image: "/howtos/inject-a-plate-/injected-plate-step14-2.jpg"
title: "Inject a plate"
tagline: ""
description: "In this awsome &quot;How To&quot; you will make an aluminium mould for injecting a plate (3mm thickness) from recycled plastic.<br /><br />Can be used for serving dry food like nuts or for objects :)<br />Should not be used for eating purposes - only if you apply a correct coating or lacquer!<br /><br />Check out the real life plate execution here: &lt;a href=&quot;https://www.youtube.com/watch?&quot;&gt;https://www.youtube.com/watch?&lt;/a&gt; v=YzjTm3FRLVY&amp;t=5s <br /><br />This product rocks!<br />*special thanks to Paul Denney!"
keywords: "PP,injection,product,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PP"
- "injection"
- "product"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/plaatjesmakers">plaatjesmakers</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready </h4>
<div class="step-text">
  Tools needed:<br />
  - CNC, tools to cut (cutting plier), drill and welder - Mask with ABEC filter, gloves, glasses<br />
  - scale, sanding paper, polishing paste (for metal) - 4 bolts of 8m width, min length 9 cm + nuts<br />
  - 2 metal dowel pins (6m example)<br />
  - 8m + 6m drill bit<br />
  - wrenches<br />
  - 2 aluminium blocks 26x26x4cm,<br />
  - metal sheet min 15x15x0.5cm<br />
  - plumbing connector 1 inch,<br />
  - Precious Plastic stamp for your plastic type<br />
  Machines needed: Injection machine and a shredder (or shredded plastic)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.27.49.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.27.49.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - The mould</h4>
<div class="step-text">
  As we will make this plate with the injection machine, we need to make the mould for it. It will be made of 3 parts: Bottom and top part out of aluminium. And next is the connector part made from steel.<br />
  The top and bottom part will be CNC-milled out of two blocks of aluminium while we’ll make the connector part manually.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/mould top + bottom.jpg">
        <img class="step-image" src="/howtos/inject-a-plate-/mould top + bottom.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/connector + bolds.jpg">
        <img class="step-image" src="/howtos/inject-a-plate-/connector + bolds.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - CNC your top and bottom parts</h4>
<div class="step-text">
  Let’s start with the CNC part. <br />
  Download the attached STEP file and CNC-mill them into the 2 separate aluminium blocks both equal sized of minimum size 26cm x 26cm x 4cm. (Note: The standard Precious Plastic Injection machine limits the max width for moulds to 28cm).<br />
  <br />
  The step file includes 6 reference points to be milled. These points make it easy to manually drill on the exact location of the aluminium blocks. If you are not lucky enough to have access to a CNC milling machine, you can send the files to a CNC cutting company (consider that this will be more expensive and will probably take longer).<br />
  <br />
  Once the mould is cut, polish the mould to achieve a high quality surface for your product. You can do that yourself or ask the CNC cutting company to polish the mould for you.<br />
  <br />
  *special thanks to Friedrich
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.13.17.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.13.17.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Drill the holes in the aluminium mould</h4>
<div class="step-text">
  Now the mould parts need holes. We have to make 2 types of holes. Total will be 6. (the injection hole comes later)<br />
  <br />
  First we drill 2 halfway holes on both inner sides of the aluminium blocks to fit 2 metal dowel pins in there. These pins make sure your mould is always aligned straight when you are injecting. (see first image) Drill the hole size according to the dowel pin size you have. I used 6m width. Drill the holes on both sides half way. <br />
  After opening and closing the mould more often the opening and closing will become more easy. <br />
  <br />
  Then 4 holes are needed to connect and close the mould with bolts and nuts. Drill 4 holes on the 4 corners of the aluminium blocks. Use the pre marked drill indicators ór close the mould straight
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.26.12.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.26.12.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8581-edited.jpg">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8581-edited.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 22.29.31.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 22.29.31.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Drill the injection hole</h4>
<div class="step-text">
  Using a 13mm bit, drill a hole in the center of the bottom mould part. This is where the plastic will flow into the mould, out of the injection machine. The center drill indicator is also included in the STEP file.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.25.12.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.25.12.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.24.39.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.24.39.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Make the connector plate</h4>
<div class="step-text">
  To make the connection plate, we need a square steel sheet of 150cm x 150cm (minimum 0.5cm thick) and a nozzle which fits your injection machine (here: 1/2 inch - BSP - type). Check the connection types of the injector. <a href="https://youtu.be/qtZv96ciFIU?t=255">https://youtu.be/qtZv96ciFIU?t=255</a> <br />
  Mark the center points for the holes according to the drawing and drill the holes. <br />
  Then place the nozzle on top of the 13mm hole and weld it to the plate. Make sure to place it exactly in the center, this will help screwing the mould to the injector easily, and make the plastic flow through more fluently. <br />
  This part needs to align perfectly to the rest of the mould and the injection machine, so try to work as precisely as possible.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.22.54.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.22.54.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.23.48.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.23.48.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.23.39.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.23.39.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Sand the mould edges </h4>
<div class="step-text">
  The mould will be more user friendly if you sand the hard edges away, as CNC cut aluminum can be very sharp. Optionally, I would advise that you add an inclined edge to the bottom part of the mould, this will make it much easier to open and close the mould.<br />
  It is important that the inside of the mould is sanded and polished for a clean finish of the product. This will make the difference between a high quality product and a rough one. Use a wooden block with sanding paper to keep your sanding straight. Start with 120 grain size and double the amount with each step (120, 240, 440 etc. all the way up to 2000). <br />
  <br />
  Use a cloth to clean your mould from aluminium dust. Don’t blow it out of the mould this is very bad for your lungs! After, use metal grained steel wool and then fine steel wool. The last step is to polish your mould with an additional clean cloth and polishing paste.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8565.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8565.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8587.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8587.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8657.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8657.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Assemble the mould </h4>
<div class="step-text">
  You are now done with the mould’s preps and can assemble all the parts together. First connect the top and bottom part, using the dowel pins to help you with alignment and bolts and nuts to tighten all together. Then you can connect the connector plate to the top and bottom part of the mould. You’re now good to go.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8719.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8719.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8715.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8715.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Get yo plastic!</h4>
<div class="step-text">
  Use PP, HDPE, LDPE or PS (you probably will have most success with PP or LDPE).<br />
  The maximum amount that can be used for the standard injector is 150g.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8798.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8798.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.17.28.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.17.28.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.16.47.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.16.47.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - oh man! Injection time! </h4>
<div class="step-text">
  Fully fill the injector with material, but be careful not to overfill as this can lead to plastic glueing to the hopper or outside of the injector. Set your temperature based on the correct melting temperatures for your plastic type. It’s good to make the lower heating element slightly hotter than the upper, to avoid solidification of the plastic. <br />
  Move the injector lever up and down to push the material deeper into the barrel. Afterwards add more material, and leave the handle bar down to keep pressure on the plastic. After around 15 min heating lift the handle bar up and open the injector. Cut away the first drip of plastic from the barrel (might still contain some unmelted plastic), and directly screw your mould to the injector. <br />
  Wear a protective mask and eye cover, as well as heat resistant gloves during this part of the process. Safety first.<br />
  Inject your plastic by pulling down with all the pressure that you can manage, to create one continuous injection. You will need to maintain pressure for 30 seconds. Do not press in parts! If you are not sure that your strength and weight are enough, you can ask someone to help you in applying force to the injection.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8855.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8855.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8861.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8861.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8878.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8878.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Discover your creation</h4>
<div class="step-text">
  Keep the handle bar down. Quickly unscrew your mould from the injection barrel, and re-plug the connection point on the injector, so that no plastic leaks out. By applying water you can speed up the cooling process. When the mould is cooled open the two sides of your mould by unscrewing the bolts. Use a hammer and pin (like your bold for example), to hammer out the plate through the nozzle connector on the top.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8889.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8889.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8904.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8904.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8907.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8907.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Finish the plate </h4>
<div class="step-text">
  Cut away the excess material at the injection point with some sharp plier. Sand the uneven areas to give it a clean finish. For this you can use a machine or your bare hands(if you dare to).<br />
  <br />
  <br />
  *Disclaimer: <br />
  Due to no full possible verification of the plastics sources<br />
  do not eat food from this plate unless you applied a lacquer to the surface.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.16.40.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.16.40.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.16.47.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.16.47.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/DSCF8911.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/DSCF8911.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Label your plastic! Stamp the plate!</h4>
<div class="step-text">
  Ideally, at this point you should add a plastic type symbol to your product to show what plastic is made from!<br />
  This is important so that later on people know which type it is, and it can be recycled again.<br />
  There are different techniques you can use to stamp your material. <br />
  Here we used the Precious Plastic stamps (You can get them online on the Bazar).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/labelcoin.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/labelcoin.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/_DSC3526.JPG">
        <img class="step-image" src="/howtos/inject-a-plate-/_DSC3526.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/stamp.jpg">
        <img class="step-image" src="/howtos/inject-a-plate-/stamp.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Finish like a heroe</h4>
<div class="step-text">
  Grab yourself a well earned beer (or tea for the under 18’s...), and admire your work! <br />
  For any questions contact me on hoogewerfthomas@gmail.com. <br />
  <br />
  Or send a photo of your creation to me ;) <br />
  <br />
  Check out the real life plate execution in Panama!<br />
  <a href="https://www.youtube.com/watch?v=YzjTm3FRLVY&amp;t=5s<br/>">https://www.youtube.com/watch?v=YzjTm3FRLVY&amp;t=5s<br /></a>
  <br />
  *special thanks to Paul Denney!<br />
  <br />
  *****Disclaimer*****<br />
  Due to no full possible verification of the plastics sources<br />
  do not eat food from this plate unless you applied a lacquer to the surface.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/plates 660x440.jpg">
        <img class="step-image" src="/howtos/inject-a-plate-/plates 660x440.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.15.46.png">
        <img class="step-image" src="/howtos/inject-a-plate-/Screenshot 2020-02-03 at 23.15.46.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/inject-a-plate-/platepanama.jpg">
        <img class="step-image" src="/howtos/inject-a-plate-/platepanama.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>