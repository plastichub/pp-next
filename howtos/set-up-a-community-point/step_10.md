### Start meeting each other 

 Alright. Now it’s time to do something with those contacts. 

A good way to bring people together is to organise a meetup. This can be with existing recycling spaces to see how to grow together or with new people to start the local recycling network.

Check out our tips for organising a meetup to get some ideas about how to do this.

👉 tiny.cc/organise-meetup