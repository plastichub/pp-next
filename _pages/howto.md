---
title: Howtos
layout: default
permalink: /howto/
breadcrumbs: true
layout : single
author: false
sidebar: 
   nav: "howtos"
---

<hr/>

### Electrics

<div class="ty-vendor-plans">
{% for doc in site.howtos %}
  {% if doc.category == "electric" and doc.enabled!=false %}
    <div class="ty-grid-list__item">
       <a href="{{ doc.url  | relative_url }}" class="index-link">
        <span class="image" >
          <img class="cover" src="{{ doc.image }}" alt="" />
        </span>
        <header class="major vertical-margin">
            {{ doc.title }}
       </header>
      </a>
    </div>
  {% endif %}
{% endfor %}
</div>

<hr/>

### Shredder

<div class="ty-vendor-plans">
{% for doc in site.howtos %}
  {% for tag in doc.tags %}
    {% if tag == "shredder" %}
    <div class="ty-grid-list__item">
       <a href="{{ doc.url  | relative_url }}" class="index-link">
        <span class="image" >
          <img class="cover" src="{{ doc.image }}" alt="" />
        </span>
        <header class="major vertical-margin">
            {{ doc.title }}
       </header>
      </a>
    </div>
  {% endif %}
  {% endfor %}
{% endfor %}
</div>

<hr/>

### Extrusion

<div class="ty-vendor-plans">
{% for doc in site.howtos %}
  {% for tag in doc.tags %}
    {% if tag == "extrusion" %}
      <div class="ty-grid-list__item">
        <a href="{{ doc.url  | relative_url }}" class="index-link">
          <span class="image" >
            <img class="cover" src="{{ doc.image }}" alt="" />
          </span>
          <header class="major vertical-margin">
              {{ doc.title }}
        </header>
        </a>
      </div>
    {% endif %}
  {% endfor %}
{% endfor %}
</div>

<hr/>

### Sheetpress

<div class="ty-vendor-plans">
{% for doc in site.howtos %}
    {% for tag in doc.tags %}
      {% if tag == "sheetpress" %}
      <div class="ty-grid-list__item">
        <a href="{{ doc.url  | relative_url }}" class="index-link">
          <span class="image" >
            <img class="cover" src="{{ doc.image }}" alt="" />
          </span>
          <header class="major vertical-margin">
              {{ doc.title }}
        </header>
        </a>
      </div>
    {% endif %}
  {% endfor %}
{% endfor %}
</div>
<hr/>

### Injection

<div class="ty-vendor-plans">
{% for doc in site.howtos %}
  {% for tag in doc.tags %}
    {% if tag == "injection" %}
      <div class="ty-grid-list__item">
        <a href="{{ doc.url  | relative_url }}" class="index-link">
          <span class="image" >
            <img class="cover" src="{{ doc.image }}" alt="" />
          </span>
          <header class="major vertical-margin">
              {{ doc.title }}
        </header>
        </a>
      </div>
    {% endif %}
  {% endfor %}
{% endfor %}
</div>
