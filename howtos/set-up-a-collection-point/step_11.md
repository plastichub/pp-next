### Start with the Action Plan 

 Before jumping into finding a space and start collecting, it is smart to sit down to properly plan your project and shape your vision and capture why you want to do this and what your goals are. 

To help you plan we’ve made a tool called the Action Plan that helps you to craft your mission, understand your customers, revenue streams, costs and much more. With this tool you should get a step closer to create a successful project. 

Find the Action Plan in the Download Kit or learn more in the Academy
👉 http://tiny.cc/business-actionplan
