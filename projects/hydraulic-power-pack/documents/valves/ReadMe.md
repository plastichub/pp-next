## Hydraulic Power Pack

# Control Valves

Mechanical or Solenoid Valves are used to control the action of the actuator. 

These can be mounted directly onto the manifold on the mounting plate:

![Valve mounting](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/valves/valve-mounting.jpg)



# Solenoid valves 

for electronic control of hydraulic systems

[Video- Hydraulic Solenoid Valves Basics](https://www.youtube.com/watch?v=c4KXmR8QuPo)

[Video- Solenoid Operated Valves (Full Lecture)](https://www.youtube.com/watch?v=CKyYF4DNyZ8)

[Video- Spool Valves (Full Lecture)](https://www.youtube.com/watch?v=Jfdmrm4A99s)


For simple momentary action with automatic return (as with combined action clamping-injection) a 2 way Normally Closed valve can be used:

[2 way Normally Closed Valve](https://www.youtube.com/watch?v=IR6yFLXYBxc)


However, for full positional control of the actuator, double-acting 5 port 2 position valve is required.

![5 Port 2 Position Schematic](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/valves/2-position-5-way-double-solenoid-valve-for-the-double-action-air-cylinder.jpg)

For this application, the valve should be 'closed centre' (all ports blocked in central position).


The suggested package for this development is:

[CETOP 5 (NG 10) DOUBLE ACTING SOLENOID OPERATED DIRECTIONAL CONTROL VALVE 220V](https://www.parker.com/Literature/Hydraulic%20Valve%20Division/hydraulicvalve/Catalog%20sections%20for%20websphere/Industrial%20Directional%20Control/Catalog%20-%20Static%20Files/D3DW.pdf)

![CETOP 5 Valve](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/valves/cetop-5-valve.jpeg)



[UK supplier- Phoenix Hydraulics](https://www.phoenixhydraulics.co.uk/Products/CETOP-5-Directional-Control-Valves-Double-Solenoid/D3W001CNTW)

[UK supplier- Steerforth](https://www.steerforth.co.uk/cetop-valve-ng10-double-acting-cetop-solenoid-valve-p-4425.html?osCsid=plu81q5eklubd3oa3f27bb5se4)




# Flow Control valves

Controlling actuator speed

[Video- Flow Control Valves (Full lecture)](https://www.youtube.com/watch?v=Tn3bsiQx1Ug)


Valve Brands:

Check:
UK- Rexroth; Parker
China- Yuken
India- Veljan

