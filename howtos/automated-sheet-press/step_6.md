### Sheetmaker part 1 

 •	The sheet maker page allows users to make an automated sheet. It is possible to select the type of plastic and the required thickness of the sheet. We use metal frames to set thickness and sheet size/shape. Based on these parameters the screen will indicate the weight needed to produce the sheet. The time and temperature needed to make a sheet is also displayed. 
•	In the following step it is possible to start the  heating process. This can be done while preparing the sheet on the prep table. The system automatically sets the correct temperature for each plastic type.
 