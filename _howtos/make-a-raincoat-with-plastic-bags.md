---
image: "/howtos/make-a-raincoat-with-plastic-bags/coat-3.jpg"
title: "Make a raincoat with plastic bags"
tagline: ""
description: "From plastic foils to a rain jacket. In this tutorial I will explain how to collect plastic foils, melt them together, sew and put it all together to create your own jacket for rainy days.<br /><br />Step 1-9: Preparing the materials<br />Step 10-16: Sewing your jacket"
keywords: "melting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather your materials </h4>
<div class="step-text">
  In order to make a jacket from plastic foils, you will first need to gather your tools and materials. These are:<br />
  <br />
  Materials<br />
  - plastic foils (see step 3), enough to make four large sheets (70cm x 170cm)<br />
  - natural fabric such as cotton for the lining<br />
  - a heat resistant sheet material such as teflon fabric as a base for ironing/pressing<br />
  - baking paper which will also be used for ironing/pressing<br />
  - optionally: fastening of some sort for the raincoat (buttons, zip etc)<br />
  <br />
  Tools<br />
  - iron or thermo press<br />
  - sewing machine, thread and scissors<br />
  <br />
  Safety recommendations<br />
  - respirator mask to prevent the inhalation of plastic fumes!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-5.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-4.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/plasticdoom_tutorial-3-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Choose your measurements </h4>
<div class="step-text">
  You will need to work out the required measurements for the jacket. The template below will provide you with a plan for taking and recording these measurements. <br />
  (A) length of the arm<br />
  (B) length from shoulder to shoulder<br />
  (C) length from shoulder to the neck centre<br />
  (D) body width<br />
  (E) desired length of the raincoat
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/3-image22.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/3-image22.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Collect and select your foils</h4>
<div class="step-text">
  This part will be a combination of your own design choice, as well as what plastic material is available around you. Collect your waste foils, which can include plastic bags and any small plastic foils, wrappers, and packaging. Try to make sure that you are using the same type of material (LDPE, HDPE, PP etc.). <br />
  <br />
  Now, use some artistic flair! From what you have, select a colour palette that you will work with. Your raincoat will have a much nicer finish if you ensure that any colours/patterns work well together. If you’re happy to use any combination of colours, then the coat will still be functional.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/4-1plasticdoom_tutorial-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/4-1plasticdoom_tutorial-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/4-2 plasticdoom_tutorial-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/4-2 plasticdoom_tutorial-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Make your base</h4>
<div class="step-text">
  Find the biggest plastic sheet you have, and make a base out of it. Foils from construction waste are often large and good for this purpose. If you don’t have something such as this to hand, then you can cut down the sides of a bag and spread it out flat on your working surface.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/5-1-plasticdoom_tutorial-4_1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/5-1-plasticdoom_tutorial-4_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/5-2-plasticdoom_tutorial-4_2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/5-2-plasticdoom_tutorial-4_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/5-3-plasticdoom_tutorial-4_3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/5-3-plasticdoom_tutorial-4_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Create a collage with your material</h4>
<div class="step-text">
  Using your plastic bags and films, create a collage on top of your base sheet. This can be created however you like; from a methodical design of strips and shapes, to a more random spread of the films. This will create the design for the material that will form your finished raincoat. Reserve a little of the plastic to form a test piece (see step 6).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/6-image7.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/6-image7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Try out your fusing technique</h4>
<div class="step-text">
  ❗️Put on your respirator mask on!<br />
  <br />
  Layer a few pieces of your reserved plastic one above the other, as a test sample. Turn on your iron and set to the highest temperature. (If you are using a thermo press you may need apply different temperature settings). Lay your sample piece on top of the teflon fabric, and put a piece of baking paper on top to avoid the plastic from sticking to your iron or press. Now iron the sample piece to see how the material fuses together.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/7-1-plasticdoom_tutorial-5-3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/7-1-plasticdoom_tutorial-5-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/7-2-plasticdoom_tutorial-5-4.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/7-2-plasticdoom_tutorial-5-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Make your fused sheet</h4>
<div class="step-text">
  Once you are comfortable with your tests, go ahead and fuse together your big sheets, remembering to use teflon fabric below, and your baking parchment paper as an ironing surface above. You will need to make at least four big sheets, around 70cm x 170cm. <br />
  <br />
  💡 Fix holes: Once complete, look over your fused plastic sheets and find any holes or parts which are not fully melted or secure. You can fix these now with your iron/press and any scraps of plastic that you have left, but be very careful not to deconstruct the material when heating the sheets again.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/8-1plasticdoom_tutorial-7-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/8-1plasticdoom_tutorial-7-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/8-2-plasticdoom_tutorial-7-4.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/8-2-plasticdoom_tutorial-7-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/8-3-plasticdoom_tutorial-7-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/8-3-plasticdoom_tutorial-7-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/8-4-plasticdoom_tutorial-7-3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/8-4-plasticdoom_tutorial-7-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Finalise your measurements</h4>
<div class="step-text">
  Come back to the template that we used in step 2. Apply your measurements to the following template, and decide on a size for measurement (F), the diameters of the hood. You can decide on this by drawing and cutting the hood construction pieces first with paper, and finding a desired size.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/9-image100.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/9-image100.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Cut the materials</h4>
<div class="step-text">
  From your fused sheets you will now need to cut the template pieces (see step 8) for the construction of your raincoat. 1 back piece, 2 front pieces, 2x sleeves, and 3 parts for the hood. Add 2cm on the sides of each piece to allow room for sewing. If you are lacking material to cut the template, you may need to extend one or more of your sheets.<br />
  <br />
  After doing this, cut the same template again from your cotton (again allowing 2cm on every side). This will form the lining of the raincoat.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-1-plasticdoom_tutorial-9-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-1-plasticdoom_tutorial-9-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-2-plasticdoom_tutorial-9-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-2-plasticdoom_tutorial-9-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-3-plasticdoom_tutorial-9-3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-3-plasticdoom_tutorial-9-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-4-plasticdoom_tutorial-9-4.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-4-plasticdoom_tutorial-9-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-5-plasticdoom_tutorial-9-5.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-5-plasticdoom_tutorial-9-5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-5-plasticdoom_tutorial-9-6.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-5-plasticdoom_tutorial-9-6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-6-plasticdoom_tutorial-9-7.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-6-plasticdoom_tutorial-9-7.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-7-plasticdoom_tutorial-9-8.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-7-plasticdoom_tutorial-9-8.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/10-10plasticdoom_tutorial-10-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/10-10plasticdoom_tutorial-10-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Sew the outer and lining together</h4>
<div class="step-text">
  Using your cut plastic sheets, and the matching cotton pieces, sew the two templates together so that the outer and lining are attached to one another. This will help the process of further working with the material, and make it easier to create your raincoat.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/11-1-plasticdoom_tutorial-11.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/11-1-plasticdoom_tutorial-11.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/11-2-plasticdoom_tutorial-12-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/11-2-plasticdoom_tutorial-12-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/11-3-plasticdoom_tutorial-12-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/11-3-plasticdoom_tutorial-12-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/11-4-plasticdoom_tutorial-12-4.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/11-4-plasticdoom_tutorial-12-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Construct the shoulders</h4>
<div class="step-text">
  The construction of the jacket should start with the shoulders. Sew together your back and two front pieces (see step 8), as shown in the image below. Make sure to leave enough space for the attachment of the hood! This is where we will use the 2cm that we added for sewing. The join of the fabric should match the template exactly and the excess material from the join should only be visible from the interior of the jacket. We will trim this later.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/12-image6 copy.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/12-image6 copy.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/12-plasticdoom_tutorial-14-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/12-plasticdoom_tutorial-14-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-4.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-5.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-6.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-7.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/12plasticdoom_tutorial-14-7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Finish the edges of your sleeves</h4>
<div class="step-text">
  Now we will process one edge (the edge that will form the wrist opening) of each sleeve. Fold the material, and using the 2cm that we added for sewing, create a finished edge for the sleeve. If you are unsure, the illustration below should clarify how this will work.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/13-image3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/13-image3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/14-.png">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/14-.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/14.png">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/14.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/14-plasticdoom_tutorial-16-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/14-plasticdoom_tutorial-16-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/14-plasticdoom_tutorial-16-3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/14-plasticdoom_tutorial-16-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Connect sleeves and shoulders</h4>
<div class="step-text">
  The open sleeve pieces should now be connected to our shoulder construction, the joint. can be seen in the illustration below. As with steps 11 and 12, use the 2cm allowance to create the join exactly to the template measurements. Any excess will only be seen from the interior.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/15-image23.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/15-image23.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/15-plasticdoom_tutorial-17-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/15-plasticdoom_tutorial-17-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/15-plasticdoom_tutorial-17-3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/15-plasticdoom_tutorial-17-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/15-plasticdoom_tutorial-17-4.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/15-plasticdoom_tutorial-17-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/15-plasticdoom_tutorial-17-5.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/15-plasticdoom_tutorial-17-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Sew the side seams</h4>
<div class="step-text">
  Fold your current construction so that the cotton side is exposed, and sew along the side seams of the raincoat; remembering to use the 2cm that you added as a sewing excess.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/16-image17.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/16-image17.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/18-plasticdoom_tutorial-18-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/18-plasticdoom_tutorial-18-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/18-plasticdoom_tutorial-18-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/18-plasticdoom_tutorial-18-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/18-plasticdoom_tutorial-18-3.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/18-plasticdoom_tutorial-18-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Create and attach the hood</h4>
<div class="step-text">
  The hood consists of three parts, two sides and one central connecting piece. They are sewn together as shown in the illustration below. Remember as always, to sew from the inside of the garment and to use the 2cm of allowance that you have in your join. Once constructed, the hood can be attached to the body of the raincoat. The construction and attachment of the hood can be tricky, so if you have any issues with exposed stitching we will fix this in the next step.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-image14.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-image14.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-1.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-2.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-7.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-7.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-9.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-9.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-11.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-11.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-12.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-12.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-13.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-13.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-14.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/17-plasticdoom_tutorial-19-14.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Finish the raincoat</h4>
<div class="step-text">
  Finishing the raincoat could be different depending on how/if you choose to fasten the front. In any case, finishing should begin with sewing clean any unfinished seams (the bottom edge for example). <br />
  <br />
  The process should then finish with any fastenings that you choose to incorporate; this could be buttons, poppers, a zip, or any alternatives that you can think of.<br />
  Now it’s ready to wear!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-raincoat-with-plastic-bags/19-.jpg">
        <img class="step-image" src="/howtos/make-a-raincoat-with-plastic-bags/19-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>