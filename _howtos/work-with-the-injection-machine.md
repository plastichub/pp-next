---
image: "/howtos/work-with-the-injection-machine/2-Machine.jpg"
title: "Work with the injection machine"
tagline: ""
description: "The scope of this How-To is the set up and operate the injection moulding machine Precious Plastic v3. The start point is right after you finished building your injection machine and want to start up the first time. This How-to is especially for beginners and tries to cover all important facts to consider for operating the injection machine."
keywords: "injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Safety</h4>
<div class="step-text">
  Wear heat resistant gloves to prevent burning yourself from heated parts like: injection barrel, nozzle, plunger and the mould, which can heat up over time! You can use thick leather gloves, which are used for welding. Preferably they have a smooth outside surface (not suede leather), so plastic cannot stick to it.<br />
  Wear safety glasses during the injection process!<br />
  Avoid contact with molten plastic at all times!<br />
  Make always sure your nozzle connectors are in a good condition! Defect adapters can enable the plastic to squeeze out from the barrel under high pressure and cause risk to health!<br />
  Be careful while opening the nozzle, plastic can drip out immediately after opening!<br />
  Use a fume extractor with a rated filter to extract the fumes! See How to xxx
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/1-Safety.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/1-Safety.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Machine Limits</h4>
<div class="step-text">
  There are some physical limitations which have to be considered when using the injection machine.<br />
  <br />
  Precious Plastic Injection Machine v3<br />
  <br />
  Volume: 150cm³<br />
  Pressure: 44bar<br />
  <br />
  The volume determines the maximum volume of the cavity together with gate, runners and sprue you can fill.<br />
  The injection pressure determines how fine your details can be and what is the minimum achievable wall thickness.<br />
  <br />
  Depending on your machine build those values might vary according to the calculation in the image.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/1.1.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/1.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/1.2.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/1.2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/1.3.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/1.3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Fix the machine</h4>
<div class="step-text">
  It is useful if you can bolt the machine to the floor, a wall or add a pallet under it to prevent it from falling over during the injection process.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/4-Machine Preperation.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/4-Machine Preperation.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Preheat the barrel</h4>
<div class="step-text">
  To start the machine plug in the plug and switch on the power.<br />
  <br />
  Do not try move the plunger as long the material melt temperature is not reached!<br />
  <br />
  There are different types of PID controllers on the market, but the main functions seem to be similar over a wide range of available products. <br />
  To change the temperature hold the button (Set) until one of the digits lights up. Switch between the digits with the button (Arrow Left) and then increase (Arrow Up) or decrease (Arrow Down) the temperature. Set the target temperature according to your material to the center of the temperature range. Check the starter kit for an overview of specific melt points of different polymer types.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/2.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Prepare your material</h4>
<div class="step-text">
  Close the injection barrel with the nozzle cap and fill in the new material into the hopper. Leaving ~1cm of space to the top of the inlet prevents the plunger from getting stuck during the injection process by shearing of unmelted plastic.<br />
  Before the injection can start, a heat-up time of 10-15min is recommended to melt the material uniformly. Do not try to move the plunger before the preheating is done!<br />
  There might be still some old material left inside the barrel. Therefore actuate the lever without a mould or closing lid attached, until you stop seeing the previous material leaving the nozzle. Make sure your production leftovers are getting recycled properly!<br />
  For big moulds (&gt;100g) the first infill might be not enough to fill the mould fully. Use the plunger to compress the material within the barrel and add more material, so the barrel is full with plastic.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/3.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Close the mould</h4>
<div class="step-text">
  You can start to prepare the mould by closing it and tighten the clamping bolts. Use two (open) ring spanners to open and close the moulds. For the best clamping results you can use an adjustable torque wrench to make sure the clamping is repetitive accurate. The necessary clamping force is also related to the used material.<br />
  <br />
  Keep in mind that mostly the mould thickness (and screw distance) is a limiting factor to the final clamping force. In case your plastic part has flash marks (plastic runs between mould halves), you may add additional steel plates to the outside to increase the structural rigidity.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/10-Close Mould.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/10-Close Mould.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Attach the nozzle connector</h4>
<div class="step-text">
  As a nozzle connector (see figure 1 and 2) on the machine side normally a female G 1/2&quot; pipe thread (red) is used. A mould needs a counter-fitting thread or a separate nozzle adapter which fits. We use a male G1/2&quot; to male G1/2&quot; adapter (yellow). <br />
  Note that the standard of G and R thread type are compatible.<br />
  <br />
  If there is no separate nozzle adapter used on the mould, take care that you do not over tighten the nozzle connector on the mould side. If you ruin a thread in a aluminium mould, the mould needs expensive and difficult repair. For this reason, try to leave the adapter attached to the mould at all times, so the thread in the mould does not get worn out.<br />
  <br />
  To prevent damage to the thread over a large number of injections (for events or small series production) change your nozzle adapter to a slider connector ( <a href="https://community.preciousplastic.com/how-to/make-a-quick-release-for-the-extrusion-machine">https://community.preciousplastic.com/how-to/make-a-quick-release-for-the-extrusion-machine</a> ) or use additional thread adapters which can be swapped easily (see figure 3).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/16_Connector-6.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/16_Connector-6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/16_Connector-4.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/16_Connector-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/16_Connector-1.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/16_Connector-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Injecting</h4>
<div class="step-text">
  To achieve the highest leverage, it is recommended to actuate the lever from the very end.<br />
  You can increase the force by operating the machines with two persons or increase the length of the lever (see figure 1). <br />
  <br />
  If you reached the final temperature and the plastic tends to drip out from the nozzle, you are ready to start with injecting. To be sure to have a successful injection, actuate the injection lever gently and remove the very front of the material for the first injection.<br />
  <br />
  To start you remove the closing cap from the injection barrel. Make sure to attach the mould as fast as possible, after removing the closing cap. Otherwise the front of the plastic might cool down and impede the injection process.<br />
  <br />
  Keep the pressure for ~5 seconds. This reduces the shrinkage and prevents from a &quot;vacuum&quot; effect, where the molten material is moved back into the injection barrel (see figure 2)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/17-Injecting-1.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/17-Injecting-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/17-Injecting-2.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/17-Injecting-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Opening the mould</h4>
<div class="step-text">
  The mould usually can be opened immediately after the injection process. Unscrew the mould clamping screws. Two flat headed screwdrivers can be used to equally separate the two mould halfs apart from each other (see figure 1).<br />
  <br />
  After opening the mould, the part can be removed from the mould. Depending on the mould design it can be easier to remove the injection sprue first (see figure 2). So all the connected parts come out at the same time. <br />
  <br />
  Be very careful in removing the part from the mould and try to avoid the use of any hard (metal) tools within the cavity! They can scratch and damage the mould permanently. A damaged mould requires time intensive re-work by sanding and polishing or even closing the holes by welding and further machining.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/Opening Mould.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/Opening Mould.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/Part Ejection.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/Part Ejection.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Done</h4>
<div class="step-text">
  Congratulations, you just injected your first part! <br />
  If something went wrong, check on the bottom of this page for our troubleshooting reference.<br />
  <br />
  Remove the single parts from the runners by breaking or cutting of the part at the gate location.<br />
  <br />
  Finish your part, by trimming off the leftover gate material with a sharp knife. Watch your fingers!<br />
  <br />
  Make sure, all your products have labels before you hand them out to customers!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/18-Done-1.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/18-Done-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/18-Done-2.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/18-Done-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Finish your work</h4>
<div class="step-text">
  You can push the injection lever to the top position, so you can start by adding fresh material the next time you start the machine. This makes sure, that the injection plunger won’t be stuck in the injection barrel.<br />
  <br />
  Clean the area where you work from all leftover plastic. The next person who uses the machine will not know which materials you used!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/work-with-the-injection-machine/20-Machine Off.jpg">
        <img class="step-image" src="/howtos/work-with-the-injection-machine/20-Machine Off.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Troubleshooting</h4>
<div class="step-text">
  Injection moulding is a complicated process where many different process parameters have to be dialed in to the right settings. We keep a Troubleshooting guide updated ( REFERENCE ) to help you if something goes wrong.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>