### Shredding 

 Here you have several options: 
1) to grind plastic manually with a hand tool;
2) to grind plastic with a shredder or crusher;
3) to buy already crushed or granulated raw materials.