### Cutting speed 

 Usually while working with wood we set the machine to up to 4500 rpm. For plastic it is not the case.

Much slower cutting speed (around 3000rpm) combined with a fast feeding will give the best results.