### Define what kind of workspace 

 For the launch of V4 we are going to have more focussed workspaces. Here you can choose what you are. If you are not sure or use multiple machines, you are probably a mixed.

Info: You will always be able to change this choice in the future, in case you change your focus.