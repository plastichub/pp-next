### Make center mould 

 -Select all of the items using the selection tool or by pressing CTRL + A
-Now duplicate all the objects pressing CTRL + D
-Now hold CTRL and move the duplicated objects to be just below it to avoid wasted material when laser cutting
-Now you can delete the rulers and the material logo by selecting it and pressing delete