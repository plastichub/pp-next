### Spread the Love 

 -Open source is not Open source if you don't share the source & files
-You can put it on GitHub or other places like GitLab as well
-Feel free to share your work with us on our social media

At Darigov Research we specialise in open source hardware, software and education to help people tackle global issues in their local community.

If you wish to support us in the work that we do consider donating or joining us on Patreon

Donate - https://www.darigovresearch.com/donate
Patreon - https://www.patreon.com/darigovresearch

Website - https://www.darigovresearch.com/
Youtube Channel  –  https://www.youtube.com/channel/UCb34hWA6u2Lif92aljhV4HA
Twitter, GitHub, Instagram - @darigovresearch