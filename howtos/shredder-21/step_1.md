### Laser cut and assemble 

 Download and send the cutting files to your local lasercutter and get the parts cut. 
This version is similar to the V2 but some components have been simplified and combined with laser cutting.

Then assemble the laser parts, making sure the alignment is right then weld the parts together.