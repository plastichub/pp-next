### Transfer the next sheet 

 Check your timetable for when the next sheet is due to go into the sheetpress. 
Open the sheetpress and then using the sliding tool, transfer the mould into the sheetpress and close the sheetpress (same as in step 6). 

Check your timetable for when it is time to unload from the cooling press, demould and store the first sheet. You can prepare by moving the prep table in front of the cooling press.