### The money bit 

 We collect the bottles for free from the hotels and resorts (I know we should really charge them for the service, even more since we also supply the collection bins for free, lesson learned:)). While the collected PET is sold back to the industry for 55 Sri Lankan Rupees per kg (about 0.26 €). The products we make from the bottle caps are our high margin items for sure. The more value addition we can create the better the margin, bulk selling just the raw material is a very low margin operation and needs a decent scale. 
