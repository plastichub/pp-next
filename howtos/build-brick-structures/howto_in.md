### Build brick structures 


Recycled bricks are a great way of recycling large amounts of plastic. So what's better than building your own recycled plastic bike shed or another similar structure? 

This How-to is split up into the following sections:
Step 1 - 2 Intro
Step 3 - 6 Fixed structure
Step 7 - 19 Build walls
Step 20 - 26 Make the roof
