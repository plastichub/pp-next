### Cleanup 

 -As this is open source work we will need to remove the default text from the template
-While we're at it we can also remove the details table on the right hand side
-We done this by using a blank white .png file and using it as a mask
-Select the "Insert Bitmap from file" button and use the masks provided in this tutorial
	-You'll need to put it in the right position & adjust for the size
	-This is done by adjusting the parameters after clicking it in the left hand panel