### Why not to wash everything by hand 

 In a small scale, it can make sense to wash your plastic by hand with a brush and some soap. As soon as we have a bigger amount of dirty plastic though it isn’t economical anymore. It takes a lot of time and uses a lot of water

 So integrating a semi-automatic machine with a sand filter helps a lot. The water is constantly cleaned and can be used for a long time. As we don’t work on an industrial level, there is still plenty of manual work involved, but it is still a much more efficient solution.
