### Single-use filter 

 A very basic way of filtering is polyester wool. Very accessible and cheap, but plastic and usable only once. So use it with care.
To build it, we need a casing for the wool, like a sealable plastic box. Drill holes for the fittings and build a simple clamp system to keep it properly closed.

It works best if put in line before the pump, so a vacuum is created inside the box and the lid does not open. Fill it up with wool, with light pressure, but not too much as the pump won’t manage to suck the water through it otherwise. The wool should have enough capacity to empty one dirty barrel, this is depending on the amount of dirt and size of box.