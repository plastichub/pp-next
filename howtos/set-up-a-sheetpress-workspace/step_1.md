### Outcomes 

 The outcome of a Sheetpress Workspace is recycled plastic sheets. 

These sheets can be of different sizes up to 1x1 m and with varying thicknesses from 5mm up to 30mm (more to be explored!).

Make sure to play around with various patterns and colours to create stunning recycled sheets. 