---
image: https://precious-plastic.org/products/v4/extrusion-pro/renderings/perspective.JPG
category: "extrusion"
title: "Precious Plastic Extrusion v4"
product_rel: "/machines/extrusion/30mm-expensive/"
tagline: ""
description: "Precious Plastic - Machine : Extrusion :: Precious Plastic Extrusion v4"
product_id: extrusion-pro
tags:
 - lydia-v4
 - v4
 - extrusion
alternatives: 
 - lydia-v4
header:
  image: /v4/extrusion-pro/media/preview.jpg
  overlay_image: /v4/extrusion-pro/media/preview.jpg
  overlay_filter: 0.2 # same as adding an opacity of 0.5 to a black background
  caption: ""
wiring: true

gallery_drawings:
 - url: "/machines/extrusion/30mm-expensive/drawings/00_How-to-make-cone-hopper.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/00_How-to-make-cone-hopper.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 0000.00-A Main assembly.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 0000.00-A Main assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1000.00-A Extruder.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1000.00-A Extruder.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1100.00-A Screw main assembly.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1100.00-A Screw main assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1100.01-A Nozzle.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1100.01-A Nozzle.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1100.02-A Screw.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1100.02-A Screw.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1100.03-A Screw tip.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1100.03-A Screw tip.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1110.00-A Gearbox flange.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1110.00-A Gearbox flange.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1110.01-A - Flange spacer.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1110.01-A - Flange spacer.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1110.02-A - Screw side flange.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1110.02-A - Screw side flange.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1110.03-A - Motor side flange.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1110.03-A - Motor side flange.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1120.00-A Bearing Body _ Shaft.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1120.00-A Bearing Body _ Shaft.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1120.01-A Seal bearing spacer.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1120.01-A Seal bearing spacer.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1120.02-A Coupling shaft.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1120.02-A Coupling shaft.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1121.00-A Bearing Body.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1121.00-A Bearing Body.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1121.01-A Transition Piece.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1121.01-A Transition Piece.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1121.02-A Bearing body.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1121.02-A Bearing body.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1130.00-A Barrel inlet.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1130.00-A Barrel inlet.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1130.01-A Barrel inlet.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1130.01-A Barrel inlet.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1130.02-A Barrel flange 1.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1130.02-A Barrel flange 1.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1130.03-A Barrel flange 2.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1130.03-A Barrel flange 2.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1140.00-A Barrel.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1140.00-A Barrel.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1140.01-A Barrel.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1140.01-A Barrel.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1140.02-A Barrel flange.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1140.02-A Barrel flange.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1140.03-A Barrel tip.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1140.03-A Barrel tip.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.00-A Cone Hopper.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.00-A Cone Hopper.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.01-A - Top.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.01-A - Top.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.02-A - Bottom.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.02-A - Bottom.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.03-A Insulation.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.03-A Insulation.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.04-A - Cone hopper.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 1200.04-A - Cone hopper.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 2000.01-A Electronics box support.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 2000.01-A Electronics box support.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 2100.00-A Electronics box assembly.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 2100.00-A Electronics box assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 3000.00-A Frame.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 3000.00-A Frame.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 3000.01-A Main frame.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 3000.01-A Main frame.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 3000.02-A Top shelf.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 3000.02-A Top shelf.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 3000.03-A Bottom shelf.PDF"
   image_path: "/machines/extrusion/30mm-expensive/drawings/EXTPRO-V1 3000.03-A Bottom shelf.jpg"
   alt: ""
   title: ""
---
<div prefix="/templates/jekyll/" file="machine.html" context="machine global" class="fragment">
    
    <div prefix="/templates/jekyll/" file="machine_layout.html" context="machine global" class="fragment">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
        </li>
        {% if page.tabs.build != false %}
        <li class="nav-item">
            <a class="nav-link" id="build-tab" data-toggle="tab" href="#build" role="tab" aria-controls="build" aria-selected="false">Build</a>
        </li>
        {% endif %}
        <li class="nav-item">
            <a class="nav-link" id="resources-tab" data-toggle="tab" href="#resources" role="tab" aria-controls="resources" aria-selected="false">Resources</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="howtos-tab" data-toggle="tab" href="#howtos" role="tab" aria-controls="howtos" aria-selected="false">Howtos</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#media" role="tab" aria-controls="media" aria-selected="false">Media</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#discussion" role="tab" aria-controls="media" aria-selected="false">Discussion</a>
        </li>

        {% if page.wiring %}
        <li class="nav-item">
            <a class="nav-link" id="wiring-tab" data-toggle="tab" href="#wiring" role="tab" aria-controls="wiring" aria-selected="false">Electronics</a>
        </li>

        {% endif %}


    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="overview" role="tabpanel" aria-labelledby="overview-tab">

            {% include download_spec.html %}

            
            <hr />
            
            <hr />

            <div prefix="/templates/jekyll/" file="used_in.md" context="machine global" class="fragment">
    <p>{% if page.usedin %}
    <h4>This is used in</h4>
    <div class="ty-vendor-plans small">
        {% for component in page.usedin %}
        {% for doc in site.machines %}
        {% if doc.product_id == component %}
        <div class="ty-grid-list__item_small">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover_small" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    {% endif %}</p>
</div>

        </div>

        {% if page.tabs.build != false %}
        <div class="tab-pane" id="build" role="tabpanel" aria-labelledby="build-tab">

            

            

            {% if page.gallery_drawings %}
            <h4>Drawings</h4>
            {% include gallery_drawings caption="" %}
            {% endif %}

        </div>
        {% endif %}

        <div class="tab-pane" id="resources" role="tabpanel" aria-labelledby="resources-tab">
            <span style="font-size:smaller">

                

                

                <h4>Changelog</h4>

                <div prefix="machines/extrusion/30mm-expensive/" file="changelog.html" context="machine" class="fragment"></div>

            </span>

        </div>
        <div class="tab-pane" id="howtos" role="tabpanel" aria-labelledby="howtos-tab">
            
            <h4>Build Howtos</h4>
            <div prefix="/templates/jekyll/" file="howto_used_in_machine.md" context="machine global" class="fragment">
    <div class="ty-vendor-plans">

        {% for doc in site.howto %}
        {% if doc.usedin and doc.enabled!=false %}
        {% for used in doc.usedin %}
        {% if used == page.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endif %}
        {% endfor %}
    </div>
</div>
        </div>
        <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="media-tab">
            
            {% if page.gallery %}
            {% include gallery caption="" %}
            {% endif %}

            {% if page.gallery_social %}
            <h4>Media on the network</h4>
            {% include gallery_social caption="" %}
            {% endif %}

        </div>

        <div class="tab-pane" id="discussion" role="tabpanel" aria-labelledby="discussion-tab">
            <div id='discourse-comments' style="min-height: 600px;"></div>
            <script type="text/javascript">
                DiscourseEmbed = {
                    discourseUrl: 'https://forum.precious-plastic.org/',
                    discourseEmbedUrl: '{{site.url}}{{page.url}}.html'
                };

                (function() {
                    var d = document.createElement('script');
                    d.type = 'text/javascript';
                    d.async = true;
                    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
                })();
            </script>
        </div>

        {% if page.wiring %}
        <div class="tab-pane" id="wiring" role="tabpanel" aria-labelledby="wiring-tab">
            
        </div>
        {% endif %}

    </div>

    <div prefix="/templates/jekyll/" file="alternative_machines.md" context="machine global" class="fragment">
    <p>{% if page.alternatives %}</p>
    <h3 id="alternatives">Alternatives</h3>
    <div class="ty-vendor-plans">

        {% for doc in site.machines %}
        {% for alternative in page.alternatives %}
        {% if alternative == doc.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    <p>{% endif %}</p>
</div>
</div>


</div>