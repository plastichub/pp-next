## Hydraulic Power Pack

# Bell Housing

If a submersible pump is used, the motor must be mounted vertically to a Bell Housing which contains the coupling to the pump.

![Bell Housing diagram](https://pim-resources.coleparmer.com/item/l/fmi-p56c-series-q-pump-head-to-nema-type-56-c-face-motor-adapter-kit-0711785.jpg)

![6028 Bell Housing](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/bell-housing/bell-housing-6028.jpg)


## Specifications:

Model: 6028 Pump Motor Adapter

Motor mounting: 56C

Flange (pump): SAE A

Overall length: 4.501" 

Diameter: 4.25"

Pilot (tank hole) diameter: 3.25"

Coupling size: PM90  3.5" max

Weight: ~ 2lb



[CAD model](https://www.lovejoy-inc.com/wp-content/uploads/CAD_files/hydraulics/bellhousings/84527105123_asm.stp)

