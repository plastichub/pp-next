### Reach out  

 When there is a possibility to arrange a meeting, you are now able to go to one of these businesses and present your collection point to them. This is an exciting moment. Keep in mind that you want them to start collecting plastic for you. 

A nice way to check if the business has plastic for you to recycle is to have a look together through their collected plastic (plastic packaging) from a week. Most businesses need the same materials weekly which also means the same packaging. If you have the time, explain why you can collect some plastic and why some not. 

Tip: Do not only tell your story but also listen well to their experiences with their plastic waste and their needs. 
