### Observe plastic uses in your area 

 Check around you. There is probably a lot of plastic being used and discarded.

It’s your task to individuate sources of plastic that could be recycled in the Precious Plastic Universe. Depending on where you live could be your neighbours, colleagues, local restaurants, shops or dumpsites.