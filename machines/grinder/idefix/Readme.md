---
id: Idefix
layout: home
---



# Grinder

![](./renderings/rev-b.jpg)

Variants : Addon, Standalone, Asterix, IdeFix mini

This build can be applied to PP v3 shredders. It only needs a few steel bars and custom made grinder knifes

**brief** : General overview for a PET grinder

**document scope** : client, vendor, manufacturer

**version** : Rev-A

**status** : design & prototyping

**license** : open-source

---


## Todos

- [x] Firmware
- [x] Cabinet
- [-] Control-Panel
- [x] Update BOM
- [x] CAD:Apply OSR naming
- [x] Laser:Apply OSR naming
- [ ] Sound-proove option

### Sound prooving 

- [Techniques for industrial machines & material supplier - UK](https://www.customaudiodirect.co.uk/soundproofing-industrial-machines#:~:text=Materials%20such%20as%20our%20Pyrosorb,reduce%20the%20overall%20sound%20output.)

- [Supplier - DE](https://www.aixfoam.com/technical-acoustics/soundproofing-machinery-construction-industrial-systems)

Since proper material seems expensive (200E for 2sqm), 2 optional layers are offered. The first should remove about 30dB, the second 60.

![](./resources/machine_enclosures2.jpg)

An additional hopper and as well an exit chute is required in this case.

## Todos - XMax (Q3-2021)

Application: plastic bottles

NOMINAL PLASTIC CAPACITY: > 50 Kg/day

NUMBER OF ROTATING BLADES: 12

SPEED CUTTING: ˜ 350 rpm

NOMINAL PARTICLE SIZE: ˜ 6 mm

TOTAL DIMENSIONS: 600x860x1600 mm

TANK HOPPER DIMENSIONS: 300 X 300 mm

DIMENSIONS OF THE CRUSHING CHAMBER: 200 X 250 mm

IMPELLER DIAMETER: 200 mm

WASTE REDUCTION PLASTIC BOTTLES: 20:1

NOMINAL ELECTRICAL POWER: 2,2 KW


## Files / Components

[Fusion360](./cad) | [3D Online Preview](https://a360.co/2wlHFaB)

[SolidWorks](./cad/)

## Constraints

- 6 mobile knifes | 2 stationary kifes - each adjustable
- v3 shredderbox * 0.3
- UFCL 205 bearings (shaft size : 30mm)
- dual pulley
- small footprint
- different sieves
- 220V/380V, 2Kw for now
- 400 - 700 RPM
- minimal height (eg: to enable it as addon)
- wheels
- Hopper with flake guide, door (flakes shoot really high)

## References

- [Short video rev.3](https://www.youtube.com/watch?v=ElOnSA7pS18&feature=youtu.be)

## Related

- [Noah umbrella project](https://gitlab.com/plastichub/noah/blob/master/machines/Noah.md)
- [PP and filament & 3D-print projects](https://precious-plastic.org/home/library/articles/filament/)
- [PP Wiki - components](https://precious-plastic.org/home/library/components/)
- [PP Forum - PET print](https://davehakkens.nl/community/forums/topic/3d-printer-for-pet-particles-no-filament-ever/)
- [Video - 3D-seed PET Printer](https://www.youtube.com/watch?v=EbxNa3WLpjA)
- [Youtube channel : large scale PET grinder & shredder](https://www.youtube.com/watch?v=3xWvHz9JjjU)

## Designs

![](./media/research/s-l1600.jpg) ([Source](https://www.ebay.de/itm/Getecha-GRS-182-A-1-38-Granulator-Beistellmuhle-Schneidmuhle-40-106/324191772811?hash=item4b7b55e08b:g:~e8AAOSwFxpe3iCh&autorefresh=true))

### Components

- [Belt Tensioner - Maedler](https://www.maedler.de/product/1643/1617/keilriemenspanner-mit-montierten-keilriemenscheiben)


### Renderings

Variant : Large

![](./renderings/idefix.jpg)

Section View 

![](./renderings/idefix_section.jpg)


### Media

[Running - Video](./media/granulating.mp4)

[Hopper - Video](./media/hopper.mp4)
