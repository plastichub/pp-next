### Preparing the Blade Axle  

 The axle for the blades is as per the Precious Plastic instructional video; a hexagonal axle that is lathe down to be round at either end. The axle is lathe down at either end to be the same diameter as the inner diameter of the ball bearing. It is important to ensure there is no gap between the axle and the ball bearing when they are fitted together. 

The very end of the now rounded axle (last 5mm) then has to be lathe down further to the diameter of the crank bolt thread. The cut off square taper is already threaded all the way through the centre and so the axle (once lathe down to the thread thickness) can sit within this to centre the square taper on the axle ready for welding.  
