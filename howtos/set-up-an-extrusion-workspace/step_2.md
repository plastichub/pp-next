### Is this for you? 

 For the Extrusion Workspace you will have to be quite technical as you have to understand how the Extrusion machine works, ideally know how to maintain it and, as a plus, know how to fix it when it needs a bit of extra love. 

Attention to details is also a nice, and some creativity to come up with new patterns and techniques.
