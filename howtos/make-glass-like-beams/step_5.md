### Get ready for extruding 

 Alright, let’s get everything ready to melt! 
First, turn on the extruder and set the temperature to 230°C. 
And then there are a few tricks to prepare your mould for a smoother process and outcome. We recommend to coat the inside of the mould with silicone oil to help getting the beam out of the mould after it cooled down. And in order to achieve a smooth material surface it helps to heat up your mould with a heat gun or warm it up in an oven for around __ minutes before attaching it to the preheated extruder.