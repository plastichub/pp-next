### Take the right precautions first. 

 When talking about safety we are only referring to precautions about working specifically with plastic, as it’s our thing. We assume that working with the table saw needs certain level of expertise so please take all the precautions related with how the machine works.

When cutting with the table saw there is danger of overheating and melting the plastic. If this happens some bad fumes could be released. So in order to work safer make sure to use a gas mask with ABEK filters to prevent inhaling possible toxic fumes. Special attention on plastics like PS and PVC.
