### Sieve and Collection Box 

 The sieve and collection box is nothing really special, just bended and welded metal sheets. We recommend using a sieve with holes about 5 - 8 mm in diameter. Very small holes will turn the plastic nearly into dust. Don't go too thin with the thickness of the metal sheet. It is important for stability and the locking system! 1,5 mm are a good size.