### Next steps 

 Time to proceed! Summarise what you learned about the group, and discuss how you can start collaborating. Note what’s existing and what is still needed (money, space, people...), and what questions need to be answered.

Decide who will take care of which topic, so everyone knows what they can do. If you have a bigger group, you might have to make working groups, which can communicate within their topic.
