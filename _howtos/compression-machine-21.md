---
image: "/howtos/compression-machine-21/compression-technical-3.jpg"
title: "Compression machine 2.1"
tagline: ""
description: "This upgrade will make your compression machine way more efficient and streamlined. Off you go!"
keywords: "hack,compression"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
- "compression"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  One of the main challenges with the V2 of our compression machine: It&#39;s a rather slow process if compared with the injection or extrusion. This is particularly true because the plastic is first heated, then compressed and cooled in the oven- lots of waiting time. Good for experimenting but not feasible if you want to run a production.<br />
  <br />
  With this upgrade the compression area (carjack) is shifted underneath the machine, this way the machine can run continuously. With this change you can now compress and cool one mould while you&#39;re heating up a second one. Besides being more productive and efficient this upgrade makes the machine easier to build as you don’t need to install the compression mechanism inside the oven. Take note of the drawings.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/compression-machine-21/compression-technical.jpg">
        <img class="step-image" src="/howtos/compression-machine-21/compression-technical.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cut and weld metal</h4>
<div class="step-text">
  Use the schematics to measure and cut your metal. Your oven may be a different size, so make sure you adjust the measurements where necessary. <br />
  Weld all your parts together and paint.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/compression-machine-21/compression-upgrade2.jpg">
        <img class="step-image" src="/howtos/compression-machine-21/compression-upgrade2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Add the oven</h4>
<div class="step-text">
  Take your desired oven and fasten it to the machine. If it already works then you could leave it as is, or you can install a PID controller which might give you slightly more control. <br />
  Tip: It’s never a bad idea to add more insulation which will make your machine more energy efficient.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/compression-machine-21/compression-technical-2.jpg">
        <img class="step-image" src="/howtos/compression-machine-21/compression-technical-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Melt your plastic!</h4>
<div class="step-text">
  You can now use your oven for plastic recycling!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/compression-machine-21/creations-containers-top.jpg">
        <img class="step-image" src="/howtos/compression-machine-21/creations-containers-top.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>