---
image: "/howtos/set-up-devsite-to-help-coding/0.jpg"
title: "Set up dev-site to help coding!"
tagline: ""
description: "Hello Coder! This is what we want to make https://build.onearmy.world/ The code of our platform can be found open-source on Github. There is still a lot of work to do and things to improve, we need a community of developers to help out improve it and fix issues. In this how-to i’ll show you how to download it and run it on your local computer so you can play around with the code, add modifications and share back to us."
keywords: ""
tags: []
---
<h4 id="summary">{{page.title}}</h4>
{{ page.description }}
<h4 class="step-title" id="step-1">Step 1 - Get the code</h4>
<div class="step-text">
  First step go to Github, and download or clone our code. I’d recommend to install the Github app to add pull request in a later stage.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-devsite-to-help-coding/1.jpg">
        <img class="step-image" src="/howtos/set-up-devsite-to-help-coding/1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Install install node js</h4>
<div class="step-text">
  Pretty straight forward, download the files on their website and install. https://nodejs.org/en/#download
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-devsite-to-help-coding/2.jpg">
        <img class="step-image" src="/howtos/set-up-devsite-to-help-coding/2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Install Yarn in Terminal</h4>
<div class="step-text">
  Open up the terminal and install Yarn in the root directory. <br />
  1: Running the command “sudo npm i -g yarn”.<br />
  2: Run the command “yarn” to install. <br />
  To make sure its installed in the root type “cd” in terminal, drag the root folder on the terminal press enter. and then run “yarn”
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-devsite-to-help-coding/3.1.jpg">
        <img class="step-image" src="/howtos/set-up-devsite-to-help-coding/3.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-devsite-to-help-coding/3.2.jpg">
        <img class="step-image" src="/howtos/set-up-devsite-to-help-coding/3.2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-devsite-to-help-coding/3.4.jpg">
        <img class="step-image" src="/howtos/set-up-devsite-to-help-coding/3.4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Step Deploy local network</h4>
<div class="step-text">
  In Terminal run command “yarn start” to run. Like above make sure it runs in the root folder
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-devsite-to-help-coding/5.jpg">
        <img class="step-image" src="/howtos/set-up-devsite-to-help-coding/5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Have fun 🤙</h4>
<div class="step-text">
  As you can see we currently in V0.4. Lot&#39;s of work to do and things to fix! Start coding and add your pull requests to Github, on Tuesday we review code :) <br />
  *
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-devsite-to-help-coding/4.jpg">
        <img class="step-image" src="/howtos/set-up-devsite-to-help-coding/4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
{% if page.related %}
<h3 id="related"> Related </h3>
{% endif %}
---
{% if page.tags %}
{% endif %}
{% if page.resources %}
{% endif %}