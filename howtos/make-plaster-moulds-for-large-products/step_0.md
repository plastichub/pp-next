### Consider pros and cons 

 Before you start, it is important to note that there are some drawbacks to using this process. Plaster moulds are not long lasting - so this may not make sense as a common way to process plastic. 

However, it is a great way to inject large, solid products and can be used as a prototyping technique. For example - if you want to test the shape of a mould before it is milled into a block of aluminium. 

You’ll need:
-Extruder machine
-Shredded plastic
-Casting plaster
-Mould release
-A model or object to replicate
-Melamine or plywood
-Heat gun
-Paint, chopped fibreglass, shellac (optional)
