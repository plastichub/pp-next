### Make a mould to extrude beams 


Beams make great use of the extrusion machine as they can be strong and enable you to create very unique patterns and colours. This guide will show you how to make a mould which can be easily translated to any size of beams. 

(Update: We also added a lasercut version, see step 7!)