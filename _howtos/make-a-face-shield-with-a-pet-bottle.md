---
image: "/howtos/make-a-face-shield-with-a-pet-bottle/Face maske Image cropped.jpg"
title: "Make a Face Shield with a PET bottle"
tagline: ""
description: "There is a global shortage of protective equipment, not even the people are currently keeping our society running. But everyone with a 3D-Printer can help and manufacture face shields for the people who are working in the front line. With this Design you can make Faceshields with a and recycle a PET bottle.<br /><br />We made a Video about the assembly to:<br /><br />&lt;a href=&quot;https://www.youtube.com/watch?v=6u6y6gD17rk&amp;feature=youtu.be&quot;&gt;https://www.youtube.com/watch?v=6u6y6gD17rk&amp;feature=youtu.be&lt;/a&gt;"
keywords: "product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/qi-tech">qi-tech</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Print Parts</h4>
<div class="step-text">
  3D-Print the Top and bottom part of the shield. Download the Files for free from Thiniverse or Prusa or use the files in the zipped folder you can download here.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step1-Print Parts.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step1-Print Parts.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 0- How to make a Face Shield.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 0- How to make a Face Shield.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step1-Printed parts design 1.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step1-Printed parts design 1.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Get elastic Band</h4>
<div class="step-text">
  You can use normal white elastic band like in our Video, but elastic band with button holes works best.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step - get elastic Band.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step - get elastic Band.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare bottle</h4>
<div class="step-text">
  First of all, you need to wash out the bottle and then cut off the top and bottom parts. Finally cut along the side of the bottle so you can bend the Plastic open.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 3-prapare bottle.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 3-prapare bottle.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 3 pepare bottle bottom part.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 3 pepare bottle bottom part.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 3-Prapare bottle 3 cut allong the middle.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 3-Prapare bottle 3 cut allong the middle.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Connect 3D-Printed parts</h4>
<div class="step-text">
  Start off with the bottom part. Fold open the PET bottle and press it into the the slit. The use the soldering iron to melt the PET and the printed parts in 3 Points that are visible from the inside.<br />
  <br />
  The Next step is to attach the top part. First, use a edding to mark where to make holes in the bottle. Then use the soldering iron to melt these holes. After attaching the top part you can use the soldering iron the melt the plastic together for additional stregth.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 4- Connect bottom part.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 4- Connect bottom part.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 4- Connect top part.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 4- Connect top part.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 4 - Hot fuse.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 4 - Hot fuse.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Round off corners</h4>
<div class="step-text">
  Use nail scissors to round off corners at the bottom of the PET shield. If you don&#39;t have nail scissors you can also use normal scissors.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 5 Use Nail sissors.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 5 Use Nail sissors.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 5 cutting.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 5 cutting.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Attach elastic band</h4>
<div class="step-text">
  First, cut about 16-20cm of elastic band. Tie the elastic band around the two holders on both sides and make sure they are secure.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 6 attach Elastic Band.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 6 attach Elastic Band.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/step 6 - attached band.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/step 6 - attached band.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Disinfect</h4>
<div class="step-text">
  Use Disinfectant and wipe over the inner side of the shield to kill off any bacteria. PRUSA has made and is constantly updating a Sterilisation Guide. Be sure to check it out for more information.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-face-shield-with-a-pet-bottle/Step 7 desinfecting.PNG">
        <img class="step-image" src="/howtos/make-a-face-shield-with-a-pet-bottle/Step 7 desinfecting.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>