### Melt the HDPE 

 We use a flat plate sandwich toaster to melt HDPE. We will occasionally use a small toaster oven as well, but this is mostly for larger melts where we need to keep a bigger mass of plastic hot. For this project we were aiming for around 8mm in thickness for the blade so the sandwich toaster can cope fine on its own.

When melting, the plastic will shrink a lot so keep adding more every few minutes, taking care not to introduce any air bubbles.