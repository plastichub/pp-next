---
image: "/howtos/make-bike-pedals--grips/pedal.jpg"
title: "Make Bike Pedals & Grips"
tagline: ""
description: "In this How-To I describe how I made the mold. <br />Injecting the pedal is fast but making the mold takes time. I suggest you to mill the mold if you have the possibility because I welded the mold and this is time consuming. You can download the 3D model and make your own version."
keywords: "HDPE,mould,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "HDPE"
- "mould"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/adrienr">adrienr</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Tools</h4>
<div class="step-text">
  - Injection machine<br />
  - angle grinder (cutting and grinding discs)<br />
  - vise<br />
  - flat file, round file (opional: small grinder rotary tool like Proxxon or Dremel)<br />
  - drill press<br />
  - clamps (at leaste 3)<br />
  - welding machine (only for pedals) &amp; welding clothes<br />
  - radius scriber (a marker works well too)<br />
  - protection glasses<br />
  - respirator with A1 P1 filter (hot plastic fumes and particles filter)<br />
  - safety gloves<br />
  - ear protection<br />
  - sanding paper (grit size around 80) (optional: electric sander)<br />
  - measuring tape<br />
  - square edge<br />
  - M6, M8 thread tap<br />
  - drill bit 4.3; 6.4; 6.8; 8.4; 11; 20 ; 22.5 mm (if you don&#39;t have the 22.5, use your round file like I did)<br />
  - tweezers (optional, for ball bearings assembly)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/injector.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/injector.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/anglegrinder.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/anglegrinder.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/instruments.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/instruments.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Material for the grips</h4>
<div class="step-text">
  Plastic waste: I made experiments with polystyrene (PS) and polyethylene (PE). PS looks more strong but I had only black one so the presented objects are made of PE. The pedal volume is 145 cm^3 and the grip 84 cm^3.<br />
  <br />
  For the grips:<br />
  <br />
  - metal tube (internal diameter = diameter you want for the grips, big ones are more comfortable. I chose 35mm. Lenght = lenght you want for the two grips - 3 mm. I have 2 x 130 mm so my grips are 127 leght)<br />
  - metal sheet (steel 4 mm thick; 180 x 60 mm)<br />
  - threaded rod (M6 x 450; M8 x 150 mm)<br />
  - 6 nuts M6<br />
  - metal cylinder (diameter 22.3 length 111 mm) (standard handlebar have diameter 22 mm so take just a little bit more)<br />
  - 1 bolt M8 x 20<br />
  - 1 washer M20<br />
  - 1 nut M8
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Material for the pedals</h4>
<div class="step-text">
  - metal sheet (steel 4mm thick; 120 x 240; 26 x 700 mm)<br />
  - welding rod (2 or 3 mm)<br />
  - 3 bolts M6 x 10 (any size would works but you need the corresponding drill bits)<br />
  - 2 pedal axles with ball bearings, washers and nuts<br />
  - wood (for positioning during welding, optional) (pedal size)<br />
  - 4 bolts and nuts M4 x 40 (only length matter)<br />
  - 24 setscrews M6 x 8
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/marquage2.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/marquage2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Grip mold: cut the parts</h4>
<div class="step-text">
  Cut the tube at the length you want for your grips (mine are 130 mm).<br />
  Cut four hexagones with side length 33 mm.<br />
  In each of them, drill three holes in the corners.<br />
  In the first one, drill another hexagone with 10 mm side length in the center.<br />
  In the second one, drill a 5 mm hole in the center.<br />
  In the third one, drill a 22.5 mm hole in the center.<br />
  In the fourth one, drill a 8 mm hole in the center.<br />
  Cut three M5 and one M8 threaded rods at 150 mm.<br />
  Cut the cylinder at the length you want (111 mm for me).<br />
  Make a M8 x 20 threaded hole at the center of one cylinder side.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/plaques.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/plaques.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/marquage.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/marquage.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Pedal mold: cut the parts</h4>
<div class="step-text">
  Cut two squares 120 x 120 mm in the metal sheet.<br />
  Make holes in the four corners (4.3 mm in my case).<br />
  Scribe the pedal shape on one square face.<br />
  Drill the injection hole where you want it(I chose diameter 5, in the center).<br />
  Cut a band of 26 mm height and at least 300 length and another of 26 (precisely) x 340 (at least).<br />
  Cut the 300 band in 15.6 (2x); 24 (2x); 18 (2x); 33 (2x); 25 (2x); 43.4 (1x).<br />
  Cut the 340 band in 40 (2x); 56 (4x); 27.7 (1x).<br />
  Cut also six small pieces in order to wedge the mold part together (5 x 10 mm).<br />
  Cut three more to make the triangle and trapeze center (25 x 7 (2x); 4 x 35 (1x)).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/marquage2.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/marquage2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/meulage.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/meulage.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/plaquettes.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/plaquettes.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Make the Pedal Mold: Weld It</h4>
<div class="step-text">
  Before welding you need to add an angle (around 2 degrees) on each vertical face of the wooden parts (triangle, trapezes and pedal). This will ease the extracting process. I accomplished this with the sanding machine. <br />
  <br />
  Weld the pedal perimeter: Clamp the wooden pedal on your welding surface and clamp the edges you want to weld (cf. picture). Continue with the other parts always ensuring that your parts are well against the ground. It was my first arc welding job and you can notice the poor result but it doesn&#39;t matter, it just extend the grinding time.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/serrejoint.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/serrejoint.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/assemblage.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/assemblage.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Inject the Grips</h4>
<div class="step-text">
  Turn the heater on.<br />
  Assemble the cylinder with the basis hexagon (Nbr. 4).<br />
  Add the three threaded rod.<br />
  Add the washer around the cylinder.<br />
  Add the tube.<br />
  Add the top hexagon with the injecting hole (Nbr. 2).<br />
  Add the top hexagon with the hexagonal hole (Nbr. 1).<br />
  Secure it with the nuts.<br />
  Start filling the machine with plastic.<br />
  Wait 5 min.<br />
  inject.<br />
  <br />
  Remove the grip using the M8 threaded rod and nut.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/moulepoignee.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/moulepoignee.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/moulepoignee2.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/moulepoignee2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/moulepoignee8.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/moulepoignee8.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Inject the Pedals</h4>
<div class="step-text">
  Heat the machine.<br />
  Fill with old plastic.<br />
  Inject.<br />
  Open the mold.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/pedalinjection2.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/pedalinjection2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/pedalinjection4.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/pedalinjection4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/pedalinjection5.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/pedalinjection5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Finish and Assembly</h4>
<div class="step-text">
  Secure the pedal in the jig.<br />
  Mount the 11 mm drill bit and align it with the pedal center.<br />
  Secure the jig to the drill.<br />
  Drill through all the pedal.<br />
  Mount the 20mm drill bit.<br />
  Drill the bike side of the pedal 8 mm depth.<br />
  Flip the pedal.<br />
  Drill the outer part of the pedal 15 mm depth (measured from the flat face, not the top triangle).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/pedalfinition.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/pedalfinition.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/pedalfinition3.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/pedalfinition3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-bike-pedals--grips/pedalfinition4.jpg">
        <img class="step-image" src="/howtos/make-bike-pedals--grips/pedalfinition4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>