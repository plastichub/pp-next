### Cut and drill holes 

 For this model we have used:

- 35.5cm x 3
- 19,5cm x 2
- 14cm x 2
- 18cm x 2

Feel free to make your modifications if you need it bigger or smaller.