Small plastic grinder , perfect for labs, small enterprises

<div>
    <a href="${product_overview}">
        <p style="text-align: center">
            <img width="100%" src="${product_overview}"/>
        </p></a>
</div>

!{specs}

<hr/>

<div>    
    <div style="padding: 16px; display: inline-block">
        ${product_resources}
    </div>
</div>
