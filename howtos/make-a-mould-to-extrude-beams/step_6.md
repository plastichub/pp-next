### Lasercut version! 

 If you have access to a lasercut service, there is also the possibility of doing this beam mould much faster! 

We added a .zip file with drawings you can use or send to a lasercut company to cut your parts. 
These plates are going to replace the use of brackets for making the beam mould. 

After you receive the plates you'll need to place them on the beam by gently tapping them  with a rubber hummer, making sure it's perpendicular and flush with the beam. To check this you can put the nozzle plate in front of it to make sure it's not in an angle. 

After this you can proceed to weld everything in place.