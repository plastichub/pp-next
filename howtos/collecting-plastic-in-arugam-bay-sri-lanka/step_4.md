### Focus on tourists 

 We decided to focus on collecting plastic from the tourism related industry as they have such a vast amount of PET bottles which happen to be in high demand by the recycling industry in Sri Lanka to be turned into recycled polyester yarn. Also, the handling is relatively easy as the bottles are clean and the bottle caps are great for the Precious Plastic machines. Also it solves the problem of PET bottles clogging up waste management that has to go to landfill. 
