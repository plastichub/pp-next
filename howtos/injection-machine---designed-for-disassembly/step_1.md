### Designed for disassembly  

 The machine has been designed to be disassembled for easy packing and transportation. It now fits in a 1000x200x200 mm box when disassembled.

The electronics box it's the same, but we added some powercon connectors for the heating elements and kettle plug for the power supply, so that it can also be unplugged and dismounted for transportation.