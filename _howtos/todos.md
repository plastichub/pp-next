- [ ] Impl. OSR 2.0
    - [ ] Steps
        - [ ] Alternatives
        - [ ] Groups
        - [ ] Order
        - [ ] Optionals, eg: heat treat, coating
        - [ ] Template extension points for ´step´
            - [ ] Before
                - [ ] Intro
                - [ ] Meta: duration, diff, ...
                - [ ] Requirements
                - [ ] Resources
             - [ ] After
                - [ ] Extro
                - [ ] Resources
                    - [ ] gauges setup files
                - [ ] Tags / See Also
    - [ ] Resource includes by resource tags
    - [ ] Link components/assemblies
    - [ ] Skills
    - [ ] Used tools, machines    
- [ ] Linking/Silbing
    - parent / child howto
    - related howto
    - alternative
    - similar
- [ ] Emitted sub references
    - Books (skills, requirements, specs, tolerances)
- [ ] Fix print media
- [x] @osr-cad: create 3dxml for each part
- [x] @osr-cad: create jpg for each part
- [ ] @osr-cad: auto-drawing for each part and subassembly
- [ ] @osr-cad: 3d viewer with links root assembly


### Deployment

- [x] Github
- [] Discourse
- [-] OneArmy
- [x] PDF (via Puppeteer renderer)
- [ ] HTML Index/Preview snippet - Mailchimp
- [ ] HTML Index/Preview snippet - Marketplace
- [ ] HTML Index/Preview snippet - Social networks

### Missing Artefacts

- [ ] Tool/Machine library
- [ ] Manufacturing/design book lookup (@osr-index)
- [ ] Library of international units
- [ ] Library of international stock sizes
- [ ] Auto lookup: nearby services



