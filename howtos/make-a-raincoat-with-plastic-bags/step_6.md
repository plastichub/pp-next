### Make your fused sheet 

 Once you are comfortable with your tests, go ahead and fuse together your big sheets, remembering to use teflon fabric below, and your baking parchment paper as an ironing surface above. You will need to make at least four big sheets, around 70cm x 170cm. 

💡 Fix holes: Once complete, look over your fused plastic sheets and find any holes or parts which are not fully melted or secure. You can fix these now with your iron/press and any scraps of plastic that you have left, but be very careful not to deconstruct the material when heating the sheets again. 