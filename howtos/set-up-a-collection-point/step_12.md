### Assemble the team 

 Alright, so now that you have a clearer idea of the subject and your vision, you'll need some more people to put this into reality.

People are what can make a project succeed or fail, so make sure that you have a small team of people who you feel comfortable to work with and who are motivated to do this with you!