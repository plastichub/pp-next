### Find your plastic supply 

 You can use the Precious Plastic Map and the Bazar to find Shredder workspaces around you that can provide you with the raw material: shredded plastic waste. 

If you have a local Community Point, they might be able to give you some advices for this as well.

👉 community.preciousplastic.com/map
👉 bazar.preciousplastic.com
