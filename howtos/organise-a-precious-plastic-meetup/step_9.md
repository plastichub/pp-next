### Introduction  

 Start with a starter round for the group to give everyone the chance to introduce themselves, so everyone has an idea what the background of the group is.

It helps to prepare some questions: Ask for their name, where they live, what they do (occupation), and how much they've been involved in Precious Plastic already. 

You can also add a fun question like: What’s your favorite icecream flavor  or what animal would you like to be?