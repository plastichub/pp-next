### Preparations 

 Due to the very thin walled diaphragm injection gate, preheating the lightswitch frame’s mould makes the process of injecting easier. The amount of plastic needed for all parts is about half a barrel, but rather work with a full barrel to ensure enough pressure.

As the mould is quite intricate and detailed, a plastic with good flow characteristics is required. 

e injected PP on 270/280°C (barrel/nozzle), but a few tests might be necessary to calibrate your machine.