### Buy shredded plastic 

 Now the only thing missing is your raw material. 

Try to get your shredded plastic as local as possible. In best case you can buy it from a Shredder Workspace in your local network.

Make sure to specify your preferred shreds size (small, medium or large) and to have a variety of colours and types, so you can be more creative.