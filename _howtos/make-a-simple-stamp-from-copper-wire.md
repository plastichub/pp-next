---
image: "/howtos/make-a-simple-stamp-from-copper-wire/COVER.jpg.png"
title: "Make a simple stamp from copper wire"
tagline: ""
description: "It is very important to mark your products with their specific plastic type, so they can be recycled better. There are several ways to do this. Here we show a very simple technique to make your own stamps with a copper wire.<br /><br />(Alternatively you can also buy plastic type stamps on the Precious Plastic bazar)"
keywords: "melting,sorting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
- "sorting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  Let&#39;s start with getting our materials and tools ready.<br />
  To make the stamps, you will need:<br />
  - Wire/cables<br />
  - Cutter (Workshop knife)<br />
  - Pliers to bend and cut<br />
  - File to soften corners<br />
  - Soldering Iron
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP1.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP1.jpg.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Stay safe</h4>
<div class="step-text">
  When you use your new stamp make sure to wear a mask as toxic fumes can arouse. In general when handling the iron be careful to not burn yourself and a workshop knife also can be quite dangerous. So stay safe.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/image1.jpg">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/image1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Get your wire</h4>
<div class="step-text">
  Copper offers very good heat transportation and works the best for the stamps. We can get this from some cables, which are made with hard copper wire. This means there is one massive string of copper and not many small ones. Check your offcut area or visit your local scrap yard, if you can’t find them, you can also buy normal copper rods. Strip your cable carefully from the plastic with the knife and you are ready to move on.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP2.1.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP2.1.jpg.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP2.2.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP2.2.jpg.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Bend your wire</h4>
<div class="step-text">
  Now bend the copper in your desired shapes. Use your pliers to support the bending and squeezing. Make sure to include 2 cm of wire orthogonally from your later or symbol to fix later in your iron.<br />
  If you feel adventurous you can also carefully solder copper pieces together, but we didn’t experiment with that, as we heat up the copper later, which can weaken the bond.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP3.1.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP3.1.jpg.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP3.3.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP3.3.jpg.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP3.2.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP3.2.jpg.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Make a library of shapes and letters</h4>
<div class="step-text">
  We found it makes sense to create everything you could need in advance so you are well prepared for marking your plastic.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP4.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP4.jpg.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Get your iron ready</h4>
<div class="step-text">
  We are using a very basic 65W soldering iron and didn’t need to change much. Your iron needs a bolt on the side to keep the copper wire in place. We had already a hole in it to fix the tip, which we just opened a little more and threaded it to M4 for a better grip. Get a short M4 bolt and you are good to go.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP5.1.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP5.1.jpg.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP5.2.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP5.2.jpg.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP5.3.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP5.3.jpg.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Stamping</h4>
<div class="step-text">
  Now you are ready to stamp. The copper shows through a slight changing of color very good when it is hot enough to stamp. This shouldn’t take more than a few seconds. Carefully press the copper on the plastic and see how it melts your work into it. Don’t apply too much pressure as the copper gets very soft when warm and can bend. Different materials need different handling so always when using something new have a piece for testing on hand.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/STEP6.jpg.png">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/STEP6.jpg.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Happy stamping!</h4>
<div class="step-text">
  Easy and fast way to mark your plastic, as you should NEVER give out plastic without labeling. Let us know how it went for you and if you discovered something new.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-simple-stamp-from-copper-wire/DSC00290.jpg">
        <img class="step-image" src="/howtos/make-a-simple-stamp-from-copper-wire/DSC00290.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>