### Keep connecting 

 Eventually, your community will collect and transform plastic locally. Nice!

Now it’s all about keeping people involved and motivated. Be available for questions, welcome newcomers and connect people to the relevant contacts and places.