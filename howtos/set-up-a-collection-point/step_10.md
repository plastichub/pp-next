### Understand your local waste management 

 It’s important to understand the waste management of your local area. Are there local industrial recycling systems? Do they sort their waste and is plastic sorted separately? Or is it the informal sector taking the load.

Find out why local people use plastic and what happens with their plastic after throwing it away so you can explain it to people. 