### Finalise your measurements 

 Come back to the template that we used in step 2. Apply your measurements to the following template, and decide on a size for measurement (F), the diameters of the hood. You can decide on this by drawing and cutting the hood construction pieces first with paper, and finding a desired size. 