### See document: Updates V4-FF 

 In here we described:
- The updates we did
- We explain the problems we encountered during building
- Also we shared the hours we put into the build with an  estimation of the cost price if you will make the machine commercial
- We propose further future improvements which can be done

Displayed picture: The biggest hickup we encountered during building was that we underestimated how important it was to ream the holes of the aluminium blocks to the exact measurements. Our solution was to ream the holes to 11.95mm, followed by heating the blocks to 250 degrees and pressure fit the heating element in to the holes.