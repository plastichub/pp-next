### Get ready! 

 In the download package you find a lot of files and drawings. At first sight it might be overwhelming, but don’t be afraid, we’re taking care of you ;-).
In addition to the download-kit, the following steps will give you further information, why the shredder is built the way it is. Furthermore a short introduction video and an assembling video is in the making to get a better understanding about the construction. Stay tuned!

Download-Link: https://www.dropbox.com/sh/xlts122wcb905q6/AABRgMZTki8gH1NqQ5SvOS-Ia?dl=0