### Create How-to's! 

 Share with the worldwide community how you run your Sheetpress Workspace so other people can learn from you and start using your solution to tackle the plastic problem. 

Make sure you only create How-to's for your best processes and techniques, no tryouts or oneoffs. This can also help you create a good name for yourself in the Precious Plastic community. 

👉.  http://tiny.cc/create-howto