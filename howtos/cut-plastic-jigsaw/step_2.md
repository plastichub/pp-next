### Choose the right blade 

 When selecting the blade several factors have to be taken in account.

Type: We prefer using High-Carbon Steel blades (HCS) and High-Speed Steel blades (HSS) for these jobs.

Teeth per inch (TPI): For brittle materials like PS will need a relatively big amount of TPI, around 10-13, in order to allow a safe and clean cut. A smaller amount will make the work faster but rougher.
For tough plastics like HDPE and PP we don’t need that many TPI, around 6 is good. An excessive amount of teeth will make it unnecessarily harder.

Shape/Blade direction: The blade can point upward or downwards. It produces a smoother cut on the side of the piece that it’s cutting towards. And sometimes in brittle materials can produce chipping on the other side.
We recommend to use blades like the ones in the image, where the cutting angle of the teeth is negative or close to zero. 