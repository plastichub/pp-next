<?php

namespace SR;

require __DIR__ . './../vendor/autoload.php';

class Signal
{
    public $version;
    public $startTime;
    public $endTime;
    public $sn;
    public $payload;
    public $type;

    public function __construct(
        $version,
        $startTime,
        $endTime,
        $sn,
        $payload,
        $type) {
        $this->version = $version;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->sn = $sn;
        $this->payload = $payload;
        $this->type = $type;
    }
    public static function foo()
    {
        return 'as';
    }
}
