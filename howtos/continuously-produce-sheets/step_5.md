### Press the sheet 

 Open the sheetpress by releasing the jack. Most jacks release valve which opens using the pumping rod. 

Using the sliding tool, transfer the mould into the sheetpress. It’s important to slide it straight otherwise it will catch on the side frame.

Now close the sheetpress (here we used pneumatic jacks with an air compressor to make it easy and fast).
Watch the spring to tell when the press is closed, if it's fully compressed, stop pressing. 

You should check the spring every 10 minutes, pressing when needed. Make sure the mould stays in the middle. Check your timetable for when the sheet has to be transferred. 