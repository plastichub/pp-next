# PP 3D Printers

Root repository for PP related 3D printers

## Machines

### Noah

Large scale printer, using an extendable scaffolding framework and the PP v3 extruder

[Details](./machines/Noah.md)

**Status** : parts ordered & development started
Estimated price : 3000 - 4000 Euro
Print volume : unlimited

![Noah](https://gitlab.com/plastichub/noah/blob/master/machines/noah/documents/frame/xy%20structure%20concept%20with%20carriage.jpg)


### MiniPET Printer

Regular off-the-shelf printer (AnyCubic-i3Mega or similar), modified to print PET dust.

<img width="200px" src="./assets/anycubicmega.jpg"/>

Estimated price, without v3 PP shredder : **1000 Euro**, includes :

- 3D Printer (185 Euro)
- PET print head (150 Euro)
- [PET grinder](https://precious-plastic.org/home/library/components/grinder/) (400 Euro)

[Details](./machines/PETPrinterS.md)

**Status** : parts ordered & development started

<hr/>

## References

- [Discord Chat - Development](https://discord.gg/j7K7n9e)

## Members

- PlasticHub : Investor, Development, Administration
- Timberstar : Development

## Tools

We use [Visual Studio Code](https://code.visualstudio.com/) to edit the documentation. There is also a *markdown preview* plugin. *Markdown* is
the format for the files which you can preview on Github as well. As alternative, you can use the [stackedit.io editor](https://stackedit.io/).
Soon there will be a more specialized tool to manage & document machine development.

- [Markdown cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [Markdown table generator](https://www.tablesgenerator.com/markdown_tables)
- [GIT Explorer Extension](https://tortoisegit.org/)
- [Short Video about Markdown and Git](https://youtu.be/BqTdc8jmdIE)
