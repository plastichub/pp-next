#ifndef VERSION_H 

    #define VERSION_H 

    #define VERSION "1.1|PlasticHub|PreciousPlastic|Obelix2021|f2f9cc9a8ee17dc27bacf2db3cdd881cc76d31d0"

    #define SUPPORT "support@plastic-hub.com | www.plastic-hub.com"
    #define BUILD "OBXMedium-RevA"
    #define FW_SRC "https://gitlab.com/plastichub/pp-next/tree/master/machines/shredder/obelix2021/firmware/obelix"
    #define CID "449"

#endif