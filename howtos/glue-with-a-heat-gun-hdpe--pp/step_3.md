### Apply the hot melt 

 Put the stick into the glue gun and wait until the machine is hot enough to melt it. Make sure you have all your  pieces handy. Apply the hot melt on the first piece, then directly place the other piece on top and apply pressure (clamping preferred). 

You want to glue the pieces together while the glue is still hot (< 25s). So make sure you have everything ready because you have very little time for error correction. Once you put them together, in a few seconds both pieces will get glued.
