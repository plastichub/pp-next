### Bigger Press = Bigger Sheets 

 This system has been upscaled to a 1.5x1.5m pressing surface. This allows you to easily make plastic sheets at the standard 1.2x1.2m size and even a bit bigger.

This is a 56% increase in capacity so you'll be able to recycle plastic at more than 50% the rate as the V4 press.


Note: The press requires 1500x3000 sheets of steel and aluminium to build. It's also quite a bit heavier, so it's no longer possible to handle the parts in this machine without the assistance of a hoist or similar lifting device.