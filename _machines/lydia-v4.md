---
image: /machines/extrusion/30mm-compact//renderings/perspective.JPG
category: "extrusion"
title: "Extrusion v4 - Lydia"
product_rel: "/machines/extrusion/30mm-compact/"
tagline: ""
description: "Precious Plastic - Machine : Extrusion :: Extrusion v4 - Lydia"
product_id: lydia-v4
tags:
 - lydia-v4
 - v4
 - extrusion
header2:
  image: /products/lydia-v4/media/preview.jpg
  overlay_image: /products/lydia-v4/media/preview.jpg
  overlay_filter: 0.2 # same as adding an opacity of 0.5 to a black background
  caption: ""
alternatives: 
  - extrusion-pro
wiring: true
has_spec: true
preview: /machines/extrusion/30mm-compact//media/teaser.png
buy: "mailto:sales@plastic-hub.com?subject=Inquiry%20-%20lydia-v4"
overview_drawing: true
teaser: "Powerful extruder for semi profesionall production. Comes with lots of extra safety electronics."
products: true
features: true
products_row:
  - image_path: /assets/site/product_brick.jpg
    alt: "Injection Press"
    title: "Injection Press"
    excerpt: "Powerful and precise injection"
    url: "https://community.preciousplastic.com/how-to/make-extruded-plastic-bricks"
    btn_class: "btn--primary horizontal-margin"
    btn_label: "Learn more"
    buy: "test"
    price: "From 2000 Euro"
  - image_path: /assets/site/product_brick_round.jpg
    alt: "Injection Press"
    title: "Injection Press"
    excerpt: "Powerful and precise injection"
    url: "https://community.preciousplastic.com/how-to/make-an-interlocking-brick"
    btn_class: "btn--primary horizontal-margin"
    btn_label: "Learn more"
    buy: "test"
    price: "From 2000 Euro"
  - image_path: /assets/site/product_beam.jpg
    alt: "Injection Press"
    title: "Injection Press"
    excerpt: "Powerful and precise injection"
    url: "https://community.preciousplastic.com/how-to/make-glass-like-beams"
    btn_class: "btn--primary horizontal-margin"
    btn_label: "Learn more"
    buy: "test"
    price: "From 2000 Euro"
  - image_path: /assets/site/product_bench.jpg
    alt: "Injection Press"
    title: "Injection Press"
    excerpt: "Powerful and precise injection"
    url: "https://community.preciousplastic.com/how-to/make-a-bench-with-beams"
    btn_class: "btn--primary horizontal-margin"
    btn_label: "Learn more"
    buy: "test"
    price: "From 2000 Euro"
gallery:
 - url: "/machines/extrusion/30mm-compact/media/latest.JPG"
   image_path: "/machines/extrusion/30mm-compact/media/latest.JPG"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/media/pers2.jpg"
   image_path: "/machines/extrusion/30mm-compact/media/pers2.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/media/preview.jpg"
   image_path: "/machines/extrusion/30mm-compact/media/preview.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/media/screw-assembly.jpg"
   image_path: "/machines/extrusion/30mm-compact/media/screw-assembly.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/media/screw-parts.jpg"
   image_path: "/machines/extrusion/30mm-compact/media/screw-parts.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/media/screw.jpg"
   image_path: "/machines/extrusion/30mm-compact/media/screw.jpg"
   alt: ""
   title: ""
gallery_drawings:
 - url: "/machines/extrusion/30mm-compact/drawings/dimensions.PDF"
   image_path: "/machines/extrusion/30mm-compact/drawings/dimensions.JPG"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/drawings/exploded.PDF"
   image_path: "/machines/extrusion/30mm-compact/drawings/exploded.JPG"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/drawings/Lydia_V4_Bearing_housing-02.PDF"
   image_path: "/machines/extrusion/30mm-compact/drawings/Lydia_V4_Bearing_housing-02.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/drawings/Lydia_V4_Bearing_housing.PDF"
   image_path: "/machines/extrusion/30mm-compact/drawings/Lydia_V4_Bearing_housing.jpg"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/drawings/overview.PDF"
   image_path: "/machines/extrusion/30mm-compact/drawings/overview.JPG"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/drawings/parts.PDF"
   image_path: "/machines/extrusion/30mm-compact/drawings/parts.JPG"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/drawings/spec.PDF"
   image_path: "/machines/extrusion/30mm-compact/drawings/spec.JPG"
   alt: ""
   title: ""
 - url: "/machines/extrusion/30mm-compact/drawings/todos.PDF"
   image_path: "/machines/extrusion/30mm-compact/drawings/todos.JPG"
   alt: ""
   title: ""
---
<div prefix="/templates/jekyll/" file="machine.html" context="machine global" class="fragment">
    
    <div prefix="/templates/jekyll/" file="machine_layout.html" context="machine global" class="fragment">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
        </li>
        {% if page.tabs.build != false %}
        <li class="nav-item">
            <a class="nav-link" id="build-tab" data-toggle="tab" href="#build" role="tab" aria-controls="build" aria-selected="false">Build</a>
        </li>
        {% endif %}
        <li class="nav-item">
            <a class="nav-link" id="resources-tab" data-toggle="tab" href="#resources" role="tab" aria-controls="resources" aria-selected="false">Resources</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="howtos-tab" data-toggle="tab" href="#howtos" role="tab" aria-controls="howtos" aria-selected="false">Howtos</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#media" role="tab" aria-controls="media" aria-selected="false">Media</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#discussion" role="tab" aria-controls="media" aria-selected="false">Discussion</a>
        </li>

        {% if page.wiring %}
        <li class="nav-item">
            <a class="nav-link" id="wiring-tab" data-toggle="tab" href="#wiring" role="tab" aria-controls="wiring" aria-selected="false">Electronics</a>
        </li>

        {% endif %}


    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="overview" role="tabpanel" aria-labelledby="overview-tab">

            {% include download_spec.html %}

            <div prefix="machines/extrusion/30mm-compact/" file="overview.html" context="machine" class="fragment">
    <div>

        <a href="/machines/extrusion/30mm-compact//renderings/perspective.JPG">
            <img src="/machines/extrusion/30mm-compact//renderings/perspective.JPG" style="margin:8px; float: left;max-width:50%;max-height: 400px;" />
        </a>

        <span style="font-size: smaller; margin-top: 16px;">
            PlasticHub Extruder based on PP v4. but with improvments and simplifications.
        </span>

        <div style="display: table-cell;">
            <div prefix="machines/extrusion/30mm-compact/" file="mini_specs.md" context="machine" class="fragment">
    <p><span id="specs" style="padding: 16px">
            <table>
                <tbody>
                    <tr>
                        <td>Type:
                        </td>
                        <td> Extruder
                        </td>
                    </tr>
                    <tr>
                        <td>Version:
                        </td>
                        <td> 4.3
                        </td>
                    </tr>
                    <tr>
                        <td> Status:
                        </td>
                        <td> Tested
                        </td>
                    </tr>
                    <tr>
                        <td> License
                        </td>
                        <td><a href="https://ohwr.org/cernohl">CERN Open Source Hardware License</a>
                        </td>
                    </tr>
                    <tr>
                        <td> Authors </td>
                        <td>
                            <a href="https://precious-plastic.org/pp/">PlasticHub S.L.</a> |
                            <a href="https://preciousplastic.com/">PrecisousPlastic - Eindhoven</a> |
                            <a href="https://www.instagram.com/peterbasschelling/">Peter Bas</a> |
                            <a href="https://www.facebook.com/david.bassetti.79">David Basetti - 3D Seed</a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </span></p>
</div>

            {% include machine_toolbar.html download="https://precious-plastic.org//archives/lydia-v4.zip" preview="" %}

        </div>

    </div>

    <a href="/machines/extrusion/30mm-compact//drawings/overview.JPG">
        <img src="/machines/extrusion/30mm-compact//drawings/overview.JPG" style="margin:8px;" />
    </a>

    <hr />

    <div prefix="machines/extrusion/30mm-compact/" file="features.md" context="machine" class="fragment">
    <h3 id="features">Features</h3>
    <ul>
        <li>Jam detection</li>
        <li>Emergency and reset button</li>
        <li>Various motor protections (thermal &amp; magnetic).</li>
        <li>Rigid, flexible and modular framework – enabling hacking and extensions. The base plate is 15 mm thick.</li>
        <li>Extrusion hopper can be easily detached.</li>
        <li>Thermocouples (sensors) have clips, easy to detach.</li>
        <li>Complies also with basic regulations in schools.</li>
        <li>Stops automatically after one hour of inactivity.</li>
        <li>High quality cables and wiring according to industrial standards.</li>
        <li>Compression screw according to v4 with added extra strength</li>
        <li>Stronger taper bearings extra trust bearing</li>
        <li>Quality heatbands and PID controllers</li>
        <li>Simplified bearing housing design with no exotic parts as in v4, easy to replace and scale for even bigger screws</li>
        <li>Semi automatic control : record &amp; replay extrusion speed</li>
    </ul>
</div>

    <hr />

    <div prefix="machines/extrusion/30mm-compact/" file="academy_overview.md" context="machine" class="fragment">
    <table>
        <thead>
            <tr>
                <th>Specification</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Type</td>
                <td>Single Screw</td>
            </tr>
            <tr>
                <td>Price new material</td>
                <td>+/- €1500</td>
            </tr>
            <tr>
                <td>Weight (inc frame)</td>
                <td>110 kg</td>
            </tr>
            <tr>
                <td>Dimension</td>
                <td>1500 x 600 x 1550 mm</td>
            </tr>
            <tr>
                <td>Power (W)</td>
                <td>5 kW</td>
            </tr>
            <tr>
                <td>Voltage</td>
                <td>400V or 220V</td>
            </tr>
            <tr>
                <td>Amperage</td>
                <td>16A</td>
            </tr>
            <tr>
                <td>Input Flake Size</td>
                <td>Small</td>
            </tr>
            <tr>
                <td>Screw diameter</td>
                <td>30mm</td>
            </tr>
            <tr>
                <td>Length of screw (mm)</td>
                <td>790 mm</td>
            </tr>
            <tr>
                <td>Effective screw length</td>
                <td>600 mm</td>
            </tr>
            <tr>
                <td>Rated Motor Power</td>
                <td>3 kW</td>
            </tr>
            <tr>
                <td>Motor Type</td>
                <td>(check the build section for more details)</td>
            </tr>
            <tr>
                <td>- Rated Motor output Torque</td>
                <td>109 Nm</td>
            </tr>
            <tr>
                <td>- Rated Motor output speed</td>
                <td>263 RPM</td>
            </tr>
            <tr>
                <td>- Max. Motor and Inverter power</td>
                <td>3 kW</td>
            </tr>
            <tr>
                <td>- Recommended motor shaft</td>
                <td>30 mm</td>
            </tr>
            <tr>
                <td>- Heating zones</td>
                <td>3</td>
            </tr>
            <tr>
                <td>- Heating power: max.</td>
                <td>2 kW</td>
            </tr>
        </tbody>
    </table>
    <h3 id="proscons">Pros &amp; cons</h3>
    <h4 id="pros">Pros</h4>
    <p>The extruder is one of the fastest and most efficient to melt plastic among Precious Plastic machines. It is a safe and reliable machine amongst our machine’s family!</p>
    <p>The Extruder Pro screw is designed to work with multiple types of plastics. Which will make you capable of working with different kind of plastic in small batches. Also this machine is conceived to be much more polyvalent than other industrial machines, allowing you to extrude into moulds, shapes, different nozzles…</p>
    <h4 id="cons">Cons</h4>
    <p>This machine requires a higher skill set of machining amongst Precious Plastic machines. It also requires a large motor and a specialized screw that make it a relatively expensive machine compare to other Precious Plastic machines.</p>
    <h3 id="basicvspro">Basic VS Pro</h3>
    <p>Compared to smaller version of extruder, this machine:</p>
    <ul>
        <li>Has a larger screw which gives a higher output</li>
        <li>Optimized screw design brings an homogeneous melt for a better quality plastic</li>
        <li>An improved bearing design to support axial and radial extrusion forces</li>
        <li>Is made to run for full days</li>
        <li>Easier assembly and servicing</li>
        <li>Overall stronger and tested components</li>
        <li>A safer and easier to build electronic box</li>
        <li>Has a larger insulated hopper allowing better fluidity</li>
    </ul>
    <h3 id="inputoutput">Input &amp; output</h3>
    <p>A variety of plastic types will be effective in this extruder. Each plastic type has its own properties and behaviours (flexible, hard, liquid etc.). Materials with a wide melting temperature range are easiest to work with. We suggest starting with PP, HDPE, LDPE or PS since they require less temperature precision. We do not recommend trying polymers with smaller melting temperature windows (such as PET), but if you decide to do so, please take safety precautions.</p>
    <p>You can set the temperature using the controllers on the electronic box. The heating elements are labeled and wired into three groups (nozzle, barrel and feeding). We recommend to use the temperatures from the table below as a general rule of thumb. This is to help make sure the plastic is fully melted right before it comes out.</p>
    <h3 id="heatingtemperatures">Heating Temperatures</h3>
    <table>
        <thead>
            <tr>
                <th>Plastic Type</th>
                <th>Feeding Zone (C°)</th>
                <th>Barrel Zone (C°)</th>
                <th>Nozzle Zone (C°)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>PP</td>
                <td>190</td>
                <td>200</td>
                <td>200</td>
            </tr>
            <tr>
                <td>PS</td>
                <td>200</td>
                <td>210</td>
                <td>210</td>
            </tr>
            <tr>
                <td>HDPE</td>
                <td>190</td>
                <td>200</td>
                <td>200</td>
            </tr>
            <tr>
                <td>LDPE</td>
                <td>190</td>
                <td>200</td>
                <td>200</td>
            </tr>
        </tbody>
    </table>
    <h3 id="throughout">Throughout</h3>
    <ul>
        <li>Beam Mould (2000 mm x 40 mm x 40 mm): 8 Min</li>
        <li>Little Lego Brick Mould: 4 Min</li>
        <li>Big Lego Brick Mould: 6 Min</li>
        <li>Skateboard Mould: 20 Min</li>
    </ul>
</div>

    <hr />

    
</div>
            <hr />
            
            <hr />

            <div prefix="/templates/jekyll/" file="used_in.md" context="machine global" class="fragment">
    <p>{% if page.usedin %}
    <h4>This is used in</h4>
    <div class="ty-vendor-plans small">
        {% for component in page.usedin %}
        {% for doc in site.machines %}
        {% if doc.product_id == component %}
        <div class="ty-grid-list__item_small">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover_small" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    {% endif %}</p>
</div>

        </div>

        {% if page.tabs.build != false %}
        <div class="tab-pane" id="build" role="tabpanel" aria-labelledby="build-tab">

            

            <div prefix="machines/extrusion/30mm-compact/" file="build.md" context="machine" class="fragment">
    <h2 id="general">General</h2>
    <p><a href="https://docs.google.com/spreadsheets/d/14AzvGXF-KGlYrc79eihJhmff0n_5hrI9Y-tWOBZUC9M/edit#gid=1334194269">BOM - Bill of materials</a></p>
    <p><strong>Parts to make</strong></p>
    <p><a href="/machines/extrusion/30mm-compact//drawings/exploded.JPG">
            <img src="/machines/extrusion/30mm-compact//drawings/exploded.JPG" alt="" />
        </a></p>
    <p><a href="https://precious-plastic.org//machines/extrusion/30mm-compact//renderings/section_1.JPG">
            <img src="https://precious-plastic.org//machines/extrusion/30mm-compact//renderings/section_1.JPG" alt="" />
        </a></p>
    <h3 id="requirements">Requirements</h3>
    <ul>
        <li>Bigger lathe with adjustable 4 jaw chuck, able to deal with a stock size of 40mm OD and 1 meter length</li>
        <li>Decent MIG Welder</li>
        <li>Drill press and power tools</li>
        <li>Optional &amp; recommended : Mill, for keyway, height gauge, precision caliper</li>
    </ul>
    <h3 id="notes">Notes</h3>
    <ul>
        <li>This is a simplified version, using 2 taper and thrust bearings. Please watch the <a href="https://www.youtube.com/embed/zWpKdaYInZ0">video</a> about the screw components</li>
    </ul>
    <h2 id="videos">Videos</h2>
    <h3 id="screwcomponents">Screw Components</h3>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/zWpKdaYInZ0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

            {% if page.gallery_drawings %}
            <h4>Drawings</h4>
            {% include gallery_drawings caption="" %}
            {% endif %}

        </div>
        {% endif %}

        <div class="tab-pane" id="resources" role="tabpanel" aria-labelledby="resources-tab">
            <span style="font-size:smaller">

                <div prefix="machines/extrusion/30mm-compact/" file="product_resources.md" context="machine" class="fragment">
    <p><strong>Product Resources</strong></p>
    <ul>
        <li><a href="">3D Preview</a></li>
        <li><a href="">Wiring</a></li>
        <li><a href="https://docs.google.com/spreadsheets/d/14AzvGXF-KGlYrc79eihJhmff0n_5hrI9Y-tWOBZUC9M/edit#gid=1334194269">BOM - Bill of materials</a></li>
    </ul>
    <p><strong>Social resources and references of our v3 extruder</strong></p>
    <ul>
        <li><a href="https://www.youtube.com/watch?v=iKEghQuUng0">Greenphenix</a></li>
        <li><a href="https://www.facebook.com/plasticprecioslasafor/">PP La Safour</a></li>
    </ul>
    <p></p>
</div>

                

                <h4>Changelog</h4>

                <div prefix="machines/extrusion/30mm-compact/" file="changelog.html" context="machine" class="fragment"></div>

            </span>

        </div>
        <div class="tab-pane" id="howtos" role="tabpanel" aria-labelledby="howtos-tab">
            <div prefix="machines/extrusion/30mm-compact/" file="howtos.md" context="machine" class="fragment"></div>
            <h4>Build Howtos</h4>
            <div prefix="/templates/jekyll/" file="howto_used_in_machine.md" context="machine global" class="fragment">
    <div class="ty-vendor-plans">

        {% for doc in site.howto %}
        {% if doc.usedin and doc.enabled!=false %}
        {% for used in doc.usedin %}
        {% if used == page.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endif %}
        {% endfor %}
    </div>
</div>
        </div>
        <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="media-tab">
            
            {% if page.gallery %}
            {% include gallery caption="" %}
            {% endif %}

            {% if page.gallery_social %}
            <h4>Media on the network</h4>
            {% include gallery_social caption="" %}
            {% endif %}

        </div>

        <div class="tab-pane" id="discussion" role="tabpanel" aria-labelledby="discussion-tab">
            <div id='discourse-comments' style="min-height: 600px;"></div>
            <script type="text/javascript">
                DiscourseEmbed = {
                    discourseUrl: 'https://forum.precious-plastic.org/',
                    discourseEmbedUrl: '{{site.url}}{{page.url}}.html'
                };

                (function() {
                    var d = document.createElement('script');
                    d.type = 'text/javascript';
                    d.async = true;
                    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
                })();
            </script>
        </div>

        {% if page.wiring %}
        <div class="tab-pane" id="wiring" role="tabpanel" aria-labelledby="wiring-tab">
            <div prefix="machines/extrusion/30mm-compact/" file="wiring.md" context="machine" class="fragment">
    <p><div prefix="machines/extrusion/30mm-compact/" file="Schematics220V.md" context="machine" class="fragment">
    <h3 id="wiring220v">Wiring - 220V</h3>
    <p>{% include product_image.html src="resources/wiring_220V_basic.png"  title="Circuit Diagram" target="resources/v4Ex-220v-wiring.svg"  %}</p>
    <h3 id="wiring220vwithcontactorsrecommended">Wiring - 220V - With Contactors (Recommended!)</h3>
    <p>{% include product_image.html src="resources/v4Ex-Contactors.jpg"  title="Circuit Diagram" target="resources/v4Ex-Contactors.svg"  %}</p>
</div></p>
    <p><div prefix="machines/extrusion/30mm-compact/" file="Schematics380V.md" context="machine" class="fragment">
    <h3 id="wiring380v">Wiring - 380V</h3>
    <p>{% include product_image.html src="resources/circuit_1.jpg"  title="Circuit Diagram" target="resources/circuit.pdf"  %}</p>
    <p>{% include product_image.html src="resources/circuit_2.jpg"  title="Circuit Diagram" target="resources/circuit.pdf"  %}</p>
</div></p>
</div>
        </div>
        {% endif %}

    </div>

    <div prefix="/templates/jekyll/" file="alternative_machines.md" context="machine global" class="fragment">
    <p>{% if page.alternatives %}</p>
    <h3 id="alternatives">Alternatives</h3>
    <div class="ty-vendor-plans">

        {% for doc in site.machines %}
        {% for alternative in page.alternatives %}
        {% if alternative == doc.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    <p>{% endif %}</p>
</div>
</div>


</div>