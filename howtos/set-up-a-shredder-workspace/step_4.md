### Check out your area 

 Get an overview of who and what is already existing in your area.

Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. 

Make sure not to jam the local network, if there are already five shredder places around you, have a chat with them how to collaborate or maybe consider starting another type of workspace.

👉 community.preciousplastic.com/map