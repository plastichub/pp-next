### Prepare your meeting  

 During your first meeting, you will briefly tell the business what, why and how you are running a Collection Point. Keep in mind that there is a possibility that the people that you will have the meeting with, don’t know anything about plastic pollution or precious plastic. Bringing something to visualize the Precious Plastic Universe can help you explain the story better. 

You are able to find a presentation in the download kit. This will help you give a clear explanation of what a Collection Point is, does and how it fits into the precious plastic universe. You can add something related to the company that you're going to, this will make them feel more related to the plastic problem. 

Tip: Practice the presentation a couple of times to feel more confident during your meeting. 

