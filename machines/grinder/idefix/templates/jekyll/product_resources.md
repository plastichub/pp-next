### Product Resources

#### Build

- [3D Preview](${product_3d})
- [Drawings](${product_drawings})
- [CAD model](${product_cad})
- [Source files](${product_github})
- [BOM - Bill of materials](${product_bom})
- [Better auto-reverse](https://gitlab.com/plastichub/firmware/tree/master/shredder-extrusion/zoe)
#### Support

- [Discord Chat](${product_chat})
- [Starterkit / Howto ](https://preciousplastic.com/starterkits/showcase/shredder.html)

<hr/>

${product_howtos}
