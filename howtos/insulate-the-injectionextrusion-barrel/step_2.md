### Form the mesh 

 Now find something cylindrical to use in order to shape your mesh into the desired shape. We have used a gas tank, but you could use another piece of steel. Get creative!

Tip: You might need to hammer it a bit smaller after your first shaping because metal often has a ‘springback’ and may be too big.
