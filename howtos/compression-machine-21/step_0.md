### Get ready 

 One of the main challenges with the V2 of our compression machine: It's a rather slow process if compared with the injection or extrusion. This is particularly true because the plastic is first heated, then compressed and cooled in the oven-  lots of waiting time. Good for experimenting but not feasible if you want to run a production.

With this upgrade the compression area (carjack) is shifted underneath the machine, this way the machine can run continuously. With this change you can now compress and cool one mould while you're heating up a second one. Besides being more productive and efficient this upgrade makes the machine easier to build as you don’t need to install the compression mechanism inside the oven. Take note of the drawings. 
