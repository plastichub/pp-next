### Drill the holes in the aluminium mould 

 Now the mould parts need holes. We have to make 2 types of holes. Total will be 6. (the injection hole comes later)

First we drill 2 halfway holes on both inner sides of the aluminium blocks to fit 2 metal dowel pins in there. These pins make sure your mould is always aligned straight when you are injecting. (see first image) Drill the hole size according to the dowel pin size you have. I used 6m width. Drill the holes on both sides half way. 
After opening and closing the mould more often the opening and closing will become more easy. 

Then 4 holes are needed to connect and close the mould with bolts and nuts. Drill 4 holes on the 4 corners of the aluminium blocks. Use the pre marked drill indicators ór close the mould straight
