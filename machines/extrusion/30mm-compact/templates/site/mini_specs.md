<span id="specs" style="padding: 16px">
    <table class="center specs">
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td> Extruder
                </td>
            </tr>        
            <tr>
                <td>Version:
                </td>
                <td> 4.3
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Tested
                </td>
            </tr>
            <tr>
            <td> License
                </td>
                <td><a href="https://ohwr.org/cernohl">CERN Open Source Hardware License</a>
            </td>
            </tr>
            <tr>
            <td> Authors </td>
                <td>
                 <li>[PlasticHub S.L.](${author_link}) </li>
                 <li>[PrecisousPlastic - Eindhoven](${author_link_pp}) </li>
                 <li>[Peter Bas](https://www.instagram.com/peterbasschelling/) </li>
                 <li>[David Basetti - 3D Seed](https://www.facebook.com/david.bassetti.79)</li>
                 <li>[Dan Shirley - Timberstar](https://www.plastic-hub.com)</li>
                </td>
            </tr>
            <tr>
                <td>Type</td>
                <td>Single Screw</td>
            </tr>
            <tr>
                <td>Weight</td>
                <td>110 kg</td>
            </tr>
            <tr>
                <td>Power (W)</td>
                <td>5 kW</td>
            </tr>
            <tr>
                <td>Voltage</td>
                <td>380V or 220V</td>
            </tr>
            <tr>
                <td>Amperage</td>
                <td>16A</td>
            </tr>
            <tr>
                <td>Input Flake Size</td>
                <td>Small</td>
            </tr>
            <tr>
                <td>Screw diameter</td>
                <td>30mm</td>
            </tr>
            <tr>
                <td>Length of screw (mm)</td>
                <td>900 mm</td>
            </tr>
            <tr>
                <td>Effective screw length</td>
                <td>600 mm</td>
            </tr>
            <tr>
                <td>Rated Motor Power</td>
                <td>3 kW</td>
            </tr>
            <tr>
                <td>Motor</td>
                <td></td>
            </tr>
            <tr>
                <td>- Rated Motor output Torque</td>
                <td>109 Nm</td>
            </tr>
            <tr>
                <td>- Rated Motor output speed</td>
                <td>263 RPM</td>
            </tr>
            <tr>
                <td>- Heating zones</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Heating power: max.</td>
                <td>2 kW</td>
            </tr>
        </tbody>
    </table>
</span>
