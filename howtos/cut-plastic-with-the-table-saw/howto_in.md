### Cut plastic with the table saw 


The table saw is the best way to achieve straight cuts through a plate or a beam. However in order to achieve a clean cut in plastic there are few considerations we will explain here.
