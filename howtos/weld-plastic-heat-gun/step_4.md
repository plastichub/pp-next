### Prepare the heat gun 

 Set the temperature of the heat gun significantly higher than the melting temperature of the plastic you are welding.
In this case we are welding HDPE with a temperature around 100ºC above the melting point, since we want to quickly melt both the surface and the stick.