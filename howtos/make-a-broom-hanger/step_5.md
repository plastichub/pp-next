### Male mold part 2 

 Now, get part no. 3 and mill one face on an angle of 15°. Then, mill the other face to an opposite angle of 15°, until the width of the narrow face matches the face of part 5 (see last image). That should be 14,19mm in the narrower face and 21mm in the wider face. 

(Drawings page 7)