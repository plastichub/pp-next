<span id="specs" style="padding: 16px">
    <table class="center specs">
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td> Sheetpress
                </td>
            </tr>
            <tr>
                <td>Version:
                </td>
                <td> 1.3
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Tested
                </td>
            </tr>
            <tr>
                <td> Sheet - size:
                </td>
                <td> 1.20 x 1.20 meter
                </td>
            </tr>
            <tr>
                <td> Based on:
                </td>
                <td> [PP Eindhoven Sheetpress](https://community.preciousplastic.com/academy/build/sheetpress)
                </td>
            </tr>
            <tr>
                <td> Authors </td>
                <td>
                    <li>[PlasticHub S.L.](${author_link})</li>
                    <li>[PrecisousPlastic - Eindhoven](${author_link_pp}) </li>
                    <li>[David Basetti - 3D Seed](https://www.facebook.com/david.bassetti.79)</li>
                    <li>[Dan Shirley - Timberstar](https://www.plastic-hub.com)</li>
                </td>
            </tr>
            <tr>
                <td>Weight</td>
                <td>480kg</td>
            </tr>
            <tr>
                <td>Voltage</td>
                <td>400V</td>
            </tr>
            <tr>
                <td>AMP</td>
                <td>32A</td>
            </tr>
            <tr>
                <td>Power</td>
                <td>15kW</td>
            </tr>
            <tr>
                <td>Input Flake Size</td>
                <td>Large, Medium, Small</td>
            </tr>
            <tr>
                <td>Max Running Time</td>
                <td>8 hours per day</td>
            </tr>
            <tr>
                <td>Max temperature</td>
                <td>300 °C</td>
            </tr>
            <tr>
                <td>Tested Plastics</td>
                <td>HDPE, LDPE, PP, PS</td>
            </tr>
            <tr>
                <td>Input Between Plates</td>
                <td>300mm</td>
            </tr>
            <tr>
                <td>Range of Sheet Thickness</td>
                <td>4 - 35mm</td>
            </tr>
            <tr>
                <td>Sheets Per Day (12mm)</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Sheets Per Day with full system (12mm)</td>
                <td>10</td>
            </tr>
        </tbody>
    </table>
</span>