### Wash Plastic: Washing Machine 


Different steps are necessary to make plastic clean. First is the prewashing after the plastic can be shredded, but it will still contain some oils, glue remains or even paper. To get rid of this we need water, soap, heat, and friction. Building a machine, which can do all of this takes time and costs a lot of money. So instead we are going to use a washing machine. It is accessible, cheap and integrates with a few modifications perfectly in an existing sand filter system.
