### Get your machines 

 Cool, now that you have a space it’s time to get hold of your machines of choice. There is no right or wrong way to go here. tWe always recommend to start small to keep the investment lower, but you can also go full in from the beginning.

There are three ways you can get the machines:

1 Build them yourself following our tutorials.
👉 tiny.cc/build-machines

2 Buy them on the Bazar.
👉 bazar.preciousplastic.com

3 Find a Machine Shop near you on the map that can build it for you.
👉 community.preciousplastic.com/mapBuilt them yourself following our tutorials