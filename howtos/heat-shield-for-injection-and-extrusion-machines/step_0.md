### Build your heat shield 

 The YouTube video shows it better.  As everyone's machines are different measurements, this is not included here, but should be self explanatory in the video.

We used round stainless steel tubing at 4 inch size.  However you can also use square tube.  As long as your tube is long enough to fit over the heater bands and insulation without direct contact.  

For insulation we use 5 meters of muffler insulation.

We secured the tubing with m6 bolts and tapped the tubing.  You can use other size bolt or even machine screws.