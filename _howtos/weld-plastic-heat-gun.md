---
image: "/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-13.jpg"
title: "Weld plastic: Heat gun"
tagline: ""
description: "With the heat gun you can weld two pieces of the same plastic type together.<br />Here you’ll find some tips for this technique."
keywords: "melting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Stay safe</h4>
<div class="step-text">
  When talking about safety, we are only referring to precautions about working specifically with plastic, as it’s our thing. Working with machines like the table saw requires a certain level of expertise, so please take all the precautions related with how the machines work.<br />
  <br />
  As we are melting plastic, bad fumes can be released. In order to work safer, make sure to use a gas mask with ABEK filters to prevent inhaling possibly toxic fumes. Special attention on plastics like PS and PVC. Also when handling with heated elements we recommend to wear working gloves.<br />
  <br />
  Recommended safety equipment:<br />
  - ABEK mask<br />
  - gloves
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-5.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Gather your tools for welding</h4>
<div class="step-text">
  Welding is a process in which we join two parts together by applying heat and adding a filler of the same material in between the parts.<br />
  In order to do this we are using a hot gun with a specific nozzle that helps us to spread the melted material on the surfaces which we want to weld. This nozzle will concentrate the heat on the welding line and will guide the stick in the process.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-8.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-8.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare the edges </h4>
<div class="step-text">
  In order to maximise the welding surface, we make a chamfer along the sides of both pieces. For that you can use a manual milling machine or a hand router with a V shape tool.<br />
  By making a chamfer we are increasing the surface that will be melted together with the welding line and therefore we’ll create a stronger weld.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-9.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-9.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-12.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-12.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Make a welding stick</h4>
<div class="step-text">
  We will need a plastic stick to weld the pieces together. It’s very important that the welding material comes from the same source as your pieces, otherwise the weld might not be homogeneous.<br />
  <br />
  The stick can be made in many ways, it can be extruded or it can be cut out of a sheet, like it is shown here. The important part is that it’s as consistent as possible and also fits inside the welding tool. Generally something between 2-4mm should work.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-3.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-11.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-11.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-1.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Prepare the heat gun</h4>
<div class="step-text">
  Set the temperature of the heat gun significantly higher than the melting temperature of the plastic you are welding.<br />
  In this case we are welding HDPE with a temperature around 100ºC above the melting point, since we want to quickly melt both the surface and the stick.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-7.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Clamp the pieces</h4>
<div class="step-text">
  During the process we will apply pressure, so we need to clamp the pieces together tightly in order to avoid undesired movements while welding.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-10.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-10.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Weld</h4>
<div class="step-text">
  Wait until the heat gun reaches the desired temperature, then put the heat gun welding nozzle right over the groove. Gently press the stick through the nozzle and follow the groove slowly but steady. In order to achieve a uniform welding we need to give the heat gun enough time to melt both the stick and the surfaces.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-4.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-2.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Done</h4>
<div class="step-text">
  If the welding has been done correctly both pieces should be fused together with the added material. A good way to see the result is by cutting through the piece and checking the consistency on the welded area.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-6.jpg">
        <img class="step-image" src="/howtos/weld-plastic-heat-gun/how-to-cut-weld-plastic-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>