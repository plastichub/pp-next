### Open the construction manual 

 Via this link: https://drive.google.com/drive/folders/1m8Bq35N_N-nw5llw7T16WAWR9GLb5i_L?usp=sharing , you can find a very detailed construction plan and bill of materials, along with technical drawings, laser cutting files and more. This should be more than enough for you to build your own bicycle powered shredder :) 


We could not use the supporting files tab, since our files exceeded the maximum file size amount.
