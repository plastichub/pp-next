### Promote your workshop 

 Depending on how open the event is, make sure you don’t forget to let people know about your workshop, so that as many people as possible get the chance to see and learn.

There are several places where you can announce it, here some ideas:
- post a story/post on social media with #preciousplastic
- post it in your country’s channel on the Precious Plastic Discord
- create an event on the community platform
👉 community.preciousplastic.com/events
