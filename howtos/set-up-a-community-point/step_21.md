### Good things take time :) 

 Building a community isn't something which happens over night. It’s a big thing, but that shouldn’t discourage you.
You can start with your friend and friend’s friend. Step by step.

Good things take time - and you have to start somewhere and plant the seed.
