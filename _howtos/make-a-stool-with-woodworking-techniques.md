---
image: "/howtos/make-a-stool-with-woodworking-techniques/preciousplastic-stool.jpg"
title: "Make a stool with woodworking techniques"
tagline: ""
description: "Learn how to make a stool only using woodworking techniques. Cutting HDPE is one of the techniques that gives the most shiny outcome without any extra steps."
keywords: "sheetpress,HDPE,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "sheetpress"
- "HDPE"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  Okay, so before jumping into the building process, get an overview of what you need to follow this How-to.<br />
  <br />
  First, download the files attached at the top of this How-to and check the part list to know exactly which tools and materials are needed.<br />
  <br />
  You can make this stool applying basic woodworking tools and techniques.<br />
  <br />
  It&#39;s made entirely out of a plastic sheet, so you’ll need to get one first or make one if you have a sheetpress :)<br />
  <br />
  Related links:<br />
  How to make sheets 👉 tiny.cc/run-the-sheetpress<br />
  Find sheets on the Bazar👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/1.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/1.1.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/1.1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Overview of the product</h4>
<div class="step-text">
  The assembly of this stool works with tight fittings (aka tolerances). The part of the legs that goes through the stool top have to be 0.9mm bigger than the hole. This in combination with the properties of the material will make a really tight fit. <br />
  <br />
  In our case we are using a 28mm thick sheet, so the dimension of the holes has to be 27.1mm. Make sure to first check the thickness of your sheet and adapt the 3D model of the wood jigs to your needs. In the download files you’ll find the dimensions that are susceptible of change marked in red.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/2.JPG">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/2.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Laser cut jigs</h4>
<div class="step-text">
  Once we have double checked the dimensions, we are going to create the patterns we’ll use to mark and cut the different parts of the stool. One for the top and another one for the legs. As we could do this repeated times is recommended to make them out of wood. In this case we will use 9mm plywood, but could be up to 12mm. <br />
  <br />
  One way of cutting this jigs is by using a laser cutter. This is a precise and affordable technique that will guarantee high precision (&lt;0.2mm tolerance).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/3.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Transfer lines to the plastic sheet</h4>
<div class="step-text">
  The next step is to decide where to cut in our plastic sheet. Depending on the quality of your plastic sheet you’ll do this differently. <br />
  <br />
  You should strive for an efficient usage of the material. But if the quality of your sheet has a huge deviation in the thickness you may need to find a good spot first.<br />
  <br />
  In the download files you can find one example where the legs are placed parallely in order to guarantee the same thickness when assembling.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/4.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/4.1.JPG">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/4.1.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Cut roughly with the jigsaw all the parts</h4>
<div class="step-text">
  Once we have the piece marked we just have to cut on the other side of the line with the jigsaw. At this point the cut doesn’t has to be super precise since we are going to use the handrouter afterwards. Just remember to leave enough space (around 8-10mm) from the edge of the mark in order to avoid that the jigsaw deviation caused by the thickness affects the final piece. <br />
  <br />
  If working on plastic with this tool is new to you, check the How-to &quot;Cut with the Jigsaw” where you can find several tricks to cut plastic easier.<br />
  👉 <a href="http://tiny.cc/cut-with-jigsaw">http://tiny.cc/cut-with-jigsaw</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/5.JPG">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/5.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/5.1.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/5.1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Finish the legs</h4>
<div class="step-text">
  Use double sided strong tape to put the jigs on top making sure we have enough space to cut out all over the contour. You will use a combination of table router and manual handrouter in order to cut the whole safely. In both cases make sure the bearing is aligned with the height of the wooden jig, so it follows it’s form.<br />
  <br />
  Once you finish with the leg, you can remove the jig by using a spatula before milling the edges with the 6mm radius counter bit. The result should be 4 legs with a perfect 90º cut and some smooth rounded parts. Check the downloading kit to know where to do it exactly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/6.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/6.1.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/6.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/6.2.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/6.2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Make the top</h4>
<div class="step-text">
  We’ll repeat a similar process to make the stool top. <br />
  <br />
  First we’ll fix the jig on top with double sided tape, you want to put enough so the jig doesn’t move later when pushing the tool against it. <br />
  <br />
  Using the handrouter for all the process first we’ll cut the outside and the four holes with the long bit, and later we’ll round the edges of all the contour of the stool as well as one of the small sides of the holes.<br />
  <br />
  Check the download files to know exactly where to do it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/7.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/7.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/7.1.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/7.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/7.2.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/7.2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Finish the legs</h4>
<div class="step-text">
  There’s one dimension that is on purpose bigger than needed. The reason why is so we can adjust it to the perfect size so it fits just tight. In order to do that we’ll have to measure the bigger length in one of the holes we just made in the stool top.<br />
  <br />
  Then we’ll grab the legs and cut or sand down with a file the front so the final piece measures 0.8mm longer than the hole. <br />
  Example: if the hole measures 95.8mm we want the piece to measure 96.6mm<br />
  <br />
  It is recommended to do it patiently since on this step we are defining partly how tight the piece is going to join.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/8.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/8.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/8.1.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/8.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/8.2.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/8.2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Assembly</h4>
<div class="step-text">
  For the assembly it should be easy to situate the legs by hand and carefully hamer it till it fits on the position. <br />
  <br />
  You will notice that the legs stick out a bit from the top, these parts should get removed and we’ll show you how in the next step.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/9.JPG">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/9.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/9.1.JPG">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/9.1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/9.2.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/9.2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Finish top</h4>
<div class="step-text">
  First use a flexible japanese saw in order to remove the part from the leg that sticks out from the top. Bend it and try to avoid deep scratches on the top. Once the majority of the material is removed you can use a chisel to take out the rest and making it as even and flat as possible. <br />
  <br />
  Finally you can sand the top going from 400 to 1200.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/10.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/10.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/10.1.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/10.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/10.2.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/10.2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Align legs</h4>
<div class="step-text">
  Next step is to make the stool even.<br />
  <br />
  Use a level to know whether the stool sits straight or not and put wooden shims until it does. Once you are happy with how it feels, use a height caliper to mark all the legs at the same height. <br />
  <br />
  fter doing that, cut them straight with a blade or sand them till you are happy with the result.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/11.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/11.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/11.1.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/11.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/11.2.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/11.2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Stamping</h4>
<div class="step-text">
  Before calling this piece done we have to make sure we mark it with the proper material code. In our case we are stamping it with the number 2 - HDPE. This is crucial later on to be able to identify what is it made of and what can be done with it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/12.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/12.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Make it live long!</h4>
<div class="step-text">
  Congratulations, you made it! Enjoy and take care of your new stool. <br />
  <br />
  This stool is designed to be disassembled easily. So in case a part breaks or you want to change something, just remove that piece and replace it with a new one. <br />
  <br />
  Also the top can be always polished to make it shiny again. Even though the scratches can give a cool look over time. <br />
  <br />
  If the piece cannot be reused or refurbished make sure to bring them to your local Precious Plastic workspace or recycle it in another responsible way :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/13.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/13.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-stool-with-woodworking-techniques/VS Context Over Table 2.jpg">
        <img class="step-image" src="/howtos/make-a-stool-with-woodworking-techniques/VS Context Over Table 2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>