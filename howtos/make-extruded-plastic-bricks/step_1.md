### Delivery of laser cut parts 

 Upon delivery, you should receive various pieces with varying thicknesses. It is good practice to check your parts immediately (compare with drawing) in case any errors have been made.  
