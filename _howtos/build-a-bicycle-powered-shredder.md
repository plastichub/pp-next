---
image: "/howtos/build-a-bicycle-powered-shredder/COVER PHOTO.png"
title: "Build a bicycle powered shredder!"
tagline: ""
description: "In this How-to, you will learn how we constructed our bicycle powered plastic shredder, and how you can built your own! Not only does it give you a relatively cheap way to shred plastic at zero operating costs, but it also gives you freedom from the electrical grid, all while keeping you fit and healthy!<br /><br />In step 1 you will find a detailed construction plan and all the files necessary."
keywords: "shredder,HDPE,collection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "shredder"
- "HDPE"
- "collection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/jamil">jamil</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Open the construction manual</h4>
<div class="step-text">
  Via this link: <a href="https://drive.google.com/drive/folders/1m8Bq35N_N-nw5llw7T16WAWR9GLb5i_L?usp=sharing">https://drive.google.com/drive/folders/1m8Bq35N_N-nw5llw7T16WAWR9GLb5i_L?usp=sharing</a> , you can find a very detailed construction plan and bill of materials, along with technical drawings, laser cutting files and more. This should be more than enough for you to build your own bicycle powered shredder :) <br />
  <br />
  <br />
  We could not use the supporting files tab, since our files exceeded the maximum file size amount.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/CONSTRUCTION MANUAL.png">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/CONSTRUCTION MANUAL.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Built or buy the shredder box</h4>
<div class="step-text">
  Precious Plastics has very detailed plans on how to built a shredder box. On the Precious Plastics Bazar you can also buy one.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/Best-of-bike-shredder-15.jpg">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/Best-of-bike-shredder-15.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Built the custom axle </h4>
<div class="step-text">
  With this custom flywheel and axle, you can create forward momentum to shred the plastic. The flywheel will also provide a perfect base to connect the gears talked about in step 5. All the cutting files and the technical drawings can be found in the files in step 1.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/Production-43.jpg">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/Production-43.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/Production-53.jpg">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/Production-53.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Shredder stand and container</h4>
<div class="step-text">
  Built the shredder stand, which will provide a raised platform for the shredded pieces to fall out, and for safety reasons. In the construction plan we explained how you could optimise this stand, since we encountered some problems with ours.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/Production-23.jpg">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/Production-23.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/Production-44.jpg">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/Production-44.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Attaching everything together</h4>
<div class="step-text">
  We tried to maxime the power output, through two ratio transmissions, which come to a total of 1:4.6. This will deliver 4.6 more power at the expense of lower rotational speed. A large gear is attached to shredder box and a large crankset is attached to the custom flywheel.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/transmission 2.png">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/transmission 2.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/transmission 1.png">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/transmission 1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Let's start shredding plastic!</h4>
<div class="step-text">
  Now with the finished result, let&#39;s start shredding some plastic! and lose a few pounds along the way :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-bicycle-powered-shredder/let's start shredding plastic.png">
        <img class="step-image" src="/howtos/build-a-bicycle-powered-shredder/let's start shredding plastic.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>