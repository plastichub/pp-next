### Recommended applications 

 This method is quite specific and expensive to get. We suggest to only use this adhesive when a really strong bonding is needed between same materials like PP or HDPE. 

Oh, and better try to avoid mixing materials with this or other permanent solutions to keep your materials as recyclable as possible.