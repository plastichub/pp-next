Popular PP extruder , perfect for labs, small enterprises; updated for a bigger screw and motor.

<div style="clear:both">
    ${mini_specs}
</div>

<div style="clear:both">
    ${product_overview_drawings}
</div>

### Features

- Advanced extrusion control with optimized workflow to fill molds
- Jam detection
- Status lights : Ok & Error
- Emergency and reset button
- Various motor protections (thermal & magnetic).
- Rigid, flexible and modular framework – enabling hacking and extensions. The base plate is 15 mm thick.
- Extrusion hopper can be easily detached.
- Thermocouples (sensors) have clips, easy to detach.
- Complies also with basic regulations in schools.
- Stops automatically after one hour of inactivity.
- High quality cables and wiring according to industrial standards.
- Compression screw according to v4 with added extra strength
- Stronger taper bearings extra trust bearing
- Quality heatbands and PID controllers
- Simplified bearing housing design with no exotic parts as in v4, easy to replace and scale for even bigger screws

<hr/>

<div>    
    <div style="padding: 16px; display: inline-block">
        ${product_resources}
    </div>
</div>
