### Transfer lines to the plastic sheet 

 The next step is to decide where to cut in our plastic sheet. Depending on the quality of your plastic sheet you’ll do this differently. 

You should strive for an efficient usage of the material. But if the quality of your sheet has a huge deviation in the thickness you may need to find a good spot first.

In the download files you can find one example where the legs are placed parallely in order to guarantee the same thickness when assembling.