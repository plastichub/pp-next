### Smooth beam material 

 Another one of our products has a different aesthetic to the outdoor bench: the indoor modular shelving unit made with PP (Polypropylene). In step 6 - 8 we will talk you through how to achieve a smooth finish with no extra sanding needed to be done to do this. Using recycled PP for extrusion has many advantages, it doesn’t shrink as much as HDPE in the mould and is also flexible, these qualities work well for the shape and texture achieved.

Like the same as before prepare your material, shred, weigh and organise. And remember to make sure its dry!

