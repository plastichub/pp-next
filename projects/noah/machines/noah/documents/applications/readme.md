# Applications for Noah


## Marine

One of the most obvious applications of a large-scale 3d printer such as Noah is boatbuilding.

The famous and recent example of this is the [University of Maine 3DIrigo](https://umaine.edu/news/blog/2019/10/10/umaine-composites-center-receives-three-guinness-world-records-related-to-largest-3d-printer/)

![3DIrigo](https://www.3dnatives.com/en/wp-content/uploads/sites/2/boat_3.jpg)

[Video](https://www.youtube.com/watch?time_continue=6&v=34F71XqvOjg&feature=emb_logo)

The design of 3DIrigo was clearly bespoke.


However many 3d printable files for boats exist already, for free download. 

The world of Radio Controlled boats has adopted desktop 3D printing, and there is huge scope for simply scaling up existing models to lifesize.

[Example: Customizable 3D printable RC boat](https://www.thingiverse.com/thing:2526204)

![3d printable RC boat](https://cdn.thingiverse.com/renders/78/7a/a9/e1/8a/1a471001e8454d8f4255adadbc624473_preview_featured.jpg)



## Civil Engineering


3D printing of megastructures is a new but fast developing field, but so far limited to traditional materials such as this [metal bridge](https://www.youtube.com/watch?v=1r_Azsa4nqU)
or this (rather pointless) [concrete one](https://www.3ders.org/articles/20190116-worlds-longest-3d-printed-bridge-opened-in-shanghai.html)

However, the economic and environmental advantages of using recycled materials are at last [starting to gain recognition](https://www.architecturaldigest.com/story/los-angeles-roads-may-soon-paved-recycled-plastic)

Plastics could be used in civil engineering projects, particularly when used in combination with [fiberous](http://www.elgcf.com/) or [granular](https://www.youtube.com/watch?time_continue=4&v=Q_a48W6BcsE&feature=emb_logo) add-mixes.

A couple of projects have shown [real promise](https://www.youtube.com/watch?v=Lm72tgGsm4s)

[3d printed bridge])https://polymaker.com/wp-content/uploads/2019/02/Bridge-Full-View.jpg)

[Article](https://polymaker.com/3d-printed-bridge/) which gives some useful information about challenges faced.

## Architecture

Exciting possibilities for using recycled plastic in architecture have not yet been fully explored but showing [great potential](https://www.dezeen.com/2017/10/27/peoples-pavilion-dutch-design-week-low-ecological-footprint-bureau-sla-overtreders-w/). 

3d printing wuth recycled plastic will allow [complex new architectural shapes] on a large scale

![printing lattice](https://inhabitat.com/wp-content/blogs.dir/1/files/2018/01/bottletop-london-3d-3-889x667.jpg)




