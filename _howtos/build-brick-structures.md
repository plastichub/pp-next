---
image: "/howtos/build-brick-structures/PA011801-01 (1).jpg"
title: "Build brick structures"
tagline: ""
description: "Recycled bricks are a great way of recycling large amounts of plastic. So what&#39;s better than building your own recycled plastic bike shed or another similar structure? <br /><br />This How-to is split up into the following sections:<br />Step 1 - 2 Intro<br />Step 3 - 6 Fixed structure<br />Step 7 - 19 Build walls<br />Step 20 - 26 Make the roof"
keywords: "PP,product,extrusion,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PP"
- "product"
- "extrusion"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Before you start</h4>
<div class="step-text">
  Before you start, this how-to pulls on a slightly different set of skills than some of the other guides and assumes you have a basic understanding of construction/building/architecture. If any of the elements of the guide are vague, we recommend you query the wider internet as most of these techniques are based on standard building methods. <br />
  In this how-to, you are going to an assortment of materials, but the basics are plastic bricks, timber planks (of various dimensions), screws, expansion bolts and threaded bar. The main tools you will need are a drill, rubber mallet and saw. <br />
  <br />
  If you have the capacity, you could also change the timber for plastic beams.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF0601.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF0601.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Fixed or mobile unit?</h4>
<div class="step-text">
  Recycled bricks are fairly lightweight, so in some cases, you may want to build a mobile/moveable object. Before starting you will need to decide this. If you choose a mobile structure you can skip Steps 2-6.<br />
  <br />
  Learn here how to make the brick 👉 tiny.cc/make-extruded-bricks
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF0604.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF0604.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Locate a concrete slab (Fixed structure)</h4>
<div class="step-text">
  To ensure a strong foundation for our structure, we will need to locate or make a concrete foundation. We will not explain how to make one here but you should be able to find comprehensive guides online.<br />
  <br />
  In this How-to we used the concrete slab of our workspace, which is at least 30cm thick. Depending on your structure 15cm could be utilised.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF0606.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF0606.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Marking holes</h4>
<div class="step-text">
  Before drilling the holes into the concrete, you need to know roughly where you are going to place your bricks. Each brick is approximately 29cm in length, and we need to place an anchor every 4 bricks (approx 1.2m apart), on corners and also at the ends of the structure. The holes also need to be located so that they go through the hollow section of the brick. The location of these holes depends on your final object, and cannot be moved halfway through the process so take extra time to make sure you get this step correct by maybe laying out your first layer before starting.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/anchordrawing_002.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/anchordrawing_002.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/PA100141.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/PA100141.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/anchordrawing_001.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/anchordrawing_001.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Drilling your holes</h4>
<div class="step-text">
  Drilling holes into concrete requires an impact hammer and a masonry drill bit. In this case, we used a 14mm drill bit for a 10mm bolt (next step). Make sure to wear respiratory protection if inside as this can potentially be a dusty job. Always wear protective glasses to protect your eyes. <br />
  <br />
  The depth you drill is dependant on the size of your expansion bolt.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/PA100142.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/PA100142.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Inserting expansion bolts</h4>
<div class="step-text">
  Expansion bolts are what we use in this how-to guide, but there are other options available including epoxy.<br />
  With the expansion bolt, we gently hammer in the bolt until flush with the concrete and then tighten until it can no longer be removed from the hole. <br />
  After this point, the steps are the same as a mobile unit.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/PA100144.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/PA100144.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/PA100145.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/PA100145.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Layout your base (Mobile unit)</h4>
<div class="step-text">
  In this Step, you are creating the bottom layer that will be connected to the top layer holding the bricks together. If you have bolted your structure to the ground you can skip this step, unless you want to create a layer of wood between the concrete and the first layer of bricks. (Which we did for our 2019 Dutch Design Week exhibit: see photo). <br />
  <br />
  This first layer of wood needs to be exactly the same length as your bricks on the first layer and screwed together to create a single unit.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6082.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6082.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Drilling holes into the base</h4>
<div class="step-text">
  We now need to drill holes for our threaded bar, and counter sink the nuts into the base of the wooden frame at about 1.2m intervals, ensuring extras are placed at the corners and ends of the wall . To do this we use a spade head bit with the same diameter as our 10mm washers and then drill the 11mm holes for the threaded bar to pass through.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6072.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6072.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6089.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6089.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Placing threaded bar</h4>
<div class="step-text">
  Now we need to attach the threaded bar to our base structure. This threaded bar needs to be longer than planned wall height. You may need to weld multiple pieces together and if you have a bolt in the concrete, you will need to weld the threaded bar to your bolt.<br />
  <br />
  For the timber, version place a nut and washer on both sides clamping the threaded bar in place.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6064.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6064.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6069.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6069.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Placing limiters</h4>
<div class="step-text">
  To ensure the bricks do not slip off the wooden substructure, we create small spacers which are exactly the same dimension of the base of the brick. These spacers should be located near/on bolts and near corners.<br />
  <br />
  These can be screwed into place.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6054.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6054.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6049.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6049.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Placing bricks</h4>
<div class="step-text">
  Your first layer can now be placed in tandem with adding the second layer. It is easiest to start in one corner and go from there adding Limiters incrementally. Bricks can easily be hammered together and no special tool is required for this, but we recommend a rubber mallet to prevent damage to the bricks.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5833.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5833.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6033.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6033.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - End bricks</h4>
<div class="step-text">
  There are 2 types of bricks required to build a wall, and a 3rd optional giving you more control over your design: single, double and triple brick. <br />
  <br />
  To create a straight edge you will need at least the single brick. this is placed just like all the other bricks.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF9666.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF9666.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF9670.JPG">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF9670.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Build your wall</h4>
<div class="step-text">
  Now continue to build your wall, hammering bricks in place layer by layer, ensuring each brick is tightly hammered together.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF6004.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF6004.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Framing the wall</h4>
<div class="step-text">
  Now that you have reached your desired height you may need to attach columns. To do this we cut the wood to a desired length and then screw to the timber base or bricks ensuring the column is straight with a spirit level.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5819.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5819.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5823.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5823.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Topping the wall</h4>
<div class="step-text">
  Next, you will need to top the wall with timber. To do this drill 11mm holes at the same spacing as previously done to the concrete of bottom frame, sliding the timber over the exposed threaded bar.<br />
  <br />
  Your dimensions for the timber should also be the same as the floor if your structure is even.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5799.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5799.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5813.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5813.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5816.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5816.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Connecting the top plate</h4>
<div class="step-text">
  Next you will need to connect the top plate together using screws and depending on the screw length you may need to drill a hole into the wood to allow the screws to join the wood. This will ensure the top plate acts as a single unit and will be strong enough when we tighten down the structure in the following steps.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5790.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5790.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5792.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5792.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Clamping the bricks together</h4>
<div class="step-text">
  Next step is to clamp the bricks together ensuring they cannot move. To do this you will need to add an additional washer and nut to the threaded bar, tightening the wood down until either the washer sinks into the wood or a good level of resistance is felt to the nut. This step should also increase the strength of the wall and its stability.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5772.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5772.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5757.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5757.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Attaching the top of the beams</h4>
<div class="step-text">
  Next step is screw in the tops of the columns to the newly added wood, now that we have bolted the structure down.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5781.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5781.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Framing & covering the brick teeth (sides)</h4>
<div class="step-text">
  Now that the man structure is clamped we need to add some framing. Here we are using 18mm by 100mm planks, screwed into our frame. Pilot holes were drilled first to avoid cracking.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5757.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5757.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5759.JPG">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5759.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Framing & covering the brick teeth (top)</h4>
<div class="step-text">
  We continue this method around the top, covering the exposed elements of the bricks, and aligning the timber clamping structure perfectly with the bricks. This work requires accurate measurements to look good, so take your time here.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5732.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5732.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5734.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5734.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5720.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5720.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Adding roof frame structure (top beam)</h4>
<div class="step-text">
  The reason we kept the columns longer than the rest of the structure is to allow us to attach a beam to support the roof. The length of this beam should be the same as the space between the columns at the floor (assuming your structure is square).<br />
  <br />
  For this step ensure your beam is level and 2 screws are placed diagonally on each column to secure it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5696 (1).jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5696 (1).jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5698 (1).jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5698 (1).jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5699.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5699.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Make roof frame structure</h4>
<div class="step-text">
  The newly installed beam will help support the roof trusses. These trusses should overhang the walls, but this will depend on your structure and roofing type. For the bike shed you will need 3, each with a notch cut to ensure good contact is made with the existing timber. To do this we used a ruler to create the desired cut and then cut it with a jigsaw. The end of the truss is also cut at an angle, this is an optional step.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5652.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5652.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5669.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5669.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5677.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5677.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Attach roof frame structure</h4>
<div class="step-text">
  Each of the trusses need to be screwed in and evenly distributed. We screw in at an angle to lock the structure in place.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5600.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5600.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5593.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5593.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-24">Step 24 - Attach roof frame structure - purlins</h4>
<div class="step-text">
  The purlins are 2 x 2 pieces of timber that will connect directly to the roofing material. Due to the length of timber, we had access to we had to join them in the middle. We overhung them from the trusses by 20cm.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5585.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5585.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/_DSF5580.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/_DSF5580.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/DSCF5743.JPG">
        <img class="step-image" src="/howtos/build-brick-structures/DSCF5743.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-25">Step 25 - Adding roof sheets</h4>
<div class="step-text">
  Next, you will need to add the roofing sheets to complete the structure. In this how-to we used a recycled roofing sheet that is a little thicker than galvanized, but most other sheeting materials will work (But its best to be sustainable in your choice!). <br />
  <br />
  To do this we lay the sheets overlapping them and making sure they are straight.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/DSCF5717.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/DSCF5717.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/DSCF5721.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/DSCF5721.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/DSCF5739.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/DSCF5739.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-26">Step 26 - Fixing roofing sheet</h4>
<div class="step-text">
  Next, you will need to use the relevant roofing screws to attach the roof to the timber. The number of screws and locations depend on your roofing material type and it is best to consult with your roofing supplier on these details.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/DSCF5707.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/DSCF5707.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-27">Step 27 - Finished!</h4>
<div class="step-text">
  Done, you are now finished!<br />
  <br />
  The best thing about the recycled plastic bricks, other than the fact they are made from precious recycled plastic, is the possibilities. We have attached a few images of options, and look forward to seeing your designs!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/table.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/table.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/shower_000.jpg">
        <img class="step-image" src="/howtos/build-brick-structures/shower_000.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-brick-structures/PA011801-01 (1).jpg">
        <img class="step-image" src="/howtos/build-brick-structures/PA011801-01 (1).jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>