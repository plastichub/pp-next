### Close the mould 

 You can start to prepare the mould by closing it and tighten the clamping bolts. Use two (open) ring spanners to open and close the moulds. For the best clamping results you can use an adjustable torque wrench to make sure the clamping is repetitive accurate. The necessary clamping force is also related to the used material.

Keep in mind that mostly the mould thickness (and screw distance) is a limiting factor to the final clamping force. In case your plastic part has flash marks (plastic runs between mould halves), you may add additional steel plates to the outside to increase the structural rigidity.