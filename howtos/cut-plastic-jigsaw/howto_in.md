### Cut plastic: Jigsaw 


The jigsaw is one of the most accessible professional tools to cut. It’s also one of the more versatile ones offering straight cuts to curves and even the possibility to make inner holes. Here we’re going to explain some tips for applying this technique on plastic.