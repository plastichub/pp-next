### Cutting settings 

 With our jigsaw we can choose between 5 different levels of cutting speed. For cutting plastic we want to set the speed high enough to cut easily but not low enough to prevent the plastic from melting. That’s why we set it on 4.

For the orbital action cutting we set it at 1 or 2 for pieces up to 10mm for PS, and for pieces up to 20mm for HDPE and PP. Higher orbital actions will make the job faster and easier but won’t always guarantee a clean cut.