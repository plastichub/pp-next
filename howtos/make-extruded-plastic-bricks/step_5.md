### Put sides together (Part B) 

 It is easiest to weld the supporting ribs (BB5, BC5, BE5 & BF5) to the 4 side plates before assembling the sides. To do this place the main plate on the welding table and clamp the ribs individually as you weld. Ensuring the main plate remains flat and does not warp. You will need to use (BG5) to ensure even spacing at the top and bottom. 

Repeat these steps for the other 3 sides.
