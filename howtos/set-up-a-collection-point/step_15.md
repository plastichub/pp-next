### Plan your way of transport 

 If you decide to go and pick up the plastic at certain points, you will need to figure out how to transport it. We like using our bike trailor, but have a look and see what makes sense for your area and learn from the local infrastructure and ways of transporting everything around. See if there is any way to make the collection process more efficient or learn from the existent. 

Tip: The transportation is also a moment that people see you going around the streets, therefore it is a good opportunity to spread your message and make your collection point more known. A good way can be for example adding your logo to your transportation devices.