### Add coolant if needed 

 With thicker pieces (> 10mm) the material sometimes keeps melting while cutting, no matter what blade and setting you use. 

In this case, adding cooling while cutting the piece helps. We like using an air compressor, which blows fresh air to the blade so it doesn’t overheat too easily.

Using both machines at the same time may be a bit difficult though, so make sure to ask for help if needed.