### Explore new possibilities 

 This technique opens up a new world of possibilities. It can be especially valuable in cases when being used instead of joining two pieces with a screw or another mechanism. Saves time and materials, and enables an easier disassembly and recyclability. :)