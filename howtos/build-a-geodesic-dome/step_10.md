### Assemble (Part 3) 

 For the third level, connect 5x 6-way-joints to the second level. Use longer beams for connecting to a 6-way-joint and short beams for connecting to a 5-way.

For the fourth and last level, simply connect the remaining 5-way-joint with the 5 short beams.
