### Get to know your surroundings  

 It’s time that the businesses in your surroundings, get to know you as a Collection Point. Researching in your local industry and how you are able to contact the businesses, would be a great start. If there is a Precious plastic community builder in the area you could ask him/her for any help. 

Another option can be that you organize a meetup this makes you able to reach out to different businesses at the same time. You can have a look at the how to - organize a precious plastic meetup to make this happen. 

Tip: Adding all the businesses to one list will give you a good overview of what they are + their contact details. We made an example of how this could look like, you can find it in the download kit. 
