#ifndef VERSION_H 

    #define VERSION_H 

    #define VERSION "1.0.1|PlasticHub|OSR|Asterix|8fca3190b5e1e04aa2c2893289af9893be182611"

    #define SUPPORT "support@plastic-hub.com | www.plastic-hub.com"
    #define BUILD "Asterix-RevA"
    #define FW_SRC "https://gitlab.com/plastichub/pp-next/tree/master/machines/shredder/asterix/firmware/firmware"
    #define CID "-1"

#endif