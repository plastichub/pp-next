The NOAH Macro scale 3d printer can be built using mostly universal scaffold tubes and fittings, and can be based around the Scaff Rollers guide wheel.

3d printable files will be available in this repository.

The Scaff Roller beta version is to be 3d printed in thermoplastic polyurethane filament. 

It is optimized for 3d printing with no overhangs greater than 45 degrees and can therefore be printed on it's side with no support structures. It should be printed with 100% infill and can be printed in "vase" mode.

Inital tests have been very positive using 3mm Ninjaflex Armadillo which has a shore hardness of 75 D. This was printed at 265C on a 40C heated bed.

The bearing code is UC210-31. This is a 1 15/16" bore replaceable bearing insert, normally used in pillow blocks. It has an extended inner ring with set screws.

A UC210 50mm bore may be cheaper and will also work but is not such a good fit- although may be easier to fit if the ends of the tubes are splayed or damaged.

The next itteration of the Scaff Roller will be a cast polyurethane resin encapsulation using a 3d printed 4-part mould. 

