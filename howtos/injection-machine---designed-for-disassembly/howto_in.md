### Injection Machine - Designed for disassembly 


Injection machine designed to be disassembled, easy to transport and ship. It now fits in a 1000x200x200 mm box when disassembled and weighs less than 30 kg. 