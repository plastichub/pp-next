---
image: "/howtos/make-a-chair-with-bent-sheets/LC Front on Angle Down -howto.jpg"
title: "Make a chair with bent sheets"
tagline: ""
description: "The technique of bending plastic sheets offers great applications in furniture design. In this How-to, we will explain how to build a chair from bent PS sheets and a steel frame.<br /><br />It’s split up in two parts:<br />Step 1-7: Making the jigs and moulds<br />Step 8-27: Building the chair"
keywords: "product,sheetpress"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "sheetpress"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  Okay, so before jumping into the building process, get an overview of what you need to follow this How-to.<br />
  <br />
  To make the moulds and metal parts, you will need access to a wood and metal workshop.<br />
  You can find the exact amounts and dimensions of materials in the attached parts list.<br />
  <br />
  As the chair is made with a plastic sheet, you’ll need to get plastic sheets or a sheet press to make the sheet (link to sheet press video) and an oven to heat it for bending. <br />
  <br />
  Related links:<br />
  How to bend plastic sheets 👉 tiny.cc/bend-plastic-sheets
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/how-to-bend-plastic--1.png">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/how-to-bend-plastic--1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Print your plans</h4>
<div class="step-text">
  Let’s start with preparing the plans to make the jigs and moulds for the chair. Print all the provided plans and templates. Pay attention to set the scale to 100% in the print dialogue!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_01.png">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_01.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Cut your parts</h4>
<div class="step-text">
  Before cutting to the exact shape, it’s easier to prepare simple wooden parts for the guides. <br />
  <br />
  Take the part list and the drawings and prepare all the wooden parts (number 1-14) needed for jigs and moulds. Make sure to put the corresponding part number on each piece, so you don’t mix them up later. Also mark the center line on parts 4 and 6-8.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_02_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_02_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_02_2.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_02_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_02_3JPG.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_02_3JPG.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Laminate the drawings</h4>
<div class="step-text">
  Cut the drawings along the dashed lines. Use a ruler and a sharp knife and work precisely. Then attach the drawings to your prepared wooden parts. If there are several drawings for one part, start from A and go on in alphabetical order.<br />
  <br />
  Apply spray glue on the back of one sheet, align it according to the instructions on the drawings and apply it evenly by pushing with your hands from the middle outwards.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_03_1.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_03_1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_03_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_03_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_03_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_03_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Build the jigs</h4>
<div class="step-text">
  Leg alignment: Cut the parts (1-3 and four wedges), drill the holes and screw everything together as shown in the drawings.<br />
  <br />
  Leg bending: Transfer the measurements from the plan to part 4, cut the required wooden pieces and screw them in place. <br />
  <br />
  Bending angle: Cut according to drawings (or adapt to your bending device)<br />
  <br />
  Cross-brace: Drill the 18mm tube holes, then saw along the cutting line. Make sure to stay on the hatched side! Assemble by screwing parts 5 in place.<br />
  <br />
  Sheet shape: Pre-drill holes and cut parts according to the drawing, then sand them smooth.<br />
  <br />
  Put some extra effort into the precision during this phase, as all the flaws made here are hereditary.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_04_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_04_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_04_2.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_04_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_04_3.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_04_3.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Build the mould</h4>
<div class="step-text">
  The last thing is to build the mould to bend the plastic sheet. You can find the building process for this mould in the How to make a mould to bend sheets 👉 tiny.cc/mould-to-bend-sheets.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_05_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_05_1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Mould done!</h4>
<div class="step-text">
  Great job, you should now have all the jigs, moulds and guides to start manufacturing the chairs! If you take care of them, you can use them over and over again. Take a break, step back and admire your work and effort!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_06_1-1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_06_1-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Cut your plastic </h4>
<div class="step-text">
  Finally we can start making our chair! <br />
  Begin with cutting the 14mm PS sheets for your seat and backrest to the dimensions in the part list. For a first rough shape, transfer the shape of the wooden jig to the plastic sheet with a thick marker and cut along that line with the jigsaw (more info about this process in the How to cut plastic: jigsaw 👉 tiny.cc/cut-with-jigsaw.<br />
  <br />
  ❗️In this process, you’ll probably create a lot of plastic dust. Please be conscious and try to save it from going into the environment.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_07_1.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_07_1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_07_2.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_07_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_07_3-1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_07_3-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Smoothen the edges</h4>
<div class="step-text">
  To refine the cut, screw the jig (through the pre-drilled holes) into the intended bottom side of your plastic sheet. Equip your router with a flush trim bit and set the height so that the bearing runs along the jig (shown in the image). Go against the rotation with a medium-low speed. And finally, use the router again to fillet the inner edges, for a better sitting comfort.<br />
  <br />
  Before removing the jig from the plastic, don’t forget to transfer the vertical centerline from the jig to the plastic parts as indicated in the drawings.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_08_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_08_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_08_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_08_2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Bend the plastic sheets</h4>
<div class="step-text">
  Time to bend! Learn the basics in the How-to “Bend sheets” and make sure to Stay safe when melting plastic.<br />
  Heat up your oven to 190°C and heat your sheets (parts A+B) for about 7min. Turn it half way through to ensure a more even heat distribution. We recommend using Teflon fabric or baking paper to prevent the plastic from sticking to the oven.<br />
  <br />
  When soft enough for bending, put the sheet into its mould and align it in the center and along the fence. After pre-bending by hand, put the upper part of the mould on top and make it snap into place. After a couple of minutes it should be cooled down and ready to be taken out of the mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_09_1.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_09_1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_09_2.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_09_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_09_3.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_09_3.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Bend the metal legs</h4>
<div class="step-text">
  For bending the legs (parts C+D), cut them to size and put them in the bending jig to mark the position of the first radius. Then bend the tube to the corresponding angle and repeat that step for every corner. The leg angle jig helps you to obtain more precision.<br />
  Once you are done make sure the leg is still planar and correct any deformation by clamping and bending the piece straight.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_10_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_10_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_10_2.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_10_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_10_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_10_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Weld the legs</h4>
<div class="step-text">
  Notch the rear legs with the angle grinder, position them in the jig and tack them with the welding machine. Be careful not to set the wooden jig on fire!<br />
  Take the legs out of the jig to finish the weld.<br />
  <br />
  ☝️ Melting metal can get really warm, so reduce the welding time in the jig to the minimum and have common fire safety measures handy.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_11_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_11_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_11_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_11_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_11_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_11_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Trim the legs</h4>
<div class="step-text">
  Put the legs back to the jig and cut them roughly where the jig ends. The final alignment will be done after assembly. Mark the position where to cut at the top end and trim with precision, as this is final.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_12_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_12_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_12_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_12_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_12_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_12_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Make the cross-braces</h4>
<div class="step-text">
  Next up it’s time to bend the seat cross-braces (parts E+F). Cut them in length approximately in a 35° angle and mark the position of the indicated holes with a center punch. <br />
  <br />
  Then bend them to the shape of the jig by clamping or hammering. This step requires a bit of experience, but it doesn’t need to be perfectly precise, as you will have the opportunity to make corrections once the seat is screwed together.<br />
  <br />
  Now drill and countersink the previously marked holes, approximately perpendicular to the curvature. Take your tube/round bar for the cross-brace (part G) and notch it as shown in step 11. <br />
  <br />
  Put everything aside for welding during the assembly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_13_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_13_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_13_2.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_13_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_13_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_13_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Backrest connector: Drill the blind holes</h4>
<div class="step-text">
  Put the backrest on its positive mould and drill 12mm holes on the center marks (Step 6). Start perpendicular to the curvature and slowly move to orthogonal.<br />
  <br />
  The outcome should be a blind hole, going as deep as possible without breaking the inside of the backrest. From the moment when the drill is orthogonal and the entire height of the drill bit is cutting, dive at least 7mm into the plastic to have enough space for the locking pins.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_14_1__.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_14_1__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_14_2__.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_14_2__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_14_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_14_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Backrest connector: Drill the locking pin holes</h4>
<div class="step-text">
  Clamp the backrest in a cross table on the drill press to drill the holes for the locking pins (part J). Make sure that the face of the backrest is perpendicular to the drill press table. <br />
  <br />
  Avoid clamping the piece too tightly as it might crack, hence support the backrest on both sides with two pieces of identical thickness to prevent it from tipping to one side. Precision is crucial in this step! <br />
  <br />
  Chose a drill around 0.5-1mm smaller than the diameter of the locking pins. Now align the x-axis to the center of the blind holes (step 13) and the y-axis to the middle of the material thickness. Drill until passing the blind holes and further in as much as possible. This is crucial to make the connecting plugs shake proof in both directions.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_15_1_.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_15_1_.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_15_2_.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_15_2_.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_15_3_.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_15_3_.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Backrest connector: Make the plugs</h4>
<div class="step-text">
  Up next, cut two connector plugs (part H) from 12mm solid steel, each 24mm long. Stick the plugs in the blind holes and mark the position of the locking pins with a drill.<br />
  <br />
  Then take it off and move to the drill press. Choose a diameter slightly bigger than the locking pin and countersink the top side. Drill a M5 center hole (Ø4.2mm), 6mm from the other end of the plug and tap it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_16_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_16_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_16_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_16_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_16_3-01.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_16_3-01.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Backrest connector: Assemble the plugs</h4>
<div class="step-text">
  Before you put everything together, enlarge the previously drilled locking pin holes to a diameter about 0.2 mm smaller than the diameter of the locking pins. The hole should be narrow enough to hold the pin tightly, but not too tight, as PS easily cracks with too much tension. (You might have to do some test samples to find the right size.)<br />
  <br />
  After drilling, mark the immersion depth of the drill bit on your locking pins to see how far you can hammer them in without cracking the material. Finally, stick the plugs into the blind holes, hammer the locking pins in and cut the excess off. Use a file to smoothen and align the ends to the plastic edge.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_17_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_17_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_17_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_17_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_17_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_17_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Backrest connector: Let the frame immerse</h4>
<div class="step-text">
  To make the steel tube of the frame immerse nicely into the backrest, you can make a hole with a hole saw made from the same tube: Cut some teeth into the tube end with an angle grinder and file them a little. Then find or make a fitting shaft that fits into your cordless drill’s chuck, weld it on the tube and maybe use a lathe to achieve a straight rotation axis.<br />
  <br />
  Put your connecting plugs in place and use them to guide your hole saw while you’re cutting a smooth slot. Remember that PS melts quickly, so give the tool some cooling time and sharpen the teeth regularly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_18_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_18_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_18_2-1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_18_2-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_18_3-1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_18_3-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Drill holes for bolts</h4>
<div class="step-text">
  Take the position of the tapped holes from the connector plugs and transfer them to the tube. After that, drill holes (Ø6mm) on the inner median of the tube’s bending radius. They will later hold together the frame and the connector plugs. It’s nice to use countersunk screws, they will make the joint invisible.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_19_1.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_19_1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_19_2.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_19_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_19_3.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_19_3.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Assemble the frame</h4>
<div class="step-text">
  Start by sticking the frame into the assembly jig, then attach the backrest with the connector plugs onto the frame. Weld the cross-braces (both seat and rear) to the legs and use the jigs to align everything accurately. Now it will pay off if you put effort into precise jigs!<br />
  <br />
  After welding, grind all the beats and make them look beautiful.<br />
  Remember: Grinder and paint make you the welder you ain&#39;t! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_20_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_20_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_20_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_20_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_20_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_20_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Stamp your product</h4>
<div class="step-text">
  Make sure to stamp every plastic part with its plastic type before it leaves your workspace!<br />
  You can use a simply heated metal wire or specially engraved stamps from the bazar. For the stamps, heat them up and clamp them to your plastic part.<br />
  <br />
  Related links:<br />
  👉 tiny.cc/wire-stamp<br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_21_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_21_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_21_2.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_21_2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_21_3.JPG">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_21_3.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Put everything together</h4>
<div class="step-text">
  Yeah, assembly time! <br />
  Put the backrest back in place. Then, enlarge the two pre-drilled holes in the seat and attach the frame with screws. While doing so, make sure that the seat’s alignment is straight. <br />
  <br />
  For the other four screws, clamp the cross-brace to the seat, pre-drill holes and screw it together. It’s recommended to use euro screws (the ones you know from attaching hinges to a cupboard), as they give a solid halt in the plastic.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_22_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_22_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_22_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_22_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_22_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_22_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-24">Step 24 - Alignment</h4>
<div class="step-text">
  Now it’s time to cut the legs to the final length. Use scrap wood or wedges to make the frame stand sturdy on a plane surface without shaking. The height of the seat should be 440mm in the front and 430mm in the back. If this is not the case (most probably), set a height gauge to the difference between the as-is and to-be and mark all four legs. Then cut them off nicely along the carved lines.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_23_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_23_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_23_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_23_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_23_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_23_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-25">Step 25 - Finish the frame</h4>
<div class="step-text">
  As plain steel easily rusts, we recommend giving the frame a nice finish. You can use various methods like a paint and brush, spray paint or powder coating. Have a look what method suits your available materials and capabilities best. In this case we used Montana Gold 7090 Coke spray paint.<br />
  <br />
  Remove the plastic parts for painting and put them back on once the paint is dry
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_24_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_24_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_24_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_24_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_24_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_24_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-26">Step 26 - Add pipe plugs</h4>
<div class="step-text">
  Pipe plugs can help to protect both the chair and the floor from getting damaged by adding a soft layer in between.<br />
  <br />
  As it might be tricky to find the perfect fitting for the angled legs, we attached an STL-file for fitiing plugs. So if you have access to a 3D printer you can easily print them yourselves (preferably with recycled filament). The letter on the inside should be aligned as shown in picture 2.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_25_3.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_25_3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_25_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_25_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_25_1 Kopie.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_25_1 Kopie.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-27">Step 27 - Make it live long!</h4>
<div class="step-text">
  Congratulations, you made it! Enjoy and take care of your new chair.<br />
  <br />
  This chair is designed to be disassembled easily. So in case a part breaks or you want to change something, just unscrew the seat from the frame, unbolt the backrest and take it off. And lastly, hit the metal plug with a hammer to break the backrest apart (here we recommend wearing safety glasses to protect your eyes).<br />
  <br />
  Once you have all materials separate again, they can be reused or recycled for new products. Make sure to bring them to your local Precious Plastic workspace or recycle in another responsible way :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/Furniture Adri Stand LC-howto.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/Furniture Adri Stand LC-howto.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_26_1.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_26_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-chair-with-bent-sheets/_26_2.jpg">
        <img class="step-image" src="/howtos/make-a-chair-with-bent-sheets/_26_2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>