### Material Selection 

 The process starts with plastic bag selection. Plastic comes in different types and shapes and it is important to work with similar material. As various types have their own physical properties, mixing material types in one project could be ineffective and lead to a non-recyclable waste.

At the bottom of the bags there is usually a plastic type symbol (a number in triangle of arrows). While they look and feel the same, the type could be different so we need to sort them.

In this project we will used LDPE (Light Density Poly Ethylene) - number 4 in the material type symbol. Hence, we will use a red and a yellow bags, shown in the photos.