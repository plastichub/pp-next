### Weld 

 Wait until the heat gun reaches the desired temperature, then put the heat gun welding nozzle right over the groove. Gently press the stick through the nozzle and follow the groove slowly but steady. In order to achieve a uniform welding we need to give the heat gun enough time to melt both the stick and the surfaces.