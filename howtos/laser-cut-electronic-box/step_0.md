### Get ready 

 An easier and quicker way to make the box is to lasercut it on 3mm plywood or MDF, which also lowers the chances of short circuits. 
Bear in mind that this is more expensive than doing it by yourself.

Each face of the box is lasercut and connected with some nut and bolts, super simple.

We've also added mounting holes for all the electronics inside the drawing which makes it easy and quick to connect everything in the right place.

The laser cut parts can leave quite some rough burn-marks, we recommend to sand them when you receive them, looks way better.