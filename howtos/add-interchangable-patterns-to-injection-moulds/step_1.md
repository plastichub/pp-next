### Design a pattern 

 We used software to create different patterns. Make sure the text is mirrored as we are producing a negative. Depending on the technique used you might need a specific export format.

You can add information (plastic type, production info) as you wish.

With lasecutting there is virtually no limit in shapes you can cut. Do mind that smaller detail is harder to inject. In our experience 2-3mm is possible with HDPE injection. 

When CNC milling the limit is set by the router bit. As it cuts by turning all corners are smoothed out by the diameter of the router bit. Check the limit of the machine you are using. We used a 1mm router bit. 
