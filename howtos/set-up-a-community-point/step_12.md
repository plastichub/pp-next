### Be the local Bazar 

 As the central point of your recycling community, you could order badges, flags, stamps, posters in bulk from the Bazar (as local as possible) and distribute them locally as needed. 

Saving lots of CO2, while making things more accessible for the local community.