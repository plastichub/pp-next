### Sunglasses model - Lenses  

 There is a wide variety of lenses in the market. Again, depending on your target and the type of eyewear you want to make you will choose one type or another. The main ones are the following, although there are many others for specific applications: 

Polycarbonate: Lenses for sports and action eyewear due to its resistance. 
CR-39 (polymer): The most used type of lenses. 
Mineral lenses: The glass lenses that are used less and less due to their weight and the fact that they break when falling. The main advantage is its scratch resistance. 
