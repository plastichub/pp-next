---
image: "/howtos/create-precious-jewellery-from-shredded-cd-cases/jewellery_01.jpg"
title: "Create precious jewellery from shredded CD cases"
tagline: ""
description: "Marta will show you a simple technique to turn old cd cases into precious jewellery."
keywords: "compression,product,PS"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "compression"
- "product"
- "PS"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather everything you need</h4>
<div class="step-text">
  Before you start get all your gear ready:<br />
  -Respirator mask<br />
  -Lots of cd cases (PS)<br />
  -Basic square mould<br />
  -Sanding paper<br />
  -Drill<br />
  -Compression machine<br />
  -Dremmel<br />
  -Plastic Type Stamp
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-precious-jewellery-from-shredded-cd-cases/jewellery_03.jpg">
        <img class="step-image" src="/howtos/create-precious-jewellery-from-shredded-cd-cases/jewellery_03.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Watch this video tutorial</h4>
<div class="step-text">
  Watch this video tutorial to learn step-by-step how to inject the jewels and polish them.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Done & Share</h4>
<div class="step-text">
  After watching this video you should understand the basics of this technique. You can reproduce the rings or try to make other precious products. We’re looking forward to seeing what you can come up with. Make sure to share back your new creations so the community can learn from you. Tag #preciousplastic on social media, create a new how-to or send us an email.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-precious-jewellery-from-shredded-cd-cases/jewellery_04.jpg">
        <img class="step-image" src="/howtos/create-precious-jewellery-from-shredded-cd-cases/jewellery_04.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>