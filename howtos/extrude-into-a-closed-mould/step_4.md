### Calculation Example 

 Our result was 0.4kg (multiply by 2 to get feed rate per min) so 0.8kg/min but because this was unpressurised we need to reduce the result by about 25%. 0.6kg/min meaning on the first extrusion test we shouldn’t let it run for longer than 2.5min before checking its progress and repeating the process adding 10second intervals until we find the exact right time. 