### Heating: sensor configuration 

 We added multiple type K sensors (eighth on each heating plate) to have an accurate heat map so we can identify problems: overheating, broken heaters,... These sensors also make it possible to check how the core and peripheral temperatures are behaving ( warming and cooling speed, fluctuations,...)
Sensors are distributed evenly, not linked to specific heaters.

How we use this data will be explained in a later step.
