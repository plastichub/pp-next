## OSR Platform

### Commons

- [-] Define new type hirarchy & traits
- [-] Custom fields / attributes
- [-] OSE Export/Import

### Deployment / Verticals

- [x] Visibilities / Masks
- [-] Adapter interface

### Specs -> Templates

- [ ] Dataformat ?
- [ ] Validation
- [-] Meta pipe cascades ?

### Bots / Crawlers

- [x] SPA DOM snapshots
- [x] indexer

### Templates

- [x] Product
- [x] Howto
- [ ] Project
- [ ] Vendor
- [ ] Researcher
- [x] Partner/Project

### Widgets

- [ ] General Rating
- [ ] Feature rating
- [ ] Issues
- [ ] Updates/Log
- [ ] Filegrid
- [ ] Resources
- [ ] Link
- [ ] 3D part/subassembly viewer
- [ ] Alternatives
- [ ] Related
- [ ] Search Results
- [ ] References
- [ ] Certs/Compliance
- [ ] Author(s)
- [ ] Specs
- [ ] Mini Specs
- [ ] Howtos
- [ ] Comment
- [ ] CI Renderer
- [ ] [Smart tables/grids](https://github.com/rowsncolumns/grid) : needs expression engine [(bison for js)](https://github.com/catx23/xexpression)

### Physical Widgets

- [ ] Hopper scales
- [ ] Machine - kW 2 volume scales/sheets

### Todo system

### ISO 9001

### ISO 14001
