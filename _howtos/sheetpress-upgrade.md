---
image: "/howtos/sheetpress-upgrade/voor how-to.jpg"
title: "Sheetpress v4.1"
tagline: ""
description: "We have build the sheetpress system with some upgrades which we believe are worth sharing! In short:<br /><br />- Shared our labour hours for realistic price estimation<br />- Making all sheets &amp; parts suitable for lasercutting and sheet metal bending<br />- Updated the bom-list<br />- Made changes to the circuit diagram<br />- Etc,<br /><br />See files below for more information"
keywords: "research,sheetpress,melting"
tags:
- "research"
- "sheetpress"
- "melting"
---
<h4 id="summary">{{page.title}}</h4>
<ul>
  <li> User : <a href="https://community.preciousplastic.com/u/leendert-sonneveld">leendert-sonneveld</a></li>
</ul>
{{ page.description }}
We have build the sheetpress system with some upgrades which we believe are worth sharing! In short:<br /><br />- Shared our labour hours for realistic price estimation<br />- Making all sheets &amp; parts suitable for lasercutting and sheet metal bending<br />- Updated the bom-list<br />- Made changes to the circuit diagram<br />- Etc,<br /><br />See files below for more information
<h3>Files</h3>
<ul>
  <li><a href="https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2Fg1xL0SOktGgEnvdAZew3%2FUpdates%20V4.1%2C%20cost%20estimation%20%26%20possible%20future%20improvements.docx?alt=media&token=1d3da719-7b30-4275-aac0-6fe9bd8fdd21">Updates V4.1, cost estimation & possible future improvements.docx</a></li>,<li><a href="https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2Fg1xL0SOktGgEnvdAZew3%2FFF%20sheet%20and%20coolpress.rar?alt=media&token=8cccebcd-7b9a-46f7-b7e3-6d39e299c3a5">FF sheet and coolpress.rar</a></li>,<li><a href="https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2Fg1xL0SOktGgEnvdAZew3%2FSheetpress%20BOM_v4.1.xlsx?alt=media&token=c0566336-0775-4c16-a2bd-487696f9224d">Sheetpress BOM_v4.1.xlsx</a></li>,<li><a href="https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2Fg1xL0SOktGgEnvdAZew3%2FSheetpress%20electric%20Circuit%20Diagram%20v4.11.pdf?alt=media&token=6c21009a-1eea-4e3a-845d-4036425c405d">Sheetpress electric Circuit Diagram v4.11.pdf</a></li>
</ul>
<h4 class="step-title" id="step-1">Step 1 - See updates 4.1 document</h4>
<div class="step-text">
  In here we described:<br />
  - The updates we did<br />
  - We explain the problems we encountered during building<br />
  - Also we shared the hours we put into the build with an estimation of the cost price if you will make the machine commercial<br />
  - We propose further future improvements which can be done<br />
  <br />
  Displayed picture: The biggest hickup we encountered during building was that we underestimated how important it was to ream the holes of the aluminium blocks to the exact measurements. Followed by heating the blocks to 250 degrees and pressure fit the heating element in to the holes.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-upgrade/document.jpg">
        <img class="step-image" src="/howtos/sheetpress-upgrade/document.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - See cad files</h4>
<div class="step-text">
  All sheets and tubes are optimised for lasercutting &amp; bended sheet metal. This reduces a lot of the labour involved in making it since the parts easily fit and are numbered.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-upgrade/screenshot 3d-cad.jpg">
        <img class="step-image" src="/howtos/sheetpress-upgrade/screenshot 3d-cad.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-upgrade/exploded view.jpg">
        <img class="step-image" src="/howtos/sheetpress-upgrade/exploded view.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - See electric & BOM diagram</h4>
<div class="step-text">
  We made some changes tot the electric circuit diagram and updated the Bom list with suppliers we have chosen
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-upgrade/BOM.jpg">
        <img class="step-image" src="/howtos/sheetpress-upgrade/BOM.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
{% if page.related %}
<h3 id="related"> Related </h3>
{% endif %}
---
{% if page.tags %}
{% endif %}
{% if page.resources %}
{% endif %}