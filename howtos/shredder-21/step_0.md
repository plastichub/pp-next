### Get ready 

 The laser cut files are updated:

We got rid of the 4 brackets on the side, making the building process easier as you don’t need to cut the angle profiles and drill holes (if you want you can still place them, they are backwards compatible).

We also improved the design of the mesh, which is more robust and can be taken out from the side. Simply slide out the two metal rods and the mesh pops out underneath (same process to put it back in).

And lastly, we've also improved the hopper making it safer and more efficient, you can now press the plastic down while the machine is running without chopping of your fingers :)
