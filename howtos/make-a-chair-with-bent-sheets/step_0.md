### Get ready 

 Okay, so before jumping into the building process, get an overview of what you need to follow this How-to.

To make the moulds and metal parts, you will need access to a wood and metal workshop.
You can find the exact amounts and dimensions of materials in the attached parts list.

As the chair is made with a plastic sheet, you’ll need to get plastic sheets or a sheet press to make the sheet (link to sheet press video) and an oven to heat it for bending. 

Related links:
How to bend plastic sheets 👉 tiny.cc/bend-plastic-sheets