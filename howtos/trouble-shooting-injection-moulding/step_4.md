### Sink marks 

 Sink marks are sections of your part surface where the material shrinks more than on other sections and creates holes or indents.

Solutions:
- Increase the holding pressure after the injection operation. After injecting it is recommended to hold the pressure for 2-15sec. depending on your wall thicknesses and part design
- Decrease the material temperature