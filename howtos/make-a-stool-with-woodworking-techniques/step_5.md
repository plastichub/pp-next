### Finish the legs 

 Use double sided strong tape to put the jigs on top making sure we have enough space to cut out all over the contour. You will use a combination of table router and manual handrouter in order to cut the whole safely. In both cases make sure the bearing is aligned with the height of the wooden jig, so it follows it’s form.
 
Once you finish with the leg, you can remove the jig by using a spatula before milling the edges with the 6mm radius counter bit. The result should be 4 legs with a perfect 90º cut and some smooth rounded parts. Check the downloading kit to know where to do it exactly.
