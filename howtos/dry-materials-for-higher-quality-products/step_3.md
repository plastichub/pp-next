### Get Silica Gel 

 You might already know silica gel from a shoebox for instance. It comes in small bags in order to keep the inside dry by absorbing any moisture. Silica gel works perfectly with polymers as well so just put around 2 kilograms in the plastic box. There is no particular amount you have to use, but the more you have in the box the less often you need to take them out and dry them.
