### Good things take time 

 Starting off will take some time in the beginning. It’s normal. Be patient, work smart and reach out to your Precious Plastic community if you need help. Everything will take off. 

Every  bit of plastic you collect is already saved from getting burnt! And keep in mind that because of your Collection Point people around you get more aware, and get the possibility to actually start working with recycled plastic.

You’re changing the world by changing your local area :)
