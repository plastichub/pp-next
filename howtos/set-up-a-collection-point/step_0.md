### Role in the Precious Plastic Universe 

 First of all, make sure you had a look at the showcase page of this Starterkit!
👉 preciousplastic.com/starterkits/showcase/collection-point  

Now about your Role: 

Collection Points are the “catchers” of the Precious Plastic Universe. 

They save plastic from going to waste while fueling the network with raw material ready to be recycled. They collect plastic from people and businesses and sort it by type. This plastic is then passed on to Shredder Workspaces. 