### MOUNT THE STRAP 

 We sew an adjustable buckle on to the strap and then pass it through the holes. The leash is 700mm long, with a bowline to attach it to the handplane and a hangman’s knot around the wrist.