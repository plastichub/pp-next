### Get some plastic waste 

 Now you need to get some plastic waste to be recycled.

You can make contact with a Collection Point near you or find clever ways to get people to bring you plastic. The volumes you will need are not huge so a small collection setup in your workspace can work as well.