<?php
namespace SR;

require __DIR__ . './../vendor/autoload.php';

use SR\ArrayUtils;
use SR\Events;
use SR\Session;

class SessionEvents
{
    protected $db;
    public function views($start, $end, $sid, $hide)
    {
        $db = \WeDevs\ORM\Eloquent\Database::instance();
        $session = Session::where('session', '=', $sid)->first();
        if (!$session) {
            d('invalid session');
            return false;
        }
        $events = Events::toRawEvents($session, 0);
        $events = Events::filterByType('view', $events);
        $events = Events::sortByTime($events);
        $events = Events::filterByTime($events, $start, $end);
        $events = array_map(function ($evt) {
            return array_merge((array) $evt, [
                'id' => '' . $evt->times['s'],
                'tabId' => $evt->payload['tabId'],
                'start' => $evt->times['s'],
                'end' => $evt->times['e'],
                'url' => $evt->payload['url'],
            ]);
        }, $events);
        $events = [
            'start' => $start,
            'end' => $end,
            'views' => $events,
        ];
        return json_encode($events);
    }
    public function events($sid, $types, $hide, $start)
    {
        $types = $types ?: '';
        $db = \WeDevs\ORM\Eloquent\Database::instance();
        $session = Session::where('session', '=', $sid)->first();
        if (!$session) {
            return false;
        }
        $events = Events::toRawEvents($session, $start);
        $events = Events::filterByType($types, $events);
        $events = Events::sortByTime($events);
        $events = array_map(function ($e) {
            return [
                'time' => $e->time,
                'type' => $e->type,
                'payload' => $e->payload,
            ];
        }, $events);
        $events = array_filter($events,function($e){
            return $e->type !== 'view';
        });
        $ret = [
            'id' => $session['id'],
            'visit' => $session['visit'],
            'events' => $events,
        ];

        return json_encode($ret);
        return json_encode($events);
        return $session['events'];
    }
    public function timelineEvents($sid, $types, $hide)
    {
        $db = \WeDevs\ORM\Eloquent\Database::instance();
        $session = Session::where('session', '=', $sid)->first();
        if (!$session) {
            return false;
        }
        $events = Events::toRawEvents($session, 0);
        $events = Events::filterByType($types, $events);
        $ret = [
            'id' => $session['id'],
            'visitID' => $session['visit'],
            'events' => $events,
            'start' => ArrayUtils::first($events)->time,
            'end' => ArrayUtils::last($events)->time,
            'tags' => json_decode($session['tags'], true),
        ];
        return json_encode($ret);
        return json_encode($events);
        return $session['events'];
    }
}
