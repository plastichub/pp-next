### The flipflop sheets  

 40% of all the waste collected on the beaches were flipflops.
This is where the project got its name from and why flipflops were an obligatory element of this boat. 

So we covered the whole boat with sheets of recycled Flipflops, giving it a very colourful look and adding an extra protective layer (the whole boat feels like a big yoga mat :))

The sheets were made by local flipflop artist James who cuts the flipflops into pieces, glues them together and sands them to an even sheet.
