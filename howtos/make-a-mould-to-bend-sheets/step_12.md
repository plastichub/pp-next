### Attach guides 

 Then mark the position of the screws, pre-drill and countersink the holes. Screw through the holes and the sheet metal into the cross-section using countersunk screws. Again, the screws should fully immerse in the material as the opposite half must not interfere with them.