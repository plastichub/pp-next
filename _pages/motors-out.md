---
title: Motor Guide
layout: default
permalink: /motor-guide/
breadcrumbs: true
layout : single
author: false
sidebar: 
   nav: "resources"
---

### Motor guide

General notes here ...

### Basic usage

| Motor Power | Plastic thickness | Results | Jams          |
| ----------- | ----------------- | ------- | ------------- |
| 2.2Kw       | <3 mm             | Ok      | Often         |
| 3Kw         | <4 mm             | Better  | Occassionally |

- Recommondations, per machine
- Warnings, per machine
- Example data (duty times, ...)

### Professional usage

- Recommondations, per machine
- Warnings, per machine
- Example data (duty times, ...)

### Reducers / Worm drives

- some notes about gear boxes here
- alternatives: pulleys (new page)

### Motor selection for machines builders

- recommendations, warrenty,...

### 3Phase vs Single Phase

- bla bla ...

### References

- Wiring (Youtube)
- Refurbishing, inspection (Youtube)

### Protection

- DIN Rail Modules : thermal, magnetic
- Auto-Reverse
- Current Sensors

### Find motors

#### Continent X

- Sources : ....
