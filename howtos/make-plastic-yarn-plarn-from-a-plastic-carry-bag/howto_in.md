### Make plastic yarn (plarn) from a plastic carry bag 


With this technique we will get a single, continuous, seamless strand of yarn without any joining knots.

All you will need is a plastic bag, a pair of scissors and any object that can be used as a weight (your mobile phone will do!).

Once you have this yarn you can apply any handcrafted technique - knitting, crochet, weaving, macramé or braiding! 