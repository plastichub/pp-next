### Weld it 

 Position the parts and fix / clamp them.
Weld the parts first with the thin soldering tip and then with the thick soldering tip. Not too fast!
Check the connection and change the parameters: temperature, welding depth and speed if necessary.
Happy melting! 
