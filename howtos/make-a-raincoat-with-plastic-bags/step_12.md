### Connect sleeves and shoulders 

 The open sleeve pieces should now be connected to our shoulder construction, the joint. can be seen in the illustration below. As with steps 11 and 12, use the 2cm allowance to create the join exactly to the template measurements. Any excess will only be seen from the interior. 