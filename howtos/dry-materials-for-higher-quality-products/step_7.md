### Renew the silica gel 

 After a while, you will notice that the silica gel reaches its maximum capacity of water that it can absorb. Simply put it in the oven for one or two hours at about 100°C/212°F and it will be as good as new.
