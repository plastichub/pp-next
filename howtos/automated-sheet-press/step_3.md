### Heating : Digital PID 

 We switched out the original hardware PID's for a software implementation on the machine controller. This allows further automatisation, data logging and a fluent integration in the user control interface. We also can track (monitor) pressing times and (max) temperatures to optimize the machine use. 
