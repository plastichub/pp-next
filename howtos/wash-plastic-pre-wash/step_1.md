### Get your materials 

 For the pre-washing machine, we are going to need:
- a 120 litre HDPE barrel
- circa 13 m steel profile (30x30mm)
- 1120 x 16 mm steel pipe
- 300 x 30 mm flat steel
- some wood for the wooden board
- four casters and fittings according to the sand filter 
- two ball valves
- and flexible pipes (also according to the sand filter)

- a motor with around 0.5 kW and 1500 r/m (can also be stronger)
- a frequency inverter fitting to the motor

Optional:
- another 120 liter HDPE barrel
- two more ball valves 
- heating element and PID controller
