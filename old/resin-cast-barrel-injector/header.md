# How To: Resin Cast Barrel Injector

### You will need:

- 3 empty cans
- [High temperature 2-part casting silicone](./s-l1600.jpg)
- plunger- aluminium bar around 40mm diameter, minimum length the same as 1 can
- drill press
- something to make a 'nozzle'- short cylinder eg. half a wine cork
- the object you want to reproduce
- oven
- PPE

