### Unmould your parts 

 Once injected, you can take out the sliders and open the mould. You don’t need to wait additionally for the plastic to cool down, the time it takes to open the mould is a sufficient cooldown time. 

Especially the lightswitch cover sits pretty tightly in the mould and levering with a spatula is needed. It helps much to lift the part evenly from each side to eject the parts.

Take care that you don’t scratch the mould cavity with metal tools. 