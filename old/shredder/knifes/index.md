---
image: /_howto/shredder/knifes/IMG_2095.JPG
category: "shredder"
title: "Knifes"
tagline: ""
description: ""
usedin:
  - zoe
  - shredder_v31

---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2089.JPG" _target="_blank">
        <img id="IMG_2089.JPG" src="./IMG_2089.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2090.JPG" _target="_blank">
        <img id="IMG_2090.JPG" src="./IMG_2090.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2091.JPG" _target="_blank">
        <img id="IMG_2091.JPG" src="./IMG_2091.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2092.JPG" _target="_blank">
        <img id="IMG_2092.JPG" src="./IMG_2092.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2093.JPG" _target="_blank">
        <img id="IMG_2093.JPG" src="./IMG_2093.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2095.JPG" _target="_blank">
        <img id="IMG_2095.JPG" src="./IMG_2095.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

