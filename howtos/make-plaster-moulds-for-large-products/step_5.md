### Drill injection + relief holes 

 Ok, time to prepare the mould for your machine. Clamp the parts of your mould together so that they align. In this case, a large hole was drilled to connect to the extruder machine. 

Some smaller holes were also drilled in various locations to act as indicators that the plastic has reached that point. They also help to prevent a build up of pressure.