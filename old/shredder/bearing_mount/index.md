---
image: /_howto/shredder/bearing_mount/IMG_2105.JPG
category: "zoe"
title: "Bearing Mount"
tagline: ""
description: ""
usedin:
  - zoe
  - shredder_v31

---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2096.JPG" _target="_blank">
        <img id="IMG_2096.JPG" src="./IMG_2096.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2097.JPG" _target="_blank">
        <img id="IMG_2097.JPG" src="./IMG_2097.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2098.JPG" _target="_blank">
        <img id="IMG_2098.JPG" src="./IMG_2098.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2099.JPG" _target="_blank">
        <img id="IMG_2099.JPG" src="./IMG_2099.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2100.JPG" _target="_blank">
        <img id="IMG_2100.JPG" src="./IMG_2100.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2101.JPG" _target="_blank">
        <img id="IMG_2101.JPG" src="./IMG_2101.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2102.JPG" _target="_blank">
        <img id="IMG_2102.JPG" src="./IMG_2102.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2103.JPG" _target="_blank">
        <img id="IMG_2103.JPG" src="./IMG_2103.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2105.JPG" _target="_blank">
        <img id="IMG_2105.JPG" src="./IMG_2105.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

