### Closing system: How complex?  

 Closing system: How complex? 
A closing system that allows for faster productions can be very costly. The clamping pressure in an injection mould is vital for good results and mould life. It is, however, not easy to find a system that allows for constant mould change and high enough pressures, evenly distributed, at a low price. 
Using simple screws is a good solution for a smaller budget: It’s cheap and offers a perfect clamping pressure if they are placed right. However, it will be slower to open and close the mould.
Our recommendation is to start with screws, understand your real needs, and work from there. 