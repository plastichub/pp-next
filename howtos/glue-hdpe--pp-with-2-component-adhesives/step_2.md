### Clean surface 

 Before applying the adhesive make sure to clean the surfaces with a non-aggressive product like IPA (Isopropyl Alcohol) cleaner and a Scotch Brite or similar to remove any dirt or release agent left from the mould. 
