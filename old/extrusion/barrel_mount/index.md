---
image: /_howto/extrusion/barrel_mount/IMG_1780.JPG
category: "extrusion"
title: "Barrel Mount"
tagline: ""
description: ""
usedin:
  - lydia-v4
  - zoe
  - extrusion-pro
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_1747.JPG" _target="_blank">
        <img id="IMG_1747.JPG" src="./IMG_1747.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1749.JPG" _target="_blank">
        <img id="IMG_1749.JPG" src="./IMG_1749.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1750.JPG" _target="_blank">
        <img id="IMG_1750.JPG" src="./IMG_1750.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1751.JPG" _target="_blank">
        <img id="IMG_1751.JPG" src="./IMG_1751.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1754.JPG" _target="_blank">
        <img id="IMG_1754.JPG" src="./IMG_1754.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1755.JPG" _target="_blank">
        <img id="IMG_1755.JPG" src="./IMG_1755.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1756.JPG" _target="_blank">
        <img id="IMG_1756.JPG" src="./IMG_1756.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1758.JPG" _target="_blank">
        <img id="IMG_1758.JPG" src="./IMG_1758.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1759.JPG" _target="_blank">
        <img id="IMG_1759.JPG" src="./IMG_1759.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1761.JPG" _target="_blank">
        <img id="IMG_1761.JPG" src="./IMG_1761.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1764.JPG" _target="_blank">
        <img id="IMG_1764.JPG" src="./IMG_1764.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1769.JPG" _target="_blank">
        <img id="IMG_1769.JPG" src="./IMG_1769.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1773.JPG" _target="_blank">
        <img id="IMG_1773.JPG" src="./IMG_1773.JPG" width="100%" />
    </a>
    <span class="thumb-label">13. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1775.JPG" _target="_blank">
        <img id="IMG_1775.JPG" src="./IMG_1775.JPG" width="100%" />
    </a>
    <span class="thumb-label">14. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1778.JPG" _target="_blank">
        <img id="IMG_1778.JPG" src="./IMG_1778.JPG" width="100%" />
    </a>
    <span class="thumb-label">15. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1780.JPG" _target="_blank">
        <img id="IMG_1780.JPG" src="./IMG_1780.JPG" width="100%" />
    </a>
    <span class="thumb-label">16. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

