### Tools you'll need 

 You'll need:
- A power drill or impact driver
- A hex driver
- A set of metric hex wrenches
- An 9/32" Drill Bit
- An 1/8" Drill Bit
- A 4mm .70 Tap
- A CNC Gantry with more than 1-2" of Z travel. I'm using a LowRider2. Tutorial on that coming soon. 