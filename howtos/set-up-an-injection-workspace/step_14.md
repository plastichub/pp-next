### Get your machines 

 Cool, now you have a space it’s time to get hold of your Injection and Shredder machine. There are three ways to do that:

1 Build them yourself following our tutorials
👉 tiny.cc/build-injection
👉 tiny.cc/build-shredder

2 Buy them on the Bazar.
👉 bazar.preciousplastic.com

3 Find a Machine Shop near you on the map that can build them for you.
👉 community.preciousplastic.com/map