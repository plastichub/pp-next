### Bend the pipe  

 To get the plastic flowing smoothly into into its new flat shape, we'll need to taper the pipe from a circle into an oval. We do this by putting the edge of the nozzle into the vise and slowly apply pressure. It's easy to over-do it here, so take it slow and check regularly. Try to avoid bendnig on the seam if your pipe has one and it can help to put a nut over or some wood inside the thread area to avoid deformation of thread. 
