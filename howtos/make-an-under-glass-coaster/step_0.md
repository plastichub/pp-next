### Cut the squares 

 Start by cutting perfect equal squares 10 cm apart. The more precise they are, the more beautiful your coasters will. 

Make sure that you have a plastic sheet thick enough to be able to hollow out the circle that will accommodate the food of the glass. You need about 5 mm so use a sheet of at least 1cm thick