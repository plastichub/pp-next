### Demould 

 You'll have to wait a while for everything to cool down at room temperature. The plaster will insulate the heat so this could take up to 12 hours depending on the size of your product.

Demould your product and be careful to preserve your mould so you can use it again!