---
image: "/howtos/upgrade-your-extrusion-screw/screw-3.jpg"
title: "Upgrade your Extrusion screw"
tagline: ""
description: "Switching to an industrial screw will open up many new exciting ways of working with the extrusion machine."
keywords: "extrusion,hack"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "hack"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get a screw</h4>
<div class="step-text">
  V2 of the extrusion machine uses a wood auger to transport the plastic from one side to the other of the barrel while being heated up and melted. It works fine for most applications but if you want to push extrusion to the next level you need a screw that builds up pressure while transporting the plastic.<br />
  <br />
  You need an industrial screw.<br />
  <br />
  You can download the technical drawing (go to top of this page) if you&#39;re adventurous enough to make it yourself, or you can buy it on the Precious Plastic Bazar.<br />
  <br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/upgrade-your-extrusion-screw/screw-2.jpg">
        <img class="step-image" src="/howtos/upgrade-your-extrusion-screw/screw-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Install the screw</h4>
<div class="step-text">
  The new screw should fit straight into your existing barrel if your machine has been built according to the drawings in our Download kit. You can also have a look on the Bazar for other sizes if you should need.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/upgrade-your-extrusion-screw/screw-1.jpg">
        <img class="step-image" src="/howtos/upgrade-your-extrusion-screw/screw-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Reinforce your machine</h4>
<div class="step-text">
  You can simply swap this screw for your existing one, but make sure you reinforce your machine. This is very important as this screw brings way more pressure on your machine, particularly on the barrel-holder, the motor and the bottom plate.<br />
  <br />
  Have a look at our ‘Beam making’ Video to learn more about this and see how you can make beams with this upgraded screw.<br />
  <br />
  👉 tiny.cc/extrude-beams<br />
  <br />
  Have fun extruding!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/upgrade-your-extrusion-screw/extrusion-screw-1.jpg">
        <img class="step-image" src="/howtos/upgrade-your-extrusion-screw/extrusion-screw-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>