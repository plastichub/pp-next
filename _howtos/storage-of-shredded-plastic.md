---
image: "/howtos/storage-of-shredded-plastic/storeplastic-lids-cover.jpg"
title: "Storage of shredded plastic"
tagline: ""
description: "There are various options for how to store shredded plastic. If you have large volumes of material, then most likely you will be suitable for industrial containers. But if you have a small workshop, then in my opinion it’s very convenient to store plastic in 19 liter water bottles.<br /><br /><br />Zip is here: &lt;a href=&quot;http://tiny.cc/679fiz&quot;&gt;http://tiny.cc/679fiz&lt;/a&gt; (Universe site allow only small zips up to 5Mb)"
keywords: "hack,collection,sorting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
- "collection"
- "sorting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/copypastestd">copypastestd</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Shredding</h4>
<div class="step-text">
  Here you have several options: <br />
  1) to grind plastic manually with a hand tool;<br />
  2) to grind plastic with a shredder or crusher;<br />
  3) to buy already crushed or granulated raw materials.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Industrial container.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Industrial container.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Pellet.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Pellet.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Tools.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Tools.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - 19L bottles</h4>
<div class="step-text">
  A 19 liter water bottle can be a good container for storing material.<br />
  Of the pluses, it can be noted: it is lightweight, it is transparent, available worldwide.<br />
  One of the disadvantages is that you may encounter a problem of static electricity.<br />
  <br />
  These bottles can be made of PET or PC (Polycarbonate). PC bottles are preferable, as they usually have a handle on the body.<br />
  There are also special racks for storing bottles, they are very simple in design (but this is the topic for the next how-to)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Static electricity.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Static electricity.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/PET & PC.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/PET & PC.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Bottle storage.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Bottle storage.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - How to use</h4>
<div class="step-text">
  In order to pour the material into the bottle, you will need a funnel (which can also be made from an extra bottle). To prevent the ingress of dust, the neck of the bottle should be covered. The easiest (not the best) way is simply to seal the bottle neck with masking tape. Yes, this will protect against dust, but the material inside will not be ventilated (and may appear mildew if there is moisture inside). It is also inconvenient to open and close the bottle, you will have to change the tape often.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Shred.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Shred.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Bottles_2.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Bottles_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Bottles.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Bottles.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Bottle lids</h4>
<div class="step-text">
  To solve this problem, I suggest using special covers with ventilation holes. They protect the material from dust, but the plastic inside can be ventilated. On the top lid there is a plastic type designation. Below the inner surface, the outer diameter is indicated.<br />
  Yes, this is another problem, even if we take bottles from the same manufacturer, from batch to batch the diameter of the neck will differ (in my practice, this value varies from 45.75 mm to 47.75 mm).<br />
  <br />
  In the attachment you will find link for files for different types of plastic and different sizes (in formats: f3d and step).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Lid.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Lid.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Lid_2.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Lid_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Lid_3.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Lid_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - How it's look like</h4>
<div class="step-text">
  The lid must be printed upside down, without supports. <br />
  Tip: you can use different filament colors for each type of plastic (in my case, gray for HDPE, black for PS, orange for PP) this is convenient for finding the right plastic on the shelf.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Prusa.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Prusa.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Lids.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Lids.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/storage-of-shredded-plastic/Bottles_full.jpg">
        <img class="step-image" src="/howtos/storage-of-shredded-plastic/Bottles_full.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>