### Set up an Extrusion Workspace 


In this How-to we’re going to guide you through all the steps to set up an Extrusion Workspace. Learn about plastic, how to find a space, get the Extrusion machine, find customers and connect to the Precious Plastic Universe. 

Download files:
👉 https://cutt.ly/starterkit-extrusion 👈

Step 1-3: Intro
Step 4-9: Learn
Step 10-19: Set up
Step 20-25: Run
Step 26-29: Share