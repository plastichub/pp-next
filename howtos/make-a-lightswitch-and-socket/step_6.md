### Mount it and love it! 

 Your parts are ready!

Keep in mind that this product is in direct contact with high voltage electricity, so don’t try to mount it yourself unless you’re qualified. 

Should one of the parts break, you can simply shred and melt it again, or bring it to another Precious Plastic workspace where they can recycle the plastic. :)