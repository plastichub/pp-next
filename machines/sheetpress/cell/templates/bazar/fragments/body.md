## Features
- integrated fan for fumes
- In case you take the dual sheetpress, up to 1.20x2.40m sheets, we supply you with a tested hydraulic system

<div style="clear:both">
    ${product_overview_drawings}
    <div style="padding: 16px; display: inline-block">
        ${product_resources}
    </div>
</div>
