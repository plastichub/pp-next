---
image: /_howto/extrusion/hopper_mount/IMG_2032.JPG
category: "extrusion"
title: "Hopper Mount"
tagline: ""
description: ""
usedin:
  - lydia-v4
  - lydia
  - zoe
  - extrusion-pro
  
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2020.JPG" _target="_blank">
        <img id="IMG_2020.JPG" src="./IMG_2020.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2021.JPG" _target="_blank">
        <img id="IMG_2021.JPG" src="./IMG_2021.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2023.JPG" _target="_blank">
        <img id="IMG_2023.JPG" src="./IMG_2023.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2024.JPG" _target="_blank">
        <img id="IMG_2024.JPG" src="./IMG_2024.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2025.JPG" _target="_blank">
        <img id="IMG_2025.JPG" src="./IMG_2025.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2026.JPG" _target="_blank">
        <img id="IMG_2026.JPG" src="./IMG_2026.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2027.JPG" _target="_blank">
        <img id="IMG_2027.JPG" src="./IMG_2027.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2029.JPG" _target="_blank">
        <img id="IMG_2029.JPG" src="./IMG_2029.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2032.JPG" _target="_blank">
        <img id="IMG_2032.JPG" src="./IMG_2032.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

