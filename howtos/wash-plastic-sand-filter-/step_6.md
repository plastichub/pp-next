### Seal the lid 

 The lid of the barrel usually comes with a rubber sealant, but in our experiences it was not good enough and broke easily. Proper sealing can be achieved by filling up the gap where the rubber was sitting with silicone or another flexible sealant of good quality.

In this case, Sikaflex is used. Apply it roughly 5mm thick and flatten with the head of a screwdriver or similar object. Make sure to let it dry for the appropriate amount of time (mentioned in the data sheet of the sealant).