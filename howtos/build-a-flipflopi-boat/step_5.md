### The big parts 

 The production of the BIG PARTS like keel and ribs was one of the biggest challenges. We managed to work with what we could get, with a lot of space for improvements!  

We collaborated with the closest plastic recycling manufacturer Regeneration Africa in Malindi, where they usually produce fencing posts and tiles.   

Here is an overview of their process. (second image). We made over 30 metal moulds for different boat parts which were filled this way, using HDPE which is the most common and easiest type to collect separately (after PET).  

Sam and his team were a huge help to explore and figure out different processes and materials!