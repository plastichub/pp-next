### Wanna get money? 

 Be aware that you are doing something special that only a few can offer. And if you're asked to do a workshop, then think about whether you want to be paid for it. How much money you take is up to you. It depends on what circumstances you have to deal with.

Here are some cost items listed:
- Transport
- Use of the machine, moulds and other tools
- Fee for your team
- Placing cost
- Coordination
- Overhead for your organisation 
