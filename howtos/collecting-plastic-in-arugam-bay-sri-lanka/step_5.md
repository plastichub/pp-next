### Provide alternatives 

 Our water refill system is public and anyone can refill for free. In the end we don’t really want to collect all these bottles! A tourist destination without any plastic bottles would be much better than a tourist destination that has a good collection for a vast amount of PET bottles