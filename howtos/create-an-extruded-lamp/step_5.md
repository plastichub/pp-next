### Extrusion time - Make the pattern 

 Once the base is nicely laid out we start creating the actual lamp’s pattern by moving the mould up and down like in the pictures below. You can get creative with the patterns created but generally you will want to create enough of a structure for the lamp to support itself.

When you’re happy with the pattern you can cut the end of the extrusion line and glue it nicely to the rest of the product.