<?php
namespace SR;

require __DIR__ . './../vendor/autoload.php';

use DeviceDetector\DeviceDetector;
use GuzzleHttp\Client;
use SR\SessionUtils;
use SR\Signal;
use Vectorface\Whip\Whip;

function json_error()
{
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            return ' - No errors';
            break;
        case JSON_ERROR_DEPTH:
            return ' - Maximum stack depth exceeded';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            return ' - Underflow or the modes mismatch';
            break;
        case JSON_ERROR_CTRL_CHAR:
            return ' - Unexpected control character found';
            break;
        case JSON_ERROR_SYNTAX:
            return ' - Syntax error, malformed JSON';
            break;
        case JSON_ERROR_UTF8:
            return ' - Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
        default:
            return ' - Unknown error';
            break;
    }
}
class SignalUtils
{
    public static function fromEvent($body)
    {
        $parsed = json_decode($body, true);
        return new Signal($parsed['v'], $parsed['s'], $parsed['e'], -1, $parsed['evts'], 'e');
    }

    public static function fromBin($params, $body)
    {
        $parsed = $body;
        return new Signal(1, intval($params['ms']), intval($params['me']), intval($params['sn']), base64_encode($parsed), 'b');
    }

    public static function toSignal($params, $body)
    {
        $type = $params['t'];
        $signal = null;
        if ($type === 'e') {
            $signal = self::fromEvent($body);
        } elseif ($type === 'b') {
            $signal = self::fromBin($params, $body);
        }
        return $signal;
    }
    public static function getLocation($ip)
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://api.ipstack.com/',
            // You can set any number of default request options.
            'timeout' => 4.0,
        ]);
        $API_KEY = '2b39f9a2e175cd8db8a9e512813f3576';

        $response = $client->request('GET', $ip . '?access_key=' . $API_KEY);
        $body = $response->getBody();
        $ret = $body->getContents();

        // d($ret, 'geo');
        return json_decode($ret, true);

    }
    public static function getAgent()
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT']; // change this to the useragent you want to parse

        $dd = new DeviceDetector($userAgent);

        $dd->parse();

        if ($dd->isBot()) {
            // handle bots,spiders,crawlers,...
            // $botInfo = $dd->getBot();
        } else {
            $client = $dd->getClient(); // holds information about browser, feed reader, media player, ...
            $osInfo = $dd->getOs();
            $device = $dd->getDeviceName();
            $brand = $dd->getBrandName();
            $model = $dd->getModel();
            return [
                "family" => $client['name'],
                'mobile' => $client['type'] === 'desktop' ? false : true,
                'os' => [
                    'family' => $osInfo['name'],
                ],
            ];
        }
    }
    public static function saveSignal(
        String $sid,
        String $referer,
        String $tags,
        String $visit,
        Signal $signal) {

        $session = SessionUtils::getSession($sid);
        if (!$signal) {
            return $session;
        }

        error_log('save ' . $signal->type . ' in ' . strlen($session['events']) . ' |');

        if (!$tags) {
            $tags = '{}';
        } else {
            $tags = urldecode($tags);
        }

        $tags = json_decode($tags, true);

        $sessionTags = $session['tags'];
        if (!$sessionTags) {
            $sessionTags = [];
        } else {
            $sessionTags = json_decode($sessionTags, true);
        }

        if (!ArrayUtils::get($sessionTags, 'ip')) {
            $whip = new Whip();
            $sessionTags['ip'] = $whip->getValidIpAddress();
        }

        if (!ArrayUtils::get($sessionTags, 'agent')) {
            $sessionTags['agent'] = self::getAgent() ?: [];
        }
/*
if (function_exists('geoip_detect2_get_info_from_ip')) {
error_log('d ' . json_encode(geoip_detect2_get_info_from_ip('79.155.246.86')));
}else{
error_log('no');
}
 */
        if (!ArrayUtils::get($sessionTags, 'location')) {
            $sessionTags['location'] = self::getLocation('79.155.246.86' /*$sessionTags['ip']*/) ?: [];
        }

        $events = $session['events'];
        if (!$events || $events == 'null' || strlen($events) === 0) {
            $events = [];
            error_log('no events ' . $sid);
        }
        if (is_string($events)) {
            $evts = json_decode($events, true);
            if ($evts != null && is_array($evts)) {
                $evts = array_values($evts);
                if ($evts) {
                    $events = $evts;
                }
            } else {
                error_log(':error ' . json_error() . ' : ' . $events);
                return $session;
            }
        }

        //if (!$events) {
        //    error_log('invalid ');
        //    $events = [];
        //}

        if ($signal->type === 'e') {
            //error_log('push : e');
        }
        if ($signal->type === 'b') {
            /*if (!json_encode($events)) {
            error_log('invalid events!');
            }*/
            //error_log('add binary! ');
        }

        array_push($events, $signal);

        // error_log('push s' . count($events));

        // d($events, 'signal');
        $events = json_encode($events);
        // d($events, 'out');

        $tags = array_merge($tags, $sessionTags);
        // d($tags, 'tags');

        $DB = \WeDevs\ORM\Eloquent\Database::instance();
        //\WeDevs\ORM\Eloquent\Database::transaction(function () {
        // $DB->beginTransaction();
        $session['events'] = $events;
        $ret = $session->update([
            'session' => $session['session'],
            'referer' => $referer,
            'visit' => $visit,
            'tags' => $tags,
            'status' => 'started',
            'events' => $events,
            'tags' => json_encode($tags),
        ]);
        $session->save();
        //$DB->commit();
        //}, 2);
        error_log('oevents ' . count($events) . ' saved: ' . $ret);
        d($session, 'session');
        return $session;
    }
}
