### Round off corners 

 Use nail scissors to round off corners at the bottom of the PET shield. If you don't have nail scissors you can also use normal scissors.