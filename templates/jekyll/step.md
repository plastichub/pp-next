<h4 class="step-title" id="step-${step_number}">Step  ${step_number} - ${title}</h4>

<div class="step-text">
    ${text}
</div>
<br/>
<div class="step-images">
    ${images}
</div>
<br/>
