### Shreds size 

 Precious Plastic shredded plastic comes in three flake sizes: small, medium and large. Each size has a proper application (for example sheetpress can be ok with big flakes while extrusion needs small).

You can create different sizes by using a sieve or shredding multiple times. More info here. 