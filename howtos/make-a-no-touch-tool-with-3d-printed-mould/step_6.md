### Bonus: Speed up the process 

 To meet demand or lower the price point of the tools, there are a number of ways you can speed up the process:

1. Heat the plastic before putting into the injector like Qi-Tech 
https://community.preciousplastic.com/how-to/rapid-fire-method-for-injection--300-face-shields-a-day
You can also use an oven to pre-heat the shreds rather than an extruder. 
2. Use multiple moulds.
This enables you to have some products cooling whilst you inject others. 
3. Fill the injection tube for multiple shots. 
If you can load your injector with multiple 40g shots of plastic, this will enable you to inject multiple shots at a time. 