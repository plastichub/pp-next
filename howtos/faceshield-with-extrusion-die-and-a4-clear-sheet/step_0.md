### Make a nozzle to attach to the extruder. 

 The nozzle needs to have a relatively large entry and a tapering end.
The best is to have some bars to cool down a bit the plastic and funnel it towards the exit.
To activate the nozzle we heat it up with hot air gun
The outside plates are only 1mm apart 
You can also do it without nozzle but it is even more complicated to control the thickness and elasticity of the future band
https://www.youtube.com/watch?v=BTiQqPFE9vs