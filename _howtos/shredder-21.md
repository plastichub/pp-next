---
image: "/howtos/shredder-21/shredder-2.jpg"
title: "Shredder 2.1"
tagline: ""
description: "We’ve gathered a lot of feedback on the shredder from our community and made a small upgrade which makes it a bit easier to build, swap the mesh and chop plastic."
keywords: "hack,shredder"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
- "shredder"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  The laser cut files are updated:<br />
  <br />
  We got rid of the 4 brackets on the side, making the building process easier as you don’t need to cut the angle profiles and drill holes (if you want you can still place them, they are backwards compatible).<br />
  <br />
  We also improved the design of the mesh, which is more robust and can be taken out from the side. Simply slide out the two metal rods and the mesh pops out underneath (same process to put it back in).<br />
  <br />
  And lastly, we&#39;ve also improved the hopper making it safer and more efficient, you can now press the plastic down while the machine is running without chopping of your fingers :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/shredder-21/shredder-1.jpg">
        <img class="step-image" src="/howtos/shredder-21/shredder-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Laser cut and assemble</h4>
<div class="step-text">
  Download and send the cutting files to your local lasercutter and get the parts cut. <br />
  This version is similar to the V2 but some components have been simplified and combined with laser cutting.<br />
  <br />
  Then assemble the laser parts, making sure the alignment is right then weld the parts together.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/shredder-21/shredder-3.jpg">
        <img class="step-image" src="/howtos/shredder-21/shredder-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Build hopper and plunger</h4>
<div class="step-text">
  As you will find in the technical drawings, there is a hopper and a plunging system that can be used to make the machine a bit safer.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/shredder-21/shredder-6.jpg">
        <img class="step-image" src="/howtos/shredder-21/shredder-6.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/shredder-21/shredder-5.jpg">
        <img class="step-image" src="/howtos/shredder-21/shredder-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>