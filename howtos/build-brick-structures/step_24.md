### Adding roof sheets 

 Next, you will need to add the roofing sheets to complete the structure. In this how-to we used a recycled roofing sheet that is a little thicker than galvanized, but most other sheeting materials will work (But its best to be sustainable in your choice!). 

To do this we lay the sheets overlapping them and making sure they are straight. 
