---
image: /_howto/controlbox/wiring/alternative_0/IMG_2326.JPG
category: "zoe"
title: "Alternative 0"
tagline: ""
description: ""
usedin:
  - hal
  - zoe
  - lydia-v4
  - extrusion-pro
  - shredder-pro
title: "HAL 9000 Wiring - Alternative"

---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2321.JPG" _target="_blank">
        <img id="IMG_2321.JPG" src="./IMG_2321.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2325.JPG" _target="_blank">
        <img id="IMG_2325.JPG" src="./IMG_2325.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2326.JPG" _target="_blank">
        <img id="IMG_2326.JPG" src="./IMG_2326.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

