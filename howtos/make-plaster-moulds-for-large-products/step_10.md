### That's it! 

 A little time consuming but a nice low tech mould making technique. It will never replace machined moulds, but can definitely be useful for prototyping larger, more organic shapes.
Here’s our final product - a stool made from old polypropylene chairs. But the possibilities are endless.

One thing that you could change is the contrast between the plastics you feed into the extruder. In this case, the colour choices were quite similar so there isn’t much contrast. This is something that can definitely be controlled depending on the look you are going for.