### Choose your categories  

 The first step of this how-to is, choosing a couple of categories that you want to sort the plastic in. This can be only the ones that you will recycle yourself or the most common ones PETE, HDPE, PVC, LDPE, PP, PS, and others. You can even add a section with “to dirty” or “to mixed”. 