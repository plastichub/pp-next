### Make the mixture 

 Get your glass jar (here is where we are going to contain the glue so make sure to use a jar that allows you to access the glue comfortably afterwards)

Put in D-Limonene (approx 25ml)
Add EPS (Styrofoam) until it forms a thick translucent glue
The right amount is 16g for 25ml of D-limonene (Density: 0.8411 g/cm3). That makes a ratio in g of 0.74 (eps/d-limonene). 

Once the EPS is mostly dissolved, immediately close the jar. D-Limonene is a solvent that evaporates in contact with the air, so make sure it’s airtight. 

Finally let the mixture rest 24h to get a consistent glue. D-limonene by itself takes a long time to fully dissolve EPS but this research explains how to shorten the drying time and at the same time increase its viscosity diluting the solution with water.

The resulting optimal glue contains 2g polystyrene, 2ml water, 0.2g lecithin and 4ml D-Limonene. 