### Make the nozzle flange: 

 Get the steel disc (no. 3) and turn a hole in the center with diameter to fit in tightly one half of the steel pipe nipple (part no. 7). (See drawings page 4)
