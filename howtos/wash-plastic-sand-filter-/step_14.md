### Complete setup 

 Nice one, you're ready to pre-wash :)

To build the complete setup with sand filter and washing machine, have a look at our How-To's here: 

Wash plastic: Pre-wash
👉 tiny.cc/wash-plastic-sandfilter

Wash plastic: Washing machine
👉 tiny.cc/plastic-washingmachine