### Make it ready to extrude 

 Alright, your mould is basically ready to go. Bolt it all together and have a look inside to make sure that there are no large gaps.
Then you can start shooting that plastic inside!

Tip for extruding: Stop your plastic before it erupts out the end. If there is too much plastic overflowing, it will be hard to demould later.
