
$(document).ready(function () {

    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        history.pushState(null, null, e.target.hash);
        if (e.target.hash === '#discussion') {
            var ifr = document.getElementById('discourse-embed-frame');
            ifr.src = ifr.src;
            $(ifr).height('600px');
        }
    })

    // Add anchors for images
    $('.stretched__content').find('img').each(function () {
        var id = $(this).attr('id');
        if (id) {
            var anchor = document.createElement("a");
            anchor.className = 'header-link';
            anchor.href = '#' + id;
            anchor.innerHTML = '<span class=\"sr-only\">Permalink</span><i class=\"fa fa-link\"></i>';
            anchor.title = "Permalink";
            $(this).parent().parent().append(anchor);
        }
    });

    // Add anchors for headings
    $('.stretched__content').find('h1, h2, h3, h4, h5, h6').each(function () {
        var id = $(this).attr('id');
        if (id) {
            var anchor = document.createElement("a");
            anchor.className = 'header-link';
            anchor.href = '#' + id;
            anchor.innerHTML = '<span class=\"sr-only\">Permalink</span><i class=\"fas fa-link\"></i>';
            anchor.title = "Permalink";
            $(this).append(anchor);
        }
    });

    [].forEach.call(document.getElementsByClassName("iframe-lightbox-link"), function (el) {
        el.lightbox = new IframeLightbox(el);
    });

    if (location.search.indexOf('admin=true') !== -1) {
        $('.fragment').each(function () {
            var context = $(this).attr('context');
            var file = $(this).attr('file');
            var context = $(this).attr('context');
            var prefix = $(this).attr('prefix');
            if (context) {

                $(this).attr("data-toggle", "tooltip");
                $(this).attr("data-trigger", "focus");
                $(this).attr("data-placement", "bottom")
                $(this).attr("data-style", "mypops");
                $(this).popover({
                    html: true,
                    content: function () {
                        //return $('#popover-content').html();
                        return `<div id="popover-content" class="hide">
                        <p>Edit this fragment <b>${context}</b> :: /${prefix} <br/> ${file} </p>
                        <ul class="popovermenu">
                            <li><a href="#">Edit in VSCode</a>
                
                            </li>
                            <li><a href="#">Edit on Github</a>
                
                            </li>
                            <li><a href="#">Edit in IDE</a>
                
                            </li>
                        </ul>
                    </div>`;
                    }
                });
                $(this).hover(function () { // Mouse over
                    $(this).addClass("editor-highlight");
                }, function () { // Mouse out
                    $(this).removeClass("editor-highlight");
                })
            }

        });
    }

});
