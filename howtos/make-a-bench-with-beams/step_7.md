### Drill thread rod holes 

 In this step we are going to be drilling the 6.5mm holes that will house the 6mm threaded rod. Begin by marking the midpoint and ‘true’ angle of Part A through part B. Follow this line around the corner to find the centre point and mark it. The lines on the side can be used to position the piece under the drill bit with a vice clamp. 

Take care to align drill and marker lines as closely as possible and drill through. We can then use this hole to mark the point on Part A for drilling. As before, position the piece under the drill press, align the bit with the edge of the part and drill.