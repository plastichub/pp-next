---
image: "/howtos/make-a-quick-release-opening-system-for-injection-moulds/quickrelease.jpg"
title: "Make a quick release opening system for injection moulds"
tagline: ""
description: "Injecting is a slower process compare to the other machines, generally it takes from 10 to 15 min to inject a product. That&#39;s why we&#39;ve been looking for different alternatives to make this process as fast as possible, even for very simple steps like opening and closing. When you inject 3-5 times is not that bad to screw in and out 4-6 bolts, but when you need to do the same step 50 times, to save a couple of seconds each time, it&#39;s a lot!"
keywords: "mould,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  Before you start, gather the materials and tools you need to make the quick release system:<br />
  <br />
  Materials: 4 bike skewers (standard thread thickness: 5mm).<br />
  You can use more depending on the type of mould you have, i would recommend you to use at least 4. <br />
  <br />
  Tools: ruler, M5 die, safety glasses, marker.<br />
  And for cutting and sanding, you can either use. a hand saw, file adna sanding paper or a grinder with cutting and sanding disc
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/overview material quick release.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/overview material quick release.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Prepare your clamps</h4>
<div class="step-text">
  The principle of this system is that you press the mould parts together with self-made clamps.<br />
  <br />
  Let’s start with adapting the quick release wheel bike clamps. <br />
  <br />
  Measure the length of the threaded part, then put the clamp through your mould and apply the measured length with a marker. This is how much you need to cut off to fit the mould thickness, so you can tighten it up properly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4667.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4667.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4676.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4676.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/2.2 Prepare your clamps.jpg">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/2.2 Prepare your clamps.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Cut the clamps</h4>
<div class="step-text">
  Cut your clamp to your needed length (with a hand saw or angle grinder) and then soften the sharp edges.<br />
  <br />
  This is very important so to screw in the nut easily without damaging its thread.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4693.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4693.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4696.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4696.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4701.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4701.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Remake the thread</h4>
<div class="step-text">
  Take your M5 die to re-make the thread. It should be the same length as it was previously (measured in Step 3). You can even make it slightly longer (max. 5 mm), to ensure that the mould will close very tightly.<br />
  <br />
  Cool, so now you have your clamps ready.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4715.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4715.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4722.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF4722.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_7149.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_7149.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Slots in the mould</h4>
<div class="step-text">
  To make it easier to attach and detach the clamps from the mould, you can make slots in your mould.<br />
  <br />
  You can include them in the cnc/laser cut file of your mould or you can also cut them yourself with an angle grinder.<br />
  <br />
  The slots should be around 6 mm wide to make it easy to slide the clamp in and out. (If for some reason it becomes wider, use bigger washers in between to fix the clamps).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_6485.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_6485.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Close and open the mould</h4>
<div class="step-text">
  That’s it, now you have all the parts you need.<br />
  <br />
  To close the mould, slide the clamps into the slots, and screw them in until you can tighten them enough to easily tighten and loosen with the clamps.<br />
  <br />
  Ready to inject!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_7140.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_7140.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_7135.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_7135.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_7124.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/IMG_7124.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Quick release system for the injection machine</h4>
<div class="step-text">
  To make the process even faster, you can also add a quick release system for the machine!<br />
  <br />
  Learn how to make the quick release system:<br />
  👉 tiny.cc/quickrelease-extruder<br />
  <br />
  Happy injecting! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF0337.JPG">
        <img class="step-image" src="/howtos/make-a-quick-release-opening-system-for-injection-moulds/_DSF0337.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>