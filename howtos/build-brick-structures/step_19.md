### Framing & covering the brick teeth (top) 

 We continue this method around the top, covering the exposed elements of the bricks, and aligning the timber clamping structure perfectly with the bricks. This work requires accurate measurements to look good, so take your time here. 
