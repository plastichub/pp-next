# Overview

This is a small version of a PET printer and utilizes as 200$ [AnyCubic i3 Mega](https://www.youtube.com/watch?v=iYGO8ipAp9M) as base.

PET flakes have to be created from water bottles, using the v3 or v4 shredder or purchased from the next PET recycling facility (100Kg=12Euro).
The flakes have to be grinded to dust as next, below 1mm particles with the [new grinder module/addon](https://precious-plastic.org/home/library/components/grinder/).

As soon you have the grinded PET, you can printed with a special print head.

## Components

[Pellet Screw](https://www.aliexpress.com/item/32967974777.html?spm=a2g0s.9042311.0.0.55374c4dxAMR78)

<img width="300px" src="./MiniPETPrinter/Screw.jpg"/>

[Pellet Extruder Motor](https://www.aliexpress.com/item/4000036126355.html?spm=a2g0s.9042311.0.0.55374c4dxAMR78)

<img width="300px" src="MiniPETPrinter/Nema17Geared.jpg"/>

or as [kit from Mahor](https://mahor.xyz/) but 4 times the price.

<img width="500px" src="MiniPETPrinter/mahor.jpg"/>

## TODOs

- complete print head with the AliExpress parts : in progress (PlasticHub)
- complate Mahor print head variant : in progress (PlasticHub)
- figure printer parameters : in progress (PlasticHub)

## Internal References

### AnyCubic i3 Mega Printer

- [support files and addons](./MiniPETPrinter/AnyCubicMega3/Readme.md)
