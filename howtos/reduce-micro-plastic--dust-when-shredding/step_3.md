### The Funnel 

 You'll need an airtight funnel to capture the shredded plastic as it falls from the sieve into the vacuum hose.
You can use something purpose built, 3D printed, or a D Square grate and plumbing pipe.

Something like this certainly does the job: www.timbecon.com.au/dust-extractor-hood-large-big-gulp

