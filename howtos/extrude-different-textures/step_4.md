### Explore textures extrusion speed 

 The speed can also affect the outcome: slower speed, deeper texture, faster less texture and the pattern is slightly different also. We have shown you an example of how we made the texture for the bubblegum bench, this is just one way of achieving this particular texture. In the pictures you can see different textures achieved by playing with the settings. 
Make sure to check the related how to's to know more about the technique.

Related links:
How to make a bench with beams 👉 tiny.cc/make-beam-bench

