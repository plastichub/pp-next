### Textured beam, material 

 In step 3 - 6 we will talk you through how to embrace the natural textured effect you can achieve when using the extruder machine. We used HDPE / PP for a textured beam to make an outdoor bench, using the same process of extrusion, with the intention of making a textured pattern on the surface, which you can achieve easily with HDPE / PP. The choice of keeping the textured effect which is a natural occurrence from the extrusion process suits outdoor furniture well, therefore we used this to our advantage.

So first things first: Start by shredding your material from medium to small shreds. The easiest way to measure how much material you will need is to test the first mould, and weigh the outcome thats the weight of the material that you will need each time. 

