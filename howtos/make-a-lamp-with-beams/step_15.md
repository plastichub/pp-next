### Assemble 

 Once the 2 components are ready you can stick the LED to the metal bar, letting the metal cable first, and then the light cable passing through the holes. 

Assemble the plate and the beam with the 3mm bolts and put a 5mm screw on the side with no light cable. This will prevent the metal cable from escaping.

Now solder the LED cable to the power controller!
