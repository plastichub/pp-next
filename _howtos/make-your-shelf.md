---
image: "/howtos/make-your-shelf/menor-2.jpg"
title: "Make your shelf"
tagline: ""
description: "Learn to build shelves or small furniture using the extruder and the injection machine."
keywords: "mould,HDPE,extrusion,PP"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "HDPE"
- "extrusion"
- "PP"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/menor-plastic">menor-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Make the beam</h4>
<div class="step-text">
  We will use a square tube 2cm in diameter and 40cm long. You don&#39;t need to screw it in, you can hold it carefully not to burn yourself by hand.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/tamaño.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/tamaño.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/extrusora.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/extrusora.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/viga.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/viga.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cut and drill holes</h4>
<div class="step-text">
  For this model we have used:<br />
  <br />
  - 35.5cm x 3<br />
  - 19,5cm x 2<br />
  - 14cm x 2<br />
  - 18cm x 2<br />
  <br />
  Feel free to make your modifications if you need it bigger or smaller.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/hole.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/hole.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/estructura2.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/estructura2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/estructura.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/estructura.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Assemble the base</h4>
<div class="step-text">
  Using the mold for the injection machine with a square shape and measures of 14cm x 14cm. We will make 8 units that we can easily glue or screw.<br />
  <br />
  If you liked it, you can follow our project and see all our products at <a href="http://www.menorplastic.com<br/>">www.menorplastic.com<br /></a>
  <br />
  Thanks for your support 😊
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/1.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/3.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-your-shelf/base.jpg">
        <img class="step-image" src="/howtos/make-your-shelf/base.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>