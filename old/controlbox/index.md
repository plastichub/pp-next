---
image: /_howto/controlbox/5.JPG
category: "zoe"
title: "Controlbox"
tagline: ""
description: ""
usedin:
  - hal
  - zoe
---



<div prefix="_howto\controlbox" file="header.md" context="md:thumbs" class="fragment">
    <h1 id="shredderextrusioncontroller">Shredder / Extrusion controller</h1>
    <h2 id="features">Features</h2>
    <ul>
        <li>Auto-Reverse. Supports 3 different methods : IR Sensor (recommended), hall sensor and proximity sensor</li>
        <li>Overheat protection</li>
        <li>Error and status feedback via LEDs</li>
        <li>Reset button</li>
        <li>Direction switch</li>
        <li>Hopper sensors</li>
        <li>Emergency switch</li>
        <li>Semi automatic extrusion control</li>
    </ul>
    <h2 id="behaviours">Behaviours</h2>
    <ul>
        <li>If jamming occurred 3 times, it stops all and goes in fatal mode, indicated by red blinking LED. In this case you push reset and/or switch to stop and then again forward after un-jamming the shredder or extrusion</li>
        <li>If power comes back whilst direction is still set to forward, it waits til the user switches to stop and explicit switches to forward again</li>
        <li>If hopper sensors are installed, the motor will stop when opening the hopper</li>
        <li>Uninterrupted shredding will stop after one hour automatically. In later releases this time will be exteneded according to measured activity via HAL and/or speed sensor. That's a safety feature in case the operator got a heart attack, etc…</li>
        <li>If set to extrusion mode, the motor won't start before the barrel didn't heat up for at least some time.</li>
    </ul>
    <p><strong>Front panel</strong></p>
    <p>from top to bottom, left to right</p>
    <ol>
        <li>Emergency switch <a href="">Amazon</a> | <a href="">Specs</a></li>
    </ol>
    <hr />
    <ol start="2">
        <li>
            <p>Power switch - Shredder <a href="">Amazon</a> | <a href="">Specs</a></p>
        </li>
        <li>
            <p>Direction switch <a href="">Amazon</a> | <a href="">Specs</a></p>
        </li>
        <li>
            <p>Power switch - Extruder <a href="">Amazon</a> | <a href="">Specs</a></p>
        </li>
        <li>
            <p>Auto-Reverse mode switch <a href="">Amazon</a> | <a href="">Specs</a></p>
        </li>
    </ol>
    <hr />
    <ol start="6">
        <li>
            <p>Status LED - Error <a href="">Amazon</a> | <a href="">Specs</a></p>
        </li>
        <li>
            <p>Status LED - Ok <a href="">Amazon</a> | <a href="">Specs</a></p>
        </li>
        <li>
            <p>Momentary Switch (Reset &amp; Extrusion semi auto control) <a href="">Amazon</a> | <a href="">Specs</a></p>
        </li>
    </ol>
    <hr />
    <ol start="9">
        <li>VFD Control Panel <a href="">Amazon</a> | <a href="">Specs</a></li>
    </ol>
    <hr />
    <ol start="10">
        <li>Temperature controller <a href="">Amazon</a> | <a href="">Specs</a></li>
    </ol>
</div>

<div class="thumbs"><div class="thumb">
    <a href="./1.JPG" _target="_blank">
        <img id="1.JPG" src="./1.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./2.JPG" _target="_blank">
        <img id="2.JPG" src="./2.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./3.JPG" _target="_blank">
        <img id="3.JPG" src="./3.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./4.JPG" _target="_blank">
        <img id="4.JPG" src="./4.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./5.JPG" _target="_blank">
        <img id="5.JPG" src="./5.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

