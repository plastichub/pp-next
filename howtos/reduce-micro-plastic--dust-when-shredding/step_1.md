### The Bucket 

 This type of bucket is fairly common, and generally pretty easy to source secondhand. Look for something in good condition, with a removable lid.

Additionally, a round bucket tends to work better than a square container for this purpose.

