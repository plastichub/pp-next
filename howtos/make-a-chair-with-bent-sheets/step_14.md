### Backrest connector: Drill the blind holes 

 Put the backrest on its positive mould and drill 12mm holes on the center marks (Step 6).  Start perpendicular to the curvature and slowly move to orthogonal.

The outcome should be a blind hole, going as deep as possible without breaking the inside of the backrest. From the moment when the drill is orthogonal and the entire height of the drill bit is cutting, dive at least 7mm into the plastic to have enough space for the locking pins.
