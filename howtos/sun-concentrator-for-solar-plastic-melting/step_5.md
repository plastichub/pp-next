### Assemble the Sun Collector 

 The sun collector made is made of tempered glass and can provide temperatures above 250°C

To fixe the tube tighten the screws of the 3D printed front holder

The overall size is 660 mm (overall) X 127mm outside diameter. The approximate interior dimensions are: 584 mm X 101mm inner diameter.