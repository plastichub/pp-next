### Sorting 

 Once the collected bottles come into our workspace we spend a considerable amount of time separating the bottle caps from the PET bottles placing them in the appropriate bags and containers. This job is done by one of our staff, sometimes tourists join in and lend us a hand.