### Choose the welding temperature 

 Do some temperature tests.
I use around 250 ° C for HDPE and 270 ° C for PP.
Please use a gas mask for this work! 
See also: https://community.preciousplastic.com/academy/plastic/safety 