### Make your stamp 

 Make a design for your stamp and mill it in metal. We made ours from brass, but I guess other metals would work as well. Tip: make sure to mirror your design