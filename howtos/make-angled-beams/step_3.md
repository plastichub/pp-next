### Cut a mounting plate 

 To connect the mould to the nozzle, we need a mounting plate. If you don’t have a fitting size handy, cut a metal sheet to a size that will fit your beam mould as well as the brackets you prepared in Step 3, and drill the holes for your brackets.

Tip: Use a tube offcut from Step 2 to plan the position of your brackets and their holes. 
