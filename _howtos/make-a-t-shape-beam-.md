---
image: "/howtos/make-a-t-shape-beam-/howto-tbeam-cover.jpg"
title: "Make a T-shape beam"
tagline: ""
description: "Showcasing the process of creating custom shaped moulds for the extrusion machine. In this How-to we will look into making a mould for a T-shaped profile."
keywords: "mould,extrusion"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Tools and equipment required</h4>
<div class="step-text">
  To make the mould you will require:<br />
  <br />
  2x L angle iron 60 x 60mm, 190cm long<br />
  1x 100mm M.S flat 190cm long <br />
  3x 12 x 12mm M.S rods 190cm long <br />
  Sheet metal ( minimum 3mm or more preferable )<br />
  5mm x30mm nut and bolt <br />
  6mm x 30mm nut and bolt<br />
  Threaded pipe or fitting (BSPT size of your nozzle)<br />
  <br />
  Angle Grinder / Metal saw<br />
  Welding machine <br />
  Drill Press / Drilling machine
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 1.1 copy.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 1.1 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Creating a template </h4>
<div class="step-text">
  Before we start work on the mould it will be good to create a template of the beam size we need to help make the mould making process easier. For the template we can use 12mm and 6mm thick MDF or plywood . Find the parts for the template in the image below (all pieces are 190cm longs)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 2.1">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 2.1" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Part I of mould </h4>
<div class="step-text">
  Now that we have the template ready we can start making the mould. Let’s place the part 1 of the template on the table and place the 100mm M.S Flat into the template. Next we can place part 2 aligned to the outer edges. Now place the 2 M.S rods and part 3 of the template into the slots.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 3.1.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 3.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 3.3 copy.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 3.3 copy.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 3.2 copy.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 3.2 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Fix mould part 1</h4>
<div class="step-text">
  Make sure to clamp the metal pieces and template, then drill 6mm holes every 15 cm for the first half and every 25 cm in the second half, on both the square rods. (Note: we add more screws in the first half as more pressure is built up here during extrusion and to prevent leakage) <br />
  <br />
  Now we can remove part 1 and 2 of the template and fasten on the nuts and bolts. Weld the outer joint of the square rods and the flat. (note: weld in shorter stretches and alternate with gaps to prevent the mould from deforming)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 4.1.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 4.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 4.2.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 4.2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 4.3.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 4.3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Mould part 2</h4>
<div class="step-text">
  Now that we have the bottom part we can move on to the second part of the mould. Place part 3 of the template back into the mould then place the 2 L angles on top and the square rod in between them and fasten down with clamps. <br />
  <br />
  Drill 5mm holes every 30cm and fasten with nuts and bolts. Weld the portion between the square rod and the L angle to seal the mould for the whole length. (Again, weld in shorter stretches and alternate with gaps to prevent the mould from deforming)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 5.1.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 5.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 5.2.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 5.2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 5.3 copy.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 5.3 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Weld the mould</h4>
<div class="step-text">
  Now that we have the two parts of the mould, we will use the holes of part 1 of the mould to drill holes into L angles, to be able to attach the two parts of the mould. Once all the holes are drilled, we can fasten them with M6 x 30mm bolts and nuts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 6.1.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 6.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 6.2 copy.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 6.2 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Make mounting brackets and nozzle</h4>
<div class="step-text">
  Cut 3 pieces of sheet metal (minimum 3mm thick) that are to be welded onto the mould so that we can connect the mould to the nozzle. Now we can weld them onto the mould. Now we need a sheet of metal for the nozzle plate. You can take the outer size of the mounting bracket and you will get the size required. Now we need to weld the BSPT onto the nozzle plate (note: place the BSPT in the junction of the T to get the best flow and results for your beam). Drill a hole in the nozzle plate to the size of the BSPT and 4 holes to connect the mounting plate and nozzle plate.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 7.1 copy.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 7.1 copy.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 7.2">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 7.2" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 7.3 copy.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 7.3 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Explore the possibilities!</h4>
<div class="step-text">
  Finally, the mould is ready, well done! Have fun extruding custom shaped and long beams. For best results extrude with a hot mould using heating elements or an oven. This technique of mould building opens up a door for many more custom shapes and sizes with regular fabrication.<br />
  <br />
  Related links:<br />
  How to extrude different textures 👉 tiny.cc/extrude-textures<br />
  How to build a shelving system with T beams 👉 tiny.cc/make-shelving-system<br />
  <br />
  Have fun exploring!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Step 8.1.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Step 8.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-t-shape-beam-/Shelf Front On-howto.jpg">
        <img class="step-image" src="/howtos/make-a-t-shape-beam-/Shelf Front On-howto.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>