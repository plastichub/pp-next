### IMPORTANT: Your contact list! 

 You can use our contact list tool as a starting point to gather all your contacts and keep an overview. Keeping this document organised and updated will be super helpful to make the connections between people in your community, so make sure to take care of it!

If you have another good way of gathering and organising the contacts and details, we're happy to learn about it :)

👉 tiny.cc/community-contactlist