### Prepare samples 

 It’s always good to have a variety of material and product samples to inspire people and help them understand the possibilities of plastic recycling. 
You can use your own products, or get some more from the Precious Plastic Bazar to show what other workspaces around the world are doing. Present the products in a beautiful setting.
👉 bazar.preciousplastic.com
