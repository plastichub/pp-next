### Assemble the mould 

 Once you’ve got all the components, it’s time to assemble the mould. Pick the length you prefer for your lamp. Connect the 2 wooden circles with each other using the 6 rods, and tighten all together.