### Get ready  

 Tools needed:
- CNC, tools to cut (cutting plier), drill and welder - Mask with ABEC filter, gloves, glasses
- scale, sanding paper, polishing paste (for metal) - 4 bolts of 8m width, min length 9 cm + nuts
- 2 metal dowel pins (6m example)
- 8m + 6m drill bit
- wrenches
- 2 aluminium blocks 26x26x4cm,
- metal sheet min 15x15x0.5cm
- plumbing connector 1 inch,
- Precious Plastic stamp for your plastic type
Machines needed: Injection machine and a shredder (or shredded plastic)