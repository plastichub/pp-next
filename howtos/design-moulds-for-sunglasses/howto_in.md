### Design moulds for sunglasses 


At Esfèrica we’ve been working to understand how to uphold the value of waste plastics for the past 5 years. Today, our project FOS Barcelona offers eyewear made with local waste and moulds for others to fabricate them in their area, in an effort to enhance distributed design. 

Here we’ll guide you through aspects we learned on our way.

1: What you need
2-5: Design sunglasses
6-10: Design mould