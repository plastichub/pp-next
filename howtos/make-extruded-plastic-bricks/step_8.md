### Putting the sides together 

 Once all 4 sides have been sanded on the inside, place them together as seen in the diagram and weld them together. A clamp may be useful in this process. 
