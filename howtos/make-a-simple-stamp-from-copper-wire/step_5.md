### Get your iron ready 

 We are using a very basic 65W soldering iron and didn’t need to change much. Your iron needs a bolt on the side to keep the copper wire in place. We had already a hole in it to fix the tip, which we just opened a little more and threaded it to M4 for a better grip. Get a short M4 bolt and you are good to go.