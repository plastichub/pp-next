### Plastic flow and mold connection 

 Think about how the plastic will flow inside the mold. The path has to be as short as possible and with enough space for the plastic to flow throughout the entire mold.
For the entrance, I usually drill a ¼ inch hole with a maximum of 10mm height. The location should be in the center, so that the plastic can be distributed equally in each direction inside the mold. For the broom hanger, I chose to fill the mold from the center of its body.
To connect the mold to the injection machine, half of a standard ½ inch pipe nipple is used. Welded to a flange that can be attached to the mold (hopefully) with the closing screws.
