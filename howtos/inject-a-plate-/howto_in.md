### Inject a plate  


In this awsome "How To" you will make an aluminium mould for injecting a plate (3mm thickness) from recycled plastic.

Can be used for serving dry food like nuts or for objects :)
Should not be used for eating purposes - only if you apply a correct coating or lacquer!

Check out the real life plate execution here: https://www.youtube.com/watch? v=YzjTm3FRLVY&t=5s 

This product rocks!
*special thanks to Paul Denney!