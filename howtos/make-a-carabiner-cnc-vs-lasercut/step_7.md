### Laser cut - Inject and finish 

 
Time to inject!

To separate the middle plate from the top plate, hammer a bolt into the nozzle, you will be able to support the top and middle part with two pieces of wood to do this step, as you can see in the first picture (that's why the middle one is sligthly smaller). 

Afterwards tap the channels gently to take everything out from the mould. You can use something like a bolt for this.

Whenever it's out, twist the carabiner from the channel and finish it with a cutter.