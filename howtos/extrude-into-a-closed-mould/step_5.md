### Measuring extruder pressure 

 We also invested in a pressure sensor that screwed onto the end of our barrel. This gave us accurate readings on if the pressure built up too high and kept us safe. These are not cheap (2,800euro) but worth the money if you intend on making lots of products. 


