### Sunglasses Model - Base 

 Depending on the type of eyewear you will want the frame to have a bigger curvature to cover as much face as possible, or a completely flat frame, to follow the lastest of the fashion trends ;)

The Base value is what is used to define this curvature. The higher the base number, the smaller the diameter of the curvature of the frame will be. Base nine is used for sport eyewear, which offers a pretty tight fit to protect the eyes as much as possible, whereas a base 2 is used for fashion eyewear where the frame is almost flat.  

Our Classic model used to be base 6. Our new models are base 4. 

