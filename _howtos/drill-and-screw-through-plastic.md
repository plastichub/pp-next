---
image: "/howtos/drill-and-screw-through-plastic/drill.jpg"
title: "Drill and screw through plastic"
tagline: ""
description: "When screwing through plastic, several factors have to be taken into account. Here we will explain a few steps and tips to make the job easier."
keywords: "melting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Stay safe</h4>
<div class="step-text">
  When talking about safety we are only referring to precautions about working specifically with plastic, as it’s our thing. We assume that working with the drill press needs a certain level of expertise so please take all the precautions related with how the tool works.<br />
  <br />
  When using the drill, special attention must be taken when working with plastics such as PS or ABS. There is a danger of overheating the material locally and accidentally releasing bad fumes. So in order to work safer make sure to use a gas mask with ABEK filters to prevent inhaling possibly toxic fumes. An easy way to identify them is to check if the filter has four colour lines (brown, grey, yellow &amp; green).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/drill-and-screw-through-plastic/zMgcexrLKN0xyn5TKCHPKIywd-a4cd9eBJHK4n2HT2s8ubEeV0JSmwKZPmpmcOHFQQJfgUC20VOsevqeXu8Wgz9jHdReZrea6-0OfDgIkGct8L9zYcmBm38XC9Lssk95aWF3AheR.jpg">
        <img class="step-image" src="/howtos/drill-and-screw-through-plastic/zMgcexrLKN0xyn5TKCHPKIywd-a4cd9eBJHK4n2HT2s8ubEeV0JSmwKZPmpmcOHFQQJfgUC20VOsevqeXu8Wgz9jHdReZrea6-0OfDgIkGct8L9zYcmBm38XC9Lssk95aWF3AheR.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Collect the valuable offcuts</h4>
<div class="step-text">
  Processes like drilling create a lot of small plastic pieces. Try to collect all the offcuts you generate since they can be used perfectly as pellets for your further projects - and that also saves them from ending up as microplastic in the environment!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/drill-and-screw-through-plastic/PG7YHWrbHyM_P5mUgCPJPdJeClkbBbOkr3tzu-kdCqKss2V5N2ZidD5vNY6_QJ9iIhIt5xLXTJf99kJfQ3E4jauYq1e8M6d97e4oag4qMDKAfdnw6Qv4SjKwaxoey5S3R2U0-5HQ.jpg">
        <img class="step-image" src="/howtos/drill-and-screw-through-plastic/PG7YHWrbHyM_P5mUgCPJPdJeClkbBbOkr3tzu-kdCqKss2V5N2ZidD5vNY6_QJ9iIhIt5xLXTJf99kJfQ3E4jauYq1e8M6d97e4oag4qMDKAfdnw6Qv4SjKwaxoey5S3R2U0-5HQ.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Pre-drill</h4>
<div class="step-text">
  In order to avoid any deformation and the possibility to break the piece we recommend to pre-drill a hole with a slightly smaller diameter than the screw (e.g. for a 3mm wood screw you’ll pre-drill a 2mm hole). This is especially needed for holes which are closer than 8mm to the edge and when working with PS in general.<br />
  <br />
  Setup:<br />
  The best way to drill through plastic is a combination of a metal drill bit and setting the machine at high speed. Special attention while working with brittle plastics like PS since one of the sides generally ends up chipped.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/drill-and-screw-through-plastic/d42_AeSwVSyCutqCgM_KP0R4mdY54eCZy-vKE82E2NatwjqRam7WnNsKTZCyNo0RLVjtfdVRFJ3jdeBsCqajNQs5soa1CFrfI2KrKE0pVFl0hLD4OBZqTiH4c5Z7pnvyE0acox1K.jpg">
        <img class="step-image" src="/howtos/drill-and-screw-through-plastic/d42_AeSwVSyCutqCgM_KP0R4mdY54eCZy-vKE82E2NatwjqRam7WnNsKTZCyNo0RLVjtfdVRFJ3jdeBsCqajNQs5soa1CFrfI2KrKE0pVFl0hLD4OBZqTiH4c5Z7pnvyE0acox1K.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/drill-and-screw-through-plastic/18bUUOYgn_IqV6KK9bl1CgY1VvpJkXc2heXZRXQSSMbjUBzlucQyPBFL6aBFRhAzlT7wf6LXW9mRcOLkKFMas58Az49kU5LiNJBaIljt2uljEnP0GqXiqiSku3Qbkwp33VOvqwou.jpg">
        <img class="step-image" src="/howtos/drill-and-screw-through-plastic/18bUUOYgn_IqV6KK9bl1CgY1VvpJkXc2heXZRXQSSMbjUBzlucQyPBFL6aBFRhAzlT7wf6LXW9mRcOLkKFMas58Az49kU5LiNJBaIljt2uljEnP0GqXiqiSku3Qbkwp33VOvqwou.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Countersink the hole</h4>
<div class="step-text">
  Depending on the type of screw you choose you can use a countersink bit to countersink one side of the hole.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/drill-and-screw-through-plastic/A7hg-jhp3UjyubiuHkOGIJh0gRXNWnIduurJokzcm1A0DTBhO5rBgTgmGi4BTGLZ-gzNH6aDYGCPGsIv9sY18flf9oXCmTr4WdeLedtQdAYPJjuOsghHIeKVg-Da9-jmJJPZj3E8.jpg">
        <img class="step-image" src="/howtos/drill-and-screw-through-plastic/A7hg-jhp3UjyubiuHkOGIJh0gRXNWnIduurJokzcm1A0DTBhO5rBgTgmGi4BTGLZ-gzNH6aDYGCPGsIv9sY18flf9oXCmTr4WdeLedtQdAYPJjuOsghHIeKVg-Da9-jmJJPZj3E8.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Threading the hole</h4>
<div class="step-text">
  When working with bolts, you might want to thread the inside of the whole. Plastic materials fully embrace this technique.<br />
  For tough materials like HDPE and PP this is not urgently needed though, as just screwing in the bolt is often enough to hold it pretty tight.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/drill-and-screw-through-plastic/Ji-Rlvxrnl9ozMOqcJ_35rxdVTzrszpaNLQuVXCvbZRzfQ5HTViEwVA4TuMvpnhrys0r7UbSWtXY33SmjUftqL-gzpeXRAXRu2cjdjZwnwzD-vtXDfjZWJ9WOO1LnuW7wJb_Vdnv.jpg">
        <img class="step-image" src="/howtos/drill-and-screw-through-plastic/Ji-Rlvxrnl9ozMOqcJ_35rxdVTzrszpaNLQuVXCvbZRzfQ5HTViEwVA4TuMvpnhrys0r7UbSWtXY33SmjUftqL-gzpeXRAXRu2cjdjZwnwzD-vtXDfjZWJ9WOO1LnuW7wJb_Vdnv.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>