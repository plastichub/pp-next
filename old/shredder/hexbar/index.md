---
image: /_howto/shredder/hexbar/IMG_2318.JPG
category: "shredder"
title: "Hexbar"
tagline: ""
description: ""
usedin:
  - zoe
  - shredder
  - shredder-pro
  - shredder_v31

---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2311.JPG" _target="_blank">
        <img id="IMG_2311.JPG" src="./IMG_2311.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2312.JPG" _target="_blank">
        <img id="IMG_2312.JPG" src="./IMG_2312.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2313.JPG" _target="_blank">
        <img id="IMG_2313.JPG" src="./IMG_2313.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2314.JPG" _target="_blank">
        <img id="IMG_2314.JPG" src="./IMG_2314.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2315.JPG" _target="_blank">
        <img id="IMG_2315.JPG" src="./IMG_2315.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2316.JPG" _target="_blank">
        <img id="IMG_2316.JPG" src="./IMG_2316.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2317.JPG" _target="_blank">
        <img id="IMG_2317.JPG" src="./IMG_2317.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2318.JPG" _target="_blank">
        <img id="IMG_2318.JPG" src="./IMG_2318.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

