<span id="specs" style="padding: 16px">
    <table class="center specs">
        <tbody>
            <tr>
                <td>Type
                </td>
                <td>Shredder
                </td>
            </tr>
            <tr>
                <td>Version
                </td>
                <td>1.0 | Rev.A
                </td>
            </tr>
            <tr>
                <td> Voltage
                </td>
                <td>220V
                </td>
            </tr>
            <tr>
                <td> Status
                </td>
                <td>Development
                </td>
            </tr>
        </tbody>
    </table>
</span>
