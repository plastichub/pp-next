### The Vacuum 

 The vacuum, is of course the most crucial component of this hack. You can use something you already have, or purchase a vacuum fit for purpose.

We went with a backpack vacuum - our small workshop is a shared space, so we need something that is fairly compact and quiet, yet powerful. As our vacuum needs to run for long periods of time, we also felt it was best to opt for one that's built for commercial use.

Direct link to our vacuum of choice: https://sydneytools.com.au/product/bayer-bp45l-1200w-hi-powered-4-5l-tank-dry-backpack-vacuum

