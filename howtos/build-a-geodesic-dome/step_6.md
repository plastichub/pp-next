### Get ready for assembly 

 Now that you have all the components you can build your structure.

Gather the following tools and metal parts for for assembling:
- 2x 10 mm spanner
- 130x Bolts - M6 x 40 mm
- 130x Nuts - M6
- 260x Washers
- Hand Drill with M6 Drill Bit