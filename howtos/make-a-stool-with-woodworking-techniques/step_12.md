### Make it live long! 

 Congratulations, you made it! Enjoy and take care of your new stool. 

This stool is designed to be disassembled easily. So in case a part breaks or you want to change something, just remove that piece and replace it with a new one. 

Also the top can be always polished to make it shiny again. Even though the scratches can give a cool look over time. 

If the piece cannot be reused or refurbished make sure to bring them to your local Precious Plastic workspace or recycle it in another responsible way :)
