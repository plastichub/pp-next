### Conical guides 

 Turn your round metal bar (no. 6) to create the conical guides and saw a channel on one side to let the air flow out when inserting. With a vice or a hammer, insert the conical guides into part no.1. 

(Drawings page 11)