### Gears and Ball Bearings 

 A used bicycle square taper bottom bracket and corresponding crank and crank bolt will need to be acquired to be able to attach the gear to the shredder. The steel square tapered end of the bottom bracket is first cut off with an angle grinder.

A sealed ball bearing will be needed to allow the blades to spin on the axle. The ball bearing needs to allow the thickest end of the taper to fit through it. In my case the square taper had a maximum diameter of 16mm and so a sealed ball bearing with an inner diameter of 17mm was used.  
