### Put everything together and fill up with sand 

 Finally, all the pieces are ready and we can assemble the filter.

First of all, cut a wooden board for the barrel to stand on, and put it in the frame.

Put in the barrel, fill up with sand until around 320 mm height, or barely covering the lower PVC pipe (here you should experiment to reach the desired pressure of around one bar).

Add the lid and tighten the bolts (later on you might need to fasten them a little more, if it is not sealed enough).

Now it’s time to install the pump with two bolts, attach the pre-filter and the 1m long connecting hose. At this step, the filter is done and can be connected to the water system.