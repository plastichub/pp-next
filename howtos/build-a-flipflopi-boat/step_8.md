### The boat build 

 Time to build the boat! Starting with the keel, the ribs and connection parts, then making the hull with the extruded planks,  and finishing with the colourful Flipflop sheets. 

 Other than using this new material, the boatbuilders made the boat in their traditional way, meaning that they used very basic tools (every screw was inserted with a hand drill and a screwdriver!).  

This is obviously something we won't be able to teach you here - that's what you need a boatbuilder for! :) 