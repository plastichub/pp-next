### Gather everything you need 

 Before you start get all your gear ready:

- Respirator mask
- Big block of aluminium
- 3D Design software
- CNC machine 
- Injection machine
- Plastic Type Stamp