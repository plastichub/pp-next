# Driving Noah's extruder

Print speed will be determined largely by the weight of the moving components, and therefore it is essential to reduce the moving weight of the print head as much a possible.

The heaviest elements of the extruder are the motor and gearbox. It may be possible to avoid moving the motor in the Z axis, instead having it mounted on the X carriage, and delivering its torque through a shaft.

However this transmission must accomodate the movement of the Z axis.


## [Hollow shaft reducer gearbox] 


![MRV 063 gearbox](https://sc01.alicdn.com/kf/UTB8HI7WmU_4iuJk43Fqq6z.FpXaG.jpg)






A hollow shaft gearbox allows the driveshaft to pass through a keyed hole. It i proposed that the shaft be driven while still being able to slide along is axis, though the driving hole in the gearbox. 


The extruder screw would be connected to the shaft, with the barrel assembly mounted on vertical rails.
This allows the shaft to move up and down through the gearbox, while still engaged with it to be driven. Then the screw and barrel can move up and down in the Z axis but the motor can remain static.

The usual method to achieve this is with a spline shaft and coupling:

![Spline shaft and bushing](https://www.abssac.co.uk/uploads/site/products/p_9ag0s/img_Splinedshafts1.JPG)

![spline](https://www.abssac.co.uk/images/Splined_Shafts_Animation.gif)
[source](https://www.abssac.co.uk/p/Lead+Screws/Splined+Shafts/83/)


![Splined shaft and bushing](https://www.ondrives.com/content/images/thumbs/0050832_294-111-100.jpeg)

[video - spline shaft drive](https://www.youtube.com/watch?v=55Ifha4apJs)



However, there is no easy way to mount the spline bushing on the gearbox

The gearbox has a 1" bore, with a keyway. It might be possible to use this keyway in the same way as a spline. Full length keyed shaft is available.

![keyed shaft](https://www.applied.com/webmedia/product_assets/hbc/h48/10031346581534.jpg)


Another solution might be to modify the gearbox by cutting more keyways to create a spline shaft gearbox

![spline shaft gearbox](https://diequa.com/wp-content/uploads/2018/09/Screen-Shot-2017-10-16-at-2.06.20-PM.png)

But the large torque is likely to cause so much friction that the shaft cannot slide in and out.

To address this it may be possible to use a ball spline. 
These use ballbearings instead of a key or spline to transfer torque while allowing the shaft to move along its axis:

![ball spline](https://www.nbcorporation.com/wp-content/uploads/splfs01.gif)

There is a system which incorporates rotational and linear motion using a combination of a ball screw and a ball spline each driven by separate motors on the same shaft. This would be a very lightweight and elegant approach but would be expensive to replicate at a large scale:

![video](https://www.youtube.com/watch?v=vkcxmM0iC8U)


## Motor calcs

The current candidate for the extruder (PP extruder) uses a 0.75kw motor running at 1430 rpm with a 1:30 reducing gearbox.

Torque before the gearbox is P x 9.549/n where P = Power (watts) and n = rpm; therefore Torque = ~5Nm

After the gearbox: 5 x 30 = 150Nm







