### Press the Sheet 

 Once the plastic is hot enough, transfer it into the mould, put on the top plate and get it into the sheet press. We are using our DIY 2-tonne bottle jack press. We also have a video of how to make this on our YouTube channel.

Leave to cool overnight. The HDPE will shrink as it cools so we like to come back every 5-10 minutes to add more pressure for the first hour or so. That way you will end up with the flattest sheet possible with no warping.