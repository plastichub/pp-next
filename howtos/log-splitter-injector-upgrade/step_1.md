### How we use it 

 In this video, we walk you through how we use our hydraulic injection moulding machine. We designed this machine to assist us in creating larger, thicker and 'trickier' products.

As safety is a concern, it may be helpful to include something to monitor pressure within the barrel. Initially, we were using a load cell, however as it reduced our stroke length, we are now either adding vents to our moulds or dosing the barrel with the correct amount of plastic for each shot.