## Hydraulic Power Pack

# Reference

## Introduction to Hydraulics- Theory

Hydraulic systems deliver power to an actuator using pressurized fluid:

![Basic System Schematic](https://www.cylinderservices.net/wp-content/uploads/2019/01/Log-Splitter-Circuit-Drawing2.jpg)

[Video- Hydraulic Power Units and Reservoirs (Full Lecture)](https://www.youtube.com/watch?v=tSc6JfwofIo)


### Hyrdraulic Power Pack Videos:

[Hydraulic Power Units and Reservoirs (Full Lecture)](https://www.youtube.com/watch?v=tSc6JfwofIo)

[Hydraulic Power Pack By KC Engg Works Mumbai](https://www.youtube.com/watch?v=yoSQT5fhs50)

[Hydraulic power pack by Makeit Extreme](https://www.youtube.com/watch?v=8pJ3x7WSnxU)


 
 
 ### Existing Products
 
 #### Little Champ by Continental
 
 [Little Champ (pdf)](https://pdf.directindustry.com/pdf/continental-hydraulics/little-champ-hydraulic-power-units-catalog/7308-97992.html)
 
 ![Little Champ](http://www.zeushydratech.com/wp-content/uploads/2015/09/Little-Champ-500x500.gif)
 
 Available in kit form: ![Little Champ Kit](https://www.continentalhydraulics.com/wp-content/uploads/2018/11/champ-kit-clean-web.jpg)

[Little Champ CAD - smallest 1hp version 7.4L/m 22L tank](https://www.3dcontentcentral.com/download-model.aspx?catalogid=2252&id=199825)

#### D-Pak by Parker

[Parker D-Pak (pdf)](https://www.parker.com/Literature/Hydraulic%20Pump%20Division/Hydraulic%20Pump%20Division%20STATIC%20FILES/Sales%20Catalogs%20&%20Thumbnails/Power%20Units/D-Pak%205%20Gallon/D,H,V-Pak_and_V-Pak_Low_Profile_Series_HY28-2661-CD-US.pdf)

![D-pak](https://img.letgo.com/images/8e/d6/6b/fb/8ed66bfb7aac2d71df1232e192f879df.jpg?impolicy=img_600)

[CAD model](https://b2b.partcommunity.com/community/pin/73525/3d-cad-models-d-pak-low-profile-series-hydraulic-power-units)

[More CAD models (better)](www.parker.com/cadfiles/246579/DPAKS.ZIP)

