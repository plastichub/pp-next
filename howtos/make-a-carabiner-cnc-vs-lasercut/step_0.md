### CNC or laser cut? 

 CNC moulds are precise and allow you to have a better surface finish rounded corners, also they are lightweight, durable and fast to unmould, since you can make products with draft angles, so it's ideal for bigger productions. However they can be very expensive and not super accessible.

On the other hand, the laser cut version of this product will make you save some money and very probably is going to be easier for you to find someone to make this job for you! However steel density is 3 times  higher than aluminum which makes it harder to work with, also it'll have less details, as you will only get the outline of the carabiner therefore the edges will be sharper.
