### Cuting the panels- CNC machine 

 In this step, you can send the files we provide to be cut on a CNC machine or laser cutting machine. Be aware, these files were made in proportion to our material thickness of 25mm so all the joints match, if you change this measurement make sure you change it in the file too.
In our case, we only cut the outline on the machine but you can use this machine to do all the bevels and number engraving too.


