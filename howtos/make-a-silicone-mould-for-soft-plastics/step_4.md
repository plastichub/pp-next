### Preparing and Mixing the Silicone 

 The hardest part is over! Now it's time to prepare the silicone for pouring.

Preparation:
Line up the measuring jug, the silicone bottles (A) and (B), the stirrer and the mould box. Remember you must work fast before the silicone sets so you must have everything ready to go.
Mixing:
Add 250ml of part A into the measuring jug, then add 250ml of B. This should equal 500ml of silicone in the jug.
Now quickly mix the silicone with your stirrer. Be sure that the mixture is thoroughly mixed otherwise your silicone will not set properly.