---
image: /_howto/couplings/IMG_2075.JPG
category: "zoe"
title: "Couplings"
tagline: ""
description: ""
usedin:
  - lydia
  - zoe
  - couplings
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2069.JPG" _target="_blank">
        <img id="IMG_2069.JPG" src="./IMG_2069.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2070.JPG" _target="_blank">
        <img id="IMG_2070.JPG" src="./IMG_2070.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2071.JPG" _target="_blank">
        <img id="IMG_2071.JPG" src="./IMG_2071.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2072.JPG" _target="_blank">
        <img id="IMG_2072.JPG" src="./IMG_2072.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2074.JPG" _target="_blank">
        <img id="IMG_2074.JPG" src="./IMG_2074.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />[Make the keyway](./keyway/)



<div class="thumb">
    <a href="./IMG_2075.JPG" _target="_blank">
        <img id="IMG_2075.JPG" src="./IMG_2075.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

