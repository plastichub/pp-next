### Reduce the speed, increase the torque 

 The key in the shredder performance is to make it turn with high torque. That will allow it to shred thicker plastics (1-3 mm) easily. So what we want to do is reduce the speed we apply to the machine and transform it into torque. There are several techniques for power transmission:

- A gearbox. In a very compact unit, we can reduce the speed in ratios of up to 100:1. Usually they have a wormgear internally, so it will block in case of a clog and it won't back-drive.

- Pulleys and belts. The benefit of these are that they can transmit power but they can also slip, which can be good when there's a clog in the shredder.

- Gears

- Chains and sprockets