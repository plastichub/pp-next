### Incomplete fill of the mould 2 

 If nothing of the above helps, there are ways you can modify the mould to ease the injection process. Please contact your mould provider prior to those changes!

- Add air vents. Especially on thin walled and large parts air vents will help with the injection process. Everyone can add air vents after the mould is made. Use a triangular file to add small channels from the cavity area which is hit last by the plastic to the outside of the mould. Do not make them deeper than 0,1mm! Otherwise you will end up with flash at those areas.

- Increase the gate size. You can increase the surface of the gate (where the plastic flows into your part geometry) to decrease the necessary injection pressure