# Compact Injector

![compact injector v0.6](/Renderings/compact injector v0.6.jpg)

A compact injection machine for recycled plastics, evolved from the Precious Plastics designs, using a 1" barrel and plunger driven by a pair of linear actuators.

This design seeks to improve on existing models by being smaller, more stable, less physically demanding and safer to use in an eduational setting.
