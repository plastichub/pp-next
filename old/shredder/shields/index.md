---
image: /_howto/shredder/shields/IMG_2134.JPG
category: "zoe"
title: "Shields"
tagline: ""
description: ""
categories:
  - zoe
usedin:
  - zoe
  - shredder_v31

---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2123.JPG" _target="_blank">
        <img id="IMG_2123.JPG" src="./IMG_2123.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2124.JPG" _target="_blank">
        <img id="IMG_2124.JPG" src="./IMG_2124.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2125.JPG" _target="_blank">
        <img id="IMG_2125.JPG" src="./IMG_2125.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2127.JPG" _target="_blank">
        <img id="IMG_2127.JPG" src="./IMG_2127.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2128.JPG" _target="_blank">
        <img id="IMG_2128.JPG" src="./IMG_2128.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2129.JPG" _target="_blank">
        <img id="IMG_2129.JPG" src="./IMG_2129.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2130.JPG" _target="_blank">
        <img id="IMG_2130.JPG" src="./IMG_2130.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2131.JPG" _target="_blank">
        <img id="IMG_2131.JPG" src="./IMG_2131.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2132.JPG" _target="_blank">
        <img id="IMG_2132.JPG" src="./IMG_2132.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2133.JPG" _target="_blank">
        <img id="IMG_2133.JPG" src="./IMG_2133.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2134.JPG" _target="_blank">
        <img id="IMG_2134.JPG" src="./IMG_2134.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

