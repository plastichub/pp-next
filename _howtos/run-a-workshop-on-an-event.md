---
image: "/howtos/run-a-workshop-on-an-event/190614-16_Altonale VisionAir42.JPG"
title: "Run a workshop on an event"
tagline: ""
description: "A crucial part of working towards reducing plastic pollution is to show people the process offline! The following introduction gives you a quick overview about all the stuff we at Kunststoffschmiede pay attention before starting a workshop in public."
keywords: "collection,melting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "collection"
- "melting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/kunststoffschmiede">kunststoffschmiede</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prepare your machines</h4>
<div class="step-text">
  Ok, so before you start giving workshops, think about what kind of machine you would like to use in this context. What kind of workshop is it? How many people are coming and how much time do they have?<br />
  All the machines you use in public have to be safe against gross negligence. For a better performance, make sure that everything is clean, working properly and maybe you have some spare parts with you.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Prepare your machines 1 - Elleke_Precious Plastic.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Prepare your machines 1 - Elleke_Precious Plastic.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Prepare your machines 2 - Elleke_Precious Plastic.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Prepare your machines 2 - Elleke_Precious Plastic.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Prepare your machines 3 - Elleke_Precious Plastic.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Prepare your machines 3 - Elleke_Precious Plastic.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Prepare your moulds</h4>
<div class="step-text">
  Like the machines you use in public your moulds have to be reliable as well. We recommend moulds with a small cycle time, so as many people as possible can produce a product...<br />
  <br />
  That means:<br />
  - Quick release-System to open and close your mould fast<br />
  - Small injection volume<br />
  - The product is easy to demould
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Prepare your moulds 1 - Neja Hrovat_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Prepare your moulds 1 - Neja Hrovat_Konglomerat eV.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare your equipment</h4>
<div class="step-text">
  You are leaving your workshop, so you need everything to run a plastic recycling workshop mobile. <br />
  As everyone has their own way of doing it, you get a list from Kunststoffschmiede (Germany) for your inspiration:<br />
  <br />
  - Injection machine and the bicycle shredder<br />
  - Tools for maintenance <br />
  - Productcounter<br />
  - Electric box (including: Power cable, distributer and some lights)<br />
  - Moulds<br />
  - Shredded plastic<br />
  - Clean and unshredded plastic<br />
  - Samples from us and the community to show the possibilities<br />
  - Informations like flyers and posters<br />
  - First Aid Kit<br />
  - Camera<br />
  - strap, tape and ropes<br />
  - Tub and cleaning stuff to wash the plastic
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Equipment 1 - Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Equipment 1 - Konglomerat eV.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Equipment 2 - Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Equipment 2 - Konglomerat eV.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Prepare your materials</h4>
<div class="step-text">
  For your workshop you need shredded plastic and some clean plastic which is ready to be shredded. Reserves are always good, maybe the shredder stops working and you are running out of fresh granules… Be prepared!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Prepare your materials 1 - Neja Hrovat_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Prepare your materials 1 - Neja Hrovat_Konglomerat eV.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Prepare your materials 2 - Neja Hrovat_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Prepare your materials 2 - Neja Hrovat_Konglomerat eV.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Prepare samples</h4>
<div class="step-text">
  It’s always good to have a variety of material and product samples to inspire people and help them understand the possibilities of plastic recycling. <br />
  You can use your own products, or get some more from the Precious Plastic Bazar to show what other workspaces around the world are doing. Present the products in a beautiful setting.<br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Samples - Löhrer_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Samples - Löhrer_Konglomerat eV.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Prepare your message</h4>
<div class="step-text">
  You also have to prepare yourself and ask yourself these questions:<br />
  <br />
  - Why are you giving this workshop?<br />
  - What kind of knowledge and experience do you want to teach people?<br />
  - What are your good at?
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Prepare yourself - Konglomerat eV.JPG">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Prepare yourself - Konglomerat eV.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Prepare your team</h4>
<div class="step-text">
  Don’t be alone, it’s much more fun and relaxed to work in a team!+<br />
  <br />
  Here at Kunststoffschmiede we recommend 3-4 supervisors when you’re working with two machines. There are always people who have a lot of questions.<br />
  <br />
  What does your team need:<br />
  - perfect operation of the machines and moulds<br />
  - communicative<br />
  - Your mindset is on one level. So your workshops have a consistent quality.<br />
  - They know the answers to the most asked questions. 👉 Next step!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Prepare your Team - Konglomerat eV.JPG">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Prepare your Team - Konglomerat eV.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Typical questions asked at a workshop in public:</h4>
<div class="step-text">
  During the process most of the people have a lot of questions. We recommend that you always have an answer to the following question:<br />
  <br />
  - What’s your story/backround?<br />
  - What’s your goal?<br />
  - What’s precious plastic?<br />
  - Can you be booked<br />
  - What type of plastic are you able to recycle?<br />
  - Are the fumes toxic?<br />
  - What is plastic? (ingredients)<br />
  - How can I change my lifestyle into an ecofriendly lifestyle?
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/FAQ - Löhrer_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/FAQ - Löhrer_Konglomerat eV.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Wanna get money?</h4>
<div class="step-text">
  Be aware that you are doing something special that only a few can offer. And if you&#39;re asked to do a workshop, then think about whether you want to be paid for it. How much money you take is up to you. It depends on what circumstances you have to deal with.<br />
  <br />
  Here are some cost items listed:<br />
  - Transport<br />
  - Use of the machine, moulds and other tools<br />
  - Fee for your team<br />
  - Placing cost<br />
  - Coordination<br />
  - Overhead for your organisation
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Wanna get money - Precious Plastic Official.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Wanna get money - Precious Plastic Official.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Promote your workshop</h4>
<div class="step-text">
  Depending on how open the event is, make sure you don’t forget to let people know about your workshop, so that as many people as possible get the chance to see and learn.<br />
  <br />
  There are several places where you can announce it, here some ideas:<br />
  - post a story/post on social media with #preciousplastic<br />
  - post it in your country’s channel on the Precious Plastic Discord<br />
  - create an event on the community platform<br />
  👉 community.preciousplastic.com/events
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Promote your Workshop.PNG">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Promote your Workshop.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Site-specific questions</h4>
<div class="step-text">
  As mentioned before you are leaving your lovely workshop. So sad :(<br />
  To be sure that everything runs great with your mobile workshop, you should clarify the following questions with the organizer:<br />
  <br />
  - Power source: Is it strong enough, and how far is the plug?<br />
  - You need fresh water? Ask for the next water tap.<br />
  - Weather conditions: Is it protected from rain, ventilated with fresh air?<br />
  - How much space is available?<br />
  - What kind of people are expected?<br />
  - When are construction and dismantling times?<br />
  - Who is liable if something goes wrong?
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Site specific questions quer.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Site specific questions quer.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Construction on site</h4>
<div class="step-text">
  Label all the stations and create enough space around the machines so many people can see what’s happening. Compare your setup with the course of the recycling process and check if it’s similar. <br />
  <br />
  Build up your mobile workshop with enough time and take a break before everything starts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Construction on side 1 - Hrovat_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Construction on side 1 - Hrovat_Konglomerat eV.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Timing</h4>
<div class="step-text">
  Test everything beforehand with friends, so that you’re sure it runs smoothly. Then you have a better feeling and you know on what points you have to work on. If you do not practice, you won’t know how many people you can look after at the same time etc.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Timing 1 - Löhrer_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Timing 1 - Löhrer_Konglomerat eV.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Timing 2 - Löhrer_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Timing 2 - Löhrer_Konglomerat eV.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Timing 3 - Löhrer_Konglomerat eV.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Timing 3 - Löhrer_Konglomerat eV.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Participation</h4>
<div class="step-text">
  Think carefully about how to get strangers involved in the process. What can they do without endangering themselves or the machines?
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Participation 2 - David Plas_BMBF.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Participation 2 - David Plas_BMBF.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Participation 3 - Stadelhofer_Konglomerat eV.JPG">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Participation 3 - Stadelhofer_Konglomerat eV.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Participation 1 - David Plas_BMBF.jpg">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Participation 1 - David Plas_BMBF.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Post processing</h4>
<div class="step-text">
  The last step is all about documentation...<br />
  <br />
  How many people have you reached and how many products did you produce? Could you make interesting contacts or was the press there and wrote a report about you? <br />
  <br />
  It’s good to track your impact and learn from your experiences.<br />
  And of course collect all your pictures! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/run-a-workshop-on-an-event/Post processing quer.png">
        <img class="step-image" src="/howtos/run-a-workshop-on-an-event/Post processing quer.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>