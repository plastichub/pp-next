---
image: "/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_02.jpg"
title: "Create a bowl using a laser cut mould"
tagline: ""
description: "Mathijs will show you how to create an iconic recycled bowl using a lasercut moud."
keywords: "PS,mould,compression,PP"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PS"
- "mould"
- "compression"
- "PP"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather everything you need</h4>
<div class="step-text">
  Before you start get all your gear ready:<br />
  <br />
  - Plastic (PP or PS)<br />
  - Lasercutted Metal<br />
  - Welding machine<br />
  - Sanding machine<br />
  - Sanding paper<br />
  - Compression machine<br />
  - Spray paint<br />
  - Knife<br />
  - Plastic Type Stamp
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_07.jpg">
        <img class="step-image" src="/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_07.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Watch this video tutorial</h4>
<div class="step-text">
  Watch this video tutorial to learn step-by-step how to laser cut the mould, weld it together, polish it and create your bowl.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Done & Share</h4>
<div class="step-text">
  After watching this video you should understand the basics of how to work with lasercut moulds. You can reproduce the bowl or try to make other precious products. We’re looking forward to seeing what you can come up with. Make sure to share back your new creations so the community can learn from you. Tag #preciousplastic on social media, create a new how-to or send us an email.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_01.jpg">
        <img class="step-image" src="/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_01.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_10.jpg">
        <img class="step-image" src="/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_10.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_08.jpg">
        <img class="step-image" src="/howtos/create-a-bowl-using-a-laser-cut-mould/bowl_08.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>