<span id="specs" style="padding: 16px">
    <table>
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td> Shredder
                </td>
            </tr>        
            <tr>
                <td>Version:
                </td>
                <td>3.1
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Mature
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
            <td> Author </td>
                <td>
                 [PlasticHub S.L.](${author_link}) |
                 [PrecisousPlastic - Eindhoven](${author_link_pp}) |
                </td>
            </tr>
        </tbody>
    </table>
</span>
