### Download 

 Click here 👉 http://tiny.cc/injectionmachine to download the following files:
- 3D .step files
- Laser cut files .dwg
- Bill of materials.
- Aseembly guide.

To build this machine you'll need to have access to:
- Metal laser cut
- Grinder
- Welding machine
- Drill press
- Basic tools: spanners, allen keys, screwdivers, etc.

If you're setting up a Injection workspace, you should still download our Injection Starterkit from our Academy. 
Also, here you'll find some links to the videos and how tos for running an Injection Workspace:

👉 https://www.youtube.com/watch?v=Iu6vh75Th2M
👉 https://community.preciousplastic.com/how-to/work-with-the-injection-machine