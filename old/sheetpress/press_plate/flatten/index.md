---
image: /_howto/sheetpress/press_plate/flatten/IMG_2398.JPG
category: "sheetpress"
title: "Flatten"
tagline: ""
description: ""
title: "Press Plate Flattening - Pre - Assembly"
usedin:
  - sheetpress-cell
  - sheetpress
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2392.JPG" _target="_blank">
        <img id="IMG_2392.JPG" src="./IMG_2392.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2394.JPG" _target="_blank">
        <img id="IMG_2394.JPG" src="./IMG_2394.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2397.JPG" _target="_blank">
        <img id="IMG_2397.JPG" src="./IMG_2397.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2398.JPG" _target="_blank">
        <img id="IMG_2398.JPG" src="./IMG_2398.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

