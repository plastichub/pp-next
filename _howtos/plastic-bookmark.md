---
image: "/howtos/plastic-bookmark/Untitled-3.png"
title: "Plastic bookmark"
tagline: ""
description: "Make the moulds<br />Plastic bookmark will be injected, you need the mould and access to an injection machine. For mould-making, download the files above and CNC-mill it yourself or send it to a mould maker.<br /><br />This mould need a lot of force to be fully injected"
keywords: "injection,PP,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "PP"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/alumoulds">alumoulds</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Preparations</h4>
<div class="step-text">
  Due to the thin object, preheating the mould around 80°C to make the process of injecting easier.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Inject the parts</h4>
<div class="step-text">
  Time to inject. The plastic should be evenly molten to easily spread within the mould, so it might run out of the nozzle just because of gravity. Use a valve into the nozzle and open it right before you start injecting.Act fast and keep the pressure for a couple of seconds before lifting the lever. This will prevent sink marks as the plastic is cooling down under pressure.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Unmould your parts</h4>
<div class="step-text">
  Once injected, open the mould if you wait a lot ,because of the plastic, shrinking maybe it will be more hard to release the object!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/plastic-bookmark/Untitled-2.png">
        <img class="step-image" src="/howtos/plastic-bookmark/Untitled-2.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/plastic-bookmark/Untitled-2.png">
        <img class="step-image" src="/howtos/plastic-bookmark/Untitled-2.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>