---
image: "/howtos/make-a-handplane-simple-mould/howto-yan.png"
title: "Make a handplane (simple mould)"
tagline: ""
description: "How to make a simple mold for injection molding out of metal sheet or existing metal forms. As an example we will focus on a bodysurf handplane fabrication."
keywords: "product,mould,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "mould"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - PLAN YOUR DESIGN</h4>
<div class="step-text">
  You can build a very simple mold directly from metal plate or existing metal. The first step is to design your object to stay simple while functional, and to define the use of your product in order to most efficiently reduce its complexity. Taking the bodysurf handplane as an example: it’s main purpose is to increase the surface of your hand while bodysurfing, the second purpose is to give some hook in the wave. The first purpose can be achieved by a big enough plank while the second can be achieved by fins, channels or simply a concave hull. This last option is the simplest to achieve without a complex CNC mold.<br />
  It’s beneficial to carry out research into similar industrial products. We researched available handplanes, and (surprise) they looked much the same as our design, with some complex additions that didn’t make any difference to us while testing. We would suggest that you should not try to exactly replicate industrial products, nor take too much inspiration from them. Use them to verify your ideas, once your thinking process is complete.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/01-A.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/01-A.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/01-B.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/01-B.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - LIMITATIONS & TESTING</h4>
<div class="step-text">
  As complex shapes will be hard to achieve without complex machinery, you will have to finish your product by hand. With our handplanes we simplify everything as much as possible, to reduce this post-injection finishing time and material waste. Simplification is key.<br />
  For the purpose of testing our design, we carried out trials with compressed plastic plates, cut to different shapes and then bent to different diameter concaves.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/02.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/02.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - FIND THE BEST MATERIAL</h4>
<div class="step-text">
  In this example we will focus on the handplane mold. As we defined that the most important parameter will be the concave mould, we needed to find a metallic piece corresponding to this concave diameter. Our testing showed us that Ø600mm would give the optimal concave, which then brings us to two options: Find a nominal OD600 pipe in a scrapyard, or ask a boilermaker to roll a plate to this diameter. Here we went for the second option as it was the easiest to find in our area.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/03.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/03.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - CHOOSE YOUR WIDTH & THICKNESS</h4>
<div class="step-text">
  We used a stainless steel plate as this would require less finishing (see finishing chapter - step 9), and is compatible with a steel casted plug for welding purposes (see welding chapter - step 7). Using a black steel plate is also completely fine and will work the same.<br />
  The plate width should correspond to your handplane length. Here, the handplane is 250mm long, so a 300mm width plate will be fine to allow space for drilling the bolt holes (see drilling chapter - step 8). For thickness, we observed within our testing that 4mm would be the minimum necessary to handle injection pressure. For this mold we decided on 5mm, as we wanted our handplane to be 5mm thick (see cutting chapter - step 5).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/04.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/04.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - MOULD EXPLAINED</h4>
<div class="step-text">
  This mold is made from three plates: the top plate with the nozzle welded, the spacer with the outline of the handplane cut, and the bottom part left untouched. Our pieces are 25mm width as the handplanes are 20mm width.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/05-A.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/05-A.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/05-B.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/05-B.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/05-C.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/05-C.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - CUT THE SPACER</h4>
<div class="step-text">
  First you should draw an outline on the middle plate that will become the spacer. You can draw your own shape (half ellipse 250*200mm shapes works well, but feel free to be creative!). We first used a grinder with a cutting disk to cut as close as possible to the drawn line, then with a flap disk to get closer. Finally we used a dremel with a metal reamer to reach the desired precision. To be sure that the shape was symmetrical, we drew on to paper using our freshly cut spacer as a guide. Then, by folding the paper in two along a longitudinal line, we could see clearly that both side were identical and correct.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/06-A.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/06-A.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/06-B.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/06-B.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - WELD THE NOZZLE</h4>
<div class="step-text">
  As we are working the original Precious Plastic injection machine, we welded a ½’ nozzle on the top plate. The nozzle should be made from steel to be weldable, and grinded flat. If your plate is stainless steel you should use a stainless steel wire metal as it will be a dissimilar material weld. Type 308 or 309 should be your first choice, but type 304 or 316 will do the job (and easier to find).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/07.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/07.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - BUILD THE MOLD</h4>
<div class="step-text">
  We drilled the injection with a 5mm drill bit, this hole should be drilled in the middle of the nozzle and done after welding to ensure a good alignment. All other drilling was made with a 8,5mm drill bit and should be done on all three plate at the same time to ensure good alignment. The three plates can be bolted together as the drilling is ongoing. We would recommend one drill every 50-100mm to secure the sealing.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/08-A.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/08-A.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/08-B.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/08-B.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - FINISH AND ASSEMBLE</h4>
<div class="step-text">
  If you choose to go for a black steel plate then you will need to sand it to remove the scale. Only the surfaces in contact with your plastic need to be sanded. You can start with a 220 grain then go progressively to 400, 600, 800, 1000 and 1500. Sanding your metal will also protect it from rusting as water will easily flow over the surface (see injection chapter - step 12). Now the mold is finished and only needs to be bolted together!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/09-A.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/09-A.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/09-B.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/09-B.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - MODIFY INJECTION MACHINE</h4>
<div class="step-text">
  Our handplane requires more plastic to be injected than the current PP machine allows. We modified and built a larger injection barrel. You can see in another documentation this process.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/10.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/10.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - SELECT THE PLASTIC</h4>
<div class="step-text">
  We decided to work with PolyPropylene (PP) on this project as we had plenty available in various colors. HDPE should also work well, but PS may not be flexible enough for this use. Once the plastic is cleaned and sorted by type and color, you can shred it and fill the injection machine with these granules. The handplanes weigh 200g so we chose to fill the machine with at least 250g of plastic, to have enough to maintain the pressure. On our machine, 20 minutes with 200°C on the top and 220°C on the bottom of the barrel works fine.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/11.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/11.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - INJECT & COOL</h4>
<div class="step-text">
  Once the plastic is heated, you should remove the injection barrel plug and screw the mold on as quickly as possible. We generally remove the first drop of melted plastic in doing so, as it is often colder.<br />
  Injection should be done as fast as possible. Once the injector is pushed to its maximum, we maintain the pressure for 30 seconds, then unscrew the mold and immediately drop it into water. The plastic is under pressure inside the mold and will tend to leak out once unscrewed, but the water will cool the plastic, maintaining the pressure inside during the cooling process. For this product we leave the mold for 2 minutes in the water, and then 15 minutes outside before unscrewing it and extracting a freshly made handplane. This time can be used to refill the injection barrel to optimise production.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/12.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/12.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - FINISH THE PRODUCT</h4>
<div class="step-text">
  Without CNC it is difficult to make complex forms, and using our molding method we could not obtain completely finished products, the final plastic form has to be finished by hand. This finishing takes approximately 45 minutes per handplane. With the 30 minutes required by the injection (if done in series) and 45 minutes of cleaning, sorting and shredding the plastic, this handplane requires about 2 hours of work.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/13.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/13.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - SHAPING THE RAILS</h4>
<div class="step-text">
  The edges of a surfboard or handplane are called rails. We start by chamfering the top edges at 45° with a band sanding machine. On the bottom ⅔ of the handplane the chamfer removes ½ of the thickness. On the top ⅓ the chamfer removes only ⅓ of the thickness. In the same area, we made a smaller 45° chamfer of ⅙ of the thickness. The transition should be smooth and visually checked consistently during the shaping. Working with a chamfer allows you to visualise the line to keep your rounding straight. We then finalize the rounding with a blade. This way you produce a bright surface without the need for fine polishing. This process only creates shavings, which can be remelted to go towards another handplane.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/14-A.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/14-A.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/14-B.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/14-B.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/14-c.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/14-c.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/14-D.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/14-D.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - DRILLING THE STRAP HOLES</h4>
<div class="step-text">
  Once the rails are shaped, we are able to use the template attached to position the strap holes. If you want to make it perfectly adapted to your hand, your wrist should be on the bottom rail, and the inner holes should be spaced by the distance between your pointer and little finger + 10mm. The adjacent holes should be separated by 10mm and the length of the holes will be your strap width. To drill them, we first drill two 4mm round holes on both sides, and then join them with a dremel and a cutting disk.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/15-A.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/15-A.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/15-B.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/15-B.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/15-C.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/15-C.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - MOUNT THE STRAP</h4>
<div class="step-text">
  We sew an adjustable buckle on to the strap and then pass it through the holes. The leash is 700mm long, with a bowline to attach it to the handplane and a hangman’s knot around the wrist.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/16-A.jpg">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/16-A.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-handplane-simple-mould/16-B.JPG">
        <img class="step-image" src="/howtos/make-a-handplane-simple-mould/16-B.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>