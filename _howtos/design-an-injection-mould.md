---
image: "/howtos/design-an-injection-mould/howto-broomhanger-mould-0.jpg"
title: "Design an injection mould"
tagline: ""
description: "An experience review designing a broom hanger mold: These are some steps to design a mold for the injection machine."
keywords: "mould,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/el-tornillo-taller">el-tornillo-taller</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Measure existing dimensions</h4>
<div class="step-text">
  Be sure to get things accurate from the beginning! If your product is designed to fit with an existing object, take all of the measures from it, so that you start from the right point. For this broom hanger, I measured the diameters of different brooms and mops, ranging from 21-24mm. As the product will be a clamping system, I’ll use the smaller measurement of 21mm as a reference.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-1.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Sketch out your product idea</h4>
<div class="step-text">
  It can be difficult to get an idea down on paper in the beginning, but it’s equally hard to formulate your design without a drawing. Let’s start with your imagination. Register on paper all the details that you can think of to create a good design.<br />
  Especially consider important aspects like the wall thickness. Influencing factors for this will include <br />
  A) The required injection pressure,<br />
  B) required flexibility of the product itself, and <br />
  C) resistance and durability of the material<br />
  <br />
  For this purpose, I use previous products I’ve developed to examine and compare the material.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-2.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - “Mold” your idea</h4>
<div class="step-text">
  What will define your product shape? As this broom hanger will be made with the injection machine, I have to think in an enclosed mold. With the product idea I’ve drawn before, I start designing the mold, looking for the best solution for each of these 6 Mold Design <br />
  <br />
  Criteria:<br />
  1) Define the product shape, <br />
  2) Use standard measures, <br />
  3) Receive the plastic and connect the mold,<br />
  4) Open after injection and eject the product,<br />
  5) Adjust and closure, and<br />
  6) Simplify the machining process.<br />
  <br />
  I’ll explain each in the next steps.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-0.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-0.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Define the product shape</h4>
<div class="step-text">
  This is thinking on what shapes are going to create the cavity inside the mold. For this product, there are basically three pieces:<br />
  A cylinder (part 1) for the space of the broomstick,<br />
  concentric with a circular tube (part 2) which contains the outside of the hanger,<br />
  and a block (part 3) to cut the inner space and create the opening. <br />
  <br />
  With this, I’m starting to think in the standard bars and/or tubes I will use.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-4.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Use standard measures</h4>
<div class="step-text">
  In my country (Colombia) the material dimensions mostly come in Inches (metal bar sections, steel planks thicknesses…), so are easier and cheaper to find. Adapting your measurements to the standard ones will avoid paying for excess material that you will then have to pay to remove.<br />
  So for the inner cylinder, I’ll use a ¾ inch rod (19,05mm), close enough to the 21mm of the broomstick. For the outer wall, I’ll use a 1 inch tube (25,4mm), so the thickness of the broom hanger will be approximately ⅛ inch (3.17mm).<br />
  Explore your local metal market and find which measures fit better for you!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-5.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Plastic flow and mold connection</h4>
<div class="step-text">
  Think about how the plastic will flow inside the mold. The path has to be as short as possible and with enough space for the plastic to flow throughout the entire mold.<br />
  For the entrance, I usually drill a ¼ inch hole with a maximum of 10mm height. The location should be in the center, so that the plastic can be distributed equally in each direction inside the mold. For the broom hanger, I chose to fill the mold from the center of its body.<br />
  To connect the mold to the injection machine, half of a standard ½ inch pipe nipple is used. Welded to a flange that can be attached to the mold (hopefully) with the closing screws.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-6.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Open after injection and expluse the product</h4>
<div class="step-text">
  As the plastic solidifies into the product, you have to be able to open the mold and take it out. This means that the mold must split into several parts to release the product. But, a less number of parts means an easier to assemble, more steady and cheaper mold. So keep it simple! For the broom hanger, as the product works as a hook, I’m thinking of three pieces: two to form the exterior of the hanger, which splits in half to release the product; and another one to form the interior that will receive the broomstick, and will be released at the end pulling it out just as it will release the broom.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-7.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Adjust and close</h4>
<div class="step-text">
  One thing is to align the mold parts accurately and the other is to ensure that it withstands injection pressure without opening or displacing. To adjust and align, I use male/female guides between the parts so they will fit in place and support the injection pressure. Conic dowels are ideal and are a great help during the mold assembly to hold everything in place. To close the mould and bear the injection pressure, I use through screws that hold the mold parts tightly. I drill the mold holes a bit wider than the screws (for ¼” screws, I drill a 5/16” hole) so the disassembly will be easier even if a mould part gets displaced during the injection process. It is also a great idea to open the screw hole side, so the screw can be taken out by the side, only by loosening the bolts a bit and avoiding to have to loosen them completely.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-8.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-8.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Simplify the machining process</h4>
<div class="step-text">
  Design your products with basic shapes. Cubes, Cylinders, Cones and Straight Lines are shapes easy to create with manual lattes and mills. Concentric, aligned and perpendicular figures improve the machining process. Minimalism in the design results in minimalism in the process. Also, design with standard measures. Holes of the size of a drilling bit, canals of the width of a milling cutter can be done faster and with more precision. So the broom hanger product is going to be basically a straight plastic tube with an opening on one side. This means straight lines milling and circular holes to create all the mold parts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-9..jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-9..jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Use the PC tools</h4>
<div class="step-text">
  We’re looking for more accuracy than we can create on paper, so to get the real dimensions of the cuts and machining, now we can model our mold using CAD software. At this point we are continuing on our process from the previous step, just with a different tool. Using CAD we can model different versions of the mold and shape new or improved versions, stemming from the 6 Mold Design Criteria. <br />
  <br />
  Here some of the decisions I made during the process and mold design: <br />
  - The best location for the injection point is in the middle of the body of the product. <br />
  - To avoid the risk of pressure opening the mold, the injection point is better in a solid part than at the joining of the mold.<br />
  - The flange with the nipple connector should be attached over a flat face of the mold with the same closing screws.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-10-1.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-10-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-10-2.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-10-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Document it</h4>
<div class="step-text">
  The design should conclude in drawings and a part list. Think on the step by step of the machining to identify the critical procedures (for example, when parts should be machined together to get an accurate fitting; or, how they will be fixed to the machine) and register all that information in the drawings.<br />
  <br />
  Do the part list for all the raw materials to work as a shopping list. Include dimension and quantity. (Remember to buy the raw parts a bit longer than the final parts. Between 5 and 10mm will be enough to fix the parts in the machines and adjust to the final measure).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broomhanger-mould-11.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broomhanger-mould-11.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Turn it into reality</h4>
<div class="step-text">
  Now that you have the design for your mold it&#39;s time to actually build it and make your product.<br />
  You can find the building process of this broom hanger in this How-to &quot;Make a broom hanger&quot; (https://community.preciousplastic.com/how-to/make-a-broom-hanger) or use these tips to come up with your own idea :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/design-an-injection-mould/howto-broom-hanger-cover.jpg">
        <img class="step-image" src="/howtos/design-an-injection-mould/howto-broom-hanger-cover.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>