### Placing bricks 

 Your first layer can now be placed in tandem with adding the second layer. It is easiest to start in one corner and go from there adding Limiters incrementally. Bricks can easily be hammered together and no special tool is required for this, but we recommend a rubber mallet to prevent damage to the bricks. 
