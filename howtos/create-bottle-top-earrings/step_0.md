### Prepare your materials 

 First off, you will need to source your plastic! You can find plastic bottles pretty much anywhere, and all you need are the lids. If you can get a pair of the same, so that you can make a nice matching set. You will also need some shredded plastic of the same material (generally HDPE, or PP) in a complementary colour to the bottle tops.

Materials needed (for one pair of earrings)
- 2 bottle tops
- some shredded plastic of the same material 
- 2 earring hooks or rings (depending on your preference)

Required tools:
- 2 smooth tiles for melting the plastic
- oven
- drill (or another tool for making a hole in the material)

Safety recommendations:
- heat resistant gloves
- respirator mask