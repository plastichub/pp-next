<?php

namespace SR;

require __DIR__ . './../vendor/autoload.php';

/**
 * get array value from array or array if key is not set or return
 * default value if key is not found. this function can deal with dot notation
 * to get get value from multidimensional associative array like config.database.pass
 * if no key is passed in second parameter will return first parameter directly. returns default value if key is not
 * found. default value also can be an exception which is thrown then
 *
 * @param array $array expects array to get key for
 * @param null|mixed $key expects key for value to get
 * @param null|mixed|Exception $default expects optional default value if value could not be retrieved by key
 * @return array|mixed|null
 * @throws Exception
 */
class ArrayUtils
{
    public static function first(array $array)
    {
        if (!$array) {
            return null;
        }
        if (count($array)) {
            return array_values($array)[0];
        }
    }
    public static function last(array $array)
    {
        if (!$array) {
            return null;
        }
        if (count($array)) {
            return array_values($array)[count($array) - 1];
        }
    }
    public static function get(array $array, $key = null, $default = null)
    {
        if (!$array) {
            return $default;
        }
        if ($key !== null) {
            if (array_key_exists($key, $array)) {
                return $array[$key];
            }
            foreach (explode('.', trim($key, '.')) as $k => $v) {
                if (!is_array($array) || !array_key_exists($v, $array)) {
                    if ($default instanceof Exception) {
                        throw $default;
                    } else {
                        return $default;
                    }
                }
                $array = $array[$v];
            }
            return $array;
        } else {
            return $array;
        }
    }
}
