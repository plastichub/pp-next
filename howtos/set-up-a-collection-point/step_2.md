### Is this for you? 

 Collection Points are great for someone who wants to do something about the plastic problem without having to build machines or make products.

It helps if you have good social skills and enjoy talking to people, educating, informing, reminding, correcting and generally helping understand the Precious Plastic Collection system.   

You will also have to be very organized as running a Collection Point might quickly become a logistical challenge.