**Product Resources**

- [3D Preview](${product_3d})
- [Drawings](${product_drawings})
- [CAD model](${product_cad})
- [Wiki](${product_wiki})
- [Source files](${product_github})
- [Discord Chat](${product_chat})
- [Wiring](${product_wiring})
- [BOM - Bill of materials](${product_bom})