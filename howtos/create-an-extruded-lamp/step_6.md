### Wait and cool 

 Once we’re done with extruding, we let it cool down. Make sure it is well cold before removing it from the mould, otherwise the plastic will shrink unpredictably and ruin your final product. 
We cool it by simply letting it rest for 20-30 mins.