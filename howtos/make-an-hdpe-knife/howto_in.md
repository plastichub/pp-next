### Make an HDPE Knife 


We show you how to make this 'reverse knife' (metal scales and plastic blade) using at-home techniques and processes. The knife is 'sharpened' but cannot hold an edge the same way a metal blade can. However it works perfectly fine for cake, fruit and vegetables. A commenter on our YouTube video also said it would be perfect for cutting lettuce as metal knives make the cut edge turn brown quickly!