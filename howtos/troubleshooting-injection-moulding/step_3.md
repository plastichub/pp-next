### Delamination 

 If the material delaminates from the part.

Solutions:
- Check if your material is clean
- Check if your material mix is pure from one material type only. Some materials will not create a chemical connection through different characteristics on a molecular level
- Decrease the material temperatue. You can verify that by cutting the part in half to check if the delamination appears at the core too
- Decrease the mould temperature. You can verify that by cutting the part in half to check if the delamination appears only at the surface