### Spread the message 

 Your mission is to get people involved by collecting their plastic. We made a bunch of posters and graphic material to help you reach out to more people and communicate what you do. This is just a starting point, feel free to create your own materials and change them according to your language or needs.

You can spread the word by handing out flyers, giving presentations, or maybe even speaking on the radio. Wherever you are or whatever you are doing, make sure that you enjoy this moment and communicate a clear and precise message. :)