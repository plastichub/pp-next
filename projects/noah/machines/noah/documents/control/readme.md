# Control for Noah

Computer Numerical Control (CNC) is the method by which Cartesian machines are manipulated.

3D printers use a control language called G-Code in order to move print head printing three-dimensional shapes. 3D printable files must always be translated into G-code for printing. Therefore Noah's control system must be capable of running G-code. 

Desktop 3d printers operate on an "open-loop" basis - the controller sends instructions to the stepper motors which carry out the commands. However, in this scenario errors may cause the steppers to "lose steps", if for example the motor is unable to produce the movement as directed, the controller continues to send data although the print head is in a different position to the control program. This can cause failed prints at the desktop level, but at the scale of Noah, such errors could be potentially dangerous, and at the very least, costly. 

In contrast to desktop 3D printers, industrial CNC machines use "closed loop" systems, which interpret data from various sensors on the machine, and adjust the controller output accordingly in real time. This reduces the margin for error dramatically, and creates  a much safer working environment. In addition to providing the controller with positional feedback relating to the work, it allows the user to set "soft stops" which limit the motion of the machine in its axes. Therefore it is proposed that Noah must be equipped with positional feedback sensors/encoders which allow closed loop control of the gantry and print head.

## Sensors and encoders

4 types of sensor will be used in the control system for Noah; these are:

- Infrared Optical Reflective Linear Incremental Encoders (position along axes)
- Physical Limit Switches (Hard End Stops/Emergency Stops)
- Optical Rotary Encoders (for motor speeds)
- Thermal sensors (see "Extruder")

The first of these will be custom-developed; the rest are commonly available. 

[Custom linear sensors for Noah](./linear-sensor/readme.md)

The neccessity for real-time, closed-loop control with a number of sensors will dictate which control processor and software can be used. This is a considerably greater task than desktop 3d printing, since real time control requires very fast processing speeds.

## Control Software

[LinuxCNC](http://linuxcnc.org/) is the prime candidate for control software, for a number of reasons. It is open source and free, and unlike any other package (except industrial packages like [Fanuc](https://www.fanuc.eu/uk/en/cnc/cnc-system)) can combine multiple sensor inputs *on the same axis*. Also it has a large commuity of users and is updated continuously.

[Complete video series on LinuxCNC](https://www.youtube.com/watch?v=HR3_izwxMzY&list=PLaamliiI72ntlrHKIFjh2VjmehRGgZpjm)

[Getting started with LinuxCNC](https://www.youtube.com/watch?v=F4xHCwtaS3w)




Another candidate is [CNCjs](https://www.npmjs.com/package/cncjs), a web based G-code sender written in javascript. CNCjs can run on the tiny £10 [Raspberry Pi Zero W](https://thepihut.com/collections/raspberry-pi/products/raspberry-pi-zero-w?variant=30332705425), which would make it very affordable. [Video](http://blog.shahada.abubakar.net/post/adding-a-raspberry-pi-zero-w-and-cnc-js-to-the-3018-woodpecker-cnc-router). However it is unclear [if it will support closed loop positional feedback](https://github.com/cncjs/cncjs/issues/441).

A further candidate is [G2-Core](https://github.com/synthetos/g2) which runs on the £35 [Arduino Due](https://store.arduino.cc/due) 

[Examples of g2-Core in use](https://github.com/synthetos/g2/wiki/g2core-in-use)





### A Closed-Loop approach using DC motors

Servo control of DC motors might be achieved with [ServoStrap](https://github.com/danithebest91/ServoStrap), an Arduino based servo emulator driver. ServoStrap uses optical rotary sensors to control motors, but these could be replaced with Noah's [custom linear sensors](./linear-sensor/readme.md). A 3-axis ServoStrap system would operate using 3 [Arduino Pro Mini](https://www.amazon.co.uk/Arduino-Pro-Mini-328-5V-16MHz/dp/B007YQZUEE) as drivers controlled by an [Arduino Mega](https://store.arduino.cc/arduino-mega-2560-rev3) with [RAMPS](https://reprap.org/wiki/Arduino_Mega_Pololu_Shield).

![Servostrap](https://www.youmagine.com/uploads/image/file/70089/carousel_shceme2.png)

(replacing the rotary disc with a linear strip)
Note this cicuit is for brushed motors, so would need an additional circuit for commutation if brushless motors are used. 
Also the H-bridge would need to be rated for high power.





## Control Hardware

LinuxCNC users generally build custom control centres from outdated PC computers. This is not an ideal candidate for a repeatable design for Noah. However, it may be possible [to run LinuxCNC for this purpose](https://www.youtube.com/watch?v=FlZiq8uCcpI) on the most recent [RaspberryPi 4](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/).

![Raspberry Pi 4 Model B](https://www.raspberrypi.org/homepage-9df4b/static/pi4-labelled-99c2e8935bb3cfdb27d23f634ea98a7e.png)



The CPU running LinuxCNC also requires an FPGA board or "breakout board". Currently the prime candidate is the [Mesa 7I76E](http://store.mesanet.com/index.php?route=product/product&product_id=290) with the [Mesa 7i85](http://store.mesanet.com/index.php?route=product/product&search=%207i85&product_id=124) daughtercard for sensor input.

Mesa 7i85

![Mesa 7i85](http://store.mesanet.com/image/cache/catalog/Products/AIO-FPGA-Cards/7i76ecolo-500x500.png) 


Mesa 7I76E

![Mesa 7I76E](http://store.mesanet.com/image/cache/catalog/Products/AIO_Daughter_Cards/7i85colo-500x500.png)




If DC servos are to be used, an alternative to be considered is the [O-Drive](https://odriverobotics.com/), a driver which allows brushless DC motors to be paired with appropriate rotary sensors to make low cost servos. This is a strong candidate since it is fully open source and can run on a Raspberry pi with LinuxCNC; however it only has 2 axes so would not be suitable to control all of Noah's motion.

[Video - O-Drive with Raspberry Pi](https://www.youtube.com/watch?v=qTVohNscEmM)



