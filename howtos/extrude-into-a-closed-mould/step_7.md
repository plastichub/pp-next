### Removing the mould 

 Once the mould is full/set time has passed, wait a moment before removing the mould to allow any build up in pressure to defuse within the barrel. 15-30sec. This will protect you from being potentially sprayed with hot plastic. While doing this remember your PPE and use thick welding gloves to remove the hot mould. 