### Role in the Precious Plastic Universe 

 First of all, make sure you had a look at the showcase page of this Starterkit!
preciousplastic.com/starterkits/showcase/machine-shop

Now about your Role:

Machine Shops provide machines, parts and moulds to individuals and recycling workspaces within the Precious Plastic Universe.

Machine Shops should also reach out to Community Points to connect with the local Precious Plastic community and maybe get help finding people who need machines and parts. 