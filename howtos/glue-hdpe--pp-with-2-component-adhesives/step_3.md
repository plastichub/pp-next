### Apply the adhesive 

 First we have to make the mixture. Usually this adhesive comes with a single-use mixer nozzle, but it can also be made by hand, with a flat stick.

Once the mixture is homogeneous we can apply that to the surface we previously cleaned and place carefully the other piece on top. The advantage of this adhesive is that you have time to move your pieces to their precise position, since it takes 5-10 min to start working.
