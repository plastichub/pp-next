### Cut roughly with the jigsaw all the parts 

 Once we have the piece marked we just have to cut on the other side of the line with the jigsaw. At this point the cut doesn’t has to be super precise since we are going to use the handrouter afterwards. Just remember to leave enough space (around 8-10mm) from the edge of the mark in order to avoid that the jigsaw deviation caused by the thickness affects the final piece. 

If working on plastic with this tool is new to you, check the How-to "Cut with the Jigsaw” where you can find several tricks to cut plastic easier.
👉 http://tiny.cc/cut-with-jigsaw