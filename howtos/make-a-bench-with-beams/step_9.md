### Cut thread and assemble 

 With all the holes drilled, insert the barrel nuts, align and screw the thread. From this we can cut the rod to the exact length for that clean finish. Then it’s just a matter of sliding the parts together, adding a washer and tightening the nuts! 

We’ve used ‘tamper-proof’ nuts in this case to prevent any fiddling with the fit in the future.