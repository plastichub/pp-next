### Stay safe 

 
In order to bend plastic easily you have to apply heat above the melting temperature which can lead to degrading the plastic. If this happens, bad fumes can be released.
So in order to work safer make sure to use a gas mask with ABEK filters to prevent inhaling possibly toxic fumes. Special attention on plastics like PS and PVC. Also when handling with heated elements it’s recommended to wear working gloves.

Recommended safety equipment:
- ABEK mask
- gloves
