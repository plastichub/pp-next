### Fix the bar to the sheet 

 Clean and sand the bottom of the bar so it’s ready to insert the 4mm bolts to fix the metal bar on the sheet. Once it’s fixed you don’t have to remove it anymore. You will need the bolts to stick out of the bar so you can grind them off and make the top surface really flat and polish as this will show on your beam.

This metal bar will define the groove where you can insert the LED stripe plus it will prevent a big amount of manual work! You could also mill it by hand but it takes a long time and creates lots of microplastic!

To prevent the plastic to flow inside the bar you could place silicon on the side. 
