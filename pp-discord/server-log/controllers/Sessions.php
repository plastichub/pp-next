<?php
namespace SR;

require __DIR__ . './../vendor/autoload.php';

use JsonPath\JsonObject;
use SR\ArrayUtils;
use SR\Events;
use SR\Session;

class Sessions
{
    protected $db;

    public static function addIf(&$array, $value)
    {
        if (!$value) {
            return;
        }
        if (!in_array($value, $array)) {
            array_push($array, $value);
        }
        return $array;
    }

    public function remove($sids)
    {
        $db = \WeDevs\ORM\Eloquent\Database::instance();
        $sessions = explode(',', $sids);
        foreach ($sessions as $sid) {
            $session = Session::where('session', $sid)->first();
            $session->delete();
        }

    }
    public function all($visitor)
    {
        $db = \WeDevs\ORM\Eloquent\Database::instance();
        $sessions;
        if ($visitor && strlen($visitor) > 0) {
            error_log('vis');
            $sessions = Session::where([
                'visit' => $visitor,
            ])->get();
        } else {
            error_log('all');
            $sessions = Session::get();
        }
        if (!$sessions) {
            return false;
        } else {
        }

        // d($sessions, 'sessions');
        // return;
        $values = [
            'tags.location.country_name' => [],
            'tags.location.continent_name' => [],
            'tags.user' => [],
            'tags.email' => [],
            'pages' => [],
            'referer' => [],
            'tags.agent.family' => [],
            'tags.agent.os.family' => [],
            'errorMessages' => [],
            'tags.ip' => [],

        ];

        $base = $sessions->map(function ($session) use (&$values) {

            $tags = json_decode($session['tags'], true);

            $tagsObj = new JsonObject($tags);
            $sessionObj = new JsonObject($session);

            self::addIf($values['tags.location.country_name'], $tagsObj->{'$.location.country_name'}[0]);
            self::addIf($values['tags.location.continent_name'], $tagsObj->{'$.location.continent_name'}[0]);
            self::addIf($values['tags.agent.family'], $tagsObj->{'$.agent.family'}[0]);
            self::addIf($values['tags.agent.os.family'], $tagsObj->{'$.agent.os.family'}[0]);
            self::addIf($values['tags.user'], $tagsObj->{'$.user'}[0]);
            self::addIf($values['tags.email'], $tagsObj->{'$.email'}[0]);
            self::addIf($values['referer'], $sessionObj->{'$.referer'}[0]);

            $raw = Events::toRawEvents($session, 0);
            $views = Events::filterByType('view', $raw);
            $pages = array_map(function ($e) {
                return $e->payload['url'];
            }, $views);

            $errors = Events::filterByType('error', $raw);
            $clicks = Events::filterByType('click', $raw);
            $inputs = Events::filterByType('inputs', $raw);
            $moves = Events::filterByType('move', $raw);

            $errorMessages = array_map(function ($e) {
                return $e->payload['info']['message'];
            }, $errors);

            $pages = array_map(function ($e) {
                return $e->payload['url'];
            }, $views);
            foreach ($pages as $page) {
                self::addIf($values['pages'], $page);
            }
            foreach ($errorMessages as $error) {
                self::addIf($values['errorMessages'], $error);
            }

            return [
                'errorMessages' => $errorMessages,
                'tags' => $tags,
                'errors' => count($errors),
                'moves' => count($moves),
                'clicks' => count($clicks),
                'pages' => $pages,
                'viewCount' => count($pages),
                'session' => $session['session'],
                'status' => $session['status'],
                'referer' => $session['referer'],
                'visit' => $session['visit'],
                'start' => ArrayUtils::first($raw)->time,
                'end' => ArrayUtils::last($raw)->time,
                'duration' => (ArrayUtils::last($raw)->time - ArrayUtils::first($raw)->time) / 1000,
            ];
        });

        $ret = [
            'sessions' => $base,
            'values' => $values,
        ];
        return json_encode($ret);
    }
}
