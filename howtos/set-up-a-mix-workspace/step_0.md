### Role in the Precious Plastic Universe 

 First of all, make sure you had a look at the showcase page of this Starterkit!
👉 preciousplastic.com/starterkits/showcase/mix

Mix Workspaces are a great place to start making the first steps into local plastic recycling.

As they have all several machines and take care of several steps of the recycling process, they are quite independent from other workspaces, but ofcourse can still collaborate with Collection Points or Shredder Workspaces to source their material.

As everyone in the network, they should be in touch with the Community Points to connect with the local Precious Plastic community.