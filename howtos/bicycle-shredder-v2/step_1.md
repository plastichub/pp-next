### Cutting Mill 

 Nearly all low speed shredders we saw in the community or around the internet were huge, heavy or sometimes not even working proper and efficient. Using high speed instead, we have the power of inertia :-). So we decided to shred plastic with human power at high speed.
There are different ways to build the axle. We decided to spend a bit more money on standard parts to reduce costs as machining and post machining on the lathe. That’s why we use those span sockets. However, we are aware that these components are not available in every country in the world. Take a look on the possibilities and competencies around you, it is also possible to manufacture the axle in a different way.

 
