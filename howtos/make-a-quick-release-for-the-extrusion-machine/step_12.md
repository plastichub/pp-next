### DONE! 

 Great you are ready to extrude!

To use the quick release you have to screw the female section onto your extruder, ensuring you are able to slide in from the top.

When in place you are ready to extrude.


You can find a full guide which includes the usage of the quick release in the How to “Extrude into a closed mould”.
👉 tiny.cc/extrude-into-closed-mould