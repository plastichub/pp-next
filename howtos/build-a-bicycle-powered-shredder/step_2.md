### Built the custom axle   

 With this custom flywheel and axle, you can create forward momentum to shred the plastic. The flywheel will also provide a perfect base to connect the gears talked about in step 5. All the cutting files and the technical drawings can be found in the files in step 1. 