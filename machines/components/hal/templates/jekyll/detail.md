<div style="clear:both">
    <div>
        <a href="${product_overview}">
            <p style="text-align: center">
                <img width="100%" src="${product_overview}">
            </p>
        </a>
    </div>
</div>

<hr/>

### Features

- Very precise auto-reverse when jamming. It stops after 3 trials and gives visual
- Status lights : Ok & Error
- Emergency and reset button
- Various motor protections (thermal & magnetic).
- Jam detection for extrusion mode.
- Stops automatically after one hour of inactivity.
- High quality cables and wiring according to industrial standards.
- High precision mounts for shredder & extrusion, making sure everything is aligned perfectly.
- Won’t start when power is back.

<hr/>