---
title: Resources
permalink: /resources/
breadcrumbs: true
layout : single
author: false
sidebar:
    nav: "resources"
type: resource
---

## Books


**Welding**

[Welding and Metal Fabrication Book pdf](https://boilersinfo.com/welding-and-metal-fabrication-book-pdf/?fbclid=IwAR0QttNIVyr3blF92p8WFmQu48Wln84uoCPjeND282pubGt4zAqi-ZeCmzs)

**Machining**

[Doing it better - Metal work - Advanced levels](https://www.amazon.com/Metalworking-Doing-Better-Tom-Lipton-ebook/dp/B00HKKEMXY) - by Tom Lipton

The bible : 'Machinery's Handbook' ([Amazon](https://www.amazon.com/Machinerys-Handbook-29th-Erik-Oberg/dp/083112900X) , [PDF](https://gitlab.com/plastichub/machinery-handbook))


### Extrusion

[Screw Feed Throughput Calculations](http://precious-plastic.org/screw-feed-throughput-calculations/)

[Extrusion Screw Analysis](http://precious-plastic.org/extrusionscrews/)

[Extruded Beam Cost Generator](https://docs.google.com/spreadsheets/d/1-L9mgjZ2ZIXi6qlmEPtT-lk3FdozrgemmA8xtPZcVCg/edit#gid=0)

### Electronics

**PID Controller**

[Fix your PID Controller](https://www.homebrewtalk.com/forum/threads/rex-c100-pid-owners-a-must-know-how-to-turn-off-digital-frequency.532794/)

[PID Controller Theory](https://controlguru.com/table-of-contents/)

[RexC100 - Autotuning (Forum)](https://davehakkens.nl/community/forums/topic/the-big-electronics-topic/page/3/#post-149159)

### Mechanics

[Gear Generator](https://geargenerator.com/#200,200,100,6,1,0,0,4,1,8,2,4,27,-90,0,0,16,4,4,27,-60,1,1,12,1,12,20,-60,2,0,60,5,12,20,0,0,0,2,-563)

[Engineering Toolbox](https://www.engineeringtoolbox.com/gear-output-torque-speed-horsepower-d_1691.html)

[ACME thread - torque calculator](https://www.amesweb.info/screws/LeadScrewCalculationsAcmeThreads.aspx)

[Gear Efficiency](https://www.machinedesign.com/mechanical-drives/gear-efficiency-key-lower-drive-cost)

[Pulley and Belt Calculator](https://www.blocklayer.com/pulley-belteng.aspx)

[Linkage Designer](http://blog.rectorsquid.com/linkage-mechanism-designer-and-simulator/)

[Gears Gears Gears](http://www.otvinta.com/download08.html)

### Motors

[Torque Calculator](http://wentec.com/unipower/calculators/power_torque.asp)

[Shredder Calculator by AndyN](https://davehakkens.nl/wp-content/uploads/2017/12/Will-it-shred-v1.xlsx) (see forum '[will it shred](https://davehakkens.nl/community/forums/topic/will-it-shred/)')

[Motor KW to Amps calculator](https://www.engineeringtoolbox.com/electrical-motor-calculator-d_832.html)

### Youtube Channels

**Machining**

[This Old Tony](https://www.youtube.com/channel/UC5NO8MgTQKHAWXp6z8Xl7yQ?&ab_channel=ThisOldTony) (Home machine shop)

[ClickSpring](https://www.youtube.com/channel/UCworsKCR-Sx6R6-BnIjS2MA?&ab_channel=Clickspring) ( Ancient tool making )

[Oxoolco](https://www.youtube.com/user/oxtoolco/featured) (Tool making and machining)

[NYC CNC](https://www.youtube.com/user/saunixcomp/featured) (CNC, machining, shop tours)

[Joe Pieczynski](https://www.youtube.com/channel/UCpp6lgdc_XO_FZYJppaFa5w) (General machining)

[MrPete222](https://www.youtube.com/user/mrpete222/featured) (Classic home shop guy)

[Abom79](https://www.youtube.com/channel/UCw3UZn1tcVe7pH3R6C3Gcng) (General machining)

[All about welding](https://www.youtube.com/user/Welddotcom?&ab_channel=Weld.com)

[The corvette guy](https://www.youtube.com/channel/UCR9OFVqdul6hJDfCifG36cQ) (CNC, micro manufacturing)

[Stefan Gotteswinter](https://www.youtube.com/channel/UCY8gSLTqvs38bR9X061jFWw) (Must watch)

[More here](https://www.madicorp.com/blog/top-machining-youtube-channels)

**Projects about plastic & recycling**

[YouTube.com/rubbishreads](https://www.youtube.com/rubbishreads) (for kids & teens)

**Electronics**

[All about PID controller wiring](https://www.youtube.com/channel/UCatCieEI4cPNteKXzBomVMQ/search?query=pid)

### Filament

[All about making filament](http://precious-plastic.org/library/resources/filament/)

[Making a filament extruder](https://precious-plastic.org/library/machines/filament-extruder/)