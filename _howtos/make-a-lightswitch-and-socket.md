---
image: "/howtos/make-a-lightswitch-and-socket/cover.jpg"
title: "Make a lightswitch and socket"
tagline: ""
description: "Making decorative objects and handcrafts is one thing, but is recycled plastic capable of replacing industrially manufactured and standardized components? The lightswitch and socket proof that!"
keywords: "product,mould,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "mould"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Make the moulds: CNC</h4>
<div class="step-text">
  Those pieces will be injected, so you need the moulds and access to an Injection machine.<br />
  <br />
  For the mould-making, download the files above and CNC-mill it yourself or send it to a mould maker. <br />
  <br />
  In the latter case, make sure to communicate clearly if your part is designed as the resulted part or already oversized to compensate for shrinkage. The file attached above is the actual size of the end result, so you need to scale it depending on your material. For the PP we used we scaled the model up by 2,6%.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/01_1.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/01_1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Make the moulds: Sliders</h4>
<div class="step-text">
  The lightswitch cover needs holes for sliders to achieve a snap connector feature which shapes an undercut.<br />
  <br />
  Drill the corresponding holes perpendicular to the unmoulding direction and use a tightly fitting pin (e.g. the back of the drill or a parallel pin) as slider.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/01_2.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/01_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/01_3.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/01_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Preparations</h4>
<div class="step-text">
  Due to the very thin walled diaphragm injection gate, preheating the lightswitch frame’s mould makes the process of injecting easier. The amount of plastic needed for all parts is about half a barrel, but rather work with a full barrel to ensure enough pressure.<br />
  <br />
  As the mould is quite intricate and detailed, a plastic with good flow characteristics is required. <br />
  <br />
  e injected PP on 270/280°C (barrel/nozzle), but a few tests might be necessary to calibrate your machine.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/02_1.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/02_1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Inject the parts</h4>
<div class="step-text">
  Time to inject. The plastic should be evenly molten to easily spread within the mould, so it might run out of the nozzle just because of gravity. Use a plug that’s screwed into the nozzle and remove it right before you start injecting.<br />
  <br />
  Act fast and keep the pressure for a couple of seconds before lifting the lever. This will prevent sink marks as the plastic is cooling down under pressure.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/03_1.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/03_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/03_2.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/03_2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Unmould your parts</h4>
<div class="step-text">
  Once injected, you can take out the sliders and open the mould. You don’t need to wait additionally for the plastic to cool down, the time it takes to open the mould is a sufficient cooldown time. <br />
  <br />
  Especially the lightswitch cover sits pretty tightly in the mould and levering with a spatula is needed. It helps much to lift the part evenly from each side to eject the parts.<br />
  <br />
  Take care that you don’t scratch the mould cavity with metal tools.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/04_1.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/04_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/04_2.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/04_2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Finish</h4>
<div class="step-text">
  Break the injection point along the predetermined breaking points. If necessary use a knife to shave off excess material.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/05_1.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/05_1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/05_2.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/05_2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/05_3.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/05_3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Mount it and love it!</h4>
<div class="step-text">
  Your parts are ready!<br />
  <br />
  Keep in mind that this product is in direct contact with high voltage electricity, so don’t try to mount it yourself unless you’re qualified. <br />
  <br />
  Should one of the parts break, you can simply shred and melt it again, or bring it to another Precious Plastic workspace where they can recycle the plastic. :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-lightswitch-and-socket/final.jpg">
        <img class="step-image" src="/howtos/make-a-lightswitch-and-socket/final.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>