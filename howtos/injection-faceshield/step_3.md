### Mount the Metal Pins 

 Depending on the pins you get you have to cut them to be shorter than 11mm.
We use H6x20mm pins and cut them to length.

Punch in the metal pins with a hammer, ore glue them in place if you don´t have a reamer.