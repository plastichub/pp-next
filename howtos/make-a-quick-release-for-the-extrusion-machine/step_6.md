### Connecting the strip to the metal body 

 Place the strip metal piece on to the plate not welded to the angle bar. No part of the strip should touch the angle bar. Then weld on the top and bottom ensuring that you weld the strip to only one plate, so you can still slide the mains parts independently.
