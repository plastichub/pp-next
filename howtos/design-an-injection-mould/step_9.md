### Use the PC tools 

 We’re looking for more accuracy than we can create on paper, so to get the real dimensions of the cuts and machining, now we can model our mold using CAD software. At this point we are continuing on our process from the previous step, just with a different tool. Using CAD we can model different versions of the mold and shape new or improved versions, stemming from the 6 Mold Design Criteria. 

Here some of the decisions I made during the process and mold design: 
- The best location for the injection point is in the middle of the body of the product. 
- To avoid the risk of pressure opening the mold, the injection point is better in a solid part than at the joining of the mold.
- The flange with the nipple connector should be attached over a flat face of the mold with the same closing screws.
