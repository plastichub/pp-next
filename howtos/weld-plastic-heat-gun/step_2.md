### Prepare the edges  

 In order to maximise the welding surface, we make a chamfer along the sides of both pieces. For that you can use a manual milling machine or a hand router with a V shape tool.
By making a chamfer we are increasing the surface that will be melted together with the welding line and therefore we’ll create a stronger weld.