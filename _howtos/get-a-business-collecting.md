---
image: "/howtos/get-a-business-collecting/Julie collection 15.jpg"
title: "Get a business collecting"
tagline: ""
description: "This is an example of how you can reach out to businesses and explain to them why, how and what you can do for them as a Collection Point."
keywords: "collection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "collection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get to know your surroundings </h4>
<div class="step-text">
  It’s time that the businesses in your surroundings, get to know you as a Collection Point. Researching in your local industry and how you are able to contact the businesses, would be a great start. If there is a Precious plastic community builder in the area you could ask him/her for any help. <br />
  <br />
  Another option can be that you organize a meetup this makes you able to reach out to different businesses at the same time. You can have a look at the how to - organize a precious plastic meetup to make this happen. <br />
  <br />
  Tip: Adding all the businesses to one list will give you a good overview of what they are + their contact details. We made an example of how this could look like, you can find it in the download kit.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/Step 1- Get to know your surroundings .jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/Step 1- Get to know your surroundings .jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/_3161887.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/_3161887.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/_3262114.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/_3262114.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Prepare your meeting </h4>
<div class="step-text">
  During your first meeting, you will briefly tell the business what, why and how you are running a Collection Point. Keep in mind that there is a possibility that the people that you will have the meeting with, don’t know anything about plastic pollution or precious plastic. Bringing something to visualize the Precious Plastic Universe can help you explain the story better. <br />
  <br />
  You are able to find a presentation in the download kit. This will help you give a clear explanation of what a Collection Point is, does and how it fits into the precious plastic universe. You can add something related to the company that you&#39;re going to, this will make them feel more related to the plastic problem. <br />
  <br />
  Tip: Practice the presentation a couple of times to feel more confident during your meeting.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/DSC_0511.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/DSC_0511.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Start contacting</h4>
<div class="step-text">
  It’s time to start contacting businesses, go through the list of local businesses and see how you are able to contact them, this can be for example by mail, social media, phone or by a visit on location and asking for the manager. <br />
  <br />
  Visiting the businesses and asking for the manager does take more time but it makes a big difference if they have seen you already once. Sometimes you are lucky and the manager is available straight away. Make sure that you are prepared for this and take your chance. <br />
  <br />
  Tip: Start with small businesses to get used to the way of communicating before you go to the big ones.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/start contacting.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/start contacting.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Reach out </h4>
<div class="step-text">
  When there is a possibility to arrange a meeting, you are now able to go to one of these businesses and present your collection point to them. This is an exciting moment. Keep in mind that you want them to start collecting plastic for you. <br />
  <br />
  A nice way to check if the business has plastic for you to recycle is to have a look together through their collected plastic (plastic packaging) from a week. Most businesses need the same materials weekly which also means the same packaging. If you have the time, explain why you can collect some plastic and why some not. <br />
  <br />
  Tip: Do not only tell your story but also listen well to their experiences with their plastic waste and their needs.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/Step 4- reach out.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/Step 4- reach out.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Agree on the conditions </h4>
<div class="step-text">
  After the meeting, you will give the business time to think about the collaboration. After a couple of days, you can arrange a new meeting or visit them again to sign an agreement/little paper that says, what you are going to pick up and when. This is nice from both sides to know what to expect. You can download and example template above. But feel free to change and edit to your local needs.<br />
  <br />
  Tip: Find the right timing to get back in touch. Don’t wait too long but also don’t be too impatient. <br />
  <br />
  Idea: You can give the business a sticker/sign to show everyone that they are part of the precious plastic universe.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/Step 5- stay in touch 2.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/Step 5- stay in touch 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/Step 5- stay in touch .jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/Step 5- stay in touch .jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Start collecting </h4>
<div class="step-text">
  Finally, it’s time to take care of your weekly/monthly pick up. Do a little check, when you receiving the collected plastic. If everything is collected in the way you did agree on (the right types, clean and label-free) you can take it to your Collection Point. The way of transporting the collected plastic depends on what suits your local environment. When everything works smoothly you can start collecting from multiple businesses and grow your collection network. <br />
  <br />
  Tip: Ask every now and then if everything is still fine and if you can do something to make your collaboration better.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/Julie collection 11.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/Julie collection 11.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/Julie collection 15.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/Julie collection 15.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/get-a-business-collecting/Julie collection 1.jpg">
        <img class="step-image" src="/howtos/get-a-business-collecting/Julie collection 1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>