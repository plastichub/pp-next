### Preparing the material 

 Get all the dimensions from your CAD model and cut all parts at the same time to speed up the process. Drill holes parallel to the long edge of the sheet metal with half the cross-sections material thickness as an offset.