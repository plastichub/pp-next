

{% if page.components %}

<h4>Required Components</h4>
<div class="ty-vendor-plans small">
    {% for component in page.components %}
        {% for doc in site.machines %}
            {% if doc.category == "component" %}
            {% if doc.product_id == component %}
                <div class="ty-grid-list__item_small">
                <a href="{{ doc.url  | relative_url }}" class="link">
                    <span class="image" >
                    <img class="cover_small" src="{{ doc.image }}" alt="" />
                    </span>
                    <header class="major">
                        {{ doc.title }}
                </header>
                </a>
                </div>
            {% endif %}
            {% endif %}
        {% endfor %}
    {% endfor %}
</div>
{% endif %}

