### Finish your work 

 You can push the injection lever to the top position, so you can start by adding fresh material the next time you start the machine. This makes sure, that the injection plunger won’t be stuck in the injection barrel.

Clean the area where you work from all leftover plastic. The next person who uses the machine will not know which materials you used!