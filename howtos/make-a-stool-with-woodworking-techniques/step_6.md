### Make the top 

 We’ll repeat a similar process to make the stool top. 

First we’ll fix the jig on top with double sided tape, you want to put enough so the jig doesn’t move later when pushing the tool against it. 

Using the handrouter for all the process first we’ll cut the outside and the four holes with the long bit, and later we’ll round the edges of all the contour of the stool as well as one of the small sides of the holes.

Check the download files to know exactly where to do it.
