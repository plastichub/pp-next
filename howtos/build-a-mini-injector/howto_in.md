### Build a Mini Injector 


This small addon for a drill press can enable you to try out injection moulding on a very low budget. It enables you to turn your plastic waste into cool products.

The only tools you will need for this project is a drill press and a metal hacksaw.
Most parts can be purchased from your local hardware store.

Download the files here: https://github.com/FriedrichKegel/Mini-Injector