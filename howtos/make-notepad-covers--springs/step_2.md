### Make the covers 

 Remove the small nozzle from the injection machine and extrude some plastic on a flat sheet, press it against another sheet in a press, wait for it to cool and remove the plastic sheet, cut it to shape, make the holes and assemble the notebook.