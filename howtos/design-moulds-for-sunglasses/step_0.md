### What you need 

 To make all the process yourself, you will need: 
1. A clear mind about the model you want to create. 
2. A computer and some experience working with CAD software.
3. A good CNC milling machine or a manufacturing partner.
4. An injection machine.

Ok. Let’s dive into the steps then.
