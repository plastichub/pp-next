### Start building 

 With the processes more or less figured out, we started producing the recycled parts for a 24m boat - They were HUGE.

The quality was very rough, but it was good to see that it was possible. And as this hasn’t been done before and was going to be a big investment, we decided to make a smaller 10m prototype first. 

 So in the next step you'll see how we actually made the Flipflopi Dogo (“dogo” = “small” in Kiswahili).