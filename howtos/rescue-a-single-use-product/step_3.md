### Create your own poster 

 Make an illustration of the product that you want to collect. This can be done digitally, drawn on paper, painted, etc. When the illustration is done you can add the sentences to make the whole poster speaking for itself.  

Give it an extra check before copying it, make sure that the poster is understandable for anyone by, for example asking feedback from people in your surroundings. 

Tip: Keep it clear and simple
