### Make the springs 

 Fill the injection machine and wait for the plastic to start flowing, with a small nozzle and a constant weight applied on the machine, use a wooden rod to "screw" the filament around it, make it as long as you want, we recommend using a wooden rod because the plastics sort of sticks and makes it easier to form the spring.