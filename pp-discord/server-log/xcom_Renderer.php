<?php
/**
 * @version 1.6
 * @link http://www.xapp-studio.com
 * @author XApp-Studio.com support@xapp-studio.com
 * @license : GPL v2. http://www.gnu.org/licenses/gpl-2.0.html
 */

$screen = (array) get_current_screen();
// error_log('screen ' . $screen['base']);
if ($screen['base'] === 'dashboard_page_siui') {

} else {
    return;
}

$XAPP_WP_NAME = 'xfile';
$XAPP_APP_FOLDER = "xfile";
$XAPP_APP_NAME = "xwordpress";

$ROOT_DIRECTORY_ABSOLUTE = realpath(dirname(__FILE__) . DS);

//wp related
$XAPP_PLUGIN_DIR_NAME = basename($ROOT_DIRECTORY_ABSOLUTE);
$XAPP_PLUGIN_URL = plugins_url('', __FILE__);

$appFile = file_get_contents(realpath($ROOT_DIRECTORY_ABSOLUTE . DS . 'assets' . DS . 'player' . DS . 'index.html'), true);
$ajax_url = admin_url('admin-ajax.php') . '?action=' . 'player-rpc';
$resource_url = admin_url('admin-ajax.php') . '?action=' . 'resource-rpc';
$rootUrl = get_si_setting(SIOptionKeys::$ROOT);
?>

<div class="loadingWrapper" id="loadingWrapper">
        <div class="loading">
            <div class="outer"></div>
            <div class="inner"></div>
        </div>
    </div>

<script type="application/javascript">
    var sessionReplay = {
        baseUrl : '<?php echo esc_url(plugins_url('assets/player/', __FILE__)) ?>',
        replayBase: '<?php echo esc_url(plugins_url('assets/player', __FILE__)) ?>',
        resourceRPC: '<?php echo $resource_url ?>',
        root: '<?php echo $rootUrl ?>',
        // workerScope: '/',
        hasConfig: true,
        toUrl:function(slug){
                return '<?php echo $ajax_url ?>' + '&view=' + encodeURIComponent(slug);
        },
        config:{
            router : '<?php echo $ajax_url ?>'
        }
	}
</script>

<?php

echo '<div id="root" class="sessionReplayRoot"></div>';
echo '<script type="text/javascript" src="' . $XAPP_PLUGIN_URL . '/assets/player/js/bundle.min.js"></script></body>';

// echo $XAPP_PLUGIN_URL;
