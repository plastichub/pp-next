### Fill out the Business Plan 

 The Business Plan template has two purposes: firstly, to clarify the idea for your and for your team, and secondly to communicate this idea clearly to potential funding and/or partners. This can be useful for grant applications, preparation for a bank loan or perhaps to take to an investor. Even if you are planning to get funding online, you can attach this document to convince more. 

For more explanation check out the video in the Academy:
👉 http://tiny.cc/business-plan-calculator