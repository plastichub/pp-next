### Find your local community 

 It is more practical and fun if you involve people from your local community, so go out there and connect! You can find community members on the Precious Plastic Map or meet them on local events and presentations about the environment and more.

Also see if there is a Community Point in the area and get in touch - they are the glue of the Precious Plastic Universe and might be able to help you in multiple ways with their overview of the people and spaces in the local recycling network.

👉 community.preciousplastic.com/map