---
image: /_howto/extrusion/screw_mount/IMG_2018.JPG
category: "extrusion"
title: "Screw Mount"
tagline: ""
description: ""
usedin:
  - lydia
  - zoe
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2008.JPG" _target="_blank">
        <img id="IMG_2008.JPG" src="./IMG_2008.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2009.JPG" _target="_blank">
        <img id="IMG_2009.JPG" src="./IMG_2009.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2011.JPG" _target="_blank">
        <img id="IMG_2011.JPG" src="./IMG_2011.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2012.JPG" _target="_blank">
        <img id="IMG_2012.JPG" src="./IMG_2012.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2013.JPG" _target="_blank">
        <img id="IMG_2013.JPG" src="./IMG_2013.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2014.JPG" _target="_blank">
        <img id="IMG_2014.JPG" src="./IMG_2014.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2015.JPG" _target="_blank">
        <img id="IMG_2015.JPG" src="./IMG_2015.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2016.JPG" _target="_blank">
        <img id="IMG_2016.JPG" src="./IMG_2016.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2017.JPG" _target="_blank">
        <img id="IMG_2017.JPG" src="./IMG_2017.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2018.JPG" _target="_blank">
        <img id="IMG_2018.JPG" src="./IMG_2018.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

