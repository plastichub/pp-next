### Join the mould 

 Closed moulds will come in two or more parts. In this example, the brick mould uses two parts. Clamp firmly together, we use locking pliers for this mould, which are quick to release while also acting as a good safety to release excess pressure. 
