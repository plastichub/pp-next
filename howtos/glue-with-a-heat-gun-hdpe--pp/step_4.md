### Recommended applications 

 Hot melts are generally accessible and easy to apply, but they offer a relatively weak bonding. But other than being a downside they can actually be a nice technique for designs which require a more temporary fix or assembly.

(Another very functional application: This adhesive can help making other joinery methods watertight by sealing the surface.)