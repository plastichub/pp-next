### Make changes 

 This can be done in the browser or on your local computer.

In the browser you can edit the files by clicking on the file name & then the edit button. It will open a new page where you can edit it & preview the changes. If you’re uploading many files you can use the upload button & drag and drop them. You’ll need to add a message for what changes you’re making in the bottom of the page.

To edit on your local computer you will need to clone your repository, make the updates locally, commit to the repository & push back to the server. If you’re working on a GitHub project if you want to work on it locally we recommend using the desktop app as it is quite intuitive and open source too!