### Make a carabiner (CNC vs. lasercut) 


 A carabiner is a useful token that can be given away after an injection moulding workshop at Precious Plastic. Its small size allows you to make up to 6 pieces in one mould, and no assembly needed after injection.

Each of them will come out with a different marbling pattern, making them unique and memorable!
