### Material for the pedals 

 - metal sheet (steel 4mm thick; 120 x 240; 26 x 700 mm)
- welding rod (2 or 3 mm)
- 3 bolts M6 x 10 (any size would works but you need the corresponding drill bits)
- 2 pedal axles with ball bearings, washers and nuts
- wood (for positioning during welding, optional) (pedal size)
- 4 bolts and nuts M4 x 40 (only length matter)
- 24 setscrews M6 x 8