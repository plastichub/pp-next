### Check out your area 

 Get an overview of who and what is already existing in your area. 

Have a look on the Precious Plastic Map to see the activity around you and if there are pins you can get in contact with. 

👉 community.preciousplastic.com/map

Make sure not to jam the local network - if there is a Community Point in your area already, get in touch and have a look how you can work together, to keep ONE clear contact point for the surrounding people of your network :)

Additionally, you can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. 