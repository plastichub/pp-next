### Prepare your polystyrene 

 Selecting the right material is crucial when it comes to aesthetics. What properties does the material have, what colour is it? 
As PS stays transparent in the recycling process, it has the perfect quality to make a light. Collect your clean PS, which is typically found in old CD and cassette cases. Make sure to shred enough, the required amount will vary depending on the size of your mould.