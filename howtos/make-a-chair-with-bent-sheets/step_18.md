### Backrest connector: Let the frame immerse 

 To make the steel tube of the frame immerse nicely into the backrest, you can make a hole with a hole saw made from the same tube: Cut some teeth into the tube end with an angle grinder and file them a little. Then find or make a fitting shaft that fits into your cordless drill’s chuck, weld it on the tube and maybe use a lathe to achieve a straight rotation axis.

Put your connecting plugs in place and use them to guide your hole saw while you’re cutting a smooth slot. Remember that PS melts quickly, so give the tool some cooling time and sharpen the teeth regularly.
