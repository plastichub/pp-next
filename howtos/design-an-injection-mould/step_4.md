### Use standard measures 

 In my country (Colombia) the material dimensions mostly come in Inches (metal bar sections, steel planks thicknesses…), so are easier and cheaper to find. Adapting your measurements to the standard ones will avoid paying for excess material that you will then have to pay to remove.
So for the inner cylinder, I’ll use a ¾ inch rod (19,05mm), close enough to the 21mm of the broomstick. For the outer wall, I’ll use a 1 inch tube (25,4mm), so the thickness of the broom hanger will be approximately ⅛ inch (3.17mm).
Explore your local metal market and find which measures fit better for you!
