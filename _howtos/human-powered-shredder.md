---
image: "/howtos/human-powered-shredder/Untitled-1.jpg"
title: "Human powered shredder"
tagline: ""
description: "The Precious Plastic machines are open source, which means you can adapt it to your requirements. So if you don&#39;t have or don&#39;t want to use a motor, there are other more &#39;humane&#39; ways to power the shredder!"
keywords: "hack,research,shredder"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
- "research"
- "shredder"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/javier-singular-mars-ltd">javier-singular-mars-ltd</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Disclaimer</h4>
<div class="step-text">
  Before we start, let us mention that this guide is supposed to be informative, only for you to be inspired about the possibilities of machine design.<br />
  <br />
  We&#39;re not responsible for the design or (mal)functioning of your machine, please seek professional help when needed.<br />
  <br />
  Always follow safety recommendations and practices when building machinery and using tools.<br />
  <br />
  Design, build and hack this machine at your own risk!<br />
  <br />
  The pictures and 3D models in this guide are from particular one-off designs, not to be taken literally as they may not make sense for your requirements.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitled-2.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitled-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - User Requirement Specifications</h4>
<div class="step-text">
  Or URS for short is just a fancy way of saying &#39;identify and define the characteristics of what you want to make&#39;. In other words, before you build the machine, these decisions will have an influence in the design:<br />
  <br />
  - How to power it. You can use a motor (single phase or three phase?), human power (hand-cranked or pedal powered?), animal power (a little hamster in a wheel?), other natural source of energy (wind or hydro power? nuclear? dark matter?)<br />
  <br />
  - How much use it&#39;s going to get. If you plan on using it once a month, perhaps it makes sense to keep it cheap and simple (hand cranked power). If you plan to use it for a few hours a day, you might want something other than your arms to power it.<br />
  <br />
  - What materials do you have available? If you have a salvaged motor maybe it can be adapted. Or if you have an exercise bike that you don&#39;t use anymore, you have half the work done for you.<br />
  <br />
  - What skills and processes do you have available? If you can weld, the standard design of the machines would work for you. If like me, you can&#39;t, you&#39;ll have to outsource the task to someone else (which can cost you more than you&#39;d like) or redesign it.<br />
  <br />
  - Is it going to be used off-grid? If access to electricity is difficult then that&#39;s going to drive the design to a non-electric one.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitled32.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitled32.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - To weld or not to weld</h4>
<div class="step-text">
  If you can&#39;t weld steel, or don&#39;t have access to it, there are other techniques that you can use for the frame:<br />
  <br />
  - Steel sections can be fastened with nuts and bolts. You save the welding time, but need to drill holes instead.<br />
  <br />
  - You can use aluminium extrusion T-slot profiles instead, which are easy to bolt and work with.<br />
  <br />
  - Be careful if you use wood as an structural element. Some parts of the shredder may be subject to strong forces that wood may not resist.<br />
  <br />
  As for the shredder itself, it can be modified so that it doesn&#39;t need welding, but would require a different technique: precision engineering (holes and dimensions need to be very accurate and perfectly square)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitleda2.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitleda2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitled-12.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitled-12.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitledd2.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitledd2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Reduce the speed, increase the torque</h4>
<div class="step-text">
  The key in the shredder performance is to make it turn with high torque. That will allow it to shred thicker plastics (1-3 mm) easily. So what we want to do is reduce the speed we apply to the machine and transform it into torque. There are several techniques for power transmission:<br />
  <br />
  - A gearbox. In a very compact unit, we can reduce the speed in ratios of up to 100:1. Usually they have a wormgear internally, so it will block in case of a clog and it won&#39;t back-drive.<br />
  <br />
  - Pulleys and belts. The benefit of these are that they can transmit power but they can also slip, which can be good when there&#39;s a clog in the shredder.<br />
  <br />
  - Gears<br />
  <br />
  - Chains and sprockets
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitl3-2.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitl3-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - I believe I can FLYWHEEL</h4>
<div class="step-text">
  The magic of the flywheels is that it gives the machine inertia: When it&#39;s turning, it keeps the machine moving with little input. Remember to put it on the &#39;fast&#39; side of your mechanism.<br />
  <br />
  The factors that influence the efficiency of the flywheel is its weight, shape and speed. Usually a disc or wheel works best, a distribution of weight away from the axis helps. The faster it moves, the better it is at mantaining rotation with little effort.<br />
  <br />
  Think of the exercise bike in the picture: the flywheel on the front will keep turning even if you&#39;re not pedalling.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitledad2.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitledad2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitleadd2.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitleadd2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Safety first! (and last)</h4>
<div class="step-text">
  Make sure to incorporate guards and covers so that fingers can&#39;t reach any moving parts at any points, minimizing the risks of injury
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Unaatitledd2.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Unaatitledd2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/human-powered-shredder/Untitleddaa2.jpg">
        <img class="step-image" src="/howtos/human-powered-shredder/Untitleddaa2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>