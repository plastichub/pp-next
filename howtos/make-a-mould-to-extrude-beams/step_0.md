### Get ready 

 This type of beam mould consists of a plate that is screwed onto the nozzle and a beam mould that can be easily attached and detached for fast production. So far this has been the go-to method for beam production because it's possible for one plate to be used with different beams. 

To make a beam mould you will need: 
- Metal tube (Preffereably with a wall thickness of 3mm or more)
- Angle iron
- Metal sheet (3mm or more)
- Threaded pipe or fitting  (BSPT size of your nozzle)
- Angle grinder / Metal saw
- Welding machine
- File 
- Drill 
- M8 or M10 nuts & bolts
