### Design your structure 

 
First of all, decide how big your dome should be and calculate the required sizes of the elements. Consider that the diameter of the structure equals double of its height.

There are several tools on the internet to help calculating the size of each individual beam. This is the one we used: 
http://www.domerama.com/calculators/2v-geodesic-dome-calculator/
You can put in your dome size and it will calculate the required beam lengths for you. 

In any case, we have to shorten the lengths a bit so that they work with our joints.
Short beams: - 64.5mm 
Long beams: - 73.4mm

In this example, we are making a 2.5m high dome (5m diameter).
