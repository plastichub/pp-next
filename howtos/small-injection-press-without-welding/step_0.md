### Get everything your need 

 Gather everything you need,

Our goal was to build a small press with standard stuff pipes without need of welding stuff.

You will need :

2x  Wood block 60x80x190mm (can be adapt)
1x   Wood Board 200x300x20mm
1x   Car Jack
4x   Straight Pipe 1/2"x 160mm
2x   Straight Pipe 1/2" x 85mm
4x   90° Pipe 1/2"
2x   Double female connector 1/2"
2x   Male wall fixation plate 
1x   T Pipe 1/2"
1x   M10 Screw x 140mm
4x   Washer M4
12x  Wood Scew 4x60mm
4x   Wood Screw 4x100mm


Here we join the blueprint for the whole assembly, thanks volks.eco for the 3D model and draw made with Fusion360.

