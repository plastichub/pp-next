### Share your event 

 Once you have the date and place for your meetup, let your group know about it! 

If you want to keep the meetup only for the contacts you already have, it’s probably enough to announce it in your communication channel.

If it will be open for newcomers to join, it’s important to spread the word: You can create an event online and share it on social media or your local news.

And you can add your meetup as an event to the Precious Plastic event page, so the community can find you more easily.

👉 community.preciousplastic.com/events