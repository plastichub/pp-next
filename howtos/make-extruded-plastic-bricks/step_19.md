### Ready to make bricks!? 

 Well done, now you’re ready to extrude!

Find advice for the extruding process in the How-to “Extrude into closed moulds”:
👉 tiny.cc/extrude-into-closed-mould

And learn how to "Build brick structures":
👉 tiny.cc/build-brick-structures
