### Learn about plastic collection 

 Next, you can get familiar with how plastic collection works around the world and the Precious Plastic plan with our Collection video.

👉 community.preciousplastic.com/academy/collect

These are the main things to take from the video: 
- Educate and enable citizens to be the change.
- Collect clean label-free plastic.
- Use the map to help people find Collection Points.
- All info and how it works are on the website
👉 collect.preciousplastic.com.

