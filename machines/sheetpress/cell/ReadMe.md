
## Status : broke, don't use !!

## Sheetpress-Cell CAD Overhaul

Here I will document my work to tidy up and overhaul the CAD files for the Sheetpress.

Commenced 03/05/20


03/05 

Started to find my way around the model. 

Got rid of a number of mates errors. 

Discovered a number of parts are imported rather than native so decided to rebuild the frame and slider mechanism.

Rebuilt frame using mirror components

Rebuilt Slider mechanism as 2 separate assemblies to allow movement

Rebuilt press plate frame with separate side rails for top and bottom

Removed most nuts and bolts to re add later using mirrors


Notes: 

New frame does not look as good because of joints. Weld does not work on filleted edge. To be addressed.

Dimensional tolerance on width is tight

Lots of work still to simplify the pressing plate assembly

Need to confirm how much metal has already been cut

NB Hole positions on frame ARE slightly different to previous model, to give 350mm and 700mm centres where possible


Committing Sheetpress-overhaul-0.1
Main working assembly is Sheetpress Global Assembly 0.1 (Current work in progress)

time: 12hrs

Julian

Well I have mentioned a few times that there was a problem with the strategy of overshooting melting temperature by 50 or 60 deg C. It generates fumes and degrades the plastics seriously. I am very concerned by this advice and I am basically told to just get on with it. Here a little explanation of what is going on:
You may know that plastic is a good thermal insulant, meaning its thermal conductivity is low. That means that if you heat up a mass from the outside, it will take a long time to be at the same temperature in the inside. The gradient between the core and the outside of what you want to melt is depending of the thermal conductivity. For the sake of simplicity, lets say the "instantaneous" thermal gradient needed to melt your example is 60 deg C. 
So to get the inside to melt, either 
(1) you heat up to the melting temperature or just above and wait that the heat transfers inside let say it takes 12h or 
(2) you rise the outside temperature 60 degrees above the melting temperature so "relatively" quickly the inside reaches the melting temperature. 
(2) is what the Team is advising and I am firmly against it because while you do that, the outside of the mass (here sheet) is at 220-240 degs, the mass that is overheated generates a lot of fumes and looses its chemical properties and basically its capacity to bond. That's why many complain that the sheets have a random output.  All the charts should be changed with a proper time-to-temperature-to-mass output. I find the current advice dangerous.