#ifndef CONFIG_H
#define CONFIG_H

#include "enums.h"
#include "common/macros.h"
#include <Controllino.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Core settings
//

#define LOOP_DELAY 10   // Our frame time, exluding delays in some places
#define BOOT_DELAY 4000 // Wait at least this amount in ms after boot before doing anything

#define PRINT_VERSION

// Interval to read analog inputs
#define ANALOG_READ_INTERVAL 200

// Please consider to set this to false for production - especially with the full feature set since this is requiring extra
// time for the serial communication and will affect the overall framerate/performance
// #define DEBUG

#define DEBUG_INTERVAL 300
#define DEBUG_BAUD_RATE 19200 // Serial port speed

#define RELAY_ON 0    // The relay bank's on value (eg: normally closed)
#define RELAY_OFF 255 // The relay bank's off value (eg: normally closed)

#define DIR_SWITCH_UP_PIN CONTROLLINO_A0
#define DIR_SWITCH_DOWN_PIN CONTROLLINO_A1

#define SHRED_START_PIN CONTROLLINO_A3

// #define IGNORE_FIRST_DIRECTION              // Uncomment to ignore the 3pos switch (forward/reverse) after booting. This prevents surprises but possibly also accidents.
//#define DIR_SWITCH_DELAY        500      // If defined, add this as blocking delay between direction changes. Needed for some types of relays.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Machine settings
//

// #define USE_MEGA                               // On Arduino Uno we have only limited ports which are not enough to enable all features.
#define USE_CONTROLLINO

// the relay type, please toggle if needed
#define POWER_NC true

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    VFD related
//

// VFD FWD Pin. This goes into the relay and then into the VFD.
#define FWD_PIN                     CONTROLLINO_D4

// VFD REV Pin. This goes into the relay and then into the VFD.
#define REV_PIN                     CONTROLLINO_D5

// Motor overload pin, if defined, this will be used to detect jamming
// On an OmronM2X, set the output via C21 (for output pin 11) to 03 for
// 'Overload' (which is set in C41 )
#define MOTOR_LOAD_PIN              CONTROLLINO_A2

// the time needed to boot up the VDF, only used when we control also the power via POWER_0
#define VFD_BOOT_TIME               500

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Motor related
//

// When shredding, stop all after this time. Uncomment to activate.
// This will be only used when AUTO_REVERSE_MODE is not AR_MODE::EXTRUSION.
// That means of course that shredding will always stop after one hour. 
// I considered this a safety feature and will be of importance when the operator is absent unexpectly.
// @TODO: in case there is a VFD speed sensor, or a hall sensor : augment mean deltas to determine 'activity' which
// will then extend the time. It may make sense to apply this feature also for extruding then.
#define MAX_SHRED_TIME              MIN_MS
#define MAX_IDLE_TIME                   SECS * 20

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Auto-Reverse
//

#define HAS_AUTO_REVERSE

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    General switches

// Operation Mode Switch
#define OP_MODE_1_PIN CONTROLLINO_IN1
#define OP_MODE_ANALOG

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Feedback

// Status LEDS (ok, error) - uncomment to activate
//#define HAS_STATUS
#define STATUS_OK_PIN A3    // This goes into the relay
#define STATUS_ERROR_PIN A2 // This goes into the relay

// Hopper - Sensors (For hopper with flip back joint and/or door) - uncomment to activate
// #define HOPPER_DOOR_PIN     11            // uncomment to activate - sets the input pin for the proximity sensor of the hopper door
// #define HOPPER_PIN          12            // uncomment to activate - sets the input pin for the proximity sensor of the hopper (if with flip joint)

// Hopper - feed sensor
// uncomment to activate - sets the input pin for the PIR sensor - signaling that there is something to shred.
// This is under development and being used in v3.5 builds. This is being used to power up the shredding system
// when anything is tossed in the feeder.
// Not impl. !
// #define HOPPER_FEED_SENSOR 13

#define FEED_READ_INTERVAL 1000 // Interval to read the feed sensor

#define FIRMWARE_VERSION 0.5

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plastic Hub Studio - internals : used by external controller setups
// Make sure it's matching
#define FIRMATA_BAUD_RATE 19200

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plastic Hub related sensors and addons

// #define ENCLOSURE_SENSOR_PIN_1      13
// #define ENCLOSURE_SENSOR_PIN_2      12

// #define CARTRIDGE_FULL_1            35
// #define CARTRIDGE_FULL_1_INTERVAL   100

// #define HOPPER_LOADED_0                CONTROLLINO_A3
// #define HOPPER_LOADED_1 28
// #define HOPPER_LOADED_INTERVAL         50

// Addon which drives a plunger up & down
// uncomment to activate

////////////////////////////////////////////////////////////////
// Auto-plunger

#define HAS_PLUNGER

#ifdef HAS_PLUNGER
    
    // motors
    #define PLUNGER_MOTOR_1_DIR_PIN CONTROLLINO_D6
    #define PLUNGER_MOTOR_1_STEP_PIN CONTROLLINO_D0
    #define PLUNGER_MOTOR_2_STEP_PIN CONTROLLINO_D1

    // limit switches
    #define PLUNGER_LIMIT_UP_1 CONTROLLINO_A4
        // limit switches
    #define PLUNGER_LIMIT_DOWN_1 CONTROLLINO_A5

    // timeout to trigger error in case there is a mechanical problem (coupling got loose)
    #define PLUNGER_PLUNGE_TIMEOUT SECS * 8

    // timeout to trigger error in case there is a mechanical problem (coupling got loose)
    #define PLUNGER_HOME_TIMEOUT SECS * 5

#endif

////////////////////////////////////////////////////////////////
//
// HMI Bridge
//
// uncomment to activate HMI uplink, this will process messages sent over serial
// #define HAS_BRIDGE

// select serial (currently the only supported protocol)
// #define HAS_SERIAL
// uncomment to send states, only needed for HMI
// #define BRIDGE_ONLINE_PIN                CONTROLLINO_D17

#ifdef HAS_BRIDGE
    // #define HAS_STATES
#endif
// #define HAS_STATES

////////////////////////////////////////////////////////////////
//
// Instrumentation
//
// #define MEARSURE_PERFORMANCE

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    externals
//

// pull in internal constants
#include "constants.h"

// pull in internal configs
#include "config_adv.h"

// The user_config.h is initially added to the github repository but changes will be ignored via .gitignore. Please keep this file safe and possibly
// on a per tenant base stored. You can override parameters in this file by using #undef SOME_PARAMETER and then re-define again if needed, otherwise disable
// default features by using #undef FEATURE_OR_PARAMETER.
// This presents the possibilty to play with the code whilst staying in the loop with latest updates.
#include "user_config.h"

// At last we check all configs and spit compiler errors
#include "config_validator.h"

#endif
