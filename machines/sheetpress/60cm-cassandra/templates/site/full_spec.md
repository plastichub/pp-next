---
title: "Cassandra v1.0 - revision-A DataSheet" 
permalink: "/specs/products/cassandra"
layout: "print"
---

## {{page.title}}

### Specification

| Specification    |     |
|----------|-------------|
| Title   |  Cassandra     |
| Version | v1.0 |
| Type   |     Sheetpress   |
| Weight |   100 kg   |
| Package Dimension   | 1052 x 1100 x 900 mm |
| Power (W) | 3 kW |
| Voltage | 380V or 220V |
| Amperage | 16A |
| Input Flake Size  | Small  |
| Screw diameter | 30mm |
| Rated Motor Power | 3 kW |
| Motor Type   |    |

<div class="col">
    <h3 class="text-center" id="dimensions">Dimensions</h3>
    <div class="vertical-margin vertical-gap">
        {% include product_image.html size="medium" src="/products/cassandra/drawings/dimensions.JPG" target="/products/cassandra/drawings/dimensions.PDF" %}
    </div>
</div>

### Components & Parts

{% include product_image.html size="small" src="/products/cassandra/drawings/parts.JPG" %}
