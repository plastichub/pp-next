# Auto-Tuning - E5

## Setup

* AT (100%) started at 80Degc
* 2 brass heaters (nozzle zone) | 300W with 3 cm gap, TC in the middle
* With screw, 40 mm barrel, 30mm screw
- no insulation
- no neighbour heating
- Control-Period : 2secs
- SP = 150 - 200
- Start Heat : 80 DegC

## Results

- AT took 2 cycles (section 0) to settle PID params. Overshooting for each cycle was around 15 Degc
- After AT done, there was an average error of 1 DegC (secion 2), peaks at 2 DegC
- Little blows with breath only affect TC readings by up to 3 DegC, medium air circulation (at 20Degc) has affects of up to 5 DegC (non-insulated)

![](./at_log_e5_hold.JPG)

![](./at_log_e5_hold_long.JPG)

After one hour holding 150 DegC, SP has been set to 200 DegC, just to see that the determined AT PID parameters are applied for other temperatures well. Again, mostly 1 DegC error.

![](./at_log_e5_hold_long_200.JPG)

Zoomed : 

![](./at_log_e5_hold_long_200_zoom.JPG)


See [more in the Excel trace, (output flags)](./at_100_warmstart_OmronE5_150_to_200.xlsx)

### PID Params

- Proportional Band : 28.9
- Integral Time : 298
- Derivative Time : 51

####  Registers (@todo)

- Derivative Time (Cooling)     : 328
- Dead Band ()                  : 1298
- Hysteresis (Cooling)          : 336
- Open/Close Hysteresis         : 901
