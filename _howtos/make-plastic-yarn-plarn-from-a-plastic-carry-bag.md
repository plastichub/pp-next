---
image: "/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/for-pp.jpg"
title: "Make plastic yarn (plarn) from a plastic carry bag"
tagline: ""
description: "With this technique we will get a single, continuous, seamless strand of yarn without any joining knots.<br /><br />All you will need is a plastic bag, a pair of scissors and any object that can be used as a weight (your mobile phone will do!).<br /><br />Once you have this yarn you can apply any handcrafted technique - knitting, crochet, weaving, macramé or braiding!"
keywords: "HDPE,LDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "HDPE"
- "LDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/thla">thla</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Video Tutorial</h4>
<div class="step-text">
  This video explains the entire process. <br />
  <br />
  Once you watch it, you can follow the detailed steps below to make your yarn.<br />
  <br />
  Enjoy!
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cutting off the Seam</h4>
<div class="step-text">
  Image 1:<br />
  <br />
  Let&#39;s consider the plastic bag to be made up of the seam, the body and the handles. Knowing these terms will help us during the process of making the yarn.<br />
  <br />
  <br />
  Image 2:<br />
  <br />
  Start by flattening the plastic bag and removing all the creases.<br />
  Cut off the seam.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/01.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/01.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/02.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/02.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Folding the Bag</h4>
<div class="step-text">
  Image 3:<br />
  <br />
  You will notice that your bag has 2 flaps or folds. <br />
  (Most plastic bags have it, but if yours doesn&#39;t, then Image 3 and Image 4 do not apply.)<br />
  <br />
  <br />
  Image 4:<br />
  <br />
  Take the flap that is away from you and fold it in, towards you.<br />
  <br />
  <br />
  Image 5:<br />
  <br />
  Now, fold the bag in half, not all the way to the end but leaving a margin of about 3 to 5 cm.<br />
  Fold it a couple of times more.<br />
  If your plastic bag is thick, you can fold it a fewer times so that it is easier to cut.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/03.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/03.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/04.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/04.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/05.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/05.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Cutting the Strips</h4>
<div class="step-text">
  Image 6:<br />
  <br />
  Place a weight at the end of the bag, on the handles.<br />
  We will be cutting parallelly along the length of the bag, not all the way till the end but until the margin.<br />
  Make cuts all the way until the handles. Cut the handles off.<br />
  <br />
  Note: <br />
  - Make cuts roughly at an equal distance from each other to get a yarn of uniform thickness.<br />
  - You can cut the yarn as thick as you like depending on your project. In the video, I have cut the yarn about 1.5 cm apart which is good to crochet with with a 3 mm hook. If you would like to make something chunkier, you can cut it 5 - 8 cm apart. Of course, the thickness of the yarn also depends on the thickness of the bag, so just play around!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/06.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/06.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Making the Diagonal Cuts</h4>
<div class="step-text">
  Image 7:<br />
  <br />
  Unfold the strips of the bag.<br />
  <br />
  <br />
  Image 8:<br />
  <br />
  Open out &amp; unfold the margin area.<br />
  <br />
  <br />
  Image 9:<br />
  <br />
  Instead if cutting parallelly, we will be cutting diagonally so that we get a continuous spiral.<br />
  Start by cutting the the first end off, the cut that is towards you.<br />
  Now cut from the 2nd cut towards you to the 1st cut on the other side, away from you. <br />
  Keep doing this until you reach the end. Cut the last end off.<br />
  <br />
  <br />
  And the yarn is ready, hooray! <br />
  Ball it up or wind it around a bobbin.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/07.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/07.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/08.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/08.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/09.jpg">
        <img class="step-image" src="/howtos/make-plastic-yarn-plarn-from-a-plastic-carry-bag/09.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>