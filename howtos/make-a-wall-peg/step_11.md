### Drill the screw holder hole 

 Drill a ⅛” hole in the center of parts no. 5 and 6. Insert a screw to create the thread in the wood. (See drawings pages 10-11-12)
