#ifndef FEATURES_H
#define FEATURES_H

#ifdef HAS_BRIDGE
  #include "bridge.h"
#endif

#ifdef HAS_SERIAL
  #include "serial.h"
#endif


#ifdef HAS_DIP
#include "_DipSwitch.h"
#endif

#ifdef HAS_TEMPERTURE
#include "temperature.h"
#endif

#ifdef HAS_SOUND
#include "alarm.h"
#endif


#ifdef HAS_STATUS
  #include "addons/Status.h"
#endif

#ifdef HAS_RESET
#include "reset.h"
#endif

#if defined(POWER_0) || defined(POWER_1)
  #include "addons/Power.h"
  #define HAS_POWER
#endif


#ifdef HAS_DIP
  #include "addons/_DipSwitch.h"
#endif



#if defined(OP_MODE_1_PIN)
  #include "addons/OperationModeSwitch.h"
  #define HAS_OP_MODE_SWITCH
#endif

#if defined(PRESS_SERVO_STEP_PIN) && defined(PRESS_SERVO_DIR_PIN)
  #include "components/ExternalServo.h"
#endif

#if defined(FWD_PIN) && defined(REV_PIN)
  #include "addons/DirectionSwitch.h"
  #define HAS_DIRECTION_SWITCH
#endif

#include "addons/DirectionSwitch.h"
#include "LinearActuator.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Plastic Hub - Addons
//

// Plastic Hub Studio  - remote control
#ifdef USE_FIRMATA
#include "firmata_link.h"
#endif


#endif