## Next door vendors

- [Shredding](http://www.ap-3.com/en/)

- [CNC / VMC & Lathe](http://www.brugacnc.com/)

- [Manufacturing](http://vmsystems.es/en/)

- [Drying](http://www.ingetecsa.com/en/home.html)

- [Old school turning/lathe shop](https://www.google.com/maps/place/Sentmenat,+Barcelona/@41.6109458,2.1409772,61m/data=!3m1!1e3!4m5!3m4!1s0x12a4ea8b78d890dd:0x400fae021a41290!8m2!3d41.6113574!4d2.1352039)

- [PVC Extrusion](https://cayfi.com/en/extrusion-tools-plastic-profiles)

### Nearby vendors

- [Zinc/Galvanizing](http://www.unizinc.com/home.php)



