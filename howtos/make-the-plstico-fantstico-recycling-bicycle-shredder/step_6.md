### Attaching the Gear  

 Next, the cut off square taper will need to be welded onto the axle. Once the square taper is welded on and lathe down so that it can be thread back through the ball bearing the gear can be attached using the crank bolt to hold it tightly in place.

The crank arm will need to be cut off using an angle grinder, then again using the angle grinder the crank can be shaped to smooth it back and create a formed shape.
