## Hydraulic Power Pack

# Reservoir

"A suggested minimum total reservoir volume is 5/8 of the maximum charge pump flow per minute with a minimum fluid volume equal to 1/2 of the maximum charge pump flow per minute. This allows 30 seconds fluid dwell for removing entrained air at the maximum return flow." [Brendan Casey, How to size a hydraulic tank](https://www.hydraulicsupermarket.com/blog/all/how-to-size-a-hydraulic-tank/)

Therefore at suggested 30L per minute, the minimum tank size should be 18.75L; It should hold 1/2 the volume of oil output per minute, so 15L of fluid.
We will go a bit bigger...


# Specifications (suggested)


- Fill Volume: 10 Gal.(37.8 litres)

- Total volume: 16.35 Gallons (61.9 liters) to plate; 10.15 Gallons (38.4 liters) to top of level gauge

- Dimensions: approx W400xL400xH387mm internal; 2.6mm mild steel plate

- Suction Line Outlet - to terminate well below fluid line, near to bottom of reservoir, end cut at 45 degrees

- Return Line Inlet - to terminate above fluid line (to avoid backpressure), having passed through filter at low pressure

- Baffle - central half wall; helps to reduce turbulence, aids cooling and helps settlement of contaminants

- Oil Level Inspection Chamber with thermometer

- Sensor Mounts

- Cleanout cover with seals - can be the main mounting plate

- Breather Vent with air filter

- Filler Port with mesh strainer and screw cap

- Drain Port with magnet

- Floorstanding support to prevent base corrosion

