### Make some products! 

 Now that you have all the things in place it’s time to start injecting some products. Browse the How-to and Create chapter in the Academy to learn how to make an injected product and adopt the best practices.