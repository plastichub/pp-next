---
image: "/howtos/make-a-mould-to-extrude-beams/1 fin.JPG"
title: "Make a mould to extrude beams"
tagline: ""
description: "Beams make great use of the extrusion machine as they can be strong and enable you to create very unique patterns and colours. This guide will show you how to make a mould which can be easily translated to any size of beams. <br /><br />(Update: We also added a lasercut version, see step 7!)"
keywords: "extrusion,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  This type of beam mould consists of a plate that is screwed onto the nozzle and a beam mould that can be easily attached and detached for fast production. So far this has been the go-to method for beam production because it&#39;s possible for one plate to be used with different beams. <br />
  <br />
  To make a beam mould you will need: <br />
  - Metal tube (Preffereably with a wall thickness of 3mm or more)<br />
  - Angle iron<br />
  - Metal sheet (3mm or more)<br />
  - Threaded pipe or fitting (BSPT size of your nozzle)<br />
  - Angle grinder / Metal saw<br />
  - Welding machine<br />
  - File <br />
  - Drill <br />
  - M8 or M10 nuts &amp; bolts
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cut beam</h4>
<div class="step-text">
  After planning your mould and gathering your materials, it&#39;s time to cut them up. Cutting your metal tube is simple enough - make it as long as you want your beam. Take note that the longer your beam, the harder your machine will need to work to fill the entire mould.<br />
  <br />
  Pro tip: It&#39;s important to remove any burrs and file the edges here, as this will ensure a better fit in the stages to come. Cut a thin slice extra to use as a template to use in the step 4.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-2-1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-2-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-2-2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-2-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make mounting brackets</h4>
<div class="step-text">
  For connecting the beam to the plate we need to make mounting brackets.. angle iron works well for this. All you need to do is cut angle iron into sections of 30-40mm and drill a hole in one side to fit your bolts. For larger beams use bigger, thicker angle iron. <br />
  <br />
  Pro tip: This is a relatively simple process.. so you can save some time here by making lots of brackets in different sizes at once. When you need to make more moulds in future you&#39;ll thank yourself for that little extra you put in.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-3-1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-3-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-3-2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-3-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Cut and plan plate</h4>
<div class="step-text">
  We can now begin with the plate. Use the size of your beam section to plan the rough size of your mounting plate. Cut a shape from your sheet that will fit your beam mould as well as the brackets cut in step 3. <br />
  <br />
  Pro Tip: Use the thin section of your beam from step 2 as a template for marking the position of your brackets and their holes. <br />
  <br />
  Cut the sheet, drill the holes.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-4-1.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-4-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-4-2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-4-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Welding the mould</h4>
<div class="step-text">
  Next we&#39;re going to weld it all together. One of the easiest ways to go about this is to attach the brackets to the plate with bolts. You can then clamp the brackets to the beam and tack them together. It&#39;s important that you only weld the brackets to the beam. <br />
  <br />
  Then position where you want your plastic to enter the mould and weld the threaded nozzle to the to other side of the plate.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-5.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-5-2.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-5-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Finishing up</h4>
<div class="step-text">
  After the fitting and welding its time to finish up. The last step in the process is to make sure yout plastic flows smoothly out your machine into the mould, and most importantly, be easily removed! For this we need to remove all &#39;undercuts&#39; by drilling two holes into the plate. The first hole will be the same size as the inside of the nozzle piece. <br />
  <br />
  This should yeild one straight hole all the way through.. but if thats not enough, or you still find there are some undercuts, you can use a larger bit to open the plate more on the beam side.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-8.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-8.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-9.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-9.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-6.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Lasercut version!</h4>
<div class="step-text">
  If you have access to a lasercut service, there is also the possibility of doing this beam mould much faster! <br />
  <br />
  We added a .zip file with drawings you can use or send to a lasercut company to cut your parts. <br />
  These plates are going to replace the use of brackets for making the beam mould. <br />
  <br />
  After you receive the plates you&#39;ll need to place them on the beam by gently tapping them with a rubber hummer, making sure it&#39;s perpendicular and flush with the beam. To check this you can put the nozzle plate in front of it to make sure it&#39;s not in an angle. <br />
  <br />
  After this you can proceed to weld everything in place.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/laser cut copia.png">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/laser cut copia.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/laser cuut2.png">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/laser cuut2.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Explore the possibilities!</h4>
<div class="step-text">
  Now you can extrude! <br />
  There is a lot to explore, you can get some inspiration in other How-to&#39;s!<br />
  <br />
  For example:<br />
  <br />
  Make glass like beams<br />
  👉 tiny.cc/make-glasslike-beams<br />
  <br />
  Extrude different textures<br />
  👉 tiny.cc/extrude-textures<br />
  <br />
  Make T-shaped beams<br />
  👉 tiny.cc/make-t-beam
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-mould-to-extrude-beams/howto-beam-7.jpg">
        <img class="step-image" src="/howtos/make-a-mould-to-extrude-beams/howto-beam-7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>