### Laser cut jigs 

 Once we have double checked the dimensions, we are going to create the patterns we’ll use to mark and cut the different parts of the stool. One for the top and another one for the legs. As we could do this repeated times is recommended to make them out of wood. In this case we will use 9mm plywood, but could be up to 12mm. 

One way of cutting this jigs is by using a laser cutter. This is a precise and affordable technique that will guarantee high precision (<0.2mm tolerance).
