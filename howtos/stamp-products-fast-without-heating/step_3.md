### Use mould for alignment 

 We like it when things are consistent. So we use a little mould to put in our product to make sure we always stamp in the same place. This actually also makes sure the product doesn’t move when applying force. Not a must, but would highly recommend 