### Be patient  

 Don’t be hasty. It might take some time before people get to know about your Collection Point and start bringing plastic. So be passionate and creative on ways to reach out to people. 