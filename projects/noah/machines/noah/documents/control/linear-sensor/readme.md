# Sensors for Noah

## Linear sensors

Noah requires linear position sensor data to create a positional closed loop on each axis. [How encoders work](https://www.youtube.com/watch?v=Q9d3d-gNii8)

[Important video](https://www.youtube.com/watch?v=b5JD2-hlvtw) many aspects of which we can emulate.


[Linear encoders with LinuxCNC](https://www.youtube.com/watch?v=_4Y_pJiaVsU)

It is proposed that this should be achieved using custom Infraread reflective sensors, since they are cheap, non contact, and the encoding strip can easily be extended.

Current work in progress for Noah's sensors is being developed around the [K-032](http://irsensor.wizecode.com/) aka [TCRT5000](https://uk.banggood.com/TCRT5000-Infrared-Reflective-Switch-IR-Barrier-Line-Track-Sensor-Module-p-1038443.html?gmcCountid=EAIaIQobChMI1fDNtNbk5QIViIjVCh3KZAXCEAQYByABEgJd8vD_BwE&cur_warehouse=CN)

![ky 032](./ky-032.JPG)

<a href="https://www.youtube.com/watch?v=3FMKrXuG8rs
" target="_blank"><img src="https://i.ytimg.com/vi/3FMKrXuG8rs/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDb4b2Bc_n18TygwK8vRmeAGp4Cnw" 
alt="TCRT5000" width="240" height="180" border="10" /></a>

These are a cheap Infrared optical sensor often used in [line following robots](https://www.youtube.com/watch?v=_35XK2IpYq4). 

Used in pairs offset by 1/4 phase allows signals to be used for quadrature encoding.

![quadrature](http://www.creative-robotics.com/sites/default/files/tutorials/QuadratureAnimation.gif)


The optical encoder strip will be a self-adhesive black and white striped tape known as [Zebra tape](http://optel-thevon.fr/185-large_default/mrl-20x50.jpg), or "piano" tape. This will probably need to be custom made.

![Zebra tape](http://optel-thevon.fr/185-thickbox_default/mrl-20x50.jpg)




The sensor can be mounted on the end of the axle, reading the encoder tape which is stuck to the side of the rail:

![sensor-idea](./sensor-idea.jpg)


Linear encoders are usuay incremental. [Incremental vs. Absolute encoders](https://www.youtube.com/watch?v=-Qk--Sjgq78)

However it may be possible to achieve absolute positional encoding, using a custom encoder tape with tick spacings based on a [De Bruijn Sequence](https://en.wikipedia.org/wiki/De_Bruijn_sequence). 
This will be a completely new type of encoder, achievable with Arduino. This will likely become a development project in its own right.


## Rotary sensors

Noah's motors will contain rotary sensors making them servos or at least hybrid servo-steppers. These are usually integrated into the motor.

It may be neccessary to add rotary encoders to the pulley mechanism at a later stage.


## Physical Stops

End stops will be neccessary at the end of the rails to prevent accidental overshoot which would be catasrophic.

These will be in the form of limit switches. Additional safety stops could be added as secondary switches activating a relay disconnecting the motor power.

