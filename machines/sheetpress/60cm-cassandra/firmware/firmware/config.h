#ifndef CONFIG_H
#define CONFIG_H

#include "enums.h"
#include "common/macros.h"
#include <Controllino.h>
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Core settings
//

#define LOOP_DELAY 10   // Our frame time, exluding delays in some places
#define BOOT_DELAY 4000 // Wait at least this amount in ms after boot before doing anything

// Please consider to set this to false for production - especially with the full feature set since this is requiring extra
// time for the serial communication and will affect the overall framerate/performance
// #define DEBUG true

#define DEBUG_INTERVAL 1000
#define DEBUG_BAUD_RATE 19200 // Serial port speed

#define RELAY_ON 0    // The relay bank's on value (eg: normally closed)
#define RELAY_OFF 255 // The relay bank's off value (eg: normally closed)

#define HAS_DIRECTION_SWITCH
#define DIR_SWITCH_UP_PIN CONTROLLINO_A0   // The 3 position's up output
#define DIR_SWITCH_DOWN_PIN CONTROLLINO_A1 // The 3 position's down output

// #define IGNORE_FIRST_DIRECTION              // Uncomment to ignore the 3pos switch (forward/reverse) after booting. This prevents surprises but possibly also accidents.
//#define DIR_SWITCH_DELAY        500      // If defined, add this as blocking delay between direction changes. Needed for some types of relays.

// Interval to read analog inputs
#define ANALOG_READ_INTERVAL 200

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Machine settings
//

// #define USE_MEGA                               // On Arduino Uno we have only limited ports which are not enough to enable all features.
#define USE_CONTROLLINO

// the relay type, please toggle if needed
#define POWER_NC true

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Actuator
//

#define PRESS_LIMIT_UP_PIN                          CONTROLLINO_A4
#define PRESS_LIMIT_DOWN_PIN                        CONTROLLINO_A5

#define PRESS_SERVO_DIR_PIN                         CONTROLLINO_D3
#define PRESS_SERVO_STEP_PIN                        CONTROLLINO_D2

// Optional (non falsy): read servo's alarm signal 
#define PRESS_SERVO_ALARM_PIN                       CONTROLLINO_IN0

// Optional (non falsy): turn on/off servo's power supply (to reset, after alarm)
#define PRESS_SERVO_POWER_PIN                       CONTROLLINO_D1
// Optional (non falsy): the max. time to move, if exceeded it's triggering an error in case the limit switch is broke
#define PRESS_MAX_MOVE_TIME                         MIN_MS * 1.5
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Motor related
//

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    General switches

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Feedback

// Status LEDS (up, down, heat)
#define STATUS_MOVE_PIN                               CONTROLLINO_D5
#define STATUS_ONLINE_PIN                             CONTROLLINO_D4
// #define STATUS_HEAT_PIN                             CONTROLLINO_A5

#define FIRMWARE_VERSION 0.5

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plastic Hub Studio - internals : used by external controller setups
// Make sure it's matching
#define FIRMATA_BAUD_RATE 19200

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Plastic Hub related sensors and addons

////////////////////////////////////////////////////////////////
//
// HMI Bridge
//

////////////////////////////////////////////////////////////////
//
// Instrumentation
//
// #define MEARSURE_PERFORMANCE

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    externals
//

// pull in internal constants
#include "constants.h"

// pull in internal configs
#include "config_adv.h"

// The user_config.h is initially added to the github repository but changes will be ignored via .gitignore. Please keep this file safe and possibly
// on a per tenant base stored. You can override parameters in this file by using #undef SOME_PARAMETER and then re-define again if needed, otherwise disable
// default features by using #undef FEATURE_OR_PARAMETER.
// This presents the possibilty to play with the code whilst staying in the loop with latest updates.
#include "user_config.h"

// At last we check all configs and spit compiler errors
#include "config_validator.h"

#endif
