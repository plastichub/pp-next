---
image: "/howtos/make-plaster-moulds-for-large-products/michael-makes-stool-1.jpg"
title: "Make plaster moulds for large products"
tagline: ""
description: "Here, we outline the process of making and using plaster moulds. It’s a great low tech way of making larger, more complex products."
keywords: "product,injection,mould,extrusion"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "injection"
- "mould"
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/michael_makes_">michael_makes_</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Consider pros and cons</h4>
<div class="step-text">
  Before you start, it is important to note that there are some drawbacks to using this process. Plaster moulds are not long lasting - so this may not make sense as a common way to process plastic. <br />
  <br />
  However, it is a great way to inject large, solid products and can be used as a prototyping technique. For example - if you want to test the shape of a mould before it is milled into a block of aluminium. <br />
  <br />
  You’ll need:<br />
  -Extruder machine<br />
  -Shredded plastic<br />
  -Casting plaster<br />
  -Mould release<br />
  -A model or object to replicate<br />
  -Melamine or plywood<br />
  -Heat gun<br />
  -Paint, chopped fibreglass, shellac (optional)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3710.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3710.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3598.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3598.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Make a model to replicate</h4>
<div class="step-text">
  You’ll need a model or object to cast your plaster mould around. This could be anything - a model you made, a 3D print, your favourite toy. Consider how many parts your mould requires. Our product required a two part mould.<br />
  <br />
  In this case, the desired shape was cut out of foam using a home made hot wire and hand sanding.<br />
  <br />
  Pay close attention to the surface finish - if there are any small bumps or dents, these will show in the final product. If you care about this - keep sanding, filling and painting.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3506.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3506.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3587.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3587.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3519.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3519.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make a box to cast your mould</h4>
<div class="step-text">
  Make a box around your model and ensure everything is sealed and secure (you don&#39;t want your model floating up when you pour the plaster). For the box, melamine works really well but you can also use plywood. <br />
  <br />
  You might also want to use a mould release (vaseline works!) to make sure the plaster releases more easily. <br />
  <br />
  Some reference pins are also handy to make sure the moulds line up with each other later on.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3594.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3594.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3537.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3537.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3529.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3529.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Mix and pour the plaster</h4>
<div class="step-text">
  Mix the casting plaster according to the manufacturer specifications. In this case, chopped fibreglass was added to the mix to increase the mould durability. <br />
  <br />
  Pour the mix into the box - as a general rule, pour it to twice the height of the model. <br />
  <br />
  As soon as you pour the mix, spend a few minutes taping the box with a hammer to make sure any air pockets rise to the surface.<br />
  <br />
  Allow the plaster to cure for a couple of days before you demould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3541.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3541.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3599.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3599.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Air dry and seal</h4>
<div class="step-text">
  Now that you have both parts of your mould, it is best to let them air dry for a couple of days. You’ll feel when they’re touch dry (and they’ll be much lighter) - this means you are ready to progress.<br />
  <br />
  As an extra step - you can add a layer of shellac on the plaster surfaces. When it cures, you can then add a mould release (silicone oil or vaseline). This will ensure plastic does not stick to your mould and you can use it again.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3599.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3599.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3548.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3548.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3654.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3654.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Drill injection + relief holes</h4>
<div class="step-text">
  Ok, time to prepare the mould for your machine. Clamp the parts of your mould together so that they align. In this case, a large hole was drilled to connect to the extruder machine. <br />
  <br />
  Some smaller holes were also drilled in various locations to act as indicators that the plastic has reached that point. They also help to prevent a build up of pressure.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3667 copy.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3667 copy.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3677 copy.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3677 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Heat your mould</h4>
<div class="step-text">
  Since this is a slow injection moulding process, you’ll need to make sure the inside of the mould stays hot the whole time. This can be done a number of ways - in this case, large holes were drilled to circulate hot air through the mould from two heat guns.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3666 copy.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3666 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Inject (using the extruder)</h4>
<div class="step-text">
  Ok, you’re almost ready to inject. Start heating your plaster mould. While that’s heating, fire up your extruder and prepare your plastic. When your mould is hot, you can start injecting. This could take anywhere from a few minutes to a few hours depending on the size of your product. In this case, the injection process took about 2.5 hours to fill the mould.<br />
  <br />
  When plastic has reached all of your reference points (those little holes you drilled earlier) that means your product is fully injected. At this point, turn off your heat guns and extruder. You also need to plug all of the holes to maintain pressure inside the mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3668 copy.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3668 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Demould</h4>
<div class="step-text">
  You&#39;ll have to wait a while for everything to cool down at room temperature. The plaster will insulate the heat so this could take up to 12 hours depending on the size of your product.<br />
  <br />
  Demould your product and be careful to preserve your mould so you can use it again!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3706 copy.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3706 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Post processing</h4>
<div class="step-text">
  If you&#39;ve done everything properly, there will only be minimal post processing required. This involves cutting off the injection point and the relief channels. <br />
  <br />
  You can also clean up the part line. We recommend doing this with a knife so you can recycle the shavings again!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/IMG_3697.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/IMG_3697.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/stool-detail.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/stool-detail.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - That's it!</h4>
<div class="step-text">
  A little time consuming but a nice low tech mould making technique. It will never replace machined moulds, but can definitely be useful for prototyping larger, more organic shapes.<br />
  Here’s our final product - a stool made from old polypropylene chairs. But the possibilities are endless.<br />
  <br />
  One thing that you could change is the contrast between the plastics you feed into the extruder. In this case, the colour choices were quite similar so there isn’t much contrast. This is something that can definitely be controlled depending on the look you are going for.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/michael-makes-stool-1.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/michael-makes-stool-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-plaster-moulds-for-large-products/michael-makes-stool-2.jpg">
        <img class="step-image" src="/howtos/make-plaster-moulds-for-large-products/michael-makes-stool-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>