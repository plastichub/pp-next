### Make the ‘Plástico Fantástico’ Recycling Bicycle Shredder 


The adapted bicycle shredder combines the power of shredding with the joy of cycling so you can collect and shred plastic ready for moulding as you travel from place to place. The adaptation has taken influence form Precious Plastic Shredder that was adapted for use on a bicycle. 

For more information about the adaptation please reach out via Instagram to ‘@jam_goreing’ 

Or see the 3 minute project video at (https://www.youtube.com/watch?v=IoSn84Axao8)
