### Rapid-Fire-Method for Injection / 300 Face Shields a Day 


With our Rapid-Fire-Method you can reduce your time-per-part by 50%.

We started 3D-Printing face shields a while ago, but we couldn´t keep up with the demand. So we built a small injection machine to speed up production. Though we had to wait 7-8 minutes bevor we could make another face shield frame,  because the plastic flakes had to heat up. 

To reduce the time-per-part we invented the Rapid-Fire-Method. Using our extruder we produce hot munition for the injection and instantly process it.

We made a video how to do this. Check it out:

https://youtu.be/69aYBJfxHw0