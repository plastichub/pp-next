### Set up a Mix Workspace 


In this How-to we’re going to guide you through all the steps to setup a Mix Workspace. Learn about plastic, how to find a space, get the machines and connect to the Precious Plastic Universe.

Download files:
👉 https://cutt.ly/starterkit-mix 👈

Step 1-3: Intro
Step 4-9: Learn
Step 10-16: Set up
Step 17-22: Run
Step 23-25: Share