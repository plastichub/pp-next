### Get ready 

 Before you start, make sure you have everything that you need:
4x T-shape beams
5x plastic sheets 100cm x 40cm
40x Metal pins 12 mm x 6 mm
80x M5 screws
2x Tension Wires (3,5 m each)

General plastic working tools 

Related links:
How to make T-shape beams 👉 tiny.cc/make-t-beam
How to make plastic sheets 👉 tiny.cc/run-the-sheetpress