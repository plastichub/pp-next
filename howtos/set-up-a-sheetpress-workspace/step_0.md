### Role 

 First of all, make sure you had a look at the showcase page of this Starterkit!
👉 preciousplastic.com/starterkits/showcase/sheetpress

Now about your Role:

Sheetpress Workspaces buy recycled shredded plastic from Shredder Workspaces and transform it into big recycled sheets. These beautiful sheets are then sold to design studios or directly to customers. 

Sheetpress Workspaces should also be in touch with a Community Point to connect, interact and get support from the local Precious Plastic community.