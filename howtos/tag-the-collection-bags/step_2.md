### Paint over your outline 

 If you order the stencil on a laser cut machine then the result will be perfect. Make sure to sand the plywood and remove the black stain from the laser.

Your stencil is ready. Now it is time to use it. I recommend a thick marker. Cut through the fabric and attach the stencil. We used a piece of canvas fabric. Use the marker to draw the triangle.
