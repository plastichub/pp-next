### Prepare the mould 

 Make sure the internal of your mould is free of rust and remains a smooth surface by lightly coating it with oil or grease (Silicone Oil is perfect). For a smooth surface, it also helps a lot to preheat the mould (especially the nozzle part) before extruding plastic into it.
