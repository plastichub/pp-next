### Make the connector plate 

 To make the connection plate, we need a square steel sheet of 150cm x 150cm (minimum 0.5cm thick) and a nozzle which fits your injection machine (here: 1/2 inch - BSP - type). Check the connection types of the injector. https://youtu.be/qtZv96ciFIU?t=255  
Mark the center points for the holes according to the drawing and drill the holes. 
Then place the nozzle on top of the 13mm hole and weld it to the plate. Make sure to place it exactly in the center, this will help screwing the mould to the injector easily, and make the plastic flow through more fluently. 
This part needs to align perfectly to the rest of the mould and the injection machine, so try to work as precisely as possible.
