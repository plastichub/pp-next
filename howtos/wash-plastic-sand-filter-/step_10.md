### How to understand the manometer 

 The manometer is the only instrument we have to understand the filter. Rising and dropping pressure tells a lot about the condition of the system.

Two things can happen:
1 - The pump has problems to suck the water, then the pressure in the filter sinks. This can either mean that the pre-filter needs cleaning or that there is a blockage in the pipes before.
2 - The pressure rises. That means there is a bigger resistance behind the pump. If it only increases a little, it means the filter clogs up and at a certain point it needs to be backwashed. If the pressure suddenly rises a lot, it probably means that there is no exit for the water. If this happens, turn off the pump right away, as it can build up pressure to 3.6 bar which can damage the filter.

To determine the correct pressure, run it with absolute clean sand and mark the value on the manometer. The best time to do this is when the filter is set up for the first time.