### Export, laser cut & inject 

 -You can now save it down as an .svg by clicking File > Save as
-Save it somewhere you will find it later
-You can now import it into the CAM (computer Aided Manufacturing) software for your laser to etch the rulers, inner rectangle & id code of the bottom plate and cut out all the other lines
-Now you will need to tap the threads and you can now injection mould it using the injection machine
-See the video by Kunststoffschmiede on the Dave Hakkens Community Channel for details on how to do this with Plexiglas