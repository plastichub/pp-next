### Prepare the beam and the bar 

 Before assembling everything together with 3mm bolts you will need to drill 3mm holes on both sides of the beam matching with the bar’s holes previously laser cut. You do this by clamping the beam together with the metal plate already bent.

After that take them apart and thread 3mm holes on the bar to fit the bolts. Finish off the beam, sand the edges and the sides so now you can insert the 4mm bolts on the sides to check if they match.