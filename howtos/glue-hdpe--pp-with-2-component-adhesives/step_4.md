### Apply pressure  

 Once the piece is in the right position, we have to apply pressure (preferably with a clamp) and leave it 24h till it’s fully curated (at 23ºC). If the temperature is lower it will take longer to curate, with each -8ºC the curating time doubles.