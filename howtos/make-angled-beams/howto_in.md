### Make angled beams 


Beams make great use of the extrusion machine as they can be used for a big variety of objects and purposes. Adding an angle to the mould offers a strong and time saving solution to produce pieces with corners.
Here we will show you how to make the mould.

To learn the basics about making regular beams first, have a look at the How to “Make a mould for extruding beams” 
(https://alpha.onearmy.world/how-to/make-a-mould-for-extruding-beams)