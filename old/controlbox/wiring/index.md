---
image: /_howto/controlbox/wiring/IMG_2000.JPG
category: "zoe"
title: "Wiring"
tagline: ""
description: ""
usedin:
  - hal
  - zoe
---



<div prefix="_howto\controlbox\wiring" file="header.md" context="md:thumbs" class="fragment"></div>

<div class="thumbs"><div class="thumb">
    <a href="./IMG_1980.JPG" _target="_blank">
        <img id="IMG_1980.JPG" src="./IMG_1980.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1982.JPG" _target="_blank">
        <img id="IMG_1982.JPG" src="./IMG_1982.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />**Power Section**

left to right

1. 3 Phase motor protection [Amazon](${BOM_MBREAKER}) | [Specs](${SPEC_MBREAKER})

2. Distribution block [Amazon](${BOM_DBLOCK}) | [Specs](${SPEC_DBLOCK})

3. Power meter [Amazon](${BOM_POWERMETER}) | [Specs](${SPEC_POWERMETER})

4. Breaker for shredder [Amazon](${BOM_BREAKER_SHREDDER}) | [Specs](${SPEC_BREAKER_SHREDDER})

5. Breaker for extrusion [Amazon](${BOM_BREAKER_EXTRUSION}) | [Specs](${SPEC_BREAKER_EXTRUSION})


<div class="thumb">
    <a href="./IMG_1983.JPG" _target="_blank">
        <img id="IMG_1983.JPG" src="./IMG_1983.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1995.JPG" _target="_blank">
        <img id="IMG_1995.JPG" src="./IMG_1995.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1996.JPG" _target="_blank">
        <img id="IMG_1996.JPG" src="./IMG_1996.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1998.JPG" _target="_blank">
        <img id="IMG_1998.JPG" src="./IMG_1998.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1999.JPG" _target="_blank">
        <img id="IMG_1999.JPG" src="./IMG_1999.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2000.JPG" _target="_blank">
        <img id="IMG_2000.JPG" src="./IMG_2000.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

