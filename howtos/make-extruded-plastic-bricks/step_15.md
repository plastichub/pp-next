### Add the quick release nozzle 

 Take part DA10 and place it onto the surface of AD5 in the marked out space. Use an 8mm drill bit to ensure perfect alignment of the holes and weld only on the shortest of the two sides. Then place part DB5 ensuring it fits with your extrusion adapter. (See Extrusion Adapter How To). Again welding only the top sections. 