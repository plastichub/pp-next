### Work with the injection machine 


The scope of this How-To is the set up and operate the injection moulding machine Precious Plastic v3. The start point is right after you finished building your injection machine and want to start up the first time. This How-to is especially for beginners and tries to cover all important facts to consider for operating the injection machine.