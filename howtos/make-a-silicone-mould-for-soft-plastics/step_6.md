### Releasing the Mould 

 
After 30mins or after the silicone stops being sticky you may release your mould. To do this you must   remove the clay from the edges of your container and silicone mould. Once the clay is removed from the container and the silicone, stretch the sides of the mould to remove the object.

Keep the clay and the plastic container as you may use them again.

Once the Object is removed you are ready to use the mould!