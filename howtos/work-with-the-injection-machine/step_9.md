### Done 

 Congratulations, you just injected your first part! 
If something went wrong, check on the bottom of this page for our troubleshooting reference.

Remove the single parts from the runners by breaking or cutting of the part at the gate location.

Finish your part, by trimming off the leftover gate material with a sharp knife. Watch your fingers!

Make sure, all your products have labels before you hand them out to customers!