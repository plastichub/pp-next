### Make your moulds 

 Let's start with making the moulds for our angles beams! Make sure you're familiar with the process of the How-to "Make angled beams" as this step is based on that process. For this bench, two moulds will be needed to make the four components. 

Take your 80x80x3mm metal tube, cut it to the measurements shown in the image and weld the mounting brackets.
