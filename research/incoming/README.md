# research
osr-research assets


### PID Control

[Optimal PID-Control on First Order Plus Time Delay Systems & Verification of the SIMC Rules](https://www.sciencedirect.com/science/article/pii/S1474667015382689) 

[Tuning Rules for delayed FO systems ](https://ijireeice.com/wp-content/uploads/2013/03/IJIREEICE2E_S_diwakar_Different_PID.pdf)

[LMI MIMO PID Tuning - Matlab](https://ch.mathworks.com/matlabcentral/fileexchange/75571-mimo-pid-tuning-by-lmis)

[PID4JS Matlab](https://ch.mathworks.com/matlabcentral/fileexchange/73931-pid_node-js_framework)

[PID Sim Matlab](https://ch.mathworks.com/matlabcentral/fileexchange/66187-pid-controller-simulator)

[PLC Academy](https://www.plcacademy.com/)

### PID Controller Development

- [PlasticHub - PID Controller - Firmware](https://gitlab.com/plastichub/pid-controller)

### Prediction

[Memory](https://en.wikipedia.org/wiki/Memory-prediction_framework)

## Calculators / Converters

- [Hex converter with various endian styles](https://www.scadacore.com/tools/programming-calculators/online-hex-converter/)

### Hydraulics

- [Pumps, lifting uneven loads](https://fpt-worldwide.com/en/split-flow-hydraulic-pumps/)

### Extruders

- [Cooling Jacket](https://www.linkedin.com/pulse/cast-vs-integral-feed-section-7-factors-consider-when-steve-maxson-2/)
- [Cooling Jacket - AL -Amazon](https://www.amazon.co.uk/Aluminum-Cooling-Jacket-36mm-40mm-Accessory/dp/B07NL6B1VK)

### 3D Print
- [Melt Pump @ Alibaba - with details, 1K+](https://german.alibaba.com/product-detail/price-of-melt-pump-herringbone-gears-plastic-extrusion-machine-1970171401.html)

### Inverters

- [OS Inverter](https://openinverter.org/docs/index.html%3Fen_home,3.html) ([Nissan Ivt logic board YT](https://www.youtube.com/watch?v=7352yeTbsy4&ab_channel=PerformanceEV))

### Brushless Motors

- [https://www.masinaelectrica.com/](https://www.masinaelectrica.com/)

### Reverse vending machines

- [Tomra](https://www.tomra.com/en/collection/reverse-vending/reverse-vending-systems/standalone-line/t70-trisort)

## Robotics (industrial) 

- [Gateways - HW supplier](http://www.adfweb.com/Home/products/ModbusTCP_DeviceNet.asp?frompg=nav10_7)

## Electrics

- [Calculators & Resources](https://engineering.electrical-equipment.org/electrical_calculation_tools)

## Machining

### Milling

[Milling without rotary table](https://www.youtube.com/watch?v=IERZziZI8xU&ab_channel=oxtoolco)

## Directories 

- [plastechtrade, new site, mostly empty](http://www.plastechtrade.com/all-category)
- [ ] [The Association of Plastic Recyclers (APR) - 10k fee, US only](https://plasticsrecycling.org/)

## Organizations 

- [https://www.terracycle.com/](https://www.terracycle.com/)
- [https://www.5gyres.org/](https://www.5gyres.org/)
- [https://www.plasticpollutioncoalition.org/](https://www.plasticpollutioncoalition.org/)
- [https://www.plasticsindustry.org/](https://www.plasticsindustry.org/)
- [https://www.theseacleaners.org/](https://www.theseacleaners.org/)

## Plastic Sorting

- [site:inventeaza.ro](inventeaza.ro/resources/rvm)
- [homebrew - remeter 2nd trial](https://openrecycling.com/en/projects/2021/01/06/plastic-type-sensor.html) | [gh://armin-GH](https://github.com/arminstr)
- [uni work - plasticscanner.com | Jerry ](https://plasticscanner.com/2020/12/20/an-explanation/)
- [DLP Ultra-mobile NIR Spectrometer for Portable Chemical Analysis with Bluetooth Connectivity - TI](https://www.ti.com/tool/TIDA-00554)

### Plastic

- [Reinforcing concrete with 3D printed polymer lattices - Goofy](https://www.3dnatives.com/en/concrete-with-3d-printed-polymer-lattices-301020205/)
- [About PET & recycling PET](https://www.petcore-europe.org/images/news/pdf/factsheet_the_facts_about_pet_dr_frank_welle_2018.pdf)


### Filament

- [RepRapable Recyclebot: Open source 3-D printable extruder for converting plastic to 3-D printing filament - SciDirect](https://www.sciencedirect.com/science/article/pii/S2468067218300208)

## Todos

### Commons

- [ ] Fume extraction - Fans are reasonble priced from 50E, sets at 150E, 1-2000qm/h, 220V/500W - [Ebay Example](https://www.ebay.de/itm/Radialventilator-REGLER-FLANSCH-FLEXROHR-Luftabsaugung-Zentrifugal-Geblase/143439011781)

### v4 (PP/PH)

#### Shredder

- [ ] Fishnets ? (seem to need tight blade cleareance but can be done at with 2kW@220V)

#### Extrusion

- [ ] Explore food market
- [x] PVC (and anything orbiting it)

#### Compression Molding

- [ ] Investigate cycle times (see @Butte)

### WebTech

- [Open Web Docs](https://opencollective.com/open-web-docs/)



