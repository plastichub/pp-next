### Collecting plastic in Arugam Bay, Sri Lanka 


Here we outline how we created an intervention into a booming tourist destination. As tourism related waste production diminished collection from the local population it is important to intervene and set up better collection mechanisms. 