### Opening the mould 

 We like to use thick and strong screwdrivers to prize open the mould. With the brick, we need two slowly leveraging it open at the same time on either side. We also created a rig made of wood to rest the mould on to allow us to hammer through the brick from the top, which forces it away from the two parts.
