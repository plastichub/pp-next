### Brainstorm 

 Get your creative mind working (maybe with some chocolate or coffee ;) )and start to write all the words that are related to this product. These words will make it easier to come up with some sentences that you can use in your graphic content. 
The sentences need to tell the user of the single-use product to drop the (clean) used product in the bucket after using it. Keep it short and clear so people do read it before they throw their spoon. 

Extra awareness: If you can, add a note to make the person think twice before he decides to use the single-use product. 
