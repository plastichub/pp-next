### Laser cut - Finish the mould 

 After receiving your laser cut order you will probably need to sand the middle plate.

Depending on how dirty the cut is, either use the grinder or just do it by hand with sanding paper.

After this, weld the nozzle. It goes exactly to the center of the top plate with a 5 mm hole, so you can drill a bigger hole with the diameter of the nozzle.

Now close the mould, remember to tighten the bolts very well and your are ready to go! 
