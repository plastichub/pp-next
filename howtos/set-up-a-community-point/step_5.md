### Check out the Discord 

 Precious Plastic also uses Discord to connect people around the world. We have channels for different countries, roles and topics. 

Make sure to see what’s already going on in your country and let people know you want to get involved.

👉 discordapp.com/invite/rnx7m4t