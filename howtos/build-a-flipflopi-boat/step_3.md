### Test manufacturers & materials 

 As the approach was to have everything made locally in Kenya, a big challenge was (still is) to find collaborations with recycling manufacturers who deliver quality materials, reliably.  

We started at at point where they were mixing plastic types together or even add sawdust or sand (as a “stiffener”) and it’s already a success to have them working with only one type of plastic, without anything else mixed in.  Part of this was getting material samples and testing joineries which would be used in the boat.  

Starting with samples can save you a lot of time and costs, before ordering a bigger amount of materials. 