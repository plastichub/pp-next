### Put everything together 

 Yeah, assembly time! 
Put the backrest back in place. Then, enlarge the two pre-drilled holes in the seat and attach the frame with screws. While doing so, make sure that the seat’s alignment is straight. 

For the other four screws, clamp the cross-brace to the seat, pre-drill holes and screw it together. It’s recommended to use euro screws (the ones you know from attaching hinges to a cupboard), as they give a solid halt in the plastic.
