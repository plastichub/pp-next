### Transfer the Holes to the HDPE 

 Once we were happy with the shape of the scales, we tapped out the pins and carefully lined up the handles with the plastic blade. This was clamped in position and we then drilled through to get the exact locations onto the plastic. We also added a deep countersink to the outer faces of each of the aluminium scales.