### Pressing: pneumatic system 

 We use pneumatics to operate the jack. It was the cheapest option as we have an air compressor available. Other alternatives would be hydraulic systems. A pneumatic valve, controlled by the system controller let’s air go through when the press needs to be raised. To release the press we installed a servo that can open or close the valve to lower the press (it opens because of its weight) 

When the timer runs out (sheetmaker) a signal sent by the Raspberry pi tells the Arduino to release the press. This can be manually overruled if needed. 

