**This template enables you:**

- always spot on holes for the sliding mechanism - needed!
- compensate bad saw cuts
- compensate too short lengths

**Other remarks**

- Always mark holes this on both sides (up & down) !
- Always use it from the same end of the tube! If the bar was too shoort, you can take this into account when welding the side frame together.
- You can alter this design to use it also as saw gauge to cut it on length.
