### Tools 

 - Injection machine
- angle grinder (cutting and grinding discs)
- vise
- flat file, round file (opional: small grinder rotary tool like Proxxon or Dremel)
- drill press
- clamps (at leaste 3)
- welding machine (only for pedals) & welding clothes
- radius scriber (a marker works well too)
- protection glasses
- respirator with A1 P1 filter (hot plastic fumes and particles filter)
- safety gloves
- ear protection
- sanding paper (grit size around 80) (optional: electric sander)
- measuring tape
- square edge
- M6, M8 thread tap
- drill bit 4.3; 6.4; 6.8; 8.4; 11; 20 ; 22.5 mm (if you don't have the 22.5, use your round file like I did)
- tweezers (optional, for ball bearings assembly)