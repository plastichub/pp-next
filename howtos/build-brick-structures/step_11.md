### End bricks 

 There are 2 types of bricks required to build a wall, and a 3rd optional giving you more control over your design: single, double and triple brick. 

To create a straight edge you will need at least the single brick. this is placed just like all the other bricks. 
