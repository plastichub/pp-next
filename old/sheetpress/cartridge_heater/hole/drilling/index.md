---
image: /_howto/sheetpress/cartridge_heater/hole/drilling/IMG_2007.JPG
category: "sheetpress"
title: "Drilling"
tagline: ""
description: ""
usedin:
  - sheetpress-cell
  - sheetpress
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2004.JPG" _target="_blank">
        <img id="IMG_2004.JPG" src="./IMG_2004.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2005.JPG" _target="_blank">
        <img id="IMG_2005.JPG" src="./IMG_2005.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2006.JPG" _target="_blank">
        <img id="IMG_2006.JPG" src="./IMG_2006.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2007.JPG" _target="_blank">
        <img id="IMG_2007.JPG" src="./IMG_2007.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

