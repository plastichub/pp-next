### Getting ready 

 To follow this How-To you will need an extrusion machine, a closed mould (like this brick mould) and lots of shredded plastic.

Related links:
Make the brick mould 👉 tiny.cc/make-extruded-bricks
More about the Extrusion Pro machine 👉 tiny.cc/build-extrusion-pro