---
image: "/howtos/set-up-a-collection-point/cover-collection-3.jpg"
title: "Set up a Collection Point"
tagline: ""
description: "In this How-to we’re going to guide you through all the steps to set up a Collection Point. Learn about plastic, how to create the space, and how to grow a collection network and connect to the Precious Plastic Universe.<br /><br />Download files: <br /> &lt;a href=&quot;https://cutt.ly/starterkit-collection&quot;&gt;https://cutt.ly/starterkit-collection&lt;/a&gt; <br /><br />Step 1-3: Intro<br />Step 4-11: Learn<br />Step 12-19: Set up<br />Step 20-25: Run<br />Step 26-29: Share"
keywords: "starterkit,collection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "starterkit"
- "collection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Role in the Precious Plastic Universe</h4>
<div class="step-text">
  First of all, make sure you had a look at the showcase page of this Starterkit!<br />
  👉 preciousplastic.com/starterkits/showcase/collection-point <br />
  <br />
  Now about your Role: <br />
  <br />
  Collection Points are the “catchers” of the Precious Plastic Universe. <br />
  <br />
  They save plastic from going to waste while fueling the network with raw material ready to be recycled. They collect plastic from people and businesses and sort it by type. This plastic is then passed on to Shredder Workspaces.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/collection points - role.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/collection points - role.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Outcomes</h4>
<div class="step-text">
  The outcome of a Collection Point is collected plastic, cleaned and sorted by type (and colour), ready to be shred and recycled. <br />
  <br />
  It can be collected from consumers or businesses. It&#39;s crucial that the plastic is label-free and clean for further processes, to keep a good quality of the recycled material. The cleaner and purer the plastic the higher its value.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-outcome.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-outcome.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Is this for you?</h4>
<div class="step-text">
  Collection Points are great for someone who wants to do something about the plastic problem without having to build machines or make products.<br />
  <br />
  It helps if you have good social skills and enjoy talking to people, educating, informing, reminding, correcting and generally helping understand the Precious Plastic Collection system. <br />
  <br />
  You will also have to be very organized as running a Collection Point might quickly become a logistical challenge.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/image2.png">
        <img class="step-image" src="/howtos/set-up-a-collection-point/image2.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Learn about Precious Plastic</h4>
<div class="step-text">
  As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions. Especially as a Collection Point you will have to answer a lot of questions about the project!<br />
  <br />
  If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.<br />
  👉 preciousplastic.com<br />
  👉 community.preciousplastic.com<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.<br />
  👉 community.preciousplastic.com/academy/universe
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/learn-precious-plastic-universe.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/learn-precious-plastic-universe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Check out your area</h4>
<div class="step-text">
  Get an overview of who and what is already existing in your area. Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. <br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/communitypoint-step4-map-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Find your local community</h4>
<div class="step-text">
  It is more practical and fun if you involve people from your local community, so go out there and connect! You can find community members on the Precious Plastic Map or meet them on local events and presentations about the environment and more.<br />
  <br />
  Also see if there is a Community Point in the area and get in touch - they are the glue of the Precious Plastic Universe and might be able to help you in multiple ways with their overview of the people and spaces in the local recycling network.<br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/image21.png">
        <img class="step-image" src="/howtos/set-up-a-collection-point/image21.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Check out the Discord</h4>
<div class="step-text">
  Day to day discussions in the Precious Plastic Universe happen on Discord.<br />
  <br />
  Introduce yourself, say hi in your country channel and start to discover the different channels where people go deep into specific topics (building, collection, design etc..)<br />
  <br />
  Join the Discord:<br />
  👉 discordapp.com/invite/rnx7m4t
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/image18.png">
        <img class="step-image" src="/howtos/set-up-a-collection-point/image18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Learn the basics of plastic</h4>
<div class="step-text">
  Alright, so you are going to start a collection point! First, it&#39;s important to get some knowledge about the plastic topic. Head over to our Academy and dive into the plastic chapters to learn about the different types and properties etc.<br />
  <br />
  As a Collection Point, you want to introduce to people the “refuse, reduce, reuse and then recycle” way of living. By knowing a lot about the plastic topic yourself, you are able to answer their questions and spread the knowledge. <br />
  <br />
  👉 <a href="http://tiny.cc/basics-about-plastic">http://tiny.cc/basics-about-plastic</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/researchplastic.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/researchplastic.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Learn about plastic collection</h4>
<div class="step-text">
  Next, you can get familiar with how plastic collection works around the world and the Precious Plastic plan with our Collection video.<br />
  <br />
  👉 community.preciousplastic.com/academy/collect<br />
  <br />
  These are the main things to take from the video: <br />
  - Educate and enable citizens to be the change.<br />
  - Collect clean label-free plastic.<br />
  - Use the map to help people find Collection Points.<br />
  - All info and how it works are on the website<br />
  👉 collect.preciousplastic.com.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/plastic-collection-point-video.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/plastic-collection-point-video.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Observe plastic uses in your area</h4>
<div class="step-text">
  Check around you. There is probably a lot of plastic being used and discarded.<br />
  <br />
  It’s your task to individuate sources of plastic that could be recycled in the Precious Plastic Universe. Depending on where you live could be your neighbours, colleagues, local restaurants, shops or dumpsites.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-observe.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-observe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Understand your local waste management</h4>
<div class="step-text">
  It’s important to understand the waste management of your local area. Are there local industrial recycling systems? Do they sort their waste and is plastic sorted separately? Or is it the informal sector taking the load.<br />
  <br />
  Find out why local people use plastic and what happens with their plastic after throwing it away so you can explain it to people.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/image13.png">
        <img class="step-image" src="/howtos/set-up-a-collection-point/image13.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Start with the Action Plan</h4>
<div class="step-text">
  Before jumping into finding a space and start collecting, it is smart to sit down to properly plan your project and shape your vision and capture why you want to do this and what your goals are. <br />
  <br />
  To help you plan we’ve made a tool called the Action Plan that helps you to craft your mission, understand your customers, revenue streams, costs and much more. With this tool you should get a step closer to create a successful project. <br />
  <br />
  Find the Action Plan in the Download Kit or learn more in the Academy<br />
  👉 <a href="http://tiny.cc/business-actionplan">http://tiny.cc/business-actionplan</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/mission.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/mission.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Assemble the team</h4>
<div class="step-text">
  Alright, so now that you have a clearer idea of the subject and your vision, you&#39;ll need some more people to put this into reality.<br />
  <br />
  People are what can make a project succeed or fail, so make sure that you have a small team of people who you feel comfortable to work with and who are motivated to do this with you!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-team.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-team.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Find a space</h4>
<div class="step-text">
  As part of your planning, it is important to find a space.<br />
  <br />
  To help you find the perfect place for your Collection Point in the Download Kit you will find a floor plan with all the minimum requirements and a little cardboard tool to place your machines and tools in the workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/how-to-collection-space.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/how-to-collection-space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Set up your space</h4>
<div class="step-text">
  Super, you’ve got a nice space!<br />
  <br />
  Follow our video to get ideas how to fully set up your Collection Point with all the additional tools, bags, furniture and posters needed to make your space functional and nice.<br />
  <br />
  👉 <a href="http://tiny.cc/space-collection">http://tiny.cc/space-collection</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/space-setup.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/space-setup.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Plan your way of transport</h4>
<div class="step-text">
  If you decide to go and pick up the plastic at certain points, you will need to figure out how to transport it. We like using our bike trailor, but have a look and see what makes sense for your area and learn from the local infrastructure and ways of transporting everything around. See if there is any way to make the collection process more efficient or learn from the existent. <br />
  <br />
  Tip: The transportation is also a moment that people see you going around the streets, therefore it is a good opportunity to spread your message and make your collection point more known. A good way can be for example adding your logo to your transportation devices.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-transport.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-transport.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Plan to get clean plastic</h4>
<div class="step-text">
  It’s the Collection Point&#39;s responsibility to collect clean and label-free plastic, this is crucial for further recycling. Cleaning all the plastic yourself will require more time, space and setup, so we highly recommend to set up your collection in a way that you don&#39;t have to take care of the cleaning. <br />
  <br />
  The best way to do this is by educating the people that bring you the plastic (we made some posters for you in the download kit). So they’re involved in the process and everyone is doing their part. The shredder workspace will pay less or might not even accept dirty plastic.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/cleaning.png">
        <img class="step-image" src="/howtos/set-up-a-collection-point/cleaning.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Put your Pin on the Map</h4>
<div class="step-text">
  If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform and put your pin on the map.<br />
  <br />
  This makes it easier for workspaces like the shredder guy to contact you to get plastic or even for a community builder to connect you to the local recycling community. Your Collection Point can also be an inspiration for others in your area to join the Precious Plastic community.<br />
  <br />
  👉 community.preciousplastic.com/sign-up
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/add to the map.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/add to the map.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Receive your plastic</h4>
<div class="step-text">
  Once you’ve learned the basics and got a space you’re ready to open the doors of your Collection Point. Now you have to get people and businesses to give you their plastic. Now is the time for the plastic to come to you (or you&#39;re going to pick it up). <br />
  <br />
  You probably won&#39;t be too pleased with the quality of the plastic you collect at first. Don&#39;t be disappointed, it takes time! It is important to invest time and energy to explain out to those collecting for you how plastic should be (clean and unlabeled) and why you are asking it like this. By involving them in the process they are willing to make these extra efforts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-receiveplastic.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-receiveplastic.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/Julie collection 15.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/Julie collection 15.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Spread the message</h4>
<div class="step-text">
  Your mission is to get people involved by collecting their plastic. We made a bunch of posters and graphic material to help you reach out to more people and communicate what you do. This is just a starting point, feel free to create your own materials and change them according to your language or needs.<br />
  <br />
  You can spread the word by handing out flyers, giving presentations, or maybe even speaking on the radio. Wherever you are or whatever you are doing, make sure that you enjoy this moment and communicate a clear and precise message. :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-spread-poster.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-spread-poster.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-spreadmessage.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-spreadmessage.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Stickers</h4>
<div class="step-text">
  In the Download Kit you can find a sticker for businesses collaborating with you and giving you their plastic waste. You can put it on their window so their customers they can see they’re part of the movement and doing something about the plastic problem.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/Sticker in Window-collection.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/Sticker in Window-collection.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Be patient </h4>
<div class="step-text">
  Don’t be hasty. It might take some time before people get to know about your Collection Point and start bringing plastic. So be passionate and creative on ways to reach out to people.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-patient.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-patient.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Connect to other workspaces</h4>
<div class="step-text">
  Find shredder (or other) workspaces around you who can shred all the plastic you’ve collected. You can find them on the Precious Plastic Map or have a look on other platforms. Develop a nice relationship as you’ll want to work as partners in crime :)<br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-shredder.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-shredder.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-24">Step 24 - upload.preciousplastic.com</h4>
<div class="step-text">
  It&#39;s nice to see your plastic collection growing! By weighing the plastic coming in, and writing it down, you will be able to have a concrete impact measure of how much plastic you are collecting and providing to other workspaces.<br />
  <br />
  Take a moment every now and then to upload your kilograms to our website - like that we can track how much impact our community makes and inspire the world with how much plastic waste we could save and recycle :)<br />
  <br />
  (Also, when you weigh it, you will understand how a few kilograms of plastic take up so much space. Incredible huh? Think globally about how much space you saved from our planet. Thank you for that!)<br />
  <br />
  👉 upload.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-collect-1.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-collect-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/weight the plastic.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/weight the plastic.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-25">Step 25 - collect.preciousplastic.com</h4>
<div class="step-text">
  To help you communicate with citizens and people around you how the Precious Plastic collection system works we made a little website to explain it, how to prepare plastic for recycling and how to find Collection Points near you. <br />
  <br />
  You can use this to get more and more people to bring you plastic.<br />
  <br />
  👉 collect.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/collect.pp.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/collect.pp.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-26">Step 26 - Create How-to's!</h4>
<div class="step-text">
  Share to the world how you run your Collection Point so other people can learn from you and start using your solution to tackle the plastic problem. <br />
  <br />
  Make sure to document and create How-to&#39;s for your best processes and techniques. This can also help you create a name for yourself in the Precious Plastic community. <br />
  <br />
  👉 community.preciousplastic.com/how-to
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-howtos.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-howtos.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-howto.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-howto.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-27">Step 27 - Learn and improve together</h4>
<div class="step-text">
  Precious Plastic is nothing without the people. Working together and helping each other. Participate in the community, share back and make use of it, it can really pay off!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collection-discord.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collection-discord.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-28">Step 28 - Good things take time</h4>
<div class="step-text">
  Starting off will take some time in the beginning. It’s normal. Be patient, work smart and reach out to your Precious Plastic community if you need help. Everything will take off. <br />
  <br />
  Every bit of plastic you collect is already saved from getting burnt! And keep in mind that because of your Collection Point people around you get more aware, and get the possibility to actually start working with recycled plastic.<br />
  <br />
  You’re changing the world by changing your local area :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/howto-collect-end.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/howto-collect-end.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-29">Step 29 - Download and start!</h4>
<div class="step-text">
  Ready and excited to start?<br />
  You&#39;re a hero!<br />
  <br />
  Download your Collection Point package and start your recycling journey!<br />
  <br />
  👉 <a href="https://cutt.ly/starterkit-collection">https://cutt.ly/starterkit-collection</a> 👈
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-collection-point/Collection Starter Kit.jpg">
        <img class="step-image" src="/howtos/set-up-a-collection-point/Collection Starter Kit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>