### CNC - Prepare your material  

 The material we are going to use for this product is Polypropylene, because of its flexibility. This is very important to make these carabiners functional.

Try to use one source of PP as a base to make it as pure a possible (at least 70%) and then add a couple of colorful flakes from other source (also PP) to make it fun.

To fill up this mould you will need around 100 grams, I would recommend you to always fill up the barrel to be able to inject with more pressure!  
