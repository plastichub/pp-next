### Create a collage with your material 

 Using your plastic bags and films, create a collage on top of your base sheet. This can be created however you like; from a methodical design of strips and shapes, to a more random spread of the films. This will create the design for the material that will form your finished raincoat. Reserve a little of the plastic to form a test piece (see step 6). 