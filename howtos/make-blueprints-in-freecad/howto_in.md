### Make Blueprints in FreeCAD 


Whether you're making moulds, building machines or other work making Blueprints is a key tool to communicate your ideas to builders, colleagues and manufacturers. FreeCAD is an amazing bit of free open source software to make 3d models and much more that is available on all platforms. It has a Blueprint feature built in and this How-Tt will show you how to use it to make Blueprints of your own!