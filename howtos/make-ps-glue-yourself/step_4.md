### Store 

 Make sure to store it really airtight. Shelf-life can range from 3-6 months. 

But the glue can remain without much change for months granted if it’s well sealed. It might be useful to add some water if planned to leave for a long time as the glue might harden.