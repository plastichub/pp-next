# Precious Plastic - Uncensored / Unofficial

v3, v4 and v5 - backports, issues, variants & fixes. This repository is currently under development and is the source for the Precious Plastic library, acting as source for research and development. It includes all v3, v4 but also upcoming v5 machines. Other than machines, you can find updated howtos, research data, papers and books.

Stay tuned, we hope to publish it soon as easy to search and navigate website at [library.precious-plastic.org](https://library.precious-plastic.org)

### Installation from GIT

1. Install [GIT LFS](https://git-lfs.github.com/) 'GIT LFS' enables large files over 50MB.
2. clone repo

```
git clone https://gitlab.com/plastichub/osr-resources 
```

3. In case files are not expanded, run

```
git lfs pull
```




## Todos (for developers only)

- [-] merge issues@pp-lib
- [x] merge issues@discord
- [x] fix links & library content
- [-] merge PH content
  - [x] flatten v3/v4 (variants but with ratings regarding completeness)

### users

- [x] Directory, by region, service ...
- [-] Addressbook
- [-] Resolve links
- [x] classification -> PPContact
- [x] check 'rejected'
- [-] OSR tags

### howtos

- [-] apply OSR specs and validation -> reports
- [ ] @gabi : please remove biased content according to [Wiki specs](https://en.wikipedia.org/wiki/Wikipedia:Neutral_point_of_view#:~:text=Neutral%20articles%20are%20written%20with,a%20particular%20point%20of%20view.)
- [ ] add file grid/preview
- [ ] add versions
- [x] Polish SEO and social preview
- [ ] Wikifactory deploy adaptor
- [ ] As forum post

### templates

- [x] Mappin -> OSR-Contact -> PH-LDAP-Contact
- [ ] Howto
  - [x] List
  - [-] Howto - Summary -> see PPLang
  - [-] Step -> see PPLang
  - [x] JSON -> OSR indexer
  - [x] Sitemap -> JekyllFeed
  - [x] SEO display -> JekyllSEO2
  - [ ] Resource Bar : PDF|Source|Download(all of it)
  - [-] Apply OSR specs to files  (@gabi)
  - [-] OSR Rating
  - [-] OSR Spec validation
  - [ ] uncensored
  - [ ] @osr-sync template
- [-] Product Thumbnail
- [x] QC protocol (from ph-osr spec) -> gsheet
- [ ] Testing protocol (from ph-osr spec) -> gsheet
- [ ] Test report
- [x] Assembly (from ph-osr spec) -> jekyll -> jekyll -> PDF
- [x] Vendor Product PDF (from ph-osr template) - pp-lang -> jekyll
- [ ] Vendor Brochure (from ph-osr template) -> pp-lang -> jekyll
- [x] License (sum) -> pp-lang -> jekyll
- [ ] Warrenty/Disclaimer (using component propagation/emitter) -> pp-lang -> jekyll
- [x] Sub cert summary -> pp-lang
- [ ] Disclaimer
  - [ ] link cases
  - [ ] manual rep
- [ ] Applications (tested only)
  - [ ] per type
  - [ ] latest known recommendation/machine with specs
  - [ ] peer cases
- [ ] CAD Sheet / Drawing
- [ ] Naming convention for parts and sub assemblies (CAD|LASER|CAM)
- [ ] Known issues
- [ ] Limitations
- [ ] Damage / callback report
- [ ] Contribs / Contributors
  - [ ] Author
- [ ] Partnership
- [ ] Internship
- [ ] Working group / member list
- [ ] Firmware
 - [ ] Source Viewer/Download
 - [ ] FW Tools (@osr-firmware/toolkit)
- [ ] Guide
- [ ] Forum post (by user)
- [ ] Project to Product
- [ ] Directory
- [ ] Item folder
 - [ ] License
 - [ ] Config
 - [ ] Templates
- [ ] BOM
  - [ ] CAD map
  - [ ] Vendor map
  - [ ] Drawing map
  - [ ] Resources
  - [ ] @osr-sync
  - [ ] @osr-notify
  - [ ] @osr-convert
- [ ] Public CE Version (osr-sync PlastiHub template)
- [ ] Definition of done
  - [ ] contributor
  - [ ] employee
- [ ] Electrical Diagram/Schematic
  - [x] Shredder
  - [x] Power circuit


## Projects

- [ ] Cheap CNC way to make wheels (with bearings) from v3 and v4 machines

## OSR-Convert

- [ ] Howto
  - [x] Expand tags to SEO keywords
  - [ ] Incl. full author sources/links
  - [-] Fetch'n embedd related from PP Search
- [ ] expand config.json::authors

## OSR - Sync
  - [ ] sub-asemblies / parts to 3dxml, step, ... 
  - [x] flat pack config.json::required components
  - [x] add local transformers (strip local names, etc...) 
  - [x] integrate staging/versioning for library (and manuals, ...) 

## OSR Viewer

- [x] PDF viewer with page URL parameters
- [x] Machine, project & product zips

## Map

- [x] Convert censored into pins
- [x] GMaps, its 2020!
- [ ] merge old map
  - [ ] plookup and completion

## V3

- [ ] redo wiring diagrams (source)
- [x] extract Zoe auto-reverse variant for simple Arduinos (with v4 compat as well)
- [x] migrate v3 imperial shredder
- [x] migrate v3.2 shredder
- [x] migrate v3.3 shredder
- [x] migrate Lydia screw mount
- [ ] integrate bertha injector
- [ ] migrate Lydia v3.3
- [x] migrate Idefix
- [x] 220V | 70sqcm variant for sheetpress, Cassandra
- [x] migrate Asterix
- [x] remove this 2kW/400V wood auger joke
- [x] redo v3 bs extruder
- [x] integrate Asterix-PP
- [x] integrate Asterix-Morren
- [ ] integrate pellet face die cutter
## V4

- [ ] Redo missing drawings
- [x] Shredder, migrate Obelix
  - [x]  update BOM for direct drive (no coupling) and missing components (lock nuts & Hal9000)
- [-] Integrate 1.30m sheetpress
- [x] Integrate 1.20m sheetpress
- [-] Integrate 1.50m sheetpress
- [x] Merge 4x - 8x SSR variant (it's fucking ~8 amps per relay only)
- [-] Redo shredder CAD, it's a fucking mess
- [x] Redo v4 Shredder (assembly sucks, performance sucks, housing instable, tab & slots stupid, stationary blades also stupid and cause jamming)
  - [-] Small
  - [x] Medium - 2kW
  - [ ] Large - 4kW
  - [ ] Extra Large - 7kW
- [ ] Integrate Asterix@Obelix
- [x] Integrate Armin's scanner : 
  - [ ] stationary @ SPI|I2C
  - [x] handheld @BT/LED
  - [ ] tomra style conveyor
- [ ] Redo sheetpress
  - [ ] 1.2m (cell) 
  - [ ] 60cm (cassandra lab version)
  - [ ] 50cm cassandra - flat pack
- [ ] integrate pellet face die cutter

# V5

**Candidates (tbc)**

- [ ] Lydia - 45mm 
  - [ ] Filament version
  - [ ] Lab version
  - [ ] Electronics : Monitoring & support for various protocols
  - [ ] Cooling
  - [ ] Nickel/Chrome plating
  - [ ] pellet face die cutter
- [ ] Obelix - 7/11 kW
- [ ] Zoe (v4 like)
- [ ] 5T Injection - automated
- [ ] Asterix 7kW - reverse vending
- [-] Idefix - 11kW
- [ ] Cassandra lab mile
- [ ] Misc: drying, flake transport
- [ ] Common
  - [ ] universal toggle clamp
  - [ ] mold cooling/heating cartridge system

## Common

- [-] redo/update BOMs to OSR specs (see partner project : https://gitlab.com/plastichub/client-oshwapp/blob/master/src/views/Help/HelpBomProtocol.vue)
- [ ] add partner projects/directory
- [ ] augment SEO context (from parents)
- [x] integrate Noah & friends
- [-] integrate customer success stories
- [x] integrate past and ongoing research (slack & forum)
- [x] Forum embed & preview
- [ ] integrate pdf render engine (manuals, warranty & assembly)
- [x] Forum bestofs
- [-] osr-deploy templates for all of the artefacts
- [-] engine :: policies -> specs -> templates -> content -> feedback
  - [ ] CAD, CAM, naming conventions, grouping (sub - assemblies) - policies
- [ ] machine / project snippet deployment for pp academy
- [ ] guide to imperial conversions
- [ ] Machine configurator (components, downloads, drawings, specs)

## Stats/KPI

- [x] Bazar trends, logger (excl. PP Mafia)
- [x] Overall network metrics, instagram, ...
- [ ] Actual growth
- [ ] investigate related orgs/partners

## Academy

- [-] add warnings, fix specs
- [-] remove/tag biased content (all the workspace and business bs)
- [-] Merge with resources/knowledge base
- [ ] Jekyll remake, for the glamorous PP SS HQ
- [x] clean slate content / ready for Jekyll

Can be done from scratch, too much goofy shit in there

### Forum

- [x] define rules for content rating/migration
- [x] extract link directory
- [ ] related module
