### Making the Mould Box   

 As take-away containers usually have uneven bases cut out the bottom of the container using a knife. You should be left with the container edges, the bottom and the lid. The container edges will be used as the walls of the box, the bottom will be used as an additional wall and the lid will become the bottom of your box.

Place the walls of the container over the lid and object. Leave a 2-3cm gap between the object and the walls. If the walls are too far from the object use the additional wall (the cut out bottom) as an insert to get closer to the object. 
The closer you are to your object the less silicone you waste!
