### Sand your parts 

 It can make a world of difference if you take the time to sand the rough cut parts. It will help remove the burn marks and soften the overall look - unless that’s what you’re going for.