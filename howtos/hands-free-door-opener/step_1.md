### Inject and assemble 

 Once you injected your parts with your injection machine, you can assemble the product.

You'll need:
- The two injected parts
- 2x M4 screws and Nuts
- 1x bike tube for in between

You can find more instructions for assembly here:
https://www.youtube.com/watch?time_continue=163&v=95aPYlXShTY&feature=emb_logo