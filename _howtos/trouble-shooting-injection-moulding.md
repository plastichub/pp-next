---
image: "/howtos/trouble-shooting-injection-moulding/Title Image.jpg"
title: "Trouble shooting injection moulding"
tagline: ""
description: "Here I try to to cover all issues that may appear while you are trying to make parts with the process of injection moulding.<br /><br />Some issues might appear but not relevant for you to fix - so see it more as an optional guide where you pick the problems you want to solve. Some &quot;problems&quot; can also widen the artistic freedom to play :)<br /><br />The solutions are ordered in a recommended order!"
keywords: "injection,research,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "research"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/easymoulds">easymoulds</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Incomplete fill of the mould 1</h4>
<div class="step-text">
  If your mould is not filled completely.<br />
  <br />
  Solutions:<br />
  - Check if there is enough material in the barrel at the next try<br />
  - Check if your injection machine volume is at least +20% larger than your mould volume<br />
  - Increase the speed between opening the injection chamber and injecting the plastic<br />
  - Check there is no &quot;cold material&quot; at the nozzle front<br />
  - Increase material temperature<br />
  - Increase mould temperature<br />
  - If your mould volume is just under the machine volume, you may need to pre-compress the material several times with a closed nozzle to densify the material
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/1.2-No Delamination.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/1.2-No Delamination.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Incomplete fill of the mould 2</h4>
<div class="step-text">
  If nothing of the above helps, there are ways you can modify the mould to ease the injection process. Please contact your mould provider prior to those changes!<br />
  <br />
  - Add air vents. Especially on thin walled and large parts air vents will help with the injection process. Everyone can add air vents after the mould is made. Use a triangular file to add small channels from the cavity area which is hit last by the plastic to the outside of the mould. Do not make them deeper than 0,1mm! Otherwise you will end up with flash at those areas.<br />
  <br />
  - Increase the gate size. You can increase the surface of the gate (where the plastic flows into your part geometry) to decrease the necessary injection pressure
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Air Vent.jpg">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Air Vent.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Gate Size.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Gate Size.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Flash</h4>
<div class="step-text">
  If plastic comes out through the split surface of your mould halfs.<br />
  <br />
  Solutions:<br />
  - Check if your mould is fully clamped at the next try. Increase the clamping pressure if possible<br />
  - Check the flatness of your mould and if there is anything preventing your mould from closing fully<br />
  - Decrease the material temperature<br />
  - Decrease the injection pressure
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Flash.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Flash.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Delamination</h4>
<div class="step-text">
  If the material delaminates from the part.<br />
  <br />
  Solutions:<br />
  - Check if your material is clean<br />
  - Check if your material mix is pure from one material type only. Some materials will not create a chemical connection through different characteristics on a molecular level<br />
  - Decrease the material temperatue. You can verify that by cutting the part in half to check if the delamination appears at the core too<br />
  - Decrease the mould temperature. You can verify that by cutting the part in half to check if the delamination appears only at the surface
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Delamination.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Delamination.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Delamination 2.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Delamination 2.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Sink marks</h4>
<div class="step-text">
  Sink marks are sections of your part surface where the material shrinks more than on other sections and creates holes or indents.<br />
  <br />
  Solutions:<br />
  - Increase the holding pressure after the injection operation. After injecting it is recommended to hold the pressure for 2-15sec. depending on your wall thicknesses and part design<br />
  - Decrease the material temperature
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Sink Marks.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Sink Marks.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Weld seams</h4>
<div class="step-text">
  In case your mould shapes geometries where the material has to flow around, chances are that at the point where the material meets again a &quot;line&quot; is visible. This is also the case if your mould has multiple gates into one cavity.<br />
  Those lines are called weld seams. They vary from a line which is only visible at the surface of the part to a line which actually is a surface through the whole geometry. In those cases the structural rigidity of this section will be influenced and a predeminated breaking point is created.<br />
  <br />
  Solutions:<br />
  - Increase the material temperature<br />
  - Increase the mould temperature<br />
  - Check if the material is clean from any contaminiations, especially free of dust or oils
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Visual Welt Seam.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Visual Welt Seam.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Functional Weld Seam.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Functional Weld Seam.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Colors don't mix up</h4>
<div class="step-text">
  In case you look for specific target colors you have to consider some parameters on colormixing plastics.<br />
  <br />
  Solutions (specifc for each case)<br />
  - To add a second color to a white material start with around 2-5% of color to 95-98% white material<br />
  - To add a second color to a black/colored material start with around 30% of color to 70% black/colored material<br />
  - To achieve a solid color you may need to &quot;masterbatch&quot; your material. This means you have to mix the different colors with an extrusion machine prior to the processing with the injection machine.<br />
  - If you use special color to colorize your material masterbatching is crucial. Try to Divide the color pellets to a smaller size.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Color Mixture.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Color Mixture.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Burn Marks</h4>
<div class="step-text">
  Your material has a darker color than the inital raw material.<br />
  <br />
  Solutions:<br />
  - Decrease the material temperature<br />
  - In rare cases: Decrease the mould temperature
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Burn Marks.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Burn Marks.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Burn Marks 2.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Burn Marks 2.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Silver Streaks</h4>
<div class="step-text">
  Silver Streaks describe the appearance of silver lines on your part. This occurs mostly with the use of PS or ABS material.<br />
  <br />
  Solutions:<br />
  - Pre drying of your material will solve the problem. A standard oven with convection heating can be used as a quick fix. For long term use special pre drying applications should be used for higher energy efficiency.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Silver Streaks.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Silver Streaks.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Air bubbles inside the part</h4>
<div class="step-text">
  In case you process clear material, make parts with integrated function (like flexing) or your parts get post processed by machining you want to avoid that air is trapped within your parts. To verify there are no air bubbles within your parts, cut them into sections carefully!<br />
  <br />
  Solution:<br />
  - Decrease the material temperature<br />
  - Pre-compress the material with a closed nozzle and make sure the material is fully molten<br />
  - Increase the pressure
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Air Bubbles.JPG">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Air Bubbles.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Forgotten anything?</h4>
<div class="step-text">
  In case you experience a new problem - please shoot me a mail with your issue + image and I will try to help out.<br />
  <a href="https://community.preciousplastic.com/u/easymoulds<br/>">https://community.preciousplastic.com/u/easymoulds<br /></a>
  <br />
  I would like to add them to this How To, so please declare if your are okay with sharing the image here!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/trouble-shooting-injection-moulding/Whats Left.jpg">
        <img class="step-image" src="/howtos/trouble-shooting-injection-moulding/Whats Left.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>