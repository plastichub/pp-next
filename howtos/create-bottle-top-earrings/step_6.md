### Finish the earrings 

 Now the discs are at the desired shape and size, all that’s left to do is to drill a hole and attach your earring hook or hoop. We used a drill press for this process, but you can use regular drill, or another tool to form a small hole in the plastic.