### Plastic object that sticks sachet 


A plastic object of just under 0.5 inches to pierce sachets like ketchup or mayonnaise, people will no longer need to use their mouth or hand to open them and not get dirty.  much more hygienic and faster.  I have 1000 units here with me that can serve as an example.

I will leave a link to show you guys how it works
https://m.youtube.com/watch?feature=youtu.be&v=yWZGY13XjmQ