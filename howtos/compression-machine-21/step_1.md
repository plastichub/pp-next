### Cut and weld metal 

 Use the schematics to measure and cut your metal. Your oven may be a different size, so make sure you adjust the measurements where necessary. 
Weld all your parts together and paint. 