### Choose your design  

 In our case, we wanted to obtain an ambient light.

After making a few tests we decided to cut the oval beam we found in the scrapyard to obtain a smaller profile and so allow the light to spread more equally. 

Mark the height you want with a metal marking tool, then you can cut the beam with an angle grinder. Make sure to be accurate. 
