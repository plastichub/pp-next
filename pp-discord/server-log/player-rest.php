<?php
require __DIR__ . '/vendor/autoload.php';

require 'debug.php';
require 'utils.php';

use Slim\Http\Request;
use Slim\Http\Response;
use SR\ArrayUtils;
use SR\SessionEvents;
use SR\Sessions;
$app = new \Slim\App;

$app->post('/', function (Request $request, Response $response, array $args) {
    $view = urldecode($request->getQueryParams()['view']);
    switch ($view) {
        case 'config/save':{
                $body = $request->getBody()->getContents();
                $config = get_option('siConfig');
                if (!$config) {
                    add_option('siConfig', $body);
                }
                update_option('siConfig', $body);
            }
    }
    return $response;
});

$app->get('/', function (Request $request, Response $response, array $args) {
    $view = urldecode($request->getQueryParams()['view']);
    switch ($view) {
        case 'sessions/raw':{
                error_log('raw');
                $sessions = new Sessions();
                $out = $sessions->all(ArrayUtils::get($request->getQueryParams(), 'visitor'));
                $response->getBody()->write($out);
                break;
            }
        case 'info':{
                $response->getBody()->write(phpinfo());
                break;
            }
        case 'sessions/remove':{
                $sessions = new Sessions();
                $out = $sessions->remove($request->getQueryParams()['session']);
                $response->getBody()->write(true);
                break;
            }
        case 'views/events':{
                $sessions = new SessionEvents();
                $out = $sessions->events(
                    $request->getQueryParams()['session'],
                    ArrayUtils::get($request->getQueryParams(), 'types', ''),
                    ArrayUtils::get($request->getQueryParams(), 'hide', ''),
                    intval(ArrayUtils::get($request->getQueryParams(), 'start', 0))
                );
                $response->getBody()->write($out);
                break;
            }
        case 'views/list':{
                $sessions = new SessionEvents();
                $out = $sessions->views(
                    intval($request->getQueryParams()['start']),
                    intval($request->getQueryParams()['end']),
                    $request->getQueryParams()['session'],
                    $request->getQueryParams()['hide']
                );
                $response->getBody()->write($out);
                break;
            }
        case 'timeline/events':{
                $sessions = new SessionEvents();
                $out = $sessions->timelineEvents(
                    $request->getQueryParams()['session'],
                    ArrayUtils::get($request->getQueryParams(), 'types', ''),
                    ArrayUtils::get($request->getQueryParams(), 'hide', '')
                );
                $response->getBody()->write($out);
                break;
            }
        case 'views/list':{
                $sessions = new SessionEvents();
                $out = $sessions->list(
                    $request->getQueryParams()['session'],
                    $request->getQueryParams()['types'],
                    $request->getQueryParams()['hide']
                );
                $response->getBody()->write($out);
                break;
            }
        default:{
                $response->getBody()->write('invalid view ' . $view);
            }
    }
    return $response;
});

$app->run();
