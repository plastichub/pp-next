### Stay safe 

 When you use your new stamp make sure to wear a mask as toxic fumes can arouse. In general when handling the iron be careful to not burn yourself and a workshop knife also can be quite dangerous. So stay safe.