<span id="specs" style="padding: 16px">
    <table class="center specs">
        <tbody>
            <tr>
                <td>Type
                </td>
                <td>Sheetpress
                </td>
            </tr>
            <tr>
                <td>Version
                </td>
                <td>0.1
                </td>
            </tr>
            <tr>
                <td>Weight
                </td>
                <td>80 - 120 Kg
                </td>
            </tr>
            <tr>
                <td> Voltage
                </td>
                <td>380V
                </td>
            </tr>
            <tr>
                <td> Status
                </td>
                <td> Development / Testing
                </td>
            </tr>
            <tr>
                <td> Sheet Size
                </td>
                <td>60 x 60 cm
                </td>
            </tr>
            <tr>
                <td> Plate Size
                </td>
                <td>70 x 70 cm
                </td>
            </tr>
             <tr>
                <td> Pressure
                </td>
                <td> 4 tons
                </td>
            </tr>
            <tr>
                <td> Input Flake Size
                </td>
                <td>Medium, Small
                </td>
            </tr>
            <tr>
                <td> Gearbox ratio
                </td>
                <td> 1:15
                </td>
            </tr>
            <tr>
                <td> Maximum Temperature
                </td>
                <td> 300 DegC
                </td>
            </tr>
            <tr>
                <td>Heaters
                </td>
                <td> 300 Watt x 32 (10kW)
                </td>
            </tr>
        </tbody>
    </table>
</span>
