---
image: "/howtos/collecting-plastic-in-arugam-bay-sri-lanka/wlab-02.jpg"
title: "Collecting plastic in Arugam Bay, Sri Lanka"
tagline: ""
description: "Here we outline how we created an intervention into a booming tourist destination. As tourism related waste production diminished collection from the local population it is important to intervene and set up better collection mechanisms."
keywords: "research,collection,sorting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "research"
- "collection"
- "sorting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/waste-less-arugam-bay">waste-less-arugam-bay</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Arugam Bay</h4>
<div class="step-text">
  We’re based in Arugam Bay, a small town of only 150 people on the east coast of Sri Lanka. The little town’s economy is mainly based on tourism with seasonal fishing but lacks proper waste management. During tourist season tourism related businesses demand all waste management capacity of the municipality so even more locals than usual resort to burning their plastic waste. Also businesses were burning large piles of plastic bottles as tourists consume such a vast amount of it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/PET and palm 1.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/PET and palm 1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Beach clean 2.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Beach clean 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Before - no collection means cleaning efforts result in burning.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Before - no collection means cleaning efforts result in burning.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - What is Wasteless Arugam Bay (WLAB)</h4>
<div class="step-text">
  We started WLAB in 2018 after we secured part of the financing through a USAID grant. We offer resource management, plastic collection, educational programs in schools and transform plastic waste into products. You know, the Precious Plastic jam! In total we are 3 full time employees.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/PET delivery to Eco Spindles.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/PET delivery to Eco Spindles.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/kNOw PLASTICS certificate ceremony.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/kNOw PLASTICS certificate ceremony.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/WhatsApp Image 2020-02-27 at 09.07.49.jpeg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/WhatsApp Image 2020-02-27 at 09.07.49.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Our strategy</h4>
<div class="step-text">
  Our strategy is to mainly collect clean PET bottles from the local population and businesses. The collected PET bottles are sold back to the industry while we keep the PE caps for our own recycling production. Sporadically we also run and assist beach cleanups.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/PET at site.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/PET at site.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Beach clean.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Beach clean.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/shredded waste plastic.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/shredded waste plastic.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Our space</h4>
<div class="step-text">
  We work in a big space in the centre of Arugam Bay. The space consists of two 20ft shipping containers and has easy access to the main road where lots of tourists wander around between a mojito and a surf session. This offers great visibility for us to educate everyone about the problem and the alternatives.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Tourists experience recycling.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Tourists experience recycling.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Project site by night.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Project site by night.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Children watching info videos on site.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Children watching info videos on site.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Focus on tourists</h4>
<div class="step-text">
  We decided to focus on collecting plastic from the tourism related industry as they have such a vast amount of PET bottles which happen to be in high demand by the recycling industry in Sri Lanka to be turned into recycled polyester yarn. Also, the handling is relatively easy as the bottles are clean and the bottle caps are great for the Precious Plastic machines. Also it solves the problem of PET bottles clogging up waste management that has to go to landfill.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/recycled Polyester yarn for testing in handloom 2.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/recycled Polyester yarn for testing in handloom 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/shredded bottle caps.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/shredded bottle caps.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Storage shed filling up.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Storage shed filling up.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Provide alternatives</h4>
<div class="step-text">
  Our water refill system is public and anyone can refill for free. In the end we don’t really want to collect all these bottles! A tourist destination without any plastic bottles would be much better than a tourist destination that has a good collection for a vast amount of PET bottles
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG-20200114-WA0009.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG-20200114-WA0009.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG_4944.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG_4944.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Bins</h4>
<div class="step-text">
  We’ve distributed over 60 bins across town to various hotels, restaurants, resorts, mosques and shops. We’ve given them out free of charge but in hindsight we would rather hand them out with a deposit. A nice paint job also helps sending the message. Our bins have a big sticker indicating the plastic we collect and also has a message that encourages people to search for alternatives.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Collection barrel wit sticker.png">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Collection barrel wit sticker.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Benefits for partners</h4>
<div class="step-text">
  As tourists and hotels become more and more aware of the plastic waste problem, it is becoming increasingly crucial for businesses to look like they are doing something. For this reason it is incredibly easy to convince business owners to have our bins. However, we found it crucial to have credible staff member approaching the businesses, trusting that we are trying to do better things.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Collection at remote beach.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Collection at remote beach.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Transport</h4>
<div class="step-text">
  To collect and transport our resources we wanted to have a fun and eye catching means of transport. We’ve tuned up a traditional tuk tuk with some amazing illustrations from a local artist.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Collection vehicle.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Collection vehicle.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/WLAB Three Wheeler 4.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/WLAB Three Wheeler 4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/WLAB Three Wheeler 1.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/WLAB Three Wheeler 1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Daily collection</h4>
<div class="step-text">
  Every week we go out with our tuk tuk to collect the plastic from the different sites. During peak season we drive and collect every day as the amount peaks drastically with tourists arriving.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Unloading PET collection.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Unloading PET collection.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Bottle flying onto tuk.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Bottle flying onto tuk.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Volumes</h4>
<div class="step-text">
  We collect about 1 ton a month. Which is a fraction of what is out there waiting to be recycled. Especially tricky is to collect from the local population. We could probably collect more than 4 tons a month of just PET! The problem is that collection is expensive and PET prices are low :(
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Storage shed filling up.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Storage shed filling up.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/PET at site.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/PET at site.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Sorting</h4>
<div class="step-text">
  Once the collected bottles come into our workspace we spend a considerable amount of time separating the bottle caps from the PET bottles placing them in the appropriate bags and containers. This job is done by one of our staff, sometimes tourists join in and lend us a hand.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Green bottle lids.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Green bottle lids.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Orange bottle lids.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Orange bottle lids.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Blue bottle lids.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Blue bottle lids.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Baling</h4>
<div class="step-text">
  In order to maximise transport we bale all our PET bottles so we can transport them more efficiently and maximise our shipping. One bales consist of 700 - 800 bottles and weighs around 23 Kg.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/baled PET.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/baled PET.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Products</h4>
<div class="step-text">
  With the PE bottle caps collected we make a variety of products including key rings, surf wax combs, buttons and buckles. These products are used within our company, sold to privates, or to fair trade wholesalers.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/recycled plastic buckles.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/recycled plastic buckles.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/recycled plastic souvenier 2.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/recycled plastic souvenier 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Recycled plastic button.JPG">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/Recycled plastic button.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - The money bit</h4>
<div class="step-text">
  We collect the bottles for free from the hotels and resorts (I know we should really charge them for the service, even more since we also supply the collection bins for free, lesson learned:)). While the collected PET is sold back to the industry for 55 Sri Lankan Rupees per kg (about 0.26 €). The products we make from the bottle caps are our high margin items for sure. The more value addition we can create the better the margin, bulk selling just the raw material is a very low margin operation and needs a decent scale.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG-20181115-WA0011.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG-20181115-WA0011.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG-20181115-WA0014.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG-20181115-WA0014.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Connecting with the industry</h4>
<div class="step-text">
  The good thing in Sri Lanka is that we have state of the art recycling companies that can process PET into recycled polyester yarn. The not so good thing is that the company pretty much has the monopoly on PET so it is tricky to get the best price without competition. (The same counts for glass. Only one buyer is available so they can set the price pretty much.)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG-20181115-WA0005.jpg">
        <img class="step-image" src="/howtos/collecting-plastic-in-arugam-bay-sri-lanka/IMG-20181115-WA0005.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - The 3 most important things to start</h4>
<div class="step-text">
  We’ve been running this operation for 2 years. It might feel a little daunting to start at the beginning. However, you don’t need to start a full operation from day one. The most essential things to start are: <br />
  - a dedicated team,<br />
  - a space,<br />
  - and most importantly: a good target area.<br />
  <br />
  For a collection operation the aim has to be to collect as much as possible with as little driving as possible. Tourist areas are ideal we believe as lots of waste is being generated by lots of people in a limited space. <br />
  <br />
  Hope this helps you getting started!<br />
  To end with, enjoy the video showing how we work :)
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>