### Make a mould 

 To have a consistent result it's best to use a mould for the bending. For bigger projects, sharp corners or intricate shapes a counter mould is recommended! This ensures the sheets is pushed in the right shape.

It’s also important to keep in mind where the clamps will be placed so make sure there is space for the clamps to grab on to.