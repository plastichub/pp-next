### Topping the wall 

 Next, you will need to top the wall with timber. To do this drill 11mm holes at the same spacing as previously done to the concrete of bottom frame, sliding the timber over the exposed threaded bar.

Your dimensions for the timber should also be the same as the floor if your structure is even.
