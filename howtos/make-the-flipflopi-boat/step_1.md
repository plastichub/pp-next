### We got a boatbuilder 

 Okay, so it all started with finding a local boat builder, who was confident and visionary enough to believe that we could build a boat from a totally different material than what they were used to.

Ali Skanda, from Lamu, was our man and gathered his boatbuilder team to apply their knowledge to a new material.