### Bend plastic sheets 


Bending plastic is a useful technique that allows turning a flat surface into a three-dimensional shape. It offers a lot of possibilities and works just by applying heat locally and with little force. 