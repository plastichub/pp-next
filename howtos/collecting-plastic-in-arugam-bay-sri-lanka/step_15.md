### Connecting with the industry 

 The good thing in Sri Lanka is that we have state of the art recycling companies that can process PET into recycled polyester yarn. The not so good thing is that the company pretty much has the monopoly on PET so it is tricky to get the best price without competition. (The same counts for glass. Only one buyer is available so they can set the price pretty much.)
