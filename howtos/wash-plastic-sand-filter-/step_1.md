### Get your materials 

 For this size of the filter, we are going to need:
- 30 litre HDPE barrel
- PVC pipes, fittings and one-inch hoses
- silicone or another flexible sealant
- a pump with roughly 650 watts and 3800 litres/hour flow rate (though any similar sized garden pump should work)

- filter sand, grain size 0.4 - 0.8mm
- a pre-filter to avoid big contaminants to clog up the pump
- a waterproof manometer with a pressure range between 0-3 bar

- roughly 7m steel profiles (30x30)
- 6x 8mm bolts
- 4x casters

Tools:
- drill 
- saw
- aligator wrench
- hot air gun or oven (to heat up plastic)
- welding machine