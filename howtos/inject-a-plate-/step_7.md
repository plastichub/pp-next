### Assemble the mould  

 You are now done with the mould’s preps and can assemble all the parts together. First connect the top and bottom part, using the dowel pins to help you with alignment and bolts and nuts to tighten all together. Then you can connect the connector plate to the top and bottom part of the mould. You’re now good to go. 