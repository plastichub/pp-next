### Sheetmaker part 3 

 •	When finished you can choose to make the same or go back to the first step. 
•	All these steps can be controlled manually on the controls page. Each of the 4 heating circuits can be set to the desired temperature and open or close the press. While the sheet maker is making an automated sheet there is no access to the manual controls . 
•	There are 2 heating maps from each plate on the graphs page, this allows users to  monitor each heating element. There are  also 2 graphs: one that displays the average temperature from the last 15 minutes and one that displays the last 2 hours.