### Stay safe 

 When melting plastic, bad fumes can be released. In order to work safer, make sure to use a breathing mask with ABEK filters to prevent inhaling possibly toxic fumes. Special attention on plastics like PS (polystyrene) and PVC. Also when handling with heated elements and plastic we recommend to wear working gloves.

Recommended safety equipment:
- ABEK mask
- gloves
