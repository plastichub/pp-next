### Designed for Disassembly 

 The whole system has been designed to be disassembled for easy packing and transportation. It now fits through a standard door when disassembled. 

The electronics box can also be unplugged and dismounted for transportation.