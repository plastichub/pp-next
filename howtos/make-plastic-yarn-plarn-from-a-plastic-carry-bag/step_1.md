### Cutting off the Seam 

 Image 1:

Let's consider the plastic bag to be made up of the seam, the body and the handles. Knowing these terms will help us during the process of making the yarn.


Image 2:

Start by flattening the plastic bag and removing all the creases.
Cut off the seam.