### View the issues 

 See the open issues to see if there’s anything you can do. We recommend finding something that is quick and simple to fix, particularly if it’s the first time you’re contributing to a project.