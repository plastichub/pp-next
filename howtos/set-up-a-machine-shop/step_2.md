### Is this for you? 

 Setting up a Machine Shop is a very technical role. You need to know how to build, understand your expertise and limits, and surround yourself with trusted partners and suppliers.

On top of providing machines and parts to workspaces and maintain them, Machine Shops also have to establish relationships with the industrial network of their area to procure material and supplies of specific components. 

And last but not least, a certain degree of organisation is required as you will probably have to work as a team in a very connected environment.
