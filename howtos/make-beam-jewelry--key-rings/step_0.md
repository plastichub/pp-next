### Cut the beames 

 The first step is to cut the beams, you can cut slices or little blocks as you want. Slices are better for earrings or necklaces and little blocks are better for key ring. Be sure to cut a slice a little thicker than the desired final thickness because when you polish it will become thinner. 