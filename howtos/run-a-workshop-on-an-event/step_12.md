### Timing 

 Test everything beforehand with friends, so that you’re sure it runs smoothly. Then you have a better feeling and you know on what points you have to work on. If you do not practice, you won’t know how many people you can look after at the same time etc.