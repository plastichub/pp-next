## PP 'Headquarter' major tasks/issues

1. Could you place a very visible download button on the main page ? We had to hand the links out over 50 + times just in the last 6 weeks and you still didn't react ...

How/where :

- https://preciousplastic.com/solutions/machines/overview.html: there should be 3 standard buttons everywhere machine related : 1. **Build**, 2. **Download**, 3. **Buy** (link to bazar category)

<hr/>

2. Can you place a big marketplace button visible on the main page ? Just like in any other website ? I understand you wanna boost your own economy but what about the community ?

How/where : The top menu, ie : Market Place

**status** : rejected

<hr/>

3. Could you please introduce fair market rules like anywhere else ? The top sellers row is obviously faked and abused in your and your buddies favor (I can demonstrate this with the original code of the bazar if you need). The featured vendor row is also non-sense : non of that guys really contributed, was here nor published designs,..., and worst one is making profit with a crucial shredder update everybody was waiting for. Also, running sales through the bazar makes only sense for items below 1000E, after that it's just too much trouble for many reasons as tax, price increase, internal tracking/CRM, invoicing,... You had 300K to maintain a stable team for many years, it's quite a offense you want to turn the community into your revenue stream because - as you often said your self - you can't deal with money. Also, imagine the consequences for the economy when rating agencies suddenly start faking data,... 

**status** : repeatedly rejected

How/where : if there is no correct & complete data, then just don't display a 'featured' or 'top' - sellers module.

<hr/>

4. Could you please apply same policies as anywhere else for wiki content, maintaining neutrality ? Sad to see it's now getting an advertising board.

**status** : repeatedly rejected

How: Please adopt policies from [Wikipedia about content](https://en.wikipedia.org/wiki/Category:Wikipedia_content_policies)

<hr/>

5. Could you please fix the academy pages ? I understand you wanna push your 'workspace' model but it makes only a small fraction of PP and I don't see any reason that you turn the specs into obstacles and confusion for others.

**status** : repeatedly rejected

How: please make the specs matching with past machines & experience (200V) and also check the bazar what is being actually produced. It's quite confusing as said to see a wood auger at 380V and 1.5Kw - which basically would break it even.

<hr/>

6. Could you please create a directory page for PP, there is more than enough and important input, projects, content. It's just too excessive work to communicate this on a daily base.

**status** : repeatedly rejected

How: create a simple gist so community curation is possible for :

1. existing updates
2. existing other machines
3. extract a directory from the map, as list
4. pipe the markdown content into mainpage/community and academy as HTML
5. there should be a section for ongoing projects as well, there is near ZERO visibiltiy for new machines, ideas, ...

<hr/>

7. Could you please put note about filament, also here we spent A LOT of time clarifying this claims for you - on a daily base, despite we're happy having new members in the F***club.

**status** : repeatedly rejected

How: please put in the FAQ a section we can link to. There is enough consensus in the community that 1) the ppv3 extruder just can't create ready to use filament and 2) there is actually no way to produce filament from recycled plastic. Please point also existing projects : https://precious-plastic.org/home/library/articles/filament/. We have to explain all this 1-3 times the week to users as said and it's really anoying.

<hr/>

8. Having that said and considering that PP is quite more than just a cute pet project, could you please seek for professional expertise to run the project ? We'd really be happy that those issues are being taken serious, preferably on a full-time base. The adoption rate as well your response time to crucial issues is way off the normal and is really damaging the project and efforts.

**status** : repeatedly rejected

How: please  find consultancy which could give advise how the PP eco system can grow on all lanes: educational, DIY (eg: connect with other projects, partnerships etc), professional (eg: what steps are needed to get PP builders being able to produce also outside of PP). Other than that, there should also an audit how the website has to look and work. IE: solution provider.

<hr/>


9. Could you please rework the website and make it more simple just? Your workspace model and the new claim ´start a business´ which you also can´t back up with facts isn´t doing any good for the project either. Alone this claim created some work again, and nobody til today could actually point something out. That´s just dangerous telling people such things. We already pushed customers back because they wanted to take big loans. There is now also enough evidence that v3 is being favored by the community/users. Might make sense to focus then on that instead (fixes, extensions, completeness). For instance, the starterkits are nice for some people but I think nobody needs really a floor plan nor getting told to put flowers in the corner (poor flowers btw.)

**status** : rejected

How: instead of pushing a fixed and arguable model, please present PP as serious solution. The workspace model should be only an addon. Showing the machines and their outcomes first might help visitors to understand PP faster. Please add the often asked "througout" rates for each machines for instance. Same here, highlight PP's real strength in all sections : DIY, professional and education.

<hr/>

10. Please put a normal forum back. Having no real place to work together is also just damaging the project, in top you're locking out seniors, non tech savy folks by targeting the young & inexperienced part of the population.  But obviously, we can't see any long term success that way either. It's already harsh to see the kind of life you're willing to live but please don't make it as narrative for the users or advertise the project that way in public. The overall success of PP relies on professional tools, management and users. Btw. there is a drastic chilling effect for innovation and progress by hiding communication behind proprietary tools; the average life time of a Discord message is below a day; nobody find this back nor can build on prio reports and articles anymore. That's a huge set back to the project as well.

**status** : repeatedly rejected

11. Create a board, place or way where stake holders can do next steps, ie ad - campaigns, bounties to address ISOs, CE, etc.. We'd be very happy to throw resources at it. The market is big enough and yelling for niche machines, anything to create real employment faster is very welcome and we feel really far from that. Currently, we're feeling more as volunteers in a locked up situation, unable to decide over our faith - nor being able to share back.

**status** : repeatedly rejected
