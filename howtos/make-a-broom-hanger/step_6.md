### Male mold assembly 

 To assemble the male mold, align the center of the previous parts with the center of part no. 2, press with clamps and drill two 3/16” deep holes. 
On part no. 5, drill flat countersinks for the screws (no. 9) head.
Fix the three parts with the button head screws, washers and nuts (no. 9-11). 

(Drawings pages 8-9)