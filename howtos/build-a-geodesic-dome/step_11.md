### Explore the possibilities! 

 Your Geodesic Dome is done!

Feel free to play around with this structure like removing some beams to create an entrance, or trying out other variations of the construction.

You could also use other materials (like recycled plastic) for your beams or add surfaces in the triangle spaces.

Have fun exploring!