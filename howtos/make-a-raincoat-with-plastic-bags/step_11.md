### Finish the edges of your sleeves 

 Now we will process one edge (the edge that will form the wrist opening) of each sleeve. Fold the material, and using the 2cm that we added for sewing, create a finished edge for the sleeve. If you are unsure, the illustration below should clarify how this will work. 
