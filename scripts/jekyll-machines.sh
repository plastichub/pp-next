#!/bin/bash

# Base directory for this entire project
BASEDIR=$(cd $(dirname $0) && pwd)

sh scripts/mkMaschine.sh machines/injection/elena
sh scripts/mkMaschine.sh machines/injection/elena-xmax
sh scripts/mkMaschine.sh machines/injection/lever
sh scripts/mkMaschine.sh machines/injection/lever-laser-cut
sh scripts/mkMaschine.sh machines/shredder/obelix2021
sh scripts/mkMaschine.sh machines/shredder/asterix
sh scripts/mkMaschine.sh machines/shredder/asterix-pp
sh scripts/mkMaschine.sh machines/combo/zoe
sh scripts/mkMaschine.sh machines/sheetpress/60cm-cassandra
sh scripts/mkMaschine.sh machines/sheetpress/cell
sh scripts/mkMaschine.sh machines/extrusion/30mm-expensive
sh scripts/mkMaschine.sh machines/extrusion/30mm-compact
sh scripts/mkMaschine.sh machines/extrusion/25mm-compact
sh scripts/mkMaschine.sh machines/extrusion/25mm-simple