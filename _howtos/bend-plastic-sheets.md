---
image: "/howtos/bend-plastic-sheets/how-to-bend-plastic-1.png"
title: "Bend plastic sheets"
tagline: ""
description: "Bending plastic is a useful technique that allows turning a flat surface into a three-dimensional shape. It offers a lot of possibilities and works just by applying heat locally and with little force."
keywords: "sheetpress,melting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "sheetpress"
- "melting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Stay safe</h4>
<div class="step-text">
  In order to bend plastic easily you have to apply heat above the melting temperature which can lead to degrading the plastic. If this happens, bad fumes can be released.<br />
  So in order to work safer make sure to use a gas mask with ABEK filters to prevent inhaling possibly toxic fumes. Special attention on plastics like PS and PVC. Also when handling with heated elements it’s recommended to wear working gloves.<br />
  <br />
  Recommended safety equipment:<br />
  - ABEK mask<br />
  - gloves
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-plastic-sheets/how-to-bend-plastic--2.png">
        <img class="step-image" src="/howtos/bend-plastic-sheets/how-to-bend-plastic--2.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Set the right temperature</h4>
<div class="step-text">
  A bending machine is a pretty basic machine that relies on two heating elements to warm up the plate just enough to perform a bending without breaking the piece. <br />
  <br />
  The first step is to set the machine at the right temperature.<br />
  We can recommend:<br />
  - for PS: 90-100ºC above the melting point, to reduce the exposure timing.<br />
  - for HDPE and PP: 50-60ºC above the melting point. These materials take longer to fully heat up and increasing the temperature could burn the surface before melting the inside. Therefore, longer exposure periods tend to give better results.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-plastic-sheets/how-to-bend-plastic--3.png">
        <img class="step-image" src="/howtos/bend-plastic-sheets/how-to-bend-plastic--3.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Heating timings</h4>
<div class="step-text">
  Once the machine reaches the desired temperature it’s time to place the plate between the two heating elements. In our tests we found that PS usually takes less than 6-10 min, while HDPE takes around 30-40 min for the same sheet thickness (10mm). <br />
  <br />
  Since PS is way quicker to bend than HDPE or PP we think that bending machines with a heated sword on top that goes inside the material could work better.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-plastic-sheets/how-to-bend-plastic-4.png">
        <img class="step-image" src="/howtos/bend-plastic-sheets/how-to-bend-plastic-4.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bend-plastic-sheets/how-to-bend-plastic-5.png">
        <img class="step-image" src="/howtos/bend-plastic-sheets/how-to-bend-plastic-5.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Time to bend</h4>
<div class="step-text">
  Once the piece is warm enough it’s time to bend. If the piece is soft enough it shouldn’t take much effort to bend it.<br />
  <br />
  Take in consideration that if the plate is thicker than 10mm some deformation may appear on the sides of plate due compression of the material in the inner part of the radius. This could be solved with a half cut along the bending edge to reduce the amount of material compressed.<br />
  <br />
  In order to achieve a good bending, hold the piece in the desired position until the plate cools down. If the plate has been heated correctly, it will keep its shape.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-plastic-sheets/how-to-bend-plastic-6.png">
        <img class="step-image" src="/howtos/bend-plastic-sheets/how-to-bend-plastic-6.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - What makes a good bend</h4>
<div class="step-text">
  The best condition for a good bend is good material.<br />
  <br />
  The quality of the plastic sheet affects the result. The more homogeneous the material is (one type of plastic from the same source) the better the bend. If the plate has multiple sources of HDPE flakes, delamination can occur and affect the result. <br />
  <br />
  The thickness of the material will also affect how much can we bend the plates. Thicker plates may not bend much without mayor deformations. Also, for thicker plates we may consider to reduce the temperature and increase the exposure timing in order to avoid the surface to degrade.<br />
  <br />
  Have fun bending and exploring new possibilities! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-plastic-sheets/how-to-bend-plastic-7.png">
        <img class="step-image" src="/howtos/bend-plastic-sheets/how-to-bend-plastic-7.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bend-plastic-sheets/how-to-bend-plastic-8.png">
        <img class="step-image" src="/howtos/bend-plastic-sheets/how-to-bend-plastic-8.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>