### Make a bench with beams 


The technique of extruding angled beams provides many applications in furniture design. In this How-To, we will take you through the steps of making a bench from angled PP beams and wood.
