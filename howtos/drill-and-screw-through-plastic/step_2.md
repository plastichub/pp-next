### Pre-drill 

 In order to avoid any deformation and the possibility to break the piece we recommend to pre-drill a hole with a slightly smaller diameter than the screw (e.g. for a 3mm wood screw you’ll pre-drill a 2mm hole). This is especially needed for holes which are closer than 8mm to the edge and when working with PS in general.

Setup:
The best way to drill through plastic is a combination of a metal drill bit and setting the machine at high speed. Special attention while working with brittle plastics like PS since one of the sides generally ends up chipped.