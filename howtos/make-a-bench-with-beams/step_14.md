### Go all in with plastic! 

 You can also make this bench entirely with plastic beams and play around with colours and textures. Note that the plastic is softer than wood and may need extra support.

And don't forget to stamp your plastic parts, so it's clear which plastic type is used here :)
