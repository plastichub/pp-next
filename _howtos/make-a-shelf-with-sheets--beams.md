---
image: "/howtos/make-a-shelf-with-sheets--beams/Shelf Front On-howto.jpg"
title: "Make a shelf with sheets & beams"
tagline: ""
description: "Contrary to popular belief, plastic is durable and tough enough to be used as a supporting material as well!<br /><br />Here we show how you can make a shelf completely made of recycled plastic sheets and beams."
keywords: "extrusion,product,sheetpress,PS,mould,PP"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "product"
- "sheetpress"
- "PS"
- "mould"
- "PP"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  Before you start, make sure you have everything that you need:<br />
  4x T-shape beams<br />
  5x plastic sheets 100cm x 40cm<br />
  40x Metal pins 12 mm x 6 mm<br />
  80x M5 screws<br />
  2x Tension Wires (3,5 m each)<br />
  <br />
  General plastic working tools <br />
  <br />
  Related links:<br />
  How to make T-shape beams 👉 tiny.cc/make-t-beam<br />
  How to make plastic sheets 👉 tiny.cc/run-the-sheetpress
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/howto-shelf-step1.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/howto-shelf-step1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Drilling the beams</h4>
<div class="step-text">
  Now that we have our beam ready, we need to drill 12 mm holes on the leg of the T. The first hole will be 7,6 cm from the floor. The distance between the next holes will be 42,5 cm, as the template. Here you will insert the metal pins.<br />
  <br />
  Between this hole and the corner of the T beam, you drill a 3 mm holes, to pass the tension wires, to hold the four beams together and tigh. In our design we drilled the four corners of the second shelf, and the back corners of the fourth shelf.<br />
  <br />
  Take a look on the edges of the beam, you may need a knife to shape the sharp edges. Remember to stay safe and use a mask and glasses here. <br />
  <br />
  Feel free to change the heights of the shelves.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step2-3.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step2-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step2-1.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step2-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step2-2.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step2-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Making the pins</h4>
<div class="step-text">
  Cut 6 cm from the round metal bar and mill it on one side, so you get a flat surface to drill two 4,2 mm holes, 1 cm away from the edges, each. Then make a M5 thread.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step3-3.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step3-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step3-1-1.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step3-1-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step3-2.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step3-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Make the shelves</h4>
<div class="step-text">
  Let’s continue with the sheets.<br />
  <br />
  Print the shape of the shelves which are provided in the download files. Create a template on a MDF sheet and use it to copy this shape five times on your plastic sheets.<br />
  <br />
  Cut it roughly with the jigsaw and route the edges with the help of the template. Drill and countersink the holes. <br />
  <br />
  Remember to wear a mask and glasses here!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step4-1.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step4-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step4-3.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step4-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step4-2.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step4-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Assembling</h4>
<div class="step-text">
  With the five 12 mm holes made on the four beams, you can insert the metal pins. <br />
  <br />
  Start with the two back beams on the floor, for assembling the first shelf (bottom one) and tighten the screws from the shelf into the metal pins. <br />
  <br />
  Repeat this process with the five shelves.<br />
  <br />
  After connecting the five shelves to the two back beams, you can insert the two front beams, and tighten the screws from the shelves into the metal pins.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step5-1.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step5-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step5-3.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step5-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step5-2.5shelves.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step5-2.5shelves.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Tension Wire</h4>
<div class="step-text">
  Time to apply our two tension wires for some more stability. Have a look at the drawing from the download files as a reference.<br />
  <br />
  Lift the shelf from the floor, and pass one the first tension wire through A, B and C. Then connect A to C.<br />
  <br />
  Take the second tension wire and pass it through 1, 2 and 3. Then connect 1 to 3.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step6-3-01.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step6-3-01.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step6-2-01.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step6-2-01.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/shelf-step6-1-01.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/shelf-step6-1-01.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - You’re done</h4>
<div class="step-text">
  Alright, you&#39;re done!<br />
  Time to enjoy your new precious shelf.<br />
  <br />
  Oh, and in case something breaks, make sure you bring the plastic parts to a Precious Plastic Workspace nearby or recycle in another reponsible way. :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/Shelf Concrete Front On.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/Shelf Concrete Front On.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-shelf-with-sheets--beams/Shelf Sexy Gradient.jpg">
        <img class="step-image" src="/howtos/make-a-shelf-with-sheets--beams/Shelf Sexy Gradient.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>