### See CAD files 

 All sheets and tubes are optimised for lasercutting & bended sheet metal. So you can order it as a kit ready to weld, no need to cut any steel tube by hand. This reduces a lot of the labour involved in making it since the parts easily fit and are numbered. 