---
image: "/howtos/sheetpress-v4--fiction-factory-version/voorkant.jpg"
title: "Sheetpress v4 -Fiction Factory version"
tagline: ""
description: "We have build the sheetpress system with some upgrades which we believe are worth sharing! In short:<br /><br />- Shared our labour hours for realistic price estimation<br />- Making all sheets &amp; parts suitable for lasercutting and sheet metal bending<br />- Updated the bom-list<br />- Made changes to the circuit diagram<br />- Etc,<br /><br />See files below for more information<br /><br />(last update:23-10-2020)"
keywords: "sheetpress,research,melting"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "sheetpress"
- "research"
- "melting"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/fiction-factory">fiction-factory</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - See document: Updates V4-FF</h4>
<div class="step-text">
  In here we described:<br />
  - The updates we did<br />
  - We explain the problems we encountered during building<br />
  - Also we shared the hours we put into the build with an estimation of the cost price if you will make the machine commercial<br />
  - We propose further future improvements which can be done<br />
  <br />
  Displayed picture: The biggest hickup we encountered during building was that we underestimated how important it was to ream the holes of the aluminium blocks to the exact measurements. <br />
  <br />
  After chatting on Discord with &quot;truth-teller&quot;, we reamed the holes to 11.95mm, followed by heating the blocks to 250 degrees (making the hole temporarily bigger) and pressure fit the heating element in to the holes.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/20200529_133007.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/20200529_133007.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/document.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/document.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - See CAD files</h4>
<div class="step-text">
  All sheets and tubes are optimised for lasercutting &amp; bended sheet metal. So you can order it as a IKEA-kit ready to weld, no need to cut any steel tubes by hand anymore. This greatly reduces the labour involved in making it since the parts easily fit and are numbered.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/FF-Sheetpress_v4.1_exploded_view_versie2_2020-Oct-15_07-35-53PM-000_CustomizedView19219998564.png">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/FF-Sheetpress_v4.1_exploded_view_versie2_2020-Oct-15_07-35-53PM-000_CustomizedView19219998564.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/b1e83d90-1c4e-423d-ac08-492d036ad003.PNG">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/b1e83d90-1c4e-423d-ac08-492d036ad003.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/Untitled-1.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/Untitled-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - See electric & BOM diagram</h4>
<div class="step-text">
  We made some changes tot the electric circuit diagram and updated the Bom list with suppliers we have chosen.<br />
  <br />
  Also we added a kWh meter which can set to zero when you start your day melting plates! This way you can track the electricity needed, and thereafter calculate this to a Co2 load for a plate of plastic. <br />
  <br />
  We added signal lights on each group and old ceramic fuses. The signal lights gives visual feedback when a heatign group is active. The ceramic fuses protect the heating goups in case of a short circuit. We discovered ceramic is needed since they can get a bit hot.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/DSC_7566.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/DSC_7566.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/BOM.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/BOM.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Making plastic Precious again!</h4>
<div class="step-text">
  When done the possibilities are endless.<br />
  <br />
  The first commercial client where we used our machine for is The Student Hotel in Delft. Thet wanted a sustainable circular design for their interior. From post-production HDPE plastic we made the black&amp;white cladding of their bar. The client wanted us to incoporate their waste streams which where mainly (HDPE) bottle-caps from soda bottles. The caps are melted in to the plate&#39;s.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/FictionFactory-TheStudentHotel_YW_DSC1443.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/FictionFactory-TheStudentHotel_YW_DSC1443.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/FictionFactory-TheStudentHotel_YW_DSC1415.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/FictionFactory-TheStudentHotel_YW_DSC1415.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/dopjes plaatje.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/dopjes plaatje.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Making plastic Precious again!</h4>
<div class="step-text">
  Another advantage of the sheetpress is the ability to bend solid surfaces. We used a lot of PET plastic plate&#39;s from Smile plastics in England. With the help of the Sheetpress we could easily bend them for the rounded bars.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/FictionFactory-TheStudentHotel_YW_DSC1389.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/FictionFactory-TheStudentHotel_YW_DSC1389.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/FictionFactory-TheStudentHotel_YW_DSC1417.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/FictionFactory-TheStudentHotel_YW_DSC1417.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/sheetpress-v4--fiction-factory-version/20200720_113045.jpg">
        <img class="step-image" src="/howtos/sheetpress-v4--fiction-factory-version/20200720_113045.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>