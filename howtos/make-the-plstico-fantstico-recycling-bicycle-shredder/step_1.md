### The Plastic Collection Tray 

 The housing will need to be elongated downwards to allow for a collection tray for the shredded plastic that can sit underneath the shredder blades. The housing needs to be elongated down around 50mm allowing a space in the middle for the collection tray as illustrated in the images. The collection tray itself is made of clear acrylic that then slots together. 