### Remove sprue and trim flash 

 With your new part made, remove the sprue and any flash. This can be done using a variety of methods depending on the design. The Daily Spork’s sprue can be easily snapped off by hand. Flash (excess material) can be removed using a deburring tool or a small knife. If using a knife, be sure to cut away from yourself :-)