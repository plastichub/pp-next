### Bend your wire 

 Now bend the copper in your desired shapes. Use your pliers to support the bending and squeezing. Make sure to include 2 cm of wire orthogonally from your later or symbol to fix later in your iron.
 If you feel adventurous you can also carefully solder copper pieces together, but we didn’t experiment with that, as we heat up the copper later, which can weaken the bond. 