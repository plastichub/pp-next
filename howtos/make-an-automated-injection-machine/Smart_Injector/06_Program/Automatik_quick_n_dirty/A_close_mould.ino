
/*
Automatic injecting extruder (close mould)
*/

void close_mould()
{
	Kvalendstop1 = digitalRead(Kendstop1);
	while (Kvalendstop1 == LOW)
	{
		if (Kdt > Kdtend)
		{
			Serial.println("accelerate");
			Kdt = Kdt - 10;
		}
		for (int i = 0; i < Kstepsinfor; i++)
		{
			digitalWrite(DIR, LOW);
			digitalWrite(DIR2, LOW);
			digitalWrite(EN, HIGH);
			digitalWrite(EN2, HIGH);
			digitalWrite(PUL, LOW);
			digitalWrite(PUL2, LOW);
			delayMicroseconds(Kdt);
			digitalWrite(PUL, HIGH);
			digitalWrite(PUL2, HIGH);
			delayMicroseconds(Kdt);
		}
		Kvalendstop1 = digitalRead(Kendstop1);
		Serial.println(Kvalendstop1);
	}
	if (Kvalendstop1 == 1)
	{
		for (int i = 0; i < Klimitedsteps1; i++)
		{
			digitalWrite(DIR, LOW);
			digitalWrite(DIR2, LOW);
			digitalWrite(EN, HIGH);
			digitalWrite(EN2, HIGH);
			digitalWrite(PUL, LOW);
			digitalWrite(PUL2, LOW);
			delayMicroseconds(Kdtref);
			digitalWrite(PUL, HIGH);
			digitalWrite(PUL2, HIGH);
			delayMicroseconds(Kdtref);
		}
	}
	Kdt = Kdtref;
}

