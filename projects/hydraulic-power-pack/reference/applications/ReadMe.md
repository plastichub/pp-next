## Hydraulic Power Pack

# Applications


This section discusses the intended application of the power pack in the context of recycled plastic.

It is suggested that the hydraulic system is intended primarily to provide power to an injection machine. The requirements of this machine will dictate the minimunm spec of the power pack.



# Hydraulic Injection Machine

Suggested Spec:

Max Projected Part Area: 200x200mm = 400cm2

Material: HDPE only - mixed source recycled shred

Fully automated with mould cooling

Shot size 1kg

Clamping mechanism: 3 platten, 4 ti-bar, 5 point double toggle

Clamping Pressure 240 tons

Mould Opening Stroke: 450mm

Target Cycle Speed: 45pcs per hour

Cavity Pressure: 800 - 1500 psi?

Hydraulic actuators: 4 (Clamping,  injection, unit, ejection) 



[Calculations](http://www.gudmould.com/news/89.html)
