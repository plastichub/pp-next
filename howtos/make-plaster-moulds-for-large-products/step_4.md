### Air dry and seal 

 Now that you have both parts of your mould, it is best to let them air dry for a couple of days. You’ll feel when they’re touch dry (and they’ll be much lighter) - this means you are ready to progress.

As an extra step - you can add a layer of shellac on the plaster surfaces. When it cures, you can then add a mould release (silicone oil or vaseline). This will ensure plastic does not stick to your mould and you can use it again.