### Make extruded plastic bricks 


Recycled plastic has the potential to replace more conventional and wasteful building materials such as hollow blocks. In this How-to, you will learn how to assemble the mould for the extruded plastic brick, ready for production.

This brick was developed with the Extrusion Pro machine.
Learn here how to build it: 