### Alignment 

 Now it’s time to cut the legs to the final length. Use scrap wood or wedges to make the frame stand sturdy on a plane surface without shaking. The height of the seat should be 440mm in the front and 430mm in the back. If this is not the case (most probably), set a height gauge to the difference between the as-is and to-be and mark all four legs. Then cut them off nicely along the carved lines.
