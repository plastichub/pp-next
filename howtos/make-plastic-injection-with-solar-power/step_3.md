### Screw cartridge & mold 

 ⚠️ The cartridge will be really hot, USE GLOVES !

Remove the two end cap of the hot cartridge.

Plunge one extremity of the tube in the cup of water.

By putting the end of the tube in the water, you will make a hard plastic piece that will push the melted plastic inside of the mold.

NB : Preheating the mold helps a lot to obtain a nice finished part, above 60°C.