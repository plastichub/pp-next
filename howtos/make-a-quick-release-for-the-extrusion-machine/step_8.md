### Welding the nozzle 

 Now on the main plate is welded to the angle bar, add your nozzle adapter (part D) to attach the quick release to our extrusion. We used a ½ inch galvanised union, but find the one which fits your extrusion machine. (Note that brass will not work here since you cannot weld brass to steel.)

The union is cut in half and then placed on the centre of the hole. The nozzle is then welded ensuring that no welds touch/damage the threads. 
