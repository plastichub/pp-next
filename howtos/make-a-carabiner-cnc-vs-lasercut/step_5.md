### Laser cut - Prepare the file 

 Download the file and send it to laser cut in steel, it’s important to ask for the middle plate (n°2) in 6 mm thick steel. This will determine the thickness of the final product.

The top and bottom plate (1 and 3) should be cut with 8-10 mm thick steel (the thicker the better, but also heavier!) to make the mould stronger and avoid bending while injecting.

(If you have other ways to reinforce moulds keeping it lighter, go for it! And don't forget to share (: ).

If you are good with the grinder you can also cut these two by yourself or just send everything to lasercut! 