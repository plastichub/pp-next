### Storage of shredded plastic 


There are various options for how to store shredded plastic. If you have large volumes of material, then most likely you will be suitable for industrial containers. But if you have a small workshop, then in my opinion it’s very convenient to store plastic in 19 liter water bottles.


Zip is here:  http://tiny.cc/679fiz (Universe site allow only small zips up to 5Mb)