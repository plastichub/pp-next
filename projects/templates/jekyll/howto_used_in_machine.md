<div class="ty-vendor-plans">

{% for doc in site.howto %}  
  {% if doc.usedin %}
    {% for used in doc.usedin %}
      {% if used == page.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endif %}
{% endfor %}
</div>
