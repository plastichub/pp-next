### Screw cartridge & mold 

 The cartridge will be really hot, USE GLOVES !

Remove the two cap ends of the hot cartridge.

Plunge one extremity of the tube in the cup of water.

By putting the end of the tube in the water, you will make a hard plastic piece that will push the melted plastic inside of the mold.

NB : Preheating the mold above 60°C helps a lot to obtain a nice finished part.
The preheating is easily achieved by putting the mold a few minutes in the solar concentrator.