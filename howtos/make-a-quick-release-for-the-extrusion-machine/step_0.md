### Prepare your pieces 

 First off, in this how-to precision is key. We recommend you to take extra care and time making this nozzle adapter. Once ready, this adapter should fit with all of your future products. Prepare the materials you need. Bear in mind that these items are a rough guide and you can use similar items that you may have available to the same effect. 

In this How-to we will use:
A: 2x 5mm plates (65mm x 70mm)
B: 2x 20mmx 3mm angle bar (70mm)
C: 2x 5mm strips (70mm x 25mm)
D: 20mm Galvanised Union
E: Scrap pieces of metal (approx 20mm x 20mm)
