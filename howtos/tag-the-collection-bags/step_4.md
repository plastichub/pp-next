### Open a hole and place the string 

 Time to open the hole, you can do it easily with a hammer and a thick nail. Now put the string and you are done. Choose a strong string, we used leather.

You can give your label some love. Sew the ends, iron them and keep them on your hanging board to reuse them again.
