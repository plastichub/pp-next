### Make connecting brackets 

 Now we will need brackets to connect the two tube pieces with each other as well as the mould to the mounting plate.
(This is where you’ll be thanking yourself if you already prepared extra ones in earlier mould productions.)

All you need to do is cut your angle iron into sections of 30-40 mm and drill a hole in one side to fit your bolts. For larger beams use bigger, thicker angle iron. 
