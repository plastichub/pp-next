### Laminate the drawings 

 Cut the drawings along the dashed lines. Use a ruler and a sharp knife and work precisely. Then attach the drawings to your prepared wooden parts. If there are several drawings for one part, start from A and go on in alphabetical order.

Apply spray glue on the back of one sheet, align it according to the instructions on the drawings and apply it evenly by pushing with your hands from the middle outwards.
