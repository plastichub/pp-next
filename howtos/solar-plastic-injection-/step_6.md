### Recap 

 This video shows the whole process and the time expected for every step. (It works, but we can improve it).

The solar concentrator is a solution that doesn't need any electricity. 

You can setup this everywhere (beach, mountain, city) in any season, you just need a little bit of sun.

If you are interested in the solar concentrator, feel free to visit volks.eco and contact my friend Marco.

He has been working on this nice solar concentrator for a long time now, and it can be used in so much other ways.

I hope you find your way with it and that it will open doors on the potential of solar energy for plastic recycling.

Thanks Marco for your input and Precious Plastic for all !