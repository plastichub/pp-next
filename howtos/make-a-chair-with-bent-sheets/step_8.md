### Smoothen the edges 

 To refine the cut, screw the jig (through the pre-drilled holes) into the intended bottom side of your plastic sheet. Equip your router with a flush trim bit and set the height so that the bearing runs along the jig (shown in the image). Go against the rotation with a medium-low speed. And finally, use the router again to fillet the inner edges, for a better sitting comfort.

Before removing the jig from the plastic, don’t forget to transfer the vertical centerline from the jig to the plastic parts as indicated in the drawings.
