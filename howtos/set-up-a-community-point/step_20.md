### Learn with other communities 

 It can also help if you connect with other Community Points to see how they are working and make sure to document and share what worked well in your community. 

This way you can create and participate in a powerful local and international network!