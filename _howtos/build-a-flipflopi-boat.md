---
image: "/howtos/build-a-flipflopi-boat/flipflopi-sailing-front-2.jpg"
title: "Build a 'Flipflopi' boat"
tagline: ""
description: "The Flipflopi is a sailing boat made from 100% recycled plastic and flip-flops collected from the streets and beaches in Kenya. <br /><br />Here we want to share how it was made and what we learned on the way."
keywords: "research,extrusion,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "research"
- "extrusion"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/the-flipflopi">the-flipflopi</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Intro</h4>
<div class="step-text">
  First off, this boat is a first of its kind, a prototype. The processes we used were done for the first time and are not perfect (far from that!). So don’t take this as a guide to copy identically but more as something to learn from and and to get inspired :) <br />
  <br />
  If you want to dive more into details, see more photos, and test results, have a look at the document here: <br />
  👉 tinyurl.com/flipflopi-build-documentation
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step1.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Get a boatbuilder</h4>
<div class="step-text">
  Okay, so it all started with finding a local boat builder, who was confident and visionary enough to believe that we could build a boat from a totally different material than what they were used to.<br />
  <br />
  Ali Skanda, from Lamu, was our man and gathered his boatbuilder team to apply their knowledge to a new material. <br />
  <br />
  We definitely recommend finding someone who knows how to build boats, so you can focus on learning how to use plastic for already existing processes, instead of trying to learn another complex skill on top of that!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step2-1.JPG">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step2-1.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step2-2.JPG">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step2-2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step2-3.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step2-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make a plan</h4>
<div class="step-text">
  With Ali Skanda we made a plan and designed the boat, to get an idea of the required components and joineries.<br />
  <br />
  The goal originally was (and still is), to build a boat which would be big enough to travel the message to fight single-use plastic and plastic pollution around the world. <br />
  <br />
  Here some of the sketches to get an overview of the parts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/BoatBuild-1.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/BoatBuild-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step3-1.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step3-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step3-2.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step3-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Test manufacturers & materials</h4>
<div class="step-text">
  As the approach was to have everything made locally in Kenya, a big challenge was (still is) to find collaborations with recycling manufacturers who deliver quality materials, reliably. <br />
  <br />
  We started at at point where they were mixing plastic types together or even add sawdust or sand (as a “stiffener”) and it’s already a success to have them working with only one type of plastic, without anything else mixed in. Part of this was getting material samples and testing joineries which would be used in the boat. <br />
  <br />
  Starting with samples can save you a lot of time and costs, before ordering a bigger amount of materials.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step4-2.JPG">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step4-2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step4-3.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step4-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step4-4.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step4-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Start building</h4>
<div class="step-text">
  With the processes more or less figured out, we started producing the recycled parts for a 24m boat - They were HUGE.<br />
  <br />
  The quality was very rough, but it was good to see that it was possible. And as this hasn’t been done before and was going to be a big investment, we decided to make a smaller 10m prototype first. <br />
  <br />
  So in the next step you&#39;ll see how we actually made the Flipflopi Dogo (“dogo” = “small” in Kiswahili).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step5-2.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step5-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/IMG_6381.JPG">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/IMG_6381.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - The big parts</h4>
<div class="step-text">
  The production of the BIG PARTS like keel and ribs was one of the biggest challenges. We managed to work with what we could get, with a lot of space for improvements! <br />
  <br />
  We collaborated with the closest plastic recycling manufacturer Regeneration Africa in Malindi, where they usually produce fencing posts and tiles. <br />
  <br />
  Here is an overview of their process. (second image). We made over 30 metal moulds for different boat parts which were filled this way, using HDPE which is the most common and easiest type to collect separately (after PET). <br />
  <br />
  Sam and his team were a huge help to explore and figure out different processes and materials!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/FF-boatbuilding-process-1.png">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/FF-boatbuilding-process-1.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step5.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/IMG_0902.JPG">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/IMG_0902.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - The planks</h4>
<div class="step-text">
  For the planks we collaborated with other manufacturers in Nairobi, which also produce fencing posts as well as other construction material. <br />
  <br />
  It&#39;s a quite common practice there to mix in sawdust to make the material stiffer and cheaper to produce (but that material mix also breaks much easier than pure plastic). It took a couple of attempts, but in the end we managed to get the 100% recycled plastic planks. <br />
  <br />
  These planks were produced with professional, industrial machines, but could definitely be made with the Precious Plastic Extrusion Pro as well! <br />
  (Extrusion Pro: <a href="https://community.preciousplastic.com/academy/build/extrusionpro)">https://community.preciousplastic.com/academy/build/extrusionpro)</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-dogo-42-.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-dogo-42-.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-howto-step6-1-.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-howto-step6-1-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - The flip-flop sheets</h4>
<div class="step-text">
  40% of all the waste collected on the beaches were flip-flops. This is where the project got its name from and why flip-flops were an obligatory element of this boat. <br />
  <br />
  So we covered the whole boat with sheets of recycled flip-flops, giving it a very colourful look and adding an extra protective layer (the whole boat feels like a big yoga mat :))<br />
  <br />
  The sheets were made by local flipflop artist James who cuts the flip-flops into pieces, glues them together and sands them to an even sheet.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi howto-flipflops-1.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi howto-flipflops-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi howto-flipflops-2.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi howto-flipflops-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/IMG_6261.JPG">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/IMG_6261.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - The boat build</h4>
<div class="step-text">
  Time to build the boat! Starting with the keel, the ribs and connection parts, then making the hull with the extruded planks, and finishing with the colourful Flipflop sheets. <br />
  <br />
  Other than using this new material, the boatbuilders made the boat in their traditional way, meaning that they used very basic tools (every screw was inserted with a hand drill and a screwdriver!). <br />
  <br />
  This is obviously something we won&#39;t be able to teach you here - that&#39;s what you need a boatbuilder for! :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-dogo-36.JPG">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-dogo-36.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-dogo-27 copy.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-dogo-27 copy.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-flipflopis.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-flipflopis.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Sail and inspire!</h4>
<div class="step-text">
  Alright, now we only had to add (second hand) parts from other sailing boats like the mast, boom, sail and ropes. And were finally ready to do what the boat was meant to be for: Sailing around Kenya to create awareness around the problem and inspire local communities to be part of a positive change!<br />
  <br />
  On our first expedition the boat sailed smoothly for more than 500km from the north of Kenya to Zanzibar, carried its passengers safely while creating excitement and fascination everywhere we went!<br />
  <br />
  ❗️IMPORTANT: Definitely test and check your boat if it&#39;s seaworthy! You&#39;ll be responsible for your passengers and yourself, and don&#39;t want to risk your lives 🙏
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/FF-boatbuilding-process-7.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/FF-boatbuilding-process-7.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/flipflopi-sailing-3 copy.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/flipflopi-sailing-3 copy.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/9-phase 1-Diani-.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/9-phase 1-Diani-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Things we learned</h4>
<div class="step-text">
  It was a big process with a lot of learning, and yes, the result is a functional boat - but the process was quite rough with a lot of space for improvement. <br />
  <br />
  On the way we also made more tests and analysed the properties of the materials. <br />
  You can find a report here: <br />
  tinyurl.com/flipflopi-material-analysis <br />
  <br />
  And the document we shared in Step 1 has everything with more details as well :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/Screenshot 2020-10-07 at 19.08.43.png">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/Screenshot 2020-10-07 at 19.08.43.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Join us! ⛵️ </h4>
<div class="step-text">
  We already reached hundreds of thousands locally and globally with our expedition and the story around the boat. It&#39;s a great tool to reach people in a positive way to push good changes.<br />
  <br />
  So, we&#39;ll be building a much bigger boat which can sail longer distances and reach millions! There is still a lot to improve and figure out for a boat of that size! But as Ali Skanda says: &quot;Kila kitu inaweze kana.&quot; - Everything is possible :) <br />
  <br />
  Hope this was insightful or at least a bit inspiring 🙃 <br />
  <br />
  Want to get involved? ✉️ theflipflopi@gmail.com <br />
  Or become a supporter (yay!) 👐 <br />
  <a href="https://www.patreon.com/theflipflopi<br/>">https://www.patreon.com/theflipflopi<br /></a>
  <br />
  And if you end up building a similar boat, make sure to share it! (@theflipflopi)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-flipflopi-boat/kubwa-next.jpg">
        <img class="step-image" src="/howtos/build-a-flipflopi-boat/kubwa-next.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>