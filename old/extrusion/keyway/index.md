---
image: /_howto/extrusion/keyway/4.JPG
category: "extrusion"
title: "Keyway"
tagline: ""
description: ""
usedin:
  - lydia-v4
  - lydia
  - zoe
  - extrusion-pro
---





<div class="thumbs"><div class="thumb">
    <a href="./1.JPG" _target="_blank">
        <img id="1.JPG" src="./1.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./2.JPG" _target="_blank">
        <img id="2.JPG" src="./2.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./3.JPG" _target="_blank">
        <img id="3.JPG" src="./3.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./4.JPG" _target="_blank">
        <img id="4.JPG" src="./4.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

