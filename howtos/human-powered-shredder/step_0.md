### Disclaimer 

 Before we start, let us mention that this guide is supposed to be informative, only for you to be inspired about the possibilities of machine design.

We're not responsible for the design or (mal)functioning of your machine, please seek professional help when needed.

Always follow safety recommendations and practices when building machinery and using tools.

Design, build and hack this machine at your own risk!

The pictures and 3D models in this guide are from particular one-off designs, not to be taken literally as they may not make sense for your requirements.