---
image: /_howto/sheetpress/cartridge_heater/press_plate_hole/IMG_2391.JPG
category: "sheetpress"
title: "Press Plate_hole"
tagline: ""
description: ""
title: "Punch marking tool - Cartridge Heater - Block"
usedin:
  - sheetpress-cell
  - sheetpress
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2379.JPG" _target="_blank">
        <img id="IMG_2379.JPG" src="./IMG_2379.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2380.JPG" _target="_blank">
        <img id="IMG_2380.JPG" src="./IMG_2380.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2381.JPG" _target="_blank">
        <img id="IMG_2381.JPG" src="./IMG_2381.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2382.JPG" _target="_blank">
        <img id="IMG_2382.JPG" src="./IMG_2382.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2383.JPG" _target="_blank">
        <img id="IMG_2383.JPG" src="./IMG_2383.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2384.JPG" _target="_blank">
        <img id="IMG_2384.JPG" src="./IMG_2384.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2385.JPG" _target="_blank">
        <img id="IMG_2385.JPG" src="./IMG_2385.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2386.JPG" _target="_blank">
        <img id="IMG_2386.JPG" src="./IMG_2386.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2387.JPG" _target="_blank">
        <img id="IMG_2387.JPG" src="./IMG_2387.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2388.JPG" _target="_blank">
        <img id="IMG_2388.JPG" src="./IMG_2388.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2390.JPG" _target="_blank">
        <img id="IMG_2390.JPG" src="./IMG_2390.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2391.JPG" _target="_blank">
        <img id="IMG_2391.JPG" src="./IMG_2391.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

