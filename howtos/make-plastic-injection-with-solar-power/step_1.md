### Load the cartridge 

 Load the shredded plastic inside the iron cartridge.

Compress as much as you can the needed amount of  plastic depending on the volume of your part, inside the cartridge.

Lock with ½ cap end your cartridge.

Load the cartridge inside the concentrator.
