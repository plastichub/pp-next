---
image: "/howtos/make-beam-jewelry--key-rings/beam jewelry.jpg"
title: "Make beam jewelry / key rings"
tagline: ""
description: "This how to explain the simple process of polishing a piece of PS BEAMS to a shining and beautiful jewelry or key ring! You will need for this a machine to cut slice of the beames and then different polishing grains. It is better to have an electric polishing machine.. you will get better result with the transparency and the shine :-)"
keywords: "PS,product,extrusion"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "PS"
- "product"
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/plasticfactorybe">plasticfactorybe</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Cut the beames</h4>
<div class="step-text">
  The first step is to cut the beams, you can cut slices or little blocks as you want. Slices are better for earrings or necklaces and little blocks are better for key ring. Be sure to cut a slice a little thicker than the desired final thickness because when you polish it will become thinner.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/A1.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/A1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/A3.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/A3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/A2.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/A2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Sculpt the slice</h4>
<div class="step-text">
  Step 2 will be to sculpt the slice to obtain the desired shape. You can sculpt it with an electric polishing machine or with different idea, it is up to you :-) I will sculpt it in a round shape to do earrings.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3141.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3141.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3154.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3154.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3161.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3161.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Polishing step</h4>
<div class="step-text">
  Now that you have your shape, you can start polish it. Make sure you start with the thickest grain and end with the finer grain. The thinner it is, the smoother and more transparent the surface will become. Dont forget to do polish the edges as well so that they are rounded and soft to the touch
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3166.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3166.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3175.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3175.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3178.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3178.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Finishes step</h4>
<div class="step-text">
  Ok now you have your little pebble smooth to the touch and almost transparent and shining. The last step is to polish it with cotton, so for that you will have to use patinas on your cotton. it will give you a great shining and a perfect transparency. <br />
  You have now your earrings (or necklace or key ring) ends!<br />
  All you have to do next is make small holes in it to pass your accessories, I use a screwdriver with a small metal bit.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3184.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3184.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3190.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3190.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/IMG_3199.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/IMG_3199.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Final result</h4>
<div class="step-text">
  This if a few exemple of what you can do and how it will look when its completely done :-)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/5EC1CA7E-A485-4763-A21C-611041AB8663-BF14D709-0D76-43F2-9E03-13E19C0B39FF.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/5EC1CA7E-A485-4763-A21C-611041AB8663-BF14D709-0D76-43F2-9E03-13E19C0B39FF.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/A5FB9A54-8442-426B-8503-B8E9B9191D5F-398B906F-1221-40E3-9BFE-65AA1FF395D9.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/A5FB9A54-8442-426B-8503-B8E9B9191D5F-398B906F-1221-40E3-9BFE-65AA1FF395D9.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-beam-jewelry--key-rings/F9E68BFB-2233-4249-9700-B011DD33B45C-68F8DB73-D9AA-4DDD-AA97-F30A43F5A190.jpg">
        <img class="step-image" src="/howtos/make-beam-jewelry--key-rings/F9E68BFB-2233-4249-9700-B011DD33B45C-68F8DB73-D9AA-4DDD-AA97-F30A43F5A190.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>