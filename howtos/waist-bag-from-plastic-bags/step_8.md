### Sew the Bag Together 

 The hardest step so far. As all the necessary parts are all sewn together, we need to close the bag. This is done while the bag is turned inside out. Mark the middles of the Front part and the Back part. Align the parts together. The zipper has to be open to allow to turn the bag outside in. Make sure the straps end up on the right side of the bag (inside the bag during this step and outside after it is sewn).

After sewing, turn the bag inside out / outside in. Make sure that all the corners are fully twisted out because it sets the overall shape of the bag. If everything is correct, the straps are outside and the bag looks like something on the photo.