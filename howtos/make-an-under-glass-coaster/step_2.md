### Hollow the circle (2) 

 Now that you have your center hole you can start to do the rest of the circle. 

Make sure to stop digging when you have 5 mm of edge left on each side. 