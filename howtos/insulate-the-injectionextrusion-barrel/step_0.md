### Get ready 

 This is a pretty simple upgrade. There are many ways to insulate barrels, but in this example we have gone with using some steel mesh and hand-bent brackets.

Before you begin, you should have: 
- Hammer
- Vice
- Drill
- Steel mesh
- Insulation wool
- Scrap metal (flat bar)
- Nuts & Bolts
