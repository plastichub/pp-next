### Resize the rulers 

 Because it may not import correctly into the software we will need to resize it to be accurate and adjust it for laser cutting. We will be doing this using guides in Inkscape

-To create a guide click on the ruler on the left hand side and drag to the right
-You should now see a blue guide, double click on it and set it to 150 mm and click enter
-Then resize the ruler so that the left hand side touches the line of the document and the right hand touches the guide
-Then repeat for the inch ruler with the guide set to 6 inches