### Assemble (Part 2) 

 For the second level, connect 5x 5-way-joinery with 2x short beams and connect 5x 6-way-joints with 2x long beams to the first level. The 5-way-joints and 6-way-joints should be alternating. Then, connect all the joints on the second level with shorter beams.
