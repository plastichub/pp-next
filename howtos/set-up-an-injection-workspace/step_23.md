### Sell 

 You can now make great products in series. Now is crucial to find people and organisations that want to buy your recycled products.
 
First, you should put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. Then, you have to get creative on how to sell your products locally. Shops, design studios, online stores and more. 

Possibilities are endless and, nowadays, everyone wants a piece of recycled plastic!
