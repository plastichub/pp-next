### Drilling the beams 

 Now that we have our beam ready, we need to drill 12 mm holes on the leg of the T. The first hole will be 7,6 cm from the floor. The distance between the next holes will be 42,5 cm, as the template. Here you will insert the metal pins.

Between this hole and the corner of the T beam, you drill a 3 mm holes, to pass the tension wires, to hold the four beams together and tigh. In our design we drilled the four corners of the second shelf, and the back corners of the fourth shelf.

Take a look on the edges of the beam, you may need a knife to shape the sharp edges. Remember to stay safe and use a mask and glasses here. 

Feel free to change the heights of the shelves.
