---
image: "/howtos/set-up-a-sheetpress-workspace/cover-sheetpress-3.jpg"
title: "Set up a Sheetpress Workspace"
tagline: ""
description: "In this How-to we’re going to guide you through all the steps to set up a Sheetpress Workspace. Learn about plastic, how to find a space, get the Sheetpress, find customers and connect to the Precious Plastic Universe. <br /><br />Download files:<br /> &lt;a href=&quot;https://cutt.ly/starterkit-sheetpress&quot;&gt;https://cutt.ly/starterkit-sheetpress&lt;/a&gt; <br /><br />Step 1-3: Intro<br />Step 4-9: Learn<br />Step 10-18: Set up<br />Step 19-23: Run<br />Step 24-25: Share"
keywords: "sheetpress,starterkit"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "sheetpress"
- "starterkit"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Role</h4>
<div class="step-text">
  First of all, make sure you had a look at the showcase page of this Starterkit!<br />
  👉 preciousplastic.com/starterkits/showcase/sheetpress<br />
  <br />
  Now about your Role:<br />
  <br />
  Sheetpress Workspaces buy recycled shredded plastic from Shredder Workspaces and transform it into big recycled sheets. These beautiful sheets are then sold to design studios or directly to customers. <br />
  <br />
  Sheetpress Workspaces should also be in touch with a Community Point to connect, interact and get support from the local Precious Plastic community.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/universe-sheetpress.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/universe-sheetpress.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Outcomes</h4>
<div class="step-text">
  The outcome of a Sheetpress Workspace is recycled plastic sheets. <br />
  <br />
  These sheets can be of different sizes up to 1x1 m and with varying thicknesses from 5mm up to 30mm (more to be explored!).<br />
  <br />
  Make sure to play around with various patterns and colours to create stunning recycled sheets.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image9.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image9.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Is this for you?</h4>
<div class="step-text">
  For the Sheetpress Workspace you will have to be quite technical as you have to understand how the Sheetpress machine works, ideally, know how to maintain it and, as a plus, know how to fix it when it needs a bit of extra love. <br />
  <br />
  Attention to details and some creativity is also a nice plus to come up with unique sheets of stunning beauty.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/Sheets Greg Close Up Badge.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/Sheets Greg Close Up Badge.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Learn about Precious Plastic</h4>
<div class="step-text">
  As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions.<br />
  <br />
  If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.<br />
  👉 preciousplastic.com<br />
  👉 community.preciousplastic.com<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.<br />
  👉 community.preciousplastic.com/academy/universe
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/learn-precious-plastic-universe.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/learn-precious-plastic-universe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Check out your area</h4>
<div class="step-text">
  Get an overview of who and what is already existing in your area.<br />
  <br />
  Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. <br />
  <br />
  Make sure not to jam the local network, if there are already five Sheetpress Workspaces around, have a chat about collaboration with them first, or maybe consider starting another type of space.<br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/communitypoint-step4-map-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Find a Community Point near you</h4>
<div class="step-text">
  Community Points are the glue of the Precious Plastic Universe. They know the ins and outs of your local network. They can help you in multiple ways but they generally have a very in-depth overview of local Precious Plastic spaces, people, useful shops, resources and can help you with planning out your project. You can find them on the map here. <br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image13.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image13.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Find your plastic supply</h4>
<div class="step-text">
  You can use the Precious Plastic Map and the Bazar to find Shredder workspaces around you that can provide you with the raw material: shredded plastic waste. <br />
  <br />
  If you have a local Community Point, they might be able to give you some advices for this as well.<br />
  <br />
  👉 community.preciousplastic.com/map<br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/SP Green Pour Close Up.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/SP Green Pour Close Up.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Get on Discord</h4>
<div class="step-text">
  Day to day discussions in the Precious Plastic Universe happen on Discord.<br />
  <br />
  Introduce yourself, say hi in your country channel and start to discover the different channels where people go deep into specific topics (building, collection, design etc..)<br />
  <br />
  Join the Discord:<br />
  👉 discordapp.com/invite/rnx7m4t
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image18.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image18.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - The basics of plastic</h4>
<div class="step-text">
  Before you start it is crucial to get a solid understanding of plastic. How it works, the different types, melting temperatures and so on. <br />
  <br />
  Head over to our Academy and dive into the plastic chapters to learn about the different types and properties and more.<br />
  <br />
  👉 <a href="http://tiny.cc/basics-about-plastic">http://tiny.cc/basics-about-plastic</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image20.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image20.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Fill in the Action Plan</h4>
<div class="step-text">
  Before jumping into making machines or finding a space it is smart to sit down and properly plan your project and shape your vision. <br />
  <br />
  To help you plan we’ve made a graphic tool called the Action Plan that helps you to craft your mission, understand your customers, revenue streams, costs and much more. With this tool, you should get a step closer to create a successful project. <br />
  <br />
  You can find the Action Plan in the Download Kit or learn more in the Academy.<br />
  👉 <a href="http://tiny.cc/business-actionplan">http://tiny.cc/business-actionplan</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image4.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image4.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Workspace Calculator</h4>
<div class="step-text">
  Now you have your project nicely planned out and it’s starting to take shape in your mind. <br />
  <br />
  It is important at this stage to make a serious estimation of how much it will cost you to set up and run your workspace. Otherwise, you might run out of money halfway. The Workspace Calculator is a spreadsheet that helps you do just that. <br />
  <br />
  You can find the Workspace Calculator in the Download Kit or learn more in the Academy.<br />
  👉 <a href="http://tiny.cc/workspace-calculator">http://tiny.cc/workspace-calculator</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image15.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image15.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Business Plan Template</h4>
<div class="step-text">
  To help you pitch your idea to potential partners, financial institutions or investors we made a Business Plan Template (and a specific example for the Sheetpress Workspace) for you to use.<br />
  <br />
  This template helps you to talk the business language and should help you access the necessary money to begin. <br />
  <br />
  For more explanation check out the video in the Academy.<br />
  👉 <a href="http://tiny.cc/business-plan-calculator">http://tiny.cc/business-plan-calculator</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image21.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image21.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Tool list</h4>
<div class="step-text">
  Alongside your Sheetpress system, you will need a number of other tools and machines to help you with the operations of the Sheetpress Workspace.<br />
  <br />
  In the Download Kit, you will find a tool list with all the necessary tools to run your workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/ToolKit.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/ToolKit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Find the space</h4>
<div class="step-text">
  To help you find and set up the space for your workspace, you can use the floorplan in the Download Kit. It includes all the minimum requirements and a little template to place your machines and tools in the workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/space.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Get your Sheetpress</h4>
<div class="step-text">
  Cool, now you have a space it’s time to get hold of your Sheetpress system. There are three ways to do that:<br />
  <br />
  1 Build it yourself following our tutorials<br />
  👉 tiny.cc/build-sheetpress<br />
  <br />
  2 Buy it on the Bazar.<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  3 Find a Machine Shop near you on the map that can build it for you.<br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image8.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image8.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Build your workspace</h4>
<div class="step-text">
  Super, you’ve got your Sheet press system!<br />
  <br />
  You can watch our video on how to fully set up your Sheetpress Workspace with all the additional tools, furniture and posters needed to make your space ready. <br />
  <br />
  👉 <a href="http://tiny.cc/space-sheetpress">http://tiny.cc/space-sheetpress</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/build-space.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/build-space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Safety</h4>
<div class="step-text">
  Always stay safe. <br />
  <br />
  Of course, Sheetpress machines get hot. And can cause a hazard in different ways.<br />
  <br />
  So before starting to melt, please check out our safety video to learn about dangers and how to stay safe when working with plastic.<br />
  👉 <a href="http://tiny.cc/plastic-safety">http://tiny.cc/plastic-safety</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/Safety 1.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/Safety 1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Buy shredded plastic</h4>
<div class="step-text">
  Now the only thing missing is your raw material. <br />
  <br />
  Try to get your shredded plastic as local as possible. In best case you can buy it from a Shredder Workspace in your local network.<br />
  <br />
  Make sure to specify your preferred shreds size (small, medium or large) and to have a variety of colours and types, so you can be more creative.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image22.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image22.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Time to make sheets!</h4>
<div class="step-text">
  Ok, having all the things in place it’s time to start baking sheets. Yeeah!<br />
  <br />
  Watch this video to learn how to make a sheet and adopt the best practices.<br />
  👉 <a href="http://tiny.cc/run-the-sheetpress">http://tiny.cc/run-the-sheetpress</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image14.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image14.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Make a variety</h4>
<div class="step-text">
  Once you get a grasp on the process make sure to make a nice variety of colours, sizes and thicknesses to attract different customers.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image11.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image11.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Maintenance</h4>
<div class="step-text">
  As you run your Sheetpress Workspace it is crucial that you maintain the Sheetpress machine in order to keep the quality of your products high and prevent failures.<br />
  <br />
  In case you haven&#39;t seen it yet, here are some guidelines on how to maintain your system:<br />
  👉 <a href="http://tiny.cc/run-the-sheetpress">http://tiny.cc/run-the-sheetpress</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image13.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image13.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Sell</h4>
<div class="step-text">
  You can now make superb sheets. Many every day. Now is crucial to find people and organisations that want to buy your recycled sheets. <br />
  <br />
  Put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. <br />
  And then you can get creative on how to sell your sheets locally. Get in touch with shops, design studios, online stores and more.<br />
  <br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image17.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image17.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Create your profile</h4>
<div class="step-text">
  If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform to connect with people.<br />
  <br />
  Follow the link below and sign up with your email, pick your role, put your pin on the map and upload a nice image to show the world what you’re doing.<br />
  👉 community.preciousplastic.com/sign-up
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image12.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image12.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-24">Step 24 - Create How-to's!</h4>
<div class="step-text">
  Share with the worldwide community how you run your Sheetpress Workspace so other people can learn from you and start using your solution to tackle the plastic problem. <br />
  <br />
  Make sure you only create How-to&#39;s for your best processes and techniques, no tryouts or oneoffs. This can also help you create a good name for yourself in the Precious Plastic community. <br />
  <br />
  👉. <a href="http://tiny.cc/create-howto">http://tiny.cc/create-howto</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/image18.png">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/image18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-25">Step 25 - Good things take time</h4>
<div class="step-text">
  Starting off will take some time in the beginning. It’s normal. Be patient, work smart and reach out to your Precious Plastic community if you need help. The efforts will pay off eventually :)<br />
  <br />
  You’re changing the world. Thank you!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/good-things.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/good-things.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-26">Step 26 - Download and start!</h4>
<div class="step-text">
  Ready and excited to start?<br />
  You&#39;re a hero!<br />
  <br />
  In this download package you will find all the files you need to set up your Sheetpress Workspace.<br />
  Download and start your recycling journey!<br />
  <br />
  👉 <a href="https://cutt.ly/starterkit-sheetpress">https://cutt.ly/starterkit-sheetpress</a> 👈
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-sheetpress-workspace/Workspace Starter Kit.jpg">
        <img class="step-image" src="/howtos/set-up-a-sheetpress-workspace/Workspace Starter Kit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>