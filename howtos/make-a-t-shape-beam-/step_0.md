### Tools and equipment required 

 To make the mould you will require:

2x L angle iron 60 x 60mm, 190cm long
1x 100mm M.S flat 190cm long 
3x 12 x 12mm M.S rods 190cm long 
Sheet metal ( minimum 3mm or more preferable )
5mm x30mm nut and bolt 
6mm x 30mm nut and bolt
Threaded pipe or fitting (BSPT size of your nozzle)

Angle Grinder / Metal saw
Welding machine 
Drill Press / Drilling machine
