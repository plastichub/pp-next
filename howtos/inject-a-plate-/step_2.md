### CNC your top and bottom parts 

 Let’s start with the CNC part. 
Download the attached STEP file and CNC-mill them into the 2 separate aluminium blocks both equal sized of minimum size 26cm x 26cm x 4cm. (Note: The standard Precious Plastic Injection machine limits the max width for moulds to 28cm).

The step file includes 6 reference points to be milled. These points make it easy to manually drill on the exact location of the aluminium blocks. If you are not lucky enough to have access to a CNC milling machine, you can send the files to a CNC cutting company (consider that this will be more expensive and will probably take longer).

Once the mould is cut, polish the mould to achieve a high quality surface for your product. You can do that yourself or ask the CNC cutting company to polish the mould for you.

*special thanks to Friedrich
