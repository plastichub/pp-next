### Store the sheet 

 Lift the sheet and lay it on top of the flat wooden strips you prepared earlier. 
It is important to be very gentle with the sheet as you move it, any bends or twists at this stage increase the chances of it to warp later. 

Place the next row of wooden strips on top of the sheet perpendicular to the last. 

If you have one available, place a flat heavy sheet on top of the strips to keep them weighed down - in our case we used a spare heating plate.

Check your timetable for when the next sheet has to be transferred to the cooling press. Once it has been transferred (Same as step 9), move the prep table back in front of the sheetpress ready to prepare the next sheet.