### Make a flat nozzle for the Extrusion Machine 


The flat nozzle is a great addition to the extrusion machine. It enables one to cover greater surface with less plastic, increasing production time and new avenues for larger, watertight products. 