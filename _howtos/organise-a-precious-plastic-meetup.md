---
image: "/howtos/organise-a-precious-plastic-meetup/SK Community-cover.jpg"
title: "Organise a Precious Plastic Meetup"
tagline: ""
description: "A meetup is a great opportunity to get to know your local recycling community, introduce Precious Plastic to people who don’t know it yet and find ways to help each other and work together to grow and succeed together.<br />Here we’ll give you some tips to organise and run this meetup.<br /><br />Meetups are a very useful tool for Community Points - if you haven’t seen it yet, you might want to check out our Community Point Starter Kit! <br /><br />Step 1-6: Get ready<br />Step 8-10: Meet up!"
keywords: "research"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "research"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get your contacts ready</h4>
<div class="step-text">
  Alright, let’s start thinking about the people who should join the meetup. Especially for the first meetup you probably want to bring together people who already know or work with Precious Plastic as well as potentially interested ones.<br />
  <br />
  Best is if you already made a search in your area and set up a communication channel for the people who are involved in the local (Precious Plastic) recycling or interested in it.<br />
  <br />
  If not done yet, follow the steps 4-9 from the Community Point Starter Kit to get some tips to start the conversation in your local community.<br />
  <br />
  👉 tiny.cc/starterkit-community
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/communitypoint-step4-map-.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/cb-contactlist.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/cb-contactlist.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Find a good place</h4>
<div class="step-text">
  Now think of a place where you can meet. Consider that it should be somewhere rather central and accessible and have enough space for the number of people who will probably join the meeting. Ideally find a place where you can show a presentation and videos (on a laptop or even better with a projector) and can comfortably sit and exchange ideas and thoughts.<br />
  <br />
  Ask in your group, maybe they have ideas or even a good space for this meeting in their own collection point, workspace, design office, fablab etc. - this could also give the group the opportunity to see and understand how other members of the community operate.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/cb-meetup-place-2.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/cb-meetup-place-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Find a good time</h4>
<div class="step-text">
  Next step is to find a date and time which work for as many people as possible.<br />
  <br />
  As you are the one organising, the first step is to see when it would work for yourself. Often it helps to suggest a few options to choose from (you can also do this with help of websites like doodle.com). Pick some dates which are a few weeks away, so you have time to promote it and people have time to plan it into their schedules.<br />
  <br />
  Sidenote: Don’t worry if you can’t find a common date for everyone - it’s just the first meetup of (hopefully) many more, so there will be more opportunities to join.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/meetup-calendar.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/meetup-calendar.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Share your event</h4>
<div class="step-text">
  Once you have the date and place for your meetup, let your group know about it! <br />
  <br />
  If you want to keep the meetup only for the contacts you already have, it’s probably enough to announce it in your communication channel.<br />
  <br />
  If it will be open for newcomers to join, it’s important to spread the word: You can create an event online and share it on social media or your local news.<br />
  <br />
  And you can add your meetup as an event to the Precious Plastic event page, so the community can find you more easily.<br />
  <br />
  👉 community.preciousplastic.com/events
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/cb-meetup-event-2.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/cb-meetup-event-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/cb-event.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/cb-event.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Make a plan</h4>
<div class="step-text">
  Now it’s time to make a more detailed plan for your meetup. Write down what you want to find out and achieve and think about what you will do with your group.<br />
  <br />
  This will depend a lot on the number and kind of people coming. Newcomers will need more explanations about Precious Plastic, more experienced people will probably focus more on figuring out how to work together to build a stronger network.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/meetup-agenda.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/meetup-agenda.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Prepare materials</h4>
<div class="step-text">
  Prepare everything you need to make your plan happen.<br />
  Organise a computer (and if available projector) for presentations and videos.<br />
  <br />
  Get paper and pencils (maybe post-its) if you want to brainstorm together and write down ideas and questions, and print if you have questionnaires to fill out or exercises to print out.<br />
  And think of any games or activities which could be nice icebreakers to get to know the group. <br />
  <br />
  If possible, also bring or ask someone to bring a camera to document your meetup and maybe even take notes of what’s being discussed. It’s always nice to capture and share these moments!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/cb-materials-2.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/cb-materials-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Extra: Prepare samples</h4>
<div class="step-text">
  It’s always nice to bring some Precious Plastic material samples and products to help people understand the process and get them excited about the results.<br />
  <br />
  If you can’t find anything locally, you can have a look at the Bazar and get some community creations.<br />
  <br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/communitypoint - samples.JPG">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/communitypoint - samples.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Prepare food</h4>
<div class="step-text">
  Food connects people in a very positive way, it’s a small thing which makes everyone happy!<br />
  <br />
  Maybe you can prepare something yourself, or talk in the group that everyone brings a bit or maybe even organise the meeting at a time that you can end it with a shared meal. (Also a good thing to make people come.)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/SK Community Food-1.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/SK Community Food-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Meet up time!</h4>
<div class="step-text">
  Time to meet! The following steps are based on the agenda we added in the download files. You can do exactly the same, but feel free to change it or add your own ideas so it fits to your context.<br />
  <br />
  Try to be there earlier than the meetup time so you can already prepare a bit. Greet the people when they arrive, introduce yourself and make sure they feel comfortable.<br />
  <br />
  When everyone is there, welcome everyone and give an overview of the agenda, the purpose of the meeting and how long it will probably take, so they know what to expect.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/SK Community-greet.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/SK Community-greet.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Introduction </h4>
<div class="step-text">
  Start with a starter round for the group to give everyone the chance to introduce themselves, so everyone has an idea what the background of the group is.<br />
  <br />
  It helps to prepare some questions: Ask for their name, where they live, what they do (occupation), and how much they&#39;ve been involved in Precious Plastic already. <br />
  <br />
  You can also add a fun question like: What’s your favorite icecream flavor or what animal would you like to be?
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/SK Community Group Chat 2.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/SK Community Group Chat 2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Talk about Precious Plastic</h4>
<div class="step-text">
  Now make sure everyone knows and understands Precious Plastic.<br />
  You can use the presentation from the download files and change it for your context, or you create your own.<br />
  <br />
  We also recommend showing the 1 min Starter Kit showcase videos to help understand the machines and possibilities.<br />
  <br />
  👉 tiny.cc/showcase-collection<br />
  👉 tiny.cc/showcase-shredder<br />
  👉 tiny.cc/showcase-sheetpress<br />
  👉 tiny.cc/showcase-extrusion<br />
  👉 tiny.cc/showcase-injection<br />
  👉 tiny.cc/showcase-mix<br />
  <br />
  Adjust the length and level of detail of your presentation to the level of knowledge and involvement of your group, so that newcomers get an understanding, and experienced recyclers don’t get too bored.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/SK Community-present.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/SK Community-present.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/SK Community Present-2.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/SK Community Present-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Group your group</h4>
<div class="step-text">
  Now let’s analyse the potential and interests in your group.<br />
  Let the group answer the following questions:<br />
  Which area would you be interested to start/contribute/offer? (collection, machine building, working with machines, funding, following/sharing)<br />
  What resources do we have available? (space, time, money, people)<br />
  Who would be volunteering, charging or investing money?<br />
  How much time could you invest (per day, per week, per month)?<br />
  In bigger groups it can be fun to do this as a grouping exercise where they have to position themselves to a certain category.<br />
  <br />
  Make sure to document this on a paper/digital document, so you can share it and refer to it when needed.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/SK Community-roles.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/SK Community-roles.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Network brainstorm</h4>
<div class="step-text">
  After finding out about the people and potential within your group, also brainstorm about more people, projects, companies, places etc. which might be useful for your local recycling network. This can be collection points or services, machine builders, recycling places, designers, shops, media etc.<br />
  <br />
  In best case, directly add new contacts to your contact list, so they don’t get lost.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/SK Community-network.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/SK Community-network.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Next steps</h4>
<div class="step-text">
  Time to proceed! Summarise what you learned about the group, and discuss how you can start collaborating. Note what’s existing and what is still needed (money, space, people...), and what questions need to be answered.<br />
  <br />
  Decide who will take care of which topic, so everyone knows what they can do. If you have a bigger group, you might have to make working groups, which can communicate within their topic.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/SK Community-badges.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/SK Community-badges.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Keep meeting</h4>
<div class="step-text">
  Once people know what to do and are excited to dive into it, meeting regularly can help a lot to keep them motivated, to stay updated and to keep strengthening the connections. Try to find a time, an interval and place which works for the team (or for at least one representing person of each working group).<br />
  If you have a fixed place, you could install a sign somewhere visually there.<br />
  <br />
  And (if not there yet), add a Community Point pin on the Precious Plastic map, so people can find this pin and get in touch with your group.<br />
  <br />
  <br />
  👉 community.preciousplastic.com/signup
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/organise-a-precious-plastic-meetup/cb-meetuptimes.jpg">
        <img class="step-image" src="/howtos/organise-a-precious-plastic-meetup/cb-meetuptimes.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>