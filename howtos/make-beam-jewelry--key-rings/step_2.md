### Polishing step 

 Now that you have your shape, you can start polish it. Make sure you start with the thickest grain and end with the finer grain. The thinner it is, the smoother and more transparent the surface will become. Dont forget to do polish the edges as well so that they are rounded and soft to the touch 