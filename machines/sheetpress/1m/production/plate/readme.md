# Press plate

<img width="600px" src="./Sheetpress-image.jpg">

[Build Video](https://www.youtube.com/watch?v=j3OctDe3xVk)

[Wiki](https://precious-plastic.org/home/library/machines/sheetpress-v4/)

**Bill of material** : [Vendor](https://gleich.de/us/products/g-al-c250/) - 500 Euro (2 pieces) - 1200x1200x10

**Data sheet** : [Vendor](https://gleich.de/us/wp-content/uploads/sites/7/2016/07/us_g.al_c250_precision_milled_plate_2019.pdf)

**Name** : G.AL® C250

**Type** : Alloy | EN AW 5083 [AlMg4,5Mn0,7]

**Remarks** 

- can be laser cut (too many holes)
- doesn't need surface finish for the second side
- does it need to be 'g-al-c250' ? 

**Features**

- Very uniform flatness
- Extremely low residual stress
- Very good corrosion resistance
- Very good homogeneity
- Very precise surface

**Attributes**

Area 2.934E+06 mm^2

Density	0.003 g / mm^3

Mass 3.867E+04 g

Volume 1.432E+07 mm^3

Physical Material Aluminum
Appearance Opaque(170,178,196)

Bounding Box
	Length 	 1200.00 mm
	Width 	 1200.00 mm
	Height 	 10.00 mm
