
### Wiring - 220V

{% include product_image.html src="resources/wiring_220V_basic.png"  title="Circuit Diagram" target="resources/v4Ex-220v-wiring.svg"  %}

### Wiring - 220V - With Contactors (Recommended!)

{% include product_image.html src="resources/v4Ex-Contactors.jpg"  title="Circuit Diagram" target="resources/v4Ex-Contactors.svg"  %}
