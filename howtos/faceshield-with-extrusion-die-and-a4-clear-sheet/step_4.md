### Staple the screen 

 Try to center the screen with your attachment then staple from one end.
3 staples suffice one on each end and one in the middle, going progressively from one side to the other.
Keep the staples open until the screen is really tight on the plastic stripe you extruded. You can repunch the screen to tighten if needed.
The plastic sheets can be sourced from overhead or like here from a lamination machine where we stuck two ends together.

The shield is only to be used in extreme cases where no other alternatives are possible. However it takes on most of the designs of approved PPE with a minimum of resource

You can get more details and updates if you follow this link. We cannot update both websites at the same times at the moment. Sorry for the inconvenience
https://www.plasticatbay.org/2020/03/29/plasticbay-faceshield-design