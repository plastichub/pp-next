### Cut pipe and bar 

 After threading the tip of pipe, we can cut it to a length that suites your machine. In this example 60mm was cut to allow room for the heating element and final sizing. 
While we're at it, we can cut two pieces of the square bar that will be used to make the mouth of the nozzle. It's important to cut the bar slightly wider than the tube. Leaving ~5mm extra on either side. 
