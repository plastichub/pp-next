### Choose the right glue sticks 

 Through testing we found that hot melt adhesives based on ethylene vinyl acetate copolymer (yellowish) work best for PP and HDPE. Also hot melt adhesives based on acylate (white) could work, but only for HDPE. 

Of course the composition of the glue will vary depending on the brand. In our research the following BÜHNEN hot melts worked well:
- A21325.1 (for PP)
- A20364.1 and J2169 (for HDPE)
