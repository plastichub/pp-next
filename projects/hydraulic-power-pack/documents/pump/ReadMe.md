## Hydraulic Power Pack

# Pump


- Flow Rate (Volumetric displacement per revolution x RPM)

	- Flow Rate as function of pressure graph

	- Nominal Flow Rate:  ~30 liters/minute

- Pressure: Up to 3150 PSI (Up to 217 bar)

- Volumetric Displacement per revolution

- Hydraulic Power Output

- Type: Gear Pump


[Video- Gear Pumps](https://www.youtube.com/watch?v=vDqec_gpaeA)
