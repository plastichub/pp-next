<?php
namespace SR;

require __DIR__ . './../vendor/autoload.php';

class SessionUtils
{
    public static function getSession($sid)
    {
        $db = \WeDevs\ORM\Eloquent\Database::instance();
        $session = Session::where(['session' => $sid])->first();
        if (empty($session)) {
            d($session, 'new2');
            $session = new Session();
            $session['session'] = $sid;
            // $session->save();
        }
        if ($session['status'] == 'closed') {
            $session = Session::firstOrNew(['session' => self::random()]);
        }
        return $session;
    }
    public static function random()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 32; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public static function endSession($sid)
    {
        $db = \WeDevs\ORM\Eloquent\Database::instance();
        $session = Session::where('session', $sid)->first();
        if ($session) {
            $session->update([
                'status' => 'closed',
            ]);
            $session->save();
        } else {
            error_log('invalid session', $sid);
        }
    }
    public static function toSID($session)
    {
        $sessionIn = urldecode($session);
        if ($sessionIn && strpos($sessionIn, '$') !== false) {
            $session = substr($sessionIn, strpos($session, '$') + 1, strlen($sessionIn));
        } else {
            $session = self::getSessionId($sessionIn);
        }
        if (!$session && strpos($sessionIn, 'srs') !== false) {
            $session = self::getSessionId($sessionIn);
        }
        return $session;
    }

    public static function getSessionId($sessionState)
    {
        $keyValuePairs = substr($sessionState, 3);
        $keyValuePairs = explode("=", $keyValuePairs);

        $sessionId = null;
        $length = count($keyValuePairs);
        for ($i = 0; $i + 1 < $length; $i = $i + 2) {
            $key = $keyValuePairs[$i];
            $value = $keyValuePairs[$i + 1];
            if ($key == "srs") {
                $sessionId = $value;
            }
        }
        return $sessionId;
    }

}
