{% if page.alternatives %}

### Alternatives

<div class="ty-vendor-plans">

{% for doc in site.machines %}  
    {% for alternative in page.alternatives %}
      {% if alternative == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
{% endfor %}
</div>
{% endif %}
