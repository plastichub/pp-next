### Glue with a heat gun (HDPE & PP) 


Hot melt adhesives don’t offer the same performance as 2-components-adhesives, but they provide an accessible, quick and wasteless solution for those applications with no need for a high performance bonding. 
 
We generally don't recommend glueing parts together as it will make the disassembly harder at the end of the product’s life. So check out other joining techniques first! If this is still needed, here some guidelines.
