### Download and start! 

 Ready and excited to start?
You're a hero!

In this download package you will find all the files you need to set up your Shredder Workspace.
Download and start your recycling journey!

👉 https://cutt.ly/starterkit-shredder 👈