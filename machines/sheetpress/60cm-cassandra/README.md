# Electric Mini Press - "Cassandra"

**Status** : Development/Testing

**ETA Rev. A** : 15th of Februrary 2021

---

## Todos

- [ ] Firmware
- [ ] Cabinett
- [x] Control-Panel
- [-] Loadcell
- [-] Heatpuck replacement
- [-] Localize sleeves
- [-] Update to Nema34
- [ ] Test conventional heat wire
- [ ] Test and optimize fume extraction
- [ ] Update BOM
- [ ] CAD:Apply OSR naming
- [ ] Laser:Apply OSR naming
- [ ] Components
  - [ ] update to servo with brake
  - [ ] servo / motor shelf cooling channels
- [ ] Lock clips
- [ ] 1:20-30 gearbox (1:60 gave around 4T pressure | 1mm/min plate travel | 15mm/min x travel)

## Todos - Rev. B

- [ ] Decrease stroke by around 10 cm, use mold cartridges instead
- [ ] Change chassis for manual leadscrew drive handle
- [ ] Increase spacing for pivots & links
- [ ] better mounting for slider rods
- [ ] links via CNC cut
- [ ] Better fume extraction all around
- [ ] table

---
 
![dev render 2](./resources/pics/dev_render2.JPG)

A Heat Press for recycling plastic. 700x700mm bed size to produce a 600x600 sheet.
Could also be used for compression moulding, bio-alternatives, etc

Platen stroke: 160mm

![development gif](./resources/pics/motion_3.gif)

Height adjustment: 0-40mm in 5mm increments

![Height adjuster](./resources/pics/height_adjuster.gif)

Clamping force: at least 2 tons

3D view 


<script src="https://embed.github.com/view/3d/TSltd/ElectricMiniPress/blob/master/MiniPress.STL"></script>

[edrawings](https://github.com/TSltd/ElectricMiniPress/blob/master/ElectricMiniPress.html)


Motor: [Nema 23](https://www.amazon.co.uk/RTELLIGENT-Stepper-57x80mm-Digital-Stepping/dp/B07VDC63TS/ref=pd_sbs_60_4/257-7992820-5444413?_encoding=UTF8&pd_rd_i=B07VDC63TS&pd_rd_r=feeb0714-9df4-45da-9c76-9da47c7a115a&pd_rd_w=wcdk1&pd_rd_wg=Kp9iJ&pf_rd_p=2773aa8e-42c5-4dbe-bda8-5cdf226aa078&pf_rd_r=4FCFW0W9KPJP1Y6KY0RZ&psc=1&refRID=4FCFW0W9KPJP1Y6KY0RZ) min 1.8Nm - preferably 3nm - £30-50

Gearbox: [NMRV030 15:1](https://www.amazon.co.uk/RETYLY-10-Reducer-Reducer-Worm-Reducer/dp/B082VVDQGH/ref=sr_1_1?dchild=1&keywords=NMRV030&qid=1591244507&s=diy&sr=1-1) -£30-50

Driver: [Pololu 36v4](https://www.amazon.co.uk/Pololu-High-Power-Stepper-Motor-Driver/dp/B07TD6B7JX)

Power supply: 48V DC

Leadscrew: Trapezoidal 20mm diameter 4mm pitch [Accu](https://www.accu.co.uk/en/carbon-steel-c45-trapezoidal-lead-screws/80757-L-Tr20x4-1R-1000-C45) 

Driven end turned down to a plain 14mm shaft, with turned collar bushing welded in place

![Screw collar exploded](./resources/pics/collar_sleeve_exploded.jpg)

![Screw collar](./resources/pics/collar_sleeve.jpg)

The collar is located between a pair of tapered roller bearings inside a bearing housing 

![Bearing housing](./resources/pics/bearing_housing.JPG)

Tapered roller bearings x2: [30204 j2/Q](https://www.bearingboys.co.uk/Metric-Sizes--SKF/30204-J2Q-SKF-Metric-Taper-Roller-Bearing-81796-p)

At the other end, the screw is turned down to 10mm, to locate in a 6204 bearing with snap ring. 
![screw other end](./resources/pics/screw_other_end.jpg)

This will allow the screw to be installed by passing it out through the end hole and then back into the gearbox, inserting the end bearing and locating with the snap ring.

Leadscrew nut: TR20 20x4mm lead trapezoidal bronze self lubricating, flanged. [Accu](https://www.accu.co.uk/en/search?query=TR20x4+%2820mm+X+4mm+Lead%29+Left-Hand+Flanged+Trapezoidal+Lead+Screw+Nuts+-+Bronze)

Slider bars: 20mm with [LBPR20 plain linear bushings](https://www.bearingboys.co.uk/LPBR-Closed-Linear-Plain-Bearings-/LPBR20-SKF-Closed-Linear-Plain-Bearing-114199-p)

Lifting arms

![arm](./resources/pics/arm%20assembly.JPG)

These are fabricated from laser cut parts and standard metric tube


Lifting arm bearings (bushings): [Oilite](http://www.bearingshopuk.co.uk/ob202416-oilite-bush/) or bronze ID20xOD24 x16mm and 32mm 

Heatplate isolating separators: 10mm lengths of [round PEEK rod](https://uk.rs-online.com/web/p/plastic-rods/0770692/) or [ceramic rod](https://uk.rs-online.com/web/p/ceramic-rods-bars/1583102/) These can be glued in position with high temperature silicone glue

Extraction: via 125m tube to activated carbon filter

Temperature control: 3x PID controllers- 1 for the upper platen, one for the lower platen main area and one for the outer ring on the lower platen (where the edge of the sheet mould will be)

---
Motion control- Ardunio

[Arduino sketch](./firmware/steppercontrol.ino) code for 2 pushbutton control (press and hold for up/down)

---

Force Calculations:

to demonstrate that the motor and gearbox are capable of at least 2 tons clamping force, lifting a platten that weighs 20kg

Lead screw calcs: 

Torque(raise) = F x Dm/2x(L+u x PI x DM)/(PI x Dm-u x L)

https://daycounter.com/Calculators/Lead-Screw-Force-Torque-Calculator.phtml

Toggle calcs:

P = F a / 2 h                    

where P = input force, F = output force, a = arm length, h = height (displacement of toggle pivot from line between end pivots)

https://www.engineeringtoolbox.com/toggle-joint-d_2077.html


Torque of 25Nm from the gearbox (as claimed) on a 20mm screw with a 4mm pitch and a brass nut with a frictional coefficient of 0.06 gives about 1575N pushing force on the nut.

Connected to an equal toggle mechanism of 125mm arm lengths, this would generate a perpendicular force of 820 newtons (80Kg) at the bottom of the stroke (toggle displacement 120mm), 98438 newtons (10 tons) at the top of the stroke (toggle displacement 1mm).

With a toggle diplacement of 5mm (press almost fully closed) the force generated is 19688 newtons or around 2 tons.

18nm from the gearbox (1.2NM x 15:1) gives 1135N push on the screw, generating 70938N (7.2T) at 1mm toggle displacement and 14118N (1.4T)at 5mm

Using a higher frictional coefficient of 0.1, torque of 18Nm from the gearbox gives 690N push on the screw, generating 359N (40Kg) at 120mm displacement 8628N (900Kg) at 5mm and 43125 (4.4T) at 1mm displacement. This seems like a more realistic estimate, given that friction on the collar and toggle pivot joints is not accounted for in this equation. Even half this would give a comfortable 2 ton max clamping force and still be able to lift 20kg from fully open. Also these calculations are based on the smallest stepper in the Nema 23 package size, up to 3nm are available

More detailed calculations will be performed using https://cdn.automationdirect.com/static/manuals/surestepmanual/appxc.pdf

At max motor speed of 400 RRM with the 15:1 gearbox and 4mm pitch screw, the motor would move the nut on the screw at 1.7mm/s, meaning the plattens would take about 70s to perform a full 120mm stroke. If a 10:1 gearbox were used, it would take 45s.


Heat Plate

700x700x10mm 

Aluminium [£422 each](https://lakelandsteel.uk/metal-sheet-steel-sheet/aluminium/aluminium-sheet-and-plate/10mm-aluminium-5083-sheet-plate-profiles-blanks-custom-cut-to-size-free-of-charge-enter-exact-dimension-for-pricing/)

Mild Steel [S275JR](https://www.metalsupermarkets.co.uk/grade-guide-s275-steel/) [£241 each](https://lakelandsteel.uk/metal-sheet-steel-sheet/mild-steel-s275-and-s355/mild-steel-sheet/10mm-thick-mild-steel-s275jr-sheet-plate-profiles-blanks-custom-cut-to-size-free-of-charge-enter-exact-dimension-for-pricing/)

A mild steel plate will probably be adequate as it is anticipated that this press is used with tray moulds rather than plastic having direct contact with the plate.


