### Assemble (Part 1) 

 There are various ways to assemble, so feel free to try out your own.
In this example, we'll start with the bottom level of the structure. Connect 10x 6-way-joints with 10x long beams into a decagon. Pay attention to keep all joints in the right direction.

6-way-joints: pink 
5-way-joints: blue
