### Participation 

 Think carefully about how to get strangers involved in the process. What can they do without endangering themselves or the machines?