### Prepare the barrel 

 We start by preparing the barrel.

All you have to do is remove the handles and drill two holes. One at the very top and one at the bottom of the barrel, so you are able to pump out all the water.

Use a 35 mm Forstner drill bit for the holes. Now you can attach the fittings.