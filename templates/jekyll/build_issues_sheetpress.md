
### Build Issues

<div class="alert alert-danger" role="alert">
    Fatal: Drilling Cartridge heaters with a M12 thread leaves up to 0.4 mm play. It's impossible to use the mentioned heat glue to fill up this space since it's very dry and doesn't stick to anything. Instead, use a 11.75 drill and finish it with a 11.9mm drill. Please check the how-to section about make this faster on a lathe.
</div>

<div class="alert alert-danger" role="alert">
Fatal: The press plate is more likely not flat and the used mounts won't flatten the plate. Use level setscrews at 9 points instead. Check the howtos.
</div>

<div class="alert alert-warning" role="alert">
Warning: It's extreme heavy to assembly and build this machine without a forklift.
</div>

<div class="alert alert-warning" role="alert">
Warning: It's unlikely to build the sideframe precise, put level setscrews at the feets of the sideframes.
</div>

<div class="alert alert-danger" role="alert">
Fatal: It's unlikely to make good and precise sink holes into the press plate with a handdrill. The handdrill gets extreme hot already after 15 - 20 holes. Use M6 holes instead but also use a small table drill press.
</div>

<div class="alert alert-warning" role="alert">
Warning: Please use at least M18 bolting to hold the upper press-plate.
</div>

<div class="alert alert-info" role="alert">
Tip: Use laser cut bars for the press plate frame. This way the frame will be more precise since it's also more easy to weld.

{% include product_image.html src="resources/press-plate-frame.JPG"  title="Press Plate Frame" %}

</div>
