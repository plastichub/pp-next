### Weld seams 

 In case your mould shapes geometries where the material has to flow around, chances are that at the point where the material meets again a "line" is visible. This is also the case if your mould has multiple gates into one cavity.
Those lines are called weld seams. They vary from a line which is only visible at the surface of the part to a line which actually is a surface through the whole geometry. In those cases the structural rigidity of this section will be influenced and a predeminated breaking point is created.

Solutions:
- Increase the material temperature
- Increase the mould temperature
- Check if the material is clean from any contaminiations, especially free of dust or oils