---
image: "/howtos/make-an-hdpe-knife/HDPE knive brothersmake.png"
title: "Make an HDPE Knife"
tagline: ""
description: "We show you how to make this &#39;reverse knife&#39; (metal scales and plastic blade) using at-home techniques and processes. The knife is &#39;sharpened&#39; but cannot hold an edge the same way a metal blade can. However it works perfectly fine for cake, fruit and vegetables. A commenter on our YouTube video also said it would be perfect for cutting lettuce as metal knives make the cut edge turn brown quickly!"
keywords: "melting,product,sheetpress,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
- "product"
- "sheetpress"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/brothers-make">brothers-make</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Knife Design</h4>
<div class="step-text">
  We aimed for a &#39;santoku&#39; style knife and drew a few different designs out. We actually asked our Instagram followers to pick the one they liked the best!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/HDPE knife - 1 - sketch - 2.JPG">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/HDPE knife - 1 - sketch - 2.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/HDPE knife - 1 - sketch - 1.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/HDPE knife - 1 - sketch - 1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Prepare the Plastic and Mould</h4>
<div class="step-text">
  Fort this design we wanted a black and white marble effect so we used these bottles (the product is called &#39;Fortisip&#39;) which are white but with a black core. We cut them up into thin strips ready for melting (all the plastic we have stored has already been washed and dried).<br />
  <br />
  The mould is a simple stacked plywood construction with varnished inner faces to prevent sticking. We don&#39;t go into detail about that here but check out our HDPE Stool video on our YouTube channel where we show the mould construction in more detail.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/HDPE knife - 2 - mould 2.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/HDPE knife - 2 - mould 2.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/HDPE knife - 2 - mould.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/HDPE knife - 2 - mould.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Melt the HDPE</h4>
<div class="step-text">
  We use a flat plate sandwich toaster to melt HDPE. We will occasionally use a small toaster oven as well, but this is mostly for larger melts where we need to keep a bigger mass of plastic hot. For this project we were aiming for around 8mm in thickness for the blade so the sandwich toaster can cope fine on its own.<br />
  <br />
  When melting, the plastic will shrink a lot so keep adding more every few minutes, taking care not to introduce any air bubbles.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 19.58.39.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 19.58.39.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 19.58.51.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 19.58.51.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 19.59.00.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 19.59.00.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Fold and Twist</h4>
<div class="step-text">
  To maximise the marbling effect and to further reduce air bubbles, we like to pick up the plastic so that we can fold and twist it. Be extremely careful whenever handling molten plastic. We wear 2 pairs of gloves when doing this. The outer pair are called &#39;silicone oven mitts&#39; and are the best gloves we have tried for doing this.<br />
  <br />
  When folding and twisting be very careful make sure you are not introducing any more bubbles. Once you are happy, put it back in the sandwich toaster (HDPE cools rapidly and it needs to be as hot as possible before transferring to the mould!)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.00.47.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.00.47.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.00.07.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.00.07.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 19.59.18.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 19.59.18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Press the Sheet</h4>
<div class="step-text">
  Once the plastic is hot enough, transfer it into the mould, put on the top plate and get it into the sheet press. We are using our DIY 2-tonne bottle jack press. We also have a video of how to make this on our YouTube channel.<br />
  <br />
  Leave to cool overnight. The HDPE will shrink as it cools so we like to come back every 5-10 minutes to add more pressure for the first hour or so. That way you will end up with the flattest sheet possible with no warping.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.01.07.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.01.07.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.01.22.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.01.22.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.01.31.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.01.31.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - De-Mould and Apply Template</h4>
<div class="step-text">
  The sheet often pops right out of the mould but occasionally we need to gently persuade it with some pliers. Once the sheet is out we cut off any flashing (saving to reuse again of course!) and stick on the chosen template design.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.06.08.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.06.08.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.06.13.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.06.13.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.06.29.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.06.29.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Cut out the Blade</h4>
<div class="step-text">
  We like to use the scroll saw for this as it creates much less waste. Wherever possible we keep as much of the waste produced for future projects. The cut edges gives a glimpse of the marble effect on the blank!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/IMG_2100.JPG">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/IMG_2100.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/IMG_2104.JPG">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/IMG_2104.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.06.35.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.06.35.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Start the Aluminium Scales</h4>
<div class="step-text">
  We used 8mm aluminium plate for the scales. We traced the template onto the metal and cut out 2 pieces with an angle grinder. We also marked out the 3 hole positions for the pins and centre-punched these.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.01.55.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.01.55.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.02.21.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.02.21.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.02.34.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.02.34.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Drill and Shape the Scales</h4>
<div class="step-text">
  We drilled the 3 pin holes and used some brass rod with a small amount of superglue to hold the 2 scales together. This way when we are shaping on our grinder they both come out exactly the same.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.03.00.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.03.00.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.03.18.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.03.18.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.03.54.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.03.54.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Transfer the Holes to the HDPE</h4>
<div class="step-text">
  Once we were happy with the shape of the scales, we tapped out the pins and carefully lined up the handles with the plastic blade. This was clamped in position and we then drilled through to get the exact locations onto the plastic. We also added a deep countersink to the outer faces of each of the aluminium scales.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.07.24.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.07.24.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.07.34.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.07.34.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Making HDPE Pins</h4>
<div class="step-text">
  We wanted the pins to match the blade, so we figured we would try and make out own out of some of the leftover HDPE. We cut a long, thin piece and then used a chisel to take off the corners. We then held this in our drill and used some sandpaper to turn this into a long dowel that we could then cut into pieces for our pins.<br />
  <br />
  It was the first time we tried this and it worked great!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.07.42.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.07.42.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.08.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.08.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.13.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.13.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Fixing the Pins</h4>
<div class="step-text">
  We inserted the pins into the handle and used a heat gun to soften the plastic until it could be pressed firmly against a hard surface. This was to get it roughly in place so it didn&#39;t matter if it wasn&#39;t perfect. Once we had done this for all 3 pins, we gently heated up both sides of each of the 3 pins (being VERY careful not to heat up the plastic blade!) and clamped it as tight as possible in a vice. Remember it will shrink as it cools, so keep tightening it every 5-10 mins!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.20.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.20.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.37.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.37.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.53.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.08.53.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Sand the Pins Flush</h4>
<div class="step-text">
  We VERY carefully sanded the pins flush to the surface of the aluminium scales. The low melting temperature of HDPE means that it warps incredibly easily so we kept is cool by dunking it in a bucket of water every few seconds. The pins turned out awesome though!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.11.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.11.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.14.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.14.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Final Shaping on the Blade</h4>
<div class="step-text">
  We refined the shape of the plastic blade using the belt grinder and spindle sander. We also draw on our bevel line and sanded to this using the belt grinder.<br />
  <br />
  This was tricky as the plastic turns very &#39;paper-y&#39; as it gets towards a sharp point. So we didn&#39;t try to make it too thin otherwise it could have potentially split on the very edge.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.20.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.20.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.33.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.33.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.27.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.27.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Final Finishing.</h4>
<div class="step-text">
  HDPE finishes really well straight off of a sharp tool. To really bring out the marble effect, we took a razor blade to &#39;scrape&#39; the surface of the plastic. This left a great finish and required no more sanding.<br />
  <br />
  To finish the handle we wet sanded up to 2,000 grit with wet and dry paper. We then used a product called Micro Mesh to take it from 2,000 to 20,000 grit which gave it a great finish.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.46.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.09.46.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.10.07.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.10.07.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.10.18.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.10.18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Test it out!</h4>
<div class="step-text">
  We asked our Patrons what they would like to see us cut with the knife and the 2 overwhelming responses were CAKE and A TOMATO! So that&#39;s exactly what we did! The blade actually cut them both surprisingly well, however it did struggle to get through the sugary crust on the banana cake (it tasted great though!)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/IMG_2162.jpg">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/IMG_2162.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/IMG_2141.jpg">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/IMG_2141.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.10.49.png">
        <img class="step-image" src="/howtos/make-an-hdpe-knife/Screenshot 2020-08-05 at 20.10.49.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Watch the Video!</h4>
<div class="step-text">
  Most of the images were taken as screen captures from the video we produced. If you want to watch it then it is linked below. Would be great to hear what you think!<br />
  <br />
  We hope this How-To was helpful!<br />
  <br />
  Matt &amp; Jonny<br />
  Brothers Make
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>