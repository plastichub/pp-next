### Shred your plastic 

 Using your Shredder, shred the collected plastic waste into small shreds ready to be used in the other machines.

Make sure to shred plastic by type and, as a plus, by colour, so you can creat beautiful patterns later on.