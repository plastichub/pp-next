### Finish the raincoat 

 Finishing the raincoat could be different depending on how/if you choose to fasten the front. In any case, finishing should begin with sewing clean any unfinished seams (the bottom edge for example). 

The process should then finish with any fastenings that you choose to incorporate; this could be buttons, poppers, a zip, or any alternatives that you can think of.
Now it’s ready to wear!
