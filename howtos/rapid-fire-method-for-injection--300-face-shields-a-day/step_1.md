### Load Injection Machine 

 Take the hot stripes and insert them into the barrel of the injection machine. Try to extrude them a bit thinner than the diameter of your barrel. If they are too thick it will be a pain to load the machine. If they are to thin the barrel won't be loaded to its full extent.