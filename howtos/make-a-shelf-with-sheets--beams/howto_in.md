### Make a shelf with sheets & beams 


Contrary to popular belief, plastic is durable and tough enough to be used as a supporting material as well!

Here we show how you can make a  shelf completely made of recycled plastic sheets and beams.