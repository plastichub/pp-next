---
image: "/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200404_095358-COLLAGE.jpg"
title: "Faceshield with extrusion die and A4 clear sheet"
tagline: ""
description: "We make a face shield from basic elements and the extrusion machine. Face shields are critically lacking in many countries affected by COVID-19. You need up to 22 per patient. We need a DIY way of making these and making these fast..<br />Remember face shields are only a complement to googles and masks and are meant to reduce the viral charge."
keywords: "extrusion,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/plasticbay_julien">plasticbay_julien</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Make a nozzle to attach to the extruder.</h4>
<div class="step-text">
  The nozzle needs to have a relatively large entry and a tapering end.<br />
  The best is to have some bars to cool down a bit the plastic and funnel it towards the exit.<br />
  To activate the nozzle we heat it up with hot air gun<br />
  The outside plates are only 1mm apart <br />
  You can also do it without nozzle but it is even more complicated to control the thickness and elasticity of the future band<br />
  <a href="https://www.youtube.com/watch?v=BTiQqPFE9vs">https://www.youtube.com/watch?v=BTiQqPFE9vs</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_142311.jpg">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_142311.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/flatnozzle.png">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/flatnozzle.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Extrude the stripe</h4>
<div class="step-text">
  This is a delicate part and needs improvement as you need to pull the plastic out so that the thickness and flow is even. Make your motor rotate at the slowest speed possible.<br />
  Because of changes in the thickness and rate of cooling your stripe might twist<br />
  We heat it up back with the hot air gun and pass it inside our metal mangle. This will straighten the band. Don#t expect miracles, you will keep the concave shape. Although in the end it makes no difference with our design<br />
  Based on the work on the Badger shield<br />
  <a href="https://www.delve.com/assets/documents/OPEN-SOURCE-FACE-SHIELD-DRAWING-v1.PDF<br/>">https://www.delve.com/assets/documents/OPEN-SOURCE-FACE-SHIELD-DRAWING-v1.PDF<br /></a>
  We can see that ideally the contact with the head should be 13&quot;/ 33 cm. Cut to length<br />
  Here we have used HDPE from ocean plastic we regularly recover on the local beaches. We recommend HDPE as it is easier to source and also low temperature to melt.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_152146.jpg">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_152146.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_151044.jpg">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_151044.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Punch holes for the attachment</h4>
<div class="step-text">
  The best for the attachment is an elastic band for clothes but we didn&#39;t have any so made with a rope like in the example of <a href="https://menorplastic.com/tutorial-para-fabricarte-tu-protector-facial-casero/<br/>">https://menorplastic.com/tutorial-para-fabricarte-tu-protector-facial-casero/<br /></a>
  however to avoid making knots and for an easy attachment you can simply punch a set of holes <br />
  A square wood chisel size 10 is perfect for that.<br />
  You have to make 3 sets of 2 holes and one set of 3 holes at one end (could be two if you use rope)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_155038.jpg">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_155038.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/schematic holes.png">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/schematic holes.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/punchstripe.png">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/punchstripe.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - lace the plastic band</h4>
<div class="step-text">
  Follow the right order for comfort, according to drawing<br />
  The 2-hole design allows to tighten at any size of head quickly.<br />
  The third hole design is to pass the elastic band in a very secure latch.<br />
  You can also make a similar knot as in the third figure for easier adjustment.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_161519.jpg">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/IMG_20200328_161519.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/designopenmould.png">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/designopenmould.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/fancyknot.png">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/fancyknot.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Staple the screen</h4>
<div class="step-text">
  Try to center the screen with your attachment then staple from one end.<br />
  3 staples suffice one on each end and one in the middle, going progressively from one side to the other.<br />
  Keep the staples open until the screen is really tight on the plastic stripe you extruded. You can repunch the screen to tighten if needed.<br />
  The plastic sheets can be sourced from overhead or like here from a lamination machine where we stuck two ends together.<br />
  <br />
  The shield is only to be used in extreme cases where no other alternatives are possible. However it takes on most of the designs of approved PPE with a minimum of resource<br />
  <br />
  You can get more details and updates if you follow this link. We cannot update both websites at the same times at the moment. Sorry for the inconvenience<br />
  <a href="https://www.plasticatbay.org/2020/03/29/plasticbay-faceshield-design">https://www.plasticatbay.org/2020/03/29/plasticbay-faceshield-design</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/2020-03-28-164551.jpg">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/2020-03-28-164551.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/2020-03-28-164622.jpg">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/2020-03-28-164622.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/2020-03-28-164612.jpg">
        <img class="step-image" src="/howtos/faceshield-with-extrusion-die-and-a4-clear-sheet/2020-03-28-164612.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>