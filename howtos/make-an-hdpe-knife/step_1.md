### Prepare the Plastic and Mould 

 Fort this design we wanted a black and white marble effect so we used these bottles (the product is called 'Fortisip') which are white but with a black core. We cut them up into thin strips ready for melting (all the plastic we have stored has already been washed and dried).

The mould is a simple stacked plywood construction with varnished inner faces to prevent sticking. We don't go into detail about that here but check out our HDPE Stool video on our YouTube channel where we show the mould construction in more detail. 