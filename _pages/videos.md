---
permalink: /videos/
title: "Videos"
excerpt: "Small videos"
toc: true
sidebar:
    nav: "resources"
---



### Shredder

Shredder electronics

{% include video id="H3RVSF6ZR70" provider="youtube" %}

Auto-Reverse

{% include video id="yYp9CV5yeQg" provider="youtube" %}

### Extrusion

Screw Components

{% include video id="zWpKdaYInZ0" provider="youtube" %}

Making an extrusion screw

{% include video id="VAPhJLgrPAI" provider="youtube" %}


### Injection

#### Arbor Injection

Arbor Injection walkthrough

{% include video id="9yOqrhqlp7Q" provider="youtube" %}


