### Start collecting  

 Finally, it’s time to take care of your weekly/monthly pick up. Do a little check, when you receiving the collected plastic. If everything is collected in the way you did agree on (the right types, clean and label-free) you can take it to your Collection Point. The way of transporting the collected plastic depends on what suits your local environment. When everything works smoothly you can start collecting from multiple businesses and grow your collection network. 
 
Tip: Ask every now and then if everything is still fine and if you can do something to make your collaboration better. 
