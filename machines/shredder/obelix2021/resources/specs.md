# Shredder for Q1 2021


## Latest update: Gearboxes


Gearbox selected will be the Cidepa CKM-67.3 1/60 3 



Reference	CKM-67.3 1/60 3

Installed HP	3,00

Installed Kw	2,20

Ratio	60,0

Motor Option	Cidepa electric motor B-5

Mounting Position	Feet

Output Shaft diameter	40

Size	67

Service Factor	0,90

Rated Torque Nm	890,00

Output Rpm	23,00

Accurate gear ratio	60,66

F. Radial Fr2 (N)	9.490,00

Gear trains	3

Weight	61,00 Kg.



The motor is an MT4 1500 which is actually rated at 3kw, guess they make an allowance for the gearbox efficiency.



So the next question is about the gears, what size/module is needed.

Max torque is 890Nm shared between the two shafts, so the transmitted torque will be 445Nm



https://shop.hpceurope.com/docFichesTechniques/SpurGear_TorqueCalculation.pda 

https://www.engineersedge.com/gear_formula.htm

https://www.engineersedge.com/calculators/gear-tooth-strength-calculator.htm

https://www.mechanicdrive.com/spur-gear-calculation

https://khkgears.net/new/gear_knowledge/gear_technical_reference/gear_materials.html



for equal gears 1:1, the axle centre distance = pitch  diameter; we need as low as possible but minimum 82


MODULE 4

A module 4 gear with 21 teeth has a pitch circle diameter of 84mm and an outside diameter of 92mm. 

Off the shelf gear part number 7702-910 has a face width of 40mm

DP size is 6 (how many teeth will fit on 1" of the gear cirumference)

According to the calculator, with a Lewis factor of 0.34, a carbon steel this size gear will transmit 369Nm

This is insufficient.


MODULE 5

A module 5 gear with 17 teeth has a pitch circle diameter of 85mm

This has an estimated Diametric pitch of 4, so a carbon steel gear of face width 50mm could transmit 554Nm

https://www.mechanicdrive.com/spur-gear-module-5-17-teeth.html

The total length including hub is 75mm. The centre hole needs to be bored out to 40mm and keyway broached to suit the axle.





---

Rough spec:

- single phase motor max 2.2kw frame size 100/110

- slowest motor speed possible 1400rpm - 4 pole 

- full load current for 2.2kw motor at 1400rpm = 12.12A

- size 110 or 90 gearbox ?

- slow speed ideally around 20rpm

- 40mm shaft, 50mm hex section

- mod 4 or 5 gears?

- 1/3 size of Chinese version and similar

- bearing set into endplate

- axles as close together as possible

- 15mm thickness steel

- laser cut and finshed with mill or cnc

- cutting edges parallel to axle (straight cut)

- alternating blades and spacer rings






Previous research:



My research suggests that the NMRV size 90 gearbox can only go down to 44rpm with a 1400 rpm 2.2kw motor. This is a 30:1 ratio. 

The high ratio NMRV 90 gearboxes can only be used with lower power motors as the output shaft is too small. The max output of an NMRV gearbox is around 270Nm with whichever combination of motor and ratio.

The size 90 gearbox at 80:1 can be used with a 0.75kw (1hp) motor to give 262 Nm at 18rpm.
NMRV 'Power' 90 at 100:1 apparently gives 270Nm. These have a max output shaft size of 38mm on request, standard is 35

The size 90 gearbox at 60:1 can be used with a 1.1kw motor to give 288Nm at 23.3rpm.




110 size gearbox:

With a 2.2kw motor,

The NMRV 110 gearbox at 80:1 gives 648 Nm at 23.3rpm


We could also consider a helical gearbox which can give 877Nm at 23rpm through a 40mm output shaft.  This is around 900 euros including 2.2kw motor. It's inline so would be awkward for design, but much higher torque than a worm drive. Even an NMRV 130 can only give 816 Nm at 17.5rpm.



---














Bearing options

Ball bearings:


6308-2RS Dunlop Sealed Ball Bearing 40mm x 90mm x 23mm = £9.32 each

https://www.bearingboys.co.uk/Dunlop-Ball-Bearings/63082RS-Dunlop-Sealed-Ball-Bearing-40mm-x-90mm-x-23mm-1945-p


62208-2RS1 SKF Sealed Ball Bearing 40mm x 80mm x 23mm = £21.59 each

https://www.bearingboys.co.uk/SKF-Ball-Bearings/63082RS1-SKF-Sealed-Ball-Bearing-40mm-x-90mm-x-23mm-1943-p





Roller bearings:


NU2208 ECP SKF Cylindrical Roller Bearing 40mm x 80mm x 23mm = £45.11 each 

Static Load: 75kN  Dynamic load: 81.5kN

https://www.bearingboys.co.uk/N-Type-Cylindrical-Roller/NU2208-ECP-SKF-Cylindrical-Roller-Bearing-40mm-x-80mm-x-23mm-2912-p


NUP308 ECP SKF Cylindrical Roller Bearing 40mm x 90mm x 23mm = £59.88 each

Static Load 78kN  Dynamic Load: 93kN



Motor:

Cidepa KM 2.2kW 1400rpm


60:1 gearbox Cidepa

Reference	KM-110/ 60 3
Installed HP	3,00
Installed Kw	2,20
Ratio	60,0
Motor Option	Cidepa electric motor B-5
Size	110
Service Factor	0,90
Rated Torque Kpm	68,19
Output Rpm	23,00
Efficiency	0,73
Accurate gear ratio	60,00
HP available	2,19
Weight	40,50 Kg.

Size: 295 x 255 x 144mm

Torque calcs

TNm = PW 9.549 / n  

where

TNm = torque (Nm)

PW =  power (watts)

n = revolution per minute (rpm)

Motor axle
2.2kw x9.549 / 1400
=15Nm

After gearbox
=2.2kw x 9.549 / 23.3

T = 901.6Nm

(To check, 15Nm x 60 = 900Nm)

At 73% efficiency
T = 658.2Nm

So roughly 650Nm


If the motor is 2800rpm, torque will be half.



Gears:

need to establish torque, size, module, supplier

Assuming trying to achieve minimum distance between axles, using 80mm roller bearings, minimium axle spacing = 82mm centers

So ideal 'pitch circle' of gear = 82mm (maybe 84)


Torque
Assuming 650Nm output from gearbox, divided over 2 axles, gear must transmit 325Nm

This table suggests that even some module 3 gears with a 20mm bore can withstand this force https://khkgears.net/pdf/ksg.pdf

(that seems optimistic to me)


RV Gearboxes - can we use a smaller size?

NMRV Power gearboxes from Motovario

https://www.transdrive.co.uk/media/1272/nmrv-nmrvp-catalogue.pdf

Sizes:

NMRV 90  238 x 206 x 130mm   24mm input, 38mm ouput 
NMRV 110  295 x 250 x 144mm   28mm input, 42mm output  - NB- Cidepa equivalent KM 110 is 288 x 255 x 144 so assume 300 x 260 x 150mm and pack
NMRV 130  335 x 292.5 x 155mm   38mm input, 45mm outpit




NMRVpower size 90 (Motovario)

https://www.transdrive.co.uk/shop/products/gearboxes/motovario/right-angle/nmrv-p090/motovario-worm-gearbox-nmrv-p090-ratio-60-1-iec80-b5-3043638




Flange- B5 or B14?

NMRV90 is only available up to 60:1 with a B5 flange, 80 or 100:1 are B14 flange (smaller face mount)

NMRV-P090

Ratio - 60:1

Frame Size / Mounting Position : IEC80 B5

Hollow output – 35mm standard (39mm is available)

Max torque 1.550 Nm and admissible radial loads max 16.500 N

Motor size 100/110

Part number – 3043638



To get lower speeds we need a bigger gearbox, NMRV 110 or NMRV 130

According to the PDF, with a 3HP (2.2kw) motor 100LA4:

NMRV 90 at 40:1 will give an output torque of 3321 in-lb	= 375 Nm at 44rpm = 67% efficient

NMRV 110 at 60:1 will give 4852 in-lb = 548 Nm at 29rpm = 62.5% efficient

NMRV 130 at 80:1 will give 5965 in-lb =	674 Nm at 22rpm


NMRV 90 at 60:1 or 80:1 are not shown on the chart but assuming the same efficiency:

**NMRV 90 at 80:1 = 14.6Nm x 80 = 1168Nm, x 67% = 782.56Nm at 22 rpm**

**NMRV 90 at 60:1 = 14.6NM x 60 = 876Nm x62.5% = 543Nm at 33rpm**

These are very high torque values which require a bigger shaft, which is why the bigger gearbox size is needed

The 110 size at 60:1 is the best compromise.







Info on the 100LA4 motor: https://new.abb.com/products/3GAA102001-ASE/m2aa-100-la4-b3-2-2kw-ac


50Hz	230V	2.20kW	1430rpm	8.52A	

Service factor 0.780	

Efficiency 82.30%	Torque 14.60 N·m






What about parallel shaft motors? more torque? slower? YES but more expensive 

JS-DV473-100-4-2,2 kW  
20 rpm 1003Nm €1,225.75 

https://www.js-technik.de/en/products/gear-reducer-geared-motors/parallel-shaft-gearmotors/12375/js-dv473-100-4-2-2-kw-20-rpm-parallel-shaft-gearmotor?c=380

 14 rpm  1439Nm €1,225.75
 
 https://www.js-technik.de/en/products/gear-reducer-geared-motors/parallel-shaft-gearmotors/12376/js-dv473-100-4-2-2-kw-14-rpm-parallel-shaft-gearmotor?c=380

JS-DV473-100-4-2,2 kW-26 rpm 774Nm  €1,225.75 

https://www.js-technik.de/en/products/gear-reducer-geared-motors/parallel-shaft-gearmotors/12374/js-dv473-100-4-2-2-kw-26-rpm-parallel-shaft-gearmotor?c=380

all with 50mm shaft


Compared to worm drive, all size 130 with 45mm output

JS- CMRV 110-100L1-4 - 2,2 kW - 23,3 rpm- 648 Nm €845.54 

https://www.js-technik.de/en/products/gear-reducer-geared-motors/worm-gear-reducer-motors/11023/js-cmrv-110-100l1-4-2-2-kw-23-3-rpm-worm-gearbox-motor?c=178

JS- CMRV 110-100L1-4 - 2,2 kW - 17.5 rpm- 816 Nm €967.51

https://www.js-technik.de/en/products/gear-reducer-geared-motors/worm-gear-reducer-motors/11119/js-cmrv-130-100l1-4-2-2-kw-17-5-rpm-worm-gearbox-motor?c=178


Slowest you can go without additional prestage reducer is 

JS- CMRV 090-100L1-4 - 2,2 kW - 46,7 rpm 351 Nm €668.74



Compared to Bevel drive

JS-KV373-100-4-2.2 kW-23 rpm 853Nm €1,086.47 

https://www.js-technik.de/en/products/gear-reducer-geared-motors/bevel-gear-reducer-motors/11682/js-kv373-100-4-2.2-kw-23-rpm-bevel-geared-motor?c=251


JS-KV473-100-4-2.2 kW-14 rpm 1372Nm €1,491.64

https://www.js-technik.de/en/products/gear-reducer-geared-motors/bevel-gear-reducer-motors/11688/js-kv473-100-4-2.2-kw-14-rpm-bevel-geared-motor?c=251




Compared to Helical

https://www.js-technik.de/en/products/gear-reducer-geared-motors/helical-gear-reducer-motors/6151/helical-gear-motor-js-mv-373-100l1-2.2kw-23rpm?c=177

Helical-Gear-Motor-JS-MV-373-100L1 - 2.2kW-23rpm 877Nm €887.84



With a 2.2kw motor the NMRV size 90 gearbox can only go down to 44rpm because the lower ratios give too much torque for the shaft

2.2kw motor with the Motovario 110 size NMRV 110 (same as Cidepa KM 110) at 60:1 will give 548 Nm at 29rpm. That would be within spec. 

It could be fitted with an 80:1 gearbox to give 782.56Nm at 22 rpm but that is after compensating for efficiency, the actual torque would be 1168Nm so it would be above what the shaft can take.

For the best torque from a 2.2kw motor we actually need a 130 size gearbox.

with 2.2kw motor, NMRV 130 at 80:1 will give 5965 in-lb	674 Nm at 22rpm

NMRV 130 is 335 x 292.5 x 155mm   38mm input, 45mm output




CHEAP GEARBOX! : £221.20 each

Size 90 Right Angle Worm Gearbox 80:1 Ratio  

18 rpm Output speed using a 4 Pole (1400 rpm) Electric Motor and 262 Nm Output Torque.

The maximum motor input size for this gearbox is 80 frame and flange size B5/B14.

https://www.ebay.co.uk/itm/Size-90-Right-Angle-Worm-Gearbox-80-1-Ratio-18-RPM-Motor-Ready-Type-NMRV/321061910748

frame size 80 is a 0.75kw motor at 1400rpm - can this be believed?

TNm = PW 9.549 / n

where

TNm = torque (Nm)

PW =  power (watts)

n = revolution per minute (rpm)

= 5.12 NM
 at 64% effifciency = 3.28nm
 
 x 80 = 262Nm Correct
 
 
 
 ..
 In theory, from motovario catalogue compatibility of motors:

NMRV 90 gearbox with size 80 motor (0.75kw, 1hp) max 100:1  gives 511Nm x 64% = 327Nm

NMRV 90 gearbox with size 90 motor (1.1kw, 1.5hp) max 60:1 gives 450Nm x 64% = 288Nm

NMRV 90 gearbox with size 100 motor (2.2kw, 3hp) max 30:1  also 450Nm = 288Nm


Suggests the NMRV output is limited to around 300Nm



the NMRV 110 is available in  up to 60:1 for a size 100 motor (2.2kw), which gives 900 x 64% = 576Nm

or up to 100:1 with a size 90 motor (1.1kw) which gives 750 x64% = 479Nm





