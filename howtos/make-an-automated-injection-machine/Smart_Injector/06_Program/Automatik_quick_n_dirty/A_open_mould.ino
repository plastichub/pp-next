
/*
Automatic injecting extruder (close mould)
*/

void open_mould()
{
	Kvalendstop2 = digitalRead(Kendstop2);
	while (Kvalendstop2 == LOW)
	{
		if (Kdt > Kdtend)
		{
			Serial.println("accelerate");
			Kdt = Kdt - 10;
		}
		for (int i = 0; i < Kstepsinfor; i++)
		{
			digitalWrite(DIR, HIGH);
			digitalWrite(DIR2, HIGH);
			digitalWrite(EN, HIGH);
			digitalWrite(EN2, HIGH);
			digitalWrite(PUL, LOW);
			digitalWrite(PUL2, LOW);
			delayMicroseconds(Kdt);
			digitalWrite(PUL, HIGH);
			digitalWrite(PUL2, HIGH);
			delayMicroseconds(Kdt);
		}
		Kvalendstop2 = digitalRead(Kendstop2);
	}
	if (Kvalendstop2 == 1)
	{
		for (int i = 0; i < Klimitedsteps2; i++)
		{
			digitalWrite(DIR, HIGH);
			digitalWrite(DIR2, HIGH);
			digitalWrite(EN, HIGH);
			digitalWrite(EN2, HIGH);
			digitalWrite(PUL, LOW);
			digitalWrite(PUL2, LOW);
			delayMicroseconds(Kdtref);
			digitalWrite(PUL, HIGH);
			digitalWrite(PUL2, HIGH);
			delayMicroseconds(Kdtref);
		}
	}
	Kdt = Kdtref;
}

