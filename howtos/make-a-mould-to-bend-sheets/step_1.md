### 3D modelling 

 Take all the measurements you derived from your prototype and feed it to CAD. For curvatures, take a quick photo, place it in CAD, scale it to the right size and rebuild the curve from there.
The mould consists of two parts, the positive and the negative half that encapsulate your desired part from both sides. The bending surface is a 1mm sheet metal screwed onto cross-sections cut from plywood. The surface offset of both halves should be set to the material thickness of your plastic sheets. 
See the attached .step file for an example mould.