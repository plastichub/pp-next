### Extrusion time - Make the base 

 Get your recycled plastic ready and load it into the extrusion machine.

Wait for the plastic filament to come out and start overlaying multiple plastic strings. First we build a solid base for the lamp: We overlap 9 lines of plastic. Make sure each line melts into the previous one so they stick well onto each other.
