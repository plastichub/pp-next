### Apply the mixture 

 Once the glue is ready and before applying it, make sure to clean the surfaces you are going to glue together. Use a non-aggressive product like IPA (Isopropyl Alcohol) cleaner and a Scotch Brite (or similar) to remove any dirt or release agent left from the mould. 

Use a stick or an old brush to spread the adhesive on the surfaces and clamp the parts together. 

Leave them curating for around 24h, at ambient temperature (around 15º-20º) to be sure that the parts are completely glued.