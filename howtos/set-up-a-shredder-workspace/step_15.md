### Sell 

 You can now produce shredded plastic, in many colours and sizes. Now it's crucial to find people and workspaces that want to buy your shredded plastic. 

Put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. 
And then you can get creative on how you can sell your shreds locally to other workspaces. 

👉 bazar.preciousplastic.com