### Unload and demould the sheet 

 Once the cooling time has completed, open the cooling press and slide the mould onto the prep table. 

Lift the top mould sheet off the mould and place it to one side. It should lift off without any resistance but if it's stuck, you may need to use a bit of force. Try not to bend it! If you lever it, make sure to use something that won't scratch the surface of the mould sheet (like a plastic beam or the overflow from a previous sheet). 

Gently try and remove the sheet from the bottom plate. If it has not stuck, it should slide freely on the mould sheet. If it has, use the same method as above to gently remove it. If it is stuck inside the frame you can leave it inside until full cooled. 