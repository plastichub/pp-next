### Disinfect 

 Use Disinfectant and wipe over the inner side of the shield to kill off any bacteria. PRUSA has made and is constantly updating a Sterilisation Guide. Be sure to check it out for more information.