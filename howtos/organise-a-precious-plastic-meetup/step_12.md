### Network brainstorm 

 After finding out about the people and potential within your group, also brainstorm about more people, projects, companies, places etc. which might be useful for your local recycling network. This can be collection points or services, machine builders, recycling places, designers, shops, media etc.

In best case, directly add new contacts to your contact list, so they don’t get lost.
