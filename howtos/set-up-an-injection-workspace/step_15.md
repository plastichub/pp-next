### Build your workspace 

 Super, you’ve got your machines! But machines alone are not enough. 

Follow our tutorials on how to fully setup your Injection Workspace with all the additional tools, furniture and posters needed to make your space ready. 

👉 tiny.cc/space-injection
