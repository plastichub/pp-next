### Talk about Precious Plastic 

 Now make sure everyone knows and understands Precious Plastic.
You can use the presentation from the download files and change it for your context, or you create your own.

We also recommend showing the 1 min Starter Kit showcase videos to help understand the machines and possibilities.

👉 tiny.cc/showcase-collection
👉 tiny.cc/showcase-shredder
👉 tiny.cc/showcase-sheetpress
👉 tiny.cc/showcase-extrusion
👉 tiny.cc/showcase-injection
👉 tiny.cc/showcase-mix

Adjust the length and level of detail of your presentation to the level of knowledge and involvement of your group, so that newcomers get an understanding, and experienced recyclers don’t get too bored.
