### What is Wasteless Arugam Bay (WLAB) 

 We started WLAB in 2018 after we secured part of the financing through a USAID grant. We offer resource management, plastic collection, educational programs in schools and transform plastic waste into products. You know, the Precious Plastic jam! In total we are 3 full time employees.