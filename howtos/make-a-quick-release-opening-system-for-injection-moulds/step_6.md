### Quick release system for the injection machine 

 To make the process even faster, you can also add a quick release system for the machine!

Learn how to make the quick release system:
👉 tiny.cc/quickrelease-extruder

Happy injecting! :)