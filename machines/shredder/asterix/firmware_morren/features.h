#ifndef FEATURES_H
#define FEATURES_H


#ifdef HAS_STATUS
  #include "addons/Status.h"
#endif


#if defined(HOPPER_DOOR_PIN) || defined(HOPPER_PIN)
#include "hopper.h"
#define HAS_HOPPER
#endif

#ifdef MOTOR_LOAD_PIN
  #include "addons/MotorLoad.h"
#endif


#if defined(OP_MODE_1_PIN)
  #include "addons/OperationModeSwitch.h"
  #define HAS_OP_MODE_SWITCH
#endif

#if defined(FWD_PIN) && defined(REV_PIN)
  #include "addons/DirectionSwitch.h"
  #define HAS_DIRECTION_SWITCH
#endif

#ifdef HAS_AUTOREVERSE
  #include "../Auto-Reverse.h"
#endif

#if defined(FWD_PIN) && defined(REV_PIN)
  #include "VFD.h"
  #define HAS_VFD
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Plastic Hub - Addons
//

// Plastic Hub Studio  - remote control
#ifdef USE_FIRMATA
#include "firmata_link.h"
#endif

#ifdef HAS_PLUNGER
  #include "Plunger.h"
#endif

#endif