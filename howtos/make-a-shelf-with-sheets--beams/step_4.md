### Assembling 

 With the five 12 mm holes made on the four beams, you can insert the metal pins. 

Start with the two back beams on the floor, for assembling the first shelf (bottom one) and tighten the screws from the shelf into the metal pins. 

Repeat this process with the five shelves.

After connecting the five shelves to the two back beams, you can insert the two front beams, and tighten the screws from the shelves into the metal pins.