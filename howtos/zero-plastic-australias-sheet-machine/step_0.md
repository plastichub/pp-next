### Sheet Press Machine and molds 

 First you will need to buy yourself a Heat Press machine, i have found that these can come in many different sizes however this "how-to" will be for the 38cm2 Machine which can be purchased via the link - https://www.ebay.com.au/itm/Brand-New-38x38-High-Pressure-Heat-Press-Machine-T-shirt-Transfer-/282662703440

You will also need:
2x Aluminium Sheets (40cm2 and 3mm-5mm Thick)
1x 30cm x 25cm x 1cm Aluminium Mold (internal measurements)

You can change the size of this, however a 33cm internal size sheet should be the max you want to make with this machine, as any larger and the edges will not melt. 

