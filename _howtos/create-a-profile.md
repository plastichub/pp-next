---
image: "/howtos/create-a-profile/7-pin-on-map.jpg"
title: "Create a profile"
tagline: ""
description: "This is how you create a profile on this platform. It&#39;s pretty straightforward. As being one if the first users here we want to give you some guidance :)<br /><br />With the current status of development, we recommend doing this on a computer, not on a phone."
keywords: "research"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "research"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Create an account.</h4>
<div class="step-text">
  Go to the right top and join the platform. Pick the username of your workspace.<br />
  And verify your mail. If you don&#39;t receive an email, try out without it, it might work anyway.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-profile/1.0-create-account.jpg">
        <img class="step-image" src="/howtos/create-a-profile/1.0-create-account.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-a-profile/1.1-create-account.jpg">
        <img class="step-image" src="/howtos/create-a-profile/1.1-create-account.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Change your settings</h4>
<div class="step-text">
  Go to the top right corner to find your settings.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-profile/2-settings.jpg">
        <img class="step-image" src="/howtos/create-a-profile/2-settings.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Choose your focus</h4>
<div class="step-text">
  Choose your focus. Most likely you are a workspace or machine shop (since all the other ones are new for the platform). It might be hard to choose. Here you can find some more information on how to choose.<br />
  <br />
  Info: Currently it’s not possible to select multiple focus activities - if you are active in more areas (collection + shredding), create one account for each.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-profile/3-choose-profile.jpg">
        <img class="step-image" src="/howtos/create-a-profile/3-choose-profile.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Define what kind of workspace</h4>
<div class="step-text">
  For the launch of V4 we are going to have more focussed workspaces. Here you can choose what you are. If you are not sure or use multiple machines, you are probably a mixed.<br />
  <br />
  Info: You will always be able to change this choice in the future, in case you change your focus.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-profile/4-choose-workspace.jpg">
        <img class="step-image" src="/howtos/create-a-profile/4-choose-workspace.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Fill in your information</h4>
<div class="step-text">
  This is the information which will be shown on your profile.<br />
  <br />
  Add a photo: of your machines, workspace or products. Please don&#39;t only use your logo, it&#39;s much more useful and more fun to see your machines.<br />
  <br />
  And write a little text about your project: Write about which machines you&#39;re running and which activities you&#39;re doing. The more complete you make this, the better!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-profile/5-info.jpg">
        <img class="step-image" src="/howtos/create-a-profile/5-info.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Your map pin</h4>
<div class="step-text">
  This is the part where you add your pin to the map.<br />
  We will use your image from the step above. You only need to add a short text to give other people a quick understanding of what you’re doing.<br />
  <br />
  Info: Currently there is still a bug in the map that doesn&#39;t show your exact location but 2 blocks away (working on it!)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-profile/6-map-pin.jpg">
        <img class="step-image" src="/howtos/create-a-profile/6-map-pin.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Publish and give feedback</h4>
<div class="step-text">
  Have a look. Edit your profile if you want to make it look better. <br />
  If you had any difficulties with this process, send us your feedback (bottom right corner), so we can try to improve it :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-a-profile/8-profile.jpg">
        <img class="step-image" src="/howtos/create-a-profile/8-profile.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-a-profile/7-pin-on-map.jpg">
        <img class="step-image" src="/howtos/create-a-profile/7-pin-on-map.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-a-profile/Screenshot 2019-11-11 at 10.41.15.png">
        <img class="step-image" src="/howtos/create-a-profile/Screenshot 2019-11-11 at 10.41.15.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>