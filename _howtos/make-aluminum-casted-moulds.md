---
image: "/howtos/make-aluminum-casted-moulds/wallgrips_01.1.jpg"
title: "Make aluminum casted moulds"
tagline: ""
description: "Johe will show you the aluminum casting techniques to make moulds and make recycled plastic wall grips.Casting your own moulds from aluminum is quite a labour intensive process but the materials and resources needed are quite basic and not expensive."
keywords: "injection,PP,product,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "PP"
- "product"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather everything you need</h4>
<div class="step-text">
  Before you start get all your gear ready:<br />
  - Respirator mask<br />
  - Plastic (PP or PS)<br />
  - Aaluminium waste<br />
  - Rrocket stove<br />
  - Charcoal<br />
  - Cclay<br />
  - Foundry sand<br />
  - Plaster<br />
  - Sanding paper<br />
  - Drill <br />
  - Injection machine<br />
  - Plastic Type Stamp
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-aluminum-casted-moulds/wallgrips_02.1.jpg">
        <img class="step-image" src="/howtos/make-aluminum-casted-moulds/wallgrips_02.1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Watch video tutorial</h4>
<div class="step-text">
  Watch this video tutorial to learn how to make aluminum casted moulds to make organic products and recreate real objects.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Done & Share</h4>
<div class="step-text">
  After watching this video you should understand the basics of aluminium casting. You can reproduce the wall grips or try to make other products. We’re looking forward to seeing what you can come up with. Make sure to share back your new creations so the community can learn from you. Tag #preciousplastic on social media, create a new how-to or send us an email.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-aluminum-casted-moulds/wallgrips_03.1.jpg">
        <img class="step-image" src="/howtos/make-aluminum-casted-moulds/wallgrips_03.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-aluminum-casted-moulds/wallgrips_04.1.jpg">
        <img class="step-image" src="/howtos/make-aluminum-casted-moulds/wallgrips_04.1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-aluminum-casted-moulds/wallgrips_05.1.jpg">
        <img class="step-image" src="/howtos/make-aluminum-casted-moulds/wallgrips_05.1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>