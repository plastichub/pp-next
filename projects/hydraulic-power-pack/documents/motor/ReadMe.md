## Hydraulic Power Pack

# Motor
2.2kW (3HP) 230V/400V 3ph 4 Pole AC Motor 182TC B14A Flange (56 C face)

![3HP Motor 56c](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/motor/3hp-motor-56c.jpg)

### Specifications:

- Frame Size: 182TC

- Enclosure: TEFC

- Flange: B14A (56C)

- No-load Current: 4.6 A

- Mounting:  
  Flange - NEMA 56 C-FACE (for bell housing)
  or B3 Foot Mounting (for external pump)  

- Power Output: 3 HP (2.2kW)

- Voltage:  - 208-230V, 460V 3 phase

- Frequency: - 60Hz

- Full Load Current: 8.5A

- No-load Current: 4.6 A

- Starting Current: 65.4 A

- Speed: - 1750 RPM

- Full load Torque: 8.8 LB-FT = 11.9312N/m

- Service Factor 1.15

- Weight: 49lb 22kg

Example: [Baldor VM3611T](https://www.baldor.com/catalog/VM3611T)


Continental ref 030T Part No 601351

[Further Spec including performance graph](https://www.baldorvip.com/servlet/productInfoPacket?nameplate=X&layoutdiagram=X&electrical=X&connectiondiagram=X&language=E&matnr=VEM3561&vkorg=bec)
