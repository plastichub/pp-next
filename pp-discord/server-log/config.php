<?php

function defaultSIConfig()
{
    $ajax_url = admin_url('admin-ajax.php') . '?action=' . 'player-rpc';
    return array(
        'enabled' => false,
        'enabledMobile' => false,
        'router' => $ajax_url,
        'root' => get_home_url(),
        'filter' => array(
            'formFields' => 'all',
            'fields' => '',
            'content' => '#content',
            'attributes' => '',
            'ip' => false,
            'location' => false,
        ),

    );
}
function getSIConfig()
{
    $siConfig = get_option('siConfig');
    if (!$siConfig) {
        $siConfig = defaultSIConfig();
    } else {
        $siConfig = json_decode($siConfig, true);
    }
    return $siConfig;
}
