### Adding stoppers 

 Now take the two small pieces of metal strips (part E) and place them onto one of the ends of your quick release (either side works as long as they are both on the same sides). These pieces will act as a limiter ensuring the future extrusion hole lines up every time. Once aligned weld the stoppers to the angle bar and non-moveable plate. 

You may need to clean up the edges with an angle grinder after but this is optional.
