**Useful Links**

- [Shredder & Extruder - Industrial Electronics ](https://davehakkens.nl/community/forums/topic/shredder-and-extrusion-industrial-electronics-2/)
- [Extrusion Beam Tests](https://davehakkens.nl/community/forums/topic/bean-extrusion-optimization/#post-131338)
- [V4 Beam Production](https://davehakkens.nl/community/forums/topic/beam-production-v4/)
- [V4 Extrusion Products](https://davehakkens.nl/community/forums/topic/extrusion-machine-products-v4/)
- [V4 Tubes & Profiles](https://davehakkens.nl/community/forums/topic/v4-extrusion-tubes-and-profiles/)
- [V4 Extrusion Moulds](https://davehakkens.nl/community/forums/topic/extrusion-moulds-v4/)
- [How-to: Beam Mould](https://community.preciousplastic.com/how-to/make-a-mould-to-extrude-beams)
- [How-to: Flat Nozzle](https://community.preciousplastic.com/how-to/make-a-flat-nozzle-for-the-extrusion-machine)
- [How-to: Different Textures](https://community.preciousplastic.com/how-to/extrude-different-textures)
- [How-to: Bench](https://community.preciousplastic.com/how-to/make-a-bench-with-beams)
- [How-to: Shelf](https://community.preciousplastic.com/how-to/build-a-shelving-system)
- [How-to: Glass Beams](https://community.preciousplastic.com/how-to/make-glasslike-beams)
