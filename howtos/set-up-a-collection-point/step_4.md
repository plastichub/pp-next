### Check out your area 

 Get an overview of who and what is already existing in your area. Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. 

👉 community.preciousplastic.com/map