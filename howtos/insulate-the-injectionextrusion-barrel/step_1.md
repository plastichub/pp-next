### Cut the mesh 

 To begin, calculate the circumference you want your insulation cover to wrap around.

Remember to add a bit extra for the tabs and then cut your mesh with a grinder or tin snips.
