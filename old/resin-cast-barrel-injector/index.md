---
image: /_howto/resin-cast-barrel-injector/z1000000.jpg
category: "injection"
title: "Resin Cast-Barrel-Injector"
tagline: ""
description: ""

---





# How To: Resin Cast Barrel Injector

### You will need:

- 3 empty cans
- [High temperature 2-part casting silicone](./s-l1600.jpg)
- plunger- aluminium bar around 40mm diameter, minimum length the same as 1 can
- drill press
- something to make a 'nozzle'- short cylinder eg. half a wine cork
- the object you want to reproduce
- oven
- PPE


### Method:

1. Cast your barrel

Fix your nozzle piece to the end of your plunger, balance it upright in a can, and cast around it with high temp 2 part silicone

![Plunger in can](./IMG-20200420-WA0021.jpg)

![Casting the barrel](./IMG-20200420-WA0022.jpg)

When the casting is solid, carefully peel off the metal can (Be careful, it will cut you easily)

![Peel off can](./IMG-20200420-WA0020.jpg)

![Peeled](./IMG-20200420-WA0019.jpg)

Remove the plunger bar from the silicone barrel- a vise can help

![Remove plunger](./IMG-20200420-WA0017.jpg)

Push out your nozzle piece to create the nozzle hole

![Nozzle piece in](./IMG-20200420-WA0018.jpg)

![Nozzle piece out](./IMG-20200420-WA0015.jpg)

Trim the end of the barrel to create a flat surface

![Trim barrel end](./IMG-20200420-WA0014.jpg)

2. Create your mould

In a similar way, create your mould in a second can. The mould needs to be cut with a razorblade to remove the original part:

![mould cut](./z1000000.jpg)

3. Your plunger, barrel and mould are ready to go!

![ready to go 1](./IMG-20200420-WA0013.jpg)

![ready to go 2](./IMG-20200420-WA0012.jpg)

Place the mould in an empty can to hold it together.

Fill the barrel with shredded plastic (HDPE), place on a heatproof tray and heat in an oven until melted.

Then (using gloves) place the hot barrel on top of the mould in the drill press.

Plunge the plunger!

![Plunger up](./IMG-20200420-WA0009.jpg)

![Plunger down](./IMG-20200420-WA0007.jpg)

After cooling you can remove your injected part from the mould

![Remove part](./IMG-20200420-WA0005.jpg)

![Finished part](./IMG-20200420-WA0000.jpg)

To check your process, cut open your part and examine for voids

![Check for voids](./IMG-20200420-WA0001.jpg)

Be safe and enjoy! :)





{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

