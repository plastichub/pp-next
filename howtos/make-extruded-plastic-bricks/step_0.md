### Download templates to send for fabrication 

 Download the laser cut kit. Then send for fabrication at your local laser cutting service. Note there are three bricks, one full brick and one two-third brick and a one-third brick. All of these are required to build a complete wall. 

Before sending to downloaded dwg files to your metal laser cutting service ensure they understand that your drawings are in MM.

Each brick has tolerances included, but may be specific to your supplier. Before ordering, check that the tolerances conform. In this brick, we use a tolerance of 0.5mm. 

