### Done 

 If the welding has been done correctly both pieces should be fused together with the added material. A good way to see the result is by cutting through the piece and checking the consistency on the welded area.