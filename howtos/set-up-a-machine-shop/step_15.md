### Build partnerships 

 The Machine Shop needs to procure many machined parts, laser-cut pieces and components. 

For this, the project manager needs to set up connections with the local industrial network to find partners that can provide reliable orders at acceptable prices. Ordering regularly or in bulk will help to maintain the prices low. 

If parts are not available locally at reasonable prices, make sure to check the Precious Plastic Bazar: Some other Machine Shop might specialise into specific components and sell them, saving you precious time and money.