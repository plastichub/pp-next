### Sunglasses model - Hinges 

 Plastic hinges: Made in the mould. This is the most sustainable choice. No materials mixtures and durability that might go beyond eyewear with metal hinges, if designed properly. The downside is the quality perception, a key point when developing products that need to sell, and a parameter that many times has no connection to actual quality, but rather with the different perceptions around materials and products present in each corner of the world.
