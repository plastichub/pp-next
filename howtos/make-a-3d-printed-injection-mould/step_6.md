### Let cool 

 After the plastic has been injected, remove the mold and let it cool before opening. Cooling times vary depending on the thickness of the object. For something like the Daily Spork, a minute or two should do the trick.