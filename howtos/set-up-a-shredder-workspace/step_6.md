### Find out how to get your plastic 

 Reach out to nearby Collection Points that should be able to provide you with the needed plastic. Or find other clever ways to collect plastic in your area. Also, your local Community Point should be able to give you a hand on that.