### Different material, different finishing 

 First of all, the finishing potentials and techniques depend a lot on the material.
Hard materials like PS are easy to polish and it’s just a matter of repeating the process with higher grain sanding paper. For softer materials like PP or HDPE this process makes it more matt instead.

If you really require a shiny result then the best choice is to work with PS.