### Framing & covering the brick teeth (sides) 

 Now that the man structure is clamped we need to add some framing. Here we are using 18mm by 100mm planks, screwed into our frame. Pilot holes were drilled first to avoid cracking. 