---
image: "/howtos/set-up-a-shredder-workspace/cover-shredder-3.jpg"
title: "Set up a Shredder Workspace"
tagline: ""
description: "In this How-to we’re going to guide you through all the steps to set up a Shredder Workspace. Learn about plastic, how to find a space, get the shredder, find customers and connect to the Precious Plastic Universe. <br /><br />Download files:<br /> &lt;a href=&quot;https://cutt.ly/starterkit-shredder&quot;&gt;https://cutt.ly/starterkit-shredder&lt;/a&gt; <br /><br />Step 1-3: Intro<br />Step 4-8: Learn<br />Step 9-11: Set up<br />Step 12-17: Run<br />Step 18-20: Share"
keywords: "starterkit,shredder"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "starterkit"
- "shredder"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Role in the Precious Plastic Universe</h4>
<div class="step-text">
  First of all, make sure you had a look at the showcase page of this Starterkit!<br />
  👉 preciousplastic.com/starterkits/showcase/shredder <br />
  <br />
  Now about your Role:<br />
  <br />
  Shredder Workspaces are the backbone of the Precious Plastic Universe. <br />
  <br />
  They shred all the plastic gathered from the Collection Points and provide the raw material for all the other workspaces who transform it into new materials and objects.<br />
  <br />
  Shredder Workspaces are also responsible for maintaining high quality and pure material for the entire local network.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/universe-shredder.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/universe-shredder.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Outcomes</h4>
<div class="step-text">
  The outcome of a Shredder Workspace is shredded plastic.<br />
  <br />
  Shredded plastic can be of various sizes (small, medium, large) and colours. Single colour shredded plastic is higher in value and people can pay more for that.<br />
  <br />
  The shredded plastic can then be bought from other Precious Plastic workspaces.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image17.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image17.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Is this for you?</h4>
<div class="step-text">
  For the Shredder Workspace you will have to be quite technical as you have to understand how the shredder machine works, ideally know how to maintain it and, as a plus, know how to fix it when it needs a bit of extra love. <br />
  <br />
  Shredder workspaces also need to connect to as many Collection Points as possible to ensure a constant flow of plastic waste so you also kind of need to like to deal with people. <br />
  <br />
  And last but not least, a certain degree of organisation is also always good to have.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image8.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image8.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Learn about Precious Plastic</h4>
<div class="step-text">
  As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions.<br />
  <br />
  If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.<br />
  👉 preciousplastic.com<br />
  👉 community.preciousplastic.com<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.<br />
  👉 community.preciousplastic.com/academy/universe
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/learn-precious-plastic-universe.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/learn-precious-plastic-universe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Check out your area</h4>
<div class="step-text">
  Get an overview of who and what is already existing in your area.<br />
  <br />
  Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. <br />
  <br />
  Make sure not to jam the local network, if there are already five shredder places around you, have a chat with them how to collaborate or maybe consider starting another type of workspace.<br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/communitypoint-step4-map-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Find a Community Point near you</h4>
<div class="step-text">
  Community Points are the glue of the Precious Plastic Universe. They know the ins and outs of your local network. They can help you in multiple ways but they generally have a very in-depth overview of local Precious Plastic spaces, people, useful shops, resources and can help you with planning out your project. You can find them on the map here. <br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image21.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image21.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Find out how to get your plastic</h4>
<div class="step-text">
  Reach out to nearby Collection Points that should be able to provide you with the needed plastic. Or find other clever ways to collect plastic in your area. Also, your local Community Point should be able to give you a hand on that.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image10.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image10.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Get on Discord</h4>
<div class="step-text">
  Day to day discussions in the Precious Plastic Universe happen on Discord.<br />
  <br />
  Introduce yourself, say hi in your country channel and start to discover the different channels where people go deep into specific topics (building, collection, design etc..)<br />
  <br />
  Join the Discord:<br />
  👉 discordapp.com/invite/rnx7m4t
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image18.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Find a space</h4>
<div class="step-text">
  To help you find the perfect place for your workspace in the Download Kit you will find a floor plan with all the minimum requirements and a little cardboard tool to place your machines and tools in the workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/space.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Get your shredder</h4>
<div class="step-text">
  Cool, now you have a space it’s time to get hold of your Shredder Pro. There are three ways to do that:<br />
  <br />
  1 Build it yourself following our tutorials<br />
  👉 tiny.cc/build-shredderpro<br />
  <br />
  2 Buy it on the Bazar.<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  3 Find a Machine Shop near you on the map that can build it for you.<br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image13.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image13.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Set up your space</h4>
<div class="step-text">
  Super, you’ve got your Shredder Pro! But a shredder alone is not enough.<br />
  <br />
  Follow our tutorials on how to fully set up your shredder workspace with all the additional tools, furniture and posters needed to make your space ready. <br />
  <br />
  👉 community.preciousplastic.com/academy/spaces/shredder
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/setup-space.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/setup-space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Safety</h4>
<div class="step-text">
  Always stay safe. Of course, don’t put your fingers or hands in the blades.<br />
  <br />
  Find out more tips on how to stay safe in the Academy:<br />
  👉 tiny.cc/plastic-safety<br />
  <br />
  It is also good to check out our safety videos on plastic here.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/Safety 1.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/Safety 1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Maintenance</h4>
<div class="step-text">
  As you run your Shredder Workspace it is crucial that you maintain the shredder machine in order to prevent failures. Find out here how to best maintain the shredder.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image19.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image19.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Sort by colour</h4>
<div class="step-text">
  When possible you should try to sort and shred the same plastic type by colour. This increases its value and gives more freedom to people making beautiful products from it.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/SP 8 Boxes Birds Eye.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/SP 8 Boxes Birds Eye.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Shreds size</h4>
<div class="step-text">
  Precious Plastic shredded plastic comes in three flake sizes: small, medium and large. Each size has a proper application (for example sheetpress can be ok with big flakes while extrusion needs small).<br />
  <br />
  You can create different sizes by using a sieve or shredding multiple times. More info here.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/shred-size.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/shred-size.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Sell</h4>
<div class="step-text">
  You can now produce shredded plastic, in many colours and sizes. Now it&#39;s crucial to find people and workspaces that want to buy your shredded plastic. <br />
  <br />
  Put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. <br />
  And then you can get creative on how you can sell your shreds locally to other workspaces. <br />
  <br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image18.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Create a profile</h4>
<div class="step-text">
  If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform to connect with more people. <br />
  <br />
  Follow this link and sign up with your email, pick your role, put your pin on the map and upload nice pics to show the world what you’re doing. <br />
  <br />
  👉 community.preciousplastic.com/sign-up
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image20.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image20.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Create How-to's!</h4>
<div class="step-text">
  Share to the world how you run your Shredder Workspace so other people can learn from you and start using your solution to tackle the plastic problem. <br />
  <br />
  Make sure to only create How-to&#39;s for your best processes and techniques, not try outs or one offs. This can also help you create a name for yourself in the Precious Plastic community. <br />
  <br />
  👉 community.preciousplastic.com/how-to
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/image4.png">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/image4.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Learn and improve together</h4>
<div class="step-text">
  Precious Plastic is nothing without the people. Working together and helping each other. Participate in the community, share back and make use of it, it can really pay off!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/howto-collection-discord.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/howto-collection-discord.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Good things take time</h4>
<div class="step-text">
  Starting off will take some time in the beginning. It’s normal. Be patience, work smart and reach out to your Precious Plastic community if you need help. Everything will take off. You’re changing the world.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/good-things.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/good-things.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Download and start!</h4>
<div class="step-text">
  Ready and excited to start?<br />
  You&#39;re a hero!<br />
  <br />
  In this download package you will find all the files you need to set up your Shredder Workspace.<br />
  Download and start your recycling journey!<br />
  <br />
  👉 <a href="https://cutt.ly/starterkit-shredder">https://cutt.ly/starterkit-shredder</a> 👈
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-shredder-workspace/Workspace Starter Kit.jpg">
        <img class="step-image" src="/howtos/set-up-a-shredder-workspace/Workspace Starter Kit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>