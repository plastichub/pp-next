<?php
/**
 * @version 1.6
 * @link http://www.xapp-studio.com
 * @author XApp-Studio.com support@xapp-studio.com
 * @license : GPL v2. http://www.gnu.org/licenses/gpl-2.0.html
 */
include_once 'xcom_LifeCycle.php';

class xcom_Plugin extends xcom_LifeCycle
{

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return array of option meta data.
     */
    public function getOptionMetaData()
    {
        return array(
            
        );
    }

//    protected function getOptionValueI18nString($optionValue) {
    //        $i18nValue = parent::getOptionValueI18nString($optionValue);
    //        return $i18nValue;
    //    }

    protected function initOptions()
    {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=105
     * @return void
     */
    public function activate()
    {
        $this->installDatabaseTables();
    }

    public function getPluginDisplayName()
    {
        return 'Session Replay';
    }

    protected function getMainPluginFileName()
    {
        return 'xcom.php';
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables()
    {
        global $wpdb;
        $tableName = $this->prefixTableName('sessionreplay');
        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
            `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `session` varchar(255) NOT NULL DEFAULT '',
            `visit` varchar(255) NOT NULL DEFAULT '',
            `referer` text NOT NULL,
            `tags` longtext NOT NULL,
            `events` longtext NOT NULL,
            `status` text NOT NULL
          )");
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables()
    {
        global $wpdb;
        $tableName = $this->prefixTableName('sessionreplay');
        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }

    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function upgrade()
    {
    }

    public function addActionsAndFilters()
    {

        // Add options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        /*add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));*/

        //$pluginDir = dirname(plugin_basename(__FILE__));

        if (!class_exists('ReduxFramework') && file_exists(dirname(__FILE__) . '/ReduxFramework/ReduxCore/framework.php')) {
            require_once dirname(__FILE__) . '/ReduxFramework/ReduxCore/framework.php';
        } else {

        }

        if (!isset($redux_demo) && file_exists(dirname(__FILE__) . '/ReduxFramework/sample/sample-config.php')) {
            require_once dirname(__FILE__) . '/ReduxFramework/sample/sample-config.php';
        }

    }
}
