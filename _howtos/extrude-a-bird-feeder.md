---
image: "/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.08 (1).jpeg"
title: "Extrude a Bird Feeder"
tagline: ""
description: "Bird feeder made with recycled plastic and Precious Plastic extruder.<br /><br />You can visit our blog and watch the video:<br />&lt;a href=&quot;https://menorplastic.com/disenamos-el-mejor-comedero-para-pajaros-con-plastico-reciclado/&quot;&gt;https://menorplastic.com/disenamos-el-mejor-comedero-para-pajaros-con-plastico-reciclado/&lt;/a&gt;"
keywords: "extrusion,product,PP,HDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "product"
- "PP"
- "HDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/menor-plastic">menor-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Select plastics</h4>
<div class="step-text">
  Crush the plastic that we are going to use (preferably HDPE) and fill extruder.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.09 (4).jpeg">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.09 (4).jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.09 (3).jpeg">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.09 (3).jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Pick some cardboard</h4>
<div class="step-text">
  Use cardboard of toilet paper or similar. Put some cloth inside.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.09 (2).jpeg">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.09 (2).jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Rolling plastic</h4>
<div class="step-text">
  We will slowly roll the plastic on the cardboard. Change direction to make it resistant.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/enrollando.png">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/enrollando.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Demolding</h4>
<div class="step-text">
  We let it cool and take out the cardboard slowly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/enfriando.png">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/enfriando.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/estructura.png">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/estructura.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Feed it</h4>
<div class="step-text">
  Done! You have a bird feeder ready to use.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/final-individual.png">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/final-individual.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.08.jpeg">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/WhatsApp Image 2020-04-04 at 16.50.08.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/extrude-a-bird-feeder/20200403_115558A.jpg">
        <img class="step-image" src="/howtos/extrude-a-bird-feeder/20200403_115558A.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>