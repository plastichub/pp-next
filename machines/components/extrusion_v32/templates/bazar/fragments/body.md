Industrial grade shredder and/or extrusion controller, works with v3 and v4. This system adds max. possible safety.

This controller uses an industrial Arduino based controller as well a DIN rail current sensor
to detect jamming. Please check the wiki section for the source code and wiring details.

This system was 8 months in development and is mostly used in our products with educational contexts.

<div>
    <a href="${product_overview}">
        <p style="text-align: center">
            <img width="100%" src="${product_overview}"/>
        </p>
    </a>
</div>

### Features

- Very precise auto-reverse
- Status lights : Ok & Error
- Emergency and reset button
- Uses contactor for the power circuit
- Extra motor protection (thermal & magnetic).
- Stops automatically after one hour of inactivity.
- High quality cables and wiring according to industrial standards.
- Won’t start when power is back after powerloss
- Incl. a VFD which enables to 3phase motors on single phase. Please let us know which voltage and power you'd like to use.
- Optional on request: Auto-Shredd (hopper filled sensor)
- Optional on request: Auto-Suspend & Resume (shut's down entire power circuit)
- All components have warrenty and are CE certified
- Modular open source firmware being also used in other products

<div>    
    <div style="padding: 16px; display: inline-block">
        ${product_resources}
    </div>
</div>
