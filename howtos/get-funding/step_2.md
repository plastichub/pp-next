### Fill out the Calculator 

 This financial forecasting tool isn’t an exact science but gives you a starting point for how much money you need to start, how many products and services you need to sell per month in order to be profitable, and how long it’s going to take to pay back your initial investment. It works with any kind of business-related to Precious Plastic! 

You can find the Workspace Calculator in the Download Kit or learn more in the Academy:
👉 http://tiny.cc/workspace-calculator