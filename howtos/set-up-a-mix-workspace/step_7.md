### The basics of plastic 

 Before you start it is crucial to get a solid understanding of plastic. How it works, the different types, melting temperatures and so on. Head over to our Academy and dive into the plastic chapters to learn about the different types and properties etc.

There is also more about the machines, products and techniques, and more. Make sure to absorb as much knowledge from there as possible. 

👉 http://tiny.cc/basics-about-plastic