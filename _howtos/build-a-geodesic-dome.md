---
image: "/howtos/build-a-geodesic-dome/00_cover .jpg"
title: "Build a geodesic dome"
tagline: ""
description: "The Geodesic Dome is a lightweight structure using a minimum of materials. It is constructed by triangular elements, which are structurally rigid and splitting the stress evenly. Usually, it is used for greenhouses or temporary structures.<br /><br />Step 1-6: Prepare your components (1-2 days)<br />Step 7-11: Assemble the structure (1-2 hours)"
keywords: "product,HDPE,PP,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "HDPE"
- "PP"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Design your structure</h4>
<div class="step-text">
  First of all, decide how big your dome should be and calculate the required sizes of the elements. Consider that the diameter of the structure equals double of its height.<br />
  <br />
  There are several tools on the internet to help calculating the size of each individual beam. This is the one we used: <br />
  <a href="http://www.domerama.com/calculators/2v-geodesic-dome-calculator/<br/>">http://www.domerama.com/calculators/2v-geodesic-dome-calculator/<br /></a>
  You can put in your dome size and it will calculate the required beam lengths for you. <br />
  <br />
  In any case, we have to shorten the lengths a bit so that they work with our joints.<br />
  Short beams: - 64.5mm <br />
  Long beams: - 73.4mm<br />
  <br />
  In this example, we are making a 2.5m high dome (5m diameter).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/01_Step 01-1 __.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/01_Step 01-1 __.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/01_Step 01-2 __.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/01_Step 01-2 __.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Make the moulds for your joints</h4>
<div class="step-text">
  To build a geodesic dome, we will need 6 pieces of 5-way joints and 20 pieces of 6-way joints. Those pieces will be injected, so we first have to make the required moulds for the injection machine. Take the 3D model from the download kit and cnc-mill it yourself or send it to a specialist to mill it for you.<br />
  <br />
  Then drill the holes for the injection point and for the bolts to close the mould.<br />
  <br />
  👉 tiny.cc/injection-moulds
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/02_Step 02-1__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/02_Step 02-1__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/02_Step 02-2__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/02_Step 02-2__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/02_Step02-3__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/02_Step02-3__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare your plastic</h4>
<div class="step-text">
  Make sure you have your material ready before starting to inject. For the joints, both HDPE and PP work for the dome due to their flexibility and strength. However, PP is recommended as it is easier to inject. PS and PET are not recommended as they are brittle. Consider that the weight of one joint is about 130g, so you will need a bit more than 3.5kg for all pieces. Use around 150g PP each time to prevent lack of pressure and loss from leak out.<br />
  <br />
  Tip: Try to use finely shredded plastic to reach the injection machine’s full capacity.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/03_Step 03 __.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/03_Step 03 __.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Inject the joints</h4>
<div class="step-text">
  Ready to inject! Set the temperature of the tube to 240°C and the nozzle to 230°C. The following workflow will take 12-15 mins per piece:<br />
  1 - Insert plastic to the hopper<br />
  2 - Heat up the plastic (8-9 min)<br />
  3 - Attach the mould <br />
  4 - Inject plastic<br />
  5 - Take the mould out of the machine<br />
  <br />
  Repeat this process for each joint. Ongoing, while you&#39;re waiting for one mould to cool down, you can prepare the other mould for the next injection process.<br />
  <br />
  If needed, here are some tips for injecting:<br />
  👉 tiny.cc/work-injection-machine
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/04_Step 04-1 __.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/04_Step 04-1 __.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/04_Step 04-2 __.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/04_Step 04-2 __.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/04_Step 04-3 __.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/04_Step 04-3 __.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Finish your joints</h4>
<div class="step-text">
  To finish the joinery, cut the extra material generated by the injection channel. Use a knife to clean the edges if needed. Then drill the M6 holes according to the marks on the joinery parts.<br />
  <br />
  P.S. Always keep the plastic flakes for future use during the process!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/05_Step 05-1 _2.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/05_Step 05-1 _2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/05_Step 05-2__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/05_Step 05-2__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Prepare the beams</h4>
<div class="step-text">
  Take your beams (we used 20mm x 30mm) and cut them to the required lengths and amounts.<br />
  For our 2.5 m high dome, we need:<br />
  A: 1302 mm x 30<br />
  B: 1472 mm x 35<br />
  If you decided for another dome size, take the dimensions you’ve calculated.<br />
  <br />
  Once you have all your beams cut into the right size, drill M6 holes at both ends of all the beams. Place the hole in the middle of the width and 20 mm from the edge.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/06_Step 06-1 _.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/06_Step 06-1 _.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/06_Step 06-2 __.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/06_Step 06-2 __.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Get ready for assembly</h4>
<div class="step-text">
  Now that you have all the components you can build your structure.<br />
  <br />
  Gather the following tools and metal parts for for assembling:<br />
  - 2x 10 mm spanner<br />
  - 130x Bolts - M6 x 40 mm<br />
  - 130x Nuts - M6<br />
  - 260x Washers<br />
  - Hand Drill with M6 Drill Bit
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/07_Step 07__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/07_Step 07__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Assemble (Concept)</h4>
<div class="step-text">
  Finally it&#39;s time to assemble! Basically, the dome is made out of 6 pentagons which are connected with their edges. The raised plate in the 6-way-joint is connecting to the 5-way-joint, the rest is connecting to another 6-way piece.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/08_Step 08__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/08_Step 08__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Assemble (Part 1)</h4>
<div class="step-text">
  There are various ways to assemble, so feel free to try out your own.<br />
  In this example, we&#39;ll start with the bottom level of the structure. Connect 10x 6-way-joints with 10x long beams into a decagon. Pay attention to keep all joints in the right direction.<br />
  <br />
  6-way-joints: pink <br />
  5-way-joints: blue
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/09_Step 09-1__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/09_Step 09-1__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/09_Step 09-2__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/09_Step 09-2__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/09_Step 09-3__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/09_Step 09-3__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Assemble (Part 2)</h4>
<div class="step-text">
  For the second level, connect 5x 5-way-joinery with 2x short beams and connect 5x 6-way-joints with 2x long beams to the first level. The 5-way-joints and 6-way-joints should be alternating. Then, connect all the joints on the second level with shorter beams.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/10_Step 10-1__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/10_Step 10-1__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/10_Step 10-2__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/10_Step 10-2__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/10_Step 10-3__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/10_Step 10-3__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Assemble (Part 3)</h4>
<div class="step-text">
  For the third level, connect 5x 6-way-joints to the second level. Use longer beams for connecting to a 6-way-joint and short beams for connecting to a 5-way.<br />
  <br />
  For the fourth and last level, simply connect the remaining 5-way-joint with the 5 short beams.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/11_Step 11-1__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/11_Step 11-1__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/11_Step 11-2__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/11_Step 11-2__.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/11_Step 11-3__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/11_Step 11-3__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Explore the possibilities!</h4>
<div class="step-text">
  Your Geodesic Dome is done!<br />
  <br />
  Feel free to play around with this structure like removing some beams to create an entrance, or trying out other variations of the construction.<br />
  <br />
  You could also use other materials (like recycled plastic) for your beams or add surfaces in the triangle spaces.<br />
  <br />
  Have fun exploring!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/13_Step 13-1__ copy.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/13_Step 13-1__ copy.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/build-a-geodesic-dome/13_Step 13-2__.jpg">
        <img class="step-image" src="/howtos/build-a-geodesic-dome/13_Step 13-2__.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>