### Preheat the barrel 

 To start the machine plug in the plug and switch on the power.

Do not try move the plunger as long the material melt temperature is not reached!

There are different types of PID controllers on the market, but the main functions seem to be similar over a wide range of available products. 
To change the temperature hold the button (Set) until one of the digits lights up. Switch between the digits with the button (Arrow Left) and then increase (Arrow Up) or decrease (Arrow Down) the temperature. Set the target temperature according to your material to the center of the temperature range. Check the starter kit for an overview of specific melt points of different polymer types.
