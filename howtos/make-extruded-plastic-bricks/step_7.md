### Smoothing the inside 

 Once cooled, grind away the welds ensuring the surface on the inside is completely smooth. Take care to not over grind the weld. Then sand the surface removing any finer imperfections. 