### Wash plastic: Pre-wash 


Plastic needs to be clean before shredding to make the maintenance of the machine easier. Here we are going to explain how to build an easy pre-washing machine to get rid of the rough dirt.

This machine integrates into a system with a sand filter and a washing machine.
