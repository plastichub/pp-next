### Backrest connector: Make the plugs 

 Up next, cut two connector plugs (part H) from 12mm solid steel, each 24mm long. Stick the plugs in the blind holes and mark the position of the locking pins with a drill.

Then take it off and move to the drill press. Choose a diameter slightly bigger than the locking pin and countersink the top side. Drill a M5 center hole (Ø4.2mm), 6mm from the other end of the plug and tap it.
