### Role 

 First of all, make sure you had a look at the showcase page of this Starterkit!
👉 preciousplastic.com/starterkits/showcase/extrusion

Now about your Role:

Extrusion workspaces buy recycled shredded plastic from Shredder Workspaces and transform it into recycled beams and bricks.

These colourful objects are then sold to design studios for further design or directly to customers.

Extrusion workspaces should also reach out to Community Point to connect with the local Precious Plastic community and, maybe, get help selling their materials.