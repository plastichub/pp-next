### Heating the Cartridge 

 Wait the time necessary depending on the sunlight exposure (between 30 and 60 min).

You can load several iron cartridge inside the solar concentrator to speed up the process.

Use a temp prob inside of the heated tube to manage the temperature of the cartridge.

The concentrator can heat up between 180-250°C.

There is a lot to learn with this method and a lot of inputs to bring on the table.