### Expanding the nozzle hole 

 Now that the nozzle is welded, we need to ensure the nozzle channel is smooth and uniform To do this we remove the sliding portion of the quick release leaving only the nozzle portion of the quick release. We then choose a drill bit that will take a fraction of a millimetre off the inside of the nozzle and make our original hole larger using a drill press.

This will ensure a clean passage for our plastic when we extrude and allow us to clean any blockages.
