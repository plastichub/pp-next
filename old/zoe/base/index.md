---
image: /_howto/zoe/base/IMG_2157.JPG
category: "zoe"
title: "Base"
tagline: ""
description: ""
tags :
 - v3
 - zoe
categories:
  - Shredder
usedin:
  - zoe
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2135.JPG" _target="_blank">
        <img id="IMG_2135.JPG" src="./IMG_2135.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2138.JPG" _target="_blank">
        <img id="IMG_2138.JPG" src="./IMG_2138.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2139.JPG" _target="_blank">
        <img id="IMG_2139.JPG" src="./IMG_2139.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2140.JPG" _target="_blank">
        <img id="IMG_2140.JPG" src="./IMG_2140.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2142.JPG" _target="_blank">
        <img id="IMG_2142.JPG" src="./IMG_2142.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2157.JPG" _target="_blank">
        <img id="IMG_2157.JPG" src="./IMG_2157.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

