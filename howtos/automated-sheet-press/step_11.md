### pressing: height sensor 

 There is a sensor attached to the upper plate measuring the distance to a reflector mounted on the lower plate. The casing is a sliced PVC tube to limit interference. This measurement is sent to the arduino and allows the machine to know the position of the plates, when to add air and when to release. The sensor we used is a Time Of Flight Sensor vl6180. 
