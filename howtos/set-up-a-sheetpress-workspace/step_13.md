### Find the space 

 To help you find and set up the space for your workspace, you can use the floorplan in the Download Kit. It includes all the minimum requirements and a little template to place your machines and tools in the workspace. 