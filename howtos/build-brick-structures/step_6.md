### Layout your base (Mobile unit) 

 In this Step, you are creating the bottom layer that will be connected to the top layer holding the bricks together. If you have bolted your structure to the ground you can skip this step, unless you want to create a layer of wood between the concrete and the first layer of bricks. (Which we did for our 2019 Dutch Design Week exhibit: see photo). 

This first layer of wood needs to be exactly the same length as your bricks on the first layer and screwed together to create a single unit. 
