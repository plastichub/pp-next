### Safety first! (and last) 

 Make sure to incorporate guards and covers so that fingers can't reach any moving parts at any points, minimizing the risks of injury