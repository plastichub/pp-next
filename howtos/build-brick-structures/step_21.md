### Make roof frame structure 

 The newly installed beam will help support the roof trusses. These trusses should overhang the walls, but this will depend on your structure and roofing type. For the bike shed you will need 3, each with a notch cut to ensure good contact is made with the existing timber. To do this we used a ruler to create the desired cut and then cut it with a jigsaw. The end of the truss is also cut at an angle, this is an optional step. 
