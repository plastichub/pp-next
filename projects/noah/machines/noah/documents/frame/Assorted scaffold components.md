Universal scaffold tube is 48.3mm diameter and are made from galvanized steel or aluminium alloy 4.2mm thick.

The Uk body for scaffolding is https://nasc.org.uk/ and their current guideline publication is the TG20:13. (£612.00!)
They offer a standalone Design guide and Operational guide (£68.50 each)
The TG9:18 gives a good overall guideline to desiging scaffold structures (see repository)

Alloy beams are built to Eurocode BS EN 1999-1-1 (see repository) which gives all engineering calcs.




The X axis rails and the gantry should be made from alloy beams (to reduce weight both in operation and in transit)


The tool head carriage will move on axles of single tubes.

Most of the connections are done with fixed 90 degree bolt-on couplings known as "doubles" or "singles" (doubles are better).

Angled bracing is done using rotating "swivel" couplings. Normally all couplings require a 21mm spanner or socket.

NOTE! Most importantly the tubes and beams can be joined end-to end without interupting the contact surface when used as a rail - using a "spigot" coupling.

This is what gives NOAH the infinate expandability.

ALLOY BEAMS (450MM DEEP)

Length Weight
4.1m 21.0kg
5.1m 26.2kg
6.1m 31.5kg
8.1m 42.0kg

ALLOY X BEAMS (750MM DEEP)

Length Weight
1.0m 8.0kg
2.0m 16.0kg
3.0m 24.0kg
4.0m 32.0kg
6.0m 48.0kg

STEEL LADDER BEAMS

Length Weight
2.4m (8ft) 28.9kg
3.0m (10ft) 36.1kg
3.9m (13ft) 47.0kg
4.8m (16ft) 57.9kg
5.4m (18ft) 65.1kg
6.4m (21ft) 76.0kg

STEEL UNIT BEAMS

Length Weight
1.8m (6ft) 23.0kg
2.7m (9ft) 34.0kg
3.6m (12ft) 43.0kg

SCAFFOLD BOARDS NEW (wood)

Length Weight
2.4m (8ft) 12.3kg
3.0m (10ft) 15.4kg
3.9m (13ft) 20.0kg

PLASTIC BOARDS

Length Weight
2.4m (8ft) 9.5kg
3.0m (10ft) 11.9kg
3.9m (13ft) 15.5kg

SCAFFOLD TUBE (single tube)

Length Weight
1.5m (5ft) 6.5kg
2.4m (8ft) 10.5kg
3.0m (10ft) 13.1kg
3.9m (13ft) 17.0kg
4.8m (16ft) 20.9kg
6.4m (21ft) 27.9kg

FITTINGS

1 x DOUBLE = 1.0kg
1 x SWIVEL = 1.1kg
1 x SINGLE = 0.6kg
1 x GRAVLOCK = 1.5kg
1 x SLEEVE = 1.0kg
1 x SPIGOT = 1.2kg
1 x BASEPLATE = 0.3KG
1 x BOARD CLIP = 0.7kg

