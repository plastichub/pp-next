### Make a welding stick 

 We will need a plastic stick to weld the pieces together. It’s very important that the welding material comes from the same source as your pieces, otherwise the weld might not be homogeneous.

The stick can be made in many ways, it can be extruded or it can be cut out of a sheet, like it is shown here. The important part is that it’s as consistent as possible and also fits inside the welding tool. Generally something between 2-4mm should work.