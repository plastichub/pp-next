### Pedal mold: cut the parts 

 Cut two squares 120 x 120 mm in the metal sheet.
Make holes in the four corners (4.3 mm in my case).
Scribe the pedal shape on one square face.
Drill the injection hole where you want it(I chose diameter 5, in the center).
Cut a band of 26 mm height and at least 300 length and another of 26 (precisely) x 340 (at least).
Cut the 300 band in 15.6 (2x); 24 (2x); 18 (2x); 33 (2x); 25 (2x); 43.4 (1x).
Cut the 340 band in 40 (2x); 56 (4x); 27.7 (1x).
Cut also six small pieces in order to wedge the mold part together (5 x 10 mm).
Cut three more to make the triangle and trapeze center (25 x 7 (2x); 4 x 35 (1x)).