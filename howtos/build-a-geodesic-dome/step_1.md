### Make the moulds for your joints 

 To build a geodesic dome, we will need 6 pieces of 5-way joints and 20 pieces of 6-way joints. Those pieces will be injected, so we first have to make the required moulds for the injection machine. Take the 3D model from the download kit and cnc-mill it yourself or send it to a specialist to mill it for you.

Then drill the holes for the injection point and for the bolts to close the mould.

👉 tiny.cc/injection-moulds