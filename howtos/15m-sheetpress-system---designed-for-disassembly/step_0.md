### Download 

 Here's a link to the build folder:
https://drive.google.com/drive/u/1/folders/19glmxFnX6oH5BvBAc8NxFk5f-9YG9HNb

If you're setting up a sheetpress workspace, you should still download our sheetpress workspace starterkit from our academy. Just use this build folder instead of the build folder in the main starterkit.

And here's a link to our original build video. The design is slightly different but the steps are the same:
https://www.youtube.com/watch?v=j3OctDe3xVk


