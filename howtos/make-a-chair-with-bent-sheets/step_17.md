### Backrest connector: Assemble the plugs 

 Before you put everything together, enlarge the previously drilled locking pin holes to a diameter about 0.2 mm smaller than the diameter of the locking pins. The hole should be narrow enough to hold the pin tightly, but not too tight, as PS easily cracks with too much tension. (You might have to do some test samples to find the right size.)

After drilling, mark the immersion depth of the drill bit on your locking pins to see how far you can hammer them in without cracking the material. Finally, stick the plugs into the blind holes, hammer the locking pins in and cut the excess off. Use a file to smoothen and align the ends to the plastic edge.
