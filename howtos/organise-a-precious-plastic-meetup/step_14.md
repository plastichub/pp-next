### Keep meeting 

 Once people know what to do and are excited to dive into it, meeting regularly can help a lot to keep them motivated, to stay updated and to keep strengthening the connections. Try to find a time, an interval and place which works for the team (or for at least one representing person of each working group).
If you have a fixed place, you could install a sign somewhere visually there.

And (if not there yet), add a Community Point pin on the Precious Plastic map, so people can find this pin and get in touch with your group.


👉 community.preciousplastic.com/signup