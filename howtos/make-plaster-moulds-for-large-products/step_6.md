### Heat your mould 

 Since this is a slow injection moulding process, you’ll need to make sure the inside of the mould stays hot the whole time. This can be done a number of ways - in this case, large holes were drilled to circulate hot air through the mould from two heat guns.