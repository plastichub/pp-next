### Stamp products fast. Without heating 


We will show you how you can stamp the products you made in a fast efficient way. The main benefit is that it doesn’t require heating so you have no heatup/cooldown times. This is good for bigger productions and just saves energy :)