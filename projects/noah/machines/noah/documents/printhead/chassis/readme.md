# Print head chassis

The chassis locates the extruder and allows mounting onto the Z-axis carriage. 

It takes the form of a mounting plate, with fixing points to secure it to the screw jack mechanism, and also to provide a means of locating a protective enclosure.

## ToDos

1. Determine dimensions of chosen extruder
