# Print head for Noah

Noah's print head consists of the following sub-assemblies:

- [Z - axis motion](./z-axis/readme.md)

- [Chassis](./chassis/readme.md)

- [Enclosure](./enclosure/readme.md)

- [Extruder](./extruder/readme.md)

- [Extruder transmisson](./extruder/transmission/readme.md)

- [Feed system](./feed-system/readme.md)
