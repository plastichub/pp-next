**Product Resources**

- [3D Preview](${product_3d})
- [Wiring](${product_wiring})
- [BOM - Bill of materials](${product_bom})

**Social resources and references of our v3 extruder**

- [Greenphenix](https://www.youtube.com/watch?v=iKEghQuUng0)
- [PP La Safour](https://www.facebook.com/plasticprecioslasafor/)

${product_howtos}
