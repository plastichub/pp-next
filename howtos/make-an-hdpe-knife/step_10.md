### Making HDPE Pins 

 We wanted the pins to match the blade, so we figured we would try and make out own out of some of the leftover HDPE. We cut a long, thin piece and then used a chisel to take off the corners. We then held this in our drill and used some sandpaper to turn this into a long dowel that we could then cut into pieces for our pins.

It was the first time we tried this and it worked great!