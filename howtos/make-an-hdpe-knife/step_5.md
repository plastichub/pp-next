### De-Mould and Apply Template 

 The sheet often pops right out of the mould but occasionally we need to gently persuade it with some pliers. Once the sheet is out we cut off any flashing (saving to reuse again of course!) and stick on the chosen template design.