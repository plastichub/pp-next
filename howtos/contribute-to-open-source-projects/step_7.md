### Wait for it to be Merged 

 Now hopefully it will be merged by the repository maintainers and you will have officially contributed to open source!

If you need more details do take a look at the video that this how-to is based off.

Check out the One Army GitHub page for repositories you can contribute to!

https://github.com/onearmy

If you need some wider context on other open source projects you can take a look at our website and our GitHub as well!

https://www.darigovresearch.com/

https://github.com/darigovresearch/
