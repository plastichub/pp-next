### Prepare your equipment 

 You are leaving your workshop, so you need everything to run a plastic recycling workshop mobile. 
As everyone has their own way of doing it, you get a list from Kunststoffschmiede (Germany) for your inspiration:

- Injection machine and the bicycle shredder
- Tools for maintenance 
- Productcounter
- Electric box (including: Power cable, distributer and some lights)
- Moulds
- Shredded plastic
- Clean and unshredded plastic
- Samples from us and the community to show the possibilities
- Informations like flyers and posters
- First Aid Kit
- Camera
- strap, tape and ropes
- Tub and cleaning stuff to wash the plastic
