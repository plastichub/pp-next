### Inspire with samples 

 You might also want to put together a little sample kit of product and material samples from the community, that can help you to explain and get people excited.

In best case you can gather some good products and samples from your local workspaces - if they are not ready yet, you can use the Bazar to find products from the global community.

👉 bazar.preciousplastic.com