### Make PS glue yourself 


PS dissolves in acetone creating a thick mixture that could be used as a gap filler or as a glue. In this case we will use D-Limonene, a natural solvent which creates strong chemical bonding on PS.