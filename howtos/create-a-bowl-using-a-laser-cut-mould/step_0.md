### Gather everything you need 

 Before you start get all your gear ready:

- Plastic (PP or PS)
- Lasercutted Metal
- Welding machine
- Sanding machine
- Sanding paper
- Compression machine
- Spray paint
- Knife
- Plastic Type Stamp

