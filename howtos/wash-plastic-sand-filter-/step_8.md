### Build the Frame 

 The frame provides a solid structure to bring the filter and the pump together and add some casters to make it mobile. To build the frame, cut your steel tubes to the required measurements and weld them together as shown in the picture. 

In addition to this, we use a metal cross to bolt the lid down. Carefully align the holes with the nuts, so the bolts will fit smoothly. One way to do this is to first build the cross, put the bolts in the holes, screw the nuts on the bolts, put the cross in place and weld the nuts to the frame.

When all is done, paint the frame frame thoroughly, to prevent it from rusting (as it will be wet frequently).