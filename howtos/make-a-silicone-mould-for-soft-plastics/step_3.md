### Sealing the Mould Box 

 Once you have the right area around the object we need to seal the base of the walls with clay so the silicone does not escape. 

Roll the clay into a ball and then into thin noodles. Press the noodles along the edges off your walls inside and out. The mould box should now be silicone tight!