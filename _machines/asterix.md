---
image: /machines/shredder/asterix//renderings/perspective.png
category: "shredder"
title: "Vending Shredder 'Asterix'"
product_rel: "/machines/shredder/asterix/"
tagline: ""
description: "Precious Plastic - Machine : Shredder :: Vending Shredder 'Asterix'"
product_id: asterix
preview: /products/asterix/media/teaser.png
_buy: "mailto:sales@plastic-hub.com?subject=Inquiry%20-%20asterix"
teaser: "Autonomous Vending Bottle Shredder"
products: false
overview_drawing: true
featuresText: true


gallery:
 - url: "/machines/shredder/asterix/media/IMG_3184.JPG"
   image_path: "/machines/shredder/asterix/media/IMG_3184.JPG"
   alt: ""
   title: ""
 - url: "/machines/shredder/asterix/media/IMG_3186.JPG"
   image_path: "/machines/shredder/asterix/media/IMG_3186.JPG"
   alt: ""
   title: ""
gallery_drawings:
 - url: "/machines/shredder/asterix/drawings/dimensions.PDF"
   image_path: "/machines/shredder/asterix/drawings/dimensions.JPG"
   alt: ""
   title: ""
 - url: "/machines/shredder/asterix/drawings/parts.PDF"
   image_path: "/machines/shredder/asterix/drawings/parts.JPG"
   alt: ""
   title: ""
---
<div prefix="/templates/jekyll/" file="machine.html" context="machine global" class="fragment">
    
    <div prefix="/templates/jekyll/" file="machine_layout.html" context="machine global" class="fragment">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
        </li>
        {% if page.tabs.build != false %}
        <li class="nav-item">
            <a class="nav-link" id="build-tab" data-toggle="tab" href="#build" role="tab" aria-controls="build" aria-selected="false">Build</a>
        </li>
        {% endif %}
        <li class="nav-item">
            <a class="nav-link" id="resources-tab" data-toggle="tab" href="#resources" role="tab" aria-controls="resources" aria-selected="false">Resources</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="howtos-tab" data-toggle="tab" href="#howtos" role="tab" aria-controls="howtos" aria-selected="false">Howtos</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#media" role="tab" aria-controls="media" aria-selected="false">Media</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#discussion" role="tab" aria-controls="media" aria-selected="false">Discussion</a>
        </li>

        {% if page.wiring %}
        <li class="nav-item">
            <a class="nav-link" id="wiring-tab" data-toggle="tab" href="#wiring" role="tab" aria-controls="wiring" aria-selected="false">Electronics</a>
        </li>

        {% endif %}


    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="overview" role="tabpanel" aria-labelledby="overview-tab">

            {% include download_spec.html %}

            <div prefix="machines/shredder/asterix/" file="overview.html" context="machine" class="fragment">
    <div>

        <a href="/machines/shredder/asterix//renderings/perspective.png">
            <img src="/machines/shredder/asterix//renderings/perspective.png" style="margin:8px; float: left;max-width:50%;max-height: 400px;" />
        </a>

        <span style="font-size: smaller; margin-top: 16px;">
            Perfect for more complicated molds and better production rates as well good quality with high precision. Easy
            to transport and ideal for a educational context and small enterprises.
        </span>

        <div style="display: table-cell;">
            <div prefix="machines/shredder/asterix/" file="mini_specs.md" context="machine_shared" class="fragment">
    <p><span id="specs" style="padding: 16px">
            <table class="center specs">
                <tbody>
                    <tr>
                        <td>Type
                        </td>
                        <td>Shredder
                        </td>
                    </tr>
                    <tr>
                        <td>Version
                        </td>
                        <td>1.0 | Rev.A
                        </td>
                    </tr>
                    <tr>
                        <td> Voltage
                        </td>
                        <td>220V
                        </td>
                    </tr>
                    <tr>
                        <td> Status
                        </td>
                        <td>Development
                        </td>
                    </tr>
                </tbody>
            </table>
        </span></p>
</div>

            {% include feature_bars.html issues="95" complete="90" safety="80" safety_url="#" %}

            {% include machine_toolbar.html download="https://precious-plastic.org//archives/asterix.zip" preview="" %}

        </div>

    </div>

    <div style="margin: 8px;clear:both">

    </div>

    <hr />

    <div prefix="machines/shredder/asterix/" file="products.html" context="machine" class="fragment">
    <div style="padding:16px;text-align: center;font-size: smaller;">
        <h3>Products</h3>
        <div class="ty-vendor-plans">

            <div class="ty-grid-list__item" style="float:left;border-color: #c5c5c5;width: 200px;display: inline-block">
                <a href="https://bazar.preciousplastic.com/moulds/injection-moulds/piranha-clamp/">
                    <img height="200px" src="/_machines/assets/clamp.jpeg">
                    <br />
                    <p style="text-align: center;">Piranha Clamp</p>
                </a>
            </div>
            <div class="ty-grid-list__item" style="float:left;border-color: #c5c5c5;width: 200px;display: inline-block">
                <a href="https://bazar.preciousplastic.com/moulds/injection-moulds/water-cup-mould/">
                    <img height="200px" src="/_machines/assets/watercup.jpeg">
                    <br />
                    <p style="text-align: center;">Water Cup</p>
                </a>
            </div>

            {% include bazar_product_thumb.html title="Hair Comb" image="/_machines/assets/comb.jpeg" url="https://bazar.preciousplastic.com/moulds/injection-moulds/hair-comb/" %}

        </div>
    </div>
</div>
</div>
            <hr />
            
            <hr />

            <div prefix="/templates/jekyll/" file="used_in.md" context="machine global" class="fragment">
    <p>{% if page.usedin %}
    <h4>This is used in</h4>
    <div class="ty-vendor-plans small">
        {% for component in page.usedin %}
        {% for doc in site.machines %}
        {% if doc.product_id == component %}
        <div class="ty-grid-list__item_small">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover_small" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    {% endif %}</p>
</div>

        </div>

        {% if page.tabs.build != false %}
        <div class="tab-pane" id="build" role="tabpanel" aria-labelledby="build-tab">

            

            <div prefix="machines/shredder/asterix/" file="build.md" context="machine" class="fragment">
    <h3 id="buildthemachine">Build the machine</h3>
</div>

            {% if page.gallery_drawings %}
            <h4>Drawings</h4>
            {% include gallery_drawings caption="" %}
            {% endif %}

        </div>
        {% endif %}

        <div class="tab-pane" id="resources" role="tabpanel" aria-labelledby="resources-tab">
            <span style="font-size:smaller">

                <div prefix="machines/shredder/asterix/" file="product_resources.md" context="machine" class="fragment">
    <ul>
        <li><a href="">3D Preview</a></li>
        <li><a href="/products/asterix/electrics/wiring.png">Electrical wiring</a></li>
    </ul>
</div>

                <div prefix="machines/shredder/asterix/" file="resources.md" context="machine" class="fragment">
    <h3 id="resources">Resources</h3>
</div>

                <h4>Changelog</h4>

                <div prefix="machines/shredder/asterix/" file="changelog.html" context="machine" class="fragment"></div>

            </span>

        </div>
        <div class="tab-pane" id="howtos" role="tabpanel" aria-labelledby="howtos-tab">
            <div prefix="machines/shredder/asterix/" file="howtos.md" context="machine" class="fragment"></div>
            <h4>Build Howtos</h4>
            <div prefix="/templates/jekyll/" file="howto_used_in_machine.md" context="machine global" class="fragment">
    <div class="ty-vendor-plans">

        {% for doc in site.howto %}
        {% if doc.usedin and doc.enabled!=false %}
        {% for used in doc.usedin %}
        {% if used == page.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endif %}
        {% endfor %}
    </div>
</div>
        </div>
        <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="media-tab">
            <div prefix="machines/shredder/asterix/" file="media.md" context="machine" class="fragment">
    <h3 id="media">Media</h3>
</div>
            {% if page.gallery %}
            {% include gallery caption="" %}
            {% endif %}

            {% if page.gallery_social %}
            <h4>Media on the network</h4>
            {% include gallery_social caption="" %}
            {% endif %}

        </div>

        <div class="tab-pane" id="discussion" role="tabpanel" aria-labelledby="discussion-tab">
            <div id='discourse-comments' style="min-height: 600px;"></div>
            <script type="text/javascript">
                DiscourseEmbed = {
                    discourseUrl: 'https://forum.precious-plastic.org/',
                    discourseEmbedUrl: '{{site.url}}{{page.url}}.html'
                };

                (function() {
                    var d = document.createElement('script');
                    d.type = 'text/javascript';
                    d.async = true;
                    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
                })();
            </script>
        </div>

        {% if page.wiring %}
        <div class="tab-pane" id="wiring" role="tabpanel" aria-labelledby="wiring-tab">
            
        </div>
        {% endif %}

    </div>

    <div prefix="/templates/jekyll/" file="alternative_machines.md" context="machine global" class="fragment">
    <p>{% if page.alternatives %}</p>
    <h3 id="alternatives">Alternatives</h3>
    <div class="ty-vendor-plans">

        {% for doc in site.machines %}
        {% for alternative in page.alternatives %}
        {% if alternative == doc.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    <p>{% endif %}</p>
</div>
</div>


</div>