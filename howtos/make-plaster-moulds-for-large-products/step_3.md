### Mix and pour the plaster 

 Mix the casting plaster according to the manufacturer specifications. In this case, chopped fibreglass was added to the mix to increase the mould durability. 

Pour the mix into the box - as a general rule, pour it to twice the height of the model. 

As soon as you pour the mix, spend a few minutes taping the box with a hammer to make sure any air pockets rise to the surface.

Allow the plaster to cure for a couple of days before you demould.