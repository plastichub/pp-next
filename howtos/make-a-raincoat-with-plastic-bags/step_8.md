### Cut the materials 

 From your fused sheets you will now need to cut the template pieces (see step 8) for the construction of your raincoat. 1 back piece, 2 front pieces, 2x sleeves, and 3 parts for the hood. Add 2cm on the sides of each piece to allow room for sewing. If you are lacking material to cut the template, you may need to extend one or more of your sheets.

After doing this, cut the same template again from your cotton (again allowing 2cm on every side). This will form the lining of the raincoat.  