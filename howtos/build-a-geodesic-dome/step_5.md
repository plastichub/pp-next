### Prepare the beams 

 Take your beams (we used 20mm x 30mm) and cut them to the required lengths and amounts.
For our 2.5 m high dome, we need:
A: 1302 mm x 30
B: 1472 mm x 35
If you decided for another dome size, take the dimensions you’ve calculated.

Once you have all your beams cut into the right size, drill M6 holes at both ends of all the beams. Place the hole in the middle of the width and 20 mm from the edge.
