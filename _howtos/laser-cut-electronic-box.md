---
image: "/howtos/laser-cut-electronic-box/laser-box-1.jpg"
title: "Laser cut electronic box"
tagline: ""
description: "The original machines have a metal box where all the electronics are stored in. It’s quite basic and easy to make, especially since you are already working with metal. However, it takes quite some time to build. <br /><br />This How-to will show you a simple upgrade."
keywords: "hack"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready</h4>
<div class="step-text">
  An easier and quicker way to make the box is to lasercut it on 3mm plywood or MDF, which also lowers the chances of short circuits. <br />
  Bear in mind that this is more expensive than doing it by yourself.<br />
  <br />
  Each face of the box is lasercut and connected with some nut and bolts, super simple.<br />
  <br />
  We&#39;ve also added mounting holes for all the electronics inside the drawing which makes it easy and quick to connect everything in the right place.<br />
  <br />
  The laser cut parts can leave quite some rough burn-marks, we recommend to sand them when you receive them, looks way better.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/laser-cut-electronic-box/box-technical-p-1080.jpeg">
        <img class="step-image" src="/howtos/laser-cut-electronic-box/box-technical-p-1080.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Laser cut the parts</h4>
<div class="step-text">
  Download the lasercut files from the top of this page and send them to your local laser cutter (or cnc) to get the parts cut.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/laser-cut-electronic-box/laser-box-5.jpg">
        <img class="step-image" src="/howtos/laser-cut-electronic-box/laser-box-5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Sand your parts</h4>
<div class="step-text">
  It can make a world of difference if you take the time to sand the rough cut parts. It will help remove the burn marks and soften the overall look - unless that’s what you’re going for.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/laser-cut-electronic-box/laser-box-4.jpg">
        <img class="step-image" src="/howtos/laser-cut-electronic-box/laser-box-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Assemble and install</h4>
<div class="step-text">
  With that done, you can now use nuts and bolts to fasten the box together and install all the wiring.<br />
  <br />
  Mount it to the machine and enjoy recycling!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/laser-cut-electronic-box/laser-box-2.jpg">
        <img class="step-image" src="/howtos/laser-cut-electronic-box/laser-box-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/laser-cut-electronic-box/laser-box-3.jpg">
        <img class="step-image" src="/howtos/laser-cut-electronic-box/laser-box-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>