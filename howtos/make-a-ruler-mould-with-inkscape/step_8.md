### Add fastening items 

 You'll now need to add holes for wing nuts and bolts or quick release clamps.

-Select the circle icon in the left menu
-Draw a circle in the rough location
-You can adjust the x & y radius from the top menu
-Now select it and duplicate it by pressing CTRL + D
-Now hold CTRL and move it to the right (holding control means it will snap to be aligned in the x or y axis of where it was before)
-Now select both top holes and duplicate them and do the same to create the bottom two holes