### Material for the grips 

 Plastic waste: I made experiments with polystyrene (PS) and polyethylene (PE). PS looks more strong but I had only black one so the presented objects are made of PE. The pedal volume is 145 cm^3 and the grip 84 cm^3.

For the grips:

- metal tube (internal diameter = diameter you want for the grips, big ones are more comfortable. I chose 35mm. Lenght = lenght you want for the two grips - 3 mm. I have 2 x 130 mm so my grips are 127 leght)
- metal sheet (steel 4 mm thick; 180 x 60 mm)
- threaded rod (M6 x 450; M8 x 150 mm)
- 6 nuts M6
- metal cylinder (diameter 22.3 length 111 mm) (standard handlebar have diameter 22 mm so take just a little bit more)
- 1 bolt M8 x 20
- 1 washer M20
- 1 nut M8