---
image: "/howtos/heat-shield-for-injection-and-extrusion-machines/20201124_150234[1].jpg"
title: "Heat Shield for Injection and Extrusion Machines"
tagline: ""
description: "Increase the safety and appearance of your injection and extrusion machine"
keywords: "injection,extrusion"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic-philippines">precious-plastic-philippines</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Build your heat shield</h4>
<div class="step-text">
  The YouTube video shows it better. As everyone&#39;s machines are different measurements, this is not included here, but should be self explanatory in the video.<br />
  <br />
  We used round stainless steel tubing at 4 inch size. However you can also use square tube. As long as your tube is long enough to fit over the heater bands and insulation without direct contact. <br />
  <br />
  For insulation we use 5 meters of muffler insulation.<br />
  <br />
  We secured the tubing with m6 bolts and tapped the tubing. You can use other size bolt or even machine screws.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>