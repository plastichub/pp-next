---
title: "Sheetpress-Cell Specification" 
permalink: "/specs/products/sheetpress-cell"
layout: "print"
---

## {{page.title}}

{% include specs.html %}

{% include product_image.html size="medium" src="/products/sheetpress-cell/drawings/spec.JPG" %}

| Specification                                 |             |
|--------------------------------------|-----------------------|
| Type                               | Sheetpress              |
| Version                            | 4.3                     |
| Weight                             | 450kg   |
| Sheetpress Dimension               | 1620 X 1620 X 1780 mm |
| Voltage                            | 400V                  |
| AMP                                | 32A                   |
| Power                              | 15kW                 |
| Input Flake Size                   | Large, Medium, Small  |
| Max Running Time                     | 8 hours per day       |
| Max temp                             | 300°C                 |
| Tested Plastics                      | HDPE, LDPE, PP, PS    |
| Using foils?                         | Yes                   |
| Input Between Plates                 | 300mm                 |
| Size of Sheet                        | 1200 x 1200 mm        |
| Range of Sheet Thickness             | 4 - 35mm              |
| Sheets Per Day (12mm)                | 3                    |
| Sheets Per Day with full system (12mm)  | 10                    |
