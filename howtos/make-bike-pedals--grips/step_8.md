### Finish and Assembly 

 Secure the pedal in the jig.
Mount the 11 mm drill bit and align it with the pedal center.
Secure the jig to the drill.
Drill through all the pedal.
Mount the 20mm drill bit.
Drill the bike side of the pedal 8 mm depth.
Flip the pedal.
Drill the outer part of the pedal 15 mm depth (measured from the flat face, not the top triangle).