**Plastic Grinder** 

<div style="clear:both">
    ${product_overview_drawings}
</div>

Small plastic grinder - ideal to grind bigger plastic flakes down to size

### Features / Details

- Motor overload protection
- Status lights 
- Emergency button
- After sales service
- 2 different sieves included

<hr/>

<p style="text-align: center">
    <h4 style="text-align: center;">Specifications</h4>
    ${specs}
</p>

${product_resources}
