### 1.5m Sheetpress System - Designed for Disassembly 


Complete set of blueprints, CAD files and instructions on how to build this upscaled sheetpress system.

-56% larger pressing surface
-Designed for easy disassembly
-Improved electronics