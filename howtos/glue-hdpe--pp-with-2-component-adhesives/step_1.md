### Which is the best Adhesive? 

 First off: Using 2-component adhesives is very wasteful and its use should be avoided as much as possible. Please first consider to use screws, snapping unions, or even change the design or the material.

But we want to show which adhesive offers the best performance in case you see no other option.

Among the world of 2-component adhesives there are several types: epoxy, methyl methacrylate, polyurethane... We found that methacrylate adhesives work best for bonding plastic. These can especially be useful when trying to bond PP and HDPE which are generally difficult to bond with other solutions. 

Of course the composition of the adhesive will change depending on the brand. Based on our experiences we recommend to use the Permabond Structural Adhesive TA4605.
