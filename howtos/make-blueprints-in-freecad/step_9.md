### Add Blueprint Details 

 -There is always a section in blueprints explaining the details of the drawing
-You can see by the green boxes what is editable in the template
-Click the green box within each section and it will open a modal window for you to fill in the text

Don't forget to add
-Who made it
-When it was made
-What the dimensions of the notations are
-What the scale of the view is
-What version number it is
-If there are multiple sheets explaining the file
