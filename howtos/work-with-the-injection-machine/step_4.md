### Prepare your material 

 Close the injection barrel with the nozzle cap and fill in the new material into the hopper. Leaving ~1cm of space to the top of the inlet prevents the plunger from getting stuck during the injection process by shearing of unmelted plastic.
Before the injection can start, a heat-up time of 10-15min is recommended to melt the material uniformly. Do not try to move the plunger before the preheating is done!
There might be still some old material left inside the barrel. Therefore actuate the lever without a mould or closing lid attached, until you stop seeing the previous material leaving the nozzle. Make sure your production leftovers are getting recycled properly!
For big moulds (>100g) the first infill might be not enough to fill the mould fully. Use the plunger to compress the material within the barrel and add more material, so the barrel is full with plastic.