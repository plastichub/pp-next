---
image: "/howtos/bend-with-the-sheetpress/fictionfactory.jpg"
title: "Bend with the Sheetpress"
tagline: ""
description: "We have used the sheetpress as a tool for bending plastic sheets, solid surface materials or even extruded beams. The process is quite simple but might require some preparation to get the best results. Below are the steps taken to do this, although there is still room for further improvements! <br /><br />Tools needed:<br />- Sheet press<br />- Some kind of mould<br />- Clamps"
keywords: "melting,sheetpress"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
- "sheetpress"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/fiction-factory">fiction-factory</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Make a mould</h4>
<div class="step-text">
  To have a consistent result it&#39;s best to use a mould for the bending. For bigger projects, sharp corners or intricate shapes a counter mould is recommended! This ensures the sheets is pushed in the right shape.<br />
  <br />
  It’s also important to keep in mind where the clamps will be placed so make sure there is space for the clamps to grab on to.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/IMG_6196 2.jpg">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/IMG_6196 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/buig.jpg">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/buig.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Temperature and timing</h4>
<div class="step-text">
  The next step is to decide on temperature and timing. <br />
  <br />
  The right temperature depends on the plastic type and thickness. You can check in between to see if it bends far enough or experiment with smaller pieces before starting with bigger sheets.<br />
  <br />
  Below are some of the settings we have worked with.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/Schermafbeelding 2020-10-29 om 14.55.03.png">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/Schermafbeelding 2020-10-29 om 14.55.03.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/Schermafbeelding 2020-10-29 om 14.56.11.png">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/Schermafbeelding 2020-10-29 om 14.56.11.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Place in the Sheetpress</h4>
<div class="step-text">
  We placed the plastic on top of a 3mm sheet of mdf to make sure it doesn’t stick to the press. Once the sheet is in place move the bottom bed to the top so it’s almost closed (see picture). A distance of 1 cm should be ok.<br />
  <br />
  It is also possible to bend sheets larger than the sheetpress, as long as the bended part fits the press (second image is a 2x1 sheet).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/IMG_6405.jpg">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/IMG_6405.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/WhatsApp Image 2020-10-29 at 09.30.44.jpeg">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/WhatsApp Image 2020-10-29 at 09.30.44.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Bend the sheets!</h4>
<div class="step-text">
  After heating up the sheets sufficiently it is time to move it to the mould. This requires some fast handling, since it quickly cools down again. Be sure to have all the clamps within reach! It also helps to do this with two persons.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/20200720_113045.jpg">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/20200720_113045.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/WhatsApp Image 2020-10-29 at 09.31.41.jpeg">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/WhatsApp Image 2020-10-29 at 09.31.41.jpeg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bend-with-the-sheetpress/IMG_6204.jpg">
        <img class="step-image" src="/howtos/bend-with-the-sheetpress/IMG_6204.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>