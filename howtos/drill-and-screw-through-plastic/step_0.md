### Stay safe 

 When talking about safety we are only referring to precautions about working specifically with plastic, as it’s our thing. We assume that working with the drill press needs a certain level of expertise so please take all the precautions related with how the tool works.

When using the drill, special attention must be taken when working with plastics such as PS or ABS. There is a danger of overheating the material locally and accidentally releasing bad fumes. So in order to work safer make sure to use a gas mask with ABEK filters to prevent inhaling possibly toxic fumes. An easy way to identify them is to check if the filter has four colour lines (brown, grey, yellow & green).
