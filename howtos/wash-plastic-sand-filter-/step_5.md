### Fix the inside parts 

 Start with installing the manometer, either find one with a coupling on the back or use the right fittings so that it faces to the front. After that, you install the PVC pipes with the right washers and tighten everything carefully with an alligator wrench. Make sure not to break the plastic threads.