### Create How-to's! 

 Share to the world how you run your Community Point so other people can learn from you and start using your solutions to grow their network. Make sure to document and share your best processes and techniques.

Also stay up to date what's going on in your network and try to motivate and help your group to create How-to's of their successful techniques, hacks and products.

👉 community.preciousplastic.com/howto