### Cut the sides for the closing screws 

 Final step! With the hand saw, cut slots (two per side) for the bolts to fit in and out more easily. For closing the mold, four bolts and butterfly nuts (no. 13-14) will be used.

(Drawings page 14)