### Make the connection piece 

 With all the parts in the bag, let’s start cutting the steel pipe nipple (no. 7) in half to make the mold nozzle.
Get the metal sheet (no. 8) and turn a hole in the center with a diameter to fit one half of the steel pipe nipple in tightly.
Weld the parts no. 7 and no. 8 together. Then chamfer the welded edge on the lathe.

(Drawings page 3-5)