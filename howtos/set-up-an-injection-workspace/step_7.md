### Learn the basics of plastic 

 Before you start it is crucial to get a solid understanding of plastic. How it works, the different types, melting temperatures and so on.

Head over to our Academy and dive deep in the Plastic chapter. 
👉 http://tiny.cc/basics-about-plastic
