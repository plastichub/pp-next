### Parting lines 

 If you go for a cheaper mould, then go for parting lines in the edge of the parts, making it easy to post process and to achieve good final results.

If you go for a good mould, place them either way, since they will be good enough for you not to post process them or simply polish them slightly. In this case, design choices might be the parameters that will help you decide. The images you see show how to slightly hide the parting line by adding a round up, which will make the mould slightly more expensive (especially if you do this on small features that could want a few tool changes and a lot more time) or how to keep it simple. 


