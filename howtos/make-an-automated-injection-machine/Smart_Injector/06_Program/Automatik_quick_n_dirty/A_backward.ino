
/*
Automatic injecting extruder (backward rotation)
*/

void backward()
{
	while (Xcurrentrotations < rotationsback)
	{
		for (int i = 0; i < Xstepsinfor; i++)
		{
			if ((i % 40) == 1 && Xdt > Xdtendback)
			{
				Xdt = Xdt - 10;
			}
			digitalWrite(DIR3, LOW);
			digitalWrite(EN3, HIGH);
			digitalWrite(PUL3, LOW);
			delayMicroseconds(Xdt);
			digitalWrite(PUL3, HIGH);
			delayMicroseconds(Xdt);
		}
		Xcurrentrotations++;
	}
	Xcurrentrotations = 0;
	while (Xcurrentrotations < 6)
	{
		for (int i = 0; i < Xstepsslowingdown; i++)
		{
			if ((i % 50) == 1 && Xdt < Xdtref)
			{
				Xdt = Xdt + 50;
			}
			digitalWrite(DIR3, LOW);
			digitalWrite(EN3, HIGH);
			digitalWrite(PUL3, LOW);
			delayMicroseconds(Xdt);
			digitalWrite(PUL3, HIGH);
			delayMicroseconds(Xdt);
		}
		Xcurrentrotations++;
	}
	Xcurrentrotations = 0;
}

//manuell rotating backwards after pressing second button
void backwardsmanuell()
{
	Xcurrentrotations = 0;
	delay(entprell);
	while (valb1 == 1 && Xcurrentrotations < rotationsback)
	{
		Serial.println("rotation backwards");
		for (int i = 0; i < Xstepsinfor; i++)
		{
			digitalWrite(DIR3, LOW);
			digitalWrite(EN3, HIGH);
			digitalWrite(PUL3, LOW);
			delayMicroseconds(Xdt);
			digitalWrite(PUL3, HIGH);
			delayMicroseconds(Xdt);
		}
		Xcurrentrotations++;
		valb1 = digitalRead(b1);
	}
	Xcurrentrotations = 0;
	while (Xdt < Xdtref)
	{
		Serial.println("deccelerate");
		Xdt++;
		digitalWrite(DIR3, LOW);
		digitalWrite(EN3, HIGH);
		digitalWrite(PUL3, LOW);
		delayMicroseconds(Xdt);
		digitalWrite(PUL3, HIGH);
		delayMicroseconds(Xdt);
	}
	delay(entprell);
	valb1 = digitalRead(b1);
}
