### MOULD EXPLAINED 

 This mold is made from three plates: the top plate with the nozzle welded, the spacer with the outline of the handplane cut, and the bottom part left untouched. Our pieces are 25mm width as the handplanes are 20mm width.