### Cut tubes 

 In this technique you cut the mould in the same way as you would cut beams for a mitre joint: i.e. if you want an angle of 90° for your beam, make two 45° cuts.
Once you know which one you need, it’s time to cut your metal tubes to your desired angle. It’s important to make the cut as clean and straight as possible to avoid big gaps when the mould is assembled. Finish up your cuts with a file to ensure your edges are smooth for the next steps to come.

Tip: Consider where the weld seam is on your piece of metal tube as it will l leave a mark on your plastic beam.
