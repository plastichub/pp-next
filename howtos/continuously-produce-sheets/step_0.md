### Check your setup 

 First, check if you have the right safety equipment: You will need gloves, a mask, safety glasses and ventilation. 

As well as a sheetpress you will also need: a cooling press, prep table and sliding tools. 

To continuously produce sheets you will need a minimum of three pairs of mould sheets and one frame for each sheet you plan to make per day. It is a good idea to have one spare set of moulds in case any get damaged. The best beams to use for the mould frames are T beams as they are strong, light and cut the edge of the sheet nicely. We used 3mm Aluminium sheets for the mould as they are strong, light, conduct heat quickly and do not stick to the plastic. 