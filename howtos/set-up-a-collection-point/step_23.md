### upload.preciousplastic.com 

 It's nice to see your plastic collection growing! By weighing the plastic coming in, and writing it down, you will be able to have a concrete impact measure of how much plastic you are collecting and providing to other workspaces.

Take a moment every now and then to upload your kilograms to our website - like that we can track how much impact our community makes and inspire the world with how much plastic waste we could save and recycle :)

(Also, when you weigh it, you will understand how a few kilograms of plastic take up so much space. Incredible huh? Think globally about how much space you saved from our planet. Thank you for that!)

👉 upload.preciousplastic.com 