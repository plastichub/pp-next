# ${breadcrumb} -> ${product_name}

**Brief** :
${product_brief}

![](./media/preview.jpg)

<div>
    <a href="${product_overview}">
        <p style="text-align: center">
            <img width="100%" src="${product_overview}">
        </p>
    </a>
</div>

**Version** : ${product_version}

**Compatible with** : ${product_pcompat}

**Related** : ${product_related}

**Components** : ${product_components}

${header}

${product_gallery}

${body}

<br/>
<hr/>

**Product Resources**

- [BOM 'Bill of materials'](${product_bom})
- [3D Preview](${product_3d})
- [Drawings](./drawings)
- [CAD model](./cad)
- [Wiki](./wiki)
- [Laser files](./laser)
- [Discord Chat](${product_chat})
- [Electrical wiring](./electronics)

${directory_listing}

${product_howtos}

<hr/>

${footer}
