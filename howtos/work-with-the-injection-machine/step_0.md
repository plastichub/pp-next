### Safety 

 Wear heat resistant gloves to prevent burning yourself from heated parts like: injection barrel, nozzle, plunger and the mould, which can heat up over time! You can use thick leather gloves, which are used for welding. Preferably they have a smooth outside surface (not suede leather), so plastic cannot stick to it.
Wear safety glasses during the injection process!
Avoid contact with molten plastic at all times!
Make always sure your nozzle connectors are in a good condition! Defect adapters can enable the plastic to squeeze out from the barrel under high pressure and cause risk to health!
Be careful while opening the nozzle, plastic can drip out immediately after opening!
Use a fume extractor with a rated filter to extract the fumes! See How to xxx