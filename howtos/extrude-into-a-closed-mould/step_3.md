### Understanding extruding times 

 Depending on the specs of your extrusion machine and the type of mould the time to fill it up will vary. For the V4 brick mould on the V4 extrusion, it takes exactly 4min. However 20 seconds either side can result in underfilled mould or even a burst one. So it’s very important to get this timing right!

One way to figure this out: Extrude at the speed you intend to work with, without a mould for 10sec, cutting the plastic and then extruding for a further 30sec collecting and weighing the result. 

Make sure to run the test again if you decided to change the size of plastic granules, or the type of plastic you are extruding as it will affect your extrusion speed. 
