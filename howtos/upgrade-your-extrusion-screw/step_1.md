### Install the screw 

 The new screw should fit straight into your existing barrel if your machine has been built according to the drawings in our Download kit. You can also have a look on the Bazar for other sizes if you should need. 