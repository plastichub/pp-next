### Extruding 

 After connecting your mould to the extrusion ensure that you stay clear of the nozzle while the machine is running. Always wear Personal Protective Equipment (PPE) and never leave the machine running unattended. Routinely checking your mould for signs of over filling and blockage. While also making sure the extruder has a constant batch of plastic granules. 