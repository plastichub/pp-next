### Cutting the Strips 

 Image 6:

Place a weight at the end of the bag, on the handles.
We will be cutting parallelly along the length of the bag, not all the way till the end but until the margin.
Make cuts all the way until the handles. Cut the handles off.

Note: 
- Make cuts roughly at an equal distance from each other to get a yarn of uniform thickness.
- You can cut the yarn as thick as you like depending on your project. In the video, I have cut the yarn about 1.5 cm apart which is good to crochet with with a 3 mm hook. If you would like to make something chunkier, you can cut it 5 - 8 cm apart. Of course, the thickness of the yarn also depends on the thickness of the bag, so just play around!

