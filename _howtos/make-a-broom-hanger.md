---
image: "/howtos/make-a-broom-hanger/howto-broom-hanger-cover.jpg"
title: "Make a broom hanger"
tagline: ""
description: "A clamp to hang brooms and mops in the wall. Here are the steps to make its mold for the injection machine."
keywords: "product,injection,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "injection"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/el-tornillo-taller">el-tornillo-taller</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get your materials and prepare the work</h4>
<div class="step-text">
  Make sure you have all your materials ready and go through the attached drawings and steps to understand the full picture of the process. This will help you to work more efficiently and accurate.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broomhanger-mould-11.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broomhanger-mould-11.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Make the connection piece</h4>
<div class="step-text">
  With all the parts in the bag, let’s start cutting the steel pipe nipple (no. 7) in half to make the mold nozzle.<br />
  Get the metal sheet (no. 8) and turn a hole in the center with a diameter to fit one half of the steel pipe nipple in tightly.<br />
  Weld the parts no. 7 and no. 8 together. Then chamfer the welded edge on the lathe.<br />
  <br />
  (Drawings page 3-5)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-1-1.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-1-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-2-3.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-2-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-3-1.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-3-1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make the female mold </h4>
<div class="step-text">
  Now we’ll make the female mold. Take the aluminium block (no. 1) and drill a 1” deep hole in the center of face A of the block. Start with smaller bits until you reach the inch. Then, mill face B to open a channel of 1” wide. Use a round point bit to get a better quality finish. <br />
  <br />
  (Drawings page 6)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-4-1.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-4-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-4-2.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-4-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-4-3.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-4-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Plastic entrance hole</h4>
<div class="step-text">
  For the plastic entrance, drill a 5mm hole through the center of your female mold.<br />
  <br />
  (Drawings page 6)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-6.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Male mold part 1</h4>
<div class="step-text">
  The male mold is made of three parts. Get your aluminium parts no. 2, 3 and 5 ready and start with the aluminium rod (no. 5). Mill one side of the rod to a height of 3,17mm and a width of 14,19mm. <br />
  <br />
  (Drawings page 7)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-7-1.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-7-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-7-2.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-7-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-7-3.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-7-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Male mold part 2</h4>
<div class="step-text">
  Now, get part no. 3 and mill one face on an angle of 15°. Then, mill the other face to an opposite angle of 15°, until the width of the narrow face matches the face of part 5 (see last image). That should be 14,19mm in the narrower face and 21mm in the wider face. <br />
  <br />
  (Drawings page 7)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-8-1.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-8-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-8-2.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-8-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-8-4.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-8-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Male mold assembly</h4>
<div class="step-text">
  To assemble the male mold, align the center of the previous parts with the center of part no. 2, press with clamps and drill two 3/16” deep holes. <br />
  On part no. 5, drill flat countersinks for the screws (no. 9) head.<br />
  Fix the three parts with the button head screws, washers and nuts (no. 9-11). <br />
  <br />
  (Drawings pages 8-9)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-9-.png">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-9-.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-9--.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-9--.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-9-3.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-9-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Drill the conical guides holes</h4>
<div class="step-text">
  Align in place the female and the male parts of the mold and fix them with a small press or locking pliers. Apply the the hole positions from the drawings to the face of part no. 2 and drill two 9,5mm diameter holes. Drill through no. 2 and 1cm deep into no.1.<br />
  <br />
  (Drawings page 10)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-10.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-10.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Conical guides</h4>
<div class="step-text">
  Turn your round metal bar (no. 6) to create the conical guides and saw a channel on one side to let the air flow out when inserting. With a vice or a hammer, insert the conical guides into part no.1. <br />
  <br />
  (Drawings page 11)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-11-2.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-11-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-11-3.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-11-3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-11-4.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-11-4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Connect the mould</h4>
<div class="step-text">
  Fix the nozzle, the female and the male parts with a small press or locking pliers, and drill four 9/32” holes through the corners of both parts.<br />
  Close the mold and turn the ends to get an even surface between the male and the female parts. <br />
  <br />
  (Drawings page 12)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-12.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-12.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-13.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-13.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Make end caps</h4>
<div class="step-text">
  Fix each cap (no. 4) in place and drill four 5mm diameter holes. All through the cap and 25mm deep into the female and male mold parts. Tap each hole of the female and male parts with a ¼” thread.<br />
  On the caps, re-drill the holes up to ¼” and fix them with the bolts (no. 12).<br />
  <br />
  (Drawings page 13)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-14-1.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-14-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-14-2.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-14-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-14-3.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-14-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Cut the sides for the closing screws</h4>
<div class="step-text">
  Final step! With the hand saw, cut slots (two per side) for the bolts to fit in and out more easily. For closing the mold, four bolts and butterfly nuts (no. 13-14) will be used.<br />
  <br />
  (Drawings page 14)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-15.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-15.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Mould done!</h4>
<div class="step-text">
  And you’re done! Here is your broom hanger mold, it&#39;s time to inject.<br />
  Flexible plastics like HDPE and PP work better for the broom hangers as they won&#39;t crack during use. <br />
  <br />
  To open the mold, us a flat screwdriver to pull apart the parts gently. To take out the plastic product, use the flat screwdriver or a putty knife to open and release it from the male mold. It&#39;s easier if you do this process when the plastic part is still hot, but remember to close it back to its original shape after releasing.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-16-1.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-16-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-16-2.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-16-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-16-3.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-16-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Happy hanging :)</h4>
<div class="step-text">
  To install the broom hanger on the wall, drill a hole and fix it with a wall plug and a screw.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-17-1.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-17-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-broom-hanger/howto-broom-hanger-17-2.jpg">
        <img class="step-image" src="/howtos/make-a-broom-hanger/howto-broom-hanger-17-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>