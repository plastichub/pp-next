### Turn it into something new! 

 Now you have your clean plastic and you can create some beautiful Precious Plastic products with it.
Though as mentioned this is not finished development, but already works and can give you a good starting point. Feel free to test and modify and share back if you find other good solutions!
❗Be extra careful with the electronics as everything runs on 220 Volts and you have to deal with a lot of water.❗
But if everything works you have a very efficient system to process large amounts of dirty plastic for your workspace.
