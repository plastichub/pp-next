### Attach roof frame structure - purlins 

 The purlins are 2 x 2 pieces of timber that will connect directly to the roofing material. Due to the length of timber, we had access to we had to join them in the middle. We overhung them from the trusses by 20cm.