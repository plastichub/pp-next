---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/mm-home-page-feature.jpg
  actions:
    - label: "<i class='fas fa-download'></i> Install now"
      url: "/docs/quick-start-guide/"
excerpt: >
  Precious Plastic Library - Unofficial <br />  
feature_row:
  - image_path: /assets/images/mm-customizable-feature.png
    alt: "customizable"
    title: "Machines"
    excerpt: "Machines, components"
    url: "/machines"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - image_path: /assets/images/mm-responsive-feature.png
    alt: "fully responsive"
    title: "Projects"
    excerpt: "All projects found in the PP universe"
    url: "/projects"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - image_path: /assets/images/mm-free-feature.png
    alt: "100% free"
    title: "Howtos"
    excerpt: "Howtos"
    url: "/howto/"
    btn_class: "btn--primary"
    btn_label: "Learn more"      
---

{% include feature_row %}
