### Welding the angle bars 

 Once both sides are grounded take the second plate (part A) and 2 angle bars (parts B) and clamp them together as seen in the picture. A snug fit is required and the angle bars must be pressed firmly and evenly against the plates.

Tack weld the angle bars to the recently added flat plate (the one without the rounded edges). Take this opportunity to make sure that the rounded corner plate can still be removed. It will be tight, that is ok - too loose would be a problem. 
