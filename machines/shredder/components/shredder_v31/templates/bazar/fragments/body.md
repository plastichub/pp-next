Open source and upgraded PP v3 shredder.

This component is being used widely in our product family since to years and every detail have been taken care of.
The unit is being produced with professional equipment and is according to industrial tolerances for all it's components.

This product is open source, please check the wiki section for downloads and the 3D model. Feel free to copy as many others did. Laser files and howtos are included.


<div style="clear:both">
    ${product_overview_drawings}
</div>

### Features / Details

- 32 hexbar, 30 mm drive shaft
- Stronger bearings : UFCL 205 
- Safety shields
- Enforced bearing mount
- Stainless knifes and hopper
- Extra treatment for knifes, max. lifespan and efficiency

<hr/>

<div>    
    <div style="padding: 16px; display: inline-block">
        ${product_resources}
    </div>
</div>
