### Different drying methods 

 There are several different DIY methods to dry polymeres; you can use an oven, a dehydrator or a vacuum dryer.
But In this How-To we will focus on a self-built drybox with silica gel, because this method delivers the best results while being cheap, easy and energy efficient. One of the problems we encountered with the oven is overheating the plastic, resulting in a big mess. We tried replacing the silica gel with a household dehydrator, but they are not designed to achieve this kind of low humidity levels needed to effectively dry plastic. We also included a temperature table for different polymers.
