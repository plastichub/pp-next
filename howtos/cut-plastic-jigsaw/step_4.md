### Secure the piece firmly 

 Vibrations will make the job unsafe and the cut inaccurate.

Therefore make sure to clamp the piece firmly to the workbench. Use clamps with a rubberised jaw to prevent any scratches or marks on the sheet.

Cut close to the workbench, to avoid as much vibration as possible and enable a stable cut.