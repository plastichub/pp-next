---
image: "/howtos/cut-plastic-with-the-table-saw/BLADES1.jpg"
title: "Cut plastic with the table saw"
tagline: ""
description: "The table saw is the best way to achieve straight cuts through a plate or a beam. However in order to achieve a clean cut in plastic there are few considerations we will explain here."
keywords: ""
enabled: "true"
sidebar:
nav: "howtos"
tags: []
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Take the right precautions first.</h4>
<div class="step-text">
  When talking about safety we are only referring to precautions about working specifically with plastic, as it’s our thing. We assume that working with the table saw needs certain level of expertise so please take all the precautions related with how the machine works.<br />
  <br />
  When cutting with the table saw there is danger of overheating and melting the plastic. If this happens some bad fumes could be released. So in order to work safer make sure to use a gas mask with ABEK filters to prevent inhaling possible toxic fumes. Special attention on plastics like PS and PVC.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-with-the-table-saw/SAFETY FIRST 2.jpg">
        <img class="step-image" src="/howtos/cut-plastic-with-the-table-saw/SAFETY FIRST 2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - What makes a clean cut</h4>
<div class="step-text">
  When cutting, the blade removes small layers of plastic, which we call chips. A circular blade is made out of multiple small blades, which are called teeth. It could happen that due to the speed and force needed in the process some chips got melted damaging both the piece and the blade.<br />
  <br />
  So in order to avoid overheating, several factors have to be taken into account and finding the right blade and using the right cutting settings play an important role.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-with-the-table-saw/CLEAN CUT4.jpg">
        <img class="step-image" src="/howtos/cut-plastic-with-the-table-saw/CLEAN CUT4.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Choose the right blade</h4>
<div class="step-text">
  When choosing a blade one important feature is the tooth configuration (drawing). That’s the shape of each teeth and that’s going to affect how the material is removed. After trying different blades we have conclude that a tooth configuration like this is more versatile as it works the best on HDPE, PP and PS. But If this specific blade is not provided by any of your local dealers try a Triple chip grind (TCG) as it would work pretty good for HDPE and PP. <br />
  <br />
  Also the cutting angle (∝) is going to define the way the material is going to be removed. This is the angle at which the blade is going to enter our piece. When working with plastic the cutting angle must be negative or at least 0º to reduce the stress while cutting and prevent chips from melting. Moreover, it’s important to keep your blades sharpened in order to guarantee the best result.<br />
  <br />
  Another important point is the number of teeth (Z) and that depends on how thick is the piece we want to cut. For a Ø300mm blade:<br />
  - A high number of teeth (84-96) works good for thicknesses up to 25mm. <br />
  - For thicker solid pieces we’ll need a blade with less teeth (72). The reason why is that we’ll need more space between tooth in order to remove the increased amount of material.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-with-the-table-saw/tooth configuration7.jpg">
        <img class="step-image" src="/howtos/cut-plastic-with-the-table-saw/tooth configuration7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Cutting speed</h4>
<div class="step-text">
  Usually while working with wood we set the machine to up to 4500 rpm. For plastic it is not the case.<br />
  <br />
  Much slower cutting speed (around 3000rpm) combined with a fast feeding will give the best results.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-with-the-table-saw/4+.jpg">
        <img class="step-image" src="/howtos/cut-plastic-with-the-table-saw/4+.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Blade height</h4>
<div class="step-text">
  How high above the plate we place the blade could have a huge impact on the result. When working with plastic, less teeth cutting at the same time seems to create less heat and it tends to give cleaner cuts.<br />
  <br />
  That’s why we recommend to leave a significant distance between the plate and the top of the blade
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-with-the-table-saw/5+.jpg">
        <img class="step-image" src="/howtos/cut-plastic-with-the-table-saw/5+.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Reduce the vibrations</h4>
<div class="step-text">
  Give the piece enough stability to embrace the cut without vibrations. This will not only guarantee a safer job but will also give you the best results.<br />
  <br />
  You can do so by clamping the piece to the table and placing your hands safely where the clamp cannot reach.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/cut-plastic-with-the-table-saw/6+.jpg">
        <img class="step-image" src="/howtos/cut-plastic-with-the-table-saw/6+.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>