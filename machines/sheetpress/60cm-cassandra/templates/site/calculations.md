
## Force Calculations

to demonstrate that the motor and gearbox are capable of at least 2 tons clamping force, lifting a platten that weighs 20kg

Lead screw calculation:

```math
Torque(raise) = F x Dm/2x(L+u x PI x DM)/(PI x Dm-u x L)
```

[See leadscrew force torque calculator](https://daycounter.com/Calculators/Lead-Screw-Force-Torque-Calculator.phtml)

Toggle clamp calculations:

```math
P = F a / 2 h
```

where P = input force, F = output force, a = arm length, h = height (displacement of toggle pivot from line between end pivots)

[See toggle joint calculation](https://www.engineeringtoolbox.com/toggle-joint-d_2077.html)

Torque of 25Nm from the gearbox (as claimed) on a 20mm screw with a 4mm pitch and a brass nut with a frictional coefficient of 0.06 gives about 1575N pushing force on the nut.

Connected to an equal toggle mechanism of 125mm arm lengths, this would generate a perpendicular force of 820 newtons (80Kg) at the bottom of the stroke (toggle displacement 120mm), 98438 newtons (10 tons) at the top of the stroke (toggle displacement 1mm).

With a toggle diplacement of 5mm (press almost fully closed) the force generated is 19688 newtons or around 2 tons.

18nm from the gearbox (1.2NM x 15:1) gives 1135N push on the screw, generating 70938N (7.2T) at 1mm toggle displacement and 14118N (1.4T)at 5mm

Using a higher frictional coefficient of 0.1, torque of 18Nm from the gearbox gives 690N push on the screw, generating 359N (40Kg) at 120mm displacement 8628N (900Kg) at 5mm and 43125 (4.4T) at 1mm displacement. This seems like a more realistic estimate, given that friction on the collar and toggle pivot joints is not accounted for in this equation. Even half this would give a comfortable 2 ton max clamping force and still be able to lift 20kg from fully open. Also these calculations are based on the smallest stepper in the Nema 23 package size, up to 3nm are available

More detailed calculations will be performed using the [calculator](https://cdn.automationdirect.com/static/manuals/surestepmanual/appxc.pdf)

At max motor speed of 400 RRM with the 15:1 gearbox and 4mm pitch screw, the motor would move the nut on the screw at 1.7mm/s, meaning the plattens would take about 70s to perform a full 120mm stroke. If a 10:1 gearbox were used, it would take 45s.

