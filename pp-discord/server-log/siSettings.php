<?php
include_once 'config.php';
/**
 * @version 0.1.0
 * @link https://github.com/mc007
 * @author XApp-Studio.com support@xapp-studio.com
 * @license : GPL v2. http://www.gnu.org/licenses/gpl-2.0.html
 */

//////////////////////////////////////////////////////////////////////////////////
//
//  Killswitchs
//
//////////////////////////////////////////////////////////////////////////////////
$screen = (array) get_current_screen();
if (!$screen['base'] === 'settings_page_SISettings#app=manager') {
    return;
}

$ROOT_DIRECTORY_ABSOLUTE = realpath(dirname(__FILE__) . DS);

//wp related
$XAPP_PLUGIN_DIR_NAME = basename($ROOT_DIRECTORY_ABSOLUTE);
$XAPP_PLUGIN_URL = plugins_url('', __FILE__);

$appFile = file_get_contents(realpath($ROOT_DIRECTORY_ABSOLUTE . DS . 'assets' . DS . 'player' . DS . 'index.html'), true);
$ajax_url = admin_url('admin-ajax.php') . '?action=' . 'player-rpc';
$resource_url = admin_url('admin-ajax.php') . '?action=' . 'resource-rpc';
$siConfig = getSIConfig();

?>

<div class="loadingWrapper" id="loadingWrapper">
        <div class="loading">
            <div class="outer"></div>
            <div class="inner"></div>
        </div>
    </div>

<script type="application/javascript">
    if(location.href.indexOf('#app=manager') ===-1){
        location.href = location.href + '#app=manager';
    }
    var sessionReplayConfig = '<?php echo json_encode($siConfig) ?>';
    var sessionReplay = {
        hasConfig: true,
        baseUrl : '<?php echo esc_url(plugins_url('assets/player/', __FILE__)) ?>',
        replayBase: '<?php echo esc_url(plugins_url('assets/player', __FILE__)) ?>',
        resourceRPC: '<?php echo $resource_url ?>',
        toUrl:function(slug){
                return '<?php echo $ajax_url ?>' + '&view=' + encodeURIComponent(slug);
        },
        config: JSON.parse(sessionReplayConfig)
	}
</script>

<?php

echo '<div id="root" class="sessionReplayRoot"></div>';
echo '<script type="text/javascript" src="' . $XAPP_PLUGIN_URL . '/assets/player/js/bundle.min.js"></script></body>';

// echo $XAPP_PLUGIN_URL;
