### Add pipe plugs 

 Pipe plugs can help to protect both the chair and the floor from getting damaged by adding a soft layer in between.

As it might be tricky to find the perfect fitting for the angled legs, we attached an STL-file for fitiing plugs. So if you have access to a 3D printer you can easily print them yourselves (preferably with recycled filament). The letter on the inside should be aligned as shown in picture 2.
