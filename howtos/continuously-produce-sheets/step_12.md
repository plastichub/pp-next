### Move to long term storage 

 After the sheets are fully cooled, they can be moved into their long term storage. We recommend to let them cool down for a minimum of 12 hours (so the next morning whilst the press is heating up might be a good opportunity to do this). 

When transferring to long term storage, you can remove the wooden strips and stack the sheets ontop of eachother.  Again, if you have one available, place a flat heavy sheet on top of the sheets to keep them weighed down. 

Don't forget to stamp them before you move on! (Check out here to find out How-to: )
If they have any rough edges, you can clean them using either a knife or deburring tool. 
