### Assembly 

 The mould comes in 3 key parts. The female top section of the mould requires no welding and is made of interlocking parts that are bolted together. The male lower section is completely welded. Each piece is also named very specifically to help you order and assemble. These names will also be engraved onto the mould to help you assemble.  

For example AB5x2.
 “A” - First letter defines what part group it belongs to. 
“B” - Second letter defines which part it is in that group. 
“5” - First number defines the thickness of the metal in mm the part is made from. 
“x2” - Specifies the number of pieces required per order to complete the mould.

