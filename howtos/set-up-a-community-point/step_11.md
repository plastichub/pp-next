### Start collaborating 

 Once you’ve made the connections and made a plan how to work together and who will do what, the group can start collaborating.

The goal here is that people know where they can bring their plastic, where to shred plastic, where to melt plastic and where to finally sell and buy the final products.
