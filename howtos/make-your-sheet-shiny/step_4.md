### Learn from your results 

 Each material behaves a bit differently, so it's always a testing and learning which mould material and technique works best for which plastic type.

Here we have two samples:
A sheet out of a mild steel mould. Polishing it afterwards made it smooth and cleaned it from dirt of the mould.
A sheet out of a very smooth stainless steel mould. No polishing was needed as it came out very shiny!

Feel free to try out different mould materials yourself and always compare your results to learn which technique works best for different products and plastic types. :)