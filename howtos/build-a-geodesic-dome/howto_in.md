### Build a geodesic dome 


The Geodesic Dome is a lightweight structure using a minimum of materials. It is constructed by triangular elements, which are structurally rigid and splitting the stress evenly. Usually, it is used for greenhouses or temporary structures.

Step 1-6: Prepare your components (1-2 days)
Step 7-11: Assemble the structure (1-2 hours) 
