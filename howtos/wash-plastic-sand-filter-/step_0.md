### Why a sand filter and how it works 

 There are many types of filters and most of them (at least the simple ones) work with sort of a membrane, which keeps the dirt. The big problem with these is though, that they will eventually clog up and then need to be cleaned or replaced. On top of that, they can be quite expensive and hard to get.

The process of rapid sand filtration uses filter sand as a membrane, which is way easier to get. Dirty water is pumped through a thick layer of sand, which keeps a high amount of contaminants. The advantage of this filter is that the sand is easy to clean through a process called backwashing. Big versions of it are used in public water recycling facilities, but there is also a smaller version used to clean swimming pools, which is more interesting for us.

So there are two options, either buy one or make one. We recommend to first check out what’s available (to save time and maybe even costs), but here we provide a manual for everyone who want or have to build it themselves.