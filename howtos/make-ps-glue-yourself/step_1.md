### Take the right precautions first 

 When making the glue it’s recommendable to work in a well ventilated space, preferably in a fume hood. 
D-Limonene is hazardous as an irritant in cases of eye contact, skin contact, and inhalation, so make sure to use an ABEK mask, safety glasses and gloves in order to prevent irritations.

For safety, before working with D-limonene, read the SDS sheet from the packaging. D-Limonene is flammable in presence of open flames and sparks. It must be kept away from flames at all times, however a mix of. PS and d-limonene is not flammable.