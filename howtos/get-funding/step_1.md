### Fill out the Action Plan 

 The Action Plan is about boiling down your business idea into the most important components, so you can see all of the moving parts interacting together. A common issue we see within our community are people trying to do too many things at once - this tool will help you prioritize! 

You can find the Action Plan in the Download Kit or learn more in the Academy
👉 http://tiny.cc/business-actionplan