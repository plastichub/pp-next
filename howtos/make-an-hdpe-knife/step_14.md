### Final Finishing. 

 HDPE finishes really well straight off of a sharp tool. To really bring out the marble effect, we took a razor blade to 'scrape' the surface of the plastic. This left a great finish and required no more sanding.

To finish the handle we wet sanded up to 2,000 grit with wet and dry paper. We then used a product called Micro Mesh to take it from 2,000 to 20,000 grit which gave it a great finish.