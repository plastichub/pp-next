---
image: /_howto/sheetpress/cartridge_heater/hole/drilling_alternative/IMG_2265.JPG
category: "sheetpress"
title: "Drilling Alternative"
tagline: ""
description: ""
usedin:
  - sheetpress-cell
  - sheetpress
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2264.JPG" _target="_blank">
        <img id="IMG_2264.JPG" src="./IMG_2264.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2265.JPG" _target="_blank">
        <img id="IMG_2265.JPG" src="./IMG_2265.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

