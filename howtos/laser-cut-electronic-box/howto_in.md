### Laser cut electronic box 


The original machines have a metal box where all the electronics are stored in. It’s quite basic and easy to make, especially since you are already working with metal. However, it takes quite some time to build. 

This How-to will show you a simple upgrade.