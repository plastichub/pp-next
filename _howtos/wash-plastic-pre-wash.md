---
image: "/howtos/wash-plastic-pre-wash/howto-washplastic-prewash.jpg"
title: "Wash plastic: Pre-wash"
tagline: ""
description: "Plastic needs to be clean before shredding to make the maintenance of the machine easier. Here we are going to explain how to build an easy pre-washing machine to get rid of the rough dirt.<br /><br />This machine integrates into a system with a sand filter and a washing machine."
keywords: "research,washing"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "research"
- "washing"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Why not to wash everything by hand</h4>
<div class="step-text">
  In a small scale, it can make sense to wash your plastic by hand with a brush and some soap. As soon as we have a bigger amount of dirty plastic though it isn’t economical anymore. It takes a lot of time and uses a lot of water<br />
  <br />
  So integrating a semi-automatic machine with a sand filter helps a lot. The water is constantly cleaned and can be used for a long time. As we don’t work on an industrial level, there is still plenty of manual work involved, but it is still a much more efficient solution.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/jtQ4aNd_Ido6UpU30HiXzlWqjGoFT0TssGDa1Tera4JTP693aNbr71-CjHq0iR3_WVAcx_Ji_j8x2Qf6QKT3FLDTbSgI5lRSWmrjfB5I0eQW-8UxetpwZfKG9yejm4wRItb7yyFd.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/jtQ4aNd_Ido6UpU30HiXzlWqjGoFT0TssGDa1Tera4JTP693aNbr71-CjHq0iR3_WVAcx_Ji_j8x2Qf6QKT3FLDTbSgI5lRSWmrjfB5I0eQW-8UxetpwZfKG9yejm4wRItb7yyFd.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Get your materials</h4>
<div class="step-text">
  For the pre-washing machine, we are going to need:<br />
  - a 120 litre HDPE barrel<br />
  - circa 13 m steel profile (30x30mm)<br />
  - 1120 x 16 mm steel pipe<br />
  - 300 x 30 mm flat steel<br />
  - some wood for the wooden board<br />
  - four casters and fittings according to the sand filter <br />
  - two ball valves<br />
  - and flexible pipes (also according to the sand filter)<br />
  <br />
  - a motor with around 0.5 kW and 1500 r/m (can also be stronger)<br />
  - a frequency inverter fitting to the motor<br />
  <br />
  Optional:<br />
  - another 120 liter HDPE barrel<br />
  - two more ball valves <br />
  - heating element and PID controller
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/iyah_fvH5SiPIOxQccqQeEOmTtEQ8DMcblTDzpZJgZdv8ypXRJS1KgQrsbA81zXYjpXHVrj7QbqDK2UbjdsIdZI8ODKsMB7Rjsq2o3E9Nt1HaGgF-wFeQLBLCKFuR3AhbJvhqwJh.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/iyah_fvH5SiPIOxQccqQeEOmTtEQ8DMcblTDzpZJgZdv8ypXRJS1KgQrsbA81zXYjpXHVrj7QbqDK2UbjdsIdZI8ODKsMB7Rjsq2o3E9Nt1HaGgF-wFeQLBLCKFuR3AhbJvhqwJh.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/0qNLp_kaiqiZXBKrkqTI1d-bS9vlkDKtMIO54fkeyXMHuUx3vSKi96RmPwhsKDFC819JdSNVVCnhTpORNNLuUxfaz0mN-uPfBSxEMarsxAKWknTuGlGEHbeoZgootrm0yGNLF51c.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/0qNLp_kaiqiZXBKrkqTI1d-bS9vlkDKtMIO54fkeyXMHuUx3vSKi96RmPwhsKDFC819JdSNVVCnhTpORNNLuUxfaz0mN-uPfBSxEMarsxAKWknTuGlGEHbeoZgootrm0yGNLF51c.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/CX-S34qsRkEk8x1bqOfH5MEYg8dCAekKUw2ZfTkBU9kt9NrIAN_dVtVK3Tlvo8juozxn-n7GiKpjq-LcgcC1LKnGApGxGdYcqmpFU2in8GqJ6rm245jc9a-DXUa_3e16L9Eaf8_0.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/CX-S34qsRkEk8x1bqOfH5MEYg8dCAekKUw2ZfTkBU9kt9NrIAN_dVtVK3Tlvo8juozxn-n7GiKpjq-LcgcC1LKnGApGxGdYcqmpFU2in8GqJ6rm245jc9a-DXUa_3e16L9Eaf8_0.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare the barrel</h4>
<div class="step-text">
  We start by preparing the barrel.<br />
  <br />
  All you have to do is remove the handles and drill two holes. One at the very top and one at the bottom of the barrel, so you are able to pump out all the water.<br />
  <br />
  Use a 35 mm Forstner drill bit for the holes. Now you can attach the fittings.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/CTnpTMpryrrWd-ok7rUjIzIh4UvUzLWK3tfEg8kbBc91A7eCDtUultFo8cm_jVzo3yGEugawyF3_dqZiLyyQO1Pch_jckm1fxljXg-W1Mh5q-2if1ise53fyY85jSXqz6HPZd5a0.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/CTnpTMpryrrWd-ok7rUjIzIh4UvUzLWK3tfEg8kbBc91A7eCDtUultFo8cm_jVzo3yGEugawyF3_dqZiLyyQO1Pch_jckm1fxljXg-W1Mh5q-2if1ise53fyY85jSXqz6HPZd5a0.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/NMD4lpeDdGdoJ3SJgPnWbNb1Zeg_n3ISrx0YUG9ltIuUC_gR8VHn9APXerCyBYWn-C8vRS9rWxtFJBDsgyt6AJl26er8feTxeOLUXbSKlm-McWr3-J_V1jaMl7RtwlanJK3woUdC.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/NMD4lpeDdGdoJ3SJgPnWbNb1Zeg_n3ISrx0YUG9ltIuUC_gR8VHn9APXerCyBYWn-C8vRS9rWxtFJBDsgyt6AJl26er8feTxeOLUXbSKlm-McWr3-J_V1jaMl7RtwlanJK3woUdC.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/9v0HDkEs6fwYjnNS051CCAGmf-iPFArZRLpRx7z1i7r8zWII0RGFqh0Quy13fhkGrQkoa7pZXsj8RhsrLxtHQkajCMWh6S7yGdPs7T83cmzSMBiJ9ljtUjV8RNcccgPLq2bwu7Mi.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/9v0HDkEs6fwYjnNS051CCAGmf-iPFArZRLpRx7z1i7r8zWII0RGFqh0Quy13fhkGrQkoa7pZXsj8RhsrLxtHQkajCMWh6S7yGdPs7T83cmzSMBiJ9ljtUjV8RNcccgPLq2bwu7Mi.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Build the mobile base</h4>
<div class="step-text">
  To be able to move the barrel (filled with water), we build a solid board with some casters.<br />
  <br />
  Cut wood plates to size, make sure they are strong enough to carry at least 100 kg and attach fitting casters. In the middle we need a hole, so the water outlet has a way to go.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/BJXKPg0Y2J93XQ3Q5E527kWS7LAIgY0VRcBYb4DngzrWjgcR-hlUsr9Ci52vVJhEtEjxSLnaE_mK_Fz8H8npPCSP2DqN5ooJqaTtO9XrNbrx8WmEQY9CE5Wf0OiD1yyfVZFrjVHD.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/BJXKPg0Y2J93XQ3Q5E527kWS7LAIgY0VRcBYb4DngzrWjgcR-hlUsr9Ci52vVJhEtEjxSLnaE_mK_Fz8H8npPCSP2DqN5ooJqaTtO9XrNbrx8WmEQY9CE5Wf0OiD1yyfVZFrjVHD.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Build the frame</h4>
<div class="step-text">
  To build the steel frame is a simple welding job. Cut your steel tubes according to the measurements in the picture. <br />
  <br />
  Drill two 17mm holes above each other in the centre of the frame to add profiles which will keep the mixer shaft in a vertical position. <br />
  To ensure that the frame always stands stable and straight, weld four nuts into the legs and add bolts with which you can adjust the height individually. <br />
  <br />
  Once everything is done, give it a proper paint job, to prevent it from rusting (as it can get very wet).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/ERFQrLA3ZsHd9cmls3hxi60QZf6i7o_D8QOsPtYEFcOHqGCHAyDoJDjJ9ia0L6-amxMfOYmm4DeYjZPXY5avPo_27DqMJ5hprmxQBSa6Eke4B06y7IQ4mUQfy6nQl4ztOP05F-GU.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/ERFQrLA3ZsHd9cmls3hxi60QZf6i7o_D8QOsPtYEFcOHqGCHAyDoJDjJ9ia0L6-amxMfOYmm4DeYjZPXY5avPo_27DqMJ5hprmxQBSa6Eke4B06y7IQ4mUQfy6nQl4ztOP05F-GU.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/rPfmKlbWcDgXaroA_38UhDR5VoMyns0PO2umee2YtRYoWzwRtPYIAcxXRsfBLuKqMGNBeFJzvkROZbdp3FrZPcP2Gf_qYJ28jn763LiWx8E8KmsXJYTaCspVT8cNxfFn-3Ls3PKO.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/rPfmKlbWcDgXaroA_38UhDR5VoMyns0PO2umee2YtRYoWzwRtPYIAcxXRsfBLuKqMGNBeFJzvkROZbdp3FrZPcP2Gf_qYJ28jn763LiWx8E8KmsXJYTaCspVT8cNxfFn-3Ls3PKO.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/nutinplace.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/nutinplace.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Build the mixer</h4>
<div class="step-text">
  The mixer should move the dirty plastic through the water. After some experiments and tests, this seemed to be the most efficient design for us. Short “arms” ensure that the plastic gets mixed without getting stuck.<br />
  <br />
  Weld the metal sheets to to the steel pipe as shown.Drill two ø8mm holes on different heights of the steel pipe, so it can be fixed in two different positions: One to mix and one to move the barrel out underneath it.<br />
  <br />
  You will also need a smaller hole to attach the gear later, but the location depends on how you will connect it. When done, paint it properly (or even better is to use stainless steel, because the friction will remove the paint fast).<br />
  Finally, drill a 20 mm hole in the centre of the lid, so the mixer can enter the barrel.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/Vu6N8TpdFfFLhivcjwgpj9rd8dUJuZsiIQEOIQ1ic1M7P11DKeFjYToLwtpq9fyd5_6hJ1AojIPH2WjWhsVDDXBplGsUhwuEPhSnDRKTR7oE3VN2kzoTWP3JjF9Nge2g14O0op9d.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/Vu6N8TpdFfFLhivcjwgpj9rd8dUJuZsiIQEOIQ1ic1M7P11DKeFjYToLwtpq9fyd5_6hJ1AojIPH2WjWhsVDDXBplGsUhwuEPhSnDRKTR7oE3VN2kzoTWP3JjF9Nge2g14O0op9d.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/agUJB3K_PP2HtKsuL2kTPT9sq0P_2TnpLnfAQwRswi3PMYSmuveYULs2n-XDy498m7y9wS6rCZve9Zb3sT5got07qxJK0_sTUCSG_Y4allwGwhpq4WkHtiU_Neo6Fh8fJndmC2iF.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/agUJB3K_PP2HtKsuL2kTPT9sq0P_2TnpLnfAQwRswi3PMYSmuveYULs2n-XDy498m7y9wS6rCZve9Zb3sT5got07qxJK0_sTUCSG_Y4allwGwhpq4WkHtiU_Neo6Fh8fJndmC2iF.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/H9cGuFYStY67v0QwZ7NIxjzXpRwiqdYT4NDWFzMQjN3QVEKPbo8LxoIe8dTAxti_i-wOFTgX8-4N6tZUgnv--6DycbB-9vIUW1zKkSuCms8iE-ICYu85mtZDyXiJiYc7TYmY0M82.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/H9cGuFYStY67v0QwZ7NIxjzXpRwiqdYT4NDWFzMQjN3QVEKPbo8LxoIe8dTAxti_i-wOFTgX8-4N6tZUgnv--6DycbB-9vIUW1zKkSuCms8iE-ICYu85mtZDyXiJiYc7TYmY0M82.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Attach the motor</h4>
<div class="step-text">
  Attach the motor parallel to the mixer shaft. The position and the way how you do it depends a lot on your motor, but in general it is pretty straight forward.<br />
  <br />
  Once the motor is fixed, connect it with the frequency inverter, so you are able to change the speed later on.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/YE9kVdd4eZIColULLvBzp8YskATC5E09MLpfpJH9XVeU1U7Ucp_blwoWkNnokbBJyUWAGiG8S2Mq2krJRFVXK98VtBaBJHpxc7pquefi7p_HfHjYrw1bbK7z1yj9zDVSiTSva5t5.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/YE9kVdd4eZIColULLvBzp8YskATC5E09MLpfpJH9XVeU1U7Ucp_blwoWkNnokbBJyUWAGiG8S2Mq2krJRFVXK98VtBaBJHpxc7pquefi7p_HfHjYrw1bbK7z1yj9zDVSiTSva5t5.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/jIJptu-DhqfgmDlTOKdgbV0-pvtHi1I1iweGQFQOHjLcwSsubGwEfPcxsqODVx_GD9s7xdnmkEZH-F46I78AYH7OIbRq5_6EmI7GVg8kFd2e-QHOYK1iWwKuQ43oja_xqcYaaDnc.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/jIJptu-DhqfgmDlTOKdgbV0-pvtHi1I1iweGQFQOHjLcwSsubGwEfPcxsqODVx_GD9s7xdnmkEZH-F46I78AYH7OIbRq5_6EmI7GVg8kFd2e-QHOYK1iWwKuQ43oja_xqcYaaDnc.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - The gear</h4>
<div class="step-text">
  The connection between motor and shaft is very simple: Two same sized gears from multiplex - one fixed to the motor, the other one on the shaft which can be lifted up.<br />
  <br />
  The connection to the shaft is made in a very simple way, as you can see on the pictures, but feel free to use a more conventional method.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/ss_m6XDnj4qhIYp5nWIaC9hiKGT8JKYzTenAo_DU63fMeUYXHEkoXFzj9P7Vl16qSx8aimN60Tfk7Viy_ZjrcJpeUQGxjygXdYQc27q_3R9cLxZxMpiOShw1iCLJb399eAIpHwGv.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/ss_m6XDnj4qhIYp5nWIaC9hiKGT8JKYzTenAo_DU63fMeUYXHEkoXFzj9P7Vl16qSx8aimN60Tfk7Viy_ZjrcJpeUQGxjygXdYQc27q_3R9cLxZxMpiOShw1iCLJb399eAIpHwGv.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/0OHlBssLa5TxOxBYRr_1L91Mrxw6Qk7uP-PmfFHXuB5ft7HlyXnAd086mzenFGB32PEdVEdJnfKa2Jr7FcEyeHUfOv6F9lY8Zj_lP_4sgDXJ3OQ7fxe8Tnff3YHVoJbyKG5Pl45N.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/0OHlBssLa5TxOxBYRr_1L91Mrxw6Qk7uP-PmfFHXuB5ft7HlyXnAd086mzenFGB32PEdVEdJnfKa2Jr7FcEyeHUfOv6F9lY8Zj_lP_4sgDXJ3OQ7fxe8Tnff3YHVoJbyKG5Pl45N.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Extra barrel with a heating element</h4>
<div class="step-text">
  Some contaminants, especially oil and fat, are difficult to remove without warm water. Furthermore it’s very useful to have a second barrel to pump in the water from the first barrel if you need to clean it or reach plastic on the bottom. <br />
  <br />
  We prepare this barrel in exactly the same way as in step 3, but add a heating element and a PID controller. Heating elements can come in many forms, for example from deep fryers. You need to find solid, waterproof way to connect it to the barrel.<br />
  <br />
  Afterwards, place the thermometer from the PID in the tank and connect the pipes to the system.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/LmK11PBGLciQK_-UTxB1lcw3JjHS46E1qcqnhBoiwNfkB-n013CYDLnG0phcsA5y8_AV7LC2BK3vkvu7z67GmwDGB7txmYsZSqCcAmX9giM6eoo2LOTSgamwYJ2eStp515MQOMFB.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/LmK11PBGLciQK_-UTxB1lcw3JjHS46E1qcqnhBoiwNfkB-n013CYDLnG0phcsA5y8_AV7LC2BK3vkvu7z67GmwDGB7txmYsZSqCcAmX9giM6eoo2LOTSgamwYJ2eStp515MQOMFB.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/iiT3mYWUPdqa7FdVGs9IC1HXLeuqglmHzcqV8DH1ujTWzHyEOPWnoZWyvZ07r_jRSIwgNoej5Ew7DxA_P6jrrMveOZLXUTB750Wj6gPw9rYR5OuVrA3VTXRvMbh4bKmPflwTBd5Q.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/iiT3mYWUPdqa7FdVGs9IC1HXLeuqglmHzcqV8DH1ujTWzHyEOPWnoZWyvZ07r_jRSIwgNoej5Ew7DxA_P6jrrMveOZLXUTB750Wj6gPw9rYR5OuVrA3VTXRvMbh4bKmPflwTBd5Q.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - How to pre-wash</h4>
<div class="step-text">
  Now it is time to connect all the pipes to the barrels and the sand filter. Fill up with water and you are good to go. Before using the pre-washing machine, we recommend to reduce the size of bigger plastic pieces and to remove the very rough dirt like sauces etc. <br />
  <br />
  Mix the water in the pre-washing machine with laundry powder or soap, turn on the sand filter so there is a constant loop of water splashing at the plastic and let the motor run.<br />
  <br />
  After a few minutes, your plastic should be clean enough to shred. You can experiment with temperature, the amount of soap and the motor speed.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/T9plXdEZmSDAt3M8bL0vCJzbAIgcazIpt8X-zl6t1xaXv6TASRcaTMHmaxUMO9HGXmbF1bj1VyBGTlJiuZJYht8dbbqLQ0DzD-4r-IujdtQDEezbogSnTlSpMcRINs8wTDl8v3Un.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/T9plXdEZmSDAt3M8bL0vCJzbAIgcazIpt8X-zl6t1xaXv6TASRcaTMHmaxUMO9HGXmbF1bj1VyBGTlJiuZJYht8dbbqLQ0DzD-4r-IujdtQDEezbogSnTlSpMcRINs8wTDl8v3Un.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Complete setup</h4>
<div class="step-text">
  Nice one, you&#39;re ready to pre-wash :)<br />
  <br />
  To build the complete setup with sand filter and washing machine, have a look at our How-To&#39;s here:<br />
  <br />
  Wash plastic: Sand filter<br />
  👉 tiny.cc/wash-plastic-sandfilter<br />
  <br />
  Wash plastic: Washing machine<br />
  👉 tiny.cc/plastic-washingmachine
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/wash-plastic-pre-wash/DSCF4779.jpg">
        <img class="step-image" src="/howtos/wash-plastic-pre-wash/DSCF4779.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>