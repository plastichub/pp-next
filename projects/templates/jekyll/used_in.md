
{% if page.usedin %}
    <h4>This is used in</h4>
    <div class="ty-vendor-plans small">
        {% for component in page.usedin %}
            {% for doc in site.machines %}
                  {% if doc.product_id == component %}
                      <div class="ty-grid-list__item_small">
                        <a href="{{ doc.url  | relative_url }}.html" class="link">
                          <span class="image" >
                            <img class="cover_small" src="{{ doc.image }}" alt="" />
                          </span>
                          <header class="major">
                                  {{ doc.title }}
                          </header>
                        </a>
                      </div>
                  {% endif %}
            {% endfor %}
        {% endfor %}
    </div>
{% endif %}

