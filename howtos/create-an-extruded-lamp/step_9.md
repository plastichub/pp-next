### Electronic support 

 Let's make the lamp functional!
First, we make another wooden circle to support the cables and electronics. Drill two guides in the wood to allow the cables to stay in the right place.

Then get the cables through the holes and connect the socket to the wooden support.