<?php
function getSessionId($sessionState)
{
    $keyValuePairs = substr($sessionState, 3);
    $keyValuePairs = explode("=", $keyValuePairs);

    $sessionId = null;
    $length = count($keyValuePairs);
    for ($i = 0; $i + 1 < $length; $i = $i + 2) {
        $key = $keyValuePairs[$i];
        $value = $keyValuePairs[$i + 1];
        if ($key == "srs") {
            $sessionId = $value;
        }
    }
    return $sessionId;
}

function toSID($session)
{
    $sessionIn = urldecode($session);
    if ($sessionIn && strpos($sessionIn, '$') !== false) {
        $session = substr($sessionIn, strpos($session, '$') + 1, strlen($sessionIn));
    } else {
        $session = getSessionId($sessionIn);
    }
    if (!$session && strpos($sessionIn, 'srs') !== false) {
        $session = getSessionId($sessionIn);
    }
    return $session;
}
