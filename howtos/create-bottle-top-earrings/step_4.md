### Press your earrings 

 After the bottle tops are sufficiently melted, put on your respirator mask, and heat resistant gloves. Remove the two tiles from the oven one by one. First, lay down the tile with your melted material on a flat and heat resistant surface, then immediately place the second tile on top of it, with the smooth side facing down.

For pressing, you will need to give a little pressure to the tiles. But not too much, otherwise the material will become too thin, and the bottle tops may spread and fuse together. Be mindful of this and press carefully. 

The plastic will need some time to cool, for this you should leave the two tiles as they are, one on top of the other. If you prepared another batch, now is the time to put it into the oven (step 3). If not, you have a few minutes to make yourself a cup of tea :)
