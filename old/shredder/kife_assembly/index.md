---
image: /_howto/shredder/kife_assembly/IMG_2112.JPG
category: "shredder"
title: "Kife Assembly"
tagline: ""
description: ""
usedin:
  - zoe
  - shredder_v31

---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2106.JPG" _target="_blank">
        <img id="IMG_2106.JPG" src="./IMG_2106.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2107.JPG" _target="_blank">
        <img id="IMG_2107.JPG" src="./IMG_2107.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2108.JPG" _target="_blank">
        <img id="IMG_2108.JPG" src="./IMG_2108.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2109.JPG" _target="_blank">
        <img id="IMG_2109.JPG" src="./IMG_2109.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2110.JPG" _target="_blank">
        <img id="IMG_2110.JPG" src="./IMG_2110.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2111.JPG" _target="_blank">
        <img id="IMG_2111.JPG" src="./IMG_2111.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2112.JPG" _target="_blank">
        <img id="IMG_2112.JPG" src="./IMG_2112.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

