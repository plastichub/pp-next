### Machine Shop services 

 As part of a Machine Shop you can offer different services to your customers:

Assembling: Putting together machines using high precision measurement tools, a small lathe or milling machine for adjustments.

Welding: Making the machines’ frames. A semi-professional MIG/MAG and a dedicated area will do the job.

Electrician: Cabling and installing the electric box. To adapt the machine to specific configurations, troubleshooting and to ensure full safety a professional level is required.

Mould Making: Creating precise aluminium moulds with a CNC machine.

Machining: Offering professional-level lathe and milling for manufacturing shredder axis and the main extrusion parts.