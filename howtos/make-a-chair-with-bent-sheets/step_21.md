### Stamp your product 

 Make sure to stamp every plastic part with its plastic type before it leaves your workspace!
You can use a simply heated metal wire or specially engraved stamps from the bazar. For the stamps, heat them up and clamp them to your plastic part.

Related links:
👉 tiny.cc/wire-stamp
👉 bazar.preciousplastic.com
