# Hydraulic Power Pack

## Overview

An open source hydraulic power pack intended to supply a single double-acting ram to actuate future hydraulic Precious Plastic injection machines.

Spec: (Suggested)

Power output (Horsepower, kW): 3hp - 2.2kW

Motor RPM: 1500

Flow Rate: ~30L/MIN

Pressure: 3000psi


## Existing Product

The initial design will be based on the Little Champ hydraulic power unit by [Continental Hydraulics UK](http://www.continentalhydraulics.co.uk):

![Little Champ 10G](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/reference/little-champ/Little-Champ-web.jpg)

![Little Champ Kit](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/reference/little-champ/champ-kit-clean-web.jpg)

[Reference Section](https://gitlab.com/plastichub/products/tree/master/projects/hydraulic-power-pack/reference) with other similar models


## Parts Overview


#### Primary Components:

- [Motor](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/motor/ReadMe.md): 2.2kW (3HP) 230V/400V 3HP 4 Pole AC Motor 182TC B14A Flange (56 C face)

- [Coupling](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/coupling/ReadMe.md): : PM90

- [Pump](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/pump/ReadMe.md)

- [Bell Housing](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/bell-housing/ReadMe.md): 6028 Pump Motor Adapter 56C to SAE A

- [Oil Reservoir](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/reservoir/ReadMe.md): 10 Gallons 38.4 litres fill level W400xL400xH387mm internal; 2.6mm mild steel plate

- [Filtration](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/filtration/ReadMe.md)

- [Pressure Relief Valve](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/pressure-relief-valve/ReadMe.md)

- [Control Valve](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/valves/ReadMe.md): CETOP 5 (NG 10) DOUBLE ACTING SOLENOID OPERATED DIRECTIONAL CONTROL VALVE 220V

- [Power Supply](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/power-supply/ReadMe.md): 3HP Invertor 220/480V

- Pressure Guage

- Flow Meter

- Intermediary Pipework and Fittings

- [Manifold](https://gitlab.com/plastichub/products/blob/master/projects/hydraulic-power-pack/documents/manifold/ReadMe.md)
: Valve Manifold, D03 Single Station, 4 SAE

- Mounting Plate: 5mm Steel

- Chassis

- Enclosure


#### Optional/Additional:

- Heat exchanger (heater/cooler)

- Offline/Kidney Loop Filtration

- Pressure Switch

- Float/level Switch

- Temperature Sensor

- Clogged Filter Bypass Indicator

- Quick Disconnect Inspection Points

- Lock-Out-Tag-Out Shutoff Valve/Switches for safety


