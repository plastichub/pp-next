### Is this for you? 

 For the Shredder Workspace you will have to be quite technical as you have to understand how the shredder machine works, ideally know how to maintain it and, as a plus, know how to fix it when it needs a bit of extra love. 

Shredder workspaces also need to connect to as many Collection Points as possible to ensure a constant flow of plastic waste so you also kind of need to like to deal with people. 

And last but not least, a certain degree of organisation is also always good to have.
