### Explore the possibilities! 

 So now you have had an insight from our experience how to achieve these two textures, but keep in mind that the beauty of using recycled plastic is that the outcome of each product is unique in its own way each product will come out differently. It really takes some time and testing to get used to the sensitivities of these methods so be sure to test and get stuck in!

We also would advise to keep documenting your results. If you altered the temperature, speed, material or the size of your mould. All these variables when changed around will affect the outcome. Labelling your material outcomes and collating a material library can also really help to have a reference when designing.


Get experimenting!


