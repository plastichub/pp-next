### Document it 

 The design should conclude in drawings and a part list. Think on the step by step of the machining to identify the critical procedures (for example, when parts should be machined together to get an accurate fitting; or, how they will be fixed to the machine) and register all that information in the drawings.

Do the part list for all the raw materials to work as a shopping list. Include dimension and quantity. (Remember to buy the raw parts a bit longer than the final parts. Between 5 and 10mm will be enough to fix the parts in the machines and adjust to the final measure).
