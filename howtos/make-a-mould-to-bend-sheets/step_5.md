### Cutting the cross-sections 

 Cut the drawings as you planned before. Use a ruler and a sharp knife and work precisely. Then take the previously cut plywood parts and laminate them with the drawings. If there are several drawings for one part, starting from the center. Apply spray mount on the back of one sheet, align it according to the instructions and sweep from the middle outwards.
Drill holes for the jigsaw to dive in and cut along the lines. Keep in mind what you want to keep and what’s going to be off-cut. Smoothen the edges with sandpaper.