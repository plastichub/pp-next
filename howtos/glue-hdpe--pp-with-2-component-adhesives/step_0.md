### Stay safe 

 While handling with 2-component adhesives it’s safe to use an ABEK mask and safety glasses in order to prevent nose, eyes and throat from irritations. Also gloves are needed to avoid any direct contact with the adhesives. Preferably it’s better to work in an open and well ventilated space to avoid gases to accumulate.

Recommended safety equipment:
- ABEK mask
- safety glasses
- gloves