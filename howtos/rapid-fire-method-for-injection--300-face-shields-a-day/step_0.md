### Extrude hot munition 

 Instead of filling up the injection machine with flakes, use your extruder to produce hot munition. Remove the nozzle, in order to extrude thick stripes. Instantly take them and fill your injection barrel with them. 

In the extruder, the plastic is mixed well and heated properly, so it is instantly processable with the injection machine.