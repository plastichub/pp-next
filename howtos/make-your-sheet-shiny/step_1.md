### Treat your mould 

 For a high quality product, a high quality mould is the key!
In order to create a clean and shiny sheet, you need a very smooth and shiny mould. It might take time to get there but it will pay off in the end: The shinier the mould is,the shinier will be the result of your sheets (and less finishing work to do).

Mould material:
For a shiny plastic sheet we recommend to use a stainless steel mould, as it’s a good material to polish. But take care because it’s also easy to scratch and fairly more expensive than other metals.

Mould treatment:
We suggest to use silicon oil as mould release in order to protect the mould during the process and help demoulding the material. Also pay attention to clean the mould after every cycle in order to extend the mould life, and to keep a good output quality.