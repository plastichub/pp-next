### Safety Add-ons 

 
Workshops with different kinds of people are our daily business. That is why we care a lot about safety!
To ensure an easy and understandable handling, a good way is to use our color system.
- Green: Everybody can touch/use it.
- Orange: The team can touch/use it.
- Red: Keep your body away from it, it's f***ing dangerous!
In addition to the color system we use common warning signs and tips and tricks written on the machine.

Furthermore we build safety add-ons, such as the belt protection and the shaft protection.

If you have ideas how to make the machine safer and better, feel free to share it with us and the community!
