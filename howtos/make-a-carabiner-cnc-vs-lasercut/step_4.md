### CNC - Inject and finish 

 After injecting you should be able to take the carabiners from the mould just by twisting them.

Then you just need to remove the leftovers from the twisting process with a cutter.

Now your carabiners are ready to use!