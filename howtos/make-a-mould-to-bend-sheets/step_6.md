### Assembling the frames 

 Screw together as you planned before. In most of the materials you should pre-drill to avoid cracking! Hence it’s a lot faster to use two cordless drills at the same time. Don’t put the wedges to the positive mould, you will need them in step 9.
As well attach some guides like aluminium L-bar to the corners in order to align the two halves more easily later. While doing so, place both halves on top of each other to make sure the whole frame is square.
