### Placing threaded bar 

 Now we need to attach the threaded bar to our base structure. This threaded bar needs to be longer than planned wall height. You may need to weld multiple pieces together and if you have a bolt in the concrete, you will need to weld the threaded bar to your bolt.

For the timber, version place a nut and washer on both sides clamping the threaded bar in place.
