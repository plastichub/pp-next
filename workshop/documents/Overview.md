# Overview & Outline

## 1.Aspects
### 1.1 Precious Plastic - the island - The status quo & forecasts
### 1.2 Green policies for plastic recycling and machine production
### 1.3 Customers and their profiles (PP, and outside of the island)
### 1.4 Entering the public market, global and local business
### 1.5 Plastic and other myths about recycling
### 1.6 True opensource & licenses

## 2. Production
### 2.1 Product design & development (tools & templates)
### 2.2 Just-in-time production & waste management (tools & templates)
### 2.3 The inventory (templates)
### 2.4 Machines, extensions & templates
### 2.5 Quality testing, standards & ISOs (tools & templates)
### 2.6 Human resources (tools, templates)
### 2.7 Documentation (tools, templates) of processes, for later analytics
### 2.8 Education (books, tools & templates)
### 2.9 Infrastructure (tools, services & templates, accounting, eg: Kanban)
### 2.10 KPIs -how to create and analyse them

## 3. Marketing, Networking & the community
### 3.1 Tools & templates for acquisition,  social, newsletter, blog,... etc...
### 3.2 Networking (tools & templates)
### 3.3 Branding
