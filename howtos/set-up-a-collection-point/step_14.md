### Set up your space 

 Super, you’ve got a nice space!

Follow our video to get ideas how to fully set up your Collection Point with all the additional tools, bags, furniture and posters needed to make your space functional and nice.

👉 http://tiny.cc/space-collection