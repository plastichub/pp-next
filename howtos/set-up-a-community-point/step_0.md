### Role in the universe 

 Community Points are the glue of the Precious Plastic Universe. They help strengthening the collaboration between the existing recycling spaces as well as involving more and more people from the general public.

Moreover they’re closely connected to Collection Points as they can serve as great help in finding new people to collect their plastic. 

Community Points should be run as a collective but if you’re alone in your area you can also start solo. 