### Dust Cyclone 

 Dust cyclones remove over 99% of dust and debris from the airstream, containing it before it ever reaches the vacuum. We've been running our Dust Extractor for quiet a while, and are yet to spot even a speck of plastic in our vacuum.

The cyclone will include a cutting template and instructions, along with nuts/bolts and connection pieces. We picked one up from eBay for around $40AUD: https://bit.ly/3iKR4e6

If you're keen to save some cash or prefer a DIY option, check out this tutorial: https://bit.ly/33FxUSP