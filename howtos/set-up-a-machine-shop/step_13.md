### Get the machines 

 Cool, now you have a space and team. 

It’s time to procure your equipment, in the tools list from the Download Kit, we provide the list of basics tools and machines you will need for starting a Machine Shop.

We recommend investing in good quality machines. If you want to reduce your investment, keep an eye on second-hand markets, you  might find proper machinery for decent prices. They will generally keep their value, last longer, and will be easier to repair due to parts availability, so the investment will eventually pay off.

 If you want to run a specialised Machine Shop or expand its capabilities, you might need to upgrade it depending on your needs.