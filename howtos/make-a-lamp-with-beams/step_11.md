### Metal bar 

 To cover the LED stripe and hang the beam from the ceiling, we designed a metal bar that holds the pieces together. 
We made drawings for lasercutting, for a better finish.

You will need to engrave the central lines to stick the LED stripe exactly in the middle, the sidelines to facilitate the bending, cut 5mm holes on each side where you will let the light cable pass through and cut 2.5mm grove which will allow to slot the metal cable through.

If you lasercut we recommend to send more than one piece as the unit price will get cheaper. 
You can find the CAD file attached above.