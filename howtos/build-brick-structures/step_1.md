### Fixed or mobile unit? 

 Recycled bricks are fairly lightweight, so in some cases, you may want to build a mobile/moveable object. Before starting you will need to decide this. If you choose a mobile structure you can skip Steps 2-6.

Learn here how to make the brick 👉 tiny.cc/make-extruded-bricks