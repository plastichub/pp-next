### Shredder 2.1 


We’ve gathered a lot of feedback on the shredder from our community and made a small upgrade which makes it a bit easier to build, swap the mesh and chop plastic.