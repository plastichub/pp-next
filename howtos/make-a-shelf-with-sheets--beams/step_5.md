### Tension Wire 

 Time to apply our two tension wires for some more stability. Have a look at the drawing from the download files as a reference.

Lift the shelf from the floor, and pass one the first tension wire through A, B and C. Then connect A to C.

Take the second tension wire and pass it through 1, 2 and 3. Then connect 1 to 3.