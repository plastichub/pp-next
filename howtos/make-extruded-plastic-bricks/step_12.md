### Weld the base 

 Using part CA5  align parts CE5 and CF5, clamp and weld. Avoid welding CA5 at this point. 

You should be left with a perfect frame to attach the two humps too. 
