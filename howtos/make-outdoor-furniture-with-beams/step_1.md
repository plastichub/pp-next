### Prepare your pieces 

 Choose a furniture design (you can just copy an existing piece of furniture which is made out of wood) and prepare the pieces you need to build it.

In most furniture it's enough to use straight beams, but you can also bend the sticks so you have round elements.