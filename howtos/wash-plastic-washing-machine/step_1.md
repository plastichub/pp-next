### Get your materials 

 For the washing machine, we are going to need:
- a 120 liter HDPE barrel
- a washing machine with a rather big drum capacity
- a sand filter 👉 tiny.cc/wash-plastic-sandfilter
- 1” 24V solenoid valve
- flexible pipes (also according to the sand filter)
- relay 240V
- 240V to 24VAC Trafo
- 3 Switches
- cables etc.
