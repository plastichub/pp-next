### Choose your stencil technique 

 You will need a stencil to create your own tags. The stencil will help you speed up the process. You can make it in many ways, such as using a cardboard box and a knife, a laser-cut machine on wood, a CNC machine on steel or plastic, or even a 3D printer using PLA. Each technique requires different materials, and therefore different costs. 

The best technique is the one you have most easily accessed, is economical and will produce a long-lasting stencil. Cardboard boxes with a cutter are the cheapest option, but it will take a long time to make and the stencil will not last long. We recommend the laser cut on plywood. Ask the nearest FabLab in your area, they can help you :).
