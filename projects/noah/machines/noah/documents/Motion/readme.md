# Motion system for Noah

Noah's motion system is built around Scaff Rollers and their sister product Sidewinders.

## Scaff Rollers

Scaff Rollers are a U-groove guide roller wheel with a deep groove bearing, and an extended inner ring which accomodates two set screws. The dimensions of the roller allow standard diameter scaffold tubes to be used as both the axle and the rail on which it travels. 

![scaff roller](https://img1.wsimg.com/isteam/ip/05044fa2-20ea-4882-a146-3bf0c0ed2864/v17.2%20badboy.jpg/:/)

For more information see www.scaff-rollers.com

## Sidewinders

Movement is created by large motors - servos or hybrid servo steppers - which drive a system of steel ropes and pulley wheels.

The Sidewinder idling sheave is based on the same design as the Scaff Roller, using scaffold tube as the axle. Its spiral profile allows steel rope to be wrapped around up to 3 times to reduce slippage, while preventing binding. 

![sidewinder-1.1.jpg](./sidewinder-1.1.jpg)

The Sidewinder Driver uses the same principle, but has a tapered bore recess to accept a locking ["Fenner"-type taper bushing](https://www.youtube.com/watch?time_continue=1&v=DC35qxDWr2o&feature=emb_logo), in order to enable mounting onto a driveshaft.

![sidewinder-driver.jpg](./sidewinder-driver.jpg)

The Sidewinder system uses steel rope 6mm diameter. 

## Motors

Noah's motors need to produce very high torque, and accurate rotary positioning. 

Conventional desktop 3d printing uses stepper motors. However, these alone will not be suitable for Noah, since they work on a position only basis are not compatable with real-time feedback compensation. Noah's system needs to allow the controller to command movement until a chosen sensor position is reached. This is a fully integrated servo system.

### The debate between servos and steppers
[SEE HERE](https://www.cnccookbook.com/ultimate-cnc-mini-mill-steppers-servos-closed-loop-open-loop/) for more information on why Noah needs a closed loop, [HERE](http://www.legacycncwoodworking.com/stepper-vs-servo-motors/) for a summary of steppers vs. servos, and [HERE](https://www.linearmotiontips.com/how-does-closed-loop-stepper-control-work/) to see why the best candidate may be hybrid servo-steppers.

Current best candidate: the 20Nm [110HCE221](https://www.rattm-motor.com/eu-delivery-110nema42-closed-loop-servo-motor-20nm2880oz-in-3-phase-hybrid-stepper-motor-3-phase-step-servo-driver-cnc-p2047978.html) Nema42 hybrid servo stepper with matching driver HSS2260. 

![110HCE221](https://ae01.alicdn.com/kf/HTB18aJgXwLD8KJjSszeq6yGRpXaP/EU-Delivery-110-Nema42-Closed-loop-Servo-motor-20N-m-2880oz-in-3-Phase-Hybrid-stepper.jpg)

[PDF user manual+spec](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwii7uSdneHlAhUiShUIHb2ZCr0QFjAAegQIBBAC&url=https%3A%2F%2Fwww.jbcnc.se%2Fsv%2Fget_file%3Fproduct_id%3D815%26file_id%3D264&usg=AOvVaw1MtyK0cGlzAcC3FKt-V4g4)

~£260 each plus delivery




Next step up in torque: 50Nm [130BYGH350D](https://www.fasttobuy.com/3-phase-nema-52-stepper-motor-drive-kit-69a-50nm-for-engraving-machine_p36145.html) Nema52 with matching driver 3MA2280-10A

![130BYGH350D stepper](https://www.fasttobuy.com/images/v/201803/15204899150.jpg)

[PDF user manual+spec](https://docs.google.com/viewer?a=v&pid=sites&srcid=c3VtZnUuY29tfHdlYnxneDo1NTU2M2RlZGU5NGU0MjIw)

Manufacturer seems to be [Sumfu](http://www.sumfu.com/)

~£320 each apparently including delivery on [AliExpress](https://www.aliexpress.com/i/32950277549.html)

The Z axis will require a stepper motor and worm drive powering a winch assembly, something like this:

![worm drive winch](https://img.alicdn.com/imgextra/i3/726718251/O1CN01oOkSZd2Ap0U8af2h1_!!0-item_pic.jpg)

## Power

Power for motors will be delivered by invertors. The ideal power source would of course eventually be a large solar panel array, but currently development assemblies of Noah will be grid-tied or running from a generator. 


## ToDos

 1. Research motors- requirements dictated by weight calculations
 2. Manufacture complete sets of Scaff Rollers and Side winders for testing; ship to developers
 
