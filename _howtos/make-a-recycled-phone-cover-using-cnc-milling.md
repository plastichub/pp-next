---
image: "/howtos/make-a-recycled-phone-cover-using-cnc-milling/cnc_01.jpg"
title: "Make a recycled phone cover using CNC milling"
tagline: ""
description: "Jerry will show you an advanced technique to create a CNC mould to produce phone cases from recycled plastic."
keywords: "injection,product,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "injection"
- "product"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather everything you need</h4>
<div class="step-text">
  Before you start get all your gear ready:<br />
  <br />
  - Respirator mask<br />
  - Big block of aluminium<br />
  - 3D Design software<br />
  - CNC machine <br />
  - Injection machine<br />
  - Plastic Type Stamp
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-recycled-phone-cover-using-cnc-milling/cnc_02.png">
        <img class="step-image" src="/howtos/make-a-recycled-phone-cover-using-cnc-milling/cnc_02.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-recycled-phone-cover-using-cnc-milling/cnc_03.png">
        <img class="step-image" src="/howtos/make-a-recycled-phone-cover-using-cnc-milling/cnc_03.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Watch this video tutorial</h4>
<div class="step-text">
  Watch this video tutorial to learn step-by-step how to make the 3D mould, CNC it and inject a recycled phone case using the Precious Plastic Injection machines.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Done & Share</h4>
<div class="step-text">
  After watching this video you should understand the basics of making a mould using CNC milling. You can reproduce the phone case or try to make other products. We’re looking forward to seeing what you can come up with. Make sure to share back your new creations so the community can learn from you. Tag #preciousplastic on social media, create a new how-to or send us an email.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-recycled-phone-cover-using-cnc-milling/cnc_04.jpg">
        <img class="step-image" src="/howtos/make-a-recycled-phone-cover-using-cnc-milling/cnc_04.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-recycled-phone-cover-using-cnc-milling/Screenshot 2020-05-12 at 22.58.55.png">
        <img class="step-image" src="/howtos/make-a-recycled-phone-cover-using-cnc-milling/Screenshot 2020-05-12 at 22.58.55.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>