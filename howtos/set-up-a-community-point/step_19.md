### Be part of the global community 

 Participate and interact in this community to learn and grow together.

Make sure people in your local network have their pins on the map so they can be found more easily.

Maybe help them set up, sell on the Precious Plastic Bazar and make it more visible and accessible to buy their machines, products and services.
