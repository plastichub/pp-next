### Setting up the Sheets 

 1. Set your machine to 200c and let it heat up to max temperature 
2. Set timer for 960 (16min) 

You will need to play around with the height of the machine and your mold as we "cook" the mold for 1 run (16min) then tighten it, then "cook" 3 more times. 

3. Once at Max Temp close the lid as tight as you can. (might still be loose or take a few times to close but try to get this as tight as you can without spilling the mold) 

 