### Export 

 -There are many file formats that you can export to such as SVG, DXF & PDF
-SVG & DXF formats have their own buttons that you can click in the top menu
-If you wish to export to PDF just right click anywhere on the page and click "Export PDF"
-Save it somewhere you will find it later