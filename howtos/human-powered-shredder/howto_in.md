### Human powered shredder 


The Precious Plastic machines are open source, which means you can adapt it to your requirements. So if you don't have or don't want to use a motor, there are other more 'humane' ways to power the shredder!