### 3D Model 

 To begin, you need a 3D model, which you can create using a 3D modeling software such as Fusion360. Molds are also available from various designers online. The Daily Spork mold is available for download above.