---
id: Idefix
layout: home
---

# PET grinder

Variants : Addon, Standalone, Asterix, IdeFix mini

This build can be applied to PP v3 shredders. It only needs a few steel bars and custom made grinder knifes

**brief** : General overview for a PET grinder

**document scope** : client, vendor, manufacturer

**version** : 0.4

**status** : design & prototyping

**license** : open-source

<hr/>

![](./overview.jpg)

## Files / Components

[Fusion360](./cad) | [3D Online Preview](https://a360.co/2wlHFaB)

[SolidWorks](./cad/solidworks)

[PET Printhead](https://mahor.xyz/producto/v4-pellet-extruder-24v/)

<hr/>

## Constraints

- 6 mobile knifes | 2 stationary kifes - each adjustable
- v3 shredderbox * 0.3
- UFCL 205 bearings (shaft size : 30mm)
- dual pulley
- small footprint
- different sieves
- 220V/380V, 2Kw for now
- 400 - 700 RPM
- minimal height (eg: to enable it as addon)
- wheels
- Hopper with flake guide, door (flakes shoot really high)

## References

- [Short video rev.3](https://www.youtube.com/watch?v=ElOnSA7pS18&feature=youtu.be)

## Related

- [Noah umbrella project](https://gitlab.com/plastichub/noah/blob/master/machines/Noah.md)
- [PP and filament & 3D-print projects](https://precious-plastic.org/home/library/articles/filament/)
- [PP Wiki - components](https://precious-plastic.org/home/library/components/)
- [PP Forum - PET print](https://davehakkens.nl/community/forums/topic/3d-printer-for-pet-particles-no-filament-ever/)
- [Video - 3D-seed PET Printer](https://www.youtube.com/watch?v=EbxNa3WLpjA)
- [Youtube channel : large scale PET grinder & shredder](https://www.youtube.com/watch?v=3xWvHz9JjjU)

## Designs

![](./media/research/s-l1600.jpg) ([Source](https://www.ebay.de/itm/Getecha-GRS-182-A-1-38-Granulator-Beistellmuhle-Schneidmuhle-40-106/324191772811?hash=item4b7b55e08b:g:~e8AAOSwFxpe3iCh&autorefresh=true))
