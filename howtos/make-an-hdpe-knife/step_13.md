### Final Shaping on the Blade 

 We refined the shape of the plastic blade using the belt grinder and spindle sander. We also draw on our bevel line and sanded to this using the belt grinder.

This was tricky as the plastic turns very 'paper-y' as it gets towards a sharp point. So we didn't try to make it too thin otherwise it could have potentially split on the very edge.