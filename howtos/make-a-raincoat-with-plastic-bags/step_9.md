### Sew the outer and lining together 

 Using your cut plastic sheets, and the matching cotton pieces, sew the two templates together so that the outer and lining are attached to one another. This will help the process of further working with the material, and make it easier to create your raincoat. 