### Contribute to open source projects 


Getting started with contributing to open source projects may seem a little daunting so we wrote a quick guide. This focuses on GitHub based projects as it is a core bit of software for collaborating on open source projects.