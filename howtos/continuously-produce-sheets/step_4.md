### Add your plastic 

 Weigh your plastic - you can use a bucket on a bathroom scale, or hang one from a travel scale. To make it easy we found a bucket that matches the amount of plastic. 
Tip: For 1sqm sheet it's 1kg of plastic (for most types) per 1mm of thickness +5%. 

Once weighed, pour the plastic into the mould frame. It’s important to spread the plastic as evenly as possible to reduce warping. To make sure it’s level, place two beams on each side of the mould, then rest a beam between them and drag it back and forth to flatten the surface. 

Finish preparing the mould by adding the second mould sheet. 
Check your timetable for when the sheet is due to go into the sheetpress.