### Test it out! 

 We asked our Patrons what they would like to see us cut with the knife and the 2 overwhelming responses were CAKE and A TOMATO! So that's exactly what we did! The blade actually cut them both surprisingly well, however it did struggle to get through the sugary crust on the banana cake (it tasted great though!)