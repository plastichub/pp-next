### Maintenance 

 As you run your Shredder Workspace it is crucial that you maintain the shredder machine in order to prevent failures. Find out here how to best maintain the shredder. 