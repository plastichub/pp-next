### Punch holes for the attachment 

 The best for the attachment is an elastic band for clothes but we didn't have any so made with a rope like in the example of https://menorplastic.com/tutorial-para-fabricarte-tu-protector-facial-casero/
however to avoid making knots and for an easy attachment you can simply punch a set of holes 
A square wood chisel size 10 is perfect for that.
You have to make 3 sets of 2 holes and one set of 3 holes at one end (could be two if you use rope)