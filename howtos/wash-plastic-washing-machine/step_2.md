### Removing everything we don’t need 

 We pump water straight into the drum with our own pump, which means we will not need any of the mixers and can remove everything until the connection to the drum. The process depends on your machine but should be doable with basic tools.
