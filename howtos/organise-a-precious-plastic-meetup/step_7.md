### Prepare food 

 Food connects people in a very positive way, it’s a small thing which makes everyone happy!

Maybe you can prepare something yourself, or talk in the group that everyone brings a bit or maybe even organise the meeting at a time that you can end it with a shared meal. (Also a good thing to make people come.)
