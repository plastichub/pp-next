### Add cut outs for the ruler 

 -To add a rectangle for the outline of the ruler click the Rectangle icon in the left menu
-You can now draw it around the rulers and select the specific height and width from the top menu
-You may need to adjust the alignment of the rectangles so that the rulers are visible
-Select the item and click the "Lower selection to bottom" icon from the top of the screen
-Repeat for the outer border of the mould