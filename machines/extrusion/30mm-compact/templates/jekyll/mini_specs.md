<span id="specs" style="padding: 16px">
    <table>
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td> Extruder
                </td>
            </tr>        
            <tr>
                <td>Version:
                </td>
                <td> 4.3
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Tested
                </td>
            </tr>
            <tr>
            <td> License
                </td>
                <td><a href="https://ohwr.org/cernohl">CERN Open Source Hardware License</a>
            </td>
            </tr>
            <tr>
            <td> Authors </td>
                <td>
                 [PlasticHub S.L.](${author_link}) |
                 [PrecisousPlastic - Eindhoven](${author_link_pp}) |
                 [Peter Bas](https://www.instagram.com/peterbasschelling/) |
                 [David Basetti - 3D Seed](https://www.facebook.com/david.bassetti.79)
                </td>
            </tr>
        </tbody>
    </table>
</span>
