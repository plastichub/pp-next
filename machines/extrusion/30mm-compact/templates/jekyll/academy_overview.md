| Specification    |     |
|----------|-------------|
| Type   |     Single Screw   |
| Price new material |  +/- €1500 |
| Weight (inc frame) |   110 kg   |
| Dimension   | 1500 x 600 x 1550 mm|
| Power (W) | 5 kW|
| Voltage | 400V or 220V |
| Amperage | 16A |
| Input Flake Size  | Small  |
| Screw diameter | 30mm |
| Length of screw (mm) | 790 mm |
| Effective screw length | 600 mm |
| Rated Motor Power | 3 kW |
| Motor Type   |    (check the build section for more details)   |
| - Rated Motor output Torque |  109 Nm |
| - Rated Motor output speed |   263 RPM   |
| - Max. Motor and Inverter power   | 3 kW|
| - Recommended motor shaft   | 30 mm|
| - Heating zones   | 3 |
| - Heating power: max.   | 2 kW|

### Pros & cons

#### Pros

The extruder is one of the fastest and most efficient to melt plastic among Precious Plastic machines. It is a safe and reliable machine amongst our machine’s family!

The Extruder Pro screw is designed to work with multiple types of plastics. Which  will make you capable of working with different kind of plastic in small batches. Also this machine is conceived to be much more polyvalent than other industrial machines, allowing you to extrude into moulds, shapes, different nozzles…

#### Cons

This machine requires a higher skill set of machining amongst Precious Plastic machines. It also requires a large motor and a specialized screw that make it a relatively expensive machine compare to other Precious Plastic machines.

### Basic VS Pro

Compared to smaller version of extruder, this machine:

- Has a larger screw which gives a higher output
- Optimized screw design brings an homogeneous melt for a better quality plastic
- An improved bearing design to support axial and radial extrusion forces
- Is made to run for full days
- Easier assembly and servicing
- Overall stronger and tested components
- A safer and easier to build electronic box
- Has a larger insulated hopper allowing better fluidity

### Input & output

A variety of plastic types will be effective in this extruder. Each plastic type has its own properties and behaviours (flexible, hard, liquid etc.). Materials with a wide melting temperature range are easiest to work with. We suggest starting with PP, HDPE, LDPE or PS since they require less temperature precision. We do not recommend trying polymers with smaller melting temperature windows (such as PET), but if you decide to do so, please take safety precautions.

You can set the temperature using the controllers on the electronic box. The heating elements are labeled and wired into three groups (nozzle, barrel and feeding). We recommend to use the temperatures from the table below as a general rule of thumb.  This is to help make sure the plastic is fully melted right before it comes out.

### Heating Temperatures

| Plastic Type | Feeding Zone (C°) | Barrel Zone (C°) | Nozzle Zone (C°) |
|--------------|-------------------|------------------|------------------|
| PP           | 190               | 200              | 200              |
| PS           | 200               | 210              | 210              |
| HDPE         | 190               | 200              | 200              |
| LDPE         | 190               | 200              | 200              |

### Throughout

- Beam Mould (2000  mm x 40 mm x 40 mm): 8 Min
- Little Lego Brick Mould: 4 Min
- Big Lego Brick Mould: 6 Min
- Skateboard Mould: 20 Min
