### sheetmaker part 2 

 •	 When the plates are up to temperature and the prep sheet is inserted the operator needs to select the press function. This will start the automated pressing and the timer. Every few minutes the system activates to maintain pressure. A notification sound is played in advance to warn operators. 
•	While the sheet maker is pressing options for further steps can be chosen : automatic open (to 10cm) when the timer finishes or stay closed. In both cases the heating will be stopped. 
•	A sound indicates the timer had run out.
