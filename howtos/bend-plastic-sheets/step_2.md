### Heating timings 

 Once the machine reaches the desired temperature it’s time to place the plate between the two heating elements. In our tests we found that PS usually takes less than 6-10 min, while HDPE takes around 30-40 min for the same sheet thickness (10mm). 

Since PS is way quicker to bend than HDPE or PP we think that  bending machines with a heated sword on top that goes inside the material could work better.