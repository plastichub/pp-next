**Product Resources**

- [Wiki](${product_wiki})
- [Wiring](${product_wiring})
- [Download](${download})
- [Product Page](${product_page})
- [3D Preview](${abs_url}/${product_rel}resources/edrawings.html)

${product_howtos}
