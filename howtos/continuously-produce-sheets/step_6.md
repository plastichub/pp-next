### Use the melt time to prepare 

 Whilst the sheet is melting, you can use the time to prepare the place where the sheet will be stored. 

The sheets will be placed directly on strips of wood so that each side remains exposed to the uniform ambient temperature. Each successive sheet will be placed on top of the previous with more strips of wood perpendicular to the last. All of these strips of wood should have the same thickness to prevent uneven storage. 

You can also prepare the second sheet on the prep table (Same as steps 4-5). In preparation for the next sheet make sure that the cooling press is open. (It opens the same way as the sheetpress).
