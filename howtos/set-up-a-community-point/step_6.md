### Create a Community Point pin! 

 If there is already a Community Point near you on the Precious Plastic Map, get in touch with the group and and see how you can join and help. Better to collaborate than to do double work. 

If there is no Community Point in your area yet, it’s time to create one. Create an account on the Community Platform and put a pin on the map. This way people can find and contact you.

👉 community.preciousplastic.com/sign-up

Have a look at these profile guidelines to get help with setting up your profile.
👉 tiny.cc/profile-guidelines