### Machine Limits 

 There are some physical limitations which have to be considered when using the injection machine.

Precious Plastic Injection Machine v3

Volume: 150cm³
Pressure: 44bar

The volume determines the maximum volume of the cavity together with gate, runners and sprue you can fill.
The injection pressure determines how fine your details can be and what is the minimum achievable wall thickness.

Depending on your machine build those values might vary according to the calculation in the image.