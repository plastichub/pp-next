### Make a quick release for the extrusion machine 


The extrusion machine has the capacity to extrude a lot of plastic very quickly, but what if you wanted to make smaller or bigger items that can’t be screwed on to the barrel. This is why we developed the low tech sliding quick release for all your extrusion needs. 