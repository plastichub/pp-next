### Adding roof frame structure (top beam) 

 The reason we kept the columns longer than the rest of the structure is to allow us to attach a beam to support the roof. The length of this beam should be the same as the space between the columns at the floor (assuming your structure is square).

For this step ensure your beam is level and 2 screws are placed diagonally on each column to secure it. 
