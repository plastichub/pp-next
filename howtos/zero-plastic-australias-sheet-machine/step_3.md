### "Cooking" the Sheets 

 Once you have closed the lid on the mold the timer will start and you have 16min to save the world...wait that isnt it. 

You have started the process of making a sheet. After the first 16min of cooking you will need to open the machine and tighten it.

1. Tighten the machine (normally 3 full turns will do the trick, however this is something you will need to work around) 
2. Close the machine and cook again for another 16min
3. After the 2nd cook is completed Flip the mold (as the machine only heats from one side) 
Flipping the mold front to back will give you the best results
4. Cook for 2 more times (16min + 16min) 

In total you should of cooked the mold 4 times (2 times on each side) 
