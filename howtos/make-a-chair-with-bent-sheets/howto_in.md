### Make a chair with bent sheets 


The technique of bending plastic sheets offers great applications in furniture design. In this How-to, we will explain how to build a chair from bent PS sheets and a steel frame.

It’s split up in two parts:
Step 1-7: Making the jigs and moulds
Step 8-27: Building the chair
