### Slots in the mould 

 To make it easier to attach and detach the clamps from the mould, you can make slots in your mould.

You can include them in the cnc/laser cut file of your mould or you can also cut them yourself with an angle grinder.

The slots should be around 6 mm wide to make it easy to slide the clamp in and out. (If for some reason it becomes wider, use bigger washers in between to fix the clamps).
