### Run workshops 

 As you become more comfortable and knowledgeable working with plastic and the machines you could start to give educational workshops in your community.

You can run workshops at your workspace, events, conferences and conventions showing how Precious Plastic recycling works. 