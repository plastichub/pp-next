<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Utilities\\' => array($vendorDir . '/galbar/jsonpath/src/Skyscanner'),
    'Pimple' => array($vendorDir . '/pimple/pimple/src'),
    'JsonPath\\' => array($vendorDir . '/galbar/jsonpath/src/Skyscanner'),
);
