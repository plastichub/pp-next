### Silicone Mould for Soft Plastics 


This tutorial will show you how to cast an object in silicone to use with soft plastics. Casting with silicone will allow you to remake intricate, delicate or unusual objects in plastic. 

You can get as experimental as you wish casting simples items such as a vase or something as extreme as a tree! Don’t worry you will be able to use this mould over and over again.
