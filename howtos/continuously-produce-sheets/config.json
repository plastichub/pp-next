{
  "creatorCountry": "fr",
  "_deleted": false,
  "slug": "continuously-produce-sheets",
  "_createdBy": "precious-plastic",
  "_id": "MxDlq9pALA3FLzZaHHiX",
  "tags": [
    "sheetpress",
    "starterkit",
    "research"
  ],
  "_created": "2021-02-23T18:16:58.850Z",
  "files": [],
  "_modified": "2021-03-03T22:25:35.713Z",
  "title": "Continuously Produce Sheets",
  "time": "< 1 day",
  "description": "How to continuously produce sheets in a full “work day\" or “work week” scenario using the Precious Plastic Sheetpress system to achieve a semi-mass-production scale output. \n\nFilled with lots of optimization and efficiently tips and tricks we have learned over the years!",
  "difficulty_level": "Medium",
  "cover_image": {
    "size": 184892,
    "updated": "2021-02-23T18:15:33.846Z",
    "timeCreated": "2021-02-23T18:15:33.846Z",
    "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/10b.JPG",
    "type": "image/jpeg",
    "name": "10b.JPG",
    "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F10b.JPG?alt=media&token=125447eb-0702-4c57-a6a8-4fd727fa37da",
    "contentType": "image/jpeg"
  },
  "steps": [
    {
      "text": "First, check if you have the right safety equipment: You will need gloves, a mask, safety glasses and ventilation. \n\nAs well as a sheetpress you will also need: a cooling press, prep table and sliding tools. \n\nTo continuously produce sheets you will need a minimum of three pairs of mould sheets and one frame for each sheet you plan to make per day. It is a good idea to have one spare set of moulds in case any get damaged. The best beams to use for the mould frames are T beams as they are strong, light and cut the edge of the sheet nicely. We used 3mm Aluminium sheets for the mould as they are strong, light, conduct heat quickly and do not stick to the plastic. ",
      "_animationKey": "unique1",
      "title": "Check your setup",
      "images": [
        {
          "timeCreated": "2021-02-23T18:15:37.129Z",
          "size": 74156,
          "updated": "2021-02-23T18:15:37.129Z",
          "type": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/1a.png",
          "name": "1a.png",
          "contentType": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F1a.png?alt=media&token=d013bf91-25ea-42ec-815f-b361818d00f2"
        },
        {
          "type": "image/jpeg",
          "contentType": "image/jpeg",
          "timeCreated": "2021-02-23T18:15:41.070Z",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/1b-a.JPG",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F1b-a.JPG?alt=media&token=498f929d-6523-42ad-afa1-b7991b271b9d",
          "name": "1b-a.JPG",
          "updated": "2021-02-23T18:15:41.070Z",
          "size": 172477
        },
        {
          "type": "image/jpeg",
          "name": "1c.JPG",
          "size": 209054,
          "updated": "2021-02-23T18:15:41.348Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F1c.JPG?alt=media&token=486a4fde-7cec-4e4a-97d5-1a1f19cad59f",
          "contentType": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/1c.JPG",
          "timeCreated": "2021-02-23T18:15:41.348Z"
        }
      ]
    },
    {
      "images": [
        {
          "size": 75029,
          "name": "2a.png",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/2a.png",
          "contentType": "image/jpeg",
          "type": "image/jpeg",
          "updated": "2021-02-23T18:15:43.842Z",
          "timeCreated": "2021-02-23T18:15:43.842Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F2a.png?alt=media&token=6dd34f84-f1a2-4a48-ad77-1659891d29a9"
        },
        {
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/2b.JPG",
          "size": 89955,
          "contentType": "image/jpeg",
          "type": "image/jpeg",
          "timeCreated": "2021-02-23T18:15:44.439Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F2b.JPG?alt=media&token=e446fc9a-1932-4435-b7d9-9319598a5e4e",
          "name": "2b.JPG",
          "updated": "2021-02-23T18:15:44.439Z"
        }
      ],
      "text": "The heating and cooling time are typically the same. \n\nYou can offset each of the three transitions (1. Load in to sheetpress, 2. transfer into cooling press 3. Unload, demould and store) by half the cooling time (In this example 40 mins / 2 = 20 mins) to give you two nice big windows of time. You can use one of the windows to demould the sheet and one of the windows to prepare the next sheet. \n\nTo make sure you don't miss any of the steps you can use a simple kitchen timer as a reminder.",
      "_animationKey": "unique2",
      "title": "Write a timetable"
    },
    {
      "text": "One way to make sure you don't forget ventilation is to attach it to the same switch as the outlet for the sheetpress so it's impossible to turn on the sheetpress without the ventilation being on. \n\nTurn on the sheet press 45mins before your first sheet is due to go in, with the pressing plates closed, to allow it to heat up. Set the PID controller to match the type of plastic you are melting, if you are not sure you can check our “Melting temperatures” poster in the download kit or on the academy. \n\nNow that the press is on, you should wear a mask whilst in the space.",
      "title": "Turn on your system",
      "_animationKey": "unique3",
      "images": [
        {
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F3a.JPG?alt=media&token=26130705-a591-4c8c-ab9a-f77b80da1d77",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/3a.JPG",
          "contentType": "image/jpeg",
          "updated": "2021-02-23T18:15:46.950Z",
          "name": "3a.JPG",
          "type": "image/jpeg",
          "timeCreated": "2021-02-23T18:15:46.950Z",
          "size": 120853
        },
        {
          "size": 101337,
          "updated": "2021-02-23T18:15:48.870Z",
          "name": "3b.JPG",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F3b.JPG?alt=media&token=988502c4-eb65-4e28-9760-e1b676423a26",
          "contentType": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/3b.JPG",
          "timeCreated": "2021-02-23T18:15:48.870Z",
          "type": "image/jpeg"
        },
        {
          "contentType": "image/jpeg",
          "updated": "2021-02-23T18:15:48.990Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F3c.JPG?alt=media&token=8d66330d-c3d0-45fe-a98a-5a040c95fa18",
          "size": 92998,
          "name": "3c.JPG",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/3c.JPG",
          "timeCreated": "2021-02-23T18:15:48.990Z",
          "type": "image/jpeg"
        }
      ]
    },
    {
      "images": [
        {
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F4a.JPG?alt=media&token=e7d963b0-3ee5-4990-a2a9-1a425b2caebb",
          "size": 101980,
          "updated": "2021-02-23T18:15:51.201Z",
          "contentType": "image/jpeg",
          "name": "4a.JPG",
          "timeCreated": "2021-02-23T18:15:51.201Z",
          "type": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/4a.JPG"
        },
        {
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/4b.JPG",
          "size": 156482,
          "updated": "2021-02-23T18:15:54.740Z",
          "contentType": "image/jpeg",
          "timeCreated": "2021-02-23T18:15:54.740Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F4b.JPG?alt=media&token=810083c6-e06d-4d8b-b20a-ef2832e12e0e",
          "name": "4b.JPG",
          "type": "image/jpeg"
        },
        {
          "contentType": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F4c%20(2).JPG?alt=media&token=817f6a30-83ad-4173-b000-b632da80b003",
          "updated": "2021-02-23T18:15:53.854Z",
          "name": "4c (2).JPG",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/4c (2).JPG",
          "timeCreated": "2021-02-23T18:15:53.854Z",
          "type": "image/jpeg",
          "size": 160126
        }
      ],
      "title": "Prepare the mould",
      "text": "Place one mould sheet on to the prep table. You can leave the other mould sheet flat on the ground with the side that will contact the plastic facing up. \n\nApply a light layer of silicone to the mould sheets. We can highly recommend using a paint roller for this. We keep the paint roller in an airtight box to keep it free of dust. Try and find a sponge roller like in the picture, not one that has hair as they will break off and stick to the mould. \n\nPlace the mould frame in the middle of the mould sheet on the prep table. ",
      "_animationKey": "uniquejpddv4"
    },
    {
      "text": "Weigh your plastic - you can use a bucket on a bathroom scale, or hang one from a travel scale. To make it easy we found a bucket that matches the amount of plastic. \nTip: For 1sqm sheet it's 1kg of plastic (for most types) per 1mm of thickness +5%. \n\nOnce weighed, pour the plastic into the mould frame. It’s important to spread the plastic as evenly as possible to reduce warping. To make sure it’s level, place two beams on each side of the mould, then rest a beam between them and drag it back and forth to flatten the surface. \n\nFinish preparing the mould by adding the second mould sheet. \nCheck your timetable for when the sheet is due to go into the sheetpress.",
      "_animationKey": "unique9wqwew",
      "images": [
        {
          "timeCreated": "2021-02-23T18:15:57.741Z",
          "updated": "2021-02-23T18:15:57.741Z",
          "type": "image/jpeg",
          "contentType": "image/jpeg",
          "name": "5a.JPG",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/5a.JPG",
          "size": 146157,
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F5a.JPG?alt=media&token=1b328f28-87a1-4ee7-8216-69fb62456dc8"
        },
        {
          "name": "5b.JPG",
          "size": 153025,
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/5b.JPG",
          "timeCreated": "2021-02-23T18:15:59.231Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F5b.JPG?alt=media&token=a6680c51-b351-45c0-b717-ebc78b4a022e",
          "contentType": "image/jpeg",
          "updated": "2021-02-23T18:15:59.231Z",
          "type": "image/jpeg"
        },
        {
          "size": 194399,
          "type": "image/jpeg",
          "contentType": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/5c.JPG",
          "updated": "2021-02-23T18:16:00.750Z",
          "name": "5c.JPG",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F5c.JPG?alt=media&token=23a2ef43-3ab3-46a6-9a4c-aaaf1c2b3328",
          "timeCreated": "2021-02-23T18:16:00.750Z"
        }
      ],
      "title": "Add your plastic"
    },
    {
      "images": [
        {
          "size": 140433,
          "type": "image/jpeg",
          "name": "6a 2.jpg",
          "updated": "2021-02-23T18:16:03.251Z",
          "contentType": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F6a%202.jpg?alt=media&token=8fedf988-57e8-4c17-a956-3f4057e8e154",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/6a 2.jpg",
          "timeCreated": "2021-02-23T18:16:03.251Z"
        },
        {
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/6b.JPG",
          "timeCreated": "2021-02-23T18:16:06.030Z",
          "type": "image/jpeg",
          "name": "6b.JPG",
          "contentType": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F6b.JPG?alt=media&token=2a943ba8-ee7c-4fc1-b074-74726c9c1ed3",
          "updated": "2021-02-23T18:16:06.030Z",
          "size": 185239
        },
        {
          "type": "image/jpeg",
          "timeCreated": "2021-02-23T18:16:05.002Z",
          "name": "6c.JPG",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/6c.JPG",
          "contentType": "image/jpeg",
          "updated": "2021-02-23T18:16:05.002Z",
          "size": 104822,
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F6c.JPG?alt=media&token=04afb593-9e85-4cfb-ba00-adf9c24cc6a4"
        }
      ],
      "_animationKey": "unique4munz",
      "text": "Open the sheetpress by releasing the jack. Most jacks release valve which opens using the pumping rod. \n\nUsing the sliding tool, transfer the mould into the sheetpress. It’s important to slide it straight otherwise it will catch on the side frame.\n\nNow close the sheetpress (here we used pneumatic jacks with an air compressor to make it easy and fast).\nWatch the spring to tell when the press is closed, if it's fully compressed, stop pressing. \n\nYou should check the spring every 10 minutes, pressing when needed. Make sure the mould stays in the middle. Check your timetable for when the sheet has to be transferred. ",
      "title": "Press the sheet"
    },
    {
      "_animationKey": "unique48yuy8",
      "text": "Whilst the sheet is melting, you can use the time to prepare the place where the sheet will be stored. \n\nThe sheets will be placed directly on strips of wood so that each side remains exposed to the uniform ambient temperature. Each successive sheet will be placed on top of the previous with more strips of wood perpendicular to the last. All of these strips of wood should have the same thickness to prevent uneven storage. \n\nYou can also prepare the second sheet on the prep table (Same as steps 4-5). In preparation for the next sheet make sure that the cooling press is open. (It opens the same way as the sheetpress).\n",
      "title": "Use the melt time to prepare",
      "images": [
        {
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F7a.JPG?alt=media&token=2b618e4b-158e-49ec-b875-195f47129fb6",
          "updated": "2021-02-23T18:16:08.835Z",
          "name": "7a.JPG",
          "type": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/7a.JPG",
          "contentType": "image/jpeg",
          "timeCreated": "2021-02-23T18:16:08.835Z",
          "size": 129157
        },
        {
          "size": 150639,
          "type": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/7b.JPG",
          "timeCreated": "2021-02-23T18:16:14.330Z",
          "updated": "2021-02-23T18:16:14.330Z",
          "contentType": "image/jpeg",
          "name": "7b.JPG",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F7b.JPG?alt=media&token=0db23369-fb24-4ff2-bf99-871a2ec663ad"
        },
        {
          "updated": "2021-02-23T18:16:13.511Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F7c.JPG?alt=media&token=6ed044a7-1984-4ef6-b9e9-b32f04a20db2",
          "name": "7c.JPG",
          "timeCreated": "2021-02-23T18:16:13.511Z",
          "size": 184919,
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/7c.JPG",
          "type": "image/jpeg",
          "contentType": "image/jpeg"
        }
      ]
    },
    {
      "title": "Transfer the sheets",
      "text": "Two minutes before the mould is due to be transferred from the sheetpress to the cooling press, apply pressure using the jack one last time, fully. During this two minutes scrape off any overflow from the mould to keep the transfer process clean. \n\nOnce the melting time has completed, open the sheetpress. Using the sliding tool slide the sheet from the sheetpress into the cooling press. \nMake sure to wear gloves. If you need to pull it from the other side you can use pliers to grip the edge of the bottom mould sheet allowing it to be moved. Do not pull the top mould sheet as it can open the mould. \n\nClose the cooling. Once the coldpress is fully closed, close the sheetpress to keep the heat.",
      "images": [
        {
          "updated": "2021-02-23T18:16:17.705Z",
          "type": "image/jpeg",
          "name": "8a.JPG",
          "size": 140218,
          "contentType": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F8a.JPG?alt=media&token=ac65e589-953c-42d1-b7ee-4f58556d0717",
          "timeCreated": "2021-02-23T18:16:17.705Z",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/8a.JPG"
        },
        {
          "updated": "2021-02-23T18:16:20.589Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F8b.JPG?alt=media&token=6c42607e-a6d5-4340-a796-d671ff28db59",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/8b.JPG",
          "size": 130721,
          "timeCreated": "2021-02-23T18:16:20.589Z",
          "name": "8b.JPG",
          "type": "image/jpeg",
          "contentType": "image/jpeg"
        },
        {
          "name": "9c (2).JPG",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F9c%20(2).JPG?alt=media&token=af4caa4e-7983-4579-b6d9-98265741e91d",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/9c (2).JPG",
          "updated": "2021-02-23T18:16:21.221Z",
          "contentType": "image/jpeg",
          "timeCreated": "2021-02-23T18:16:21.221Z",
          "type": "image/jpeg",
          "size": 129043
        }
      ],
      "_animationKey": "uniqueqrkuv"
    },
    {
      "images": [
        {
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F8c.JPG?alt=media&token=d4159880-a331-4a72-b13a-0d0e8b68aebe",
          "type": "image/jpeg",
          "contentType": "image/jpeg",
          "updated": "2021-02-23T18:16:24.557Z",
          "size": 123453,
          "name": "8c.JPG",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/8c.JPG",
          "timeCreated": "2021-02-23T18:16:24.557Z"
        },
        {
          "timeCreated": "2021-02-23T18:16:27.808Z",
          "updated": "2021-02-23T18:16:27.808Z",
          "type": "image/jpeg",
          "size": 86628,
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F9b.png?alt=media&token=977923d0-a5ac-48b6-934e-a84aee62246f",
          "name": "9b.png",
          "contentType": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/9b.png"
        },
        {
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F9c.JPG?alt=media&token=afe854fb-ae36-49e8-b995-460ba79474f0",
          "updated": "2021-02-23T18:16:29.184Z",
          "timeCreated": "2021-02-23T18:16:29.184Z",
          "size": 202971,
          "type": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/9c.JPG",
          "name": "9c.JPG",
          "contentType": "image/jpeg"
        }
      ],
      "text": "Check your timetable for when the next sheet is due to go into the sheetpress. \nOpen the sheetpress and then using the sliding tool, transfer the mould into the sheetpress and close the sheetpress (same as in step 6). \n\nCheck your timetable for when it is time to unload from the cooling press, demould and store the first sheet. You can prepare by moving the prep table in front of the cooling press.",
      "title": "Transfer the next sheet",
      "_animationKey": "unique8pwlqh"
    },
    {
      "text": "Once the cooling time has completed, open the cooling press and slide the mould onto the prep table. \n\nLift the top mould sheet off the mould and place it to one side. It should lift off without any resistance but if it's stuck, you may need to use a bit of force. Try not to bend it! If you lever it, make sure to use something that won't scratch the surface of the mould sheet (like a plastic beam or the overflow from a previous sheet). \n\nGently try and remove the sheet from the bottom plate. If it has not stuck, it should slide freely on the mould sheet. If it has, use the same method as above to gently remove it. If it is stuck inside the frame you can leave it inside until full cooled. ",
      "title": "Unload and demould the sheet",
      "_animationKey": "uniquez5fcrf",
      "images": [
        {
          "contentType": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/10a.JPG",
          "name": "10a.JPG",
          "updated": "2021-02-23T18:16:36.125Z",
          "type": "image/jpeg",
          "size": 151745,
          "timeCreated": "2021-02-23T18:16:36.125Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F10a.JPG?alt=media&token=4301607e-6e0b-4d72-99aa-1e47bb1a435f"
        },
        {
          "size": 184892,
          "updated": "2021-02-23T18:16:32.449Z",
          "timeCreated": "2021-02-23T18:16:32.449Z",
          "name": "10b.JPG",
          "contentType": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/10b.JPG",
          "type": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F10b.JPG?alt=media&token=125447eb-0702-4c57-a6a8-4fd727fa37da"
        },
        {
          "contentType": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F10c.JPG?alt=media&token=cf056bf8-0990-4411-b7e8-3a7a51de66f3",
          "timeCreated": "2021-02-23T18:16:37.632Z",
          "updated": "2021-02-23T18:16:37.632Z",
          "name": "10c.JPG",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/10c.JPG",
          "size": 212434,
          "type": "image/jpeg"
        }
      ]
    },
    {
      "title": "Store the sheet",
      "_animationKey": "uniqueukgmbg",
      "images": [
        {
          "name": "11a.JPG",
          "contentType": "image/jpeg",
          "type": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F11a.JPG?alt=media&token=fd0b8c8a-6084-42e0-b3b3-6eba66e8c206",
          "updated": "2021-02-23T18:16:41.973Z",
          "timeCreated": "2021-02-23T18:16:41.973Z",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/11a.JPG",
          "size": 215183
        },
        {
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/11b.JPG",
          "contentType": "image/jpeg",
          "size": 211115,
          "type": "image/jpeg",
          "updated": "2021-02-23T18:16:47.249Z",
          "timeCreated": "2021-02-23T18:16:47.249Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F11b.JPG?alt=media&token=fd3cbb49-acc6-48f7-b23e-510a4f3d3c16",
          "name": "11b.JPG"
        },
        {
          "size": 165594,
          "contentType": "image/jpeg",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F11c.JPG?alt=media&token=59d50d5c-6d28-4b12-96b0-3fac86eda07e",
          "name": "11c.JPG",
          "timeCreated": "2021-02-23T18:16:44.954Z",
          "updated": "2021-02-23T18:16:44.954Z",
          "type": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/11c.JPG"
        }
      ],
      "text": "Lift the sheet and lay it on top of the flat wooden strips you prepared earlier. \nIt is important to be very gentle with the sheet as you move it, any bends or twists at this stage increase the chances of it to warp later. \n\nPlace the next row of wooden strips on top of the sheet perpendicular to the last. \n\nIf you have one available, place a flat heavy sheet on top of the strips to keep them weighed down - in our case we used a spare heating plate.\n\nCheck your timetable for when the next sheet has to be transferred to the cooling press. Once it has been transferred (Same as step 9), move the prep table back in front of the sheetpress ready to prepare the next sheet."
    },
    {
      "title": "Repeat! ",
      "text": "You can now repeat steps 4-11 until your day is finished. ",
      "_animationKey": "uniquevwen6k",
      "images": [
        {
          "timeCreated": "2021-02-23T18:16:50.917Z",
          "type": "image/jpeg",
          "contentType": "image/jpeg",
          "updated": "2021-02-23T18:16:50.917Z",
          "size": 167046,
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F12a.JPG?alt=media&token=97264e5d-f260-49df-8f19-e0efbf9f52f2",
          "name": "12a.JPG",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/12a.JPG"
        }
      ]
    },
    {
      "title": "Move to long term storage",
      "_animationKey": "uniquezuoke",
      "images": [
        {
          "size": 255621,
          "updated": "2021-02-23T18:16:58.207Z",
          "timeCreated": "2021-02-23T18:16:58.207Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F13a.JPG?alt=media&token=7275ecb4-bd76-4254-8f68-df2df55c9483",
          "name": "13a.JPG",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/13a.JPG",
          "contentType": "image/jpeg",
          "type": "image/jpeg"
        },
        {
          "size": 195002,
          "timeCreated": "2021-02-23T18:16:56.893Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F13b.JPG?alt=media&token=81f51410-098c-448b-926b-725c2b19e2ff",
          "contentType": "image/jpeg",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/13b.JPG",
          "name": "13b.JPG",
          "updated": "2021-02-23T18:16:56.893Z",
          "type": "image/jpeg"
        },
        {
          "timeCreated": "2021-02-23T18:16:58.377Z",
          "downloadUrl": "https://firebasestorage.googleapis.com/v0/b/onearmyworld.appspot.com/o/uploads%2Fhowtos%2FMxDlq9pALA3FLzZaHHiX%2F13c.JPG?alt=media&token=6cf9d849-a8b5-4920-a12b-ef8b76c9d1ce",
          "fullPath": "uploads/howtos/MxDlq9pALA3FLzZaHHiX/13c.JPG",
          "type": "image/jpeg",
          "name": "13c.JPG",
          "contentType": "image/jpeg",
          "updated": "2021-02-23T18:16:58.377Z",
          "size": 143972
        }
      ],
      "text": "After the sheets are fully cooled, they can be moved into their long term storage. We recommend to let them cool down for a minimum of 12 hours (so the next morning whilst the press is heating up might be a good opportunity to do this). \n\nWhen transferring to long term storage, you can remove the wooden strips and stack the sheets ontop of eachother.  Again, if you have one available, place a flat heavy sheet on top of the sheets to keep them weighed down. \n\nDon't forget to stamp them before you move on! (Check out here to find out How-to: )\nIf they have any rough edges, you can clean them using either a knife or deburring tool. \n"
    }
  ],
  "moderation": "draft"
}