### Stay safe 

 When talking about safety we are only referring to precautions about working specifically with plastic, as it’s our thing. We assume that working with the jigsaw needs a certain level of expertise so please take all the precautions related with how the tools work.

When cutting with the jigsaw there is danger of overheating and melting the plastic. If this happens some bad fumes could be released. 

Special attention on plastics like PS and PVC! So in order to work safer make sure to use a gas mask with ABEK filters to prevent inhaling possible toxic fumes. An easy way to identify them is to check if the filter has four colour lines (brown, grey, yellow & green).