---
image: "/howtos/zero-plastic-australias-sheet-machine/20200319_170513.jpg"
title: "Zero Plastic Australia's Sheet Machine"
tagline: ""
description: "A Simple, Cheap and Easy way to make sheets of recycled plastic (30cm x 25cm x 1cm)<br /><br />After having a Heart attack i realised that if i don&#39;t share this knowledge it wont help anyone, all i ask is that you help spread the word and to give Zero Plastics Australia a like and to #zeroplasticsaustralia with any work you make from these sheets."
keywords: "starterkit,HDPE,research,sheetpress"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "starterkit"
- "HDPE"
- "research"
- "sheetpress"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/zero-plastics-australia-">zero-plastics-australia-</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Sheet Press Machine and molds</h4>
<div class="step-text">
  First you will need to buy yourself a Heat Press machine, i have found that these can come in many different sizes however this &quot;how-to&quot; will be for the 38cm2 Machine which can be purchased via the link - <a href="https://www.ebay.com.au/itm/Brand-New-38x38-High-Pressure-Heat-Press-Machine-T-shirt-Transfer-/282662703440<br/>">https://www.ebay.com.au/itm/Brand-New-38x38-High-Pressure-Heat-Press-Machine-T-shirt-Transfer-/282662703440<br /></a>
  <br />
  You will also need:<br />
  2x Aluminium Sheets (40cm2 and 3mm-5mm Thick)<br />
  1x 30cm x 25cm x 1cm Aluminium Mold (internal measurements)<br />
  <br />
  You can change the size of this, however a 33cm internal size sheet should be the max you want to make with this machine, as any larger and the edges will not melt.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200529_141228.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200529_141228.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200529_140412.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200529_140412.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200529_140512 (1).jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200529_140512 (1).jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Setting up your Mold</h4>
<div class="step-text">
  We will be using HDPE to make these sheets (however have made sheets using PP and LDPE)<br />
  <br />
  1. Add 1 Aluminium sheet to your heat press.<br />
  2. Add Mold. <br />
  3. Fill Mold with 800 grams of HDPE shredded plastic.<br />
  4. Add the other Aluminium sheet. <br />
  <br />
  If you have a mold release i would recommend added it to the sheets and mold.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200529_140433 (1).jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200529_140433 (1).jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200529_142529.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200529_142529.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200602_130154.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200602_130154.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Setting up the Sheets</h4>
<div class="step-text">
  1. Set your machine to 200c and let it heat up to max temperature <br />
  2. Set timer for 960 (16min) <br />
  <br />
  You will need to play around with the height of the machine and your mold as we &quot;cook&quot; the mold for 1 run (16min) then tighten it, then &quot;cook&quot; 3 more times. <br />
  <br />
  3. Once at Max Temp close the lid as tight as you can. (might still be loose or take a few times to close but try to get this as tight as you can without spilling the mold)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200529_142524.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200529_142524.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - "Cooking" the Sheets</h4>
<div class="step-text">
  Once you have closed the lid on the mold the timer will start and you have 16min to save the world...wait that isnt it. <br />
  <br />
  You have started the process of making a sheet. After the first 16min of cooking you will need to open the machine and tighten it.<br />
  <br />
  1. Tighten the machine (normally 3 full turns will do the trick, however this is something you will need to work around) <br />
  2. Close the machine and cook again for another 16min<br />
  3. After the 2nd cook is completed Flip the mold (as the machine only heats from one side) <br />
  Flipping the mold front to back will give you the best results<br />
  4. Cook for 2 more times (16min + 16min) <br />
  <br />
  In total you should of cooked the mold 4 times (2 times on each side)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200602_130058.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200602_130058.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Cooling the Sheets</h4>
<div class="step-text">
  IMMEDIATELY Once the last cook is complete grab your mold and put it HOT SIDE to the ground.<br />
  <br />
  This is super important and will save your sheets from warping. <br />
  <br />
  1. HOT SIDE to cool concrete ground. <br />
  2. Add weight onto the top of your mold (the more the better)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200529_153628.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200529_153628.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200529_153728.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200529_153728.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - We're Done! </h4>
<div class="step-text">
  Now wait about 30min the longer the better but once the mold has cooled you can open it up and will have a perfect sheet. <br />
  <br />
  You can use a utility knife to cut the sheet out of the mold if you havent used any mold release.<br />
  <br />
  If the sheet is still hot, flip it over and add the weight back on top again for another 30min. <br />
  <br />
  However that it is! you should now be looking at your finished sheet! yay! go you! <br />
  <br />
  I will try to upload a video on all of this shortly
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200602_120214.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200602_120214.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200319_165802.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200319_165802.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/20200319_172422.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/20200319_172422.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - FAQs</h4>
<div class="step-text">
  You might have a few other questions with this as it is a big process but once you work it out it is very easy to do with hardly any labor time needed when making the sheet. <br />
  <br />
  1.How to adjust the height of the machine? this will come in the manual with the Heat press but it is just the screw at the top of the machine. <br />
  2. can you use different plastics? Yes but i have only been collecting HDPE. please share the run time and temp if you use another type of plastic <br />
  3. What can you use these sheets for? have a look at the photos attached or visit our social pages for further updates Zero Plastics Australia<br />
  <br />
  If you need any other questions please email us at zero.plastics.australia@gmail.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/IMG_20200523_113844_550.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/IMG_20200523_113844_550.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/IMG_20200528_174354_029.jpg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/IMG_20200528_174354_029.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/zero-plastic-australias-sheet-machine/received_1999586570184921.jpeg">
        <img class="step-image" src="/howtos/zero-plastic-australias-sheet-machine/received_1999586570184921.jpeg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>