---
image: "/howtos/make-extruded-plastic-bricks/Brick Lean-howto.jpg"
title: "Make extruded plastic bricks"
tagline: ""
description: "Recycled plastic has the potential to replace more conventional and wasteful building materials such as hollow blocks. In this How-to, you will learn how to assemble the mould for the extruded plastic brick, ready for production.<br /><br />This brick was developed with the Extrusion Pro machine.<br />Learn here how to build it:"
keywords: "mould,extrusion,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "mould"
- "extrusion"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Download templates to send for fabrication</h4>
<div class="step-text">
  Download the laser cut kit. Then send for fabrication at your local laser cutting service. Note there are three bricks, one full brick and one two-third brick and a one-third brick. All of these are required to build a complete wall. <br />
  <br />
  Before sending to downloaded dwg files to your metal laser cutting service ensure they understand that your drawings are in MM.<br />
  <br />
  Each brick has tolerances included, but may be specific to your supplier. Before ordering, check that the tolerances conform. In this brick, we use a tolerance of 0.5mm.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/Screenshot.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/Screenshot.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Delivery of laser cut parts</h4>
<div class="step-text">
  Upon delivery, you should receive various pieces with varying thicknesses. It is good practice to check your parts immediately (compare with drawing) in case any errors have been made.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0116.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0116.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Preparation of parts</h4>
<div class="step-text">
  Before assembling your mould for welding, all rough edges created by the laser cutting will need to be removed so the mould can be assembled tightly. To do this we used a handheld sander however this can easily be done with a Dremel or by hand with a file. Take extra care not to damage the sharp corners of the metal.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0524.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0524.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0528.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0528.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0530.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0530.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Assembly</h4>
<div class="step-text">
  The mould comes in 3 key parts. The female top section of the mould requires no welding and is made of interlocking parts that are bolted together. The male lower section is completely welded. Each piece is also named very specifically to help you order and assemble. These names will also be engraved onto the mould to help you assemble. <br />
  <br />
  For example AB5x2.<br />
  “A” - First letter defines what part group it belongs to. <br />
  “B” - Second letter defines which part it is in that group. <br />
  “5” - First number defines the thickness of the metal in mm the part is made from. <br />
  “x2” - Specifies the number of pieces required per order to complete the mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/MALE.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/MALE.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/FEMALE.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/FEMALE.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/TOP.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/TOP.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Assembly of the top section (Part A)</h4>
<div class="step-text">
  Each corner requires 1 bolt and 3 nuts. The bolt should be 45-50 mm long, the nuts are 10mm. Place 6 bolts through the top plate (AD5) and add an additional two nuts to each. This should be followed by the interlocking parts (AB5 &amp; AC5) followed by the bottom plate (AA5). The ideal distance of the two plates (AA5 &amp; AD5) is 20 mm.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0652.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0652.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/TOP.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/TOP.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0605.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0605.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Put sides together (Part B)</h4>
<div class="step-text">
  It is easiest to weld the supporting ribs (BB5, BC5, BE5 &amp; BF5) to the 4 side plates before assembling the sides. To do this place the main plate on the welding table and clamp the ribs individually as you weld. Ensuring the main plate remains flat and does not warp. You will need to use (BG5) to ensure even spacing at the top and bottom. <br />
  <br />
  Repeat these steps for the other 3 sides.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0729.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0729.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0710.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0710.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0735.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0735.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Internal welding</h4>
<div class="step-text">
  Once all of the ribs have been tacked, turn the plates over and weld (while clamped) the groves.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0753.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0753.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0755.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0755.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Smoothing the inside</h4>
<div class="step-text">
  Once cooled, grind away the welds ensuring the surface on the inside is completely smooth. Take care to not over grind the weld. Then sand the surface removing any finer imperfections.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0774.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0774.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0761.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0761.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Putting the sides together</h4>
<div class="step-text">
  Once all 4 sides have been sanded on the inside, place them together as seen in the diagram and weld them together. A clamp may be useful in this process.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/FEMALE_iso.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/FEMALE_iso.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0811.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0811.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Adding the bottom and top frame</h4>
<div class="step-text">
  Once all 4 sides have been welded. Place part number (BG5) on the bottom and part (BI5) on the top. This may need a gentle hammering to fit, but must not be forced otherwise it may bend. <br />
  <br />
  Once in place clamp together. Ensuring the inside surface is smooth and weld any external surface.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0828.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0828.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0825.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0825.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0831.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0831.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Prepare the male mould (Part C) </h4>
<div class="step-text">
  The male part of the mould is the final section but is probably the most complicated to assemble. Take these stages slow.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/MALE.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/MALE.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Weld each hump individually</h4>
<div class="step-text">
  The main section comes in many parts that we are going to tackle one at a time. The first is the humps. There are two and they are identical. Clamp them together and weld them along the seams. Preserving the corners. <br />
  <br />
  Like in step 8, use an angle grinder and sander to achieve round and smooth corners.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1034.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1034.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0788.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0788.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF0797.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF0797.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Weld the base</h4>
<div class="step-text">
  Using part CA5 align parts CE5 and CF5, clamp and weld. Avoid welding CA5 at this point. <br />
  <br />
  You should be left with a perfect frame to attach the two humps too.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1094.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1094.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1037.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1037.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1039.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1039.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Welding the humps to the base</h4>
<div class="step-text">
  Now place the humps on top of the base frame and clamp them tight together.<br />
  <br />
  First weld around the outside, then turn the mould over and weld the point where the two humps meet to avoid plastic leaking in the future.<br />
  <br />
  Finish by grinding and sanding the welds again for a smooth surface and round edges.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1136.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1136.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1108.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1108.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1183.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1183.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Adding the support structure</h4>
<div class="step-text">
  Take part CA5 and clamp to the base, Tack welding on the inside only baring in mind that parts CB5, CC5 and CCD will be placed inside. Once all parts are placed inside weld all accessible ribs in full. Clamping like previous steps.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/MALE_base+ribs_iso.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/MALE_base+ribs_iso.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1241.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1241.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF1188.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF1188.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Add the quick release nozzle</h4>
<div class="step-text">
  Take part DA10 and place it onto the surface of AD5 in the marked out space. Use an 8mm drill bit to ensure perfect alignment of the holes and weld only on the shortest of the two sides. Then place part DB5 ensuring it fits with your extrusion adapter. (See Extrusion Adapter How To). Again welding only the top sections.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/NOZZLE.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/NOZZLE.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF5479.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF5479.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF5476.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF5476.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Adding bolt release mechanism</h4>
<div class="step-text">
  To ensure we can always take the mould apart we need to install a set of bolts that prevent the mould from being stuck together. To do this, we disassemble the top section of the mould (Part A). Taking part AD5x1, thread a bolt through the bottom of the two 12mm holes and tighten the nut until it is placed firmly on the surface of the part. Then weld the nut that is on the top surface of the mould, ensuring no weld touches the nut.<br />
  <br />
  Repeat with this process with part AA5.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF5512.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF5512.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF5504.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF5504.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF5514.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF5514.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Fully assemble</h4>
<div class="step-text">
  Fully assemble all parts, connecting A to B using bolts. and you should be left with something like this.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF5540.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF5540.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF5539.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF5539.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF5541.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF5541.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Paint</h4>
<div class="step-text">
  The mould is almost ready. Depending on if you purchased a Stainless Steel mould or Mild steel, you may want to combat rust with a nice coat of paint. <br />
  <br />
  You will need something that can handle high temperatures above 300c, we use paint for fireplaces or engine parts. Spraying only the external surfaces of the mould. To protect the inside of the mould we use light rubbing of oil after every use.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF9686.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF9686.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF9688.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF9688.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF9689.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF9689.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Ready to make bricks!?</h4>
<div class="step-text">
  Well done, now you’re ready to extrude!<br />
  <br />
  Find advice for the extruding process in the How-to “Extrude into closed moulds”:<br />
  👉 tiny.cc/extrude-into-closed-mould<br />
  <br />
  And learn how to &quot;Build brick structures&quot;:<br />
  👉 tiny.cc/build-brick-structures
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/OI000039-01.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/OI000039-01.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-extruded-plastic-bricks/_DSF9615.jpg">
        <img class="step-image" src="/howtos/make-extruded-plastic-bricks/_DSF9615.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>