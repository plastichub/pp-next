### Weld plastic parts together 


If you want to join several plastic parts together, you can join them together at the joint surfaces with a simple soldering iron. 
It's quick and you don't need any additional material. ♻️👌