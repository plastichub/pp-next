### Drilling your holes 

 Drilling holes into concrete requires an impact hammer and a masonry drill bit. In this case, we used a 14mm drill bit for a 10mm bolt (next step). Make sure to wear respiratory protection if inside as this can potentially be a dusty job. Always wear protective glasses to protect your eyes. 

The depth you drill is dependant on the size of your expansion bolt. 
