### Check the results 

 The simplest method to measure how much moisture has been absorbed is weighing the granulate beforehand and afterwards. 
Unfortunately, if you are using hot air to dry granulat, volatiles are released from the sample, also reducing the weight. Therefore the weight loss does not exactly equal the loss of moisture so the measurement is imprecise. The professional approach involves making accurate moisture measurements in the drying hopper. This is possible using measurements of dielectric properties in real time. sensors are very expensive though.
