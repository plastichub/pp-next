# SHEETPRESS - AUSTRALIAN VERSION

NOTE, this is not a substitute for the official sheetpress starter-kit!

This folder contains the revised sheetpress blueprints, laser-cut files and CAD for fabrication in Australia.

Differences:

- 125x75x3 RHS steel (instead of 120x60x3 RHS steel)

- 76x38x3 RHS steel (instead of 80x40x3 RHS steel)

- Laser files and 2D drawings adjusted accordingly

- S355 structural steel is a European structural steel. We recommend using any structural steel available from your local laser cutting supplier

If you have any questions visit our community platform or Discord
https://community.preciousplastic.com/
https://discordapp.com/invite/rnx7m4t


Good luck!!



