<span id="specs" style="padding: 16px">
    <table class="center specs">
        <tbody>
            <tr>
                <td>Type:
                </td>
                <td>Sheetpress
                </td>
            </tr>        
            <tr>
                <td>Version:
                </td>
                <td>0.1
                </td>
            </tr>
            <tr>
                <td> Status:
                </td>
                <td> Development
                </td>
            </tr>
            <tr>
            <td> Authors </td>
                <td>
                 <li>[PlasticHub S.L.](${author_link})</li>
                </td>
            </tr>
        </tbody>
    </table>
</span>
