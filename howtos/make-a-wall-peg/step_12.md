### Done! 

 And you’re done! Here is your Wall Peg mold.
Remember to put a new screw in the wooden mold base every time you are going to inject. If you forget, the hole will be filled with plastic and won’t work. But don’t worry! Drill it again and you are done.
To open the mold, take off the bolts sideways, then cut the plastic at the entrance and pull apart the mold parts. Then, unscrew the peg off the wooden part and you have your peg ready.. 
Since the plug has some volume, it will take time to cool down and the outgoing screw will be soft. Avoid tilting it and make sure it is in the right position.
It will work with all the plastics and it is very easy and smooth to inject. Just explore and find your favorite plastics and mixtures.
