---
image: "/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_01.jpg"
title: "Make a durable tote bag from plastic bags"
tagline: ""
description: "Bjorn will show you a very simple technique to turn plastic bags into flat sheets (like fabric) to create a tote bag."
keywords: "product,LDPE"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "LDPE"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather everything you need</h4>
<div class="step-text">
  Before you start get all your gear ready:<br />
  - Respirator mask<br />
  - Lots of plastic PE bags from shopping or supermarkets<br />
  - Baking paper<br />
  - Cotton sheet<br />
  - Iron<br />
  - Sewing machine
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_02.jpg">
        <img class="step-image" src="/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_02.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_03.jpg">
        <img class="step-image" src="/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_03.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Watch this video tutorial</h4>
<div class="step-text">
  Watch this video tutorial to learn step-by-step how to transform lots of shopping bags into a more durable and cool tote bag.
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Done & Share</h4>
<div class="step-text">
  After watching this video you should understand the basics of this simple technique. You can reproduce the tote bag or try to make other products. We’re looking forward to seeing what you can come up with. Make sure to share back your new creations so the community can learn from you. Tag #preciousplastic on social media, create a new how-to or send us an email.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_05.jpg">
        <img class="step-image" src="/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_05.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_04.jpg">
        <img class="step-image" src="/howtos/make-a-durable-tote-bag-from-plastic-bags/bag_04.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>