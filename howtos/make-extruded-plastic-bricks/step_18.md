### Paint 

 The mould is almost ready. Depending on if you purchased a Stainless Steel mould or Mild steel, you may want to combat rust with a nice coat of paint. 

You will need something that can handle high temperatures above 300c, we use paint for fireplaces or engine parts. Spraying only the external surfaces of the mould. To protect the inside of the mould we use light rubbing of oil after every use. 