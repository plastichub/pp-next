---
image: "/howtos/make-a-flat-nozzle-for-the-extrusion-machine/10 (9).JPG"
title: "Make a flat nozzle for the Extrusion Machine"
tagline: ""
description: "The flat nozzle is a great addition to the extrusion machine. It enables one to cover greater surface with less plastic, increasing production time and new avenues for larger, watertight products."
keywords: "extrusion"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather materials and tools</h4>
<div class="step-text">
  To make a flat nozzle you will need:<br />
  <br />
  - 26mm (1&quot;) Round Tube. *It must fit into your extruder head.<br />
  - 5mm x 5mm Square bar. 100mm will be enough<br />
  - File <br />
  - Sander<br />
  - Vise<br />
  - Angle grinder or Metal saw<br />
  - Welding machine<br />
  - BSPT Thread cutter
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/1.JPG">
        <img class="step-image" src="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/1.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Thread your pipe</h4>
<div class="step-text">
  Now that you have your tools and materials ready, it&#39;s time to start making! To start the process we will need to clamp the length of pipe into a vise and make a thread with the thread cutter. Some pressure is needed to start the cut, so it can be useful starting in the vertical position so that you can use gravity to assist. Remember to use some oil to keep the cut smooth and your teeth sharp.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/2 (2).JPG">
        <img class="step-image" src="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/2 (2).JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Cut pipe and bar</h4>
<div class="step-text">
  After threading the tip of pipe, we can cut it to a length that suites your machine. In this example 60mm was cut to allow room for the heating element and final sizing. <br />
  While we&#39;re at it, we can cut two pieces of the square bar that will be used to make the mouth of the nozzle. It&#39;s important to cut the bar slightly wider than the tube. Leaving ~5mm extra on either side.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/3 (4).JPG">
        <img class="step-image" src="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/3 (4).JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Bend the pipe </h4>
<div class="step-text">
  To get the plastic flowing smoothly into into its new flat shape, we&#39;ll need to taper the pipe from a circle into an oval. We do this by putting the edge of the nozzle into the vise and slowly apply pressure. It&#39;s easy to over-do it here, so take it slow and check regularly. Try to avoid bendnig on the seam if your pipe has one and it can help to put a nut over or some wood inside the thread area to avoid deformation of thread.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/4.JPG">
        <img class="step-image" src="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/4.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Soften the edges</h4>
<div class="step-text">
  During the cuts and bending your parts, they can get a bit rough. It&#39;s important to remove any excess material and round all your edges with a sander or a file. This will help your parts fit together well and make handling your nozzle more comfortable. A crucial spot to focus on is the section that has just been bent because it has most likely deformed. This is where the two pieces of bar will be welded, so getting it flat is a good idea!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/5 (2).JPG">
        <img class="step-image" src="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/5 (2).JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Weld the mouth</h4>
<div class="step-text">
  This is the last step in the process and is crucial to do your best. You should choose how wide you want the nozzle to be and find an object to maintain that distance. In this example an old angle grinder disc has been used as a spacer. The two pieces have been held together with a clamp and tacked to the pipe on the short edges. Once you have those in place, you can then seal up the long edges and finish the nozzle.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/6 (2).JPG">
        <img class="step-image" src="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/6 (2).JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - 'Strude some flat noods</h4>
<div class="step-text">
  With your nozzle all finished, it&#39;s time to extrude around your meticulous mould and create those epic objects from your imagination.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/7 (4).JPG">
        <img class="step-image" src="/howtos/make-a-flat-nozzle-for-the-extrusion-machine/7 (4).JPG" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>