#include <Vector.h>
#include <Streaming.h>
#include <Arduino.h>
#include "app.h"
#include "features.h"

#include "./components/ExternalServo.h"
#include "enums.h"

const bool manual = true;

short App::onActuatorChange(short state)
{
    switch (state)
    {

    case ERROR_FATAL:
    case LinearActuator::E_ACTUATOR_STATE::ERROR:
    {
        }

    default:
        break;
    }
}

short App::loopPress()
{
    servo->loop();
    if (manual)
    {
        _loop_manual();
    }
    else
    {
        loop_normal();
    }
}

void App::_loop_manual()
{
    uchar sw = dirSwitch->loop();
    if (sw == POS3_DIRECTION::DOWN)
    {
        // servo->move(1);
        actuator->move(0);
        statusLightMove.setBlink(true);
        statusLightMove.on();
    }
    else if (sw == POS3_DIRECTION::UP)
    {
        // servo->move(1);
        actuator->move(1);
        statusLightMove.setBlink(true);
        statusLightMove.on();
    }
    else
    {
        actuator->stop(0);
        statusLightMove.setBlink(false);
        statusLightMove.off();
        //servo->stop();
    }
}
void App::onError(int error)
{
    if (_state != ERROR)
    {
        _state = ERROR;
    }

    /*
    statusLightDown.setBlink(true);
    statusLightUp.setBlink(true);
    statusLightHeat.setBlink(true);
    */
}
void App::loop_normal()
{
    uchar sw = dirSwitch->loop();
    short actuatorState = actuator->getSate();

    if (sw && sw == dirSwitch->last())
    {
        onError(ERROR);
        return;
    }

    switch (sw)
    {

    case POS3_DIRECTION::UP:
    {
        short moving = actuator->plunge(false);
        statusLightMove.setBlink(moving);
        statusLightMove.on();
        break;
    }

    case POS3_DIRECTION::DOWN:
    {
        short moving = actuator->home(false);
        statusLightMove.setBlink(moving);
        statusLightMove.on();
        break;
    }
    case POS3_DIRECTION::MIDDLE:
    {
        short plunge = actuator->reset();
        statusLightMove.setBlink(false);
        statusLightMove.off();
        break;
    }

    default:
        break;
    }
}
