### Prepare the wood 

 With your parts done, you can now continue with your wooden planks (or other material) for the seating. In this case we’ve gone with pine cut into 8x strips of 28x60x1500. This has then been drilled, sanded and oiled for the finish. These planks will be mounted to the plastic bench via wood screws.