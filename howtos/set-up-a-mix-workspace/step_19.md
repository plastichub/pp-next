### Maintenance 

 As you run your Mix Workspace it is crucial that you maintain the different machines in order to prevent failures.

You can ask the local Machine Shop for help. 