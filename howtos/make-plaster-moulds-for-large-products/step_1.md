### Make a model to replicate 

 You’ll need a model or object to cast your plaster mould around. This could be anything - a model you made, a 3D print, your favourite toy. Consider how many parts your mould requires. Our product required a two part mould.

In this case, the desired shape was cut out of foam using a home made hot wire and hand sanding.

Pay close attention to the surface finish - if there are any small bumps or dents, these will show in the final product. If you care about this - keep sanding, filling and painting.
