### The mould 

 As we will make this plate with the injection machine, we need to make the mould for it. It will be made of 3 parts: Bottom and top part out of aluminium.  And next is the connector part made from steel.
The top and bottom part will be CNC-milled out of two blocks of aluminium while we’ll make the connector part manually. 