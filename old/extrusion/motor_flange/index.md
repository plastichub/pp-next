---
image: /_howto/extrusion/motor_flange/IMG_2458.JPG
category: "extrusion"
title: "Motor Flange"
tagline: ""
description: ""
usedin:
  - lydia-v4
  - extrusion-pro
---





<div class="thumbs">First cut and face the bearing housing first. It's good to hold the large motor flange in the chuck.

<div class="thumb">
    <a href="./IMG_2399.JPG" _target="_blank">
        <img id="IMG_2399.JPG" src="./IMG_2399.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
Mark the center line and continue the line so that it's easy to center and tack weld it on the motor flange.
<div class="thumb">
    <a href="./IMG_2400.JPG" _target="_blank">
        <img id="IMG_2400.JPG" src="./IMG_2400.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2401.JPG" _target="_blank">
        <img id="IMG_2401.JPG" src="./IMG_2401.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2402.JPG" _target="_blank">
        <img id="IMG_2402.JPG" src="./IMG_2402.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2403.JPG" _target="_blank">
        <img id="IMG_2403.JPG" src="./IMG_2403.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2405.JPG" _target="_blank">
        <img id="IMG_2405.JPG" src="./IMG_2405.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />

Face it off at high speed and feed when using carbide inserts!

<div class="thumb">
    <a href="./IMG_2411.JPG" _target="_blank">
        <img id="IMG_2411.JPG" src="./IMG_2411.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />

Now mark the ID for the bearing housing register, a little undersized! It's better to mark it this way to reduce the amount of trial and error.


<div class="thumb">
    <a href="./IMG_2416.JPG" _target="_blank">
        <img id="IMG_2416.JPG" src="./IMG_2416.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2417.JPG" _target="_blank">
        <img id="IMG_2417.JPG" src="./IMG_2417.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />

Now mark the ID for the motor register, a little undersized! It's better to mark it this way to reduce the amount of trial and error.

<div class="thumb">
    <a href="./IMG_2418.JPG" _target="_blank">
        <img id="IMG_2418.JPG" src="./IMG_2418.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2419.JPG" _target="_blank">
        <img id="IMG_2419.JPG" src="./IMG_2419.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr />

Cut out the motor register. Use a carbide insert for finishing it. This register is important and needs to be precise as possible, + 0.01mm is a good fit since you hammer it right on.


<div class="thumb">
    <a href="./IMG_2421.JPG" _target="_blank">
        <img id="IMG_2421.JPG" src="./IMG_2421.JPG" width="100%" />
    </a>
    <span class="thumb-label">12. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2442.JPG" _target="_blank">
        <img id="IMG_2442.JPG" src="./IMG_2442.JPG" width="100%" />
    </a>
    <span class="thumb-label">13. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2443.JPG" _target="_blank">
        <img id="IMG_2443.JPG" src="./IMG_2443.JPG" width="100%" />
    </a>
    <span class="thumb-label">14. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2444.JPG" _target="_blank">
        <img id="IMG_2444.JPG" src="./IMG_2444.JPG" width="100%" />
    </a>
    <span class="thumb-label">15. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2446.JPG" _target="_blank">
        <img id="IMG_2446.JPG" src="./IMG_2446.JPG" width="100%" />
    </a>
    <span class="thumb-label">16. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2447.JPG" _target="_blank">
        <img id="IMG_2447.JPG" src="./IMG_2447.JPG" width="100%" />
    </a>
    <span class="thumb-label">17. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2448.JPG" _target="_blank">
        <img id="IMG_2448.JPG" src="./IMG_2448.JPG" width="100%" />
    </a>
    <span class="thumb-label">18. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2449.JPG" _target="_blank">
        <img id="IMG_2449.JPG" src="./IMG_2449.JPG" width="100%" />
    </a>
    <span class="thumb-label">19. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2452.JPG" _target="_blank">
        <img id="IMG_2452.JPG" src="./IMG_2452.JPG" width="100%" />
    </a>
    <span class="thumb-label">20. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2453.JPG" _target="_blank">
        <img id="IMG_2453.JPG" src="./IMG_2453.JPG" width="100%" />
    </a>
    <span class="thumb-label">21. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2454.JPG" _target="_blank">
        <img id="IMG_2454.JPG" src="./IMG_2454.JPG" width="100%" />
    </a>
    <span class="thumb-label">22. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2455.JPG" _target="_blank">
        <img id="IMG_2455.JPG" src="./IMG_2455.JPG" width="100%" />
    </a>
    <span class="thumb-label">23. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2456.JPG" _target="_blank">
        <img id="IMG_2456.JPG" src="./IMG_2456.JPG" width="100%" />
    </a>
    <span class="thumb-label">24. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2458.JPG" _target="_blank">
        <img id="IMG_2458.JPG" src="./IMG_2458.JPG" width="100%" />
    </a>
    <span class="thumb-label">25. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

