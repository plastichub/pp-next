### Overview of the product 

 The assembly of this stool works with tight fittings (aka tolerances). The part of the legs that goes through the stool top have to be 0.9mm bigger than the hole. This in combination with the properties of the material will make a really tight fit. 

In our case we are using a 28mm thick sheet, so the dimension of the holes has to be 27.1mm. Make sure to first check the thickness of your sheet and adapt the 3D model of the wood jigs to your needs. In the download files you’ll find the dimensions that are susceptible of change marked in red.
