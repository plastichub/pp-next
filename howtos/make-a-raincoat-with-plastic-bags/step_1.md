### Choose your measurements  

 You will need to work out the required measurements for the jacket. The template below will provide you with a plan for taking and recording these measurements. 
(A) length of the arm
(B) length from shoulder to shoulder
(C) length from shoulder to the neck centre
(D) body width
(E) desired length of the raincoat