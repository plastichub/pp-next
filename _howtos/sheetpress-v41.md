---
image: "/howtos/sheetpress-v41/cover-extrusion-3.jpg"
title: "Set up an Extrusion Workspace"
tagline: ""
description: "In this How-to we’re going to guide you through all the steps to set up an Extrusion Workspace. Learn about plastic, how to find a space, get the Extrusion machine, find customers and connect to the Precious Plastic Universe. <br /><br />Download files:<br />👉 https://cutt.ly/starterkit-extrusion 👈<br /><br />Step 1-3: Intro<br />Step 4-9: Learn<br />Step 10-19: Set up<br />Step 20-25: Run<br />Step 26-29: Share"
keywords: "starterkit,extrusion"
tags:
- "starterkit"
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4>
{{ page.description }}
<h4 class="step-title" id="step-1">Step 1 - Role</h4>
<div class="step-text">
  First of all, make sure you had a look at the showcase page of this Starterkit!<br />
  👉 preciousplastic.com/starterkits/showcase/extrusion<br />
  <br />
  Now about your Role:<br />
  <br />
  Extrusion workspaces buy recycled shredded plastic from Shredder Workspaces and transform it into recycled beams and bricks.<br />
  <br />
  These colourful objects are then sold to design studios for further design or directly to customers.<br />
  <br />
  Extrusion workspaces should also reach out to Community Point to connect with the local Precious Plastic community and, maybe, get help selling their materials.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/universe-extrusion.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/universe-extrusion.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Outcomes</h4>
<div class="step-text">
  The outcome of an Extrusion Workspace can be recycled plastic beams, bricks or other big objects. <br />
  <br />
  The beams can be of various sizes and shapes, and as long as needed. Make sure you don&#39;t miss playing around with different gradients and colours to make create a playful variety of outcomes.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image9.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/image9.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Is this for you?</h4>
<div class="step-text">
  For the Extrusion Workspace you will have to be quite technical as you have to understand how the Extrusion machine works, ideally know how to maintain it and, as a plus, know how to fix it when it needs a bit of extra love. <br />
  <br />
  Attention to details is also a nice, and some creativity to come up with new patterns and techniques.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image19.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image19.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Learn about Precious Plastic</h4>
<div class="step-text">
  As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions.<br />
  <br />
  If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.<br />
  👉 preciousplastic.com<br />
  👉 community.preciousplastic.com<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.<br />
  👉 community.preciousplastic.com/academy/universe
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/learn-precious-plastic-universe.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/learn-precious-plastic-universe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Check out your area</h4>
<div class="step-text">
  Get an overview of who and what is already existing in your area.<br />
  <br />
  Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. <br />
  <br />
  Make sure not to jam the local network, if there are already many Extrusion Workspaces around, have a chat about collaboration with them first, or maybe consider starting another type of space.<br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/communitypoint-step4-map-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Find a Community Point near you</h4>
<div class="step-text">
  Community Points are the glue of the Precious Plastic Universe. They know the ins and outs of your local network. They can help you in multiple ways but they generally have a very in-depth overview of local Precious Plastic spaces, people, useful shops, resources and can help you with planning out your project. You can find them on the map here. <br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image13.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/image13.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Find your plastic supply</h4>
<div class="step-text">
  You can use the Precious Plastic Map and the Bazar to find Shredder workspaces around you that can provide you with the raw material: shredded plastic waste. <br />
  <br />
  If you have a local Community Point, they might be able to give you a hand with this as well.<br />
  <br />
  👉 community.preciousplastic.com/map<br />
  👉 bazar.preciousplastic.com
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image18.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Get on Discord</h4>
<div class="step-text">
  Day to day discussions in the Precious Plastic Universe happen on Discord.<br />
  <br />
  Introduce yourself, say hi in your country channel and start to discover the different channels where people go deep into specific topics (building, collection, design etc..)<br />
  <br />
  Join the Discord:<br />
  👉 discordapp.com/invite/rnx7m4t
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image18.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/image18.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Learn the basics of plastic</h4>
<div class="step-text">
  Before you start it is crucial to get a solid understanding of plastic. How it works, the different types, melting temperatures and so on. Head over to our Academy and dive into the plastic chapters to learn about the different types and properties etc.<br />
  <br />
  👉 http://tiny.cc/basics-about-plastic
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image17.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image17.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Fill in the Action Plan</h4>
<div class="step-text">
  Before jumping into making machines or finding a space it is smart to sit down and properly plan your project and shape your vision. <br />
  <br />
  To help you plan we’ve made a graphic tool called the Action Plan that helps you to craft your mission, understand your customers, revenue streams, costs and much more. With this tool, you should get a step closer to create a successful project. <br />
  <br />
  You can find the Action Plan in the Download Kit or learn more in the Academy<br />
  👉 http://tiny.cc/business-actionplan
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image3.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image3.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Workspace Calculator</h4>
<div class="step-text">
  Now you have your project nicely planned out and it’s starting to take shape in your mind. <br />
  <br />
  It is important at this stage to make a serious estimation of how much it will cost you to set up and run your workspace. Otherwise, you might run out of money halfway. The Workspace Calculator is a spreadsheet that helps you do just that. <br />
  <br />
  You can find the Workspace Calculator in the Download Kit or learn more in the Academy:<br />
  👉 http://tiny.cc/workspace-calculator
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image12.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image12.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Business Plan Template</h4>
<div class="step-text">
  To help you pitch your idea to potential partners, financial institutions or investors we made a Business Plan Template (and a specific example for the Extrusion Workspace) for you to use.<br />
  <br />
  This template helps you to talk the business language and should help you access the necessary money to begin. <br />
  <br />
  For more explanation check out the video in the Academy:<br />
  👉 http://tiny.cc/business-plan-calculator
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image20.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image20.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Tool list</h4>
<div class="step-text">
  Alongside your Extrusion machine, you will need a number of other tools and machines to help you with the operations of the Extrusion Workspace.<br />
  <br />
  In the Download Kit, you will find a tool list with all the necessary tools to run your workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/ToolKit.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/ToolKit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Find the space</h4>
<div class="step-text">
  To help you find the perfect place for your workspace you can use the floor plan in the Download Kit, with all the minimum requirements and a little cardboard tool to place your machines and tools in the workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/space.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Get your extrusion</h4>
<div class="step-text">
  Cool, now you have a space it’s time to get hold of your Extrusion machine. There are three ways to do that:<br />
  <br />
  1 Build it yourself following our tutorials<br />
  👉 tiny.cc/build-extrusion-pro<br />
  <br />
  2 Buy it on the Bazar.<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  3 Find a Machine Shop near you on the map that can build it for you.<br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image11.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image11.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Build your space</h4>
<div class="step-text">
  Super, you’ve got your Extrusion! But an Extrusion alone is not enough.<br />
  <br />
  You can watch our video on how to fully set up your Extrusion workspace with all the additional tools, furniture and posters needed to make your space ready. <br />
  <br />
  👉 http://tiny.cc/space-shredder
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/build-workspace.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/build-workspace.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Safety</h4>
<div class="step-text">
  Always stay safe!<br />
  <br />
  Of course, Extrusion machines get hot. And can cause a hazard in different ways.<br />
  <br />
  So before starting to melt, please check out our safety video to learn about dangers and how to stay safe when working with plastic.<br />
  <br />
  👉 http://tiny.cc/plastic-safety
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/Safety 1.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/Safety 1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Buy shredded plastic </h4>
<div class="step-text">
  Buy shredded plastic from your local Shredder Workspace. Make sure to specify your preferred shreds size (small, medium or large) and to have a variety of colours and types.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image22.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image22.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Time to make beams!</h4>
<div class="step-text">
  Now that you have all the things in place it’s time to start making beams. Watch this video to learn how to make beams and adopt the best practices.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image23.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image23.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Make a variety</h4>
<div class="step-text">
  Once you get a grasp on the process make sure to make a nice variety of colours, sizes and thicknesses to attract different customers.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image6.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image6.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Maintenance</h4>
<div class="step-text">
  As you run your Extrusion Workspace it is crucial that you maintain the Extrusion machines in order to prevent failures. Find out here how to best maintain the Extrusion.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image10.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image10.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Sell your beams</h4>
<div class="step-text">
  You can now make beautiful beams. Many every day. Now is crucial to find people and organisations that want to buy your recycled beams. <br />
  <br />
  First, you should put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. Then you have to get creative on how to sell your beams locally. Shops, design studios, online stores and more.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image14.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image14.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Create your profile</h4>
<div class="step-text">
  If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform to connect with people. Follow this link and sign up with your email, pick your role, put your pin on the map and upload nice pics to show the world what you’re doing.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image8.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image8.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-24">Step 24 - Create How-tos!</h4>
<div class="step-text">
  Share to the world how you run your Extrusion Workspace so other people can learn from you and start using your solution to tackle the plastic problem. <br />
  <br />
  Make sure to only create How-tos for your best processes and techniques, not tryouts or one-offs. This can also help you create a name for yourself in the Precious Plastic community.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image15.png">
        <img class="step-image" src="/howtos/sheetpress-v41/image15.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-25">Step 25 - Stay active on Discord</h4>
<div class="step-text">
  Precious Plastic is people. People working together and helping each other. Go to Discord and connect with people.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/image5.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/image5.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-26">Step 26 - Good things take time</h4>
<div class="step-text">
  Starting off will take some time in the beginning. It’s normal. Be patience, work smart and reach out to your Precious Plastic community if you need help. Everything will take off. <br />
  You’re changing the world.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/good-things.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/good-things.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-27">Step 27 - Download and start!</h4>
<div class="step-text">
  Ready and excited to start?<br />
  You&#39;re a hero!<br />
  <br />
  In this download package you will find all the files you need to set up your Extrusion Workspace.<br />
  Download and start your recycling journey!<br />
  <br />
  👉 https://cutt.ly/starterkit-extrusion 👈
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/sheetpress-v41/Workspace Starter Kit.jpg">
        <img class="step-image" src="/howtos/sheetpress-v41/Workspace Starter Kit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
{% if page.related %}
<h3 id="related"> Related </h3>
{% endif %}
---
{% if page.tags %}
{% endif %}
{% if page.resources %}
{% endif %}