## General Purpose Extrusion Firmware

### VFD

#### Protocols

- [x] Modbus, revA
- [x] Analog
- [ ] Profibus Mapping/Proxy
- [ ] RPM - PID

#### Sensors

- [x] Jamming, revA
- [ ] Visual feedback
- [ ] Sound

### PIDs

#### Protocols 

- [ ] Modbus
- [x] Analog
- [ ] Profibus Mapping/Proxy

#### Sensors / Signals

- [ ] Burnout
- [ ] Overshoot
- [x] AT finish
- [x] Overcurrent

## Commons

- [ ] Error dispatch, rev-C
- [ ] External LED Bank, rev-C
- [ ] HMI interface for Plastic Hub Studio (PP OS build), January
