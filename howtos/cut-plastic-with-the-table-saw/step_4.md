### Blade height 

 How high above the plate we place the blade could have a huge impact on the result. When working with plastic, less teeth cutting at the same time seems to create less heat and it tends to give cleaner cuts.

That’s why we recommend to leave a significant distance between the plate and the top of the blade