# Z axis motion for Noah's printhead

Noah's print head must be able to move up and down.

Initially Noah's print head was to be entirely built from scaffolding tubes, with the complete extruder assembly mounted to the moving head, and uing a winch to raise and lower the carriage. 
However, weight considerations have forced other options to be explored. A heavy carriage would limit Z-axis speed, and this would also prevent 45 degree printing.

Firstly, to reduce the weight of the printhead, the extruder motor is fitted to the X carriage rather than to the Z carriage, and employ a transmission system as discussed [here](https://gitlab.com/plastichub/noah/blob/master/machines/noah/documents/printhead/extruder/transmission/readme.md)

### Screw Jacks

![Animated gif](https://polimak.com/wp-content/uploads/Dogrusal-Hareket-Vidali-Kriko.gif)

[Video](https://www.youtube.com/watch?v=i-Z4hz_KX0M)
[PDF](https://rotero.com/media/downloads/SERVOMECH%20Acme%20Screw%20Jacks.pdf)

In order to avoid using a heavy gearbox to drive a pulley for Z motion, Noah's Z carriage could use Acme Screw Jacks - motorized lead screw actuators which use standard Acme threaded bar.
These are effectively equivalent to a worm-drive gearbox, and will allow maximum potential stroke of 6 meters in the Z axis - as this is the maximum length [Acme threaded bar](https://www.automotioncomponents.co.uk/en/page/lead-screws-from-automotion) is available in. 

![Acme Screw Jack travelling screw](https://servomech.com/wp-content/uploads/2019/03/screw-jack-MA50-ModA-NF.jpg)



The screw jack should be the 'travelling screw' type as shown (but inverted).  [Video](https://www.youtube.com/watch?v=5SN5R32XyH4)
Using a lead screw type drive will prevent the carriage accidentally falling on power-off.

The ideal solution would be an actual ball screw but these would be very expensive to buy at the length required for Noah. A custom screw might be possible. Acme screw pitch is low, and friction also slows them down. However 300mm/s should be possible.

For stability, four screw jacks would be ideal, but two may be sufficient. Multiple screw jacks can be coupled to a single motor, mounted on the X carriage next to the extruder motor.
