---
image: "/howtos/make-an-automated-injection-molding-machine/Photo 26.08.20, 14 03 00.jpg"
title: "Make an automated injection molding machine"
tagline: ""
description: "This how-to contains information on how we built a low-cost, automated injection molding machine. Unfortunately the folder is way to big for uploading it here. <br /><br />We added a link to our dropbox, where you can download the folder :)"
keywords: "extrusion,injection,other machine,research"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
- "injection"
- "other machine"
- "research"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/sotop-recycling">sotop-recycling</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Download the folder</h4>
<div class="step-text">
  Download and read carefully the Guide.pdf.<br />
  <br />
  Link:<br />
  <br />
  <a href="https://www.dropbox.com/sh/bvus8maneewhxhk/AACpXCwufQSTb2FyYe8HFChFa?dl=0<br/>">https://www.dropbox.com/sh/bvus8maneewhxhk/AACpXCwufQSTb2FyYe8HFChFa?dl=0<br /></a>
  <br />
  IMPORTANT! The machine is still work in progress and not perfect yet. We added a troubleshooting chapter in the download with details on what has to be improved.<br />
  <br />
  The Guide.pdf contains the most important informations and gives you an idea how all the info is structured.<br />
  <br />
  The package contains:<br />
  <br />
  - CAD<br />
  - Bill of material (BOM)<br />
  - Blueprints<br />
  - Program<br />
  - Circuit diagram<br />
  - Additional pictures<br />
  - ...<br />
  <br />
  We decided to rather put a little bit more information than to few. So dont be scared when we are going a bit into the details :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/Blueprint.PNG">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/Blueprint.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/BOM.PNG">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/BOM.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/circuit.PNG">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/circuit.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Check out the design</h4>
<div class="step-text">
  Before you consider building this machine, make sure that it is within your capabilities.<br />
  <br />
  It is much more complex than a normal injection or extrusion machine.<br />
  <br />
  In the download folder there is a very detailed CAD of the current state :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/Rendering.PNG">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/Rendering.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/section analysis.PNG">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/section analysis.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/CAD angle.PNG">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/CAD angle.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Replicate and do even better;)</h4>
<div class="step-text">
  If you want to accept the challenge, then build the machine yourself.<br />
  <br />
  In this video you see how we can already work with it :)<br />
  <br />
  <a href="https://www.youtube.com/watch?v=Eq9IbetsLB4&amp;t=2s<br/>">https://www.youtube.com/watch?v=Eq9IbetsLB4&amp;t=2s<br /></a>
  <br />
  If you have good Ideas and you made improvements with your machine, please let us know and share back!<br />
  <br />
  Enjoy, SOTOP-Recycling :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/Photo 26.08.20, 14 34 26.jpg">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/Photo 26.08.20, 14 34 26.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/Photo 26.08.20, 14 01 47.jpg">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/Photo 26.08.20, 14 01 47.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-automated-injection-molding-machine/phone covers.jpg">
        <img class="step-image" src="/howtos/make-an-automated-injection-molding-machine/phone covers.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>