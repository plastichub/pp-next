### Explore the possibilities! 

 Et voilá , this is how you create beams and materials with magical colours, transparencies and patterns. As the mixing process is hard to control, achieving exactly the same outcome is unrealistic. But the beauty of melting plastic this way is that each piece will be unique! The transparency of PS offers itself perfectly for lighting, like shown in our how-to "Make a lamp from PS".

But the further possibilities of this material can be taken into many other directions.
Have fun exploring!