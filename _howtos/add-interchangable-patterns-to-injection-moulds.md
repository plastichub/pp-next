---
image: "/howtos/add-interchangable-patterns-to-injection-moulds/Luminus-Sheep-on-wheels-2.jpg"
title: "Add interchangable patterns to injection moulds"
tagline: ""
description: "Using lasercut, cnc milled or handmade plates you can add interchangable text or patterns to injection mould products. Add plastic types or logo&#39;s to the products."
keywords: "hack,mould,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
- "mould"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/sheep-on-wheels">sheep-on-wheels</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Have a mould</h4>
<div class="step-text">
  We are adding a part to an existing mould. In this example we use the hexagon tile mould. It&#39;s a simple 3 part flat mould held together with boults. Outer size : 15 x 15 cm. The top and bottom plate are 10mm, the center part with hexagon cutout is 4mm.<br />
  <br />
  We will describe how to do lasercutting and CNC milling. For acces to a lasercutter/cnc milling device you can check with your local Fablab/makerspace, sometimes even high schools or universities.<br />
  <br />
  For the lasercut add ons we use 4mm acrylic plates (15 x15cm).<br />
  For the CNC we used 3mm aluminium plates (15 x 15cm).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/Screenshot 2020-02-01 at 15.28.06.png">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/Screenshot 2020-02-01 at 15.28.06.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/IMG_5319 copy.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/IMG_5319 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Design a pattern</h4>
<div class="step-text">
  We used software to create different patterns. Make sure the text is mirrored as we are producing a negative. Depending on the technique used you might need a specific export format.<br />
  <br />
  You can add information (plastic type, production info) as you wish.<br />
  <br />
  With lasecutting there is virtually no limit in shapes you can cut. Do mind that smaller detail is harder to inject. In our experience 2-3mm is possible with HDPE injection. <br />
  <br />
  When CNC milling the limit is set by the router bit. As it cuts by turning all corners are smoothed out by the diameter of the router bit. Check the limit of the machine you are using. We used a 1mm router bit.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-1-1.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-1-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-1-2.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-1-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Create a compatible file </h4>
<div class="step-text">
  When lasercutting a 2D file is sufficient. Some software allows to use PNG, AI or other files. <br />
  <br />
  When CNC milling a 3D file is needed. Using software like Fusion 360 you can change your vector file into a 3D shape.<br />
  <br />
  In our example we&#39;ve set the cutting depth to 1.5mm.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/Screenshot 2020-05-15 at 11.34.57.png">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/Screenshot 2020-05-15 at 11.34.57.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Lasercut</h4>
<div class="step-text">
  Depending on the type of cutter used settings will differ. <br />
  <br />
  Best is to do some test cuts first, changing the speed and intensity of the laser. Acrylic is safe to ut but does smell so dust extraction is advised. <br />
  <br />
  Make sure the pattern is not cut trough the acrylic plate as it will weaken the texture mould and make it less durable (although its possible for some shapes).<br />
  <br />
  Make sure the edges are cut through to prevent having to drill and cut manually later on in the proces. <br />
  <br />
  You can use multiple depths in one mould by assigning different settings to the cutting lines.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-4.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-4.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-8.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-8.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/IMG_5703 copy.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/IMG_5703 copy.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - CNC milling</h4>
<div class="step-text">
  Run the file through the software, clamp the material in place and start milling. <br />
  <br />
  Again it is wise to do some speed tests first.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-5-1.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-5-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-5-2.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-5-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Inject</h4>
<div class="step-text">
  Add the texture plate to the mould and start injecting. <br />
  <br />
  Best results when cooling down the mould quickly.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/IMG_5014 copy.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/IMG_5014 copy.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-6.jpg">
        <img class="step-image" src="/howtos/add-interchangable-patterns-to-injection-moulds/sheeponweels-injection-mould-6.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>