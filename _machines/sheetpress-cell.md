---
image: /machines/sheetpress/cell//renderings/perspective.JPG
category: "sheetpress"
title: "Sheetpress - Cell"
product_rel: "/machines/sheetpress/cell/"
tagline: ""
description: "Precious Plastic - Machine : Sheetpress :: Sheetpress - Cell"
product_id: sheetpress-cell
wiring: true
has_spec: true
products: true
overview_drawing: true
preview: 
buy: "mailto:sales@plastic-hub.com?subject=Inquiry%20-%20sheetpress-cell"
teaser: "Make large sheets from recycled plastic up to 1.20m !"
products: true
products_row:
  - image_path: /assets/site/product_sheets.jpg
    alt: "Injection Press"
    title: "Injection Press"
    excerpt: "Powerful and precise injection"
    url: "https://community.preciousplastic.com/how-to/make-extruded-plastic-bricks"
    btn_class: "btn--primary horizontal-margin"
    btn_label: "Learn more"
    buy: "test"
    price: "From 2000 Euro"
  - image_path: /assets/site/product_chair.jpg
    alt: "Injection Press"
    title: "Injection Press"
    excerpt: "Powerful and precise injection"
    url: "https://community.preciousplastic.com/how-to/make-a-chair-with-bent-sheets"
    btn_class: "btn--primary horizontal-margin"
    btn_label: "Learn more"
    buy: "test"
    price: "From 2000 Euro"
  - image_path: /assets/site/product_table.jpg
    alt: "Injection Press"
    title: "Injection Press"
    excerpt: "Powerful and precise injection"
    url: "https://community.preciousplastic.com/how-to/make-a-chair-with-bent-sheets"
    btn_class: "btn--primary horizontal-margin"
    btn_label: "Learn more"
    buy: "test"
    price: "From 2000 Euro"
  - image_path: /assets/site/product_table2.jpg
    alt: "Injection Press"
    title: "Injection Press"
    excerpt: "Powerful and precise injection"
    url: "https://community.preciousplastic.com/how-to/make-a-chair-with-bent-sheets"
    btn_class: "btn--primary horizontal-margin"
    btn_label: "Learn more"
    buy: "test"
    price: "From 2000 Euro"

gallery_drawings:
 - url: "/machines/sheetpress/cell/drawings/dimensions.PDF"
   image_path: "/machines/sheetpress/cell/drawings/dimensions.JPG"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/Lower Platten Frame Assembly Drawing.PDF"
   image_path: "/machines/sheetpress/cell/drawings/Lower Platten Frame Assembly Drawing.jpg"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/Lower Platten Frame Components Drawing.PDF"
   image_path: "/machines/sheetpress/cell/drawings/Lower Platten Frame Components Drawing.jpg"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/parts.PDF"
   image_path: "/machines/sheetpress/cell/drawings/parts.JPG"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/Platen Frame Side Rail Lower.PDF"
   image_path: "/machines/sheetpress/cell/drawings/Platen Frame Side Rail Lower.jpg"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/Sheetpress_Baseframe_Drawing1.PDF"
   image_path: "/machines/sheetpress/cell/drawings/Sheetpress_Baseframe_Drawing1.JPG"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/Sheetpress_Platenframe_Drawing1.PDF"
   image_path: "/machines/sheetpress/cell/drawings/Sheetpress_Platenframe_Drawing1.JPG"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/Sheetpress_Sideframe_Drawing1.PDF"
   image_path: "/machines/sheetpress/cell/drawings/Sheetpress_Sideframe_Drawing1.JPG"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/spec.PDF"
   image_path: "/machines/sheetpress/cell/drawings/spec.JPG"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/Upper Platen Assembly Drawing.PDF"
   image_path: "/machines/sheetpress/cell/drawings/Upper Platen Assembly Drawing.jpg"
   alt: ""
   title: ""
 - url: "/machines/sheetpress/cell/drawings/Upper Platen Components Drawing.PDF"
   image_path: "/machines/sheetpress/cell/drawings/Upper Platen Components Drawing.jpg"
   alt: ""
   title: ""
---
<div prefix="/templates/jekyll/" file="machine.html" context="machine global" class="fragment">
    
    <div prefix="/templates/jekyll/" file="machine_layout.html" context="machine global" class="fragment">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
        </li>
        {% if page.tabs.build != false %}
        <li class="nav-item">
            <a class="nav-link" id="build-tab" data-toggle="tab" href="#build" role="tab" aria-controls="build" aria-selected="false">Build</a>
        </li>
        {% endif %}
        <li class="nav-item">
            <a class="nav-link" id="resources-tab" data-toggle="tab" href="#resources" role="tab" aria-controls="resources" aria-selected="false">Resources</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="howtos-tab" data-toggle="tab" href="#howtos" role="tab" aria-controls="howtos" aria-selected="false">Howtos</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#media" role="tab" aria-controls="media" aria-selected="false">Media</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" id="media-tab" data-toggle="tab" href="#discussion" role="tab" aria-controls="media" aria-selected="false">Discussion</a>
        </li>

        {% if page.wiring %}
        <li class="nav-item">
            <a class="nav-link" id="wiring-tab" data-toggle="tab" href="#wiring" role="tab" aria-controls="wiring" aria-selected="false">Electronics</a>
        </li>

        {% endif %}


    </ul>

    <div class="tab-content">

        <div class="tab-pane active" id="overview" role="tabpanel" aria-labelledby="overview-tab">

            {% include download_spec.html %}

            <div prefix="machines/sheetpress/cell/" file="overview.html" context="machine" class="fragment">
    <div>

        <a href="">
            <img src="" style="margin:8px; float: left;max-width:50%;max-height: 400px;" />
        </a>

        <span style="font-size: smaller; margin-top: 16px;">
            Precious Plastic Sheetpress with updates.
        </span>

        <div style="display: table-cell;">
            <div prefix="machines/sheetpress/cell/" file="mini_specs.md" context="machine" class="fragment">
    <p><span id="specs" style="padding: 16px">
            <table class="center specs">
                <tbody>
                    <tr>
                        <td>Type:
                        </td>
                        <td> Sheetpress
                        </td>
                    </tr>
                    <tr>
                        <td>Version:
                        </td>
                        <td> 4.3
                        </td>
                    </tr>
                    <tr>
                        <td> Status:
                        </td>
                        <td> Tested
                        </td>
                    </tr>
                    <tr>
                        <td> Sheet - size:
                        </td>
                        <td> 1.20 x 1.20 meter
                        </td>
                    </tr>
                    <tr>
                        <td> Based on:
                        </td>
                        <td> <a href="https://community.preciousplastic.com/academy/build/sheetpress">PP Eindhoven Sheetpress</a>
                        </td>
                    </tr>
                    <tr>
                        <td> Authors </td>
                        <td>
                            <li><a href="https://precious-plastic.org/pp/">PlasticHub S.L.</a></li>
                            <li><a href="https://preciousplastic.com/">PrecisousPlastic - Eindhoven</a> </li>
                            <li><a href="https://www.facebook.com/david.bassetti.79">David Basetti - 3D Seed</a></li>
                        </td>
                    </tr>
                </tbody>
            </table>
        </span></p>
</div>

            {% include machine_toolbar.html download="https://precious-plastic.org//archives/sheetpress-cell.zip" preview="https://a360.co/2ZxZ8Jy" %}

        </div>

    </div>

    <div style="margin: 8px;clear:both">

    </div>

    <a href="/products/sheetpress-cell/renderings/single_cell-outline.JPG">
        <img src="/products/sheetpress-cell/renderings/single_cell-outline.JPG" style="margin:8px;" />
    </a>

    <hr />

    <div prefix="machines/sheetpress/cell/" file="features.md" context="machine" class="fragment">
    <h3 id="featureschanges">Features / Changes</h3>
    <ul>
        <li>sheet size : 1.30</li>
        <li>interlockable</li>
        <li>interlockable</li>
        <li>integrated fan for fumes</li>
        <li>heater blocks with perfect fit for the the cartridge heaters. This garantuees maximum lifetime as well energy savings over the 0.3mm loose fit as in the original.</li>
        <li>In case you take the dual sheetpress, up to 1.20x2.40m sheets, we supply you with a tested hydraulic system</li>
        <li>better rails as in v4.0</li>
    </ul>
</div>

    <hr />

    <div prefix="machines/sheetpress/cell/" file="academy_overview.md" context="machine" class="fragment">
    <table>
        <thead>
            <tr>
                <th>Specification</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Type</td>
                <td>Sheetpress</td>
            </tr>
            <tr>
                <td>Version</td>
                <td>1</td>
            </tr>
            <tr>
                <td>Price new material in NL</td>
                <td>+/- €2.550</td>
            </tr>
            <tr>
                <td>Additional system (Cooling press + table)</td>
                <td>+/- €860</td>
            </tr>
            <tr>
                <td>Weight</td>
                <td>450kg (Sheetpress)</td>
            </tr>
            <tr>
                <td>Sheetpress Dimension</td>
                <td>1620 X 1620 X 1780 mm</td>
            </tr>
            <tr>
                <td>Voltage</td>
                <td>400V</td>
            </tr>
            <tr>
                <td>AMP</td>
                <td>32A</td>
            </tr>
            <tr>
                <td>Power</td>
                <td>15kW</td>
            </tr>
            <tr>
                <td>Input Flake Size</td>
                <td>Large, Medium, Small</td>
            </tr>
            <tr>
                <td>Max Running Time</td>
                <td>8 hours per day</td>
            </tr>
            <tr>
                <td>Max temp</td>
                <td>300°C</td>
            </tr>
            <tr>
                <td>Tested Plastics</td>
                <td>HDPE, LDPE, PP, PS</td>
            </tr>
            <tr>
                <td>Using foils?</td>
                <td>Yes</td>
            </tr>
            <tr>
                <td>Input Between Plates</td>
                <td>300mm</td>
            </tr>
            <tr>
                <td>Size of Sheet</td>
                <td>1200 x 1200 mm</td>
            </tr>
            <tr>
                <td>Range of Sheet Thickness</td>
                <td>4 - 35mm</td>
            </tr>
            <tr>
                <td>Sheets Per Day (12mm)</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Sheets Per Day with full system (12mm)</td>
                <td>10</td>
            </tr>
        </tbody>
    </table>
    <h3 id="pros">Pros</h3>
    <p>Low material cost in comparison with market machines, typically 10,000+ for systems that produce sheets greater or equal to 1mx1m.</p>
    <p>This is the first press we have seen that transfers heat through conduction alone, not convection or conduction and convection which is the typical heating technique. This makes the heating process faster and more energy efficient than the alternatives on the market.</p>
    <p>Can be run by one person, most other systems require two people to operate. No other systems on the market include usability tools meaning multiple people are needed at various steps of the sheet making process for sheets of this size.</p>
    <p>With the full system you can heat and press sheets simultaneously which allows you to produce sheets much faster than a standalone Sheetpress which is typically the type of system available on the market.</p>
    <p>Ease of build and diversity of molds, the molds we have developed are comparatively simpler and cheaper to make than molds from other systems.</p>
    <h3 id="cons">Cons</h3>
    <p>Sheet size 1m x 1m, many manufacturing processes have been designed to work with 1220mm x 1220mm and there are sheet pressing systems available on the market that can produce sheets larger than this system.</p>
    <p>Most mid-small workspace machinery only requires 16A but the Sheetpress requires 32A.
        Compared with other Precious Plastic machines the Sheepress is significantly larger. It does not fit through a standard door or double doors like all the others and requires at least a small garage door to fit it inside.</p>
    <p>The machine does not have wheels and is not moveable by human power, so it requires a pallet/pump truck to move. Some other sheet pressing systems come with wheels but we opted not to for stability.</p>
    <h1 id="inputoutput">Input &amp; output</h1>
    <h3 id="plasticinput">Plastic Input</h3>
    <p>We have successfully pressed sheets from clean HDPE, LDPE, PP and PS. The press can take any type of flakes (small, medium large). We recommend using the large flakes produced from the shredder pro using with the large sieve. Since it reduces the amount of shredding. We have had success in pressing sheets made from non-shredded bottle caps, the melting time just increases slightly with a larger size of input material. Here is a chart to give an overview in different thicknesses, times and temperatures.</p>
    <p>⚠️ Important: you cannot press dirty plastic as it will leave burned residue on the mould which is very difficult to remove.</p>
    <table>
        <thead>
            <tr>
                <th>Sheet thickness</th>
                <th>HDPE <br> 220°</th>
                <th>PP <br> 230°</th>
                <th>PS <br> 240°</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>6MM</td>
                <td>6.5 KG <br> 40 Min</td>
                <td>6.5 KG <br> 40 Min</td>
                <td>6.5 KG <br> 40 Min</td>
            </tr>
            <tr>
                <td>8MM</td>
                <td>8.5 KG <br> 45 Min</td>
                <td>8.5 KG <br> 45 Min</td>
                <td>8.5 KG <br> 45 Min</td>
            </tr>
            <tr>
                <td>10MM</td>
                <td>11 KG <br> 50 Min</td>
                <td>11 KG <br> 50 Min</td>
                <td>11 KG <br> 50 Min</td>
            </tr>
            <tr>
                <td>12MM</td>
                <td>12 KG <br> 55 Min</td>
                <td>12 KG <br> 55 Min</td>
                <td>12 KG <br> 55 Min</td>
            </tr>
            <tr>
                <td>20MM</td>
                <td>22 KG <br> 60 Min</td>
                <td>22 KG <br> 60 Min</td>
                <td>22 KG <br> 60 Min</td>
            </tr>
        </tbody>
    </table>
    <h3 id="playingwithcolors">Playing with colors</h3>
    <p>This is where there is room to play. You can basically mix any color as long as it is the same type of plastic. A few variables to play with, the size of the shredded plastic. Small, medium or large flakes will have a different effect. Also using transparent plastic in there gives an interesting layer of depth. More examples on the poster in the downloadkit</p>
    <p><img src="/assets/build/sheetpress-colors.jpg" alt="Sheetpress colors" /></p>
    <h3 id="howmanysheetsatatime">How many sheets at a time?</h3>
    <p>We recommend pressing only one sheet at a time. It may be possible to press more efficiently by melting multiple at a time but we have not figured out how to cool them evenly yet. If you figure out a way, please share it on our online community! </p>
    <h3 id="sheetsize">Sheet size</h3>
    <p>The aluminum pressing plates are 1220x1220mm but there is often an overflow from the mould so we recommend making moulds of 1040mm so that once they have shrunk from cooling they are just over 1m, then the edges can be trimmed to make a perfect 1mx1m square. Thickness can be between 4 to 35 MM.</p>
    <h1 id="usefullinks">Useful Links</h1>
    <ul>
        <li><a href="https://bazar.preciousplastic.com"> Buy or sell parts & machines on our bazar</a></li>
        <li><a href="https://community.preciousplastic.com/map"> Find a local machine shop on our map</a></li>
        <li><a href="https://community.preciousplastic.com/how-to"> Visit our how-to's for upgrades and hacks</a></li>
        <li><a href="spaces/sheetpress.md">How to setup a full Sheetpress workspace</a></li>
        <li><a href="https://discordapp.com/invite/XQDmQVT">For questions go to our #build channel in Discord</a></li>
        <li><a href="https://community.preciousplastic.com/how-to/make-a-chair-with-bent-sheets">How to make a chair with sheets</a></li>
        <li><a href="https://davehakkens.nl/community/forums/topic/v4-sheet-press-system/">Forum post about building the Sheetpress system</a></li>
        <li><a href="https://davehakkens.nl/community/forums/topic/sheet-press-mould-oven/">Forum post about building a big oven</a></li>
        <li><a href="https://davehakkens.nl/community/forums/topic/v4-sheet-press-system/">Forum post about an alternative version</a></li>
        <li><a href="https://community.preciousplastic.com/how-to/bend-plastic-sheets">How to bend sheets</a></li>
        <li><a href="https://community.preciousplastic.com/how-to/make-a-shelving-system">How to build a shelving system with sheets</a></li>
    </ul>
</div>

    <hr />

    

</div>
            <hr />
            
            <hr />

            <div prefix="/templates/jekyll/" file="used_in.md" context="machine global" class="fragment">
    <p>{% if page.usedin %}
    <h4>This is used in</h4>
    <div class="ty-vendor-plans small">
        {% for component in page.usedin %}
        {% for doc in site.machines %}
        {% if doc.product_id == component %}
        <div class="ty-grid-list__item_small">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover_small" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    {% endif %}</p>
</div>

        </div>

        {% if page.tabs.build != false %}
        <div class="tab-pane" id="build" role="tabpanel" aria-labelledby="build-tab">

            <div prefix="machines/sheetpress/cell/" file="build_issues.md" context="machine" class="fragment">
    <p><div prefix="/templates/jekyll/" file="build_issues_sheetpress.md" context="machine global" class="fragment">
    <h3 id="buildissues">Build Issues</h3>
    <div class="alert alert-danger" role="alert">
        Fatal: Drilling Cartridge heaters with a M12 thread leaves up to 0.4 mm play. It's impossible to use the mentioned heat glue to fill up this space since it's very dry and doesn't stick to anything. Instead, use a 11.75 drill and finish it with a 11.9mm drill. Please check the how-to section about make this faster on a lathe.
    </div>
    <div class="alert alert-danger" role="alert">
        Fatal: The press plate is more likely not flat and the used mounts won't flatten the plate. Use level setscrews at 9 points instead. Check the howtos.
    </div>
    <div class="alert alert-warning" role="alert">
        Warning: It's extreme heavy to assembly and build this machine without a forklift.
    </div>
    <div class="alert alert-warning" role="alert">
        Warning: It's unlikely to build the sideframe precise, put level setscrews at the feets of the sideframes.
    </div>
    <div class="alert alert-danger" role="alert">
        Fatal: It's unlikely to make good and precise sink holes into the press plate with a handdrill. The handdrill gets extreme hot already after 15 - 20 holes. Use M6 holes instead but also use a small table drill press.
    </div>
    <div class="alert alert-warning" role="alert">
        Warning: Please use at least M18 bolting to hold the upper press-plate.
    </div>
    <div class="alert alert-info" role="alert">
        Tip: Use laser cut bars for the press plate frame. This way the frame will be more precise since it's also more easy to weld.

        {% include product_image.html src="resources/press-plate-frame.JPG"  title="Press Plate Frame" %}

    </div>
</div></p>
</div>

            <div prefix="machines/sheetpress/cell/" file="build.md" context="machine" class="fragment">
    <h2 id="general">General</h2>
    <ul>
        <li><a href="">BOM - Bill of materials</a></li>
    </ul>
    <h3 id="buildvideo">Build Video</h3>
    <p><a href="https://www.youtube.com/watch?v=j3OctDe3xVk&feature=emb_title">Build Video</a></p>
    <ul>
        <li><a href="https://youtu.be/j3OctDe3xVk?t=213">Lower frame</a></li>
        <li><a href="https://youtu.be/j3OctDe3xVk?t=279">Side frames</a></li>
        <li><a href="https://youtu.be/j3OctDe3xVk?t=443">Press - Plate - Mount : <strong>Dont!</strong></a></li>
        <li><a href="https://youtu.be/j3OctDe3xVk?t=990">Final test</a></li>
    </ul>
    <hr />
    <h3 id="requirements">Requirements</h3>
    <p><table>
<thead>
<tr>
<th>Tool / Machine</th>
<th>Description</th>
<th>Price</th>
<th>Category</th>
</tr>
</thead>
<tbody>
<tr>
<td>Forklift</td>
<td>If you don't have good crane which can lift up to 200KG up to 1.70m you might look for a forklift.</td>
<td>700 Euro +</td>
<td>Assembly</td>
</tr>
<tr>
<td>11.75 mm Drill</td>
<td>This is needed for the cartridge heater blocks</td>
<td>20 Euro</td>
<td>Cartridge Heaters</td>
</tr>
<tr>
<td>11.90 mm Drill</td>
<td>This is needed for the cartridge heater blocks - for the final pass</td>
<td>20 Euro</td>
<td>Cartridge Heaters</td>
</tr>
<tr>
<td>Small Drillpress</td>
<td>That's a better choice over a hand drill which is getting very hot after 20 M6 holes alone.</td>
<td>250 Euro</td>
<td>Press Plate holes</td>
</tr>
<tr>
<td>Shop Press</td>
<td>This is needed to press cartridge heaters into the blocks. As alternative you try a bigger vise which is faster but may damage the heaters. Ideally you use both.</td>
<td>220 Euro</td>
<td>Cartridge Heaters</td>
</tr>
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</tbody>
</table></p>
    <p><a href="https://docs.google.com/spreadsheets/d/1lSuaTlKM3EEFC4LCiqUVU5M98ugaGAiQGYHQaabcFZo/edit?usp=sharing">Table Source</a></p>
    <div class="container">
        <div class="row">
            <div class="col">
                {% include product_image.html download=false size="medium" src="resources/mini_drillpress.JPG"  title="Mini Drill Press" %}
            </div>
            <div class="col">
                {% include product_image.html download=false size="medium" src="resources/forklift.JPG"  title="Forklift" %}
            </div>
            <div class="col">
                {% include product_image.html download=false size="medium" src="resources/press.JPG"  title="Shop Press" %}
            </div>
            <div class="col">
                {% include product_image.html download=false size="medium" src="resources/cheater-press.JPG"  title="Pressing cartridge heaters" %}
            </div>
        </div>
    </div>
    <hr />
</div>

            {% if page.gallery_drawings %}
            <h4>Drawings</h4>
            {% include gallery_drawings caption="" %}
            {% endif %}

        </div>
        {% endif %}

        <div class="tab-pane" id="resources" role="tabpanel" aria-labelledby="resources-tab">
            <span style="font-size:smaller">

                <div prefix="machines/sheetpress/cell/" file="product_resources.md" context="machine" class="fragment">
    <p><strong>Product Resources</strong></p>
    <ul>
        <li><a href="https://a360.co/2ZxZ8Jy">3D Preview</a></li>
        <li><a href="https://gitlab.com/plastichub/products/tree/master/products/sheetpress-cell/drawings">Drawings</a></li>
        <li><a href="https://gitlab.com/plastichub/products/tree/master/products/sheetpress-cell/cad">CAD model</a></li>
        <li><a href="https://library.plastic-hub.com/machines/sheetpress-cell.html">Wiki</a></li>
        <li><a href="https://gitlab.com/plastichub/products/tree/master/products/sheetpress-cell">Source files</a></li>
        <li><a href="https://discord.gg/SN6MT5N">Discord Chat</a></li>
        <li><a href="">Wiring</a></li>
        <li><a href="">BOM - Bill of materials</a></li>
    </ul>
</div>

                

                <h4>Changelog</h4>

                <div prefix="machines/sheetpress/cell/" file="changelog.html" context="machine" class="fragment"></div>

            </span>

        </div>
        <div class="tab-pane" id="howtos" role="tabpanel" aria-labelledby="howtos-tab">
            <div prefix="machines/sheetpress/cell/" file="howtos.md" context="machine" class="fragment"></div>
            <h4>Build Howtos</h4>
            <div prefix="/templates/jekyll/" file="howto_used_in_machine.md" context="machine global" class="fragment">
    <div class="ty-vendor-plans">

        {% for doc in site.howto %}
        {% if doc.usedin and doc.enabled!=false %}
        {% for used in doc.usedin %}
        {% if used == page.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endif %}
        {% endfor %}
    </div>
</div>
        </div>
        <div class="tab-pane" id="media" role="tabpanel" aria-labelledby="media-tab">
            
            {% if page.gallery %}
            {% include gallery caption="" %}
            {% endif %}

            {% if page.gallery_social %}
            <h4>Media on the network</h4>
            {% include gallery_social caption="" %}
            {% endif %}

        </div>

        <div class="tab-pane" id="discussion" role="tabpanel" aria-labelledby="discussion-tab">
            <div id='discourse-comments' style="min-height: 600px;"></div>
            <script type="text/javascript">
                DiscourseEmbed = {
                    discourseUrl: 'https://forum.precious-plastic.org/',
                    discourseEmbedUrl: '{{site.url}}{{page.url}}.html'
                };

                (function() {
                    var d = document.createElement('script');
                    d.type = 'text/javascript';
                    d.async = true;
                    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
                })();
            </script>
        </div>

        {% if page.wiring %}
        <div class="tab-pane" id="wiring" role="tabpanel" aria-labelledby="wiring-tab">
            <div prefix="machines/sheetpress/cell/" file="wiring.md" context="machine" class="fragment">
    <p><div prefix="machines/sheetpress/cell/" file="Schematics380V.md" context="machine" class="fragment">
    <h3 id="wiring380v">Wiring - 380V</h3>
    <p>{% include product_image.html src="resources/circuit_1.jpg"  title="Circuit Diagram" target="resources/circuit.pdf"  %}</p>
</div></p>
</div>
        </div>
        {% endif %}

    </div>

    <div prefix="/templates/jekyll/" file="alternative_machines.md" context="machine global" class="fragment">
    <p>{% if page.alternatives %}</p>
    <h3 id="alternatives">Alternatives</h3>
    <div class="ty-vendor-plans">

        {% for doc in site.machines %}
        {% for alternative in page.alternatives %}
        {% if alternative == doc.product_id %}
        <div class="ty-grid-list__item">
            <a href="{{ doc.url  | relative_url }}" class="link">
                <span class="image">
                    <img class="cover" src="{{ doc.image }}" alt="" />
                </span>
                <header class="major">
                    {{ doc.title }}
                </header>
            </a>
        </div>
        {% endif %}
        {% endfor %}
        {% endfor %}
    </div>
    <p>{% endif %}</p>
</div>
</div>


</div>