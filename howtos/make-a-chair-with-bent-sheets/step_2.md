### Cut your parts 

 Before cutting to the exact shape, it’s easier to prepare simple wooden parts for the guides. 

Take the part list and the drawings and prepare all the wooden parts (number 1-14) needed for jigs and moulds. Make sure to put the corresponding part number on each piece, so you don’t mix them up later. Also mark the center line on parts 4 and 6-8.
