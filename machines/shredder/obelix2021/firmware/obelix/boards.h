#ifndef BOARDS_H
#define BOARDS_H

// back - compat
#if ARDUINO > 22
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    #define BOARD_MEGA
#endif

#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
    #define BOARD_MEGA
#endif

#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega16U4__)
    #define BOARD_LEONARDO
#endif

#if defined(CONTROLLINO_MINI)
    #define BOARD_CONTROLLINO_UNO
  #elif defined(CONTROLLINO_MAXI) 
    #elif defined(CONTROLLINO_MEGA) 
      #elif defined(CONTROLLINO_MAXI_AUTOMATION) 
        #define BOARD_CONTROLLINO_MEGA
      #else 
        #error Please, select one of the CONTROLLINO variants in Tools->Board
#endif

#endif