### GET READY TO INJECT THOUSANDS! 

 Once you understand the process fully, it’s time to tailor to your needs, your machines, moulds and products so you can reach the targets you set. Automating part of your process might be a good idea once you start selling regularly.
And don’t forget to pay a lot of attention to the colour formulas for your products, it’s fun!
