---
image: "/howtos/solar-plastic-injection-/image82.png"
title: "Solar Plastic Injection"
tagline: ""
description: "Today volks.eco and PlastOK are proud to share with you a solution to inject some precious plastic parts using only solar and human power.<br />We believe this method will spread further the precious plastic concept and increase the potential use of plastic waste anywhere in the world.<br />This is our first step in the solar precious plastic experience.<br />Don’t forget your sunscreen, sunglasses and enjoy !"
keywords: "melting,injection,research,other machine"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
- "injection"
- "research"
- "other machine"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/plastok">plastok</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prepare the setup</h4>
<div class="step-text">
  You need :<br />
  <br />
  - volks.eco solar concentrator <br />
  - Hydraulic Jack press<br />
  - Cartridge made with a plumbing pipe<br />
  - Shredded HDPE<br />
  - Injection mold<br />
  - Cup of water<br />
  - Oven gloves<br />
  - Clamp<br />
  <br />
  Find a sunny place and preheat the solar tube concentrator
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/SetUp.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/SetUp.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/DJI_0201.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/DJI_0201.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/a_sunny_place.png">
        <img class="step-image" src="/howtos/solar-plastic-injection-/a_sunny_place.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Load the cartridge</h4>
<div class="step-text">
  Load the shredded plastic inside the iron cartridge.<br />
  <br />
  Compress as much as you can the needed amount of plastic depending on the volume of your part, inside the cartridge.<br />
  <br />
  Lock the plumbing pipe with ½&quot; of cap ends.<br />
  <br />
  Load the cartridge inside the solar concentrator.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2255.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2255.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/fill_the_cartridge.png">
        <img class="step-image" src="/howtos/solar-plastic-injection-/fill_the_cartridge.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2256.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2256.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Heating the Cartridge</h4>
<div class="step-text">
  Wait the time necessary depending on the sunlight exposure (between 30 and 60 min).<br />
  <br />
  You can load several iron cartridge inside the solar concentrator to speed up the process.<br />
  <br />
  Use a temp prob inside of the heated tube to manage the temperature of the cartridge.<br />
  <br />
  The concentrator can heat up between 180-250°C.<br />
  <br />
  There is a lot to learn with this method and a lot of inputs to bring on the table.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2282.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2282.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2285.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2285.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/Temp Prob.webp">
        <img class="step-image" src="/howtos/solar-plastic-injection-/Temp Prob.webp" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Screw cartridge & mold</h4>
<div class="step-text">
  The cartridge will be really hot, USE GLOVES !<br />
  <br />
  Remove the two cap ends of the hot cartridge.<br />
  <br />
  Plunge one extremity of the tube in the cup of water.<br />
  <br />
  By putting the end of the tube in the water, you will make a hard plastic piece that will push the melted plastic inside of the mold.<br />
  <br />
  NB : Preheating the mold above 60°C helps a lot to obtain a nice finished part.<br />
  The preheating is easily achieved by putting the mold a few minutes in the solar concentrator.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2298.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2298.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2296.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2296.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2305.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2305.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Inject manually</h4>
<div class="step-text">
  Inject the plastic inside the mold, the quicker you can, the better it is.<br />
  <br />
  We will share how to build our portable hydraulic jack press next time.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/P1400314.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/P1400314.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/P1400284.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/P1400284.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/injection.webp">
        <img class="step-image" src="/howtos/solar-plastic-injection-/injection.webp" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Extract the injected part </h4>
<div class="step-text">
  Be sure to have cooled down the mold (if needed you can put it in cold water) before extracting your injected part.<br />
  <br />
  Now you have a nicely injected finished part made only with human and solar power !<br />
  <br />
  Mechanical improvement has to be made with the mold for the extraction, it&#39;s just a prototype for this proof of concept
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/open_the_mold.png">
        <img class="step-image" src="/howtos/solar-plastic-injection-/open_the_mold.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2363.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2363.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/solar-plastic-injection-/IMG_2358.JPG">
        <img class="step-image" src="/howtos/solar-plastic-injection-/IMG_2358.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Recap</h4>
<div class="step-text">
  This video shows the whole process and the time expected for every step. (It works, but we can improve it).<br />
  <br />
  The solar concentrator is a solution that doesn&#39;t need any electricity. <br />
  <br />
  You can setup this everywhere (beach, mountain, city) in any season, you just need a little bit of sun.<br />
  <br />
  If you are interested in the solar concentrator, feel free to visit volks.eco and contact my friend Marco.<br />
  <br />
  He has been working on this nice solar concentrator for a long time now, and it can be used in so much other ways.<br />
  <br />
  I hope you find your way with it and that it will open doors on the potential of solar energy for plastic recycling.<br />
  <br />
  Thanks Marco for your input and Precious Plastic for all !
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>