### Weld the mould 

 Now that we have the two parts of the mould, we will use the holes of part 1 of the mould to drill holes into L angles, to be able to attach the two parts of the mould. Once all the holes are drilled, we can fasten them with M6 x 30mm bolts and nuts. 

