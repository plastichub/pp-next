### Connect to other workspaces 

 Find shredder (or other) workspaces around you who can shred all the plastic you’ve collected. You can find them on the Precious Plastic Map or have a look on other platforms. Develop a nice relationship as you’ll want to work as partners in crime :)

👉 community.preciousplastic.com/map