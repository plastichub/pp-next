### Bins 

 We’ve distributed over 60 bins across town to various hotels, restaurants, resorts, mosques and shops. We’ve given them out free of charge but in hindsight we would rather hand them out with a deposit. A nice paint job also helps sending the message. Our bins have a big sticker indicating the plastic we collect and also has a message that encourages people to search for alternatives. 
