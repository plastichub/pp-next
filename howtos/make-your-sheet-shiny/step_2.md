### From sanding to polishing 

 For a smooth and even surface we first have to sand it with low density grain sanding paper (from 200-800). This will remove the surface layer and clean the sheet from scratches and tiny holes.

For deeper scratches you’ll have to spend more time or even use lower density sanding paper. Once the scratches are no longer visible you can use higher density grain papers in order to polish the surface.

In the case of hard plastics like PS you can get shiny results while on HDPE or PP they will remain matt.