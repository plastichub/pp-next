---
image: /_howto/zoe/fence/IMG_2169.JPG
category: "zoe"
title: "Fence"
tagline: ""
description: ""
tags :
 - v3
 - zoe
categories:
  - Shredder
usedin:
  - zoe
  - shredder_v31

---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2163.JPG" _target="_blank">
        <img id="IMG_2163.JPG" src="./IMG_2163.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2164.JPG" _target="_blank">
        <img id="IMG_2164.JPG" src="./IMG_2164.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2166.JPG" _target="_blank">
        <img id="IMG_2166.JPG" src="./IMG_2166.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2167.JPG" _target="_blank">
        <img id="IMG_2167.JPG" src="./IMG_2167.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2168.JPG" _target="_blank">
        <img id="IMG_2168.JPG" src="./IMG_2168.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2169.JPG" _target="_blank">
        <img id="IMG_2169.JPG" src="./IMG_2169.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

