---
image: "/howtos/layout-your-plastic-/Let’s make plastic more understandeble .jpg"
title: "Layout your plastic"
tagline: ""
description: "Most of the people don’t even realize how much plastic waste they create. The throw-away culture makes it happen that you leave this waste behind. This makes us, in a lot of moments, just skipping the moment of actually taking a look at what we throw away. <br /><br />By laying all the collected plastic waste out and sorting it in categories it will give a clear overview. This can be done at a Collection Point or even after a cleanup. Lay it out and explain/talk/share with the people around you."
keywords: "collection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "collection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Choose your categories </h4>
<div class="step-text">
  The first step of this how-to is, choosing a couple of categories that you want to sort the plastic in. This can be only the ones that you will recycle yourself or the most common ones PETE, HDPE, PVC, LDPE, PP, PS, and others. You can even add a section with “to dirty” or “to mixed”.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/Choose the categories.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/Choose the categories.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Measure the space that is needed</h4>
<div class="step-text">
  By letting someone in the local area collect their plastic waste for a week or 2, you will know how much space you will need to lay this out in the layout. Layout their collected plastic in the categories that you choose. Start to measure the outlines per category and the outline of the whole layout.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/Measure the space.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/Measure the space.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/plastic pile.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/plastic pile.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Choose a good spot</h4>
<div class="step-text">
  Now the size of the layout is clear we can go to find the right spot to place the layout. It’s nice to have enough space around the layout to be able to move and take pictures. It will make sense to keep the layout close to the collection bags, so the visitor can after seeing his plastic, throw it into the bags to be recycled.<br />
  <br />
  Tip: You can also make it to the wall and play with shelves to make it easier to take photos of. It can be done in lots of different ways to be creative and share your results with the community.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/-Howto - annotations template.png">
        <img class="step-image" src="/howtos/layout-your-plastic-/-Howto - annotations template.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Mask the outlines</h4>
<div class="step-text">
  It’s the moment to mask the tape on the floor. Follow the measurements that you wrote down earlier. Line out the rectangles by using a measuring tape/90-degree corner and paste the masking tape on the lines. Make sure that you press all the tape nicely, so it doesn’t come off too easily.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/Start masking the tape on the right spot 2.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/Start masking the tape on the right spot 2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/Start masking the tape on the right spot.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/Start masking the tape on the right spot.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Make a sign for every category </h4>
<div class="step-text">
  In this step, it’s time to visualize each category by an illustration or written sign. This can be done on a piece of paper (something temporary) or being painted on the floor (something permanent). Make sure that you choose a color that has good contrast with the background, this will make the layout clear even when people take photos from far.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/Give every square its own purpose 3.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/Give every square its own purpose 3.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/Give every square its own purpose 2.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/Give every square its own purpose 2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Take a step back and look</h4>
<div class="step-text">
  Finally, the moment is there to use this plastic layout tool. You can use it for example, when a collector comes in your Collection Point and he/she has time to listen to some more information about plastic. Explain how to separate the collected plastic by the categories. <br />
  <br />
  Tip: Give a hand where needed, it’s fun to do this together.<br />
  <br />
  After sorting you can both take two steps backward and look at the overview of the laid out plastic. Start a little conversation about the result and the differences in the products that you see. It&#39;s your moment to shine with all your knowledge about plastic, go for it and share :) If there is even more time you can give some plastic-free examples to reduce plastic use. <br />
  <br />
  Tip: Let the collector take a photo of the overview to share on, for example, social media.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/test.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/test.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/Let’s make plastic more understandeble  2.jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/Let’s make plastic more understandeble  2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/layout-your-plastic-/Let’s make plastic more understandeble .jpg">
        <img class="step-image" src="/howtos/layout-your-plastic-/Let’s make plastic more understandeble .jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>