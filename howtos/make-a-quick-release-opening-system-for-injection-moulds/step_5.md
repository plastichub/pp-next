### Close and open the mould 

 That’s it, now you have all the parts you need.

To close the mould, slide the clamps into the slots, and screw them in until you can tighten them enough  to easily tighten and loosen with the clamps.

Ready to inject!
