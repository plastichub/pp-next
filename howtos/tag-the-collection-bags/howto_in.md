### Tag the collection bags 


In this how-to, you will learn how to make tags for your collection bags. The tags are attached to the bags during transport. This way the Shredder Workspace will not confuse the plastic-type waste from the Collection Point. 