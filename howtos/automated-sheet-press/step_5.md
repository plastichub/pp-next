### Digital interface 

 The digital interface runs on a Raspberry Pi that is connected with a touch screen. It is split up into two parts, the server part and the client side. The client side is a website that you can access from the Pi’s browser or from any device that is connected to the wifi on the raspberry Pi. The server is made with Nodejs. 
