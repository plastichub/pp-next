### Prepare your materials 

 For your workshop you need shredded plastic and some clean plastic which is ready to be shredded. Reserves are always good, maybe the shredder stops working and you are running out of fresh granules…  Be prepared!