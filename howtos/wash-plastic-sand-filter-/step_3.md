### Build the inside with PVC pipes 

 The heart of the filter is the pipe system inside. PVC pipes offer an available, easy to handle and robust solution for that. You can cut them with a hand saw or a metal chop saw and seal them with PVC glue. To connect them to the barrel you need to find threaded Bulkhead fittings with a PVC adapter.

As the bottom pipe lies beneath the sand, we need to avoid sand getting in there, but the water should pass easily. A fine mesh, from stainless steel or plastic, works the best.

It is important to have a big opening at the bottom pipe, so the water can be pushed through the sand more easily. Big PVC adapter pieces work very well for this purpose, as they are available with a lid, which is perfect to keep the mesh in place. If not available, there are many other ways to reach the same results. Just keep in mind to create a big area for the water to pass through and a mesh or small holes to keep the sand out of the tube.

The size of the pipes should be 40 mm, but if you can only get 30 mm it will also work. On the sketches, you see the exact measurements the pipes need to have to fit into the barrel and the holes we drilled before. You can also see what kind of fittings to get (feel free to improvise here).