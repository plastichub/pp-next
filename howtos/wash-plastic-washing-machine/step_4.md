### Electronics 

 This step is still under development and has to be taken fully on your own risk. In our case, it worked but the box would need a better layout design to make it safer. 
What we are doing is:
The main switch which turns on/off everything, this means we also lead the power cable from the washing machine into this box. We have a switch that changes the state of the pump between always on and on on-demand. When the pump turns on, also the solenoid valve needs to open, so we reach that with a 220V to 24VAC transformer, which also interacts with the relay. It doesn’t open when the pump is in the function always on. I also moved the electronics from the pump into my box, to keep everything together.
If you also have a Pre-Washing Station with a heated water barrel going, then you should also move the PID controller and a switch for it into this box. 
❗Again to emphasize, this is not a professional setup and you should act with care and knowledge. 
