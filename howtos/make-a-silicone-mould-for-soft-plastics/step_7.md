### Using the Mould 

 Once the mould is released you are ready to begin using it.

Please note; this mould is suitable for soft plastics. This mould is best when plastic is hand-pressed into the mould. Be sure that you are using heat and chemical resistant gloves when handling the plastic. 

For this mould I have reused plastic from another project to melt and press into my mould. 
You may need to sand or cut off some messy edges to complete the look you want.

Silicone will capture any surface as will your plastic when being moulded so go big or go home!


Note: I have only used soft plastics (LDPE & HDPE) with this technique but please feel free to experiment.