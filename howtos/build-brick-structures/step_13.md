### Framing the wall 

 Now that you have reached your desired height you may need to attach columns. To do this we cut the wood to a desired length and then screw to the timber base or bricks ensuring the column is straight with a spirit level.
