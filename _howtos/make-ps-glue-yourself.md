---
image: "/howtos/make-ps-glue-yourself/howto-glue.png"
title: "Make PS glue yourself"
tagline: ""
description: "PS dissolves in acetone creating a thick mixture that could be used as a gap filler or as a glue. In this case we will use D-Limonene, a natural solvent which creates strong chemical bonding on PS."
keywords: "melting,PS"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "melting"
- "PS"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get your materials</h4>
<div class="step-text">
  For this process we will need the following materials and equipment.<br />
  <br />
  Materials needed to make the glue: <br />
  - D-limone <br />
  - EPS (Styrofoam) <br />
  - glass jar with lid <br />
  <br />
  Materials/tools needed for application:<br />
  - IPA (Isopropyl Alcohol) cleaner<br />
  - Scotch Brite (or similar)<br />
  - stick or brush to spread<br />
  - clamp to press together<br />
  - stick to stir<br />
  <br />
  Recommended safety equipment:<br />
  - ABEK mask<br />
  - gloves<br />
  - safety glasses
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-ps-glue-yourself/howto-glue-materials.png">
        <img class="step-image" src="/howtos/make-ps-glue-yourself/howto-glue-materials.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Take the right precautions first</h4>
<div class="step-text">
  When making the glue it’s recommendable to work in a well ventilated space, preferably in a fume hood. <br />
  D-Limonene is hazardous as an irritant in cases of eye contact, skin contact, and inhalation, so make sure to use an ABEK mask, safety glasses and gloves in order to prevent irritations.<br />
  <br />
  For safety, before working with D-limonene, read the SDS sheet from the packaging. D-Limonene is flammable in presence of open flames and sparks. It must be kept away from flames at all times, however a mix of. PS and d-limonene is not flammable.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-ps-glue-yourself/howto-glue-precautions.png">
        <img class="step-image" src="/howtos/make-ps-glue-yourself/howto-glue-precautions.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make the mixture</h4>
<div class="step-text">
  Get your glass jar (here is where we are going to contain the glue so make sure to use a jar that allows you to access the glue comfortably afterwards)<br />
  <br />
  Put in D-Limonene (approx 25ml)<br />
  Add EPS (Styrofoam) until it forms a thick translucent glue<br />
  The right amount is 16g for 25ml of D-limonene (Density: 0.8411 g/cm3). That makes a ratio in g of 0.74 (eps/d-limonene). <br />
  <br />
  Once the EPS is mostly dissolved, immediately close the jar. D-Limonene is a solvent that evaporates in contact with the air, so make sure it’s airtight. <br />
  <br />
  Finally let the mixture rest 24h to get a consistent glue. D-limonene by itself takes a long time to fully dissolve EPS but this research explains how to shorten the drying time and at the same time increase its viscosity diluting the solution with water.<br />
  <br />
  The resulting optimal glue contains 2g polystyrene, 2ml water, 0.2g lecithin and 4ml D-Limonene.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-ps-glue-yourself/howto-glue-mixture-1.jpg">
        <img class="step-image" src="/howtos/make-ps-glue-yourself/howto-glue-mixture-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-ps-glue-yourself/howto-glue-mixture-2.jpg">
        <img class="step-image" src="/howtos/make-ps-glue-yourself/howto-glue-mixture-2.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Apply the mixture</h4>
<div class="step-text">
  Once the glue is ready and before applying it, make sure to clean the surfaces you are going to glue together. Use a non-aggressive product like IPA (Isopropyl Alcohol) cleaner and a Scotch Brite (or similar) to remove any dirt or release agent left from the mould. <br />
  <br />
  Use a stick or an old brush to spread the adhesive on the surfaces and clamp the parts together. <br />
  <br />
  Leave them curating for around 24h, at ambient temperature (around 15º-20º) to be sure that the parts are completely glued.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-ps-glue-yourself/howto-glue-apply-1.jpg">
        <img class="step-image" src="/howtos/make-ps-glue-yourself/howto-glue-apply-1.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-ps-glue-yourself/howto-glue-apply-2.jpg">
        <img class="step-image" src="/howtos/make-ps-glue-yourself/howto-glue-apply-2.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-ps-glue-yourself/howto-glue-apply-3.jpg">
        <img class="step-image" src="/howtos/make-ps-glue-yourself/howto-glue-apply-3.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Store</h4>
<div class="step-text">
  Make sure to store it really airtight. Shelf-life can range from 3-6 months. <br />
  <br />
  But the glue can remain without much change for months granted if it’s well sealed. It might be useful to add some water if planned to leave for a long time as the glue might harden.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-ps-glue-yourself/howto-glue-end.jpg">
        <img class="step-image" src="/howtos/make-ps-glue-yourself/howto-glue-end.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>