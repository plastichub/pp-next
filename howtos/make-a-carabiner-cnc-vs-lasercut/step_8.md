### Mark your products! 

 Last but not least, stamp your carabiners!

This is one of the most important steps and at the same time, the most forgotten. It’s very important to stamp your plastic in order to be able to recycle it in the future if its needed.

You can either engrave it by hand with a dremel directly into your mould or make a stamp which you can also use for other products.

Here you can find a tutorial on how to make a simple wire stamp:
👉 tiny.cc/wire-stamp