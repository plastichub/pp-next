# Feed system for Noah

## Exploring Ideas

The choice of feed system will be influential to the design of the print head, and to Noah's overall design and delpoyment. 

The main choice is between a hopper-fed system or a filament-fed system.
Examples below concentrate on high volumetric rate of extrusion.

### Hopper fed

Pros:
- Minimal preparation of raw material for printing is most efficient
- Some say faster extrusion rates are possible with pellets/flakes

Cons:
- More difficult to handle - Constant automated hopper feed may be mechanically difficult to achieve
- Material (PET in particular) needs to be ground very finely and when powderized needs to be kept very dry

Opitions:

- Direct screw extrusion from shredded plastic from PP shredder; (Ideal candidate but possibly most challenging)
- Screw Extrusion from powderized PET using upgraded PP shredder; using [Robodigg extruder screw](https://www.robotdigg.com/product/1691/16mm-or-20mm-extruder-screw,-barrel-n-nozzle)
- Extrusion from extruded and pelletized plastic from PP extruder;

Existing Models:

The [Dyze Pulsar](https://dyzedesign.com/pulsar-pellet-extruder/) is designed for large scale 3d printing and outputs 500m3/s (2.5kg/hr)


### Filament fed

Filament-fed extruder screws have a feed section to allow filament to be drawn into the barrel as it winds around the narrow feed thread.

![filament feed screw](https://gitlab.com/plastichub/noah/blob/master/machines/noah/documents/printhead/filamentfeedscrew.jpg)

Pros:

- Simple and consistent feed rate with minimal additional mechanics - [hobbed drive gears](https://reprap.org/wiki/Drive-gear) driven by a small stepper can control feed rate precisely- but may not be required when using a feed screw.
- Simple delivery from a spool via ['Bowden' tube](https://all3dp.com/2/bowden-tube-all-you-need-to-know/)
- Can be adapted easily from existing units eg. the Robodigg Screw, Electric drill, fan heater
- Potential to emulate many methods used in conventional desktop 3d printing such as filament splicing for colour changes
- Could stimulate a new market for PP wide-gauge filament, producing income for PP workshops (PP filament Hub?)
- Probably would make a lighter print head?

Cons:

- Melting plastic twice requires twice the energy input
- Producing uniform filament is challenging in itself (but certainly possible)

Options-

- Screw extrusion of filament made with PP extruder, using modified [Robotdigg extruder screw](https://www.robotdigg.com/product/1691/16mm-or-20mm-extruder-screw,-barrel-n-nozzle) at the print head hot end;
- Screw extrusion of filament made with PP extruder, using a second PP extruder at the print head hot end;
- Screw extrusion of filament made with PP extruder, using custom design extruder based around the 
[extrusion screw](https://www.aliexpress.com/i/32789186227.html) for [Lesite/WELDY/Weldplast handheld extrusion welding guns](https://www.youtube.com/watch?time_continue=6&v=riEnKMjkegI&feature=emb_logo). Lesite models appear to have a separate [feed screw sheave](https://www.aliexpress.com/item/32788433148.html)

![feed screw sheave](https://ae01.alicdn.com/kf/HTB1zPbnPXXXXXbvapXXq6xXFXXX8/201140567/HTB1zPbnPXXXXXbvapXXq6xXFXXX8.jpg?size=17565&height=284&width=362&hash=0cba07aa6fee321d959aa8e163cd2c47)

Existing Models:

Some commerical extruders operate using only hot air to pre-heat the barrel; most of the work melting the plastic is done by the friction and pressure within the barrel.

Handheld commerical units are often driven by standard electric hand drills with apparently no modification. This possibly indicates a relatively high screw speed, which delivers high friction and pressure.

The company Leister currently produces the [WELDPLAST 200-i / 600-i](https://www.leister.com/en/plastic-welding/products/extrusion-welders/weldplast-200-i-600-i) which is capable of exruding 2kg per hour and is designed for large scale 3d printing.

The [HFE 900](https://3dplatform.com/extruders/) is an industrial extruder capable of 273mm3/s (~1.3kg/h) and costs $10k lol

The [Demtech Pro x-5](http://www.demtech.com/product/pro-x5-extrusion-welder/) can output 5kg /hr using filament. This was one of the highest output rates of all commercial units surveyed, and the detailed [user manual](http://www.demtech.com/wp-content/uploads/2015/01/600-Manual_Pro-X5-RA-High-RES.pdf) available makes it a good candidate for reverse-engineering.

[Video](https://www.youtube.com/watch?v=c9nRSHSPE44)

![ProX5 barrel](https://gitlab.com/plastichub/noah/blob/master/machines/noah/documents/print%20head/pro%20x5%20barrel%20screw%20and%20heaters.jpg)Exploded diagram of the Pro x-5 extruder barrel showng the screw with filament feed section. (User manual, p25)


For reference, the manual states this unit requires a 6.5kW power supply.

The highest output rate of all units surveyed was the [Leister Weldplast S6](https://www.leister.com/en/plastic-welding/plastic-fabrication/products/extrusion-welders/weldplast-s6), giving 6kg/hour.


Notes: 

- All the handheld units use either hot air only, or a combination of hot air and heating bands, to melt the plastic intially., but most of the heat required to melt the plastic is generated through friction and pressure using a higher screw speed and a relatively small nozzle size. High pressure allows a wider print line to be extruded through a smaller nozzle, as a result of [Extrudate Swell or "die swell"](https://www.youtube.com/watch?v=hcU4_cYidqI) (8m20s mark)

The hot air in these units is also directed onto the workpiece around the extruded filament and infront of the tool path. This increases layer adhesion as the substrate surface is slightly melted. This allows HDPE filament to be used. This is a factor which has apparently not yet been broadly developed in large scale 3d printing:

[Struggling to 3d print a boat with HDPE](https://www.youtube.com/watch?v=7QFqtEP2ErI)

It is predicted that, just as layer cooling fans are an important feature of desktop 3d-printers, layer *heating* will be an important feature of large-scale 3d printing with trash.


Ideas for the future:

- Filament extruders above use only a single filament feed; what about using 2 filaments entering the barrel on opposite sides?

- Increase volumetric rate and mix colours by using [multiple extruders combining into a single nozzle](https://www.reprap.me/diamond-fullcolor-hotend.html)

- Print multiple objects or integrated lattice structures by using an array of extuders at fixed intervals on one axis, sharing the movement of a single print head?

- ['Dot Matrix' style printing](https://www.youtube.com/watch?v=VXntl3ff5tc&feature=youtu.be) using a wide array of individually controlled nozzles 

- Melt filament using a high powered laser. This would concievably produce highly repeatable results with very good energy efficiency, but would require an advanced development project of its own!
