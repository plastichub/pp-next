---
image: /_howto/extrusion/v4_screw/IMG_2486.JPG
category: "extrusion"
title: "V4 Screw"
tagline: ""
description: ""
title: "V4 Extrusion Screw Shaft"
usedin:
  - lydia-v4
  - extrusion-pro
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_2471.JPG" _target="_blank">
        <img id="IMG_2471.JPG" src="./IMG_2471.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2472.JPG" _target="_blank">
        <img id="IMG_2472.JPG" src="./IMG_2472.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2473.JPG" _target="_blank">
        <img id="IMG_2473.JPG" src="./IMG_2473.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2474.JPG" _target="_blank">
        <img id="IMG_2474.JPG" src="./IMG_2474.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2475.JPG" _target="_blank">
        <img id="IMG_2475.JPG" src="./IMG_2475.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2476.JPG" _target="_blank">
        <img id="IMG_2476.JPG" src="./IMG_2476.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2478.JPG" _target="_blank">
        <img id="IMG_2478.JPG" src="./IMG_2478.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2480.JPG" _target="_blank">
        <img id="IMG_2480.JPG" src="./IMG_2480.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2484.JPG" _target="_blank">
        <img id="IMG_2484.JPG" src="./IMG_2484.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2485.JPG" _target="_blank">
        <img id="IMG_2485.JPG" src="./IMG_2485.JPG" width="100%" />
    </a>
    <span class="thumb-label">10. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_2486.JPG" _target="_blank">
        <img id="IMG_2486.JPG" src="./IMG_2486.JPG" width="100%" />
    </a>
    <span class="thumb-label">11. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

