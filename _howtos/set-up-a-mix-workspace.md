---
image: "/howtos/set-up-a-mix-workspace/cover-mix-3.jpg"
title: "Set up a Mix Workspace"
tagline: ""
description: "In this How-to we’re going to guide you through all the steps to setup a Mix Workspace. Learn about plastic, how to find a space, get the machines and connect to the Precious Plastic Universe.<br /><br />Download files:<br /> &lt;a href=&quot;https://cutt.ly/starterkit-mix&quot;&gt;https://cutt.ly/starterkit-mix&lt;/a&gt; <br /><br />Step 1-3: Intro<br />Step 4-9: Learn<br />Step 10-16: Set up<br />Step 17-22: Run<br />Step 23-25: Share"
keywords: "starterkit"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "starterkit"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Role in the Precious Plastic Universe</h4>
<div class="step-text">
  First of all, make sure you had a look at the showcase page of this Starterkit!<br />
  👉 preciousplastic.com/starterkits/showcase/mix<br />
  <br />
  Mix Workspaces are a great place to start making the first steps into local plastic recycling.<br />
  <br />
  As they have all several machines and take care of several steps of the recycling process, they are quite independent from other workspaces, but ofcourse can still collaborate with Collection Points or Shredder Workspaces to source their material.<br />
  <br />
  As everyone in the network, they should be in touch with the Community Points to connect with the local Precious Plastic community.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/universe-mix.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/universe-mix.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Outcomes</h4>
<div class="step-text">
  The Mix Workspace can have multiple outcomes. The knowledge and education generated for its participants and the broader society are huge. Amongst other 5 things Mix Workspaces can output small productions, workshops or presentations.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image2.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image2.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Is this for you?</h4>
<div class="step-text">
  If you want to start a Mix Workspace the most important aspect is to be super keen on learning this new craft, local plastic recycling. <br />
  <br />
  A technical background or interest could be a plus but is not mandatory as you could also get the machines built for you. As a plus, some product design knowledge could help you when you start making products. <br />
  <br />
  Working on a Mix Workspace you could be dealing with people on a daily basis for supplies, workshops, visits and more. It’s good to be a little sociable and enjoy explaining the project to others.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image8.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image8.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Learn about Precious Plastic</h4>
<div class="step-text">
  As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions.<br />
  <br />
  If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.<br />
  👉 preciousplastic.com<br />
  👉 community.preciousplastic.com<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.<br />
  👉 community.preciousplastic.com/academy/universe/universe
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/precious-plastic-universe.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/precious-plastic-universe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Check out your area</h4>
<div class="step-text">
  Get an overview of who and what is already existing in your area.<br />
  <br />
  Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. <br />
  <br />
  Make sure not to jam the local network, if there are already five Mix Workspaces around you, have a chat about a collaboration with them first, or maybe consider starting another type of space.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/communitypoint-step4-map-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Find a Community Point near you</h4>
<div class="step-text">
  Community Points are the glue of the Precious Plastic Universe. They know the ins and outs of your local network.<br />
  <br />
  They can help you in multiple ways but they generally have a very in-depth overview of local Precious Plastic spaces, people, useful shops, resources and can help you with planning out your project. You can find them on the map here. <br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image10.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image10.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Get on Discord</h4>
<div class="step-text">
  Day to day discussions in the Precious Plastic Universe happen on Discord.<br />
  <br />
  Introduce yourself, say hi in your country channel and start to discover the different channels where people go deep into specific topics (building, collection, design etc..)<br />
  <br />
  Join the Discord:<br />
  👉 discordapp.com/invite/rnx7m4t
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image21.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image21.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - The basics of plastic</h4>
<div class="step-text">
  Before you start it is crucial to get a solid understanding of plastic. How it works, the different types, melting temperatures and so on. Head over to our Academy and dive into the plastic chapters to learn about the different types and properties etc.<br />
  <br />
  There is also more about the machines, products and techniques, and more. Make sure to absorb as much knowledge from there as possible. <br />
  <br />
  👉 <a href="http://tiny.cc/basics-about-plastic">http://tiny.cc/basics-about-plastic</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image12.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image12.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Assemble a team</h4>
<div class="step-text">
  It’s good to work and learn in a team.<br />
  <br />
  Put together a passionate team of people able to collaborate and work together to learn different ways to tackle the plastic problem.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/team.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/team.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Find a space</h4>
<div class="step-text">
  To help you find and set up the space for your workspace, you can use the floorplan in the Download Kit. It includes all the minimum requirements and a little template to place your machines and tools in the workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image9.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image9.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Get your machines</h4>
<div class="step-text">
  Cool, now that you have a space it’s time to get hold of your machines of choice. There is no right or wrong way to go here. tWe always recommend to start small to keep the investment lower, but you can also go full in from the beginning.<br />
  <br />
  There are three ways you can get the machines:<br />
  <br />
  1 Build them yourself following our tutorials.<br />
  👉 tiny.cc/build-machines<br />
  <br />
  2 Buy them on the Bazar.<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  3 Find a Machine Shop near you on the map that can build it for you.<br />
  👉 community.preciousplastic.com/mapBuilt them yourself following our tutorials
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/machines.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/machines.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/extr-machine.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/extr-machine.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Build your workspace</h4>
<div class="step-text">
  Super, you’ve got your machines! But machines alone are not enough. <br />
  <br />
  Follow our tutorials on how to fully setup your Mix Workspace with all the additional tools, furniture and posters needed to make your space ready.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/build-space.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/build-space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Safety</h4>
<div class="step-text">
  Before we start always make sure to stay safe! <br />
  <br />
  Of course, the machines get hot. And can cause a hazard in different ways, find out tips on how to stay safe here. It is also crucial to check out our safety videos on how to melt plastic safely here.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image7.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image7.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Get some plastic waste</h4>
<div class="step-text">
  Now you need to get some plastic waste to be recycled.<br />
  <br />
  You can make contact with a Collection Point near you or find clever ways to get people to bring you plastic. The volumes you will need are not huge so a small collection setup in your workspace can work as well.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image4.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image4.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Shred your plastic</h4>
<div class="step-text">
  Using your Shredder, shred the collected plastic waste into small shreds ready to be used in the other machines.<br />
  <br />
  Make sure to shred plastic by type and, as a plus, by colour, so you can creat beautiful patterns later on.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image20.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image20.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Get moulds</h4>
<div class="step-text">
  To make products you need moulds. Moulds can be done in multiple ways.<br />
  <br />
  You can have a look at our Create section in the Academy.<br />
  👉 tiny.cc/create-with-plastic<br />
  <br />
  And check out our How-to&#39;s to get inspired and learn about different moulds.<br />
  👉 community.preciousplastic.com/howto
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image19.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image19.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Make some products!</h4>
<div class="step-text">
  Now that you have all the things in place it’s time to start making some products with the different machines.<br />
  <br />
  Browse the How-to and Create chapter in the Academy to learn how to make different products and adopt the best practices.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image11.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image11.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Experiment and have fun</h4>
<div class="step-text">
  Make sure to play around and have some fun trying out new things, moulds, patterns and designs. Learning is about playing and experimenting.<br />
  <br />
  If you discover something cool, share it back to the community, so everyone can learn together :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image23.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image23.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Run workshops</h4>
<div class="step-text">
  As you become more comfortable and knowledgeable working with plastic and the machines you could start to give educational workshops in your community.<br />
  <br />
  You can run workshops at your workspace, events, conferences and conventions showing how Precious Plastic recycling works.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image6.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image6.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Maintenance</h4>
<div class="step-text">
  As you run your Mix Workspace it is crucial that you maintain the different machines in order to prevent failures.<br />
  <br />
  You can ask the local Machine Shop for help.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image15.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image15.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Create your profile</h4>
<div class="step-text">
  If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform to connect with people.<br />
  <br />
  Follow the link below and sign up with your email, pick your role, put your pin on the map and upload nice pics to show the world what you’re doing.<br />
  <br />
  👉 community.preciousplastic.com/sign-up
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image14.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image14.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Create How-to's!</h4>
<div class="step-text">
  Share to the world how you run your Mix Workspace so other people can learn from you and start using your solution to build machines to tackle the plastic problem. <br />
  <br />
  Make sure to only create How-tos for your best processes and techniques, not try outs or one offs. This can also help you create a name for yourself in the Precious Plastic community. <br />
  <br />
  👉 community.preciousplastic.com/how-to
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/image17.png">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/image17.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Learn and improve together</h4>
<div class="step-text">
  Precious Plastic is nothing without the people. Working together and helping each other. Participate in the community, share back and make use of it, it can really pay off!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/howto-collection-discord.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/howto-collection-discord.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-24">Step 24 - Good things take time</h4>
<div class="step-text">
  Starting off will take some time in the beginning. It’s normal. Be patience, work smart and reach out to your Precious Plastic community if you need help. Everything will take off. <br />
  <br />
  You’re changing the world.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/good-things.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/good-things.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-25">Step 25 - Download and start!</h4>
<div class="step-text">
  Ready and excited to start?<br />
  You&#39;re a hero!<br />
  <br />
  In this download package you will find all the files you need to set up your Mix Workspace.<br />
  Download and start your recycling journey!<br />
  <br />
  <br />
  👉 <a href="https://cutt.ly/starterkit-mix">https://cutt.ly/starterkit-mix</a> 👈
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-a-mix-workspace/Workspace Starter Kit.jpg">
        <img class="step-image" src="/howtos/set-up-a-mix-workspace/Workspace Starter Kit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>