<?php
require __DIR__ . '/vendor/autoload.php';

require 'debug.php';
require 'utils.php';

use Slim\Http\Request;
use Slim\Http\Response;
use SR\SessionUtils;
use SR\SignalUtils;

$app = new \Slim\App;

function defaultResponse($sessionId)
{
    return "OK(BF)|srC=" . urlencode("=3=srs=" . $sessionId . "=id:e84cefa5042eef39=1=ox=0=perc=100000=mul=1") . "|name=ruxitagent|featureHash=ICA23QSVfqrtu|buildNumber=10159181203155412|lastModification=1543849470229";
}
$app->post('/', function (Request $request, Response $response, array $args) {

    $sid = SessionUtils::toSID($request->getQueryParams()['s']);
    $body = $request->getBody()->getContents();

    if ($body === 'end') {
        SessionUtils::endSession($sid);
        return defaultResponse(SessionUtils::random());
    }
    $signal = SignalUtils::toSignal($request->getQueryParams(), $body);
    if ($signal) {
        $session = SignalUtils::saveSignal(
            $sid,
            $request->getQueryParams()['r'],
            $request->getQueryParams()['f'],
            $request->getQueryParams()['v'],
            $signal
        );
        // $session->save();
    }
    $response->getBody()->write(defaultResponse($sid));
    return $response;
});
$app->run();
