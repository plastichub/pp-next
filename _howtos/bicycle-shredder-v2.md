---
image: "/howtos/bicycle-shredder-v2/ND800_0005336_Loehrer.jpg"
title: "Bicycle Shredder V2"
tagline: ""
description: "In this how-to we’re giving you an overview about the construction of our bicycle shredder. One and a half years ago we made the first video about how to build a bicycle shredder. Since then, we made further adjustments and are now proud to present you the second version of the machine (with a much better documentation)."
keywords: "hack,shredder"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "hack"
- "shredder"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/kunststoffschmiede">kunststoffschmiede</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Get ready!</h4>
<div class="step-text">
  In the download package you find a lot of files and drawings. At first sight it might be overwhelming, but don’t be afraid, we’re taking care of you ;-).<br />
  In addition to the download-kit, the following steps will give you further information, why the shredder is built the way it is. Furthermore a short introduction video and an assembling video is in the making to get a better understanding about the construction. Stay tuned!<br />
  <br />
  Download-Link: <a href="https://www.dropbox.com/sh/xlts122wcb905q6/AABRgMZTki8gH1NqQ5SvOS-Ia?dl=0">https://www.dropbox.com/sh/xlts122wcb905q6/AABRgMZTki8gH1NqQ5SvOS-Ia?dl=0</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/ND800_0005383_freigestellt_Loehrer.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/ND800_0005383_freigestellt_Loehrer.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cutting Mill</h4>
<div class="step-text">
  Nearly all low speed shredders we saw in the community or around the internet were huge, heavy or sometimes not even working proper and efficient. Using high speed instead, we have the power of inertia :-). So we decided to shred plastic with human power at high speed.<br />
  There are different ways to build the axle. We decided to spend a bit more money on standard parts to reduce costs as machining and post machining on the lathe. That’s why we use those span sockets. However, we are aware that these components are not available in every country in the world. Take a look on the possibilities and competencies around you, it is also possible to manufacture the axle in a different way.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/cutting mill.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/cutting mill.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/ND800_0005357_Loehrer.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/ND800_0005357_Loehrer.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Material Lock</h4>
<div class="step-text">
  Shredding plastic with the bicycle shredder - the knives of the cutting mill should run at high speed. To ensure a safe and stable shredding process, we needed a way to get the plastic into the cutting mill without opening it. In the past we worked with several versions of a pipe lock. Finally, we dismissed the pipe lock because of malfunctions. That&#39;s why we developed a more classic and simple version of a save hopper. The downside of the new version is the huge size and weight compared to the old one.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/height adjustment.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/height adjustment.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/new material lock.png">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/new material lock.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Sieve and Collection Box</h4>
<div class="step-text">
  The sieve and collection box is nothing really special, just bended and welded metal sheets. We recommend using a sieve with holes about 5 - 8 mm in diameter. Very small holes will turn the plastic nearly into dust. Don&#39;t go too thin with the thickness of the metal sheet. It is important for stability and the locking system! 1,5 mm are a good size.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/collection box sieve.png">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/collection box sieve.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Friction Wheel</h4>
<div class="step-text">
  The friction wheel transfers the speed from the rear wheel to the cutting mill. We use plywood for the wheel because we can mill it with our own cnc machine. Nethertheless there are definitely better materials for the friction wheel, for example something out of plastic.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/friction wheel.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/friction wheel.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Steel Frame</h4>
<div class="step-text">
  Building the steel frame requires proper knowledge and experience in welding and working with metal. If you’re new in the game, look for someone with experience in metalwork.To simplify the welding process we prepared gauges -&gt; plywood sheets to keep the right distances between the steel tubes and some big 45° angles.<br />
  <br />
  Before welding everything make sure everything is drilled and milled into the steel tubes. Afterwards it&#39;s a bit tricky to get them to the right places!<br />
  <br />
  Another advice: welding the m16 nuts; make sure the two nuts are in one alignment. That&#39;s important to clamp the rear-wheel right in place. We take a long steel bar (14mm in diameter).
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/ND800_0005340_Loehrer.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/ND800_0005340_Loehrer.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/ND800_0005365_Loehrer.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/ND800_0005365_Loehrer.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Height Adjustment</h4>
<div class="step-text">
  The height adjustment is one of the fastest things you can realise :-).<br />
  For the turning knob we welded a small steel strip on a clamping ring. The rest of it are just ready-to-use standard parts.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/height adjustment.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/height adjustment.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Wheel Mount</h4>
<div class="step-text">
  The wheel-mount is a lot of lathe work but not too complex!<br />
  For the turning knob we lasered a part out of a thick metal sheet and welded it on a nut. Instead of a lasered part you can realise the turning knob as well by some steel strips and an angle grinder.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/wheel mount.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/wheel mount.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/ND800_0005337_Loehrer.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/ND800_0005337_Loehrer.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/ND800_0005379_Loehrer.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/ND800_0005379_Loehrer.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Safety Add-ons</h4>
<div class="step-text">
  Workshops with different kinds of people are our daily business. That is why we care a lot about safety!<br />
  To ensure an easy and understandable handling, a good way is to use our color system.<br />
  - Green: Everybody can touch/use it.<br />
  - Orange: The team can touch/use it.<br />
  - Red: Keep your body away from it, it&#39;s f***ing dangerous!<br />
  In addition to the color system we use common warning signs and tips and tricks written on the machine.<br />
  <br />
  Furthermore we build safety add-ons, such as the belt protection and the shaft protection.<br />
  <br />
  If you have ideas how to make the machine safer and better, feel free to share it with us and the community!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/bicycle-shredder-v2/shaft protection + belt protection.jpg">
        <img class="step-image" src="/howtos/bicycle-shredder-v2/shaft protection + belt protection.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>