### Is this for you? 

 For this role you will have to be very communicative and social. Ideally you like talking to people about the Precious Plastic online and in real life. So access to a phone and computer to use all the platforms properly is crucial.

As a community builder it’s also important to be organised and good at keeping an overview of the area and your local network. Your goal is to strengthen the existing community and expand to new audiences.

How you can go about sparking your community will vary depending on your context, but there are a number of tools you can use in order to get started.