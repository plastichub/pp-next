### Remake the thread 

 Take your M5 die to re-make the thread. It should be the same length as it was previously (measured in Step 3). You can even make it slightly longer (max. 5 mm), to ensure that the mould will close very tightly.

Cool, so now you have your clamps ready.
