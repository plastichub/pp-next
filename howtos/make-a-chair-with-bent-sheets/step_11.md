### Weld the legs 

 Notch the rear legs with the angle grinder, position them in the jig and tack them with the welding machine. Be careful not to set the wooden jig on fire!
Take the legs out of the jig to finish the weld.

☝️ Melting metal can get really warm, so reduce the welding time in the jig to the minimum and have common fire safety measures handy.
