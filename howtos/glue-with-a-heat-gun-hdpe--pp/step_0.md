### Stay safe 

 While handling a glue gun, basically we are melting glue, therefore gloves are needed in order to avoid any direct contact with the hot glue. Also, it’s better to work in an open and well ventilated space to avoid gases to accumulate. And even though it’s not obligatory it’s always recommendable to wear a mask. 

Safety recommendations:
- gloves
- ventilated space, mask
