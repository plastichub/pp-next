---
image: /_howto/extrusion/thermo_couple_clips/IMG_1727.JPG
category: "extrusion"
title: "Thermo Couple_clips"
tagline: ""
description: ""
usedin:
  - lydia-v
  - lydia
  - zoe
  - injection
  - elena
  - extrusion-pro
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_1719.JPG" _target="_blank">
        <img id="IMG_1719.JPG" src="./IMG_1719.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1720.JPG" _target="_blank">
        <img id="IMG_1720.JPG" src="./IMG_1720.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1721.JPG" _target="_blank">
        <img id="IMG_1721.JPG" src="./IMG_1721.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1722.JPG" _target="_blank">
        <img id="IMG_1722.JPG" src="./IMG_1722.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1723.JPG" _target="_blank">
        <img id="IMG_1723.JPG" src="./IMG_1723.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1724.JPG" _target="_blank">
        <img id="IMG_1724.JPG" src="./IMG_1724.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1725.JPG" _target="_blank">
        <img id="IMG_1725.JPG" src="./IMG_1725.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1727.JPG" _target="_blank">
        <img id="IMG_1727.JPG" src="./IMG_1727.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

