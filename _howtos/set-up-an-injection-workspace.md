---
image: "/howtos/set-up-an-injection-workspace/cover-injection-3.jpg"
title: "Set up an Injection Workspace"
tagline: ""
description: "In this How-to we’re going to guide you through all the steps to setup an Injection Workspace. Learn about plastic, how to find a space, get the Injection and Shredder machine, find customers and connect to the Precious Plastic Universe. <br /><br />Download Files:<br /> &lt;a href=&quot;https://cutt.ly/starterkit-injection&quot;&gt;https://cutt.ly/starterkit-injection&lt;/a&gt; <br /><br />Step 1-3: Intro<br />Step 4-9: Learn<br />Step 10-19: Set up<br />Step 20-25: Run<br />Step 26-29: Share"
keywords: "starterkit,injection"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "starterkit"
- "injection"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Role</h4>
<div class="step-text">
  First of all, make sure you had a look at the showcase page of this Starterkit!<br />
  👉 preciousplastic.com/starterkits/showcase/injection<br />
  <br />
  Now about your Role:<br />
  <br />
  Injection Workspaces shred plastic waste and transform it into valuable products. The volumes of plastic are smaller, so they can either set up a small plastic collection in-house or get in contact with the local Collection point. The products are sold directly to customers or organizations. Workshops are also a valuable asset for Injection Workspaces. <br />
  <br />
  Injection workspaces should also reach out to Community Point to connect with the local Precious Plastic community and maybe get help selling their products or find plastic waste.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/universe-injection.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/universe-injection.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Outcomes</h4>
<div class="step-text">
  The outcome of an Injection Workspace is recycled plastic products. These products can vary greatly depending on needs but they’re generally of high precision and can be made in series creating small productions for customers.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image13.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image13.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Is this for you?</h4>
<div class="step-text">
  For the Injection Workspace you will have to be quite technical as you have to understand how the Injection machine and shredder work, ideally know how to maintain it and, as a plus, know how to fix it when it needs a bit of extra love. <br />
  <br />
  You will also need some degree of design understanding and creativity to be able to come up with good products and design a mould. A plus would also be some knowledge on 3D software.<br />
  <br />
  And paying attention to details will result in stunning and high quality products.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image8.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image8.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Learn about Precious Plastic</h4>
<div class="step-text">
  As a start, it’s super important to really know and understand Precious Plastic. What drives the project, how it works, its philosophy and solutions.<br />
  <br />
  If you haven’t already, dig our website, community platform and Bazar to have a deep understanding of the project.<br />
  👉 preciousplastic.com<br />
  👉 community.preciousplastic.com<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  Also, make sure to check the Universe chapter to fully understand how the Precious Plastic Universe works.<br />
  👉 community.preciousplastic.com/academy/universe
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/learn-precious-plastic-universe.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/learn-precious-plastic-universe.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Check out your area</h4>
<div class="step-text">
  Get an overview of who and what is already existing in your area.<br />
  <br />
  Have a look on the Precious Plastic Map to see the activity around you. You can also search for more people on the Precious Plastic Bazar or search for #preciousplastic on social media. <br />
  <br />
  👉 community.preciousplastic.com/map<br />
  <br />
  Make sure not to jam the local network, if there are already five Injection places around you, have a chat with them how to collaborate or maybe consider starting another type of workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/communitypoint-step4-map-.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/communitypoint-step4-map-.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Find a Community Point near you</h4>
<div class="step-text">
  Community Points are the glue of the Precious Plastic Universe. They know the ins and outs of your local network.<br />
  <br />
  They can help you in multiple ways but they generally have a very in-depth overview of local Precious Plastic spaces, people, useful shops, resources and can help you with planning out your project. You can find them on the map. <br />
  <br />
  <br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image10.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image10.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Get on Discord</h4>
<div class="step-text">
  Day to day discussions in the Precious Plastic Universe happen on Discord.<br />
  <br />
  Introduce yourself, say hi in your country channel and start to discover the different channels where people go deep into specific topics (building, collection, design etc..)<br />
  <br />
  Join the Discord:<br />
  👉 discordapp.com/invite/rnx7m4t
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image23.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image23.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Learn the basics of plastic</h4>
<div class="step-text">
  Before you start it is crucial to get a solid understanding of plastic. How it works, the different types, melting temperatures and so on.<br />
  <br />
  Head over to our Academy and dive deep in the Plastic chapter. <br />
  👉 <a href="http://tiny.cc/basics-about-plastic">http://tiny.cc/basics-about-plastic</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image14.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image14.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Fill in the Action Plan</h4>
<div class="step-text">
  Before jumping into making machines or finding a space it is smart to sit down and properly plan your project and shape your vision. <br />
  <br />
  To help you plan we’ve made a graphic tool called the Action Plan that helps you to craft your mission, understand your customers, revenue streams, costs and much more. With this tool, you should get a step closer to create a successful project. <br />
  <br />
  You can find the Action Plan in the Download Kit or learn more in the Academy<br />
  👉 <a href="http://tiny.cc/business-actionplan">http://tiny.cc/business-actionplan</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image1.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Workspace Calculator</h4>
<div class="step-text">
  Now you have your project nicely planned out and it’s starting to take shape. <br />
  <br />
  It is important at this stage to make a serious estimation of how much it will cost you to set up and run your workspace. Otherwise, you might run out of money halfway. The Workspace Calculator is a spreadsheet that helps you to do that. <br />
  <br />
  You can find the Workspace Calculator in the Download Kit or learn more in the Academy:<br />
  👉 <a href="http://tiny.cc/workspace-calculator">http://tiny.cc/workspace-calculator</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image19.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image19.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Business Plan Template</h4>
<div class="step-text">
  To help you pitch your idea to potential partners, financial institutions or investors we made a Business Plan Template (and a specific example for the Injection Workspace) for you to use. This template helps you to talk the business language and should help you access the necessary money to begin. <br />
  <br />
  For more explanation check out the video in the Academy:<br />
  👉 <a href="http://tiny.cc/business-plan-calculator">http://tiny.cc/business-plan-calculator</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image11.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image11.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Tool list</h4>
<div class="step-text">
  Alongside your Injection and Shredder machine you will need a number of other tools and machines to help you with the operations of the Injection Workspace. In the Download Kit you will find a tool list with all the necessary tools to run your workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/ToolKit.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/ToolKit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Assemble your team</h4>
<div class="step-text">
  We’ve heard it over and over. People are what makes a project succeed or fail. Choose carefully and create a team of passionate people that want to change the world and are not afraid of sleepless nights.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/team.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/team.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Find a space</h4>
<div class="step-text">
  To help you find the perfect place for your workspace in the Download Kit you will find a floor plan with all the minimum requirements and a little cardboard tool to place your machines and tools in the workspace.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/space.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Get your machines</h4>
<div class="step-text">
  Cool, now you have a space it’s time to get hold of your Injection and Shredder machine. There are three ways to do that:<br />
  <br />
  1 Build them yourself following our tutorials<br />
  👉 tiny.cc/build-injection<br />
  👉 tiny.cc/build-shredder<br />
  <br />
  2 Buy them on the Bazar.<br />
  👉 bazar.preciousplastic.com<br />
  <br />
  3 Find a Machine Shop near you on the map that can build them for you.<br />
  👉 community.preciousplastic.com/map
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/machines.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/machines.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-16">Step 16 - Build your workspace</h4>
<div class="step-text">
  Super, you’ve got your machines! But machines alone are not enough. <br />
  <br />
  Follow our tutorials on how to fully setup your Injection Workspace with all the additional tools, furniture and posters needed to make your space ready. <br />
  <br />
  👉 tiny.cc/space-injection
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/build-space.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/build-space.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-17">Step 17 - Safety</h4>
<div class="step-text">
  Always stay safe. <br />
  <br />
  Of course, Injection machines get hot. And can cause a hazard in different ways.<br />
  <br />
  So before starting to melt, please check out our safety video to learn about dangers and how to stay safe when working with plastic.<br />
  <br />
  👉 <a href="http://tiny.cc/plastic-safety">http://tiny.cc/plastic-safety</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/Safety 1.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/Safety 1.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-18">Step 18 - Get your precious plastic</h4>
<div class="step-text">
  Now you need to get some plastic waste to be recycled. <br />
  <br />
  You can make contact with a Collection Point or find clever ways to get people to bring you plastic. The volumes you will need are not huge so a small collection setup in your workspace can work.<br />
  <br />
  If you want to dive more into ways to collect plastic,
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image4.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image4.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-19">Step 19 - Shred your plastic</h4>
<div class="step-text">
  Shred the plastic waste collected into small shreds ready to be used in the injection machine.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image16.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image16.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-20">Step 20 - Get some moulds</h4>
<div class="step-text">
  To make products you need moulds. Moulds can be done in multiple ways. You can check our Create section in the Academy or the How-to for inspiration and to learn more about moulds.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image22.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image22.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-21">Step 21 - Make some products!</h4>
<div class="step-text">
  Now that you have all the things in place it’s time to start injecting some products. Browse the How-to and Create chapter in the Academy to learn how to make an injected product and adopt the best practices.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image12.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image12.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-22">Step 22 - Make a variety</h4>
<div class="step-text">
  Once you get a grasp on the process make sure to make a nice variety of colours and patterns to attract different customers. Once mastered a product, you could think of new products and designs.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image9.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image9.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-23">Step 23 - Maintenance</h4>
<div class="step-text">
  As you run your Injection Workspace it is crucial that you maintain the Injection and Shredder machine in order to prevent failures.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image18.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image18.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-24">Step 24 - Sell</h4>
<div class="step-text">
  You can now make great products in series. Now is crucial to find people and organisations that want to buy your recycled products.<br />
  <br />
  First, you should put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. Then, you have to get creative on how to sell your products locally. Shops, design studios, online stores and more. <br />
  <br />
  Possibilities are endless and, nowadays, everyone wants a piece of recycled plastic!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image6.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image6.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-25">Step 25 - Run workshops</h4>
<div class="step-text">
  A great way to educate (and earn some money) is to make workshops and demonstrations for people, organizations and schools. You can run workshops at events, conferences and conventions showing how Precious Plastic recycling works.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image5.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image5.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-26">Step 26 - Create your profile</h4>
<div class="step-text">
  If you haven’t already, it’s time to create your profile on the Precious Plastic Community Platform to connect with people. Follow this link and sign up with your email, pick your role, put your pin on the map and upload nice pics to show the world what you’re doing.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image17.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image17.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-27">Step 27 - Create How-tos!</h4>
<div class="step-text">
  Share to the world how you run your Injection Workspace so other people can learn from you and start using your solution to tackle the plastic problem. <br />
  <br />
  Make sure to only create How-tos for your best processes and techniques, not try outs or one offs. This can also help you create a name for yourself in the Precious Plastic community.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/image21.png">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/image21.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-28">Step 28 - Stay active on Discord</h4>
<div class="step-text">
  Precious Plastic is people. People working together and helping each other. Go to Discord and connect with people.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/howto-collection-discord.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/howto-collection-discord.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-29">Step 29 - Good things take time</h4>
<div class="step-text">
  Starting off will take some time in the beginning. It’s normal. Be patience, work smart and reach out to your Precious Plastic community if you need help. Everything will take off. <br />
  <br />
  You’re changing the world. Thank you!
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/good-things.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/good-things.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-30">Step 30 - Download and start!</h4>
<div class="step-text">
  Ready and excited to start?<br />
  You&#39;re a hero!<br />
  <br />
  In this download package you will find all the files you need to set up your Injection Workspace.<br />
  Download and start your recycling journey!<br />
  <br />
  👉 <a href="https://cutt.ly/starterkit-injection">https://cutt.ly/starterkit-injection</a> 👈
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/set-up-an-injection-workspace/Workspace Starter Kit.jpg">
        <img class="step-image" src="/howtos/set-up-an-injection-workspace/Workspace Starter Kit.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>