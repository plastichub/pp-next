### Add the reinforcement 

 Depending on the material you have used, you may need to reinforce the seat. In this case a short section of a 25x25mm plastic beam has been fastened to the middle of the seat. However, wood will work just fine or perhaps another material.
