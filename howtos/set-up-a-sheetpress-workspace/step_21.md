### Sell 

 You can now make superb sheets. Many every day. Now is crucial to find people and organisations that want to buy your recycled sheets. 

Put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. 
And then you can get creative on how to sell your sheets locally. Get in touch with shops, design studios, online stores and more.

👉 bazar.preciousplastic.com