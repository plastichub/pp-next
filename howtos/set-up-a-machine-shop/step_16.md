### Buy in bulk 

 It’s often good to buy materials and parts in bulk as it can help you save money, packaging and shipping. However, this can be a little difficult in the beginning as it requires some initial capital to invest. 

We don’t recommend to accept orders/payments and then buy in bulk, this could result in delayed shipping and angry customers. If you do want to take this approach make sure to mention it before you sell, to prepare everyone for longer waiting times and keep your customers happy :)