### Sell your beams 

 You can now make beautiful beams. Many every day. Now is crucial to find people and organisations that want to buy your recycled beams. 

First, you should put them on the Precious Plastic Bazar to access an audience that is already interested in recycled products. Then you have to get creative on how to sell your beams locally. Shops, design studios, online stores and more. 
