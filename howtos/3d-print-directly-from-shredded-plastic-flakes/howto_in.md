### 3D print directly from shredded plastic flakes! 


This is a modified version of the Precious Plastic extruder that is optimized for large format 3D printing. It uses a NEMA23 stepper motor so that it can be controlled by existing 3D printing software and control boards.

You can mount this to any CNC gantry with a few inches of Z. I'm currently using a LowRider2 Gantry by V1engineering.com .

You can watch a tutorial video here: https://vimeo.com/381749194