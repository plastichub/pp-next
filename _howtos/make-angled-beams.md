---
image: "/howtos/make-angled-beams/0-cover.JPG"
title: "Make angled beams"
tagline: ""
description: "Beams make great use of the extrusion machine as they can be used for a big variety of objects and purposes. Adding an angle to the mould offers a strong and time saving solution to produce pieces with corners.<br />Here we will show you how to make the mould.<br /><br />To learn the basics about making regular beams first, have a look at the How to “Make a mould for extruding beams” <br />(https://alpha.onearmy.world/how-to/make-a-mould-for-extruding-beams)"
keywords: "extrusion"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "extrusion"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Gather materials and tools</h4>
<div class="step-text">
  To make a beam mould you will need:<br />
  - Metal tube (wall thickness preferably 3mm or more)<br />
  - Angle iron<br />
  - Metal sheet (3mm or more)<br />
  - Threaded pipe or fitting (BSPT size of your nozzle)<br />
  - Angle grinder or metal saw<br />
  - Welding machine<br />
  - File<br />
  - Drill<br />
  - M8 or M10 nuts and bolts
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/1 materials 1.JPG">
        <img class="step-image" src="/howtos/make-angled-beams/1 materials 1.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Cut tubes</h4>
<div class="step-text">
  In this technique you cut the mould in the same way as you would cut beams for a mitre joint: i.e. if you want an angle of 90° for your beam, make two 45° cuts.<br />
  Once you know which one you need, it’s time to cut your metal tubes to your desired angle. It’s important to make the cut as clean and straight as possible to avoid big gaps when the mould is assembled. Finish up your cuts with a file to ensure your edges are smooth for the next steps to come.<br />
  <br />
  Tip: Consider where the weld seam is on your piece of metal tube as it will l leave a mark on your plastic beam.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/2  2 file.JPG">
        <img class="step-image" src="/howtos/make-angled-beams/2  2 file.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/2 2 file.JPG">
        <img class="step-image" src="/howtos/make-angled-beams/2 2 file.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Make connecting brackets</h4>
<div class="step-text">
  Now we will need brackets to connect the two tube pieces with each other as well as the mould to the mounting plate.<br />
  (This is where you’ll be thanking yourself if you already prepared extra ones in earlier mould productions.)<br />
  <br />
  All you need to do is cut your angle iron into sections of 30-40 mm and drill a hole in one side to fit your bolts. For larger beams use bigger, thicker angle iron.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/3 1 cut-.jpg">
        <img class="step-image" src="/howtos/make-angled-beams/3 1 cut-.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/3 2 drill.jpg">
        <img class="step-image" src="/howtos/make-angled-beams/3 2 drill.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Cut a mounting plate</h4>
<div class="step-text">
  To connect the mould to the nozzle, we need a mounting plate. If you don’t have a fitting size handy, cut a metal sheet to a size that will fit your beam mould as well as the brackets you prepared in Step 3, and drill the holes for your brackets.<br />
  <br />
  Tip: Use a tube offcut from Step 2 to plan the position of your brackets and their holes.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/4 lay.JPG">
        <img class="step-image" src="/howtos/make-angled-beams/4 lay.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Weld mounting brackets</h4>
<div class="step-text">
  With your parts cut and ready, it’s time to weld.<br />
  As described in Step 5 of Make a mould for extruding beams, weld together the brackets to the beam mould and the nozzle to the mounting plate.<br />
  <br />
  To the attach the brackets that will hold together the two parts of the angled beam, lay both parts on a flat surface and plan where you want the brackets to sit. It can be helpful to start with the brackets bolted together and overlay one side (as shown on the image), so that they can function as an alignment.<br />
  Once everything sits in the right position, tack the brackets to the beam and repeat the process on the other side. Once all your parts are tacked into position, weld it all up!<br />
  Tip: By aligning the brackets with the angle of your cut, and adding a bit of space between the brackets you can achieve a well sealed joint.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/5 1 lay.JPG">
        <img class="step-image" src="/howtos/make-angled-beams/5 1 lay.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/5 2 weldone.jpg">
        <img class="step-image" src="/howtos/make-angled-beams/5 2 weldone.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Make it ready to extrude</h4>
<div class="step-text">
  Alright, your mould is basically ready to go. Bolt it all together and have a look inside to make sure that there are no large gaps.<br />
  Then you can start shooting that plastic inside!<br />
  <br />
  Tip for extruding: Stop your plastic before it erupts out the end. If there is too much plastic overflowing, it will be hard to demould later.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/6-extrude 1.JPG">
        <img class="step-image" src="/howtos/make-angled-beams/6-extrude 1.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Explore new possibilities</h4>
<div class="step-text">
  This technique opens up a new world of possibilities. It can be especially valuable in cases when being used instead of joining two pieces with a screw or another mechanism. Saves time and materials, and enables an easier disassembly and recyclability. :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-angled-beams/7-finito.jpg">
        <img class="step-image" src="/howtos/make-angled-beams/7-finito.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>