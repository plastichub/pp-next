### Threading the hole 

 When working with bolts, you might want to thread the inside of the whole. Plastic materials fully embrace this technique.
For tough materials like HDPE and PP this is not urgently needed though, as just screwing in the bolt is often enough to hold it pretty tight. 