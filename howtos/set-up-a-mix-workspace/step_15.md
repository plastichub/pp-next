### Get moulds 

 To make products you need moulds. Moulds can be done in multiple ways.

You can have a look at our Create section in the Academy.
👉  tiny.cc/create-with-plastic

And check out our How-to's to get inspired and learn about different moulds.
👉  community.preciousplastic.com/howto