---
image: /pp/_howto/extrusion/motor_flange_o/IMG_1595.JPG
category: "extrusion"
title: "Motor Flange_o"
tagline: ""
description: ""
enabled: false
usedin:
  - lydia-v4
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_1558.JPG" _target="_blank">
        <img id="IMG_1558.JPG" src="./IMG_1558.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1562.JPG" _target="_blank">
        <img id="IMG_1562.JPG" src="./IMG_1562.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1576.JPG" _target="_blank">
        <img id="IMG_1576.JPG" src="./IMG_1576.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1585.JPG" _target="_blank">
        <img id="IMG_1585.JPG" src="./IMG_1585.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1588.JPG" _target="_blank">
        <img id="IMG_1588.JPG" src="./IMG_1588.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1590.JPG" _target="_blank">
        <img id="IMG_1590.JPG" src="./IMG_1590.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1591.JPG" _target="_blank">
        <img id="IMG_1591.JPG" src="./IMG_1591.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1592.JPG" _target="_blank">
        <img id="IMG_1592.JPG" src="./IMG_1592.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1595.JPG" _target="_blank">
        <img id="IMG_1595.JPG" src="./IMG_1595.JPG" width="100%" />
    </a>
    <span class="thumb-label">9. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}.html" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

