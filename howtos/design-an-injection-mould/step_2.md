### “Mold” your idea 

 What will define your product shape? As this broom hanger will be made with the injection machine, I have to think in an enclosed mold. With the product idea I’ve drawn before, I start designing the mold, looking for the best solution for each of these 6 Mold Design 

Criteria:
1) Define the product shape, 
2) Use standard measures, 
3) Receive the plastic and connect the mold,
4) Open after injection and eject the product,
5) Adjust and closure, and
6) Simplify the machining process.

I’ll explain each in the next steps.
