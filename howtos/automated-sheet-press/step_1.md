### Heating: heater configuration 

 We chose to create 2 separate loops in every heated plate (Upper and Lower). One is the core or inside loop (5 cartridge heaters), the other is the outside loop (4 heaters). We expect the center loop to have less heat loss so it will shut down sooner/more. 
One of the reasons we made a small version is to be able to use it in our mobile workspace. Reducing power consumption to 230V increases the chance we can plug it in almost anywhere.
