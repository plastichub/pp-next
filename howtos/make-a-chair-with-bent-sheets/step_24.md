### Finish the frame 

 As plain steel easily rusts, we recommend giving the frame a nice finish. You can use various methods like a paint and brush, spray paint or powder coating. Have a look what method suits your available materials and capabilities best. In this case we used Montana Gold 7090 Coke spray paint.

Remove the plastic parts for painting and put them back on once the paint is dry
