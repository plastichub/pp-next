---
image: "/howtos/15m-sheetpress-system---designed-for-disassembly/Group 102.png"
title: "1.5m Sheetpress System - Designed for Disassembly"
tagline: ""
description: "Complete set of blueprints, CAD files and instructions on how to build this upscaled sheetpress system.<br /><br />-56% larger pressing surface<br />-Designed for easy disassembly<br />-Improved electronics"
keywords: "sheetpress"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "sheetpress"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Download</h4>
<div class="step-text">
  Here&#39;s a link to the build folder:<br />
  <a href="https://drive.google.com/drive/u/1/folders/19glmxFnX6oH5BvBAc8NxFk5f-9YG9HNb<br/>">https://drive.google.com/drive/u/1/folders/19glmxFnX6oH5BvBAc8NxFk5f-9YG9HNb<br /></a>
  <br />
  If you&#39;re setting up a sheetpress workspace, you should still download our sheetpress workspace starterkit from our academy. Just use this build folder instead of the build folder in the main starterkit.<br />
  <br />
  And here&#39;s a link to our original build video. The design is slightly different but the steps are the same:<br />
  <a href="https://www.youtube.com/watch?v=j3OctDe3xVk">https://www.youtube.com/watch?v=j3OctDe3xVk</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/15m-sheetpress-system---designed-for-disassembly/Group 1.png">
        <img class="step-image" src="/howtos/15m-sheetpress-system---designed-for-disassembly/Group 1.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Bigger Press = Bigger Sheets</h4>
<div class="step-text">
  This system has been upscaled to a 1.5x1.5m pressing surface. This allows you to easily make plastic sheets at the standard 1.2x1.2m size and even a bit bigger.<br />
  <br />
  This is a 56% increase in capacity so you&#39;ll be able to recycle plastic at more than 50% the rate as the V4 press.<br />
  <br />
  <br />
  Note: The press requires 1500x3000 sheets of steel and aluminium to build. It&#39;s also quite a bit heavier, so it&#39;s no longer possible to handle the parts in this machine without the assistance of a hoist or similar lifting device.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/15m-sheetpress-system---designed-for-disassembly/DSC02831.JPG">
        <img class="step-image" src="/howtos/15m-sheetpress-system---designed-for-disassembly/DSC02831.JPG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/15m-sheetpress-system---designed-for-disassembly/untitled.47.jpg">
        <img class="step-image" src="/howtos/15m-sheetpress-system---designed-for-disassembly/untitled.47.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Designed for Disassembly</h4>
<div class="step-text">
  The whole system has been designed to be disassembled for easy packing and transportation. It now fits through a standard door when disassembled. <br />
  <br />
  The electronics box can also be unplugged and dismounted for transportation.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/15m-sheetpress-system---designed-for-disassembly/untitled.48.jpg">
        <img class="step-image" src="/howtos/15m-sheetpress-system---designed-for-disassembly/untitled.48.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/15m-sheetpress-system---designed-for-disassembly/DSC02852.JPG">
        <img class="step-image" src="/howtos/15m-sheetpress-system---designed-for-disassembly/DSC02852.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Improved Electronics</h4>
<div class="step-text">
  We&#39;ve made some changes to the electrical system to better suit the increased power requirements of this larger machine.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/15m-sheetpress-system---designed-for-disassembly/Group 2.png">
        <img class="step-image" src="/howtos/15m-sheetpress-system---designed-for-disassembly/Group 2.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Build it, hack it, share it!</h4>
<div class="step-text">
  If you have any other ideas, improvements or hacks for this machine - make sure to share them with the community.<br />
  You can do this through a how-to, discord or instagram!<br />
  <br />
  Have fun building :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/15m-sheetpress-system---designed-for-disassembly/DSC02844.JPG">
        <img class="step-image" src="/howtos/15m-sheetpress-system---designed-for-disassembly/DSC02844.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>