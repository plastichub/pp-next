---
image: /_howto/sheetpress/cartridge_heater/hole/block_vise/IMG_1931.JPG
category: "sheetpress"
title: "Block Vise"
tagline: ""
description: ""
usedin:
  - sheetpress-cell
  - sheetpress
---





<div class="thumbs"><div class="thumb">
    <a href="./IMG_1921.JPG" _target="_blank">
        <img id="IMG_1921.JPG" src="./IMG_1921.JPG" width="100%" />
    </a>
    <span class="thumb-label">1. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1922.JPG" _target="_blank">
        <img id="IMG_1922.JPG" src="./IMG_1922.JPG" width="100%" />
    </a>
    <span class="thumb-label">2. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1923.JPG" _target="_blank">
        <img id="IMG_1923.JPG" src="./IMG_1923.JPG" width="100%" />
    </a>
    <span class="thumb-label">3. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1924.JPG" _target="_blank">
        <img id="IMG_1924.JPG" src="./IMG_1924.JPG" width="100%" />
    </a>
    <span class="thumb-label">4. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1926.JPG" _target="_blank">
        <img id="IMG_1926.JPG" src="./IMG_1926.JPG" width="100%" />
    </a>
    <span class="thumb-label">5. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1928.JPG" _target="_blank">
        <img id="IMG_1928.JPG" src="./IMG_1928.JPG" width="100%" />
    </a>
    <span class="thumb-label">6. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1930.JPG" _target="_blank">
        <img id="IMG_1930.JPG" src="./IMG_1930.JPG" width="100%" />
    </a>
    <span class="thumb-label">7. </span>
</div>
<hr />
<div class="thumb">
    <a href="./IMG_1931.JPG" _target="_blank">
        <img id="IMG_1931.JPG" src="./IMG_1931.JPG" width="100%" />
    </a>
    <span class="thumb-label">8. </span>
</div>
<hr /></div>



{% if page.usedin %}
### This is used in

<div class="ty-vendor-plans">

{% for used in page.usedin %}
    {% for doc in site.machines %}
      {% if used == doc.product_id %}
        <div class="ty-grid-list__item">
          <a href="{{ doc.url  | relative_url }}" class="link">
            <span class="image" >
              <img class="cover" src="{{ doc.image }}" alt="" />
            </span>
            <header class="major">
                {{ doc.title }}
          </header>
          </a>
        </div>
      {% endif %}
    {% endfor %}
  {% endfor %}
</div>
{% endif %}

