### What makes a good bend 

 The best condition for a good bend is good material.

The quality of the plastic sheet affects the result. The more homogeneous the material is (one type of plastic from the same source) the better the bend. If the plate has multiple sources of HDPE flakes, delamination can occur and affect the result. 

The thickness of the material will also affect how much can we bend the plates. Thicker plates may not bend much without mayor deformations. Also, for thicker plates we may consider to reduce the temperature and increase the exposure timing in order to avoid the surface to degrade.

Have fun bending and exploring new possibilities! :)