### Build the jigs 

 Leg alignment: Cut the parts (1-3 and four wedges), drill the holes and screw everything together as shown in the drawings.

Leg bending: Transfer the measurements from the plan to part 4, cut the required wooden pieces and screw them in place. 

Bending angle: Cut according to drawings (or adapt to your bending device)

Cross-brace: Drill the 18mm tube holes, then saw along the cutting line. Make sure to stay on the hatched side! Assemble by screwing parts 5 in place.

Sheet shape: Pre-drill holes and cut parts according to the drawing, then sand them smooth.

Put some extra effort into the precision during this phase, as all the flaws made here are hereditary.
