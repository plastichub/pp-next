### Welding it to your mould 

 Once a smooth motion is achieved, you can weld this piece to your mould permanently. Take care to not block the movement with fresh welds. 
