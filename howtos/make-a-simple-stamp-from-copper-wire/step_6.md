### Stamping 

 Now you are ready to stamp. The copper shows through a slight changing of color very good when it is hot enough to stamp. This shouldn’t take more than a few seconds. Carefully press the copper on the plastic and see how it melts your work into it. Don’t apply too much pressure as the copper gets very soft when warm and can bend. Different materials need different handling so always when using something new have a piece for testing on hand.

