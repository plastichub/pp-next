---
image: "/howtos/make-a-ruler-mould-with-inkscape/ruler.jpg"
title: "Make a ruler mould with Inkscape"
tagline: ""
description: "Educational resources are not equally distributed across the world so you might as well recycle plastic and make products that are useful tools for learning and creating at the same time. In this how-to you will learn how to use Inkscape to make a laser cut ruler mould for injection moulding!"
keywords: "research,other machine,product,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "research"
- "other machine"
- "product"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/darigov-research">darigov-research</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Download Inkscape</h4>
<div class="step-text">
  -Go to <a href="https://inkscape.org/">https://inkscape.org/</a> and select the distribution you need for your operating system<br />
  -Follow the instructions for the installation guide<br />
  <br />
  -For reference intermediary files and the final file which was made by doing the steps in this tutorial is attached to the how-to so you can test to see if you can reproduce the final product on your own or adapt it to your needs
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/1_Inkscape_website.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/1_Inkscape_website.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/1_Inkscape_download_page.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/1_Inkscape_download_page.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Create a blank document</h4>
<div class="step-text">
  -Open Inkscape and it should open a new document for you<br />
  -You can adjust the document size by clicking File &gt; Document Properties<br />
  -We chose A4 for this project but you can adjust this to your needs<br />
  -Save it down in a place you can find later
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/2_New_Document.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/2_New_Document.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/2_Document_properties_dropdown.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/2_Document_properties_dropdown.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/2_Document_properties_menu.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/2_Document_properties_menu.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Generate the ruler</h4>
<div class="step-text">
  Vector Ruler Generator is a free and open source tool to generate rulers for etching/laser cutting into various materials<br />
  <br />
  -Go to <a href="https://robbbb.github.io/VectorRuler/<br/>">https://robbbb.github.io/VectorRuler/<br /></a>
  -On our ruler we wanted both centimetres and inches and to be roughly 15 cm long<br />
  -Select the parameters you want and save them down somewhere you can find them with understandable file names so you know which one&#39;s which
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/3_Vector_Ruler_Generator.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/3_Vector_Ruler_Generator.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/3_Vector_Ruler_Generator_centimetre_settings.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/3_Vector_Ruler_Generator_centimetre_settings.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/3_Vector_Ruler_Generator_inch_settings.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/3_Vector_Ruler_Generator_inch_settings.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Import the files</h4>
<div class="step-text">
  -Click File &gt; Import and select the file<br />
  -You will see a dialogue screen with some details<br />
  -Then click OK and you should see the files now<br />
  -Then repeat for the other ruler
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/4_Import_a_file.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/4_Import_a_file.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/4_Dialogue_Screen.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/4_Dialogue_Screen.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/4_Imported_files.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/4_Imported_files.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Resize the rulers</h4>
<div class="step-text">
  Because it may not import correctly into the software we will need to resize it to be accurate and adjust it for laser cutting. We will be doing this using guides in Inkscape<br />
  <br />
  -To create a guide click on the ruler on the left hand side and drag to the right<br />
  -You should now see a blue guide, double click on it and set it to 150 mm and click enter<br />
  -Then resize the ruler so that the left hand side touches the line of the document and the right hand touches the guide<br />
  -Then repeat for the inch ruler with the guide set to 6 inches
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/5_Guide_set.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/5_Guide_set.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/5_Ruler_resize_cm.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/5_Ruler_resize_cm.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/5_Ruler_resize_in.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/5_Ruler_resize_in.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Adjust for laser cutting</h4>
<div class="step-text">
  If you are laser cutting a mould you need to ensure that the text is flipped so it reads correctly when you inject the part<br />
  <br />
  -Select the part you wish to flip<br />
  -Then click on the &quot;Flip selected objects horizontally&quot; button on the top of the page<br />
  -You can also use the &quot;Rotate selection 90°&quot; buttons to adjust as necessary
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/6_Select_item.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/6_Select_item.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/6_Flip_item.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/6_Flip_item.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/6_Rotate_item.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/6_Rotate_item.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Add plastic identifier</h4>
<div class="step-text">
  Noting what plastic the rulers you are making will be made from is important because then the person in the future who is recycling it will know what temperatures to use. These can be found in the download kit for Precious Plastic or you can download them from the files attached to this How-to.<br />
  <br />
  -Import the symbol you need in the same way you done with the ruler files<br />
  -It will then give you some import settings<br />
  -We selected &quot;rough&quot; precision to keep the file size down and because we know that the detail will be sufficient for laser cutting<br />
  -Now align it and place it where you want it to be
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/7_Import_window.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/7_Import_window.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/7_Imported_identifier.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/7_Imported_identifier.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/7_Adjusted_identifier.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/7_Adjusted_identifier.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Add cut outs for the ruler</h4>
<div class="step-text">
  -To add a rectangle for the outline of the ruler click the Rectangle icon in the left menu<br />
  -You can now draw it around the rulers and select the specific height and width from the top menu<br />
  -You may need to adjust the alignment of the rectangles so that the rulers are visible<br />
  -Select the item and click the &quot;Lower selection to bottom&quot; icon from the top of the screen<br />
  -Repeat for the outer border of the mould
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/8_Create_inner_rectangle.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/8_Create_inner_rectangle.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/8_Lower_Rectangle.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/8_Lower_Rectangle.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/8_Create_outer_rectangle.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/8_Create_outer_rectangle.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Add fastening items</h4>
<div class="step-text">
  You&#39;ll now need to add holes for wing nuts and bolts or quick release clamps.<br />
  <br />
  -Select the circle icon in the left menu<br />
  -Draw a circle in the rough location<br />
  -You can adjust the x &amp; y radius from the top menu<br />
  -Now select it and duplicate it by pressing CTRL + D<br />
  -Now hold CTRL and move it to the right (holding control means it will snap to be aligned in the x or y axis of where it was before)<br />
  -Now select both top holes and duplicate them and do the same to create the bottom two holes
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/9_Insert_first_circle.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/9_Insert_first_circle.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/9_Insert_second_circle.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/9_Insert_second_circle.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/9_Insert_last_circles.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/9_Insert_last_circles.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Make center mould</h4>
<div class="step-text">
  -Select all of the items using the selection tool or by pressing CTRL + A<br />
  -Now duplicate all the objects pressing CTRL + D<br />
  -Now hold CTRL and move the duplicated objects to be just below it to avoid wasted material when laser cutting<br />
  -Now you can delete the rulers and the material logo by selecting it and pressing delete
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/10_Select_all.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/10_Select_all.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/10_Duplicate_mould.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/10_Duplicate_mould.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/10_Remove_rulers_and_identifier.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/10_Remove_rulers_and_identifier.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Make top mould</h4>
<div class="step-text">
  -Select all of the center mould using the selection tools and duplicate it again<br />
  -Now move it down like in the previous step so that the edges are touching<br />
  -Now you can delete the center rectangle and replace it with a circle for the nozzle of the injection machine that you have
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/11_Select_center_mould.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/11_Select_center_mould.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/11_Duplicate_center_mould.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/11_Duplicate_center_mould.PNG" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/11_Add_nozzle_circle.PNG">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/11_Add_nozzle_circle.PNG" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Export, laser cut & inject</h4>
<div class="step-text">
  -You can now save it down as an .svg by clicking File &gt; Save as<br />
  -Save it somewhere you will find it later<br />
  -You can now import it into the CAM (computer Aided Manufacturing) software for your laser to etch the rulers, inner rectangle &amp; id code of the bottom plate and cut out all the other lines<br />
  -Now you will need to tap the threads and you can now injection mould it using the injection machine<br />
  -See the video by Kunststoffschmiede on the Dave Hakkens Community Channel for details on how to do this with Plexiglas
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Spread the Love</h4>
<div class="step-text">
  -Open source is not Open source if you don&#39;t share the source &amp; files<br />
  -You can put it on GitHub or other places like GitLab as well<br />
  -Feel free to share your work with us on our social media<br />
  <br />
  At Darigov Research we specialise in open source hardware, software and education to help people tackle global issues in their local community.<br />
  <br />
  If you wish to support us in the work that we do consider donating or joining us on Patreon<br />
  <br />
  Donate - <a href="https://www.darigovresearch.com/donate<br/>">https://www.darigovresearch.com/donate<br /></a>
  Patreon - <a href="https://www.patreon.com/darigovresearch<br/>">https://www.patreon.com/darigovresearch<br /></a>
  <br />
  Website - <a href="https://www.darigovresearch.com/<br/>">https://www.darigovresearch.com/<br /></a>
  Youtube Channel – <a href="https://www.youtube.com/channel/UCb34hWA6u2Lif92aljhV4HA<br/>">https://www.youtube.com/channel/UCb34hWA6u2Lif92aljhV4HA<br /></a>
  Twitter, GitHub, Instagram - @darigovresearch
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Get started</h4>
<div class="step-text">
  We&#39;ve been building Precious Plastic machines since V2 of the machines so we&#39;re very passionate about the great work that has been done so far and where this community will grow to!<br />
  <br />
  If you&#39;re interested in purchasing a machine or interested in inquiring about our services for any research and development purposes do take a look at the products we are selling on the Bazar or message us directly!<br />
  <br />
  <a href="https://bazar.preciousplastic.com/darigov-research-limited/">https://bazar.preciousplastic.com/darigov-research-limited/</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-a-ruler-mould-with-inkscape/14_Get_Started.png">
        <img class="step-image" src="/howtos/make-a-ruler-mould-with-inkscape/14_Get_Started.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>