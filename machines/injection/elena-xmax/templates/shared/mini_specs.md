<span id="specs" style="padding: 16px">
    <table class="center specs">
        <tbody>
            <tr>
                <td>Type
                </td>
                <td>Manual Injection
                </td>
            </tr>
            <tr>
                <td>Version
                </td>
                <td>1.4
                </td>
            </tr>
            <tr>
                <td> Status
                </td>
                <td> Mature
                </td>
            </tr>
            <tr>
                <td>Weight
                </td>
                <td> 40 KG
                </td>
            </tr>
            <tr>
                <td>Dimensions
                </td>
                <td>
                    Packaged : 50cm x 15 cm x 110cm <br/>
                    Assembled : 1.6m x 20 cm x 1.1m
                </td>
            </tr>
            <tr>
                <td>Voltage
                </td>
                <td> 110V / 220V
                </td>
            </tr>
            <tr>
                <td>Flake size:
                </td>
                <td> Small to Medium
                </td>
            </tr>
            <tr>
                <td>Shot size
                </td>
                <td> 350 mm (barrel length) x 25 mm (barrel diameter) : <br/> 0.17 Liter | 171.81 cm³ | 150 gramm (PP)
                </td>
            </tr>
            <tr>
            <td> License
                </td>
                <td><a href="https://ohwr.org/cernohl">CERN Open Source Hardware License</a>
            </td>
            </tr>
            <tr>
            <td> Authors </td>
                <td>
                 [PlasticHub S.L.](${author_link})
                </td>
            </tr>
        </tbody>
    </table>
</span>
