---
image: "/howtos/create-bottle-top-earrings/how-to-bori-15.png"
title: "Create bottle top earrings"
tagline: ""
description: "An easy technique to transform bottle tops into cool colourful earrings."
keywords: "product,HDPE,compression"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "product"
- "HDPE"
- "compression"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Prepare your materials</h4>
<div class="step-text">
  First off, you will need to source your plastic! You can find plastic bottles pretty much anywhere, and all you need are the lids. If you can get a pair of the same, so that you can make a nice matching set. You will also need some shredded plastic of the same material (generally HDPE, or PP) in a complementary colour to the bottle tops.<br />
  <br />
  Materials needed (for one pair of earrings)<br />
  - 2 bottle tops<br />
  - some shredded plastic of the same material <br />
  - 2 earring hooks or rings (depending on your preference)<br />
  <br />
  Required tools:<br />
  - 2 smooth tiles for melting the plastic<br />
  - oven<br />
  - drill (or another tool for making a hole in the material)<br />
  <br />
  Safety recommendations:<br />
  - heat resistant gloves<br />
  - respirator mask
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-14.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-14.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-13.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-13.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-12.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-12.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Get ready for melting</h4>
<div class="step-text">
  Set your oven to preheat at 170°C. <br />
  Whilst it’s warming up, set down one of your tiles and lay out the bottle tops with even and sufficient spacing to allow for the caps to melt without fusing to one another. With your coloured shreds, put a little sprinkling of plastic into each bottle top. This will create a nice aesthetic to the finish! You will not need to add much, as the pressed material should be relatively thin.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-4.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-4.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Transfer tiles to the oven</h4>
<div class="step-text">
  Once the oven is preheated, it’s time to put your tiles in and watch the plastic melt! Both tiles will need to be heated, one with the bottle tops prepared (as in the previous step) and the other should be heated for pressing.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-5.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-5.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Check on the melting progress</h4>
<div class="step-text">
  Now your tops and tiles are heating in the oven, there is a little time to wait. With our oven we leave the tops for 30 minutes. During this time, you could already prepare another batch of earrings (see step 2). After 10 minutes, check on the melting progress of your bottle tops. <br />
  <br />
  Some oven strengths vary, so you might need have to adjust the melting temperature or time for the best result. Below, you can see how the bottle tops looked after 10 minutes, and then how the looked when ready for pressing, after 30 minutes in the oven.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-17.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-17.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-7.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-7.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Press your earrings</h4>
<div class="step-text">
  After the bottle tops are sufficiently melted, put on your respirator mask, and heat resistant gloves. Remove the two tiles from the oven one by one. First, lay down the tile with your melted material on a flat and heat resistant surface, then immediately place the second tile on top of it, with the smooth side facing down.<br />
  <br />
  For pressing, you will need to give a little pressure to the tiles. But not too much, otherwise the material will become too thin, and the bottle tops may spread and fuse together. Be mindful of this and press carefully. <br />
  <br />
  The plastic will need some time to cool, for this you should leave the two tiles as they are, one on top of the other. If you prepared another batch, now is the time to put it into the oven (step 3). If not, you have a few minutes to make yourself a cup of tea :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-8.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-8.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-9.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-9.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Finish the plastic</h4>
<div class="step-text">
  Once the tiles have cooled (around 10 minutes) you can separate them and reveal your freshly pressed plastic discs. With practice, they should come out nearly perfectly round, but if some don&#39;t have the desired shape, now is the time to finish them. This can be done by sanding or cutting to achieve the desired outcome. Alternatively, you can select the best and save the rest for re-shredding.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-10.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-10.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-11.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-11.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Finish the earrings</h4>
<div class="step-text">
  Now the discs are at the desired shape and size, all that’s left to do is to drill a hole and attach your earring hook or hoop. We used a drill press for this process, but you can use regular drill, or another tool to form a small hole in the plastic.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-3.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-3.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-2.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-2.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Finished!</h4>
<div class="step-text">
  Once the earring element is attached, these beauties are ready to wear! Simply put them on, and enjoy.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/create-bottle-top-earrings/how-to-bori-1.png">
        <img class="step-image" src="/howtos/create-bottle-top-earrings/how-to-bori-1.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>