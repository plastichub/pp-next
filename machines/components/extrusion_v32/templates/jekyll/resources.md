# Resources


<hr/>

## Controllino - Uno - Installation

We use [Controllino-UNO-Pure](https://www.controllino.biz/product/controllino-mini-pure/) which is an industrial grade DIN rail Arduino.
Please check more on the website, they also have distributors all over the planet.

{% include product_image.html src="resources/controllino-uno.jpg" size="medium" %}

### General requirements

* PC with Arduino IDE (1.6.4 or newer) (Windows, Linux, MAC)
* Internet connection

### CONTROLLINO library

* Start Arduino IDE, navigate to Sketch–>Include Library–>Manage Libraries
* In the Library Manager type CONTROLLINO into the filter text box and search for CONTROLLINO library
* When found, select the latest version and install it. The installation process should be fully automated
* When finished – check in Sketch–>Include Library menu that you can see the CONTROLLINO library there
* You can also check if you can see the set of CONTROLLINO examples in File->Examples->CONTROLLINO

### CONTROLLINO boards hardware definition

* Navigate to File–>Preferences
* Copy-paste the following link to the Additional Boards Manager URLs: [https://raw.githubusercontent.com/CONTROLLINO-PLC/CONTROLLINO_Library/master/Boards/package_ControllinoHardware_index.json](https://raw.githubusercontent.com/CONTROLLINO-PLC/CONTROLLINO_Library/master/Boards/package_ControllinoHardware_index.json)
* Press OK button
* Then navigate to Tools–>Board: “Foo“–>Boards Manager
* In the Boards Manager type CONTROLLINO into the filter text box and search for CONTROLLINO boards
* When found, select the latest version and install it. The installation process should be fully automated
* When finished – check in Tools–>Board: “Foo“–> menu that you can see the CONTROLLINO boards there

#### Controllino Pin Layout

{% include product_image.html src="resources/CONTROLLINO-MINI-Pinout_small.jpg" size="medium" target="resources/CONTROLLINO-MINI-Pinout.jpg" %}
