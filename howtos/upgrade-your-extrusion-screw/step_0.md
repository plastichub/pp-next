### Get a screw 

 V2 of the extrusion machine uses a wood auger to transport the plastic from one side to the other of the barrel while being heated up and melted. It works fine for most applications but if you want to push extrusion to the next level you need a screw that builds up pressure while transporting the plastic.

You need an industrial screw.

You can download the technical drawing (go to top of this page) if you're adventurous enough to make it yourself, or you can buy it on the Precious Plastic Bazar.

👉 bazar.preciousplastic.com