### Set up a Shredder Workspace 


In this How-to we’re going to guide you through all the steps to set up a Shredder Workspace. Learn about plastic, how to find a space, get the shredder, find customers and connect to the Precious Plastic Universe. 

Download files:
👉 https://cutt.ly/starterkit-shredder 👈

Step 1-3: Intro
Step 4-8: Learn
Step 9-11: Set up
Step 12-17: Run
Step 18-20: Share