### Workspace Calculator 

 Now you have your project nicely planned out and it’s starting to take shape in your mind. 

It is important at this stage to make a serious estimation of how much it will cost you to set up and run your workspace. Otherwise, you might run out of money halfway. The Workspace Calculator is a spreadsheet that helps you do just that. 

You can find the Workspace Calculator in the Download Kit or learn more in the Academy.
👉 http://tiny.cc/workspace-calculator