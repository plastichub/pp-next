### Automated Sheet Press 


Here we describe some of the adaptations we made to the sheetpress. It is a step towards an automated version. The main goal was to adapt the sheetpress so that it needs minimal time investment. 
Our version has a 600x600mm pressing surface. We chose this size to fit our needs: a transportable machine that runs on standard single phase 230V.

If you want more info, contact us :)

