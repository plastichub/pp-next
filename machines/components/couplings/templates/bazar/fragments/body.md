<p>This is our best seller and is truly open source. It comes with auto-reverse and lots of safety functions.</p>

<div>
    <a href="${product_overview}">
        <p style="text-align: center">
            <img width="100%" src="${product_overview}"/>
        </p>
    </a>
    <a href="https://precious-plastic.org/products/products/components/hal/overview.JPG">
        <p style="text-align: center">
            <img width="100%" src="https://precious-plastic.org/products/products/components/hal/overview.JPG"/>
        </p>
    </a>
</div>

### Features

- Safe hopper – with optional door.
- Very precise auto-reverse when jamming. It stops after 3 trials and gives visual
- Status lights : Ok & Error
- Emergency and reset button
- Various motor protections (thermal & magnetic).
- Jam detection for extrusion mode.
- Rigid, flexible and modular framework – enabling hacking and extensions. - The base plate is 15 mm thick.
- The legs have M10 threads on each end so you can connect levelers or wheels. The legs have also extra holes to install easily extra sheets all around.
- Extrusion hopper can be easily detached.
- Thermocouples (sensors) have clips, easy to detach.
- Complies also with basic regulations in schools.
- Stops automatically after one hour of inactivity.
- High quality cables and wiring according to industrial standards.
- High precision mounts for shredder & extrusion, making sure everything is aligned perfectly.
- Won’t start when power is back.
- Strong 32 mm hexbar and 35 mm driveshaft
- Compression screw

<hr/>

<div>    
    <div style="padding: 16px; display: inline-block">
        ${product_resources}
    </div>
</div>
