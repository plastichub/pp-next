### Learn and improve together 

 Precious Plastic is nothing without the people. Working together and helping each other. Participate in the community, share back and make use of it, it can really pay off!