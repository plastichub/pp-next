# Precious Plastic Shredder - version 3.5

Modified PreciousPlastic shredder for vending usage

### Shredder modifications:

- stronger housing
- 32mm hexbar
- 30mm shaft
- UCFL206 Bearings

### Features

- Auto-Plunger
- Auto-Reverse
- Safety door
- Optional : auto-Stop/Shred (uses optical sensor)


### CAD Todos

- [ ] Integrate M32 locknuts
- [ ] Add M32 thread(s) on axle
