---
title: "Lydia-v4" 
permalink: "/specs/products/lydia-v4"
layout: "print"
---

## {{page.title}}

{% include specs.html %}



| Specification    |     |
|----------|-------------|
| Title   |  Lydia-v4     |
| Version | v4.3 |
| Type   |     Single Screw Extrusion   |
| Weight |   110 kg   |
| Dimension   | 1052 x 753 x 368 mm|
| Power (W) | 5 kW |
| Voltage | 400V or 220V |
| Amperage | 16A |
| Input Flake Size  | Small  |
| Screw diameter | 30mm |
| Length of screw (mm) | 790 mm |
| Effective screw length | 600 mm |
| Rated Motor Power | 3 kW |
| Motor Type   |    |
| - Rated Motor output Torque |  109 Nm |
| - Rated Motor output speed |   263 RPM   |
| - Max. Motor and Inverter power   | 3 kW|
| - Recommended motor shaft   | 30 mm|
| - Heating zones   | 3 |
| - Heating power: max.   | 2 kW|

{% include product_image.html size="medium" src="/products/lydia-v4/drawings/spec.JPG" %}

<div class="row vertical-margin vertical-gap">
  <div class="col">
    <h3 class="text-center" id="components">Dimensions</h3>
    <div class="vertical-margin vertical-gap"></div>
    <div class="parts_table">
{% include product_image.html size="medium" src="/products/lydia-v4/drawings/dimensions.JPG" %}
    </div>
  </div>
</div>

<div class="row vertical-margin vertical-gap">
  <div class="col">
    <h3 class="text-center" id="components">Parts</h3>
    <div class="vertical-margin vertical-gap"></div>
    <div class="parts_table">
{% include product_image.html size="medium" src="/products/lydia-v4/drawings/parts.JPG" %}
    </div>
  </div>
</div>