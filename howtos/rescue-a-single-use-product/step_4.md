### Find the right spot 

 Now you will need to find places where you can place this set-up and start to collect. Visit some locations that use a certain single-use product that you want to collect. Do this with the bucket and the poster, that will make it easier for the owner of the location to understand what you mean. You can also use the presentation + the “how to interact with businesses”. 
