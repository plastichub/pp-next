### Inject the Grips 

 Turn the heater on.
Assemble the cylinder with the basis hexagon (Nbr. 4).
Add the three threaded rod.
Add the washer around the cylinder.
Add the tube.
Add the top hexagon with the injecting hole (Nbr. 2).
Add the top hexagon with the hexagonal hole (Nbr. 1).
Secure it with the nuts.
Start filling the machine with plastic.
Wait 5 min.
inject.

Remove the grip using the M8 threaded rod and nut.