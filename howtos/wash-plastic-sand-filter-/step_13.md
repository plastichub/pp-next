### Cleaning the pre-filter 

 Cleaning the pre-filter is simple, but needs to be done quite frequently. 

First, pump the water out. For this, you turn it upside down, turn on the pump and then unscrew it. The filter capsule then can be taken out, without dirty plastic water all over the place.

Take a dry dishwashing brush and brush the dirt off the membrane into a container, as it contains lots of micro plastic and you want to avoid it getting into the drainage.