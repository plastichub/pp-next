### Find a good time 

 Next step is to find a date and time which work for as many people as possible.

As you are the one organising, the first step is to see when it would work for yourself. Often it helps to suggest a few options to choose from (you can also do this with help of websites like doodle.com). Pick some dates which are a few weeks away, so you have time to promote it and people have time to plan it into their schedules.

Sidenote: Don’t worry if you can’t find a common date for everyone - it’s just the first meetup of (hopefully) many more, so there will be more opportunities to join.
