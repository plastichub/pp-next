### Try out your fusing technique 

 ❗️Put on your respirator mask on!

Layer a few pieces of your reserved plastic one above the other, as a test sample. Turn on your iron and set to the highest temperature. (If you are using a thermo press you may need apply different temperature settings). Lay your sample piece on top of the teflon fabric, and put a piece of baking paper on top to avoid the plastic from sticking to your iron or press. Now iron the sample piece to see how the material fuses together.