### Reinforce your machine 

 You can simply swap this screw for your existing one, but make sure you reinforce your machine. This is very important as this screw brings way more pressure on your machine, particularly on the barrel-holder, the motor and the bottom plate.
 
Have a look at our ‘Beam making’ Video to learn more about this and see how you can make beams with this upgraded screw.

👉 tiny.cc/extrude-beams

Have fun extruding!