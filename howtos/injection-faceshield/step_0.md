### Order the laser cut parts 

 In the Downloadkit you can find all the DXF Files for lasercutting the parts.

I used 6mm steel for the top, bottom and one of the insert plate.
The other insert plate is made out of 5mm steel so that you can make the holes in the foil with an office punch.

If you are planing to make a bigger production, there is also a CNC Mould attached.
U can skip to step 4, if u are using the CNC Mould.
For the CNC Mould you can use 20 - 25mm aluminium.
