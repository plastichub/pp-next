---
image: "/howtos/make-blueprints-in-freecad/Howto-blueprints.png"
title: "Make Blueprints in FreeCAD"
tagline: ""
description: "Whether you&#39;re making moulds, building machines or other work making Blueprints is a key tool to communicate your ideas to builders, colleagues and manufacturers. FreeCAD is an amazing bit of free open source software to make 3d models and much more that is available on all platforms. It has a Blueprint feature built in and this How-Tt will show you how to use it to make Blueprints of your own!"
keywords: "other machine,product,research,mould"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "other machine"
- "product"
- "research"
- "mould"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/darigov-research">darigov-research</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Download FreeCAD</h4>
<div class="step-text">
  -Go to <a href="https://www.freecadweb.org/downloads.php">https://www.freecadweb.org/downloads.php</a> and select the distribution you need for your operating system<br />
  -Follow the instructions for the installation guide
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/0_Freecad.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/0_Freecad.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/1_Download.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/1_Download.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Import 3D Model</h4>
<div class="step-text">
  -We took this model from the starter kit but you can also use a part that you have designed yourself in FreeCAD<br />
  -Create a new empty document by clicking the new document button in the menu<br />
  -Click File &gt; Import then select the file you wish to use
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/2_New_Document.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/2_New_Document.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/2_Import.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/2_Import.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/2_File_View.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/2_File_View.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Select the TechDraw Workspace</h4>
<div class="step-text">
  -FreeCAD has various workspaces which has different buttons and are used for different use cases<br />
  -The one we need is the TechDraw workspace as it is used for create<br />
  -All you need to to do is select it from the dropdown at the top of the application
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/3_Workspace_Dropdown.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/3_Workspace_Dropdown.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/3_TechDraw_Workspace.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/3_TechDraw_Workspace.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Generate a blank template</h4>
<div class="step-text">
  -FreeCAD already comes prepared with various Blueprint sizes and standards<br />
  -You can select it by clicking the folder icon in the TechDraw workspace and select the type<br />
  -Or you can select the default one by clicking the &quot;Insert new default Page&quot; button in the TechDraw workspace<br />
  -You can also make your own as it takes an SVG format so you can adapt it to your needs
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/4_Import_Template.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/4_Import_Template.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/4_Default_Template.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/4_Default_Template.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Import perspective view</h4>
<div class="step-text">
  -Select the item you wish to do a blueprint of in the view by left clicking the item<br />
  -Then holding CTRL &amp; clicking on the page in the side menu<br />
  -Click on the &quot;Insert Projection View&quot; button<br />
  -Open the Page and you will see the drawing in the page by <br />
  -You can adjust the rotation, scaling &amp; which views are visible from the side menu<br />
  -Click &quot;OK&quot; in the task menu to confirm your choice<br />
  -If it&#39;s not updating click the refresh button in the top menu<br />
  -The good thing about doing it this was is that the blueprint and any dimensions you add should update if you update the base model
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/5_Item_Selection.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/5_Item_Selection.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/5_Default_Preview.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/5_Default_Preview.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/5_Final_View.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/5_Final_View.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-6">Step 6 - Add Linear Dimensions</h4>
<div class="step-text">
  -Select two points by holding CTRL and clicking on the two points you need<br />
  -Click the horizontal or vertical dimension buttons &amp; adjust location of the dimension to your needs <br />
  -Adjust the location so the dimension is legible<br />
  -Repeat till all relevant information is present
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/6_Point_Selection.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/6_Point_Selection.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/6_Adjust_Dimension.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/6_Adjust_Dimension.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/6_All_Vertical_and_Horizontal_Dimensions.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/6_All_Vertical_and_Horizontal_Dimensions.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-7">Step 7 - Add Diameter Dimensions</h4>
<div class="step-text">
  -Select the circle you wish to add a diameter to<br />
  -Click on the Diameter dimension button<br />
  -Adjust the location so the dimension is legible<br />
  -Repeat for all circles in your drawing
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/7_Diameter_Selection.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/7_Diameter_Selection.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/7_Diameter_View.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/7_Diameter_View.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/7_All_Diameters.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/7_All_Diameters.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-8">Step 8 - Add Angle Dimensions</h4>
<div class="step-text">
  -Click the &quot;Insert Annotation&quot; button in the TechDraw workspace<br />
  -You can adjust the text &amp; text size in the left hand panel<br />
  -We have added the names of each of the views as the frame titles are not visible on export
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/8_Angle_Selection.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/8_Angle_Selection.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/8_Angle_Inserted.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/8_Angle_Inserted.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/8_Angle_Adjusted.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/8_Angle_Adjusted.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-9">Step 9 - Add Annotations</h4>
<div class="step-text">
  -Click the &quot;Insert Annotation&quot; button in the TechDraw workspace<br />
  -You can adjust the text &amp; text size in the left hand panel<br />
  -We have added the names of each of the views as the frame titles are not visible on export
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/9_Insert_Annotation.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/9_Insert_Annotation.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/9_Default_Annotation.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/9_Default_Annotation.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/9_All_Annotations.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/9_All_Annotations.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-10">Step 10 - Add Blueprint Details</h4>
<div class="step-text">
  -There is always a section in blueprints explaining the details of the drawing<br />
  -You can see by the green boxes what is editable in the template<br />
  -Click the green box within each section and it will open a modal window for you to fill in the text<br />
  <br />
  Don&#39;t forget to add<br />
  -Who made it<br />
  -When it was made<br />
  -What the dimensions of the notations are<br />
  -What the scale of the view is<br />
  -What version number it is<br />
  -If there are multiple sheets explaining the file
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/10_Blueprint_Details.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/10_Blueprint_Details.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/10_Designed_by_Changed.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/10_Designed_by_Changed.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/10_All_Details_Changed.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/10_All_Details_Changed.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-11">Step 11 - Cleanup</h4>
<div class="step-text">
  -As this is open source work we will need to remove the default text from the template<br />
  -While we&#39;re at it we can also remove the details table on the right hand side<br />
  -We done this by using a blank white .png file and using it as a mask<br />
  -Select the &quot;Insert Bitmap from file&quot; button and use the masks provided in this tutorial<br />
  -You&#39;ll need to put it in the right position &amp; adjust for the size<br />
  -This is done by adjusting the parameters after clicking it in the left hand panel
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/11_Insert_bitmap.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/11_Insert_bitmap.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/11_first_image.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/11_first_image.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/11_All_masks.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/11_All_masks.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-12">Step 12 - Add the license</h4>
<div class="step-text">
  -We will fill in the license of the starter kit and we need to note what was done for this blueprint<br />
  -We added it by using another annotation as shown before
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/12_License.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/12_License.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-13">Step 13 - Export</h4>
<div class="step-text">
  -There are many file formats that you can export to such as SVG, DXF &amp; PDF<br />
  -SVG &amp; DXF formats have their own buttons that you can click in the top menu<br />
  -If you wish to export to PDF just right click anywhere on the page and click &quot;Export PDF&quot;<br />
  -Save it somewhere you will find it later
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/13_Export.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/13_Export.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/13_Export_PDF.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/13_Export_PDF.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/13_Exported_PDF.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/13_Exported_PDF.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-14">Step 14 - Spread the Love</h4>
<div class="step-text">
  -Open source is not Open source if you don&#39;t share the source &amp; files<br />
  -You can put it on GitHub or other places like GitLab as well<br />
  -Feel free to share your work with us on our social media <br />
  <br />
  At Darigov Research we specialise in open source hardware, software and education to help people tackle global issues in their local community.<br />
  <br />
  If you wish to support us in the work that we do consider donating or joining us on Patreon<br />
  <br />
  Donate - <a href="https://www.darigovresearch.com/donate">https://www.darigovresearch.com/donate</a> <br />
  Patreon - <a href="https://www.patreon.com/darigovresearch<br/>">https://www.patreon.com/darigovresearch<br /></a>
  <br />
  Website - <a href="https://www.darigovresearch.com/">https://www.darigovresearch.com/</a> <br />
  Youtube Channel – <a href="https://www.youtube.com/channel/UCb34hWA6u2Lif92aljhV4HA<br/>">https://www.youtube.com/channel/UCb34hWA6u2Lif92aljhV4HA<br /></a>
  Twitter, GitHub, Instagram - @darigovresearch
</div>
<br />
<div class="step-images">
  <div class="row">
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-15">Step 15 - Get Started</h4>
<div class="step-text">
  We&#39;ve been building Precious Plastic machines since V2 of the machines so we&#39;re very passionate about the great work that has been done so far and where this community will grow to!<br />
  <br />
  If you&#39;re interested in purchasing a machine or interested in inquiring about our services for any research and development purposes do take a look at the products we are selling on the Bazar or message us directly!<br />
  <br />
  <a href="https://bazar.preciousplastic.com/darigov-research-limited/">https://bazar.preciousplastic.com/darigov-research-limited/</a>
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-blueprints-in-freecad/15_Get_Started.png">
        <img class="step-image" src="/howtos/make-blueprints-in-freecad/15_Get_Started.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>