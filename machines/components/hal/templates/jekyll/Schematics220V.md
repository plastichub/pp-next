
### Wiring - 220V

<ul class="nav nav-pills nav-fill" id="myTab" role="tablist">
  
  <li class="nav-item">
    <a class="nav-link active" id="circuit-tab" data-toggle="tab" role="tab" aria-controls="circuit" aria-selected="true" href="#circuit">Overview</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="power-tab" data-toggle="tab" role="tab" aria-controls="power" aria-selected="true" href="#power">Power Circuit</a>
  </li>
  
  <li class="nav-item">
    <a class="nav-link" id="extrusion-tab" data-toggle="tab" role="tab" aria-controls="extrusion" aria-selected="false" href="#extrusion">Extrusion Circuit</a>
  </li>
 <li class="nav-item">
    <a class="nav-link" id="digital-tab" data-toggle="tab" role="tab" aria-controls="digital" aria-selected="false" href="#digital">Digital Circuit</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="motor-tab" data-toggle="tab" role="tab" aria-controls="motor" aria-selected="false" href="#motor">Motor Circuit</a>
  </li>
</ul>

<div class="tab-content">

<div class="tab-pane active" id="circuit" role="tabpanel" aria-labelledby="circuit-tab">
  
{% include product_image.html src="resources/circuit.jpg"  target="resources/circuit.svg" title="Circuit Diagram" target="resources/circuit.svg"  %}

  </div>

<div class="tab-pane" id="power" role="tabpanel" aria-labelledby="power-tab">

{% include product_image.html src="resources/power_circuit.jpg"  target="resources/power_circuit.svg" title="Power Circuit Diagram" target="resources/power_circuit.svg"  %}

</div>
 
<div class="tab-pane" id="extrusion" role="tabpanel" aria-labelledby="extrusion-tab">

{% include product_image.html src="resources/extrusion_circuit.jpg" target="resources/extrusion_circuit.svg" title="Extrusion Circuit Diagram"  %}
</div>
  
<div class="tab-pane" id="digital" role="tabpanel" aria-labelledby="digital-tab">  

{% include product_image.html src="resources/digital_circuit.svg" title="Digital Circuit" target="resources/digital_circuit.svg"  %}

${component_digital}

[Table Source](${component_wiring_source})

</div>

<div class="tab-pane" id="motor" role="tabpanel" aria-labelledby="motor-tab">  

{% include product_image.html src="resources/motor_circuit.jpg" target="resources/motor_circuit.svg" title="Motor Circuit Diagram"  %}
  
</div>

</div>
