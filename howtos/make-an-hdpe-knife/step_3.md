### Fold and Twist 

 To maximise the marbling effect and to further reduce air bubbles, we like to pick up the plastic so that we can fold and twist it. Be extremely careful whenever handling molten plastic. We wear 2 pairs of gloves when doing this. The outer pair are called 'silicone oven mitts' and are the best gloves we have tried for doing this.

When folding and twisting be very careful make sure you are not introducing any more bubbles. Once you are happy, put it back in the sandwich toaster (HDPE cools rapidly and it needs to be as hot as possible before transferring to the mould!)