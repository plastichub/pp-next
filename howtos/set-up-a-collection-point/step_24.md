### collect.preciousplastic.com 

 To help you communicate with citizens and people around you how the Precious Plastic collection system works we made a little website to explain it, how to prepare plastic for recycling and how to find Collection Points near you. 

You can use this to get more and more people to bring you plastic.

👉 collect.preciousplastic.com