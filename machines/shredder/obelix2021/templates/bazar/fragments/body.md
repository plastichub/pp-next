**Upgraded v4 Shredder** 

<div style="clear:both">
    ${product_overview_drawings}
</div>

We upgraded the v4 shredder after instensive testing to make it work with a sieve but also for more materials.
### Features / Details

- Jam detection with auto reverse using industrial components
- Motor overheat protection
- Status lights : Ok & Error & Overload
- Emergency button
- Modular and professional firmware
- 3 Years guarantee
- After sales service
- Enclosure/Hopper : stainless
- 2 different sieves included
- Sieve can be height adjusted
- The upper hopper with door can be flipped back
- Hopper system can be detached fast for cleaning
- The bin has a window

<hr/>


${product_resources}
