### Connect all the bits! 

 Cyclone:
• Place the template (included with cyclone) in the middle of the lid
• Mark and drill the bolt holes
• Mark and cut centre hole
• Connect the cyclone using the nuts and bolts provided (see images in Step 3)

Funnel:
• Connect the funnel directly below the shredder sieve. We used a connected a D Square grate, some left over plumbing pipe and cut a whole in the cap (see image in Step 4)

Vacuum connection:
• Cut the vacuum pipe in half
• Using one half of the cut vacuum pipe, connect the top outlet of the cyclone to the vacuum 
• Connect the side inlet of the cyclone to the funnel beneath your shredder

Happy shredding!
