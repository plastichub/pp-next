### Wash plastic: Sand filter  


Washing plastics is a pretty dirty process. We’re dealing with organic contaminants, chemical waste but also lots of micro plastics. Being able to filter this water is very important.
There are many different filters, but a pressurised sand filter (Rapid Sand Filter) proved to be the most effective and accessible choice. Here we will explain how you can build and run it.

This machine integrates into a system with a pre-washing and a washing machine.
