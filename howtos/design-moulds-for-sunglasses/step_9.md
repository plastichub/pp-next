### Surface finishings 

 Depending on budget and intentions you can leave the tool paths, or bead blast or polish them.

1: Leaving the tool paths is the cheapest option. You can always play with the cutting direction to make patterns - the result can be pretty interesting. 

2: Bead blasting is the cheapest way to have an even surface in a mould. With this, however, it is not possible to get a shiny finish. There are several standards for bead blasting, varying for different parts of the world. Your manufacturing partner will be able to guide you through them.

3: Polishing offers the evenest result. As it is made by hand, it is also the most expensive one. Look for the grade you need.