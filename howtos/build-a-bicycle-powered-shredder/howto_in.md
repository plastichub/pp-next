### Build a bicycle powered shredder! 


In this How-to, you will learn how we constructed our bicycle powered plastic shredder, and how you can built your own! Not only does it give you a relatively cheap way to shred plastic at zero operating costs, but it also gives you freedom from the electrical grid, all while keeping you fit and healthy!

In step 1 you will find a detailed construction plan and all the files necessary. 