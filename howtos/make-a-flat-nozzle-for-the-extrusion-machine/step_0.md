### Gather materials and tools 

 To make a flat nozzle you will need:

- 26mm (1") Round Tube. *It must fit into your extruder head.
- 5mm x 5mm Square bar. 100mm will be enough
- File 
- Sander
- Vise
- Angle grinder or Metal saw
- Welding machine
- BSPT Thread cutter
