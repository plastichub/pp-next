### Sketch out your product idea 

 It can be difficult to get an idea down on paper in the beginning, but it’s equally hard to formulate your design without a drawing. Let’s start with your imagination. Register on paper all the details that you can think of to create a good design.
Especially consider important aspects like the wall thickness. Influencing factors for this will include 
A) The required injection pressure,
B) required flexibility of the product itself, and 
C) resistance and durability of the material

For this purpose, I use previous products I’ve developed to examine and compare the material.