---
image: "/howtos/make-an-under-glass-coaster/A59E05F0-C417-4DEF-B382-64E99D334F1A-7302341C-FCF1-4FA6-832A-B2D7EDAB8602.jpg"
title: "Make an under glass coaster"
tagline: ""
description: "This How-to explains how to create a beautiful under glass coaster from a HDPE sheet (20mm)."
keywords: "HDPE,sheetpress,product"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "HDPE"
- "sheetpress"
- "product"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/plasticfactorybe">plasticfactorybe</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Cut the squares</h4>
<div class="step-text">
  Start by cutting perfect equal squares 10 cm apart. The more precise they are, the more beautiful your coasters will. <br />
  <br />
  Make sure that you have a plastic sheet thick enough to be able to hollow out the circle that will accommodate the food of the glass. You need about 5 mm so use a sheet of at least 1cm thick
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3293.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3293.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3298.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3298.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Hollow the circle (1)</h4>
<div class="step-text">
  Now that you have your squares, you need à lathe machine to do your hollow circles. <br />
  <br />
  Wedge your square well in the clamping elements and make a first hole in the center of 5 mm deep.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3357.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3357.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3358.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3358.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3359.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3359.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Hollow the circle (2)</h4>
<div class="step-text">
  Now that you have your center hole you can start to do the rest of the circle. <br />
  <br />
  Make sure to stop digging when you have 5 mm of edge left on each side.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3364.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3364.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3367.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3367.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - smooth it out</h4>
<div class="step-text">
  With a polishing machine, round the 4 angles and smooth the faces if necessary. <br />
  <br />
  Also round the edge of the hollow circle
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3371.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3371.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3375.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3375.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/IMG_3379.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/IMG_3379.jpg" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Final result</h4>
<div class="step-text">
  Now you can drink a glass on a magnificent under glass coaster :)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/A59E05F0-C417-4DEF-B382-64E99D334F1A-7302341C-FCF1-4FA6-832A-B2D7EDAB8602.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/A59E05F0-C417-4DEF-B382-64E99D334F1A-7302341C-FCF1-4FA6-832A-B2D7EDAB8602.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/384470AB-33CF-403D-9F53-607A9313C54D-3D675F4E-AECC-4B3E-B9ED-473FB0F29DC8.jpg">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/384470AB-33CF-403D-9F53-607A9313C54D-3D675F4E-AECC-4B3E-B9ED-473FB0F29DC8.jpg" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/make-an-under-glass-coaster/905A90A7-8804-4E78-AF10-A8CDEB32A96D-F24A14D9-7E3A-467F-81A8-6215262871A8.JPG">
        <img class="step-image" src="/howtos/make-an-under-glass-coaster/905A90A7-8804-4E78-AF10-A8CDEB32A96D-F24A14D9-7E3A-467F-81A8-6215262871A8.JPG" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>