### Preparation of parts 

 Before assembling your mould for welding, all rough edges created by the laser cutting will need to be removed so the mould can be assembled tightly. To do this we used a handheld sander however this can easily be done with a Dremel or by hand with a file. Take extra care not to damage the sharp corners of the metal. 
