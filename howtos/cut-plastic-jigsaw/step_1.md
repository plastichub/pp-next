### Collect the chips 

 While cutting plastic with this tool, lots of chips will end up flying around the machine.

Try to keep your working area clean and collect the chips as they can be used for further recycling. Like this you help reducing the amount of waste generated in your workspace and you save the chips from ending up as micro plastic in our environment! :)