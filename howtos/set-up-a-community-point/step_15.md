### Keep growing 

 The more people get to know about Precious Plastic and participate, the better it can run and succeed.

So constantly keep your eyes open for new people and partners (and make sure they are in your contact list and on the map), so everyone in your community can find them.

To help you in the process we’ve prepared some graphic materials you can use as a starting point. 