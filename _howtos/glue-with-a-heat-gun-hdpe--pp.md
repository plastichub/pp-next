---
image: "/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-1 copy.png"
title: "Glue with a heat gun (HDPE & PP)"
tagline: ""
description: "Hot melt adhesives don’t offer the same performance as 2-components-adhesives, but they provide an accessible, quick and wasteless solution for those applications with no need for a high performance bonding. <br /> <br />We generally don&#39;t recommend glueing parts together as it will make the disassembly harder at the end of the product’s life. So check out other joining techniques first! If this is still needed, here some guidelines."
keywords: "HDPE,melting,PP"
enabled: "true"
sidebar:
nav: "howtos"
tags:
- "HDPE"
- "melting"
- "PP"
---
<h4 id="summary">{{page.title}}</h4> by <a href="https://community.preciousplastic.com/u/precious-plastic">precious-plastic</a> | Source: [Precious-Plastic-Community-Platform](http://community.preciousplastic.com/)
<br />
<br />
{{ page.description }}
---
---
<h4 class="step-title" id="step-1">Step 1 - Stay safe</h4>
<div class="step-text">
  While handling a glue gun, basically we are melting glue, therefore gloves are needed in order to avoid any direct contact with the hot glue. Also, it’s better to work in an open and well ventilated space to avoid gases to accumulate. And even though it’s not obligatory it’s always recommendable to wear a mask. <br />
  <br />
  Safety recommendations:<br />
  - gloves<br />
  - ventilated space, mask
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-2 copy.png">
        <img class="step-image" src="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-2 copy.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-2">Step 2 - Choose the right glue sticks</h4>
<div class="step-text">
  Through testing we found that hot melt adhesives based on ethylene vinyl acetate copolymer (yellowish) work best for PP and HDPE. Also hot melt adhesives based on acylate (white) could work, but only for HDPE. <br />
  <br />
  Of course the composition of the glue will vary depending on the brand. In our research the following BÜHNEN hot melts worked well:<br />
  - A21325.1 (for PP)<br />
  - A20364.1 and J2169 (for HDPE)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-3 copy.png">
        <img class="step-image" src="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-3 copy.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-3">Step 3 - Prepare the surfaces</h4>
<div class="step-text">
  It’s known that hot melts don’t offer a very strong bonding, so to maximize the contact area and improve the grip we recommend to scratch the surfaces where the joinery will be made.<br />
  <br />
  Before applying the hot melt make sure to clean the surfaces you are going to glue together with a non-aggressive product like IPA (Isopropyl Alcohol) cleaner and a Scotch Brite or similar to remove any dirt or release agent left from the mould.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-5 copy.png">
        <img class="step-image" src="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-5 copy.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-4 copy.png">
        <img class="step-image" src="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-4 copy.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-4">Step 4 - Apply the hot melt</h4>
<div class="step-text">
  Put the stick into the glue gun and wait until the machine is hot enough to melt it. Make sure you have all your pieces handy. Apply the hot melt on the first piece, then directly place the other piece on top and apply pressure (clamping preferred). <br />
  <br />
  You want to glue the pieces together while the glue is still hot (&lt; 25s). So make sure you have everything ready because you have very little time for error correction. Once you put them together, in a few seconds both pieces will get glued.
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-6 copy.png">
        <img class="step-image" src="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-6 copy.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-7 copy.png">
        <img class="step-image" src="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-7 copy.png" />
      </a>
    </div>
    <br />
    <div class="col-sm">
      <a href="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-8 copy.png">
        <img class="step-image" src="/howtos/glue-with-a-heat-gun-hdpe--pp/how-to-hotmeltadhesives-8 copy.png" />
      </a>
    </div>
  </div>
</div>
<br />
<br />
<h4 class="step-title" id="step-5">Step 5 - Recommended applications</h4>
<div class="step-text">
  Hot melts are generally accessible and easy to apply, but they offer a relatively weak bonding. But other than being a downside they can actually be a nice technique for designs which require a more temporary fix or assembly.<br />
  <br />
  (Another very functional application: This adhesive can help making other joinery methods watertight by sealing the surface.)
</div>
<br />
<div class="step-images">
  <div class="row">
    <div class="col-sm">
      <a href="/howtos/glue-with-a-heat-gun-hdpe--pp/how to glue  copy.png">
        <img class="step-image" src="/howtos/glue-with-a-heat-gun-hdpe--pp/how to glue  copy.png" />
      </a>
    </div>
  </div>
</div>
<br />
---
---
---
<div class="resources">
  {% if page.related %}
  <h3 id="related"> Related </h3>
  {% endif %}
  ---
  {% if page.similar %}
  <h3 id="related"> Similar </h3>
  {% endif %}
  ---
  {% if page.howto_tags %}
  <h3 id="related"> Tags </h3>
  {% endif %}
  {% if page.resources %}
  <h3 id="related"> Resources </h3>
  {% endif %}
  <div>