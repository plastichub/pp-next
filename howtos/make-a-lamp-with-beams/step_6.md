### Weld the metal bars to the u profile 

 The order in which you make this mold is quite important as having the sheet ready will help you welding the bars and the U profile in the right place. 

Set the components on the welding table, clamp the 2 metal bars on the top of the sheet, (the bars will lock the beam in the middle) make sure to clamp everything properly.

Firstly, tap weld the bars to the sheet (this will help you block the pieces), then you can start welding the bars along with the U profile stay, 50mm alternating each side, doesn’t need to be welded all length.
