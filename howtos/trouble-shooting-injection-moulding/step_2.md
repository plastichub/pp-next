### Flash 

 If plastic comes out through the split surface of your mould halfs.

Solutions:
- Check if your mould is fully clamped at the next try. Increase the clamping pressure if possible
- Check the flatness of your mould and if there is anything preventing your mould from closing fully
- Decrease the material temperature
- Decrease the injection pressure
