### Get ready 

 Alright so before starting, here some basics to check out and prepare first.

For this technique you will need:
Machines: Shredder (or already shredded plastic), Extrusion machine, Oven
Tools: Scissors / Knife, Pliers, Weighing scales and two containers, Silicone oil, Clamps, Spanner
Materials: Polystyrene and pigment

If you haven't extruded beams before, learn more in the How-to "Make a mould for beams".
(https://community.preciousplastic.com/how-to/make-a-mould-for-extruding-beams)