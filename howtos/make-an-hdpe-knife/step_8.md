### Drill and Shape the Scales 

 We drilled the 3 pin holes and used some brass rod with a small amount of superglue to hold the 2 scales together. This way when we are shaping on our grinder they both come out exactly the same.